﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ButtonHandler : MonoBehaviour {



	public Button MercuryButton;
	public Button VenusButton;
	public Button EarthButton;
	public Button MarsButton;
	public Button JupiterButton;
	public Button SaturnButton;
	public Button UranusButton;
	public Button NeptuneButton;
	public Button PlutoButton;

	List<Button> Planets = new List<Button>();

	public Sprite openButton;
	public Sprite closedButton;
	public GameObject GameHandler;
	GameHandler gameHandlerScript;



	// Use this for initialization
	void Start () {

		MercuryButton = GameObject.Find ("MercuryButton").GetComponent<Button>();
		Debug.Log (MercuryButton);




		Planets.Add(MercuryButton);
		Planets.Add(VenusButton); 
		Planets.Add(EarthButton);
		Planets.Add(MarsButton);
		Planets.Add(JupiterButton); 
		Planets.Add(SaturnButton);
		Planets.Add(UranusButton);
		Planets.Add(NeptuneButton); 
		Planets.Add(PlutoButton);

		gameHandlerScript = GameHandler.GetComponent<GameHandler> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void Clear() {
		foreach(Button planet in Planets)
		{
			planet.GetComponent<Image> ().sprite = openButton;
		}
	}
	public void SelectMercury() {
		Clear ();
		MercuryButton.GetComponent<Image> ().sprite = closedButton;
		MercuryButton.GetComponent<Image> ().gameObject.SetActive (true);
		gameHandlerScript.currentPlanetIndex = 0;
		Debug.Log ("Mercury");

	}
	public void SelectVenus() {
		Clear ();
		VenusButton.GetComponent<Image> ().sprite = closedButton;
		VenusButton.GetComponent<Image> ().gameObject.SetActive (true);
		gameHandlerScript.currentPlanetIndex = 1;

	}
	public void SelectEarth() {
		Clear ();
		EarthButton.GetComponent<Image> ().sprite = closedButton;
		EarthButton.GetComponent<Image> ().gameObject.SetActive (true);
		gameHandlerScript.currentPlanetIndex = 2;


	}
	public void SelectMars() {
		Clear ();
		MarsButton.GetComponent<Image> ().sprite = closedButton;
		MarsButton.GetComponent<Image> ().gameObject.SetActive (true);
		gameHandlerScript.currentPlanetIndex = 3;


	}
	public void SelectJupiter() {
		Clear ();
		JupiterButton.GetComponent<Image> ().sprite = closedButton;
		gameHandlerScript.currentPlanetIndex = 4;


	}
	public void SelectSaturn() {
		Clear ();
		SaturnButton.GetComponent<Image> ().sprite = closedButton;
		SaturnButton.GetComponent<Image> ().gameObject.SetActive (true);
		gameHandlerScript.currentPlanetIndex = 5;


	}
	public void SelectUranus() {
		Clear ();
		UranusButton.GetComponent<Image> ().sprite = closedButton;
		UranusButton.GetComponent<Image> ().gameObject.SetActive (true);
		gameHandlerScript.currentPlanetIndex = 6;

	}
	public void SelectNeptune() {
		Clear ();
		NeptuneButton.GetComponent<Image> ().sprite = closedButton;
		NeptuneButton.GetComponent<Image> ().gameObject.SetActive (true);
		gameHandlerScript.currentPlanetIndex = 7;

	}
	public void SelectPluto() {
		Clear ();
		PlutoButton.GetComponent<Image> ().sprite = closedButton;
		PlutoButton.GetComponent<Image> ().gameObject.SetActive (true);
		gameHandlerScript.currentPlanetIndex = 8;

	}

}
