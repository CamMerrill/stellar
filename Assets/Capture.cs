﻿// Attach this script to a Camera
//Also attach a GameObject that has a Renderer (e.g. a cube) in the Display field
//Press the space key in Play mode to capture

using UnityEngine;

public class Capture : MonoBehaviour

{
	public Texture2D texture;

	// Grab the camera's view when this variable is true.
	public bool grab;

	// The "m_Display" is the GameObject whose Texture will be set to the captured image.
	public Renderer m_Display;

	private void Update()
	{
		//Press space to start the screen grab
		if (Input.GetKeyDown(KeyCode.Space))
			grab = true;
	}

	public void Grab() {
		grab = true;

	}

	private void OnPostRender()
	{
		if (grab)
		{
			//Create a new texture with the width and height of the screen
			texture = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
			//Read the pixels in the Rect starting at 0,0 and ending at the screen's width and height
			texture.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0, false);
			texture.Apply();
			GameObject.Find ("GameHandlerContainer").GetComponent<GameHandler> ().texture = texture;
			GameObject.Find ("GameHandlerContainer").GetComponent<GameHandler> ().ToMuseumView ();

			grab = false;
		}
	}
}