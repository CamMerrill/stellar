﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_FlowHandler : MonoBehaviour {

	public GUISkin guiSkin;


	Rect windowRect = new Rect (0, 0, 400, 380);

	// Use this for initialization
	void Start () {
		windowRect.x = (Screen.width - windowRect.width)/2;
		windowRect.y = (Screen.height - windowRect.height)/2;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnGUI () 
	{
		GUI.skin = guiSkin;
	}
}
