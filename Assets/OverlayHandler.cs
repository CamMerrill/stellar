﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OverlayHandler : MonoBehaviour {

    public GameObject planet;
	// Use this for initialization
	void Start () {
        planet = GameObject.Find("HitCubeParent").transform.GetChild(0).gameObject;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void EnableOverlay()
    {
        transform.gameObject.SetActive(true);
    }

    void DisableOverlay()
    {
        transform.gameObject.SetActive(false);
    }
    public void ToggleOverlay()
    {
        transform.gameObject.SetActive(transform.gameObject.activeSelf);
    }
    public void IncreasePlanetSize()
    {
        planet.transform.localScale += new Vector3(0.1f, 0.1f, 0.1f);
    }
    public void DecreasePlanetSize()
    {
        planet.transform.localScale -= new Vector3(0.1f, 0.1f, 0.1f);
    }
    public void EnableRotation()
    {

    }
    public void DisableRotation()
    {

    }
    public void ToggleRotation()
    {

    }
}
