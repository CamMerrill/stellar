﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GameHandler : MonoBehaviour {


	public int currentPlanetIndex = 0;
	public GameObject MenuViewContainer;
	public GameObject ARViewContainer;
	public GameObject MuseumViewContainer;
    public GameObject MissionViewContainer;
    public GameObject FactViewContainer;
    public GameObject TutorialViewContainer;

	public Texture2D texture;


	Texture2D drawTexture;
	List<GameObject> Planets = new List<GameObject> ();
	List<string> Facts = new List<string> ();


	/* *
	 * 0 Mercury
	 * 1 Venus
	 * 2 Earth
	 * 3 Mars
	 * 4 Jupiter
	 * 5 Saturn
	 * 6 Uranus
	 * 7 Neptune
	 * 8 Pluto
	 * */


	// Use this for initialization
	void Start () {
		PopulatePlanets ();

		//Mercury
		Facts.Add ("\n\n\"Mercury has wrinkles.\"\n" +
			"As the iron core of the planet cooled and contracted, the surface of the planet became wrinkled. Scientist have named these wrinkles, Lobate Scarps. These Scarps hundreds of miles long!");
		//Venus
		Facts.Add ("\n\n\"Venus is the second brightest object in the night sky." + "\"\n" +
			"\n Only the Moon is brighter. With a magnitude of between -3.8 to -4.6 Venus is so bright it can be seen during daytime on a clear day.");
		//Earth
		Facts.Add ("\n\n\"Earth has a powerful magnetic field.\" \n \n " +
			"This phenomenon is caused by the nickel-iron core of the planet, coupled with its rapid rotation. This field protects the Earth from the effects of solar wind.");
		//Mars/
		Facts.Add ("\n\n\"Mars has the largest dust storms in the solar system.\" \n \n " +
			"They can last for months and cover the entire planet. The seasons are extreme because its elliptical (oval-shaped) orbital path around the Sun is more elongated.");
		//Jupiter
		Facts.Add ("\n\n\"The Great Red Spot is a huge storm on Jupiter.\" \n \n " +
			"It has raged for at least 350 years. It is so large that three Earths could fit inside it.\n");
		
		//Saturn
		Facts.Add ("\n\n\"Saturn can be seen with the naked eye. \n \n " +
			"It is the fifth brightest object in the solar system and is also easily studied through binoculars, or a small telescope!");
		//Uranus
		Facts.Add ("\n\n\"Uranus hits the coldest temperatures of any planet.\" \n \n " +
			"With minimum atmospheric temperature of -224°C Uranus is nearly coldest planet in the solar system. While Neptune doesn’t get as cold as Uranus it is on average colder.");
		
		//Neptune
		Facts.Add ("\n\n\"Neptune has 14 moons.\" \n \n " +
			"The most interesting moon is Triton. It was likely captured by the gravitational pull of Neptune. It is probably the coldest world in the solar system.");
		
		//Pluto
		Facts.Add ("\n\n\"Mercury is the smallest planet in the Solar System.\" \n \n " +
			"One of th 12,742 Kilometres for the Earth.");
		



		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void ToMenuView() {
		SetViewsFalse ();
		MenuViewContainer.SetActive (true);
		foreach (GameObject planet in Planets) {
			planet.SetActive (false);
		}

	}
	public void ToMuseumView() {
		SetViewsFalse ();

		MuseumViewContainer.SetActive (true);
		Sprite img = Sprite.Create(texture, new Rect(0,0,texture.width, texture.height), new Vector2(0.0f,0.0f));
		GameObject.Find ("PortraitImage").GetComponent<Image>().sprite = img;



	}
    public void ToFactView()
    {
		SetViewsFalse ();
		FactViewContainer.SetActive (true);

		Debug.Log ("Current Planet Index: " + currentPlanetIndex);
		GameObject planetName = GameObject.Find ("planetname");
		planetName.GetComponent<Text> ().text = Planets [currentPlanetIndex].name;


		GameObject.Find ("factbox").GetComponent<Text> ().text = Facts [currentPlanetIndex];
    }
    public void ToTutorialView()
    {
		SetViewsFalse ();
        TutorialViewContainer.SetActive(true);
    }

    
	public void ToARView() {
		SetViewsFalse ();
		PopulatePlanets ();
		ARViewContainer.SetActive (true);
	}

	public void SavePicture() {

		IOSCamera.Instance.SaveTextureToCameraRoll (texture);

	}

	private void OnImage (IOSImagePickResult result) {
		if(result.IsSucceeded) {
			drawTexture = result.Image;
		}
		IOSCamera.OnImagePicked -= OnImage;
	}

	private void PopulatePlanets() {
		ARViewContainer.SetActive (true);
		if (Planets.Count == 0) {

			Planets.Add (GameObject.Find ("Mercury"));
			Planets.Add (GameObject.Find ("Venus"));
			Planets.Add (GameObject.Find ("Earth"));
			Planets.Add (GameObject.Find ("Mars"));
			Planets.Add (GameObject.Find ("Jupiter"));
			Planets.Add (GameObject.Find ("Saturn"));
			Planets.Add (GameObject.Find ("Uranus"));
			Planets.Add (GameObject.Find ("Neptune"));
			Planets.Add (GameObject.Find ("Pluto"));
		}

		foreach (GameObject planet in Planets) {
			planet.SetActive (false);
		}
		Planets [currentPlanetIndex].SetActive (true);
		ARViewContainer.SetActive (false);
	}
	private void SetViewsFalse() {

		MenuViewContainer.SetActive(false);
		ARViewContainer.SetActive(false);
		MuseumViewContainer.SetActive(false);
		MissionViewContainer.SetActive(false);
		FactViewContainer.SetActive(false);
		TutorialViewContainer.SetActive(false);
	}


}
