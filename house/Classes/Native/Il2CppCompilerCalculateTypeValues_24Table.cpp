﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// SA.IOSNative.StoreKit.BillingInitChecker/BillingInitListener
struct BillingInitListener_t1231368121;
// System.Collections.Generic.List`1<UnityEngine.Texture2D>
struct List_1_t1017553631;
// System.String
struct String_t;
// SA.Common.Models.Error
struct Error_t340543044;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Collections.Generic.Dictionary`2<System.Int32,CK_Record>
struct Dictionary_2_t3086237675;
// CK_RecordID
struct CK_RecordID_t3242371914;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t1632706988;
// System.Collections.Generic.Dictionary`2<System.Int32,CK_RecordID>
struct Dictionary_2_t2131085245;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// IOSCamera
struct IOSCamera_t2024555079;
// System.Action`1<CK_RecordResult>
struct Action_1_t4181966144;
// System.Action`1<CK_RecordDeleteResult>
struct Action_1_t2630400500;
// System.Action`1<CK_QueryResult>
struct Action_1_t1209691443;
// System.Collections.Generic.Dictionary`2<System.Int32,CK_Database>
struct Dictionary_2_t1708305661;
// SA.IOSNative.UserNotifications.NotificationContent
struct NotificationContent_t1589641823;
// SA.IOSNative.UserNotifications.NotificationTrigger
struct NotificationTrigger_t2337490352;
// System.Action
struct Action_t1264377477;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Collections.Generic.Dictionary`2<System.Int32,GK_Score>
struct Dictionary_2_t3399002878;
// System.Collections.Generic.Dictionary`2<System.String,System.Action`1<SA.Common.Models.Result>>
struct Dictionary_2_t4128125612;
// System.Action`1<System.Collections.Generic.List`1<SA.IOSNative.UserNotifications.NotificationRequest>>
struct Action_1_t3266654270;
// System.Action`1<SA.Common.Models.Result>
struct Action_1_t47902017;
// System.Action`1<SA.IOSNative.UserNotifications.NotificationRequest>
struct Action_1_t1794579528;
// SA.IOSNative.UserNotifications.NotificationRequest
struct NotificationRequest_t1622111933;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t2865362463;
// System.Collections.Generic.List`1<GK_Player>
struct List_1_t638460237;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Collections.Generic.List`1<MP_MediaItem>
struct List_1_t3969582666;
// System.Char[]
struct CharU5BU5D_t3528271667;
// SA.IOSNative.UserNotifications.DateComponents
struct DateComponents_t2005439303;
// ISN_DeviceToken
struct ISN_DeviceToken_t822270907;
// CK_Record
struct CK_Record_t4197524344;
// CK_Database
struct CK_Database_t2819592330;
// GK_TBM_Match
struct GK_TBM_Match_t83581881;
// GK_RTM_Match
struct GK_RTM_Match_t949836071;
// System.Collections.Generic.Dictionary`2<System.String,GK_TBM_Match>
struct Dictionary_2_t4163805476;
// GK_SavedGame
struct GK_SavedGame_t3445071760;
// System.Collections.Generic.List`1<GK_SavedGame>
struct List_1_t622179206;
// System.Void
struct Void_t1185182177;
// System.Collections.Generic.List`1<CK_Record>
struct List_1_t1374631790;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// GK_TBM_Participant
struct GK_TBM_Participant_t2994240226;
// System.Collections.Generic.List`1<GK_TBM_Participant>
struct List_1_t171347672;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Action`1<GK_SaveDataLoaded>
struct Action_1_t244797016;
// ISN_LocalNotificationsController
struct ISN_LocalNotificationsController_t3493627228;
// ISN_MediaController
struct ISN_MediaController_t2009090688;
// SK_CloudService
struct SK_CloudService_t1139212890;
// ISN_ReplayKit
struct ISN_ReplayKit_t1483172299;
// IOSSocialManager
struct IOSSocialManager_t1565743305;
// SA.IOSNative.StoreKit.PaymentManager
struct PaymentManager_t249135127;
// ISN_RemoteNotificationsController
struct ISN_RemoteNotificationsController_t2928859492;
// SA.IOSNative.UserNotifications.NativeReceiver
struct NativeReceiver_t2681933426;
// ISN_FilePicker
struct ISN_FilePicker_t2788391163;
// IOSVideoManager
struct IOSVideoManager_t1926991955;
// iCloudManager
struct iCloudManager_t2877946005;
// ISN_CloudKit
struct ISN_CloudKit_t3134663278;
// System.Action`1<ISN_FilePickerResult>
struct Action_1_t2655722685;
// System.Action`1<IOSDialogResult>
struct Action_1_t416443874;
// System.Action`1<IOSImagePickResult>
struct Action_1_t4292612745;
// System.Action`1<System.String>
struct Action_1_t2019918284;
// System.Action`1<iCloudData>
struct Action_1_t267273497;
// System.Action`1<System.Collections.Generic.List`1<iCloudData>>
struct Action_1_t1739348239;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.Action`1<iCloudData>>>
struct Dictionary_2_t1524604538;
// System.Action`1<SA.IOSNative.StoreKit.RestoreResult>
struct Action_1_t1653158450;
// System.Action`1<SA.IOSNative.StoreKit.PurchaseResult>
struct Action_1_t3399145967;
// System.Action`1<SA.IOSNative.StoreKit.VerificationResponse>
struct Action_1_t3441667023;
// System.Collections.Generic.Dictionary`2<System.Int32,SA.IOSNative.StoreKit.StoreProductView>
struct Dictionary_2_t1460844201;
// System.Action`1<TextMessageComposeResult>
struct Action_1_t1565920682;
// System.Action`1<ISN_RemoteNotificationsRegistrationResult>
struct Action_1_t1103427813;
// ISN_RemoteNotification
struct ISN_RemoteNotification_t3991987818;
// System.Action`1<ISN_RemoteNotification>
struct Action_1_t4164455413;
// MP_MediaItem
struct MP_MediaItem_t2497507924;
// System.Action`1<MP_MediaPickerResult>
struct Action_1_t804714572;
// System.Action`1<MP_MediaItem>
struct Action_1_t2669975519;
// System.Action`1<MP_MusicPlaybackState>
struct Action_1_t1181815693;
// System.Action`1<SK_AuthorizationResult>
struct Action_1_t3677057630;
// System.Action`1<SK_RequestCapabilitieResult>
struct Action_1_t30760723;
// System.Action`1<SK_RequestStorefrontIdentifierResult>
struct Action_1_t1006234518;
// ISN_LocalNotification
struct ISN_LocalNotification_t2969587787;
// System.Action`1<ISN_LocalNotification>
struct Action_1_t3142055382;
// System.Action`1<ReplayKitVideoShareResult>
struct Action_1_t845705176;
// System.Action`1<SA.Common.Models.Error>
struct Action_1_t513010639;
// System.Action`1<System.Boolean>
struct Action_1_t269755560;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef BILLINGINITCHECKER_T1575809803_H
#define BILLINGINITCHECKER_T1575809803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSNative.StoreKit.BillingInitChecker
struct  BillingInitChecker_t1575809803  : public RuntimeObject
{
public:
	// SA.IOSNative.StoreKit.BillingInitChecker/BillingInitListener SA.IOSNative.StoreKit.BillingInitChecker::_listener
	BillingInitListener_t1231368121 * ____listener_0;

public:
	inline static int32_t get_offset_of__listener_0() { return static_cast<int32_t>(offsetof(BillingInitChecker_t1575809803, ____listener_0)); }
	inline BillingInitListener_t1231368121 * get__listener_0() const { return ____listener_0; }
	inline BillingInitListener_t1231368121 ** get_address_of__listener_0() { return &____listener_0; }
	inline void set__listener_0(BillingInitListener_t1231368121 * value)
	{
		____listener_0 = value;
		Il2CppCodeGenWriteBarrier((&____listener_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BILLINGINITCHECKER_T1575809803_H
#ifndef ISN_FILEPICKERRESULT_T2483255090_H
#define ISN_FILEPICKERRESULT_T2483255090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ISN_FilePickerResult
struct  ISN_FilePickerResult_t2483255090  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Texture2D> ISN_FilePickerResult::PickedImages
	List_1_t1017553631 * ___PickedImages_0;

public:
	inline static int32_t get_offset_of_PickedImages_0() { return static_cast<int32_t>(offsetof(ISN_FilePickerResult_t2483255090, ___PickedImages_0)); }
	inline List_1_t1017553631 * get_PickedImages_0() const { return ___PickedImages_0; }
	inline List_1_t1017553631 ** get_address_of_PickedImages_0() { return &___PickedImages_0; }
	inline void set_PickedImages_0(List_1_t1017553631 * value)
	{
		___PickedImages_0 = value;
		Il2CppCodeGenWriteBarrier((&___PickedImages_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISN_FILEPICKERRESULT_T2483255090_H
#ifndef MP_MEDIAITEM_T2497507924_H
#define MP_MEDIAITEM_T2497507924_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MP_MediaItem
struct  MP_MediaItem_t2497507924  : public RuntimeObject
{
public:
	// System.String MP_MediaItem::_Id
	String_t* ____Id_0;
	// System.String MP_MediaItem::_Title
	String_t* ____Title_1;
	// System.String MP_MediaItem::_Artist
	String_t* ____Artist_2;
	// System.String MP_MediaItem::_AlbumTitle
	String_t* ____AlbumTitle_3;
	// System.String MP_MediaItem::_AlbumArtist
	String_t* ____AlbumArtist_4;
	// System.String MP_MediaItem::_Genre
	String_t* ____Genre_5;
	// System.String MP_MediaItem::_PlaybackDuration
	String_t* ____PlaybackDuration_6;
	// System.String MP_MediaItem::_Composer
	String_t* ____Composer_7;

public:
	inline static int32_t get_offset_of__Id_0() { return static_cast<int32_t>(offsetof(MP_MediaItem_t2497507924, ____Id_0)); }
	inline String_t* get__Id_0() const { return ____Id_0; }
	inline String_t** get_address_of__Id_0() { return &____Id_0; }
	inline void set__Id_0(String_t* value)
	{
		____Id_0 = value;
		Il2CppCodeGenWriteBarrier((&____Id_0), value);
	}

	inline static int32_t get_offset_of__Title_1() { return static_cast<int32_t>(offsetof(MP_MediaItem_t2497507924, ____Title_1)); }
	inline String_t* get__Title_1() const { return ____Title_1; }
	inline String_t** get_address_of__Title_1() { return &____Title_1; }
	inline void set__Title_1(String_t* value)
	{
		____Title_1 = value;
		Il2CppCodeGenWriteBarrier((&____Title_1), value);
	}

	inline static int32_t get_offset_of__Artist_2() { return static_cast<int32_t>(offsetof(MP_MediaItem_t2497507924, ____Artist_2)); }
	inline String_t* get__Artist_2() const { return ____Artist_2; }
	inline String_t** get_address_of__Artist_2() { return &____Artist_2; }
	inline void set__Artist_2(String_t* value)
	{
		____Artist_2 = value;
		Il2CppCodeGenWriteBarrier((&____Artist_2), value);
	}

	inline static int32_t get_offset_of__AlbumTitle_3() { return static_cast<int32_t>(offsetof(MP_MediaItem_t2497507924, ____AlbumTitle_3)); }
	inline String_t* get__AlbumTitle_3() const { return ____AlbumTitle_3; }
	inline String_t** get_address_of__AlbumTitle_3() { return &____AlbumTitle_3; }
	inline void set__AlbumTitle_3(String_t* value)
	{
		____AlbumTitle_3 = value;
		Il2CppCodeGenWriteBarrier((&____AlbumTitle_3), value);
	}

	inline static int32_t get_offset_of__AlbumArtist_4() { return static_cast<int32_t>(offsetof(MP_MediaItem_t2497507924, ____AlbumArtist_4)); }
	inline String_t* get__AlbumArtist_4() const { return ____AlbumArtist_4; }
	inline String_t** get_address_of__AlbumArtist_4() { return &____AlbumArtist_4; }
	inline void set__AlbumArtist_4(String_t* value)
	{
		____AlbumArtist_4 = value;
		Il2CppCodeGenWriteBarrier((&____AlbumArtist_4), value);
	}

	inline static int32_t get_offset_of__Genre_5() { return static_cast<int32_t>(offsetof(MP_MediaItem_t2497507924, ____Genre_5)); }
	inline String_t* get__Genre_5() const { return ____Genre_5; }
	inline String_t** get_address_of__Genre_5() { return &____Genre_5; }
	inline void set__Genre_5(String_t* value)
	{
		____Genre_5 = value;
		Il2CppCodeGenWriteBarrier((&____Genre_5), value);
	}

	inline static int32_t get_offset_of__PlaybackDuration_6() { return static_cast<int32_t>(offsetof(MP_MediaItem_t2497507924, ____PlaybackDuration_6)); }
	inline String_t* get__PlaybackDuration_6() const { return ____PlaybackDuration_6; }
	inline String_t** get_address_of__PlaybackDuration_6() { return &____PlaybackDuration_6; }
	inline void set__PlaybackDuration_6(String_t* value)
	{
		____PlaybackDuration_6 = value;
		Il2CppCodeGenWriteBarrier((&____PlaybackDuration_6), value);
	}

	inline static int32_t get_offset_of__Composer_7() { return static_cast<int32_t>(offsetof(MP_MediaItem_t2497507924, ____Composer_7)); }
	inline String_t* get__Composer_7() const { return ____Composer_7; }
	inline String_t** get_address_of__Composer_7() { return &____Composer_7; }
	inline void set__Composer_7(String_t* value)
	{
		____Composer_7 = value;
		Il2CppCodeGenWriteBarrier((&____Composer_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MP_MEDIAITEM_T2497507924_H
#ifndef RESULT_T4170401718_H
#define RESULT_T4170401718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Models.Result
struct  Result_t4170401718  : public RuntimeObject
{
public:
	// SA.Common.Models.Error SA.Common.Models.Result::_Error
	Error_t340543044 * ____Error_0;

public:
	inline static int32_t get_offset_of__Error_0() { return static_cast<int32_t>(offsetof(Result_t4170401718, ____Error_0)); }
	inline Error_t340543044 * get__Error_0() const { return ____Error_0; }
	inline Error_t340543044 ** get_address_of__Error_0() { return &____Error_0; }
	inline void set__Error_0(Error_t340543044 * value)
	{
		____Error_0 = value;
		Il2CppCodeGenWriteBarrier((&____Error_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESULT_T4170401718_H
#ifndef REPLAYKITVIDEOSHARERESULT_T673237581_H
#define REPLAYKITVIDEOSHARERESULT_T673237581_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReplayKitVideoShareResult
struct  ReplayKitVideoShareResult_t673237581  : public RuntimeObject
{
public:
	// System.String[] ReplayKitVideoShareResult::_Sources
	StringU5BU5D_t1281789340* ____Sources_0;

public:
	inline static int32_t get_offset_of__Sources_0() { return static_cast<int32_t>(offsetof(ReplayKitVideoShareResult_t673237581, ____Sources_0)); }
	inline StringU5BU5D_t1281789340* get__Sources_0() const { return ____Sources_0; }
	inline StringU5BU5D_t1281789340** get_address_of__Sources_0() { return &____Sources_0; }
	inline void set__Sources_0(StringU5BU5D_t1281789340* value)
	{
		____Sources_0 = value;
		Il2CppCodeGenWriteBarrier((&____Sources_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REPLAYKITVIDEOSHARERESULT_T673237581_H
#ifndef ICLOUDDATA_T94805902_H
#define ICLOUDDATA_T94805902_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iCloudData
struct  iCloudData_t94805902  : public RuntimeObject
{
public:
	// System.String iCloudData::m_key
	String_t* ___m_key_0;
	// System.String iCloudData::m_val
	String_t* ___m_val_1;
	// System.Boolean iCloudData::m_IsEmpty
	bool ___m_IsEmpty_2;

public:
	inline static int32_t get_offset_of_m_key_0() { return static_cast<int32_t>(offsetof(iCloudData_t94805902, ___m_key_0)); }
	inline String_t* get_m_key_0() const { return ___m_key_0; }
	inline String_t** get_address_of_m_key_0() { return &___m_key_0; }
	inline void set_m_key_0(String_t* value)
	{
		___m_key_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_key_0), value);
	}

	inline static int32_t get_offset_of_m_val_1() { return static_cast<int32_t>(offsetof(iCloudData_t94805902, ___m_val_1)); }
	inline String_t* get_m_val_1() const { return ___m_val_1; }
	inline String_t** get_address_of_m_val_1() { return &___m_val_1; }
	inline void set_m_val_1(String_t* value)
	{
		___m_val_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_val_1), value);
	}

	inline static int32_t get_offset_of_m_IsEmpty_2() { return static_cast<int32_t>(offsetof(iCloudData_t94805902, ___m_IsEmpty_2)); }
	inline bool get_m_IsEmpty_2() const { return ___m_IsEmpty_2; }
	inline bool* get_address_of_m_IsEmpty_2() { return &___m_IsEmpty_2; }
	inline void set_m_IsEmpty_2(bool value)
	{
		___m_IsEmpty_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ICLOUDDATA_T94805902_H
#ifndef BILLINGNATIVEBRIDGE_T1474950826_H
#define BILLINGNATIVEBRIDGE_T1474950826_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSNative.StoreKit.BillingNativeBridge
struct  BillingNativeBridge_t1474950826  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BILLINGNATIVEBRIDGE_T1474950826_H
#ifndef CK_RECORD_T4197524344_H
#define CK_RECORD_T4197524344_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CK_Record
struct  CK_Record_t4197524344  : public RuntimeObject
{
public:
	// CK_RecordID CK_Record::_Id
	CK_RecordID_t3242371914 * ____Id_1;
	// System.String CK_Record::_Type
	String_t* ____Type_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> CK_Record::_Data
	Dictionary_2_t1632706988 * ____Data_3;
	// System.Int32 CK_Record::_internalId
	int32_t ____internalId_4;

public:
	inline static int32_t get_offset_of__Id_1() { return static_cast<int32_t>(offsetof(CK_Record_t4197524344, ____Id_1)); }
	inline CK_RecordID_t3242371914 * get__Id_1() const { return ____Id_1; }
	inline CK_RecordID_t3242371914 ** get_address_of__Id_1() { return &____Id_1; }
	inline void set__Id_1(CK_RecordID_t3242371914 * value)
	{
		____Id_1 = value;
		Il2CppCodeGenWriteBarrier((&____Id_1), value);
	}

	inline static int32_t get_offset_of__Type_2() { return static_cast<int32_t>(offsetof(CK_Record_t4197524344, ____Type_2)); }
	inline String_t* get__Type_2() const { return ____Type_2; }
	inline String_t** get_address_of__Type_2() { return &____Type_2; }
	inline void set__Type_2(String_t* value)
	{
		____Type_2 = value;
		Il2CppCodeGenWriteBarrier((&____Type_2), value);
	}

	inline static int32_t get_offset_of__Data_3() { return static_cast<int32_t>(offsetof(CK_Record_t4197524344, ____Data_3)); }
	inline Dictionary_2_t1632706988 * get__Data_3() const { return ____Data_3; }
	inline Dictionary_2_t1632706988 ** get_address_of__Data_3() { return &____Data_3; }
	inline void set__Data_3(Dictionary_2_t1632706988 * value)
	{
		____Data_3 = value;
		Il2CppCodeGenWriteBarrier((&____Data_3), value);
	}

	inline static int32_t get_offset_of__internalId_4() { return static_cast<int32_t>(offsetof(CK_Record_t4197524344, ____internalId_4)); }
	inline int32_t get__internalId_4() const { return ____internalId_4; }
	inline int32_t* get_address_of__internalId_4() { return &____internalId_4; }
	inline void set__internalId_4(int32_t value)
	{
		____internalId_4 = value;
	}
};

struct CK_Record_t4197524344_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,CK_Record> CK_Record::_Records
	Dictionary_2_t3086237675 * ____Records_0;

public:
	inline static int32_t get_offset_of__Records_0() { return static_cast<int32_t>(offsetof(CK_Record_t4197524344_StaticFields, ____Records_0)); }
	inline Dictionary_2_t3086237675 * get__Records_0() const { return ____Records_0; }
	inline Dictionary_2_t3086237675 ** get_address_of__Records_0() { return &____Records_0; }
	inline void set__Records_0(Dictionary_2_t3086237675 * value)
	{
		____Records_0 = value;
		Il2CppCodeGenWriteBarrier((&____Records_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CK_RECORD_T4197524344_H
#ifndef CK_RECORDID_T3242371914_H
#define CK_RECORDID_T3242371914_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CK_RecordID
struct  CK_RecordID_t3242371914  : public RuntimeObject
{
public:
	// System.Int32 CK_RecordID::_internalId
	int32_t ____internalId_0;
	// System.String CK_RecordID::_Name
	String_t* ____Name_1;

public:
	inline static int32_t get_offset_of__internalId_0() { return static_cast<int32_t>(offsetof(CK_RecordID_t3242371914, ____internalId_0)); }
	inline int32_t get__internalId_0() const { return ____internalId_0; }
	inline int32_t* get_address_of__internalId_0() { return &____internalId_0; }
	inline void set__internalId_0(int32_t value)
	{
		____internalId_0 = value;
	}

	inline static int32_t get_offset_of__Name_1() { return static_cast<int32_t>(offsetof(CK_RecordID_t3242371914, ____Name_1)); }
	inline String_t* get__Name_1() const { return ____Name_1; }
	inline String_t** get_address_of__Name_1() { return &____Name_1; }
	inline void set__Name_1(String_t* value)
	{
		____Name_1 = value;
		Il2CppCodeGenWriteBarrier((&____Name_1), value);
	}
};

struct CK_RecordID_t3242371914_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,CK_RecordID> CK_RecordID::_Ids
	Dictionary_2_t2131085245 * ____Ids_2;

public:
	inline static int32_t get_offset_of__Ids_2() { return static_cast<int32_t>(offsetof(CK_RecordID_t3242371914_StaticFields, ____Ids_2)); }
	inline Dictionary_2_t2131085245 * get__Ids_2() const { return ____Ids_2; }
	inline Dictionary_2_t2131085245 ** get_address_of__Ids_2() { return &____Ids_2; }
	inline void set__Ids_2(Dictionary_2_t2131085245 * value)
	{
		____Ids_2 = value;
		Il2CppCodeGenWriteBarrier((&____Ids_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CK_RECORDID_T3242371914_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef U3CSAVESCREENSHOTU3EC__ITERATOR0_T775223138_H
#define U3CSAVESCREENSHOTU3EC__ITERATOR0_T775223138_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IOSCamera/<SaveScreenshot>c__Iterator0
struct  U3CSaveScreenshotU3Ec__Iterator0_t775223138  : public RuntimeObject
{
public:
	// System.Int32 IOSCamera/<SaveScreenshot>c__Iterator0::<width>__0
	int32_t ___U3CwidthU3E__0_0;
	// System.Int32 IOSCamera/<SaveScreenshot>c__Iterator0::<height>__0
	int32_t ___U3CheightU3E__0_1;
	// UnityEngine.Texture2D IOSCamera/<SaveScreenshot>c__Iterator0::<tex>__0
	Texture2D_t3840446185 * ___U3CtexU3E__0_2;
	// IOSCamera IOSCamera/<SaveScreenshot>c__Iterator0::$this
	IOSCamera_t2024555079 * ___U24this_3;
	// System.Object IOSCamera/<SaveScreenshot>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean IOSCamera/<SaveScreenshot>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 IOSCamera/<SaveScreenshot>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CwidthU3E__0_0() { return static_cast<int32_t>(offsetof(U3CSaveScreenshotU3Ec__Iterator0_t775223138, ___U3CwidthU3E__0_0)); }
	inline int32_t get_U3CwidthU3E__0_0() const { return ___U3CwidthU3E__0_0; }
	inline int32_t* get_address_of_U3CwidthU3E__0_0() { return &___U3CwidthU3E__0_0; }
	inline void set_U3CwidthU3E__0_0(int32_t value)
	{
		___U3CwidthU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CheightU3E__0_1() { return static_cast<int32_t>(offsetof(U3CSaveScreenshotU3Ec__Iterator0_t775223138, ___U3CheightU3E__0_1)); }
	inline int32_t get_U3CheightU3E__0_1() const { return ___U3CheightU3E__0_1; }
	inline int32_t* get_address_of_U3CheightU3E__0_1() { return &___U3CheightU3E__0_1; }
	inline void set_U3CheightU3E__0_1(int32_t value)
	{
		___U3CheightU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CtexU3E__0_2() { return static_cast<int32_t>(offsetof(U3CSaveScreenshotU3Ec__Iterator0_t775223138, ___U3CtexU3E__0_2)); }
	inline Texture2D_t3840446185 * get_U3CtexU3E__0_2() const { return ___U3CtexU3E__0_2; }
	inline Texture2D_t3840446185 ** get_address_of_U3CtexU3E__0_2() { return &___U3CtexU3E__0_2; }
	inline void set_U3CtexU3E__0_2(Texture2D_t3840446185 * value)
	{
		___U3CtexU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtexU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CSaveScreenshotU3Ec__Iterator0_t775223138, ___U24this_3)); }
	inline IOSCamera_t2024555079 * get_U24this_3() const { return ___U24this_3; }
	inline IOSCamera_t2024555079 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(IOSCamera_t2024555079 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CSaveScreenshotU3Ec__Iterator0_t775223138, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CSaveScreenshotU3Ec__Iterator0_t775223138, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CSaveScreenshotU3Ec__Iterator0_t775223138, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSAVESCREENSHOTU3EC__ITERATOR0_T775223138_H
#ifndef CK_DATABASE_T2819592330_H
#define CK_DATABASE_T2819592330_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CK_Database
struct  CK_Database_t2819592330  : public RuntimeObject
{
public:
	// System.Action`1<CK_RecordResult> CK_Database::ActionRecordSaved
	Action_1_t4181966144 * ___ActionRecordSaved_0;
	// System.Action`1<CK_RecordResult> CK_Database::ActionRecordFetchComplete
	Action_1_t4181966144 * ___ActionRecordFetchComplete_1;
	// System.Action`1<CK_RecordDeleteResult> CK_Database::ActionRecordDeleted
	Action_1_t2630400500 * ___ActionRecordDeleted_2;
	// System.Action`1<CK_QueryResult> CK_Database::ActionQueryComplete
	Action_1_t1209691443 * ___ActionQueryComplete_3;
	// System.Int32 CK_Database::_InternalId
	int32_t ____InternalId_5;

public:
	inline static int32_t get_offset_of_ActionRecordSaved_0() { return static_cast<int32_t>(offsetof(CK_Database_t2819592330, ___ActionRecordSaved_0)); }
	inline Action_1_t4181966144 * get_ActionRecordSaved_0() const { return ___ActionRecordSaved_0; }
	inline Action_1_t4181966144 ** get_address_of_ActionRecordSaved_0() { return &___ActionRecordSaved_0; }
	inline void set_ActionRecordSaved_0(Action_1_t4181966144 * value)
	{
		___ActionRecordSaved_0 = value;
		Il2CppCodeGenWriteBarrier((&___ActionRecordSaved_0), value);
	}

	inline static int32_t get_offset_of_ActionRecordFetchComplete_1() { return static_cast<int32_t>(offsetof(CK_Database_t2819592330, ___ActionRecordFetchComplete_1)); }
	inline Action_1_t4181966144 * get_ActionRecordFetchComplete_1() const { return ___ActionRecordFetchComplete_1; }
	inline Action_1_t4181966144 ** get_address_of_ActionRecordFetchComplete_1() { return &___ActionRecordFetchComplete_1; }
	inline void set_ActionRecordFetchComplete_1(Action_1_t4181966144 * value)
	{
		___ActionRecordFetchComplete_1 = value;
		Il2CppCodeGenWriteBarrier((&___ActionRecordFetchComplete_1), value);
	}

	inline static int32_t get_offset_of_ActionRecordDeleted_2() { return static_cast<int32_t>(offsetof(CK_Database_t2819592330, ___ActionRecordDeleted_2)); }
	inline Action_1_t2630400500 * get_ActionRecordDeleted_2() const { return ___ActionRecordDeleted_2; }
	inline Action_1_t2630400500 ** get_address_of_ActionRecordDeleted_2() { return &___ActionRecordDeleted_2; }
	inline void set_ActionRecordDeleted_2(Action_1_t2630400500 * value)
	{
		___ActionRecordDeleted_2 = value;
		Il2CppCodeGenWriteBarrier((&___ActionRecordDeleted_2), value);
	}

	inline static int32_t get_offset_of_ActionQueryComplete_3() { return static_cast<int32_t>(offsetof(CK_Database_t2819592330, ___ActionQueryComplete_3)); }
	inline Action_1_t1209691443 * get_ActionQueryComplete_3() const { return ___ActionQueryComplete_3; }
	inline Action_1_t1209691443 ** get_address_of_ActionQueryComplete_3() { return &___ActionQueryComplete_3; }
	inline void set_ActionQueryComplete_3(Action_1_t1209691443 * value)
	{
		___ActionQueryComplete_3 = value;
		Il2CppCodeGenWriteBarrier((&___ActionQueryComplete_3), value);
	}

	inline static int32_t get_offset_of__InternalId_5() { return static_cast<int32_t>(offsetof(CK_Database_t2819592330, ____InternalId_5)); }
	inline int32_t get__InternalId_5() const { return ____InternalId_5; }
	inline int32_t* get_address_of__InternalId_5() { return &____InternalId_5; }
	inline void set__InternalId_5(int32_t value)
	{
		____InternalId_5 = value;
	}
};

struct CK_Database_t2819592330_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,CK_Database> CK_Database::_Databases
	Dictionary_2_t1708305661 * ____Databases_4;
	// System.Action`1<CK_RecordResult> CK_Database::<>f__am$cache0
	Action_1_t4181966144 * ___U3CU3Ef__amU24cache0_6;
	// System.Action`1<CK_RecordResult> CK_Database::<>f__am$cache1
	Action_1_t4181966144 * ___U3CU3Ef__amU24cache1_7;
	// System.Action`1<CK_RecordDeleteResult> CK_Database::<>f__am$cache2
	Action_1_t2630400500 * ___U3CU3Ef__amU24cache2_8;
	// System.Action`1<CK_QueryResult> CK_Database::<>f__am$cache3
	Action_1_t1209691443 * ___U3CU3Ef__amU24cache3_9;

public:
	inline static int32_t get_offset_of__Databases_4() { return static_cast<int32_t>(offsetof(CK_Database_t2819592330_StaticFields, ____Databases_4)); }
	inline Dictionary_2_t1708305661 * get__Databases_4() const { return ____Databases_4; }
	inline Dictionary_2_t1708305661 ** get_address_of__Databases_4() { return &____Databases_4; }
	inline void set__Databases_4(Dictionary_2_t1708305661 * value)
	{
		____Databases_4 = value;
		Il2CppCodeGenWriteBarrier((&____Databases_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_6() { return static_cast<int32_t>(offsetof(CK_Database_t2819592330_StaticFields, ___U3CU3Ef__amU24cache0_6)); }
	inline Action_1_t4181966144 * get_U3CU3Ef__amU24cache0_6() const { return ___U3CU3Ef__amU24cache0_6; }
	inline Action_1_t4181966144 ** get_address_of_U3CU3Ef__amU24cache0_6() { return &___U3CU3Ef__amU24cache0_6; }
	inline void set_U3CU3Ef__amU24cache0_6(Action_1_t4181966144 * value)
	{
		___U3CU3Ef__amU24cache0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_7() { return static_cast<int32_t>(offsetof(CK_Database_t2819592330_StaticFields, ___U3CU3Ef__amU24cache1_7)); }
	inline Action_1_t4181966144 * get_U3CU3Ef__amU24cache1_7() const { return ___U3CU3Ef__amU24cache1_7; }
	inline Action_1_t4181966144 ** get_address_of_U3CU3Ef__amU24cache1_7() { return &___U3CU3Ef__amU24cache1_7; }
	inline void set_U3CU3Ef__amU24cache1_7(Action_1_t4181966144 * value)
	{
		___U3CU3Ef__amU24cache1_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_8() { return static_cast<int32_t>(offsetof(CK_Database_t2819592330_StaticFields, ___U3CU3Ef__amU24cache2_8)); }
	inline Action_1_t2630400500 * get_U3CU3Ef__amU24cache2_8() const { return ___U3CU3Ef__amU24cache2_8; }
	inline Action_1_t2630400500 ** get_address_of_U3CU3Ef__amU24cache2_8() { return &___U3CU3Ef__amU24cache2_8; }
	inline void set_U3CU3Ef__amU24cache2_8(Action_1_t2630400500 * value)
	{
		___U3CU3Ef__amU24cache2_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_8), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_9() { return static_cast<int32_t>(offsetof(CK_Database_t2819592330_StaticFields, ___U3CU3Ef__amU24cache3_9)); }
	inline Action_1_t1209691443 * get_U3CU3Ef__amU24cache3_9() const { return ___U3CU3Ef__amU24cache3_9; }
	inline Action_1_t1209691443 ** get_address_of_U3CU3Ef__amU24cache3_9() { return &___U3CU3Ef__amU24cache3_9; }
	inline void set_U3CU3Ef__amU24cache3_9(Action_1_t1209691443 * value)
	{
		___U3CU3Ef__amU24cache3_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CK_DATABASE_T2819592330_H
#ifndef CK_QUERY_T2012057349_H
#define CK_QUERY_T2012057349_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CK_Query
struct  CK_Query_t2012057349  : public RuntimeObject
{
public:
	// System.String CK_Query::_Predicate
	String_t* ____Predicate_0;
	// System.String CK_Query::_RecordType
	String_t* ____RecordType_1;

public:
	inline static int32_t get_offset_of__Predicate_0() { return static_cast<int32_t>(offsetof(CK_Query_t2012057349, ____Predicate_0)); }
	inline String_t* get__Predicate_0() const { return ____Predicate_0; }
	inline String_t** get_address_of__Predicate_0() { return &____Predicate_0; }
	inline void set__Predicate_0(String_t* value)
	{
		____Predicate_0 = value;
		Il2CppCodeGenWriteBarrier((&____Predicate_0), value);
	}

	inline static int32_t get_offset_of__RecordType_1() { return static_cast<int32_t>(offsetof(CK_Query_t2012057349, ____RecordType_1)); }
	inline String_t* get__RecordType_1() const { return ____RecordType_1; }
	inline String_t** get_address_of__RecordType_1() { return &____RecordType_1; }
	inline void set__RecordType_1(String_t* value)
	{
		____RecordType_1 = value;
		Il2CppCodeGenWriteBarrier((&____RecordType_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CK_QUERY_T2012057349_H
#ifndef ISN_NOTIFICATIONTYPE_T2610276734_H
#define ISN_NOTIFICATIONTYPE_T2610276734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ISN_NotificationType
struct  ISN_NotificationType_t2610276734  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISN_NOTIFICATIONTYPE_T2610276734_H
#ifndef IOSNATIVEPOPUPMANAGER_T203992631_H
#define IOSNATIVEPOPUPMANAGER_T203992631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IOSNativePopUpManager
struct  IOSNativePopUpManager_t203992631  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSNATIVEPOPUPMANAGER_T203992631_H
#ifndef NOTIFICATIONREQUEST_T1622111933_H
#define NOTIFICATIONREQUEST_T1622111933_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSNative.UserNotifications.NotificationRequest
struct  NotificationRequest_t1622111933  : public RuntimeObject
{
public:
	// System.String SA.IOSNative.UserNotifications.NotificationRequest::_Id
	String_t* ____Id_0;
	// SA.IOSNative.UserNotifications.NotificationContent SA.IOSNative.UserNotifications.NotificationRequest::_Content
	NotificationContent_t1589641823 * ____Content_1;
	// SA.IOSNative.UserNotifications.NotificationTrigger SA.IOSNative.UserNotifications.NotificationRequest::_Trigger
	NotificationTrigger_t2337490352 * ____Trigger_2;

public:
	inline static int32_t get_offset_of__Id_0() { return static_cast<int32_t>(offsetof(NotificationRequest_t1622111933, ____Id_0)); }
	inline String_t* get__Id_0() const { return ____Id_0; }
	inline String_t** get_address_of__Id_0() { return &____Id_0; }
	inline void set__Id_0(String_t* value)
	{
		____Id_0 = value;
		Il2CppCodeGenWriteBarrier((&____Id_0), value);
	}

	inline static int32_t get_offset_of__Content_1() { return static_cast<int32_t>(offsetof(NotificationRequest_t1622111933, ____Content_1)); }
	inline NotificationContent_t1589641823 * get__Content_1() const { return ____Content_1; }
	inline NotificationContent_t1589641823 ** get_address_of__Content_1() { return &____Content_1; }
	inline void set__Content_1(NotificationContent_t1589641823 * value)
	{
		____Content_1 = value;
		Il2CppCodeGenWriteBarrier((&____Content_1), value);
	}

	inline static int32_t get_offset_of__Trigger_2() { return static_cast<int32_t>(offsetof(NotificationRequest_t1622111933, ____Trigger_2)); }
	inline NotificationTrigger_t2337490352 * get__Trigger_2() const { return ____Trigger_2; }
	inline NotificationTrigger_t2337490352 ** get_address_of__Trigger_2() { return &____Trigger_2; }
	inline void set__Trigger_2(NotificationTrigger_t2337490352 * value)
	{
		____Trigger_2 = value;
		Il2CppCodeGenWriteBarrier((&____Trigger_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIFICATIONREQUEST_T1622111933_H
#ifndef STOREPRODUCTVIEW_T2572130870_H
#define STOREPRODUCTVIEW_T2572130870_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSNative.StoreKit.StoreProductView
struct  StoreProductView_t2572130870  : public RuntimeObject
{
public:
	// System.Action SA.IOSNative.StoreKit.StoreProductView::Loaded
	Action_t1264377477 * ___Loaded_0;
	// System.Action SA.IOSNative.StoreKit.StoreProductView::LoadFailed
	Action_t1264377477 * ___LoadFailed_1;
	// System.Action SA.IOSNative.StoreKit.StoreProductView::Appeared
	Action_t1264377477 * ___Appeared_2;
	// System.Action SA.IOSNative.StoreKit.StoreProductView::Dismissed
	Action_t1264377477 * ___Dismissed_3;
	// System.Int32 SA.IOSNative.StoreKit.StoreProductView::_id
	int32_t ____id_4;
	// System.Collections.Generic.List`1<System.String> SA.IOSNative.StoreKit.StoreProductView::_ids
	List_1_t3319525431 * ____ids_5;

public:
	inline static int32_t get_offset_of_Loaded_0() { return static_cast<int32_t>(offsetof(StoreProductView_t2572130870, ___Loaded_0)); }
	inline Action_t1264377477 * get_Loaded_0() const { return ___Loaded_0; }
	inline Action_t1264377477 ** get_address_of_Loaded_0() { return &___Loaded_0; }
	inline void set_Loaded_0(Action_t1264377477 * value)
	{
		___Loaded_0 = value;
		Il2CppCodeGenWriteBarrier((&___Loaded_0), value);
	}

	inline static int32_t get_offset_of_LoadFailed_1() { return static_cast<int32_t>(offsetof(StoreProductView_t2572130870, ___LoadFailed_1)); }
	inline Action_t1264377477 * get_LoadFailed_1() const { return ___LoadFailed_1; }
	inline Action_t1264377477 ** get_address_of_LoadFailed_1() { return &___LoadFailed_1; }
	inline void set_LoadFailed_1(Action_t1264377477 * value)
	{
		___LoadFailed_1 = value;
		Il2CppCodeGenWriteBarrier((&___LoadFailed_1), value);
	}

	inline static int32_t get_offset_of_Appeared_2() { return static_cast<int32_t>(offsetof(StoreProductView_t2572130870, ___Appeared_2)); }
	inline Action_t1264377477 * get_Appeared_2() const { return ___Appeared_2; }
	inline Action_t1264377477 ** get_address_of_Appeared_2() { return &___Appeared_2; }
	inline void set_Appeared_2(Action_t1264377477 * value)
	{
		___Appeared_2 = value;
		Il2CppCodeGenWriteBarrier((&___Appeared_2), value);
	}

	inline static int32_t get_offset_of_Dismissed_3() { return static_cast<int32_t>(offsetof(StoreProductView_t2572130870, ___Dismissed_3)); }
	inline Action_t1264377477 * get_Dismissed_3() const { return ___Dismissed_3; }
	inline Action_t1264377477 ** get_address_of_Dismissed_3() { return &___Dismissed_3; }
	inline void set_Dismissed_3(Action_t1264377477 * value)
	{
		___Dismissed_3 = value;
		Il2CppCodeGenWriteBarrier((&___Dismissed_3), value);
	}

	inline static int32_t get_offset_of__id_4() { return static_cast<int32_t>(offsetof(StoreProductView_t2572130870, ____id_4)); }
	inline int32_t get__id_4() const { return ____id_4; }
	inline int32_t* get_address_of__id_4() { return &____id_4; }
	inline void set__id_4(int32_t value)
	{
		____id_4 = value;
	}

	inline static int32_t get_offset_of__ids_5() { return static_cast<int32_t>(offsetof(StoreProductView_t2572130870, ____ids_5)); }
	inline List_1_t3319525431 * get__ids_5() const { return ____ids_5; }
	inline List_1_t3319525431 ** get_address_of__ids_5() { return &____ids_5; }
	inline void set__ids_5(List_1_t3319525431 * value)
	{
		____ids_5 = value;
		Il2CppCodeGenWriteBarrier((&____ids_5), value);
	}
};

struct StoreProductView_t2572130870_StaticFields
{
public:
	// System.Action SA.IOSNative.StoreKit.StoreProductView::<>f__am$cache0
	Action_t1264377477 * ___U3CU3Ef__amU24cache0_6;
	// System.Action SA.IOSNative.StoreKit.StoreProductView::<>f__am$cache1
	Action_t1264377477 * ___U3CU3Ef__amU24cache1_7;
	// System.Action SA.IOSNative.StoreKit.StoreProductView::<>f__am$cache2
	Action_t1264377477 * ___U3CU3Ef__amU24cache2_8;
	// System.Action SA.IOSNative.StoreKit.StoreProductView::<>f__am$cache3
	Action_t1264377477 * ___U3CU3Ef__amU24cache3_9;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_6() { return static_cast<int32_t>(offsetof(StoreProductView_t2572130870_StaticFields, ___U3CU3Ef__amU24cache0_6)); }
	inline Action_t1264377477 * get_U3CU3Ef__amU24cache0_6() const { return ___U3CU3Ef__amU24cache0_6; }
	inline Action_t1264377477 ** get_address_of_U3CU3Ef__amU24cache0_6() { return &___U3CU3Ef__amU24cache0_6; }
	inline void set_U3CU3Ef__amU24cache0_6(Action_t1264377477 * value)
	{
		___U3CU3Ef__amU24cache0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_7() { return static_cast<int32_t>(offsetof(StoreProductView_t2572130870_StaticFields, ___U3CU3Ef__amU24cache1_7)); }
	inline Action_t1264377477 * get_U3CU3Ef__amU24cache1_7() const { return ___U3CU3Ef__amU24cache1_7; }
	inline Action_t1264377477 ** get_address_of_U3CU3Ef__amU24cache1_7() { return &___U3CU3Ef__amU24cache1_7; }
	inline void set_U3CU3Ef__amU24cache1_7(Action_t1264377477 * value)
	{
		___U3CU3Ef__amU24cache1_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_8() { return static_cast<int32_t>(offsetof(StoreProductView_t2572130870_StaticFields, ___U3CU3Ef__amU24cache2_8)); }
	inline Action_t1264377477 * get_U3CU3Ef__amU24cache2_8() const { return ___U3CU3Ef__amU24cache2_8; }
	inline Action_t1264377477 ** get_address_of_U3CU3Ef__amU24cache2_8() { return &___U3CU3Ef__amU24cache2_8; }
	inline void set_U3CU3Ef__amU24cache2_8(Action_t1264377477 * value)
	{
		___U3CU3Ef__amU24cache2_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_8), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_9() { return static_cast<int32_t>(offsetof(StoreProductView_t2572130870_StaticFields, ___U3CU3Ef__amU24cache3_9)); }
	inline Action_t1264377477 * get_U3CU3Ef__amU24cache3_9() const { return ___U3CU3Ef__amU24cache3_9; }
	inline Action_t1264377477 ** get_address_of_U3CU3Ef__amU24cache3_9() { return &___U3CU3Ef__amU24cache3_9; }
	inline void set_U3CU3Ef__amU24cache3_9(Action_t1264377477 * value)
	{
		___U3CU3Ef__amU24cache3_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STOREPRODUCTVIEW_T2572130870_H
#ifndef ISN_REMOTENOTIFICATION_T3991987818_H
#define ISN_REMOTENOTIFICATION_T3991987818_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ISN_RemoteNotification
struct  ISN_RemoteNotification_t3991987818  : public RuntimeObject
{
public:
	// System.String ISN_RemoteNotification::_Body
	String_t* ____Body_0;

public:
	inline static int32_t get_offset_of__Body_0() { return static_cast<int32_t>(offsetof(ISN_RemoteNotification_t3991987818, ____Body_0)); }
	inline String_t* get__Body_0() const { return ____Body_0; }
	inline String_t** get_address_of__Body_0() { return &____Body_0; }
	inline void set__Body_0(String_t* value)
	{
		____Body_0 = value;
		Il2CppCodeGenWriteBarrier((&____Body_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISN_REMOTENOTIFICATION_T3991987818_H
#ifndef GK_SCORECOLLECTION_T3739344104_H
#define GK_SCORECOLLECTION_T3739344104_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GK_ScoreCollection
struct  GK_ScoreCollection_t3739344104  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,GK_Score> GK_ScoreCollection::AllTimeScores
	Dictionary_2_t3399002878 * ___AllTimeScores_0;
	// System.Collections.Generic.Dictionary`2<System.Int32,GK_Score> GK_ScoreCollection::WeekScores
	Dictionary_2_t3399002878 * ___WeekScores_1;
	// System.Collections.Generic.Dictionary`2<System.Int32,GK_Score> GK_ScoreCollection::TodayScores
	Dictionary_2_t3399002878 * ___TodayScores_2;

public:
	inline static int32_t get_offset_of_AllTimeScores_0() { return static_cast<int32_t>(offsetof(GK_ScoreCollection_t3739344104, ___AllTimeScores_0)); }
	inline Dictionary_2_t3399002878 * get_AllTimeScores_0() const { return ___AllTimeScores_0; }
	inline Dictionary_2_t3399002878 ** get_address_of_AllTimeScores_0() { return &___AllTimeScores_0; }
	inline void set_AllTimeScores_0(Dictionary_2_t3399002878 * value)
	{
		___AllTimeScores_0 = value;
		Il2CppCodeGenWriteBarrier((&___AllTimeScores_0), value);
	}

	inline static int32_t get_offset_of_WeekScores_1() { return static_cast<int32_t>(offsetof(GK_ScoreCollection_t3739344104, ___WeekScores_1)); }
	inline Dictionary_2_t3399002878 * get_WeekScores_1() const { return ___WeekScores_1; }
	inline Dictionary_2_t3399002878 ** get_address_of_WeekScores_1() { return &___WeekScores_1; }
	inline void set_WeekScores_1(Dictionary_2_t3399002878 * value)
	{
		___WeekScores_1 = value;
		Il2CppCodeGenWriteBarrier((&___WeekScores_1), value);
	}

	inline static int32_t get_offset_of_TodayScores_2() { return static_cast<int32_t>(offsetof(GK_ScoreCollection_t3739344104, ___TodayScores_2)); }
	inline Dictionary_2_t3399002878 * get_TodayScores_2() const { return ___TodayScores_2; }
	inline Dictionary_2_t3399002878 ** get_address_of_TodayScores_2() { return &___TodayScores_2; }
	inline void set_TodayScores_2(Dictionary_2_t3399002878 * value)
	{
		___TodayScores_2 = value;
		Il2CppCodeGenWriteBarrier((&___TodayScores_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GK_SCORECOLLECTION_T3739344104_H
#ifndef NOTIFICATIONCENTER_T1582563873_H
#define NOTIFICATIONCENTER_T1582563873_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSNative.UserNotifications.NotificationCenter
struct  NotificationCenter_t1582563873  : public RuntimeObject
{
public:

public:
};

struct NotificationCenter_t1582563873_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Action`1<SA.Common.Models.Result>> SA.IOSNative.UserNotifications.NotificationCenter::OnCallbackDictionary
	Dictionary_2_t4128125612 * ___OnCallbackDictionary_0;
	// System.Action`1<System.Collections.Generic.List`1<SA.IOSNative.UserNotifications.NotificationRequest>> SA.IOSNative.UserNotifications.NotificationCenter::OnPendingNotificationsCallback
	Action_1_t3266654270 * ___OnPendingNotificationsCallback_1;
	// System.Action`1<SA.Common.Models.Result> SA.IOSNative.UserNotifications.NotificationCenter::RequestPermissionsCallback
	Action_1_t47902017 * ___RequestPermissionsCallback_2;
	// System.Action`1<SA.IOSNative.UserNotifications.NotificationRequest> SA.IOSNative.UserNotifications.NotificationCenter::OnWillPresentNotification
	Action_1_t1794579528 * ___OnWillPresentNotification_3;
	// SA.IOSNative.UserNotifications.NotificationRequest SA.IOSNative.UserNotifications.NotificationCenter::LastNotificationRequest
	NotificationRequest_t1622111933 * ___LastNotificationRequest_4;

public:
	inline static int32_t get_offset_of_OnCallbackDictionary_0() { return static_cast<int32_t>(offsetof(NotificationCenter_t1582563873_StaticFields, ___OnCallbackDictionary_0)); }
	inline Dictionary_2_t4128125612 * get_OnCallbackDictionary_0() const { return ___OnCallbackDictionary_0; }
	inline Dictionary_2_t4128125612 ** get_address_of_OnCallbackDictionary_0() { return &___OnCallbackDictionary_0; }
	inline void set_OnCallbackDictionary_0(Dictionary_2_t4128125612 * value)
	{
		___OnCallbackDictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___OnCallbackDictionary_0), value);
	}

	inline static int32_t get_offset_of_OnPendingNotificationsCallback_1() { return static_cast<int32_t>(offsetof(NotificationCenter_t1582563873_StaticFields, ___OnPendingNotificationsCallback_1)); }
	inline Action_1_t3266654270 * get_OnPendingNotificationsCallback_1() const { return ___OnPendingNotificationsCallback_1; }
	inline Action_1_t3266654270 ** get_address_of_OnPendingNotificationsCallback_1() { return &___OnPendingNotificationsCallback_1; }
	inline void set_OnPendingNotificationsCallback_1(Action_1_t3266654270 * value)
	{
		___OnPendingNotificationsCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&___OnPendingNotificationsCallback_1), value);
	}

	inline static int32_t get_offset_of_RequestPermissionsCallback_2() { return static_cast<int32_t>(offsetof(NotificationCenter_t1582563873_StaticFields, ___RequestPermissionsCallback_2)); }
	inline Action_1_t47902017 * get_RequestPermissionsCallback_2() const { return ___RequestPermissionsCallback_2; }
	inline Action_1_t47902017 ** get_address_of_RequestPermissionsCallback_2() { return &___RequestPermissionsCallback_2; }
	inline void set_RequestPermissionsCallback_2(Action_1_t47902017 * value)
	{
		___RequestPermissionsCallback_2 = value;
		Il2CppCodeGenWriteBarrier((&___RequestPermissionsCallback_2), value);
	}

	inline static int32_t get_offset_of_OnWillPresentNotification_3() { return static_cast<int32_t>(offsetof(NotificationCenter_t1582563873_StaticFields, ___OnWillPresentNotification_3)); }
	inline Action_1_t1794579528 * get_OnWillPresentNotification_3() const { return ___OnWillPresentNotification_3; }
	inline Action_1_t1794579528 ** get_address_of_OnWillPresentNotification_3() { return &___OnWillPresentNotification_3; }
	inline void set_OnWillPresentNotification_3(Action_1_t1794579528 * value)
	{
		___OnWillPresentNotification_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnWillPresentNotification_3), value);
	}

	inline static int32_t get_offset_of_LastNotificationRequest_4() { return static_cast<int32_t>(offsetof(NotificationCenter_t1582563873_StaticFields, ___LastNotificationRequest_4)); }
	inline NotificationRequest_t1622111933 * get_LastNotificationRequest_4() const { return ___LastNotificationRequest_4; }
	inline NotificationRequest_t1622111933 ** get_address_of_LastNotificationRequest_4() { return &___LastNotificationRequest_4; }
	inline void set_LastNotificationRequest_4(NotificationRequest_t1622111933 * value)
	{
		___LastNotificationRequest_4 = value;
		Il2CppCodeGenWriteBarrier((&___LastNotificationRequest_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIFICATIONCENTER_T1582563873_H
#ifndef NOTIFICATIONCONTENT_T1589641823_H
#define NOTIFICATIONCONTENT_T1589641823_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSNative.UserNotifications.NotificationContent
struct  NotificationContent_t1589641823  : public RuntimeObject
{
public:
	// System.String SA.IOSNative.UserNotifications.NotificationContent::Title
	String_t* ___Title_0;
	// System.String SA.IOSNative.UserNotifications.NotificationContent::Subtitle
	String_t* ___Subtitle_1;
	// System.String SA.IOSNative.UserNotifications.NotificationContent::Body
	String_t* ___Body_2;
	// System.String SA.IOSNative.UserNotifications.NotificationContent::Sound
	String_t* ___Sound_3;
	// System.Int32 SA.IOSNative.UserNotifications.NotificationContent::Badge
	int32_t ___Badge_4;
	// System.String SA.IOSNative.UserNotifications.NotificationContent::LaunchImageName
	String_t* ___LaunchImageName_5;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> SA.IOSNative.UserNotifications.NotificationContent::UserInfo
	Dictionary_2_t2865362463 * ___UserInfo_6;

public:
	inline static int32_t get_offset_of_Title_0() { return static_cast<int32_t>(offsetof(NotificationContent_t1589641823, ___Title_0)); }
	inline String_t* get_Title_0() const { return ___Title_0; }
	inline String_t** get_address_of_Title_0() { return &___Title_0; }
	inline void set_Title_0(String_t* value)
	{
		___Title_0 = value;
		Il2CppCodeGenWriteBarrier((&___Title_0), value);
	}

	inline static int32_t get_offset_of_Subtitle_1() { return static_cast<int32_t>(offsetof(NotificationContent_t1589641823, ___Subtitle_1)); }
	inline String_t* get_Subtitle_1() const { return ___Subtitle_1; }
	inline String_t** get_address_of_Subtitle_1() { return &___Subtitle_1; }
	inline void set_Subtitle_1(String_t* value)
	{
		___Subtitle_1 = value;
		Il2CppCodeGenWriteBarrier((&___Subtitle_1), value);
	}

	inline static int32_t get_offset_of_Body_2() { return static_cast<int32_t>(offsetof(NotificationContent_t1589641823, ___Body_2)); }
	inline String_t* get_Body_2() const { return ___Body_2; }
	inline String_t** get_address_of_Body_2() { return &___Body_2; }
	inline void set_Body_2(String_t* value)
	{
		___Body_2 = value;
		Il2CppCodeGenWriteBarrier((&___Body_2), value);
	}

	inline static int32_t get_offset_of_Sound_3() { return static_cast<int32_t>(offsetof(NotificationContent_t1589641823, ___Sound_3)); }
	inline String_t* get_Sound_3() const { return ___Sound_3; }
	inline String_t** get_address_of_Sound_3() { return &___Sound_3; }
	inline void set_Sound_3(String_t* value)
	{
		___Sound_3 = value;
		Il2CppCodeGenWriteBarrier((&___Sound_3), value);
	}

	inline static int32_t get_offset_of_Badge_4() { return static_cast<int32_t>(offsetof(NotificationContent_t1589641823, ___Badge_4)); }
	inline int32_t get_Badge_4() const { return ___Badge_4; }
	inline int32_t* get_address_of_Badge_4() { return &___Badge_4; }
	inline void set_Badge_4(int32_t value)
	{
		___Badge_4 = value;
	}

	inline static int32_t get_offset_of_LaunchImageName_5() { return static_cast<int32_t>(offsetof(NotificationContent_t1589641823, ___LaunchImageName_5)); }
	inline String_t* get_LaunchImageName_5() const { return ___LaunchImageName_5; }
	inline String_t** get_address_of_LaunchImageName_5() { return &___LaunchImageName_5; }
	inline void set_LaunchImageName_5(String_t* value)
	{
		___LaunchImageName_5 = value;
		Il2CppCodeGenWriteBarrier((&___LaunchImageName_5), value);
	}

	inline static int32_t get_offset_of_UserInfo_6() { return static_cast<int32_t>(offsetof(NotificationContent_t1589641823, ___UserInfo_6)); }
	inline Dictionary_2_t2865362463 * get_UserInfo_6() const { return ___UserInfo_6; }
	inline Dictionary_2_t2865362463 ** get_address_of_UserInfo_6() { return &___UserInfo_6; }
	inline void set_UserInfo_6(Dictionary_2_t2865362463 * value)
	{
		___UserInfo_6 = value;
		Il2CppCodeGenWriteBarrier((&___UserInfo_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIFICATIONCONTENT_T1589641823_H
#ifndef SK_STOREREVIEWCONTROLLER_T56888101_H
#define SK_STOREREVIEWCONTROLLER_T56888101_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSNative.StoreKit.SK_StoreReviewController
struct  SK_StoreReviewController_t56888101  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SK_STOREREVIEWCONTROLLER_T56888101_H
#ifndef GK_RTM_MATCH_T949836071_H
#define GK_RTM_MATCH_T949836071_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GK_RTM_Match
struct  GK_RTM_Match_t949836071  : public RuntimeObject
{
public:
	// System.Int32 GK_RTM_Match::_ExpectedPlayerCount
	int32_t ____ExpectedPlayerCount_0;
	// System.Collections.Generic.List`1<GK_Player> GK_RTM_Match::_Players
	List_1_t638460237 * ____Players_1;

public:
	inline static int32_t get_offset_of__ExpectedPlayerCount_0() { return static_cast<int32_t>(offsetof(GK_RTM_Match_t949836071, ____ExpectedPlayerCount_0)); }
	inline int32_t get__ExpectedPlayerCount_0() const { return ____ExpectedPlayerCount_0; }
	inline int32_t* get_address_of__ExpectedPlayerCount_0() { return &____ExpectedPlayerCount_0; }
	inline void set__ExpectedPlayerCount_0(int32_t value)
	{
		____ExpectedPlayerCount_0 = value;
	}

	inline static int32_t get_offset_of__Players_1() { return static_cast<int32_t>(offsetof(GK_RTM_Match_t949836071, ____Players_1)); }
	inline List_1_t638460237 * get__Players_1() const { return ____Players_1; }
	inline List_1_t638460237 ** get_address_of__Players_1() { return &____Players_1; }
	inline void set__Players_1(List_1_t638460237 * value)
	{
		____Players_1 = value;
		Il2CppCodeGenWriteBarrier((&____Players_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GK_RTM_MATCH_T949836071_H
#ifndef ISN_DEVICETOKEN_T822270907_H
#define ISN_DEVICETOKEN_T822270907_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ISN_DeviceToken
struct  ISN_DeviceToken_t822270907  : public RuntimeObject
{
public:
	// System.String ISN_DeviceToken::_tokenString
	String_t* ____tokenString_0;
	// System.Byte[] ISN_DeviceToken::_tokenBytes
	ByteU5BU5D_t4116647657* ____tokenBytes_1;

public:
	inline static int32_t get_offset_of__tokenString_0() { return static_cast<int32_t>(offsetof(ISN_DeviceToken_t822270907, ____tokenString_0)); }
	inline String_t* get__tokenString_0() const { return ____tokenString_0; }
	inline String_t** get_address_of__tokenString_0() { return &____tokenString_0; }
	inline void set__tokenString_0(String_t* value)
	{
		____tokenString_0 = value;
		Il2CppCodeGenWriteBarrier((&____tokenString_0), value);
	}

	inline static int32_t get_offset_of__tokenBytes_1() { return static_cast<int32_t>(offsetof(ISN_DeviceToken_t822270907, ____tokenBytes_1)); }
	inline ByteU5BU5D_t4116647657* get__tokenBytes_1() const { return ____tokenBytes_1; }
	inline ByteU5BU5D_t4116647657** get_address_of__tokenBytes_1() { return &____tokenBytes_1; }
	inline void set__tokenBytes_1(ByteU5BU5D_t4116647657* value)
	{
		____tokenBytes_1 = value;
		Il2CppCodeGenWriteBarrier((&____tokenBytes_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISN_DEVICETOKEN_T822270907_H
#ifndef SK_UTIL_T1571557834_H
#define SK_UTIL_T1571557834_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSNative.StoreKit.SK_Util
struct  SK_Util_t1571557834  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SK_UTIL_T1571557834_H
#ifndef NOTIFICATIONTRIGGER_T2337490352_H
#define NOTIFICATIONTRIGGER_T2337490352_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSNative.UserNotifications.NotificationTrigger
struct  NotificationTrigger_t2337490352  : public RuntimeObject
{
public:
	// System.Boolean SA.IOSNative.UserNotifications.NotificationTrigger::repeated
	bool ___repeated_0;

public:
	inline static int32_t get_offset_of_repeated_0() { return static_cast<int32_t>(offsetof(NotificationTrigger_t2337490352, ___repeated_0)); }
	inline bool get_repeated_0() const { return ___repeated_0; }
	inline bool* get_address_of_repeated_0() { return &___repeated_0; }
	inline void set_repeated_0(bool value)
	{
		___repeated_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIFICATIONTRIGGER_T2337490352_H
#ifndef MP_MEDIAPICKERRESULT_T632246977_H
#define MP_MEDIAPICKERRESULT_T632246977_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MP_MediaPickerResult
struct  MP_MediaPickerResult_t632246977  : public Result_t4170401718
{
public:
	// System.Collections.Generic.List`1<MP_MediaItem> MP_MediaPickerResult::_SelectedmediaItems
	List_1_t3969582666 * ____SelectedmediaItems_1;

public:
	inline static int32_t get_offset_of__SelectedmediaItems_1() { return static_cast<int32_t>(offsetof(MP_MediaPickerResult_t632246977, ____SelectedmediaItems_1)); }
	inline List_1_t3969582666 * get__SelectedmediaItems_1() const { return ____SelectedmediaItems_1; }
	inline List_1_t3969582666 ** get_address_of__SelectedmediaItems_1() { return &____SelectedmediaItems_1; }
	inline void set__SelectedmediaItems_1(List_1_t3969582666 * value)
	{
		____SelectedmediaItems_1 = value;
		Il2CppCodeGenWriteBarrier((&____SelectedmediaItems_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MP_MEDIAPICKERRESULT_T632246977_H
#ifndef NULLABLE_1_T378540539_H
#define NULLABLE_1_T378540539_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Int32>
struct  Nullable_1_t378540539 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t378540539, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t378540539, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T378540539_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef CALENDARTRIGGER_T159864792_H
#define CALENDARTRIGGER_T159864792_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSNative.UserNotifications.CalendarTrigger
struct  CalendarTrigger_t159864792  : public NotificationTrigger_t2337490352
{
public:
	// SA.IOSNative.UserNotifications.DateComponents SA.IOSNative.UserNotifications.CalendarTrigger::ComponentsOfDateToFire
	DateComponents_t2005439303 * ___ComponentsOfDateToFire_1;

public:
	inline static int32_t get_offset_of_ComponentsOfDateToFire_1() { return static_cast<int32_t>(offsetof(CalendarTrigger_t159864792, ___ComponentsOfDateToFire_1)); }
	inline DateComponents_t2005439303 * get_ComponentsOfDateToFire_1() const { return ___ComponentsOfDateToFire_1; }
	inline DateComponents_t2005439303 ** get_address_of_ComponentsOfDateToFire_1() { return &___ComponentsOfDateToFire_1; }
	inline void set_ComponentsOfDateToFire_1(DateComponents_t2005439303 * value)
	{
		___ComponentsOfDateToFire_1 = value;
		Il2CppCodeGenWriteBarrier((&___ComponentsOfDateToFire_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALENDARTRIGGER_T159864792_H
#ifndef TIMEINTERVALTRIGGER_T2848746056_H
#define TIMEINTERVALTRIGGER_T2848746056_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSNative.UserNotifications.TimeIntervalTrigger
struct  TimeIntervalTrigger_t2848746056  : public NotificationTrigger_t2337490352
{
public:
	// System.Int32 SA.IOSNative.UserNotifications.TimeIntervalTrigger::intervalToFire
	int32_t ___intervalToFire_1;

public:
	inline static int32_t get_offset_of_intervalToFire_1() { return static_cast<int32_t>(offsetof(TimeIntervalTrigger_t2848746056, ___intervalToFire_1)); }
	inline int32_t get_intervalToFire_1() const { return ___intervalToFire_1; }
	inline int32_t* get_address_of_intervalToFire_1() { return &___intervalToFire_1; }
	inline void set_intervalToFire_1(int32_t value)
	{
		___intervalToFire_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEINTERVALTRIGGER_T2848746056_H
#ifndef SK_REQUESTSTOREFRONTIDENTIFIERRESULT_T833766923_H
#define SK_REQUESTSTOREFRONTIDENTIFIERRESULT_T833766923_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SK_RequestStorefrontIdentifierResult
struct  SK_RequestStorefrontIdentifierResult_t833766923  : public Result_t4170401718
{
public:
	// System.String SK_RequestStorefrontIdentifierResult::_StorefrontIdentifier
	String_t* ____StorefrontIdentifier_1;

public:
	inline static int32_t get_offset_of__StorefrontIdentifier_1() { return static_cast<int32_t>(offsetof(SK_RequestStorefrontIdentifierResult_t833766923, ____StorefrontIdentifier_1)); }
	inline String_t* get__StorefrontIdentifier_1() const { return ____StorefrontIdentifier_1; }
	inline String_t** get_address_of__StorefrontIdentifier_1() { return &____StorefrontIdentifier_1; }
	inline void set__StorefrontIdentifier_1(String_t* value)
	{
		____StorefrontIdentifier_1 = value;
		Il2CppCodeGenWriteBarrier((&____StorefrontIdentifier_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SK_REQUESTSTOREFRONTIDENTIFIERRESULT_T833766923_H
#ifndef ISN_REMOTENOTIFICATIONSREGISTRATIONRESULT_T930960218_H
#define ISN_REMOTENOTIFICATIONSREGISTRATIONRESULT_T930960218_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ISN_RemoteNotificationsRegistrationResult
struct  ISN_RemoteNotificationsRegistrationResult_t930960218  : public Result_t4170401718
{
public:
	// ISN_DeviceToken ISN_RemoteNotificationsRegistrationResult::_Token
	ISN_DeviceToken_t822270907 * ____Token_1;

public:
	inline static int32_t get_offset_of__Token_1() { return static_cast<int32_t>(offsetof(ISN_RemoteNotificationsRegistrationResult_t930960218, ____Token_1)); }
	inline ISN_DeviceToken_t822270907 * get__Token_1() const { return ____Token_1; }
	inline ISN_DeviceToken_t822270907 ** get_address_of__Token_1() { return &____Token_1; }
	inline void set__Token_1(ISN_DeviceToken_t822270907 * value)
	{
		____Token_1 = value;
		Il2CppCodeGenWriteBarrier((&____Token_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISN_REMOTENOTIFICATIONSREGISTRATIONRESULT_T930960218_H
#ifndef IOSIMAGEPICKRESULT_T4120145150_H
#define IOSIMAGEPICKRESULT_T4120145150_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IOSImagePickResult
struct  IOSImagePickResult_t4120145150  : public Result_t4170401718
{
public:
	// UnityEngine.Texture2D IOSImagePickResult::_image
	Texture2D_t3840446185 * ____image_1;

public:
	inline static int32_t get_offset_of__image_1() { return static_cast<int32_t>(offsetof(IOSImagePickResult_t4120145150, ____image_1)); }
	inline Texture2D_t3840446185 * get__image_1() const { return ____image_1; }
	inline Texture2D_t3840446185 ** get_address_of__image_1() { return &____image_1; }
	inline void set__image_1(Texture2D_t3840446185 * value)
	{
		____image_1 = value;
		Il2CppCodeGenWriteBarrier((&____image_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSIMAGEPICKRESULT_T4120145150_H
#ifndef CK_RECORDRESULT_T4009498549_H
#define CK_RECORDRESULT_T4009498549_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CK_RecordResult
struct  CK_RecordResult_t4009498549  : public Result_t4170401718
{
public:
	// CK_Record CK_RecordResult::_Record
	CK_Record_t4197524344 * ____Record_1;
	// CK_Database CK_RecordResult::_Database
	CK_Database_t2819592330 * ____Database_2;

public:
	inline static int32_t get_offset_of__Record_1() { return static_cast<int32_t>(offsetof(CK_RecordResult_t4009498549, ____Record_1)); }
	inline CK_Record_t4197524344 * get__Record_1() const { return ____Record_1; }
	inline CK_Record_t4197524344 ** get_address_of__Record_1() { return &____Record_1; }
	inline void set__Record_1(CK_Record_t4197524344 * value)
	{
		____Record_1 = value;
		Il2CppCodeGenWriteBarrier((&____Record_1), value);
	}

	inline static int32_t get_offset_of__Database_2() { return static_cast<int32_t>(offsetof(CK_RecordResult_t4009498549, ____Database_2)); }
	inline CK_Database_t2819592330 * get__Database_2() const { return ____Database_2; }
	inline CK_Database_t2819592330 ** get_address_of__Database_2() { return &____Database_2; }
	inline void set__Database_2(CK_Database_t2819592330 * value)
	{
		____Database_2 = value;
		Il2CppCodeGenWriteBarrier((&____Database_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CK_RECORDRESULT_T4009498549_H
#ifndef GK_TBM_ENDTRUNRESULT_T3483094632_H
#define GK_TBM_ENDTRUNRESULT_T3483094632_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GK_TBM_EndTrunResult
struct  GK_TBM_EndTrunResult_t3483094632  : public Result_t4170401718
{
public:
	// GK_TBM_Match GK_TBM_EndTrunResult::_Match
	GK_TBM_Match_t83581881 * ____Match_1;

public:
	inline static int32_t get_offset_of__Match_1() { return static_cast<int32_t>(offsetof(GK_TBM_EndTrunResult_t3483094632, ____Match_1)); }
	inline GK_TBM_Match_t83581881 * get__Match_1() const { return ____Match_1; }
	inline GK_TBM_Match_t83581881 ** get_address_of__Match_1() { return &____Match_1; }
	inline void set__Match_1(GK_TBM_Match_t83581881 * value)
	{
		____Match_1 = value;
		Il2CppCodeGenWriteBarrier((&____Match_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GK_TBM_ENDTRUNRESULT_T3483094632_H
#ifndef GK_RTM_QUERYACTIVITYRESULT_T3645624704_H
#define GK_RTM_QUERYACTIVITYRESULT_T3645624704_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GK_RTM_QueryActivityResult
struct  GK_RTM_QueryActivityResult_t3645624704  : public Result_t4170401718
{
public:
	// System.Int32 GK_RTM_QueryActivityResult::_Activity
	int32_t ____Activity_1;

public:
	inline static int32_t get_offset_of__Activity_1() { return static_cast<int32_t>(offsetof(GK_RTM_QueryActivityResult_t3645624704, ____Activity_1)); }
	inline int32_t get__Activity_1() const { return ____Activity_1; }
	inline int32_t* get_address_of__Activity_1() { return &____Activity_1; }
	inline void set__Activity_1(int32_t value)
	{
		____Activity_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GK_RTM_QUERYACTIVITYRESULT_T3645624704_H
#ifndef GK_RTM_MATCHSTARTEDRESULT_T3577638810_H
#define GK_RTM_MATCHSTARTEDRESULT_T3577638810_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GK_RTM_MatchStartedResult
struct  GK_RTM_MatchStartedResult_t3577638810  : public Result_t4170401718
{
public:
	// GK_RTM_Match GK_RTM_MatchStartedResult::_Match
	GK_RTM_Match_t949836071 * ____Match_1;

public:
	inline static int32_t get_offset_of__Match_1() { return static_cast<int32_t>(offsetof(GK_RTM_MatchStartedResult_t3577638810, ____Match_1)); }
	inline GK_RTM_Match_t949836071 * get__Match_1() const { return ____Match_1; }
	inline GK_RTM_Match_t949836071 ** get_address_of__Match_1() { return &____Match_1; }
	inline void set__Match_1(GK_RTM_Match_t949836071 * value)
	{
		____Match_1 = value;
		Il2CppCodeGenWriteBarrier((&____Match_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GK_RTM_MATCHSTARTEDRESULT_T3577638810_H
#ifndef GK_TBM_MATCHDATAUPDATERESULT_T51603500_H
#define GK_TBM_MATCHDATAUPDATERESULT_T51603500_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GK_TBM_MatchDataUpdateResult
struct  GK_TBM_MatchDataUpdateResult_t51603500  : public Result_t4170401718
{
public:
	// GK_TBM_Match GK_TBM_MatchDataUpdateResult::_Match
	GK_TBM_Match_t83581881 * ____Match_1;

public:
	inline static int32_t get_offset_of__Match_1() { return static_cast<int32_t>(offsetof(GK_TBM_MatchDataUpdateResult_t51603500, ____Match_1)); }
	inline GK_TBM_Match_t83581881 * get__Match_1() const { return ____Match_1; }
	inline GK_TBM_Match_t83581881 ** get_address_of__Match_1() { return &____Match_1; }
	inline void set__Match_1(GK_TBM_Match_t83581881 * value)
	{
		____Match_1 = value;
		Il2CppCodeGenWriteBarrier((&____Match_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GK_TBM_MATCHDATAUPDATERESULT_T51603500_H
#ifndef GK_TBM_LOADMATCHRESULT_T1229115337_H
#define GK_TBM_LOADMATCHRESULT_T1229115337_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GK_TBM_LoadMatchResult
struct  GK_TBM_LoadMatchResult_t1229115337  : public Result_t4170401718
{
public:
	// GK_TBM_Match GK_TBM_LoadMatchResult::_Match
	GK_TBM_Match_t83581881 * ____Match_1;

public:
	inline static int32_t get_offset_of__Match_1() { return static_cast<int32_t>(offsetof(GK_TBM_LoadMatchResult_t1229115337, ____Match_1)); }
	inline GK_TBM_Match_t83581881 * get__Match_1() const { return ____Match_1; }
	inline GK_TBM_Match_t83581881 ** get_address_of__Match_1() { return &____Match_1; }
	inline void set__Match_1(GK_TBM_Match_t83581881 * value)
	{
		____Match_1 = value;
		Il2CppCodeGenWriteBarrier((&____Match_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GK_TBM_LOADMATCHRESULT_T1229115337_H
#ifndef GK_TBM_LOADMATCHESRESULT_T4223942821_H
#define GK_TBM_LOADMATCHESRESULT_T4223942821_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GK_TBM_LoadMatchesResult
struct  GK_TBM_LoadMatchesResult_t4223942821  : public Result_t4170401718
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,GK_TBM_Match> GK_TBM_LoadMatchesResult::LoadedMatches
	Dictionary_2_t4163805476 * ___LoadedMatches_1;

public:
	inline static int32_t get_offset_of_LoadedMatches_1() { return static_cast<int32_t>(offsetof(GK_TBM_LoadMatchesResult_t4223942821, ___LoadedMatches_1)); }
	inline Dictionary_2_t4163805476 * get_LoadedMatches_1() const { return ___LoadedMatches_1; }
	inline Dictionary_2_t4163805476 ** get_address_of_LoadedMatches_1() { return &___LoadedMatches_1; }
	inline void set_LoadedMatches_1(Dictionary_2_t4163805476 * value)
	{
		___LoadedMatches_1 = value;
		Il2CppCodeGenWriteBarrier((&___LoadedMatches_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GK_TBM_LOADMATCHESRESULT_T4223942821_H
#ifndef GK_SAVEDATALOADED_T72329421_H
#define GK_SAVEDATALOADED_T72329421_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GK_SaveDataLoaded
struct  GK_SaveDataLoaded_t72329421  : public Result_t4170401718
{
public:
	// GK_SavedGame GK_SaveDataLoaded::_SavedGame
	GK_SavedGame_t3445071760 * ____SavedGame_1;

public:
	inline static int32_t get_offset_of__SavedGame_1() { return static_cast<int32_t>(offsetof(GK_SaveDataLoaded_t72329421, ____SavedGame_1)); }
	inline GK_SavedGame_t3445071760 * get__SavedGame_1() const { return ____SavedGame_1; }
	inline GK_SavedGame_t3445071760 ** get_address_of__SavedGame_1() { return &____SavedGame_1; }
	inline void set__SavedGame_1(GK_SavedGame_t3445071760 * value)
	{
		____SavedGame_1 = value;
		Il2CppCodeGenWriteBarrier((&____SavedGame_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GK_SAVEDATALOADED_T72329421_H
#ifndef GK_FETCHRESULT_T82030703_H
#define GK_FETCHRESULT_T82030703_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GK_FetchResult
struct  GK_FetchResult_t82030703  : public Result_t4170401718
{
public:
	// System.Collections.Generic.List`1<GK_SavedGame> GK_FetchResult::_SavedGames
	List_1_t622179206 * ____SavedGames_1;

public:
	inline static int32_t get_offset_of__SavedGames_1() { return static_cast<int32_t>(offsetof(GK_FetchResult_t82030703, ____SavedGames_1)); }
	inline List_1_t622179206 * get__SavedGames_1() const { return ____SavedGames_1; }
	inline List_1_t622179206 ** get_address_of__SavedGames_1() { return &____SavedGames_1; }
	inline void set__SavedGames_1(List_1_t622179206 * value)
	{
		____SavedGames_1 = value;
		Il2CppCodeGenWriteBarrier((&____SavedGames_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GK_FETCHRESULT_T82030703_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef GK_SAVESRESOLVERESULT_T3961500742_H
#define GK_SAVESRESOLVERESULT_T3961500742_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GK_SavesResolveResult
struct  GK_SavesResolveResult_t3961500742  : public Result_t4170401718
{
public:
	// System.Collections.Generic.List`1<GK_SavedGame> GK_SavesResolveResult::_ResolvedSaves
	List_1_t622179206 * ____ResolvedSaves_1;

public:
	inline static int32_t get_offset_of__ResolvedSaves_1() { return static_cast<int32_t>(offsetof(GK_SavesResolveResult_t3961500742, ____ResolvedSaves_1)); }
	inline List_1_t622179206 * get__ResolvedSaves_1() const { return ____ResolvedSaves_1; }
	inline List_1_t622179206 ** get_address_of__ResolvedSaves_1() { return &____ResolvedSaves_1; }
	inline void set__ResolvedSaves_1(List_1_t622179206 * value)
	{
		____ResolvedSaves_1 = value;
		Il2CppCodeGenWriteBarrier((&____ResolvedSaves_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GK_SAVESRESOLVERESULT_T3961500742_H
#ifndef GK_SAVERESULT_T325278800_H
#define GK_SAVERESULT_T325278800_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GK_SaveResult
struct  GK_SaveResult_t325278800  : public Result_t4170401718
{
public:
	// GK_SavedGame GK_SaveResult::_SavedGame
	GK_SavedGame_t3445071760 * ____SavedGame_1;

public:
	inline static int32_t get_offset_of__SavedGame_1() { return static_cast<int32_t>(offsetof(GK_SaveResult_t325278800, ____SavedGame_1)); }
	inline GK_SavedGame_t3445071760 * get__SavedGame_1() const { return ____SavedGame_1; }
	inline GK_SavedGame_t3445071760 ** get_address_of__SavedGame_1() { return &____SavedGame_1; }
	inline void set__SavedGame_1(GK_SavedGame_t3445071760 * value)
	{
		____SavedGame_1 = value;
		Il2CppCodeGenWriteBarrier((&____SavedGame_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GK_SAVERESULT_T325278800_H
#ifndef GK_SAVEREMOVERESULT_T1262993476_H
#define GK_SAVEREMOVERESULT_T1262993476_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GK_SaveRemoveResult
struct  GK_SaveRemoveResult_t1262993476  : public Result_t4170401718
{
public:
	// System.String GK_SaveRemoveResult::_SaveName
	String_t* ____SaveName_1;

public:
	inline static int32_t get_offset_of__SaveName_1() { return static_cast<int32_t>(offsetof(GK_SaveRemoveResult_t1262993476, ____SaveName_1)); }
	inline String_t* get__SaveName_1() const { return ____SaveName_1; }
	inline String_t** get_address_of__SaveName_1() { return &____SaveName_1; }
	inline void set__SaveName_1(String_t* value)
	{
		____SaveName_1 = value;
		Il2CppCodeGenWriteBarrier((&____SaveName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GK_SAVEREMOVERESULT_T1262993476_H
#ifndef GK_TBM_MATCHENDRESULT_T3737364521_H
#define GK_TBM_MATCHENDRESULT_T3737364521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GK_TBM_MatchEndResult
struct  GK_TBM_MatchEndResult_t3737364521  : public Result_t4170401718
{
public:
	// GK_TBM_Match GK_TBM_MatchEndResult::_Match
	GK_TBM_Match_t83581881 * ____Match_1;

public:
	inline static int32_t get_offset_of__Match_1() { return static_cast<int32_t>(offsetof(GK_TBM_MatchEndResult_t3737364521, ____Match_1)); }
	inline GK_TBM_Match_t83581881 * get__Match_1() const { return ____Match_1; }
	inline GK_TBM_Match_t83581881 ** get_address_of__Match_1() { return &____Match_1; }
	inline void set__Match_1(GK_TBM_Match_t83581881 * value)
	{
		____Match_1 = value;
		Il2CppCodeGenWriteBarrier((&____Match_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GK_TBM_MATCHENDRESULT_T3737364521_H
#ifndef CK_QUERYRESULT_T1037223848_H
#define CK_QUERYRESULT_T1037223848_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CK_QueryResult
struct  CK_QueryResult_t1037223848  : public Result_t4170401718
{
public:
	// CK_Database CK_QueryResult::_Database
	CK_Database_t2819592330 * ____Database_1;
	// System.Collections.Generic.List`1<CK_Record> CK_QueryResult::_Records
	List_1_t1374631790 * ____Records_2;

public:
	inline static int32_t get_offset_of__Database_1() { return static_cast<int32_t>(offsetof(CK_QueryResult_t1037223848, ____Database_1)); }
	inline CK_Database_t2819592330 * get__Database_1() const { return ____Database_1; }
	inline CK_Database_t2819592330 ** get_address_of__Database_1() { return &____Database_1; }
	inline void set__Database_1(CK_Database_t2819592330 * value)
	{
		____Database_1 = value;
		Il2CppCodeGenWriteBarrier((&____Database_1), value);
	}

	inline static int32_t get_offset_of__Records_2() { return static_cast<int32_t>(offsetof(CK_QueryResult_t1037223848, ____Records_2)); }
	inline List_1_t1374631790 * get__Records_2() const { return ____Records_2; }
	inline List_1_t1374631790 ** get_address_of__Records_2() { return &____Records_2; }
	inline void set__Records_2(List_1_t1374631790 * value)
	{
		____Records_2 = value;
		Il2CppCodeGenWriteBarrier((&____Records_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CK_QUERYRESULT_T1037223848_H
#ifndef TIMESPAN_T881159249_H
#define TIMESPAN_T881159249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t881159249 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_8;

public:
	inline static int32_t get_offset_of__ticks_8() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249, ____ticks_8)); }
	inline int64_t get__ticks_8() const { return ____ticks_8; }
	inline int64_t* get_address_of__ticks_8() { return &____ticks_8; }
	inline void set__ticks_8(int64_t value)
	{
		____ticks_8 = value;
	}
};

struct TimeSpan_t881159249_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t881159249  ___MaxValue_5;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t881159249  ___MinValue_6;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t881159249  ___Zero_7;

public:
	inline static int32_t get_offset_of_MaxValue_5() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MaxValue_5)); }
	inline TimeSpan_t881159249  get_MaxValue_5() const { return ___MaxValue_5; }
	inline TimeSpan_t881159249 * get_address_of_MaxValue_5() { return &___MaxValue_5; }
	inline void set_MaxValue_5(TimeSpan_t881159249  value)
	{
		___MaxValue_5 = value;
	}

	inline static int32_t get_offset_of_MinValue_6() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MinValue_6)); }
	inline TimeSpan_t881159249  get_MinValue_6() const { return ___MinValue_6; }
	inline TimeSpan_t881159249 * get_address_of_MinValue_6() { return &___MinValue_6; }
	inline void set_MinValue_6(TimeSpan_t881159249  value)
	{
		___MinValue_6 = value;
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___Zero_7)); }
	inline TimeSpan_t881159249  get_Zero_7() const { return ___Zero_7; }
	inline TimeSpan_t881159249 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(TimeSpan_t881159249  value)
	{
		___Zero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T881159249_H
#ifndef GK_TBM_REMATCHRESULT_T1046172076_H
#define GK_TBM_REMATCHRESULT_T1046172076_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GK_TBM_RematchResult
struct  GK_TBM_RematchResult_t1046172076  : public Result_t4170401718
{
public:
	// GK_TBM_Match GK_TBM_RematchResult::_Match
	GK_TBM_Match_t83581881 * ____Match_1;

public:
	inline static int32_t get_offset_of__Match_1() { return static_cast<int32_t>(offsetof(GK_TBM_RematchResult_t1046172076, ____Match_1)); }
	inline GK_TBM_Match_t83581881 * get__Match_1() const { return ____Match_1; }
	inline GK_TBM_Match_t83581881 ** get_address_of__Match_1() { return &____Match_1; }
	inline void set__Match_1(GK_TBM_Match_t83581881 * value)
	{
		____Match_1 = value;
		Il2CppCodeGenWriteBarrier((&____Match_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GK_TBM_REMATCHRESULT_T1046172076_H
#ifndef GK_TBM_MATCHTURNRESULT_T2275859377_H
#define GK_TBM_MATCHTURNRESULT_T2275859377_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GK_TBM_MatchTurnResult
struct  GK_TBM_MatchTurnResult_t2275859377  : public Result_t4170401718
{
public:
	// GK_TBM_Match GK_TBM_MatchTurnResult::_Match
	GK_TBM_Match_t83581881 * ____Match_1;

public:
	inline static int32_t get_offset_of__Match_1() { return static_cast<int32_t>(offsetof(GK_TBM_MatchTurnResult_t2275859377, ____Match_1)); }
	inline GK_TBM_Match_t83581881 * get__Match_1() const { return ____Match_1; }
	inline GK_TBM_Match_t83581881 ** get_address_of__Match_1() { return &____Match_1; }
	inline void set__Match_1(GK_TBM_Match_t83581881 * value)
	{
		____Match_1 = value;
		Il2CppCodeGenWriteBarrier((&____Match_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GK_TBM_MATCHTURNRESULT_T2275859377_H
#ifndef CK_RECORDDELETERESULT_T2457932905_H
#define CK_RECORDDELETERESULT_T2457932905_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CK_RecordDeleteResult
struct  CK_RecordDeleteResult_t2457932905  : public Result_t4170401718
{
public:
	// CK_RecordID CK_RecordDeleteResult::_RecordID
	CK_RecordID_t3242371914 * ____RecordID_1;
	// CK_Database CK_RecordDeleteResult::_Database
	CK_Database_t2819592330 * ____Database_2;

public:
	inline static int32_t get_offset_of__RecordID_1() { return static_cast<int32_t>(offsetof(CK_RecordDeleteResult_t2457932905, ____RecordID_1)); }
	inline CK_RecordID_t3242371914 * get__RecordID_1() const { return ____RecordID_1; }
	inline CK_RecordID_t3242371914 ** get_address_of__RecordID_1() { return &____RecordID_1; }
	inline void set__RecordID_1(CK_RecordID_t3242371914 * value)
	{
		____RecordID_1 = value;
		Il2CppCodeGenWriteBarrier((&____RecordID_1), value);
	}

	inline static int32_t get_offset_of__Database_2() { return static_cast<int32_t>(offsetof(CK_RecordDeleteResult_t2457932905, ____Database_2)); }
	inline CK_Database_t2819592330 * get__Database_2() const { return ____Database_2; }
	inline CK_Database_t2819592330 ** get_address_of__Database_2() { return &____Database_2; }
	inline void set__Database_2(CK_Database_t2819592330 * value)
	{
		____Database_2 = value;
		Il2CppCodeGenWriteBarrier((&____Database_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CK_RECORDDELETERESULT_T2457932905_H
#ifndef GK_TBM_MATCHQUITRESULT_T1382588157_H
#define GK_TBM_MATCHQUITRESULT_T1382588157_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GK_TBM_MatchQuitResult
struct  GK_TBM_MatchQuitResult_t1382588157  : public Result_t4170401718
{
public:
	// System.String GK_TBM_MatchQuitResult::_MatchId
	String_t* ____MatchId_1;

public:
	inline static int32_t get_offset_of__MatchId_1() { return static_cast<int32_t>(offsetof(GK_TBM_MatchQuitResult_t1382588157, ____MatchId_1)); }
	inline String_t* get__MatchId_1() const { return ____MatchId_1; }
	inline String_t** get_address_of__MatchId_1() { return &____MatchId_1; }
	inline void set__MatchId_1(String_t* value)
	{
		____MatchId_1 = value;
		Il2CppCodeGenWriteBarrier((&____MatchId_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GK_TBM_MATCHQUITRESULT_T1382588157_H
#ifndef GK_TBM_MATCHINITRESULT_T1763010693_H
#define GK_TBM_MATCHINITRESULT_T1763010693_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GK_TBM_MatchInitResult
struct  GK_TBM_MatchInitResult_t1763010693  : public Result_t4170401718
{
public:
	// GK_TBM_Match GK_TBM_MatchInitResult::_Match
	GK_TBM_Match_t83581881 * ____Match_1;

public:
	inline static int32_t get_offset_of__Match_1() { return static_cast<int32_t>(offsetof(GK_TBM_MatchInitResult_t1763010693, ____Match_1)); }
	inline GK_TBM_Match_t83581881 * get__Match_1() const { return ____Match_1; }
	inline GK_TBM_Match_t83581881 ** get_address_of__Match_1() { return &____Match_1; }
	inline void set__Match_1(GK_TBM_Match_t83581881 * value)
	{
		____Match_1 = value;
		Il2CppCodeGenWriteBarrier((&____Match_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GK_TBM_MATCHINITRESULT_T1763010693_H
#ifndef GK_TBM_MATCHREMOVEDRESULT_T392638254_H
#define GK_TBM_MATCHREMOVEDRESULT_T392638254_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GK_TBM_MatchRemovedResult
struct  GK_TBM_MatchRemovedResult_t392638254  : public Result_t4170401718
{
public:
	// System.String GK_TBM_MatchRemovedResult::_MatchId
	String_t* ____MatchId_1;

public:
	inline static int32_t get_offset_of__MatchId_1() { return static_cast<int32_t>(offsetof(GK_TBM_MatchRemovedResult_t392638254, ____MatchId_1)); }
	inline String_t* get__MatchId_1() const { return ____MatchId_1; }
	inline String_t** get_address_of__MatchId_1() { return &____MatchId_1; }
	inline void set__MatchId_1(String_t* value)
	{
		____MatchId_1 = value;
		Il2CppCodeGenWriteBarrier((&____MatchId_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GK_TBM_MATCHREMOVEDRESULT_T392638254_H
#ifndef PRICETIER_T132027085_H
#define PRICETIER_T132027085_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSNative.StoreKit.PriceTier
struct  PriceTier_t132027085 
{
public:
	// System.Int32 SA.IOSNative.StoreKit.PriceTier::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PriceTier_t132027085, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRICETIER_T132027085_H
#ifndef SK_CLOUDSERVICEAUTHORIZATIONSTATUS_T2788808645_H
#define SK_CLOUDSERVICEAUTHORIZATIONSTATUS_T2788808645_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SK_CloudServiceAuthorizationStatus
struct  SK_CloudServiceAuthorizationStatus_t2788808645 
{
public:
	// System.Int32 SK_CloudServiceAuthorizationStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SK_CloudServiceAuthorizationStatus_t2788808645, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SK_CLOUDSERVICEAUTHORIZATIONSTATUS_T2788808645_H
#ifndef AUTHORIZATIONSTATUS_T3886484108_H
#define AUTHORIZATIONSTATUS_T3886484108_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSNative.UserNotifications.AuthorizationStatus
struct  AuthorizationStatus_t3886484108 
{
public:
	// System.Int32 SA.IOSNative.UserNotifications.AuthorizationStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AuthorizationStatus_t3886484108, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHORIZATIONSTATUS_T3886484108_H
#ifndef NOTIFICATIONSTATUS_T26805291_H
#define NOTIFICATIONSTATUS_T26805291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSNative.UserNotifications.NotificationStatus
struct  NotificationStatus_t26805291 
{
public:
	// System.Int32 SA.IOSNative.UserNotifications.NotificationStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(NotificationStatus_t26805291, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIFICATIONSTATUS_T26805291_H
#ifndef DATECOMPONENTS_T2005439303_H
#define DATECOMPONENTS_T2005439303_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSNative.UserNotifications.DateComponents
struct  DateComponents_t2005439303  : public RuntimeObject
{
public:
	// System.Nullable`1<System.Int32> SA.IOSNative.UserNotifications.DateComponents::Year
	Nullable_1_t378540539  ___Year_0;
	// System.Nullable`1<System.Int32> SA.IOSNative.UserNotifications.DateComponents::Month
	Nullable_1_t378540539  ___Month_1;
	// System.Nullable`1<System.Int32> SA.IOSNative.UserNotifications.DateComponents::Day
	Nullable_1_t378540539  ___Day_2;
	// System.Nullable`1<System.Int32> SA.IOSNative.UserNotifications.DateComponents::Hour
	Nullable_1_t378540539  ___Hour_3;
	// System.Nullable`1<System.Int32> SA.IOSNative.UserNotifications.DateComponents::Minute
	Nullable_1_t378540539  ___Minute_4;
	// System.Nullable`1<System.Int32> SA.IOSNative.UserNotifications.DateComponents::Second
	Nullable_1_t378540539  ___Second_5;
	// System.Nullable`1<System.Int32> SA.IOSNative.UserNotifications.DateComponents::Weekday
	Nullable_1_t378540539  ___Weekday_6;
	// System.Nullable`1<System.Int32> SA.IOSNative.UserNotifications.DateComponents::Quarter
	Nullable_1_t378540539  ___Quarter_7;

public:
	inline static int32_t get_offset_of_Year_0() { return static_cast<int32_t>(offsetof(DateComponents_t2005439303, ___Year_0)); }
	inline Nullable_1_t378540539  get_Year_0() const { return ___Year_0; }
	inline Nullable_1_t378540539 * get_address_of_Year_0() { return &___Year_0; }
	inline void set_Year_0(Nullable_1_t378540539  value)
	{
		___Year_0 = value;
	}

	inline static int32_t get_offset_of_Month_1() { return static_cast<int32_t>(offsetof(DateComponents_t2005439303, ___Month_1)); }
	inline Nullable_1_t378540539  get_Month_1() const { return ___Month_1; }
	inline Nullable_1_t378540539 * get_address_of_Month_1() { return &___Month_1; }
	inline void set_Month_1(Nullable_1_t378540539  value)
	{
		___Month_1 = value;
	}

	inline static int32_t get_offset_of_Day_2() { return static_cast<int32_t>(offsetof(DateComponents_t2005439303, ___Day_2)); }
	inline Nullable_1_t378540539  get_Day_2() const { return ___Day_2; }
	inline Nullable_1_t378540539 * get_address_of_Day_2() { return &___Day_2; }
	inline void set_Day_2(Nullable_1_t378540539  value)
	{
		___Day_2 = value;
	}

	inline static int32_t get_offset_of_Hour_3() { return static_cast<int32_t>(offsetof(DateComponents_t2005439303, ___Hour_3)); }
	inline Nullable_1_t378540539  get_Hour_3() const { return ___Hour_3; }
	inline Nullable_1_t378540539 * get_address_of_Hour_3() { return &___Hour_3; }
	inline void set_Hour_3(Nullable_1_t378540539  value)
	{
		___Hour_3 = value;
	}

	inline static int32_t get_offset_of_Minute_4() { return static_cast<int32_t>(offsetof(DateComponents_t2005439303, ___Minute_4)); }
	inline Nullable_1_t378540539  get_Minute_4() const { return ___Minute_4; }
	inline Nullable_1_t378540539 * get_address_of_Minute_4() { return &___Minute_4; }
	inline void set_Minute_4(Nullable_1_t378540539  value)
	{
		___Minute_4 = value;
	}

	inline static int32_t get_offset_of_Second_5() { return static_cast<int32_t>(offsetof(DateComponents_t2005439303, ___Second_5)); }
	inline Nullable_1_t378540539  get_Second_5() const { return ___Second_5; }
	inline Nullable_1_t378540539 * get_address_of_Second_5() { return &___Second_5; }
	inline void set_Second_5(Nullable_1_t378540539  value)
	{
		___Second_5 = value;
	}

	inline static int32_t get_offset_of_Weekday_6() { return static_cast<int32_t>(offsetof(DateComponents_t2005439303, ___Weekday_6)); }
	inline Nullable_1_t378540539  get_Weekday_6() const { return ___Weekday_6; }
	inline Nullable_1_t378540539 * get_address_of_Weekday_6() { return &___Weekday_6; }
	inline void set_Weekday_6(Nullable_1_t378540539  value)
	{
		___Weekday_6 = value;
	}

	inline static int32_t get_offset_of_Quarter_7() { return static_cast<int32_t>(offsetof(DateComponents_t2005439303, ___Quarter_7)); }
	inline Nullable_1_t378540539  get_Quarter_7() const { return ___Quarter_7; }
	inline Nullable_1_t378540539 * get_address_of_Quarter_7() { return &___Quarter_7; }
	inline void set_Quarter_7(Nullable_1_t378540539  value)
	{
		___Quarter_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATECOMPONENTS_T2005439303_H
#ifndef TRANSACTIONERRORCODE_T4212012852_H
#define TRANSACTIONERRORCODE_T4212012852_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSNative.StoreKit.TransactionErrorCode
struct  TransactionErrorCode_t4212012852 
{
public:
	// System.Int32 SA.IOSNative.StoreKit.TransactionErrorCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TransactionErrorCode_t4212012852, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSACTIONERRORCODE_T4212012852_H
#ifndef TEXTMESSAGECOMPOSERESULT_T1393453087_H
#define TEXTMESSAGECOMPOSERESULT_T1393453087_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextMessageComposeResult
struct  TextMessageComposeResult_t1393453087 
{
public:
	// System.Int32 TextMessageComposeResult::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextMessageComposeResult_t1393453087, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTMESSAGECOMPOSERESULT_T1393453087_H
#ifndef TRANSACTIONSHANDLINGMODE_T1662146145_H
#define TRANSACTIONSHANDLINGMODE_T1662146145_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSNative.StoreKit.TransactionsHandlingMode
struct  TransactionsHandlingMode_t1662146145 
{
public:
	// System.Int32 SA.IOSNative.StoreKit.TransactionsHandlingMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TransactionsHandlingMode_t1662146145, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSACTIONSHANDLINGMODE_T1662146145_H
#ifndef PRODUCTTYPE_T773894259_H
#define PRODUCTTYPE_T773894259_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSNative.StoreKit.ProductType
struct  ProductType_t773894259 
{
public:
	// System.Int32 SA.IOSNative.StoreKit.ProductType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ProductType_t773894259, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRODUCTTYPE_T773894259_H
#ifndef PURCHASESTATE_T1124174412_H
#define PURCHASESTATE_T1124174412_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSNative.StoreKit.PurchaseState
struct  PurchaseState_t1124174412 
{
public:
	// System.Int32 SA.IOSNative.StoreKit.PurchaseState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PurchaseState_t1124174412, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PURCHASESTATE_T1124174412_H
#ifndef SK_CLOUDSERVICECAPABILITY_T967689214_H
#define SK_CLOUDSERVICECAPABILITY_T967689214_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SK_CloudServiceCapability
struct  SK_CloudServiceCapability_t967689214 
{
public:
	// System.Int32 SK_CloudServiceCapability::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SK_CloudServiceCapability_t967689214, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SK_CLOUDSERVICECAPABILITY_T967689214_H
#ifndef ALERTSTYLE_T1308137552_H
#define ALERTSTYLE_T1308137552_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSNative.UserNotifications.AlertStyle
struct  AlertStyle_t1308137552 
{
public:
	// System.Int32 SA.IOSNative.UserNotifications.AlertStyle::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AlertStyle_t1308137552, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALERTSTYLE_T1308137552_H
#ifndef DATETIMEKIND_T3468814247_H
#define DATETIMEKIND_T3468814247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t3468814247 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t3468814247, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T3468814247_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef ISN_IMAGESOURCE_T3439168966_H
#define ISN_IMAGESOURCE_T3439168966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ISN_ImageSource
struct  ISN_ImageSource_t3439168966 
{
public:
	// System.Int32 ISN_ImageSource::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ISN_ImageSource_t3439168966, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISN_IMAGESOURCE_T3439168966_H
#ifndef IOSGALLERYLOADIMAGEFORMAT_T3491991360_H
#define IOSGALLERYLOADIMAGEFORMAT_T3491991360_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IOSGalleryLoadImageFormat
struct  IOSGalleryLoadImageFormat_t3491991360 
{
public:
	// System.Int32 IOSGalleryLoadImageFormat::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(IOSGalleryLoadImageFormat_t3491991360, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSGALLERYLOADIMAGEFORMAT_T3491991360_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef GK_PLAYERCONNECTIONSTATE_T3314454918_H
#define GK_PLAYERCONNECTIONSTATE_T3314454918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GK_PlayerConnectionState
struct  GK_PlayerConnectionState_t3314454918 
{
public:
	// System.Int32 GK_PlayerConnectionState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GK_PlayerConnectionState_t3314454918, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GK_PLAYERCONNECTIONSTATE_T3314454918_H
#ifndef GK_MATCHSENDDATAMODE_T148239142_H
#define GK_MATCHSENDDATAMODE_T148239142_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GK_MatchSendDataMode
struct  GK_MatchSendDataMode_t148239142 
{
public:
	// System.Int32 GK_MatchSendDataMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GK_MatchSendDataMode_t148239142, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GK_MATCHSENDDATAMODE_T148239142_H
#ifndef GK_TURNBASEDMATCHOUTCOME_T2945463772_H
#define GK_TURNBASEDMATCHOUTCOME_T2945463772_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GK_TurnBasedMatchOutcome
struct  GK_TurnBasedMatchOutcome_t2945463772 
{
public:
	// System.Int32 GK_TurnBasedMatchOutcome::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GK_TurnBasedMatchOutcome_t2945463772, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GK_TURNBASEDMATCHOUTCOME_T2945463772_H
#ifndef GK_TURNBASEDPARTICIPANTSTATUS_T2860676061_H
#define GK_TURNBASEDPARTICIPANTSTATUS_T2860676061_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GK_TurnBasedParticipantStatus
struct  GK_TurnBasedParticipantStatus_t2860676061 
{
public:
	// System.Int32 GK_TurnBasedParticipantStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GK_TurnBasedParticipantStatus_t2860676061, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GK_TURNBASEDPARTICIPANTSTATUS_T2860676061_H
#ifndef GK_TURNBASEDMATCHSTATUS_T3641047812_H
#define GK_TURNBASEDMATCHSTATUS_T3641047812_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GK_TurnBasedMatchStatus
struct  GK_TurnBasedMatchStatus_t3641047812 
{
public:
	// System.Int32 GK_TurnBasedMatchStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GK_TurnBasedMatchStatus_t3641047812, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GK_TURNBASEDMATCHSTATUS_T3641047812_H
#ifndef IOSDIALOGRESULT_T243976279_H
#define IOSDIALOGRESULT_T243976279_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IOSDialogResult
struct  IOSDialogResult_t243976279 
{
public:
	// System.Int32 IOSDialogResult::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(IOSDialogResult_t243976279, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSDIALOGRESULT_T243976279_H
#ifndef GK_COLLECTIONTYPE_T4065955501_H
#define GK_COLLECTIONTYPE_T4065955501_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GK_CollectionType
struct  GK_CollectionType_t4065955501 
{
public:
	// System.Int32 GK_CollectionType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GK_CollectionType_t4065955501, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GK_COLLECTIONTYPE_T4065955501_H
#ifndef GK_TIMESPAN_T2679980730_H
#define GK_TIMESPAN_T2679980730_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GK_TimeSpan
struct  GK_TimeSpan_t2679980730 
{
public:
	// System.Int32 GK_TimeSpan::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GK_TimeSpan_t2679980730, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GK_TIMESPAN_T2679980730_H
#ifndef MP_MUSICSHUFFLEMODE_T1928103744_H
#define MP_MUSICSHUFFLEMODE_T1928103744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MP_MusicShuffleMode
struct  MP_MusicShuffleMode_t1928103744 
{
public:
	// System.Int32 MP_MusicShuffleMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MP_MusicShuffleMode_t1928103744, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MP_MUSICSHUFFLEMODE_T1928103744_H
#ifndef MP_MUSICPLAYBACKSTATE_T1009348098_H
#define MP_MUSICPLAYBACKSTATE_T1009348098_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MP_MusicPlaybackState
struct  MP_MusicPlaybackState_t1009348098 
{
public:
	// System.Int32 MP_MusicPlaybackState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MP_MusicPlaybackState_t1009348098, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MP_MUSICPLAYBACKSTATE_T1009348098_H
#ifndef MP_MUSICREPEATMODE_T1738310452_H
#define MP_MUSICREPEATMODE_T1738310452_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MP_MusicRepeatMode
struct  MP_MusicRepeatMode_t1738310452 
{
public:
	// System.Int32 MP_MusicRepeatMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MP_MusicRepeatMode_t1738310452, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MP_MUSICREPEATMODE_T1738310452_H
#ifndef SK_REQUESTCAPABILITIERESULT_T4153260424_H
#define SK_REQUESTCAPABILITIERESULT_T4153260424_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SK_RequestCapabilitieResult
struct  SK_RequestCapabilitieResult_t4153260424  : public Result_t4170401718
{
public:
	// SK_CloudServiceCapability SK_RequestCapabilitieResult::_Capability
	int32_t ____Capability_1;

public:
	inline static int32_t get_offset_of__Capability_1() { return static_cast<int32_t>(offsetof(SK_RequestCapabilitieResult_t4153260424, ____Capability_1)); }
	inline int32_t get__Capability_1() const { return ____Capability_1; }
	inline int32_t* get_address_of__Capability_1() { return &____Capability_1; }
	inline void set__Capability_1(int32_t value)
	{
		____Capability_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SK_REQUESTCAPABILITIERESULT_T4153260424_H
#ifndef PRODUCT_T3117358678_H
#define PRODUCT_T3117358678_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSNative.StoreKit.Product
struct  Product_t3117358678  : public RuntimeObject
{
public:
	// System.Boolean SA.IOSNative.StoreKit.Product::IsOpen
	bool ___IsOpen_0;
	// System.Boolean SA.IOSNative.StoreKit.Product::_IsAvailable
	bool ____IsAvailable_1;
	// System.String SA.IOSNative.StoreKit.Product::_Id
	String_t* ____Id_2;
	// System.String SA.IOSNative.StoreKit.Product::_DisplayName
	String_t* ____DisplayName_3;
	// System.String SA.IOSNative.StoreKit.Product::_Description
	String_t* ____Description_4;
	// System.Single SA.IOSNative.StoreKit.Product::_Price
	float ____Price_5;
	// System.String SA.IOSNative.StoreKit.Product::_LocalizedPrice
	String_t* ____LocalizedPrice_6;
	// System.String SA.IOSNative.StoreKit.Product::_CurrencySymbol
	String_t* ____CurrencySymbol_7;
	// System.String SA.IOSNative.StoreKit.Product::_CurrencyCode
	String_t* ____CurrencyCode_8;
	// UnityEngine.Texture2D SA.IOSNative.StoreKit.Product::_Texture
	Texture2D_t3840446185 * ____Texture_9;
	// SA.IOSNative.StoreKit.ProductType SA.IOSNative.StoreKit.Product::_ProductType
	int32_t ____ProductType_10;
	// SA.IOSNative.StoreKit.PriceTier SA.IOSNative.StoreKit.Product::_PriceTier
	int32_t ____PriceTier_11;

public:
	inline static int32_t get_offset_of_IsOpen_0() { return static_cast<int32_t>(offsetof(Product_t3117358678, ___IsOpen_0)); }
	inline bool get_IsOpen_0() const { return ___IsOpen_0; }
	inline bool* get_address_of_IsOpen_0() { return &___IsOpen_0; }
	inline void set_IsOpen_0(bool value)
	{
		___IsOpen_0 = value;
	}

	inline static int32_t get_offset_of__IsAvailable_1() { return static_cast<int32_t>(offsetof(Product_t3117358678, ____IsAvailable_1)); }
	inline bool get__IsAvailable_1() const { return ____IsAvailable_1; }
	inline bool* get_address_of__IsAvailable_1() { return &____IsAvailable_1; }
	inline void set__IsAvailable_1(bool value)
	{
		____IsAvailable_1 = value;
	}

	inline static int32_t get_offset_of__Id_2() { return static_cast<int32_t>(offsetof(Product_t3117358678, ____Id_2)); }
	inline String_t* get__Id_2() const { return ____Id_2; }
	inline String_t** get_address_of__Id_2() { return &____Id_2; }
	inline void set__Id_2(String_t* value)
	{
		____Id_2 = value;
		Il2CppCodeGenWriteBarrier((&____Id_2), value);
	}

	inline static int32_t get_offset_of__DisplayName_3() { return static_cast<int32_t>(offsetof(Product_t3117358678, ____DisplayName_3)); }
	inline String_t* get__DisplayName_3() const { return ____DisplayName_3; }
	inline String_t** get_address_of__DisplayName_3() { return &____DisplayName_3; }
	inline void set__DisplayName_3(String_t* value)
	{
		____DisplayName_3 = value;
		Il2CppCodeGenWriteBarrier((&____DisplayName_3), value);
	}

	inline static int32_t get_offset_of__Description_4() { return static_cast<int32_t>(offsetof(Product_t3117358678, ____Description_4)); }
	inline String_t* get__Description_4() const { return ____Description_4; }
	inline String_t** get_address_of__Description_4() { return &____Description_4; }
	inline void set__Description_4(String_t* value)
	{
		____Description_4 = value;
		Il2CppCodeGenWriteBarrier((&____Description_4), value);
	}

	inline static int32_t get_offset_of__Price_5() { return static_cast<int32_t>(offsetof(Product_t3117358678, ____Price_5)); }
	inline float get__Price_5() const { return ____Price_5; }
	inline float* get_address_of__Price_5() { return &____Price_5; }
	inline void set__Price_5(float value)
	{
		____Price_5 = value;
	}

	inline static int32_t get_offset_of__LocalizedPrice_6() { return static_cast<int32_t>(offsetof(Product_t3117358678, ____LocalizedPrice_6)); }
	inline String_t* get__LocalizedPrice_6() const { return ____LocalizedPrice_6; }
	inline String_t** get_address_of__LocalizedPrice_6() { return &____LocalizedPrice_6; }
	inline void set__LocalizedPrice_6(String_t* value)
	{
		____LocalizedPrice_6 = value;
		Il2CppCodeGenWriteBarrier((&____LocalizedPrice_6), value);
	}

	inline static int32_t get_offset_of__CurrencySymbol_7() { return static_cast<int32_t>(offsetof(Product_t3117358678, ____CurrencySymbol_7)); }
	inline String_t* get__CurrencySymbol_7() const { return ____CurrencySymbol_7; }
	inline String_t** get_address_of__CurrencySymbol_7() { return &____CurrencySymbol_7; }
	inline void set__CurrencySymbol_7(String_t* value)
	{
		____CurrencySymbol_7 = value;
		Il2CppCodeGenWriteBarrier((&____CurrencySymbol_7), value);
	}

	inline static int32_t get_offset_of__CurrencyCode_8() { return static_cast<int32_t>(offsetof(Product_t3117358678, ____CurrencyCode_8)); }
	inline String_t* get__CurrencyCode_8() const { return ____CurrencyCode_8; }
	inline String_t** get_address_of__CurrencyCode_8() { return &____CurrencyCode_8; }
	inline void set__CurrencyCode_8(String_t* value)
	{
		____CurrencyCode_8 = value;
		Il2CppCodeGenWriteBarrier((&____CurrencyCode_8), value);
	}

	inline static int32_t get_offset_of__Texture_9() { return static_cast<int32_t>(offsetof(Product_t3117358678, ____Texture_9)); }
	inline Texture2D_t3840446185 * get__Texture_9() const { return ____Texture_9; }
	inline Texture2D_t3840446185 ** get_address_of__Texture_9() { return &____Texture_9; }
	inline void set__Texture_9(Texture2D_t3840446185 * value)
	{
		____Texture_9 = value;
		Il2CppCodeGenWriteBarrier((&____Texture_9), value);
	}

	inline static int32_t get_offset_of__ProductType_10() { return static_cast<int32_t>(offsetof(Product_t3117358678, ____ProductType_10)); }
	inline int32_t get__ProductType_10() const { return ____ProductType_10; }
	inline int32_t* get_address_of__ProductType_10() { return &____ProductType_10; }
	inline void set__ProductType_10(int32_t value)
	{
		____ProductType_10 = value;
	}

	inline static int32_t get_offset_of__PriceTier_11() { return static_cast<int32_t>(offsetof(Product_t3117358678, ____PriceTier_11)); }
	inline int32_t get__PriceTier_11() const { return ____PriceTier_11; }
	inline int32_t* get_address_of__PriceTier_11() { return &____PriceTier_11; }
	inline void set__PriceTier_11(int32_t value)
	{
		____PriceTier_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRODUCT_T3117358678_H
#ifndef PURCHASERESULT_T3226678372_H
#define PURCHASERESULT_T3226678372_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSNative.StoreKit.PurchaseResult
struct  PurchaseResult_t3226678372  : public Result_t4170401718
{
public:
	// System.String SA.IOSNative.StoreKit.PurchaseResult::_ProductIdentifier
	String_t* ____ProductIdentifier_1;
	// SA.IOSNative.StoreKit.PurchaseState SA.IOSNative.StoreKit.PurchaseResult::_State
	int32_t ____State_2;
	// System.String SA.IOSNative.StoreKit.PurchaseResult::_Receipt
	String_t* ____Receipt_3;
	// System.String SA.IOSNative.StoreKit.PurchaseResult::_TransactionIdentifier
	String_t* ____TransactionIdentifier_4;
	// System.String SA.IOSNative.StoreKit.PurchaseResult::_ApplicationUsername
	String_t* ____ApplicationUsername_5;

public:
	inline static int32_t get_offset_of__ProductIdentifier_1() { return static_cast<int32_t>(offsetof(PurchaseResult_t3226678372, ____ProductIdentifier_1)); }
	inline String_t* get__ProductIdentifier_1() const { return ____ProductIdentifier_1; }
	inline String_t** get_address_of__ProductIdentifier_1() { return &____ProductIdentifier_1; }
	inline void set__ProductIdentifier_1(String_t* value)
	{
		____ProductIdentifier_1 = value;
		Il2CppCodeGenWriteBarrier((&____ProductIdentifier_1), value);
	}

	inline static int32_t get_offset_of__State_2() { return static_cast<int32_t>(offsetof(PurchaseResult_t3226678372, ____State_2)); }
	inline int32_t get__State_2() const { return ____State_2; }
	inline int32_t* get_address_of__State_2() { return &____State_2; }
	inline void set__State_2(int32_t value)
	{
		____State_2 = value;
	}

	inline static int32_t get_offset_of__Receipt_3() { return static_cast<int32_t>(offsetof(PurchaseResult_t3226678372, ____Receipt_3)); }
	inline String_t* get__Receipt_3() const { return ____Receipt_3; }
	inline String_t** get_address_of__Receipt_3() { return &____Receipt_3; }
	inline void set__Receipt_3(String_t* value)
	{
		____Receipt_3 = value;
		Il2CppCodeGenWriteBarrier((&____Receipt_3), value);
	}

	inline static int32_t get_offset_of__TransactionIdentifier_4() { return static_cast<int32_t>(offsetof(PurchaseResult_t3226678372, ____TransactionIdentifier_4)); }
	inline String_t* get__TransactionIdentifier_4() const { return ____TransactionIdentifier_4; }
	inline String_t** get_address_of__TransactionIdentifier_4() { return &____TransactionIdentifier_4; }
	inline void set__TransactionIdentifier_4(String_t* value)
	{
		____TransactionIdentifier_4 = value;
		Il2CppCodeGenWriteBarrier((&____TransactionIdentifier_4), value);
	}

	inline static int32_t get_offset_of__ApplicationUsername_5() { return static_cast<int32_t>(offsetof(PurchaseResult_t3226678372, ____ApplicationUsername_5)); }
	inline String_t* get__ApplicationUsername_5() const { return ____ApplicationUsername_5; }
	inline String_t** get_address_of__ApplicationUsername_5() { return &____ApplicationUsername_5; }
	inline void set__ApplicationUsername_5(String_t* value)
	{
		____ApplicationUsername_5 = value;
		Il2CppCodeGenWriteBarrier((&____ApplicationUsername_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PURCHASERESULT_T3226678372_H
#ifndef SK_AUTHORIZATIONRESULT_T3504590035_H
#define SK_AUTHORIZATIONRESULT_T3504590035_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SK_AuthorizationResult
struct  SK_AuthorizationResult_t3504590035  : public Result_t4170401718
{
public:
	// SK_CloudServiceAuthorizationStatus SK_AuthorizationResult::_AuthorizationStatus
	int32_t ____AuthorizationStatus_1;

public:
	inline static int32_t get_offset_of__AuthorizationStatus_1() { return static_cast<int32_t>(offsetof(SK_AuthorizationResult_t3504590035, ____AuthorizationStatus_1)); }
	inline int32_t get__AuthorizationStatus_1() const { return ____AuthorizationStatus_1; }
	inline int32_t* get_address_of__AuthorizationStatus_1() { return &____AuthorizationStatus_1; }
	inline void set__AuthorizationStatus_1(int32_t value)
	{
		____AuthorizationStatus_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SK_AUTHORIZATIONRESULT_T3504590035_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef DATETIME_T3738529785_H
#define DATETIME_T3738529785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t3738529785 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t881159249  ___ticks_10;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_11;

public:
	inline static int32_t get_offset_of_ticks_10() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___ticks_10)); }
	inline TimeSpan_t881159249  get_ticks_10() const { return ___ticks_10; }
	inline TimeSpan_t881159249 * get_address_of_ticks_10() { return &___ticks_10; }
	inline void set_ticks_10(TimeSpan_t881159249  value)
	{
		___ticks_10 = value;
	}

	inline static int32_t get_offset_of_kind_11() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___kind_11)); }
	inline int32_t get_kind_11() const { return ___kind_11; }
	inline int32_t* get_address_of_kind_11() { return &___kind_11; }
	inline void set_kind_11(int32_t value)
	{
		___kind_11 = value;
	}
};

struct DateTime_t3738529785_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t3738529785  ___MaxValue_12;
	// System.DateTime System.DateTime::MinValue
	DateTime_t3738529785  ___MinValue_13;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t1281789340* ___ParseTimeFormats_14;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t1281789340* ___ParseYearDayMonthFormats_15;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t1281789340* ___ParseYearMonthDayFormats_16;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t1281789340* ___ParseDayMonthYearFormats_17;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t1281789340* ___ParseMonthDayYearFormats_18;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t1281789340* ___MonthDayShortFormats_19;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t1281789340* ___DayMonthShortFormats_20;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t385246372* ___daysmonth_21;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t385246372* ___daysmonthleap_22;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_23;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_24;

public:
	inline static int32_t get_offset_of_MaxValue_12() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MaxValue_12)); }
	inline DateTime_t3738529785  get_MaxValue_12() const { return ___MaxValue_12; }
	inline DateTime_t3738529785 * get_address_of_MaxValue_12() { return &___MaxValue_12; }
	inline void set_MaxValue_12(DateTime_t3738529785  value)
	{
		___MaxValue_12 = value;
	}

	inline static int32_t get_offset_of_MinValue_13() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MinValue_13)); }
	inline DateTime_t3738529785  get_MinValue_13() const { return ___MinValue_13; }
	inline DateTime_t3738529785 * get_address_of_MinValue_13() { return &___MinValue_13; }
	inline void set_MinValue_13(DateTime_t3738529785  value)
	{
		___MinValue_13 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_14() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseTimeFormats_14)); }
	inline StringU5BU5D_t1281789340* get_ParseTimeFormats_14() const { return ___ParseTimeFormats_14; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseTimeFormats_14() { return &___ParseTimeFormats_14; }
	inline void set_ParseTimeFormats_14(StringU5BU5D_t1281789340* value)
	{
		___ParseTimeFormats_14 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_14), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_15() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearDayMonthFormats_15)); }
	inline StringU5BU5D_t1281789340* get_ParseYearDayMonthFormats_15() const { return ___ParseYearDayMonthFormats_15; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearDayMonthFormats_15() { return &___ParseYearDayMonthFormats_15; }
	inline void set_ParseYearDayMonthFormats_15(StringU5BU5D_t1281789340* value)
	{
		___ParseYearDayMonthFormats_15 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_15), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_16() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearMonthDayFormats_16)); }
	inline StringU5BU5D_t1281789340* get_ParseYearMonthDayFormats_16() const { return ___ParseYearMonthDayFormats_16; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearMonthDayFormats_16() { return &___ParseYearMonthDayFormats_16; }
	inline void set_ParseYearMonthDayFormats_16(StringU5BU5D_t1281789340* value)
	{
		___ParseYearMonthDayFormats_16 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_16), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_17() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseDayMonthYearFormats_17)); }
	inline StringU5BU5D_t1281789340* get_ParseDayMonthYearFormats_17() const { return ___ParseDayMonthYearFormats_17; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseDayMonthYearFormats_17() { return &___ParseDayMonthYearFormats_17; }
	inline void set_ParseDayMonthYearFormats_17(StringU5BU5D_t1281789340* value)
	{
		___ParseDayMonthYearFormats_17 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_17), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_18() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseMonthDayYearFormats_18)); }
	inline StringU5BU5D_t1281789340* get_ParseMonthDayYearFormats_18() const { return ___ParseMonthDayYearFormats_18; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseMonthDayYearFormats_18() { return &___ParseMonthDayYearFormats_18; }
	inline void set_ParseMonthDayYearFormats_18(StringU5BU5D_t1281789340* value)
	{
		___ParseMonthDayYearFormats_18 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_18), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_19() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MonthDayShortFormats_19)); }
	inline StringU5BU5D_t1281789340* get_MonthDayShortFormats_19() const { return ___MonthDayShortFormats_19; }
	inline StringU5BU5D_t1281789340** get_address_of_MonthDayShortFormats_19() { return &___MonthDayShortFormats_19; }
	inline void set_MonthDayShortFormats_19(StringU5BU5D_t1281789340* value)
	{
		___MonthDayShortFormats_19 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_19), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_20() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___DayMonthShortFormats_20)); }
	inline StringU5BU5D_t1281789340* get_DayMonthShortFormats_20() const { return ___DayMonthShortFormats_20; }
	inline StringU5BU5D_t1281789340** get_address_of_DayMonthShortFormats_20() { return &___DayMonthShortFormats_20; }
	inline void set_DayMonthShortFormats_20(StringU5BU5D_t1281789340* value)
	{
		___DayMonthShortFormats_20 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_20), value);
	}

	inline static int32_t get_offset_of_daysmonth_21() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonth_21)); }
	inline Int32U5BU5D_t385246372* get_daysmonth_21() const { return ___daysmonth_21; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonth_21() { return &___daysmonth_21; }
	inline void set_daysmonth_21(Int32U5BU5D_t385246372* value)
	{
		___daysmonth_21 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_21), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_22() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonthleap_22)); }
	inline Int32U5BU5D_t385246372* get_daysmonthleap_22() const { return ___daysmonthleap_22; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonthleap_22() { return &___daysmonthleap_22; }
	inline void set_daysmonthleap_22(Int32U5BU5D_t385246372* value)
	{
		___daysmonthleap_22 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_22), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_23() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___to_local_time_span_object_23)); }
	inline RuntimeObject * get_to_local_time_span_object_23() const { return ___to_local_time_span_object_23; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_23() { return &___to_local_time_span_object_23; }
	inline void set_to_local_time_span_object_23(RuntimeObject * value)
	{
		___to_local_time_span_object_23 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_23), value);
	}

	inline static int32_t get_offset_of_last_now_24() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___last_now_24)); }
	inline int64_t get_last_now_24() const { return ___last_now_24; }
	inline int64_t* get_address_of_last_now_24() { return &___last_now_24; }
	inline void set_last_now_24(int64_t value)
	{
		___last_now_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T3738529785_H
#ifndef GK_SCORE_T215322251_H
#define GK_SCORE_T215322251_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GK_Score
struct  GK_Score_t215322251  : public RuntimeObject
{
public:
	// System.Int32 GK_Score::_Rank
	int32_t ____Rank_0;
	// System.Int64 GK_Score::_Score
	int64_t ____Score_1;
	// System.Int64 GK_Score::_Context
	int64_t ____Context_2;
	// System.String GK_Score::_PlayerId
	String_t* ____PlayerId_3;
	// System.String GK_Score::_LeaderboardId
	String_t* ____LeaderboardId_4;
	// GK_CollectionType GK_Score::_Collection
	int32_t ____Collection_5;
	// GK_TimeSpan GK_Score::_TimeSpan
	int32_t ____TimeSpan_6;

public:
	inline static int32_t get_offset_of__Rank_0() { return static_cast<int32_t>(offsetof(GK_Score_t215322251, ____Rank_0)); }
	inline int32_t get__Rank_0() const { return ____Rank_0; }
	inline int32_t* get_address_of__Rank_0() { return &____Rank_0; }
	inline void set__Rank_0(int32_t value)
	{
		____Rank_0 = value;
	}

	inline static int32_t get_offset_of__Score_1() { return static_cast<int32_t>(offsetof(GK_Score_t215322251, ____Score_1)); }
	inline int64_t get__Score_1() const { return ____Score_1; }
	inline int64_t* get_address_of__Score_1() { return &____Score_1; }
	inline void set__Score_1(int64_t value)
	{
		____Score_1 = value;
	}

	inline static int32_t get_offset_of__Context_2() { return static_cast<int32_t>(offsetof(GK_Score_t215322251, ____Context_2)); }
	inline int64_t get__Context_2() const { return ____Context_2; }
	inline int64_t* get_address_of__Context_2() { return &____Context_2; }
	inline void set__Context_2(int64_t value)
	{
		____Context_2 = value;
	}

	inline static int32_t get_offset_of__PlayerId_3() { return static_cast<int32_t>(offsetof(GK_Score_t215322251, ____PlayerId_3)); }
	inline String_t* get__PlayerId_3() const { return ____PlayerId_3; }
	inline String_t** get_address_of__PlayerId_3() { return &____PlayerId_3; }
	inline void set__PlayerId_3(String_t* value)
	{
		____PlayerId_3 = value;
		Il2CppCodeGenWriteBarrier((&____PlayerId_3), value);
	}

	inline static int32_t get_offset_of__LeaderboardId_4() { return static_cast<int32_t>(offsetof(GK_Score_t215322251, ____LeaderboardId_4)); }
	inline String_t* get__LeaderboardId_4() const { return ____LeaderboardId_4; }
	inline String_t** get_address_of__LeaderboardId_4() { return &____LeaderboardId_4; }
	inline void set__LeaderboardId_4(String_t* value)
	{
		____LeaderboardId_4 = value;
		Il2CppCodeGenWriteBarrier((&____LeaderboardId_4), value);
	}

	inline static int32_t get_offset_of__Collection_5() { return static_cast<int32_t>(offsetof(GK_Score_t215322251, ____Collection_5)); }
	inline int32_t get__Collection_5() const { return ____Collection_5; }
	inline int32_t* get_address_of__Collection_5() { return &____Collection_5; }
	inline void set__Collection_5(int32_t value)
	{
		____Collection_5 = value;
	}

	inline static int32_t get_offset_of__TimeSpan_6() { return static_cast<int32_t>(offsetof(GK_Score_t215322251, ____TimeSpan_6)); }
	inline int32_t get__TimeSpan_6() const { return ____TimeSpan_6; }
	inline int32_t* get_address_of__TimeSpan_6() { return &____TimeSpan_6; }
	inline void set__TimeSpan_6(int32_t value)
	{
		____TimeSpan_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GK_SCORE_T215322251_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef GK_TBM_PARTICIPANT_T2994240226_H
#define GK_TBM_PARTICIPANT_T2994240226_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GK_TBM_Participant
struct  GK_TBM_Participant_t2994240226  : public RuntimeObject
{
public:
	// System.String GK_TBM_Participant::_PlayerId
	String_t* ____PlayerId_0;
	// System.String GK_TBM_Participant::_MatchId
	String_t* ____MatchId_1;
	// System.DateTime GK_TBM_Participant::_TimeoutDate
	DateTime_t3738529785  ____TimeoutDate_2;
	// System.DateTime GK_TBM_Participant::_LastTurnDate
	DateTime_t3738529785  ____LastTurnDate_3;
	// GK_TurnBasedParticipantStatus GK_TBM_Participant::_Status
	int32_t ____Status_4;
	// GK_TurnBasedMatchOutcome GK_TBM_Participant::_MatchOutcome
	int32_t ____MatchOutcome_5;

public:
	inline static int32_t get_offset_of__PlayerId_0() { return static_cast<int32_t>(offsetof(GK_TBM_Participant_t2994240226, ____PlayerId_0)); }
	inline String_t* get__PlayerId_0() const { return ____PlayerId_0; }
	inline String_t** get_address_of__PlayerId_0() { return &____PlayerId_0; }
	inline void set__PlayerId_0(String_t* value)
	{
		____PlayerId_0 = value;
		Il2CppCodeGenWriteBarrier((&____PlayerId_0), value);
	}

	inline static int32_t get_offset_of__MatchId_1() { return static_cast<int32_t>(offsetof(GK_TBM_Participant_t2994240226, ____MatchId_1)); }
	inline String_t* get__MatchId_1() const { return ____MatchId_1; }
	inline String_t** get_address_of__MatchId_1() { return &____MatchId_1; }
	inline void set__MatchId_1(String_t* value)
	{
		____MatchId_1 = value;
		Il2CppCodeGenWriteBarrier((&____MatchId_1), value);
	}

	inline static int32_t get_offset_of__TimeoutDate_2() { return static_cast<int32_t>(offsetof(GK_TBM_Participant_t2994240226, ____TimeoutDate_2)); }
	inline DateTime_t3738529785  get__TimeoutDate_2() const { return ____TimeoutDate_2; }
	inline DateTime_t3738529785 * get_address_of__TimeoutDate_2() { return &____TimeoutDate_2; }
	inline void set__TimeoutDate_2(DateTime_t3738529785  value)
	{
		____TimeoutDate_2 = value;
	}

	inline static int32_t get_offset_of__LastTurnDate_3() { return static_cast<int32_t>(offsetof(GK_TBM_Participant_t2994240226, ____LastTurnDate_3)); }
	inline DateTime_t3738529785  get__LastTurnDate_3() const { return ____LastTurnDate_3; }
	inline DateTime_t3738529785 * get_address_of__LastTurnDate_3() { return &____LastTurnDate_3; }
	inline void set__LastTurnDate_3(DateTime_t3738529785  value)
	{
		____LastTurnDate_3 = value;
	}

	inline static int32_t get_offset_of__Status_4() { return static_cast<int32_t>(offsetof(GK_TBM_Participant_t2994240226, ____Status_4)); }
	inline int32_t get__Status_4() const { return ____Status_4; }
	inline int32_t* get_address_of__Status_4() { return &____Status_4; }
	inline void set__Status_4(int32_t value)
	{
		____Status_4 = value;
	}

	inline static int32_t get_offset_of__MatchOutcome_5() { return static_cast<int32_t>(offsetof(GK_TBM_Participant_t2994240226, ____MatchOutcome_5)); }
	inline int32_t get__MatchOutcome_5() const { return ____MatchOutcome_5; }
	inline int32_t* get_address_of__MatchOutcome_5() { return &____MatchOutcome_5; }
	inline void set__MatchOutcome_5(int32_t value)
	{
		____MatchOutcome_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GK_TBM_PARTICIPANT_T2994240226_H
#ifndef ISN_LOCALNOTIFICATION_T2969587787_H
#define ISN_LOCALNOTIFICATION_T2969587787_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ISN_LocalNotification
struct  ISN_LocalNotification_t2969587787  : public RuntimeObject
{
public:
	// System.Int32 ISN_LocalNotification::_Id
	int32_t ____Id_0;
	// System.DateTime ISN_LocalNotification::_Date
	DateTime_t3738529785  ____Date_1;
	// System.String ISN_LocalNotification::_Message
	String_t* ____Message_2;
	// System.Boolean ISN_LocalNotification::_UseSound
	bool ____UseSound_3;
	// System.Int32 ISN_LocalNotification::_Badges
	int32_t ____Badges_4;
	// System.String ISN_LocalNotification::_Data
	String_t* ____Data_5;
	// System.String ISN_LocalNotification::_SoundName
	String_t* ____SoundName_6;

public:
	inline static int32_t get_offset_of__Id_0() { return static_cast<int32_t>(offsetof(ISN_LocalNotification_t2969587787, ____Id_0)); }
	inline int32_t get__Id_0() const { return ____Id_0; }
	inline int32_t* get_address_of__Id_0() { return &____Id_0; }
	inline void set__Id_0(int32_t value)
	{
		____Id_0 = value;
	}

	inline static int32_t get_offset_of__Date_1() { return static_cast<int32_t>(offsetof(ISN_LocalNotification_t2969587787, ____Date_1)); }
	inline DateTime_t3738529785  get__Date_1() const { return ____Date_1; }
	inline DateTime_t3738529785 * get_address_of__Date_1() { return &____Date_1; }
	inline void set__Date_1(DateTime_t3738529785  value)
	{
		____Date_1 = value;
	}

	inline static int32_t get_offset_of__Message_2() { return static_cast<int32_t>(offsetof(ISN_LocalNotification_t2969587787, ____Message_2)); }
	inline String_t* get__Message_2() const { return ____Message_2; }
	inline String_t** get_address_of__Message_2() { return &____Message_2; }
	inline void set__Message_2(String_t* value)
	{
		____Message_2 = value;
		Il2CppCodeGenWriteBarrier((&____Message_2), value);
	}

	inline static int32_t get_offset_of__UseSound_3() { return static_cast<int32_t>(offsetof(ISN_LocalNotification_t2969587787, ____UseSound_3)); }
	inline bool get__UseSound_3() const { return ____UseSound_3; }
	inline bool* get_address_of__UseSound_3() { return &____UseSound_3; }
	inline void set__UseSound_3(bool value)
	{
		____UseSound_3 = value;
	}

	inline static int32_t get_offset_of__Badges_4() { return static_cast<int32_t>(offsetof(ISN_LocalNotification_t2969587787, ____Badges_4)); }
	inline int32_t get__Badges_4() const { return ____Badges_4; }
	inline int32_t* get_address_of__Badges_4() { return &____Badges_4; }
	inline void set__Badges_4(int32_t value)
	{
		____Badges_4 = value;
	}

	inline static int32_t get_offset_of__Data_5() { return static_cast<int32_t>(offsetof(ISN_LocalNotification_t2969587787, ____Data_5)); }
	inline String_t* get__Data_5() const { return ____Data_5; }
	inline String_t** get_address_of__Data_5() { return &____Data_5; }
	inline void set__Data_5(String_t* value)
	{
		____Data_5 = value;
		Il2CppCodeGenWriteBarrier((&____Data_5), value);
	}

	inline static int32_t get_offset_of__SoundName_6() { return static_cast<int32_t>(offsetof(ISN_LocalNotification_t2969587787, ____SoundName_6)); }
	inline String_t* get__SoundName_6() const { return ____SoundName_6; }
	inline String_t** get_address_of__SoundName_6() { return &____SoundName_6; }
	inline void set__SoundName_6(String_t* value)
	{
		____SoundName_6 = value;
		Il2CppCodeGenWriteBarrier((&____SoundName_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISN_LOCALNOTIFICATION_T2969587787_H
#ifndef GK_TBM_MATCH_T83581881_H
#define GK_TBM_MATCH_T83581881_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GK_TBM_Match
struct  GK_TBM_Match_t83581881  : public RuntimeObject
{
public:
	// System.String GK_TBM_Match::Id
	String_t* ___Id_0;
	// System.String GK_TBM_Match::Message
	String_t* ___Message_1;
	// GK_TBM_Participant GK_TBM_Match::CurrentParticipant
	GK_TBM_Participant_t2994240226 * ___CurrentParticipant_2;
	// System.DateTime GK_TBM_Match::CreationTimestamp
	DateTime_t3738529785  ___CreationTimestamp_3;
	// System.Byte[] GK_TBM_Match::Data
	ByteU5BU5D_t4116647657* ___Data_4;
	// GK_TurnBasedMatchStatus GK_TBM_Match::Status
	int32_t ___Status_5;
	// System.Collections.Generic.List`1<GK_TBM_Participant> GK_TBM_Match::Participants
	List_1_t171347672 * ___Participants_6;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(GK_TBM_Match_t83581881, ___Id_0)); }
	inline String_t* get_Id_0() const { return ___Id_0; }
	inline String_t** get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(String_t* value)
	{
		___Id_0 = value;
		Il2CppCodeGenWriteBarrier((&___Id_0), value);
	}

	inline static int32_t get_offset_of_Message_1() { return static_cast<int32_t>(offsetof(GK_TBM_Match_t83581881, ___Message_1)); }
	inline String_t* get_Message_1() const { return ___Message_1; }
	inline String_t** get_address_of_Message_1() { return &___Message_1; }
	inline void set_Message_1(String_t* value)
	{
		___Message_1 = value;
		Il2CppCodeGenWriteBarrier((&___Message_1), value);
	}

	inline static int32_t get_offset_of_CurrentParticipant_2() { return static_cast<int32_t>(offsetof(GK_TBM_Match_t83581881, ___CurrentParticipant_2)); }
	inline GK_TBM_Participant_t2994240226 * get_CurrentParticipant_2() const { return ___CurrentParticipant_2; }
	inline GK_TBM_Participant_t2994240226 ** get_address_of_CurrentParticipant_2() { return &___CurrentParticipant_2; }
	inline void set_CurrentParticipant_2(GK_TBM_Participant_t2994240226 * value)
	{
		___CurrentParticipant_2 = value;
		Il2CppCodeGenWriteBarrier((&___CurrentParticipant_2), value);
	}

	inline static int32_t get_offset_of_CreationTimestamp_3() { return static_cast<int32_t>(offsetof(GK_TBM_Match_t83581881, ___CreationTimestamp_3)); }
	inline DateTime_t3738529785  get_CreationTimestamp_3() const { return ___CreationTimestamp_3; }
	inline DateTime_t3738529785 * get_address_of_CreationTimestamp_3() { return &___CreationTimestamp_3; }
	inline void set_CreationTimestamp_3(DateTime_t3738529785  value)
	{
		___CreationTimestamp_3 = value;
	}

	inline static int32_t get_offset_of_Data_4() { return static_cast<int32_t>(offsetof(GK_TBM_Match_t83581881, ___Data_4)); }
	inline ByteU5BU5D_t4116647657* get_Data_4() const { return ___Data_4; }
	inline ByteU5BU5D_t4116647657** get_address_of_Data_4() { return &___Data_4; }
	inline void set_Data_4(ByteU5BU5D_t4116647657* value)
	{
		___Data_4 = value;
		Il2CppCodeGenWriteBarrier((&___Data_4), value);
	}

	inline static int32_t get_offset_of_Status_5() { return static_cast<int32_t>(offsetof(GK_TBM_Match_t83581881, ___Status_5)); }
	inline int32_t get_Status_5() const { return ___Status_5; }
	inline int32_t* get_address_of_Status_5() { return &___Status_5; }
	inline void set_Status_5(int32_t value)
	{
		___Status_5 = value;
	}

	inline static int32_t get_offset_of_Participants_6() { return static_cast<int32_t>(offsetof(GK_TBM_Match_t83581881, ___Participants_6)); }
	inline List_1_t171347672 * get_Participants_6() const { return ___Participants_6; }
	inline List_1_t171347672 ** get_address_of_Participants_6() { return &___Participants_6; }
	inline void set_Participants_6(List_1_t171347672 * value)
	{
		___Participants_6 = value;
		Il2CppCodeGenWriteBarrier((&___Participants_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GK_TBM_MATCH_T83581881_H
#ifndef BILLINGINITLISTENER_T1231368121_H
#define BILLINGINITLISTENER_T1231368121_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSNative.StoreKit.BillingInitChecker/BillingInitListener
struct  BillingInitListener_t1231368121  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BILLINGINITLISTENER_T1231368121_H
#ifndef GK_SAVEDGAME_T3445071760_H
#define GK_SAVEDGAME_T3445071760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GK_SavedGame
struct  GK_SavedGame_t3445071760  : public RuntimeObject
{
public:
	// System.Action`1<GK_SaveDataLoaded> GK_SavedGame::ActionDataLoaded
	Action_1_t244797016 * ___ActionDataLoaded_0;
	// System.String GK_SavedGame::_Id
	String_t* ____Id_1;
	// System.String GK_SavedGame::_Name
	String_t* ____Name_2;
	// System.String GK_SavedGame::_DeviceName
	String_t* ____DeviceName_3;
	// System.DateTime GK_SavedGame::_ModificationDate
	DateTime_t3738529785  ____ModificationDate_4;
	// System.String GK_SavedGame::_OriginalDateString
	String_t* ____OriginalDateString_5;
	// System.Byte[] GK_SavedGame::_Data
	ByteU5BU5D_t4116647657* ____Data_6;
	// System.Boolean GK_SavedGame::_IsDataLoaded
	bool ____IsDataLoaded_7;

public:
	inline static int32_t get_offset_of_ActionDataLoaded_0() { return static_cast<int32_t>(offsetof(GK_SavedGame_t3445071760, ___ActionDataLoaded_0)); }
	inline Action_1_t244797016 * get_ActionDataLoaded_0() const { return ___ActionDataLoaded_0; }
	inline Action_1_t244797016 ** get_address_of_ActionDataLoaded_0() { return &___ActionDataLoaded_0; }
	inline void set_ActionDataLoaded_0(Action_1_t244797016 * value)
	{
		___ActionDataLoaded_0 = value;
		Il2CppCodeGenWriteBarrier((&___ActionDataLoaded_0), value);
	}

	inline static int32_t get_offset_of__Id_1() { return static_cast<int32_t>(offsetof(GK_SavedGame_t3445071760, ____Id_1)); }
	inline String_t* get__Id_1() const { return ____Id_1; }
	inline String_t** get_address_of__Id_1() { return &____Id_1; }
	inline void set__Id_1(String_t* value)
	{
		____Id_1 = value;
		Il2CppCodeGenWriteBarrier((&____Id_1), value);
	}

	inline static int32_t get_offset_of__Name_2() { return static_cast<int32_t>(offsetof(GK_SavedGame_t3445071760, ____Name_2)); }
	inline String_t* get__Name_2() const { return ____Name_2; }
	inline String_t** get_address_of__Name_2() { return &____Name_2; }
	inline void set__Name_2(String_t* value)
	{
		____Name_2 = value;
		Il2CppCodeGenWriteBarrier((&____Name_2), value);
	}

	inline static int32_t get_offset_of__DeviceName_3() { return static_cast<int32_t>(offsetof(GK_SavedGame_t3445071760, ____DeviceName_3)); }
	inline String_t* get__DeviceName_3() const { return ____DeviceName_3; }
	inline String_t** get_address_of__DeviceName_3() { return &____DeviceName_3; }
	inline void set__DeviceName_3(String_t* value)
	{
		____DeviceName_3 = value;
		Il2CppCodeGenWriteBarrier((&____DeviceName_3), value);
	}

	inline static int32_t get_offset_of__ModificationDate_4() { return static_cast<int32_t>(offsetof(GK_SavedGame_t3445071760, ____ModificationDate_4)); }
	inline DateTime_t3738529785  get__ModificationDate_4() const { return ____ModificationDate_4; }
	inline DateTime_t3738529785 * get_address_of__ModificationDate_4() { return &____ModificationDate_4; }
	inline void set__ModificationDate_4(DateTime_t3738529785  value)
	{
		____ModificationDate_4 = value;
	}

	inline static int32_t get_offset_of__OriginalDateString_5() { return static_cast<int32_t>(offsetof(GK_SavedGame_t3445071760, ____OriginalDateString_5)); }
	inline String_t* get__OriginalDateString_5() const { return ____OriginalDateString_5; }
	inline String_t** get_address_of__OriginalDateString_5() { return &____OriginalDateString_5; }
	inline void set__OriginalDateString_5(String_t* value)
	{
		____OriginalDateString_5 = value;
		Il2CppCodeGenWriteBarrier((&____OriginalDateString_5), value);
	}

	inline static int32_t get_offset_of__Data_6() { return static_cast<int32_t>(offsetof(GK_SavedGame_t3445071760, ____Data_6)); }
	inline ByteU5BU5D_t4116647657* get__Data_6() const { return ____Data_6; }
	inline ByteU5BU5D_t4116647657** get_address_of__Data_6() { return &____Data_6; }
	inline void set__Data_6(ByteU5BU5D_t4116647657* value)
	{
		____Data_6 = value;
		Il2CppCodeGenWriteBarrier((&____Data_6), value);
	}

	inline static int32_t get_offset_of__IsDataLoaded_7() { return static_cast<int32_t>(offsetof(GK_SavedGame_t3445071760, ____IsDataLoaded_7)); }
	inline bool get__IsDataLoaded_7() const { return ____IsDataLoaded_7; }
	inline bool* get_address_of__IsDataLoaded_7() { return &____IsDataLoaded_7; }
	inline void set__IsDataLoaded_7(bool value)
	{
		____IsDataLoaded_7 = value;
	}
};

struct GK_SavedGame_t3445071760_StaticFields
{
public:
	// System.Action`1<GK_SaveDataLoaded> GK_SavedGame::<>f__am$cache0
	Action_1_t244797016 * ___U3CU3Ef__amU24cache0_8;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_8() { return static_cast<int32_t>(offsetof(GK_SavedGame_t3445071760_StaticFields, ___U3CU3Ef__amU24cache0_8)); }
	inline Action_1_t244797016 * get_U3CU3Ef__amU24cache0_8() const { return ___U3CU3Ef__amU24cache0_8; }
	inline Action_1_t244797016 ** get_address_of_U3CU3Ef__amU24cache0_8() { return &___U3CU3Ef__amU24cache0_8; }
	inline void set_U3CU3Ef__amU24cache0_8(Action_1_t244797016 * value)
	{
		___U3CU3Ef__amU24cache0_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GK_SAVEDGAME_T3445071760_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef SINGLETON_1_T3123399374_H
#define SINGLETON_1_T3123399374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Pattern.Singleton`1<ISN_LocalNotificationsController>
struct  Singleton_1_t3123399374  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Singleton_1_t3123399374_StaticFields
{
public:
	// T SA.Common.Pattern.Singleton`1::_instance
	ISN_LocalNotificationsController_t3493627228 * ____instance_2;
	// System.Boolean SA.Common.Pattern.Singleton`1::applicationIsQuitting
	bool ___applicationIsQuitting_3;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t3123399374_StaticFields, ____instance_2)); }
	inline ISN_LocalNotificationsController_t3493627228 * get__instance_2() const { return ____instance_2; }
	inline ISN_LocalNotificationsController_t3493627228 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(ISN_LocalNotificationsController_t3493627228 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____instance_2), value);
	}

	inline static int32_t get_offset_of_applicationIsQuitting_3() { return static_cast<int32_t>(offsetof(Singleton_1_t3123399374_StaticFields, ___applicationIsQuitting_3)); }
	inline bool get_applicationIsQuitting_3() const { return ___applicationIsQuitting_3; }
	inline bool* get_address_of_applicationIsQuitting_3() { return &___applicationIsQuitting_3; }
	inline void set_applicationIsQuitting_3(bool value)
	{
		___applicationIsQuitting_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T3123399374_H
#ifndef SINGLETON_1_T1638862834_H
#define SINGLETON_1_T1638862834_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Pattern.Singleton`1<ISN_MediaController>
struct  Singleton_1_t1638862834  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Singleton_1_t1638862834_StaticFields
{
public:
	// T SA.Common.Pattern.Singleton`1::_instance
	ISN_MediaController_t2009090688 * ____instance_2;
	// System.Boolean SA.Common.Pattern.Singleton`1::applicationIsQuitting
	bool ___applicationIsQuitting_3;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t1638862834_StaticFields, ____instance_2)); }
	inline ISN_MediaController_t2009090688 * get__instance_2() const { return ____instance_2; }
	inline ISN_MediaController_t2009090688 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(ISN_MediaController_t2009090688 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____instance_2), value);
	}

	inline static int32_t get_offset_of_applicationIsQuitting_3() { return static_cast<int32_t>(offsetof(Singleton_1_t1638862834_StaticFields, ___applicationIsQuitting_3)); }
	inline bool get_applicationIsQuitting_3() const { return ___applicationIsQuitting_3; }
	inline bool* get_address_of_applicationIsQuitting_3() { return &___applicationIsQuitting_3; }
	inline void set_applicationIsQuitting_3(bool value)
	{
		___applicationIsQuitting_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T1638862834_H
#ifndef SINGLETON_1_T768985036_H
#define SINGLETON_1_T768985036_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Pattern.Singleton`1<SK_CloudService>
struct  Singleton_1_t768985036  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Singleton_1_t768985036_StaticFields
{
public:
	// T SA.Common.Pattern.Singleton`1::_instance
	SK_CloudService_t1139212890 * ____instance_2;
	// System.Boolean SA.Common.Pattern.Singleton`1::applicationIsQuitting
	bool ___applicationIsQuitting_3;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t768985036_StaticFields, ____instance_2)); }
	inline SK_CloudService_t1139212890 * get__instance_2() const { return ____instance_2; }
	inline SK_CloudService_t1139212890 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(SK_CloudService_t1139212890 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____instance_2), value);
	}

	inline static int32_t get_offset_of_applicationIsQuitting_3() { return static_cast<int32_t>(offsetof(Singleton_1_t768985036_StaticFields, ___applicationIsQuitting_3)); }
	inline bool get_applicationIsQuitting_3() const { return ___applicationIsQuitting_3; }
	inline bool* get_address_of_applicationIsQuitting_3() { return &___applicationIsQuitting_3; }
	inline void set_applicationIsQuitting_3(bool value)
	{
		___applicationIsQuitting_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T768985036_H
#ifndef SINGLETON_1_T1112944445_H
#define SINGLETON_1_T1112944445_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Pattern.Singleton`1<ISN_ReplayKit>
struct  Singleton_1_t1112944445  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Singleton_1_t1112944445_StaticFields
{
public:
	// T SA.Common.Pattern.Singleton`1::_instance
	ISN_ReplayKit_t1483172299 * ____instance_2;
	// System.Boolean SA.Common.Pattern.Singleton`1::applicationIsQuitting
	bool ___applicationIsQuitting_3;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t1112944445_StaticFields, ____instance_2)); }
	inline ISN_ReplayKit_t1483172299 * get__instance_2() const { return ____instance_2; }
	inline ISN_ReplayKit_t1483172299 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(ISN_ReplayKit_t1483172299 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____instance_2), value);
	}

	inline static int32_t get_offset_of_applicationIsQuitting_3() { return static_cast<int32_t>(offsetof(Singleton_1_t1112944445_StaticFields, ___applicationIsQuitting_3)); }
	inline bool get_applicationIsQuitting_3() const { return ___applicationIsQuitting_3; }
	inline bool* get_address_of_applicationIsQuitting_3() { return &___applicationIsQuitting_3; }
	inline void set_applicationIsQuitting_3(bool value)
	{
		___applicationIsQuitting_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T1112944445_H
#ifndef SINGLETON_1_T1195515451_H
#define SINGLETON_1_T1195515451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Pattern.Singleton`1<IOSSocialManager>
struct  Singleton_1_t1195515451  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Singleton_1_t1195515451_StaticFields
{
public:
	// T SA.Common.Pattern.Singleton`1::_instance
	IOSSocialManager_t1565743305 * ____instance_2;
	// System.Boolean SA.Common.Pattern.Singleton`1::applicationIsQuitting
	bool ___applicationIsQuitting_3;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t1195515451_StaticFields, ____instance_2)); }
	inline IOSSocialManager_t1565743305 * get__instance_2() const { return ____instance_2; }
	inline IOSSocialManager_t1565743305 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(IOSSocialManager_t1565743305 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____instance_2), value);
	}

	inline static int32_t get_offset_of_applicationIsQuitting_3() { return static_cast<int32_t>(offsetof(Singleton_1_t1195515451_StaticFields, ___applicationIsQuitting_3)); }
	inline bool get_applicationIsQuitting_3() const { return ___applicationIsQuitting_3; }
	inline bool* get_address_of_applicationIsQuitting_3() { return &___applicationIsQuitting_3; }
	inline void set_applicationIsQuitting_3(bool value)
	{
		___applicationIsQuitting_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T1195515451_H
#ifndef SINGLETON_1_T4173874569_H
#define SINGLETON_1_T4173874569_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Pattern.Singleton`1<SA.IOSNative.StoreKit.PaymentManager>
struct  Singleton_1_t4173874569  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Singleton_1_t4173874569_StaticFields
{
public:
	// T SA.Common.Pattern.Singleton`1::_instance
	PaymentManager_t249135127 * ____instance_2;
	// System.Boolean SA.Common.Pattern.Singleton`1::applicationIsQuitting
	bool ___applicationIsQuitting_3;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t4173874569_StaticFields, ____instance_2)); }
	inline PaymentManager_t249135127 * get__instance_2() const { return ____instance_2; }
	inline PaymentManager_t249135127 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(PaymentManager_t249135127 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____instance_2), value);
	}

	inline static int32_t get_offset_of_applicationIsQuitting_3() { return static_cast<int32_t>(offsetof(Singleton_1_t4173874569_StaticFields, ___applicationIsQuitting_3)); }
	inline bool get_applicationIsQuitting_3() const { return ___applicationIsQuitting_3; }
	inline bool* get_address_of_applicationIsQuitting_3() { return &___applicationIsQuitting_3; }
	inline void set_applicationIsQuitting_3(bool value)
	{
		___applicationIsQuitting_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T4173874569_H
#ifndef SINGLETON_1_T2558631638_H
#define SINGLETON_1_T2558631638_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Pattern.Singleton`1<ISN_RemoteNotificationsController>
struct  Singleton_1_t2558631638  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Singleton_1_t2558631638_StaticFields
{
public:
	// T SA.Common.Pattern.Singleton`1::_instance
	ISN_RemoteNotificationsController_t2928859492 * ____instance_2;
	// System.Boolean SA.Common.Pattern.Singleton`1::applicationIsQuitting
	bool ___applicationIsQuitting_3;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t2558631638_StaticFields, ____instance_2)); }
	inline ISN_RemoteNotificationsController_t2928859492 * get__instance_2() const { return ____instance_2; }
	inline ISN_RemoteNotificationsController_t2928859492 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(ISN_RemoteNotificationsController_t2928859492 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____instance_2), value);
	}

	inline static int32_t get_offset_of_applicationIsQuitting_3() { return static_cast<int32_t>(offsetof(Singleton_1_t2558631638_StaticFields, ___applicationIsQuitting_3)); }
	inline bool get_applicationIsQuitting_3() const { return ___applicationIsQuitting_3; }
	inline bool* get_address_of_applicationIsQuitting_3() { return &___applicationIsQuitting_3; }
	inline void set_applicationIsQuitting_3(bool value)
	{
		___applicationIsQuitting_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T2558631638_H
#ifndef SINGLETON_1_T2311705572_H
#define SINGLETON_1_T2311705572_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Pattern.Singleton`1<SA.IOSNative.UserNotifications.NativeReceiver>
struct  Singleton_1_t2311705572  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Singleton_1_t2311705572_StaticFields
{
public:
	// T SA.Common.Pattern.Singleton`1::_instance
	NativeReceiver_t2681933426 * ____instance_2;
	// System.Boolean SA.Common.Pattern.Singleton`1::applicationIsQuitting
	bool ___applicationIsQuitting_3;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t2311705572_StaticFields, ____instance_2)); }
	inline NativeReceiver_t2681933426 * get__instance_2() const { return ____instance_2; }
	inline NativeReceiver_t2681933426 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(NativeReceiver_t2681933426 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____instance_2), value);
	}

	inline static int32_t get_offset_of_applicationIsQuitting_3() { return static_cast<int32_t>(offsetof(Singleton_1_t2311705572_StaticFields, ___applicationIsQuitting_3)); }
	inline bool get_applicationIsQuitting_3() const { return ___applicationIsQuitting_3; }
	inline bool* get_address_of_applicationIsQuitting_3() { return &___applicationIsQuitting_3; }
	inline void set_applicationIsQuitting_3(bool value)
	{
		___applicationIsQuitting_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T2311705572_H
#ifndef SINGLETON_1_T2418163309_H
#define SINGLETON_1_T2418163309_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Pattern.Singleton`1<ISN_FilePicker>
struct  Singleton_1_t2418163309  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Singleton_1_t2418163309_StaticFields
{
public:
	// T SA.Common.Pattern.Singleton`1::_instance
	ISN_FilePicker_t2788391163 * ____instance_2;
	// System.Boolean SA.Common.Pattern.Singleton`1::applicationIsQuitting
	bool ___applicationIsQuitting_3;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t2418163309_StaticFields, ____instance_2)); }
	inline ISN_FilePicker_t2788391163 * get__instance_2() const { return ____instance_2; }
	inline ISN_FilePicker_t2788391163 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(ISN_FilePicker_t2788391163 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____instance_2), value);
	}

	inline static int32_t get_offset_of_applicationIsQuitting_3() { return static_cast<int32_t>(offsetof(Singleton_1_t2418163309_StaticFields, ___applicationIsQuitting_3)); }
	inline bool get_applicationIsQuitting_3() const { return ___applicationIsQuitting_3; }
	inline bool* get_address_of_applicationIsQuitting_3() { return &___applicationIsQuitting_3; }
	inline void set_applicationIsQuitting_3(bool value)
	{
		___applicationIsQuitting_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T2418163309_H
#ifndef BASEIOSPOPUP_T1731871609_H
#define BASEIOSPOPUP_T1731871609_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BaseIOSPopup
struct  BaseIOSPopup_t1731871609  : public MonoBehaviour_t3962482529
{
public:
	// System.String BaseIOSPopup::title
	String_t* ___title_2;
	// System.String BaseIOSPopup::message
	String_t* ___message_3;

public:
	inline static int32_t get_offset_of_title_2() { return static_cast<int32_t>(offsetof(BaseIOSPopup_t1731871609, ___title_2)); }
	inline String_t* get_title_2() const { return ___title_2; }
	inline String_t** get_address_of_title_2() { return &___title_2; }
	inline void set_title_2(String_t* value)
	{
		___title_2 = value;
		Il2CppCodeGenWriteBarrier((&___title_2), value);
	}

	inline static int32_t get_offset_of_message_3() { return static_cast<int32_t>(offsetof(BaseIOSPopup_t1731871609, ___message_3)); }
	inline String_t* get_message_3() const { return ___message_3; }
	inline String_t** get_address_of_message_3() { return &___message_3; }
	inline void set_message_3(String_t* value)
	{
		___message_3 = value;
		Il2CppCodeGenWriteBarrier((&___message_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEIOSPOPUP_T1731871609_H
#ifndef SINGLETON_1_T1556764101_H
#define SINGLETON_1_T1556764101_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Pattern.Singleton`1<IOSVideoManager>
struct  Singleton_1_t1556764101  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Singleton_1_t1556764101_StaticFields
{
public:
	// T SA.Common.Pattern.Singleton`1::_instance
	IOSVideoManager_t1926991955 * ____instance_2;
	// System.Boolean SA.Common.Pattern.Singleton`1::applicationIsQuitting
	bool ___applicationIsQuitting_3;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t1556764101_StaticFields, ____instance_2)); }
	inline IOSVideoManager_t1926991955 * get__instance_2() const { return ____instance_2; }
	inline IOSVideoManager_t1926991955 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(IOSVideoManager_t1926991955 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____instance_2), value);
	}

	inline static int32_t get_offset_of_applicationIsQuitting_3() { return static_cast<int32_t>(offsetof(Singleton_1_t1556764101_StaticFields, ___applicationIsQuitting_3)); }
	inline bool get_applicationIsQuitting_3() const { return ___applicationIsQuitting_3; }
	inline bool* get_address_of_applicationIsQuitting_3() { return &___applicationIsQuitting_3; }
	inline void set_applicationIsQuitting_3(bool value)
	{
		___applicationIsQuitting_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T1556764101_H
#ifndef SINGLETON_1_T2507718151_H
#define SINGLETON_1_T2507718151_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Pattern.Singleton`1<iCloudManager>
struct  Singleton_1_t2507718151  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Singleton_1_t2507718151_StaticFields
{
public:
	// T SA.Common.Pattern.Singleton`1::_instance
	iCloudManager_t2877946005 * ____instance_2;
	// System.Boolean SA.Common.Pattern.Singleton`1::applicationIsQuitting
	bool ___applicationIsQuitting_3;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t2507718151_StaticFields, ____instance_2)); }
	inline iCloudManager_t2877946005 * get__instance_2() const { return ____instance_2; }
	inline iCloudManager_t2877946005 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(iCloudManager_t2877946005 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____instance_2), value);
	}

	inline static int32_t get_offset_of_applicationIsQuitting_3() { return static_cast<int32_t>(offsetof(Singleton_1_t2507718151_StaticFields, ___applicationIsQuitting_3)); }
	inline bool get_applicationIsQuitting_3() const { return ___applicationIsQuitting_3; }
	inline bool* get_address_of_applicationIsQuitting_3() { return &___applicationIsQuitting_3; }
	inline void set_applicationIsQuitting_3(bool value)
	{
		___applicationIsQuitting_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T2507718151_H
#ifndef SINGLETON_1_T1654327225_H
#define SINGLETON_1_T1654327225_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Pattern.Singleton`1<IOSCamera>
struct  Singleton_1_t1654327225  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Singleton_1_t1654327225_StaticFields
{
public:
	// T SA.Common.Pattern.Singleton`1::_instance
	IOSCamera_t2024555079 * ____instance_2;
	// System.Boolean SA.Common.Pattern.Singleton`1::applicationIsQuitting
	bool ___applicationIsQuitting_3;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t1654327225_StaticFields, ____instance_2)); }
	inline IOSCamera_t2024555079 * get__instance_2() const { return ____instance_2; }
	inline IOSCamera_t2024555079 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(IOSCamera_t2024555079 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____instance_2), value);
	}

	inline static int32_t get_offset_of_applicationIsQuitting_3() { return static_cast<int32_t>(offsetof(Singleton_1_t1654327225_StaticFields, ___applicationIsQuitting_3)); }
	inline bool get_applicationIsQuitting_3() const { return ___applicationIsQuitting_3; }
	inline bool* get_address_of_applicationIsQuitting_3() { return &___applicationIsQuitting_3; }
	inline void set_applicationIsQuitting_3(bool value)
	{
		___applicationIsQuitting_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T1654327225_H
#ifndef SINGLETON_1_T2764435424_H
#define SINGLETON_1_T2764435424_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Pattern.Singleton`1<ISN_CloudKit>
struct  Singleton_1_t2764435424  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Singleton_1_t2764435424_StaticFields
{
public:
	// T SA.Common.Pattern.Singleton`1::_instance
	ISN_CloudKit_t3134663278 * ____instance_2;
	// System.Boolean SA.Common.Pattern.Singleton`1::applicationIsQuitting
	bool ___applicationIsQuitting_3;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t2764435424_StaticFields, ____instance_2)); }
	inline ISN_CloudKit_t3134663278 * get__instance_2() const { return ____instance_2; }
	inline ISN_CloudKit_t3134663278 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(ISN_CloudKit_t3134663278 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____instance_2), value);
	}

	inline static int32_t get_offset_of_applicationIsQuitting_3() { return static_cast<int32_t>(offsetof(Singleton_1_t2764435424_StaticFields, ___applicationIsQuitting_3)); }
	inline bool get_applicationIsQuitting_3() const { return ___applicationIsQuitting_3; }
	inline bool* get_address_of_applicationIsQuitting_3() { return &___applicationIsQuitting_3; }
	inline void set_applicationIsQuitting_3(bool value)
	{
		___applicationIsQuitting_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T2764435424_H
#ifndef ISN_FILEPICKER_T2788391163_H
#define ISN_FILEPICKER_T2788391163_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ISN_FilePicker
struct  ISN_FilePicker_t2788391163  : public Singleton_1_t2418163309
{
public:

public:
};

struct ISN_FilePicker_t2788391163_StaticFields
{
public:
	// System.Action`1<ISN_FilePickerResult> ISN_FilePicker::MediaPickFinished
	Action_1_t2655722685 * ___MediaPickFinished_4;

public:
	inline static int32_t get_offset_of_MediaPickFinished_4() { return static_cast<int32_t>(offsetof(ISN_FilePicker_t2788391163_StaticFields, ___MediaPickFinished_4)); }
	inline Action_1_t2655722685 * get_MediaPickFinished_4() const { return ___MediaPickFinished_4; }
	inline Action_1_t2655722685 ** get_address_of_MediaPickFinished_4() { return &___MediaPickFinished_4; }
	inline void set_MediaPickFinished_4(Action_1_t2655722685 * value)
	{
		___MediaPickFinished_4 = value;
		Il2CppCodeGenWriteBarrier((&___MediaPickFinished_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISN_FILEPICKER_T2788391163_H
#ifndef IOSRATEUSPOPUP_T2981493649_H
#define IOSRATEUSPOPUP_T2981493649_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IOSRateUsPopUp
struct  IOSRateUsPopUp_t2981493649  : public BaseIOSPopup_t1731871609
{
public:
	// System.String IOSRateUsPopUp::rate
	String_t* ___rate_4;
	// System.String IOSRateUsPopUp::remind
	String_t* ___remind_5;
	// System.String IOSRateUsPopUp::declined
	String_t* ___declined_6;
	// System.Action`1<IOSDialogResult> IOSRateUsPopUp::OnComplete
	Action_1_t416443874 * ___OnComplete_7;

public:
	inline static int32_t get_offset_of_rate_4() { return static_cast<int32_t>(offsetof(IOSRateUsPopUp_t2981493649, ___rate_4)); }
	inline String_t* get_rate_4() const { return ___rate_4; }
	inline String_t** get_address_of_rate_4() { return &___rate_4; }
	inline void set_rate_4(String_t* value)
	{
		___rate_4 = value;
		Il2CppCodeGenWriteBarrier((&___rate_4), value);
	}

	inline static int32_t get_offset_of_remind_5() { return static_cast<int32_t>(offsetof(IOSRateUsPopUp_t2981493649, ___remind_5)); }
	inline String_t* get_remind_5() const { return ___remind_5; }
	inline String_t** get_address_of_remind_5() { return &___remind_5; }
	inline void set_remind_5(String_t* value)
	{
		___remind_5 = value;
		Il2CppCodeGenWriteBarrier((&___remind_5), value);
	}

	inline static int32_t get_offset_of_declined_6() { return static_cast<int32_t>(offsetof(IOSRateUsPopUp_t2981493649, ___declined_6)); }
	inline String_t* get_declined_6() const { return ___declined_6; }
	inline String_t** get_address_of_declined_6() { return &___declined_6; }
	inline void set_declined_6(String_t* value)
	{
		___declined_6 = value;
		Il2CppCodeGenWriteBarrier((&___declined_6), value);
	}

	inline static int32_t get_offset_of_OnComplete_7() { return static_cast<int32_t>(offsetof(IOSRateUsPopUp_t2981493649, ___OnComplete_7)); }
	inline Action_1_t416443874 * get_OnComplete_7() const { return ___OnComplete_7; }
	inline Action_1_t416443874 ** get_address_of_OnComplete_7() { return &___OnComplete_7; }
	inline void set_OnComplete_7(Action_1_t416443874 * value)
	{
		___OnComplete_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnComplete_7), value);
	}
};

struct IOSRateUsPopUp_t2981493649_StaticFields
{
public:
	// System.Action`1<IOSDialogResult> IOSRateUsPopUp::<>f__am$cache0
	Action_1_t416443874 * ___U3CU3Ef__amU24cache0_8;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_8() { return static_cast<int32_t>(offsetof(IOSRateUsPopUp_t2981493649_StaticFields, ___U3CU3Ef__amU24cache0_8)); }
	inline Action_1_t416443874 * get_U3CU3Ef__amU24cache0_8() const { return ___U3CU3Ef__amU24cache0_8; }
	inline Action_1_t416443874 ** get_address_of_U3CU3Ef__amU24cache0_8() { return &___U3CU3Ef__amU24cache0_8; }
	inline void set_U3CU3Ef__amU24cache0_8(Action_1_t416443874 * value)
	{
		___U3CU3Ef__amU24cache0_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSRATEUSPOPUP_T2981493649_H
#ifndef IOSCAMERA_T2024555079_H
#define IOSCAMERA_T2024555079_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IOSCamera
struct  IOSCamera_t2024555079  : public Singleton_1_t1654327225
{
public:
	// System.Boolean IOSCamera::_IsWaitngForResponce
	bool ____IsWaitngForResponce_7;
	// System.Boolean IOSCamera::_IsInitialized
	bool ____IsInitialized_8;

public:
	inline static int32_t get_offset_of__IsWaitngForResponce_7() { return static_cast<int32_t>(offsetof(IOSCamera_t2024555079, ____IsWaitngForResponce_7)); }
	inline bool get__IsWaitngForResponce_7() const { return ____IsWaitngForResponce_7; }
	inline bool* get_address_of__IsWaitngForResponce_7() { return &____IsWaitngForResponce_7; }
	inline void set__IsWaitngForResponce_7(bool value)
	{
		____IsWaitngForResponce_7 = value;
	}

	inline static int32_t get_offset_of__IsInitialized_8() { return static_cast<int32_t>(offsetof(IOSCamera_t2024555079, ____IsInitialized_8)); }
	inline bool get__IsInitialized_8() const { return ____IsInitialized_8; }
	inline bool* get_address_of__IsInitialized_8() { return &____IsInitialized_8; }
	inline void set__IsInitialized_8(bool value)
	{
		____IsInitialized_8 = value;
	}
};

struct IOSCamera_t2024555079_StaticFields
{
public:
	// System.Action`1<IOSImagePickResult> IOSCamera::OnImagePicked
	Action_1_t4292612745 * ___OnImagePicked_4;
	// System.Action`1<SA.Common.Models.Result> IOSCamera::OnImageSaved
	Action_1_t47902017 * ___OnImageSaved_5;
	// System.Action`1<System.String> IOSCamera::OnVideoPathPicked
	Action_1_t2019918284 * ___OnVideoPathPicked_6;

public:
	inline static int32_t get_offset_of_OnImagePicked_4() { return static_cast<int32_t>(offsetof(IOSCamera_t2024555079_StaticFields, ___OnImagePicked_4)); }
	inline Action_1_t4292612745 * get_OnImagePicked_4() const { return ___OnImagePicked_4; }
	inline Action_1_t4292612745 ** get_address_of_OnImagePicked_4() { return &___OnImagePicked_4; }
	inline void set_OnImagePicked_4(Action_1_t4292612745 * value)
	{
		___OnImagePicked_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnImagePicked_4), value);
	}

	inline static int32_t get_offset_of_OnImageSaved_5() { return static_cast<int32_t>(offsetof(IOSCamera_t2024555079_StaticFields, ___OnImageSaved_5)); }
	inline Action_1_t47902017 * get_OnImageSaved_5() const { return ___OnImageSaved_5; }
	inline Action_1_t47902017 ** get_address_of_OnImageSaved_5() { return &___OnImageSaved_5; }
	inline void set_OnImageSaved_5(Action_1_t47902017 * value)
	{
		___OnImageSaved_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnImageSaved_5), value);
	}

	inline static int32_t get_offset_of_OnVideoPathPicked_6() { return static_cast<int32_t>(offsetof(IOSCamera_t2024555079_StaticFields, ___OnVideoPathPicked_6)); }
	inline Action_1_t2019918284 * get_OnVideoPathPicked_6() const { return ___OnVideoPathPicked_6; }
	inline Action_1_t2019918284 ** get_address_of_OnVideoPathPicked_6() { return &___OnVideoPathPicked_6; }
	inline void set_OnVideoPathPicked_6(Action_1_t2019918284 * value)
	{
		___OnVideoPathPicked_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnVideoPathPicked_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSCAMERA_T2024555079_H
#ifndef ISN_CLOUDKIT_T3134663278_H
#define ISN_CLOUDKIT_T3134663278_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ISN_CloudKit
struct  ISN_CloudKit_t3134663278  : public Singleton_1_t2764435424
{
public:
	// CK_Database ISN_CloudKit::_PrivateDB
	CK_Database_t2819592330 * ____PrivateDB_4;
	// CK_Database ISN_CloudKit::_PublicDB
	CK_Database_t2819592330 * ____PublicDB_5;

public:
	inline static int32_t get_offset_of__PrivateDB_4() { return static_cast<int32_t>(offsetof(ISN_CloudKit_t3134663278, ____PrivateDB_4)); }
	inline CK_Database_t2819592330 * get__PrivateDB_4() const { return ____PrivateDB_4; }
	inline CK_Database_t2819592330 ** get_address_of__PrivateDB_4() { return &____PrivateDB_4; }
	inline void set__PrivateDB_4(CK_Database_t2819592330 * value)
	{
		____PrivateDB_4 = value;
		Il2CppCodeGenWriteBarrier((&____PrivateDB_4), value);
	}

	inline static int32_t get_offset_of__PublicDB_5() { return static_cast<int32_t>(offsetof(ISN_CloudKit_t3134663278, ____PublicDB_5)); }
	inline CK_Database_t2819592330 * get__PublicDB_5() const { return ____PublicDB_5; }
	inline CK_Database_t2819592330 ** get_address_of__PublicDB_5() { return &____PublicDB_5; }
	inline void set__PublicDB_5(CK_Database_t2819592330 * value)
	{
		____PublicDB_5 = value;
		Il2CppCodeGenWriteBarrier((&____PublicDB_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISN_CLOUDKIT_T3134663278_H
#ifndef ICLOUDMANAGER_T2877946005_H
#define ICLOUDMANAGER_T2877946005_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iCloudManager
struct  iCloudManager_t2877946005  : public Singleton_1_t2507718151
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.Action`1<iCloudData>>> iCloudManager::s_requestDataCallbacks
	Dictionary_2_t1524604538 * ___s_requestDataCallbacks_7;

public:
	inline static int32_t get_offset_of_s_requestDataCallbacks_7() { return static_cast<int32_t>(offsetof(iCloudManager_t2877946005, ___s_requestDataCallbacks_7)); }
	inline Dictionary_2_t1524604538 * get_s_requestDataCallbacks_7() const { return ___s_requestDataCallbacks_7; }
	inline Dictionary_2_t1524604538 ** get_address_of_s_requestDataCallbacks_7() { return &___s_requestDataCallbacks_7; }
	inline void set_s_requestDataCallbacks_7(Dictionary_2_t1524604538 * value)
	{
		___s_requestDataCallbacks_7 = value;
		Il2CppCodeGenWriteBarrier((&___s_requestDataCallbacks_7), value);
	}
};

struct iCloudManager_t2877946005_StaticFields
{
public:
	// System.Action`1<SA.Common.Models.Result> iCloudManager::OnCloudInitAction
	Action_1_t47902017 * ___OnCloudInitAction_4;
	// System.Action`1<iCloudData> iCloudManager::OnCloudDataReceivedAction
	Action_1_t267273497 * ___OnCloudDataReceivedAction_5;
	// System.Action`1<System.Collections.Generic.List`1<iCloudData>> iCloudManager::OnStoreDidChangeExternally
	Action_1_t1739348239 * ___OnStoreDidChangeExternally_6;

public:
	inline static int32_t get_offset_of_OnCloudInitAction_4() { return static_cast<int32_t>(offsetof(iCloudManager_t2877946005_StaticFields, ___OnCloudInitAction_4)); }
	inline Action_1_t47902017 * get_OnCloudInitAction_4() const { return ___OnCloudInitAction_4; }
	inline Action_1_t47902017 ** get_address_of_OnCloudInitAction_4() { return &___OnCloudInitAction_4; }
	inline void set_OnCloudInitAction_4(Action_1_t47902017 * value)
	{
		___OnCloudInitAction_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnCloudInitAction_4), value);
	}

	inline static int32_t get_offset_of_OnCloudDataReceivedAction_5() { return static_cast<int32_t>(offsetof(iCloudManager_t2877946005_StaticFields, ___OnCloudDataReceivedAction_5)); }
	inline Action_1_t267273497 * get_OnCloudDataReceivedAction_5() const { return ___OnCloudDataReceivedAction_5; }
	inline Action_1_t267273497 ** get_address_of_OnCloudDataReceivedAction_5() { return &___OnCloudDataReceivedAction_5; }
	inline void set_OnCloudDataReceivedAction_5(Action_1_t267273497 * value)
	{
		___OnCloudDataReceivedAction_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnCloudDataReceivedAction_5), value);
	}

	inline static int32_t get_offset_of_OnStoreDidChangeExternally_6() { return static_cast<int32_t>(offsetof(iCloudManager_t2877946005_StaticFields, ___OnStoreDidChangeExternally_6)); }
	inline Action_1_t1739348239 * get_OnStoreDidChangeExternally_6() const { return ___OnStoreDidChangeExternally_6; }
	inline Action_1_t1739348239 ** get_address_of_OnStoreDidChangeExternally_6() { return &___OnStoreDidChangeExternally_6; }
	inline void set_OnStoreDidChangeExternally_6(Action_1_t1739348239 * value)
	{
		___OnStoreDidChangeExternally_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnStoreDidChangeExternally_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ICLOUDMANAGER_T2877946005_H
#ifndef IOSMESSAGE_T148091823_H
#define IOSMESSAGE_T148091823_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IOSMessage
struct  IOSMessage_t148091823  : public BaseIOSPopup_t1731871609
{
public:
	// System.String IOSMessage::ok
	String_t* ___ok_4;
	// System.Action IOSMessage::OnComplete
	Action_t1264377477 * ___OnComplete_5;

public:
	inline static int32_t get_offset_of_ok_4() { return static_cast<int32_t>(offsetof(IOSMessage_t148091823, ___ok_4)); }
	inline String_t* get_ok_4() const { return ___ok_4; }
	inline String_t** get_address_of_ok_4() { return &___ok_4; }
	inline void set_ok_4(String_t* value)
	{
		___ok_4 = value;
		Il2CppCodeGenWriteBarrier((&___ok_4), value);
	}

	inline static int32_t get_offset_of_OnComplete_5() { return static_cast<int32_t>(offsetof(IOSMessage_t148091823, ___OnComplete_5)); }
	inline Action_t1264377477 * get_OnComplete_5() const { return ___OnComplete_5; }
	inline Action_t1264377477 ** get_address_of_OnComplete_5() { return &___OnComplete_5; }
	inline void set_OnComplete_5(Action_t1264377477 * value)
	{
		___OnComplete_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnComplete_5), value);
	}
};

struct IOSMessage_t148091823_StaticFields
{
public:
	// System.Action IOSMessage::<>f__am$cache0
	Action_t1264377477 * ___U3CU3Ef__amU24cache0_6;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_6() { return static_cast<int32_t>(offsetof(IOSMessage_t148091823_StaticFields, ___U3CU3Ef__amU24cache0_6)); }
	inline Action_t1264377477 * get_U3CU3Ef__amU24cache0_6() const { return ___U3CU3Ef__amU24cache0_6; }
	inline Action_t1264377477 ** get_address_of_U3CU3Ef__amU24cache0_6() { return &___U3CU3Ef__amU24cache0_6; }
	inline void set_U3CU3Ef__amU24cache0_6(Action_t1264377477 * value)
	{
		___U3CU3Ef__amU24cache0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSMESSAGE_T148091823_H
#ifndef IOSDIALOG_T662133956_H
#define IOSDIALOG_T662133956_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IOSDialog
struct  IOSDialog_t662133956  : public BaseIOSPopup_t1731871609
{
public:
	// System.String IOSDialog::yes
	String_t* ___yes_4;
	// System.String IOSDialog::no
	String_t* ___no_5;
	// System.Action`1<IOSDialogResult> IOSDialog::OnComplete
	Action_1_t416443874 * ___OnComplete_6;

public:
	inline static int32_t get_offset_of_yes_4() { return static_cast<int32_t>(offsetof(IOSDialog_t662133956, ___yes_4)); }
	inline String_t* get_yes_4() const { return ___yes_4; }
	inline String_t** get_address_of_yes_4() { return &___yes_4; }
	inline void set_yes_4(String_t* value)
	{
		___yes_4 = value;
		Il2CppCodeGenWriteBarrier((&___yes_4), value);
	}

	inline static int32_t get_offset_of_no_5() { return static_cast<int32_t>(offsetof(IOSDialog_t662133956, ___no_5)); }
	inline String_t* get_no_5() const { return ___no_5; }
	inline String_t** get_address_of_no_5() { return &___no_5; }
	inline void set_no_5(String_t* value)
	{
		___no_5 = value;
		Il2CppCodeGenWriteBarrier((&___no_5), value);
	}

	inline static int32_t get_offset_of_OnComplete_6() { return static_cast<int32_t>(offsetof(IOSDialog_t662133956, ___OnComplete_6)); }
	inline Action_1_t416443874 * get_OnComplete_6() const { return ___OnComplete_6; }
	inline Action_1_t416443874 ** get_address_of_OnComplete_6() { return &___OnComplete_6; }
	inline void set_OnComplete_6(Action_1_t416443874 * value)
	{
		___OnComplete_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnComplete_6), value);
	}
};

struct IOSDialog_t662133956_StaticFields
{
public:
	// System.Action`1<IOSDialogResult> IOSDialog::<>f__am$cache0
	Action_1_t416443874 * ___U3CU3Ef__amU24cache0_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_7() { return static_cast<int32_t>(offsetof(IOSDialog_t662133956_StaticFields, ___U3CU3Ef__amU24cache0_7)); }
	inline Action_1_t416443874 * get_U3CU3Ef__amU24cache0_7() const { return ___U3CU3Ef__amU24cache0_7; }
	inline Action_1_t416443874 ** get_address_of_U3CU3Ef__amU24cache0_7() { return &___U3CU3Ef__amU24cache0_7; }
	inline void set_U3CU3Ef__amU24cache0_7(Action_1_t416443874 * value)
	{
		___U3CU3Ef__amU24cache0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSDIALOG_T662133956_H
#ifndef NATIVERECEIVER_T2681933426_H
#define NATIVERECEIVER_T2681933426_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSNative.UserNotifications.NativeReceiver
struct  NativeReceiver_t2681933426  : public Singleton_1_t2311705572
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVERECEIVER_T2681933426_H
#ifndef PAYMENTMANAGER_T249135127_H
#define PAYMENTMANAGER_T249135127_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSNative.StoreKit.PaymentManager
struct  PaymentManager_t249135127  : public Singleton_1_t4173874569
{
public:
	// System.Boolean SA.IOSNative.StoreKit.PaymentManager::_IsStoreLoaded
	bool ____IsStoreLoaded_13;
	// System.Boolean SA.IOSNative.StoreKit.PaymentManager::_IsWaitingLoadResult
	bool ____IsWaitingLoadResult_14;
	// System.Collections.Generic.Dictionary`2<System.Int32,SA.IOSNative.StoreKit.StoreProductView> SA.IOSNative.StoreKit.PaymentManager::_productsView
	Dictionary_2_t1460844201 * ____productsView_16;

public:
	inline static int32_t get_offset_of__IsStoreLoaded_13() { return static_cast<int32_t>(offsetof(PaymentManager_t249135127, ____IsStoreLoaded_13)); }
	inline bool get__IsStoreLoaded_13() const { return ____IsStoreLoaded_13; }
	inline bool* get_address_of__IsStoreLoaded_13() { return &____IsStoreLoaded_13; }
	inline void set__IsStoreLoaded_13(bool value)
	{
		____IsStoreLoaded_13 = value;
	}

	inline static int32_t get_offset_of__IsWaitingLoadResult_14() { return static_cast<int32_t>(offsetof(PaymentManager_t249135127, ____IsWaitingLoadResult_14)); }
	inline bool get__IsWaitingLoadResult_14() const { return ____IsWaitingLoadResult_14; }
	inline bool* get_address_of__IsWaitingLoadResult_14() { return &____IsWaitingLoadResult_14; }
	inline void set__IsWaitingLoadResult_14(bool value)
	{
		____IsWaitingLoadResult_14 = value;
	}

	inline static int32_t get_offset_of__productsView_16() { return static_cast<int32_t>(offsetof(PaymentManager_t249135127, ____productsView_16)); }
	inline Dictionary_2_t1460844201 * get__productsView_16() const { return ____productsView_16; }
	inline Dictionary_2_t1460844201 ** get_address_of__productsView_16() { return &____productsView_16; }
	inline void set__productsView_16(Dictionary_2_t1460844201 * value)
	{
		____productsView_16 = value;
		Il2CppCodeGenWriteBarrier((&____productsView_16), value);
	}
};

struct PaymentManager_t249135127_StaticFields
{
public:
	// System.Action`1<SA.Common.Models.Result> SA.IOSNative.StoreKit.PaymentManager::OnStoreKitInitComplete
	Action_1_t47902017 * ___OnStoreKitInitComplete_6;
	// System.Action SA.IOSNative.StoreKit.PaymentManager::OnRestoreStarted
	Action_t1264377477 * ___OnRestoreStarted_7;
	// System.Action`1<SA.IOSNative.StoreKit.RestoreResult> SA.IOSNative.StoreKit.PaymentManager::OnRestoreComplete
	Action_1_t1653158450 * ___OnRestoreComplete_8;
	// System.Action`1<System.String> SA.IOSNative.StoreKit.PaymentManager::OnTransactionStarted
	Action_1_t2019918284 * ___OnTransactionStarted_9;
	// System.Action`1<SA.IOSNative.StoreKit.PurchaseResult> SA.IOSNative.StoreKit.PaymentManager::OnTransactionComplete
	Action_1_t3399145967 * ___OnTransactionComplete_10;
	// System.Action`1<System.String> SA.IOSNative.StoreKit.PaymentManager::OnProductPurchasedExternally
	Action_1_t2019918284 * ___OnProductPurchasedExternally_11;
	// System.Action`1<SA.IOSNative.StoreKit.VerificationResponse> SA.IOSNative.StoreKit.PaymentManager::OnVerificationComplete
	Action_1_t3441667023 * ___OnVerificationComplete_12;
	// System.Int32 SA.IOSNative.StoreKit.PaymentManager::_nextId
	int32_t ____nextId_15;
	// System.String SA.IOSNative.StoreKit.PaymentManager::lastPurchasedProduct
	String_t* ___lastPurchasedProduct_17;

public:
	inline static int32_t get_offset_of_OnStoreKitInitComplete_6() { return static_cast<int32_t>(offsetof(PaymentManager_t249135127_StaticFields, ___OnStoreKitInitComplete_6)); }
	inline Action_1_t47902017 * get_OnStoreKitInitComplete_6() const { return ___OnStoreKitInitComplete_6; }
	inline Action_1_t47902017 ** get_address_of_OnStoreKitInitComplete_6() { return &___OnStoreKitInitComplete_6; }
	inline void set_OnStoreKitInitComplete_6(Action_1_t47902017 * value)
	{
		___OnStoreKitInitComplete_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnStoreKitInitComplete_6), value);
	}

	inline static int32_t get_offset_of_OnRestoreStarted_7() { return static_cast<int32_t>(offsetof(PaymentManager_t249135127_StaticFields, ___OnRestoreStarted_7)); }
	inline Action_t1264377477 * get_OnRestoreStarted_7() const { return ___OnRestoreStarted_7; }
	inline Action_t1264377477 ** get_address_of_OnRestoreStarted_7() { return &___OnRestoreStarted_7; }
	inline void set_OnRestoreStarted_7(Action_t1264377477 * value)
	{
		___OnRestoreStarted_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnRestoreStarted_7), value);
	}

	inline static int32_t get_offset_of_OnRestoreComplete_8() { return static_cast<int32_t>(offsetof(PaymentManager_t249135127_StaticFields, ___OnRestoreComplete_8)); }
	inline Action_1_t1653158450 * get_OnRestoreComplete_8() const { return ___OnRestoreComplete_8; }
	inline Action_1_t1653158450 ** get_address_of_OnRestoreComplete_8() { return &___OnRestoreComplete_8; }
	inline void set_OnRestoreComplete_8(Action_1_t1653158450 * value)
	{
		___OnRestoreComplete_8 = value;
		Il2CppCodeGenWriteBarrier((&___OnRestoreComplete_8), value);
	}

	inline static int32_t get_offset_of_OnTransactionStarted_9() { return static_cast<int32_t>(offsetof(PaymentManager_t249135127_StaticFields, ___OnTransactionStarted_9)); }
	inline Action_1_t2019918284 * get_OnTransactionStarted_9() const { return ___OnTransactionStarted_9; }
	inline Action_1_t2019918284 ** get_address_of_OnTransactionStarted_9() { return &___OnTransactionStarted_9; }
	inline void set_OnTransactionStarted_9(Action_1_t2019918284 * value)
	{
		___OnTransactionStarted_9 = value;
		Il2CppCodeGenWriteBarrier((&___OnTransactionStarted_9), value);
	}

	inline static int32_t get_offset_of_OnTransactionComplete_10() { return static_cast<int32_t>(offsetof(PaymentManager_t249135127_StaticFields, ___OnTransactionComplete_10)); }
	inline Action_1_t3399145967 * get_OnTransactionComplete_10() const { return ___OnTransactionComplete_10; }
	inline Action_1_t3399145967 ** get_address_of_OnTransactionComplete_10() { return &___OnTransactionComplete_10; }
	inline void set_OnTransactionComplete_10(Action_1_t3399145967 * value)
	{
		___OnTransactionComplete_10 = value;
		Il2CppCodeGenWriteBarrier((&___OnTransactionComplete_10), value);
	}

	inline static int32_t get_offset_of_OnProductPurchasedExternally_11() { return static_cast<int32_t>(offsetof(PaymentManager_t249135127_StaticFields, ___OnProductPurchasedExternally_11)); }
	inline Action_1_t2019918284 * get_OnProductPurchasedExternally_11() const { return ___OnProductPurchasedExternally_11; }
	inline Action_1_t2019918284 ** get_address_of_OnProductPurchasedExternally_11() { return &___OnProductPurchasedExternally_11; }
	inline void set_OnProductPurchasedExternally_11(Action_1_t2019918284 * value)
	{
		___OnProductPurchasedExternally_11 = value;
		Il2CppCodeGenWriteBarrier((&___OnProductPurchasedExternally_11), value);
	}

	inline static int32_t get_offset_of_OnVerificationComplete_12() { return static_cast<int32_t>(offsetof(PaymentManager_t249135127_StaticFields, ___OnVerificationComplete_12)); }
	inline Action_1_t3441667023 * get_OnVerificationComplete_12() const { return ___OnVerificationComplete_12; }
	inline Action_1_t3441667023 ** get_address_of_OnVerificationComplete_12() { return &___OnVerificationComplete_12; }
	inline void set_OnVerificationComplete_12(Action_1_t3441667023 * value)
	{
		___OnVerificationComplete_12 = value;
		Il2CppCodeGenWriteBarrier((&___OnVerificationComplete_12), value);
	}

	inline static int32_t get_offset_of__nextId_15() { return static_cast<int32_t>(offsetof(PaymentManager_t249135127_StaticFields, ____nextId_15)); }
	inline int32_t get__nextId_15() const { return ____nextId_15; }
	inline int32_t* get_address_of__nextId_15() { return &____nextId_15; }
	inline void set__nextId_15(int32_t value)
	{
		____nextId_15 = value;
	}

	inline static int32_t get_offset_of_lastPurchasedProduct_17() { return static_cast<int32_t>(offsetof(PaymentManager_t249135127_StaticFields, ___lastPurchasedProduct_17)); }
	inline String_t* get_lastPurchasedProduct_17() const { return ___lastPurchasedProduct_17; }
	inline String_t** get_address_of_lastPurchasedProduct_17() { return &___lastPurchasedProduct_17; }
	inline void set_lastPurchasedProduct_17(String_t* value)
	{
		___lastPurchasedProduct_17 = value;
		Il2CppCodeGenWriteBarrier((&___lastPurchasedProduct_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAYMENTMANAGER_T249135127_H
#ifndef IOSSOCIALMANAGER_T1565743305_H
#define IOSSOCIALMANAGER_T1565743305_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IOSSocialManager
struct  IOSSocialManager_t1565743305  : public Singleton_1_t1195515451
{
public:

public:
};

struct IOSSocialManager_t1565743305_StaticFields
{
public:
	// System.Action IOSSocialManager::OnFacebookPostStart
	Action_t1264377477 * ___OnFacebookPostStart_4;
	// System.Action IOSSocialManager::OnTwitterPostStart
	Action_t1264377477 * ___OnTwitterPostStart_5;
	// System.Action IOSSocialManager::OnInstagramPostStart
	Action_t1264377477 * ___OnInstagramPostStart_6;
	// System.Action`1<SA.Common.Models.Result> IOSSocialManager::OnFacebookPostResult
	Action_1_t47902017 * ___OnFacebookPostResult_7;
	// System.Action`1<SA.Common.Models.Result> IOSSocialManager::OnTwitterPostResult
	Action_1_t47902017 * ___OnTwitterPostResult_8;
	// System.Action`1<SA.Common.Models.Result> IOSSocialManager::OnInstagramPostResult
	Action_1_t47902017 * ___OnInstagramPostResult_9;
	// System.Action`1<SA.Common.Models.Result> IOSSocialManager::OnMailResult
	Action_1_t47902017 * ___OnMailResult_10;
	// System.Action`1<TextMessageComposeResult> IOSSocialManager::OnTextMessageResult
	Action_1_t1565920682 * ___OnTextMessageResult_11;
	// System.Action`1<TextMessageComposeResult> IOSSocialManager::<>f__am$cache0
	Action_1_t1565920682 * ___U3CU3Ef__amU24cache0_12;

public:
	inline static int32_t get_offset_of_OnFacebookPostStart_4() { return static_cast<int32_t>(offsetof(IOSSocialManager_t1565743305_StaticFields, ___OnFacebookPostStart_4)); }
	inline Action_t1264377477 * get_OnFacebookPostStart_4() const { return ___OnFacebookPostStart_4; }
	inline Action_t1264377477 ** get_address_of_OnFacebookPostStart_4() { return &___OnFacebookPostStart_4; }
	inline void set_OnFacebookPostStart_4(Action_t1264377477 * value)
	{
		___OnFacebookPostStart_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnFacebookPostStart_4), value);
	}

	inline static int32_t get_offset_of_OnTwitterPostStart_5() { return static_cast<int32_t>(offsetof(IOSSocialManager_t1565743305_StaticFields, ___OnTwitterPostStart_5)); }
	inline Action_t1264377477 * get_OnTwitterPostStart_5() const { return ___OnTwitterPostStart_5; }
	inline Action_t1264377477 ** get_address_of_OnTwitterPostStart_5() { return &___OnTwitterPostStart_5; }
	inline void set_OnTwitterPostStart_5(Action_t1264377477 * value)
	{
		___OnTwitterPostStart_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnTwitterPostStart_5), value);
	}

	inline static int32_t get_offset_of_OnInstagramPostStart_6() { return static_cast<int32_t>(offsetof(IOSSocialManager_t1565743305_StaticFields, ___OnInstagramPostStart_6)); }
	inline Action_t1264377477 * get_OnInstagramPostStart_6() const { return ___OnInstagramPostStart_6; }
	inline Action_t1264377477 ** get_address_of_OnInstagramPostStart_6() { return &___OnInstagramPostStart_6; }
	inline void set_OnInstagramPostStart_6(Action_t1264377477 * value)
	{
		___OnInstagramPostStart_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnInstagramPostStart_6), value);
	}

	inline static int32_t get_offset_of_OnFacebookPostResult_7() { return static_cast<int32_t>(offsetof(IOSSocialManager_t1565743305_StaticFields, ___OnFacebookPostResult_7)); }
	inline Action_1_t47902017 * get_OnFacebookPostResult_7() const { return ___OnFacebookPostResult_7; }
	inline Action_1_t47902017 ** get_address_of_OnFacebookPostResult_7() { return &___OnFacebookPostResult_7; }
	inline void set_OnFacebookPostResult_7(Action_1_t47902017 * value)
	{
		___OnFacebookPostResult_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnFacebookPostResult_7), value);
	}

	inline static int32_t get_offset_of_OnTwitterPostResult_8() { return static_cast<int32_t>(offsetof(IOSSocialManager_t1565743305_StaticFields, ___OnTwitterPostResult_8)); }
	inline Action_1_t47902017 * get_OnTwitterPostResult_8() const { return ___OnTwitterPostResult_8; }
	inline Action_1_t47902017 ** get_address_of_OnTwitterPostResult_8() { return &___OnTwitterPostResult_8; }
	inline void set_OnTwitterPostResult_8(Action_1_t47902017 * value)
	{
		___OnTwitterPostResult_8 = value;
		Il2CppCodeGenWriteBarrier((&___OnTwitterPostResult_8), value);
	}

	inline static int32_t get_offset_of_OnInstagramPostResult_9() { return static_cast<int32_t>(offsetof(IOSSocialManager_t1565743305_StaticFields, ___OnInstagramPostResult_9)); }
	inline Action_1_t47902017 * get_OnInstagramPostResult_9() const { return ___OnInstagramPostResult_9; }
	inline Action_1_t47902017 ** get_address_of_OnInstagramPostResult_9() { return &___OnInstagramPostResult_9; }
	inline void set_OnInstagramPostResult_9(Action_1_t47902017 * value)
	{
		___OnInstagramPostResult_9 = value;
		Il2CppCodeGenWriteBarrier((&___OnInstagramPostResult_9), value);
	}

	inline static int32_t get_offset_of_OnMailResult_10() { return static_cast<int32_t>(offsetof(IOSSocialManager_t1565743305_StaticFields, ___OnMailResult_10)); }
	inline Action_1_t47902017 * get_OnMailResult_10() const { return ___OnMailResult_10; }
	inline Action_1_t47902017 ** get_address_of_OnMailResult_10() { return &___OnMailResult_10; }
	inline void set_OnMailResult_10(Action_1_t47902017 * value)
	{
		___OnMailResult_10 = value;
		Il2CppCodeGenWriteBarrier((&___OnMailResult_10), value);
	}

	inline static int32_t get_offset_of_OnTextMessageResult_11() { return static_cast<int32_t>(offsetof(IOSSocialManager_t1565743305_StaticFields, ___OnTextMessageResult_11)); }
	inline Action_1_t1565920682 * get_OnTextMessageResult_11() const { return ___OnTextMessageResult_11; }
	inline Action_1_t1565920682 ** get_address_of_OnTextMessageResult_11() { return &___OnTextMessageResult_11; }
	inline void set_OnTextMessageResult_11(Action_1_t1565920682 * value)
	{
		___OnTextMessageResult_11 = value;
		Il2CppCodeGenWriteBarrier((&___OnTextMessageResult_11), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_12() { return static_cast<int32_t>(offsetof(IOSSocialManager_t1565743305_StaticFields, ___U3CU3Ef__amU24cache0_12)); }
	inline Action_1_t1565920682 * get_U3CU3Ef__amU24cache0_12() const { return ___U3CU3Ef__amU24cache0_12; }
	inline Action_1_t1565920682 ** get_address_of_U3CU3Ef__amU24cache0_12() { return &___U3CU3Ef__amU24cache0_12; }
	inline void set_U3CU3Ef__amU24cache0_12(Action_1_t1565920682 * value)
	{
		___U3CU3Ef__amU24cache0_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSSOCIALMANAGER_T1565743305_H
#ifndef ISN_REMOTENOTIFICATIONSCONTROLLER_T2928859492_H
#define ISN_REMOTENOTIFICATIONSCONTROLLER_T2928859492_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ISN_RemoteNotificationsController
struct  ISN_RemoteNotificationsController_t2928859492  : public Singleton_1_t2558631638
{
public:
	// ISN_RemoteNotification ISN_RemoteNotificationsController::_LaunchNotification
	ISN_RemoteNotification_t3991987818 * ____LaunchNotification_5;

public:
	inline static int32_t get_offset_of__LaunchNotification_5() { return static_cast<int32_t>(offsetof(ISN_RemoteNotificationsController_t2928859492, ____LaunchNotification_5)); }
	inline ISN_RemoteNotification_t3991987818 * get__LaunchNotification_5() const { return ____LaunchNotification_5; }
	inline ISN_RemoteNotification_t3991987818 ** get_address_of__LaunchNotification_5() { return &____LaunchNotification_5; }
	inline void set__LaunchNotification_5(ISN_RemoteNotification_t3991987818 * value)
	{
		____LaunchNotification_5 = value;
		Il2CppCodeGenWriteBarrier((&____LaunchNotification_5), value);
	}
};

struct ISN_RemoteNotificationsController_t2928859492_StaticFields
{
public:
	// System.Action`1<ISN_RemoteNotificationsRegistrationResult> ISN_RemoteNotificationsController::_RegistrationCallback
	Action_1_t1103427813 * ____RegistrationCallback_4;
	// System.Action`1<ISN_RemoteNotification> ISN_RemoteNotificationsController::OnRemoteNotificationReceived
	Action_1_t4164455413 * ___OnRemoteNotificationReceived_6;

public:
	inline static int32_t get_offset_of__RegistrationCallback_4() { return static_cast<int32_t>(offsetof(ISN_RemoteNotificationsController_t2928859492_StaticFields, ____RegistrationCallback_4)); }
	inline Action_1_t1103427813 * get__RegistrationCallback_4() const { return ____RegistrationCallback_4; }
	inline Action_1_t1103427813 ** get_address_of__RegistrationCallback_4() { return &____RegistrationCallback_4; }
	inline void set__RegistrationCallback_4(Action_1_t1103427813 * value)
	{
		____RegistrationCallback_4 = value;
		Il2CppCodeGenWriteBarrier((&____RegistrationCallback_4), value);
	}

	inline static int32_t get_offset_of_OnRemoteNotificationReceived_6() { return static_cast<int32_t>(offsetof(ISN_RemoteNotificationsController_t2928859492_StaticFields, ___OnRemoteNotificationReceived_6)); }
	inline Action_1_t4164455413 * get_OnRemoteNotificationReceived_6() const { return ___OnRemoteNotificationReceived_6; }
	inline Action_1_t4164455413 ** get_address_of_OnRemoteNotificationReceived_6() { return &___OnRemoteNotificationReceived_6; }
	inline void set_OnRemoteNotificationReceived_6(Action_1_t4164455413 * value)
	{
		___OnRemoteNotificationReceived_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnRemoteNotificationReceived_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISN_REMOTENOTIFICATIONSCONTROLLER_T2928859492_H
#ifndef IOSVIDEOMANAGER_T1926991955_H
#define IOSVIDEOMANAGER_T1926991955_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IOSVideoManager
struct  IOSVideoManager_t1926991955  : public Singleton_1_t1556764101
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSVIDEOMANAGER_T1926991955_H
#ifndef ISN_MEDIACONTROLLER_T2009090688_H
#define ISN_MEDIACONTROLLER_T2009090688_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ISN_MediaController
struct  ISN_MediaController_t2009090688  : public Singleton_1_t1638862834
{
public:
	// MP_MediaItem ISN_MediaController::_NowPlayingItem
	MP_MediaItem_t2497507924 * ____NowPlayingItem_4;
	// MP_MusicPlaybackState ISN_MediaController::_State
	int32_t ____State_5;
	// System.Collections.Generic.List`1<MP_MediaItem> ISN_MediaController::_CurrentQueue
	List_1_t3969582666 * ____CurrentQueue_6;

public:
	inline static int32_t get_offset_of__NowPlayingItem_4() { return static_cast<int32_t>(offsetof(ISN_MediaController_t2009090688, ____NowPlayingItem_4)); }
	inline MP_MediaItem_t2497507924 * get__NowPlayingItem_4() const { return ____NowPlayingItem_4; }
	inline MP_MediaItem_t2497507924 ** get_address_of__NowPlayingItem_4() { return &____NowPlayingItem_4; }
	inline void set__NowPlayingItem_4(MP_MediaItem_t2497507924 * value)
	{
		____NowPlayingItem_4 = value;
		Il2CppCodeGenWriteBarrier((&____NowPlayingItem_4), value);
	}

	inline static int32_t get_offset_of__State_5() { return static_cast<int32_t>(offsetof(ISN_MediaController_t2009090688, ____State_5)); }
	inline int32_t get__State_5() const { return ____State_5; }
	inline int32_t* get_address_of__State_5() { return &____State_5; }
	inline void set__State_5(int32_t value)
	{
		____State_5 = value;
	}

	inline static int32_t get_offset_of__CurrentQueue_6() { return static_cast<int32_t>(offsetof(ISN_MediaController_t2009090688, ____CurrentQueue_6)); }
	inline List_1_t3969582666 * get__CurrentQueue_6() const { return ____CurrentQueue_6; }
	inline List_1_t3969582666 ** get_address_of__CurrentQueue_6() { return &____CurrentQueue_6; }
	inline void set__CurrentQueue_6(List_1_t3969582666 * value)
	{
		____CurrentQueue_6 = value;
		Il2CppCodeGenWriteBarrier((&____CurrentQueue_6), value);
	}
};

struct ISN_MediaController_t2009090688_StaticFields
{
public:
	// System.Action`1<MP_MediaPickerResult> ISN_MediaController::ActionMediaPickerResult
	Action_1_t804714572 * ___ActionMediaPickerResult_7;
	// System.Action`1<MP_MediaPickerResult> ISN_MediaController::ActionQueueUpdated
	Action_1_t804714572 * ___ActionQueueUpdated_8;
	// System.Action`1<MP_MediaItem> ISN_MediaController::ActionNowPlayingItemChanged
	Action_1_t2669975519 * ___ActionNowPlayingItemChanged_9;
	// System.Action`1<MP_MusicPlaybackState> ISN_MediaController::ActionPlaybackStateChanged
	Action_1_t1181815693 * ___ActionPlaybackStateChanged_10;

public:
	inline static int32_t get_offset_of_ActionMediaPickerResult_7() { return static_cast<int32_t>(offsetof(ISN_MediaController_t2009090688_StaticFields, ___ActionMediaPickerResult_7)); }
	inline Action_1_t804714572 * get_ActionMediaPickerResult_7() const { return ___ActionMediaPickerResult_7; }
	inline Action_1_t804714572 ** get_address_of_ActionMediaPickerResult_7() { return &___ActionMediaPickerResult_7; }
	inline void set_ActionMediaPickerResult_7(Action_1_t804714572 * value)
	{
		___ActionMediaPickerResult_7 = value;
		Il2CppCodeGenWriteBarrier((&___ActionMediaPickerResult_7), value);
	}

	inline static int32_t get_offset_of_ActionQueueUpdated_8() { return static_cast<int32_t>(offsetof(ISN_MediaController_t2009090688_StaticFields, ___ActionQueueUpdated_8)); }
	inline Action_1_t804714572 * get_ActionQueueUpdated_8() const { return ___ActionQueueUpdated_8; }
	inline Action_1_t804714572 ** get_address_of_ActionQueueUpdated_8() { return &___ActionQueueUpdated_8; }
	inline void set_ActionQueueUpdated_8(Action_1_t804714572 * value)
	{
		___ActionQueueUpdated_8 = value;
		Il2CppCodeGenWriteBarrier((&___ActionQueueUpdated_8), value);
	}

	inline static int32_t get_offset_of_ActionNowPlayingItemChanged_9() { return static_cast<int32_t>(offsetof(ISN_MediaController_t2009090688_StaticFields, ___ActionNowPlayingItemChanged_9)); }
	inline Action_1_t2669975519 * get_ActionNowPlayingItemChanged_9() const { return ___ActionNowPlayingItemChanged_9; }
	inline Action_1_t2669975519 ** get_address_of_ActionNowPlayingItemChanged_9() { return &___ActionNowPlayingItemChanged_9; }
	inline void set_ActionNowPlayingItemChanged_9(Action_1_t2669975519 * value)
	{
		___ActionNowPlayingItemChanged_9 = value;
		Il2CppCodeGenWriteBarrier((&___ActionNowPlayingItemChanged_9), value);
	}

	inline static int32_t get_offset_of_ActionPlaybackStateChanged_10() { return static_cast<int32_t>(offsetof(ISN_MediaController_t2009090688_StaticFields, ___ActionPlaybackStateChanged_10)); }
	inline Action_1_t1181815693 * get_ActionPlaybackStateChanged_10() const { return ___ActionPlaybackStateChanged_10; }
	inline Action_1_t1181815693 ** get_address_of_ActionPlaybackStateChanged_10() { return &___ActionPlaybackStateChanged_10; }
	inline void set_ActionPlaybackStateChanged_10(Action_1_t1181815693 * value)
	{
		___ActionPlaybackStateChanged_10 = value;
		Il2CppCodeGenWriteBarrier((&___ActionPlaybackStateChanged_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISN_MEDIACONTROLLER_T2009090688_H
#ifndef SK_CLOUDSERVICE_T1139212890_H
#define SK_CLOUDSERVICE_T1139212890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SK_CloudService
struct  SK_CloudService_t1139212890  : public Singleton_1_t768985036
{
public:

public:
};

struct SK_CloudService_t1139212890_StaticFields
{
public:
	// System.Action`1<SK_AuthorizationResult> SK_CloudService::OnAuthorizationFinished
	Action_1_t3677057630 * ___OnAuthorizationFinished_4;
	// System.Action`1<SK_RequestCapabilitieResult> SK_CloudService::OnCapabilitiesRequestFinished
	Action_1_t30760723 * ___OnCapabilitiesRequestFinished_5;
	// System.Action`1<SK_RequestStorefrontIdentifierResult> SK_CloudService::OnStorefrontIdentifierRequestFinished
	Action_1_t1006234518 * ___OnStorefrontIdentifierRequestFinished_6;

public:
	inline static int32_t get_offset_of_OnAuthorizationFinished_4() { return static_cast<int32_t>(offsetof(SK_CloudService_t1139212890_StaticFields, ___OnAuthorizationFinished_4)); }
	inline Action_1_t3677057630 * get_OnAuthorizationFinished_4() const { return ___OnAuthorizationFinished_4; }
	inline Action_1_t3677057630 ** get_address_of_OnAuthorizationFinished_4() { return &___OnAuthorizationFinished_4; }
	inline void set_OnAuthorizationFinished_4(Action_1_t3677057630 * value)
	{
		___OnAuthorizationFinished_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnAuthorizationFinished_4), value);
	}

	inline static int32_t get_offset_of_OnCapabilitiesRequestFinished_5() { return static_cast<int32_t>(offsetof(SK_CloudService_t1139212890_StaticFields, ___OnCapabilitiesRequestFinished_5)); }
	inline Action_1_t30760723 * get_OnCapabilitiesRequestFinished_5() const { return ___OnCapabilitiesRequestFinished_5; }
	inline Action_1_t30760723 ** get_address_of_OnCapabilitiesRequestFinished_5() { return &___OnCapabilitiesRequestFinished_5; }
	inline void set_OnCapabilitiesRequestFinished_5(Action_1_t30760723 * value)
	{
		___OnCapabilitiesRequestFinished_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnCapabilitiesRequestFinished_5), value);
	}

	inline static int32_t get_offset_of_OnStorefrontIdentifierRequestFinished_6() { return static_cast<int32_t>(offsetof(SK_CloudService_t1139212890_StaticFields, ___OnStorefrontIdentifierRequestFinished_6)); }
	inline Action_1_t1006234518 * get_OnStorefrontIdentifierRequestFinished_6() const { return ___OnStorefrontIdentifierRequestFinished_6; }
	inline Action_1_t1006234518 ** get_address_of_OnStorefrontIdentifierRequestFinished_6() { return &___OnStorefrontIdentifierRequestFinished_6; }
	inline void set_OnStorefrontIdentifierRequestFinished_6(Action_1_t1006234518 * value)
	{
		___OnStorefrontIdentifierRequestFinished_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnStorefrontIdentifierRequestFinished_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SK_CLOUDSERVICE_T1139212890_H
#ifndef ISN_LOCALNOTIFICATIONSCONTROLLER_T3493627228_H
#define ISN_LOCALNOTIFICATIONSCONTROLLER_T3493627228_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ISN_LocalNotificationsController
struct  ISN_LocalNotificationsController_t3493627228  : public Singleton_1_t3123399374
{
public:
	// ISN_LocalNotification ISN_LocalNotificationsController::_LaunchNotification
	ISN_LocalNotification_t2969587787 * ____LaunchNotification_6;

public:
	inline static int32_t get_offset_of__LaunchNotification_6() { return static_cast<int32_t>(offsetof(ISN_LocalNotificationsController_t3493627228, ____LaunchNotification_6)); }
	inline ISN_LocalNotification_t2969587787 * get__LaunchNotification_6() const { return ____LaunchNotification_6; }
	inline ISN_LocalNotification_t2969587787 ** get_address_of__LaunchNotification_6() { return &____LaunchNotification_6; }
	inline void set__LaunchNotification_6(ISN_LocalNotification_t2969587787 * value)
	{
		____LaunchNotification_6 = value;
		Il2CppCodeGenWriteBarrier((&____LaunchNotification_6), value);
	}
};

struct ISN_LocalNotificationsController_t3493627228_StaticFields
{
public:
	// System.Action`1<SA.Common.Models.Result> ISN_LocalNotificationsController::OnNotificationScheduleResult
	Action_1_t47902017 * ___OnNotificationScheduleResult_7;
	// System.Action`1<ISN_LocalNotification> ISN_LocalNotificationsController::OnLocalNotificationReceived
	Action_1_t3142055382 * ___OnLocalNotificationReceived_8;

public:
	inline static int32_t get_offset_of_OnNotificationScheduleResult_7() { return static_cast<int32_t>(offsetof(ISN_LocalNotificationsController_t3493627228_StaticFields, ___OnNotificationScheduleResult_7)); }
	inline Action_1_t47902017 * get_OnNotificationScheduleResult_7() const { return ___OnNotificationScheduleResult_7; }
	inline Action_1_t47902017 ** get_address_of_OnNotificationScheduleResult_7() { return &___OnNotificationScheduleResult_7; }
	inline void set_OnNotificationScheduleResult_7(Action_1_t47902017 * value)
	{
		___OnNotificationScheduleResult_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnNotificationScheduleResult_7), value);
	}

	inline static int32_t get_offset_of_OnLocalNotificationReceived_8() { return static_cast<int32_t>(offsetof(ISN_LocalNotificationsController_t3493627228_StaticFields, ___OnLocalNotificationReceived_8)); }
	inline Action_1_t3142055382 * get_OnLocalNotificationReceived_8() const { return ___OnLocalNotificationReceived_8; }
	inline Action_1_t3142055382 ** get_address_of_OnLocalNotificationReceived_8() { return &___OnLocalNotificationReceived_8; }
	inline void set_OnLocalNotificationReceived_8(Action_1_t3142055382 * value)
	{
		___OnLocalNotificationReceived_8 = value;
		Il2CppCodeGenWriteBarrier((&___OnLocalNotificationReceived_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISN_LOCALNOTIFICATIONSCONTROLLER_T3493627228_H
#ifndef ISN_REPLAYKIT_T1483172299_H
#define ISN_REPLAYKIT_T1483172299_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ISN_ReplayKit
struct  ISN_ReplayKit_t1483172299  : public Singleton_1_t1112944445
{
public:
	// System.Boolean ISN_ReplayKit::_IsRecodingAvailableToShare
	bool ____IsRecodingAvailableToShare_10;

public:
	inline static int32_t get_offset_of__IsRecodingAvailableToShare_10() { return static_cast<int32_t>(offsetof(ISN_ReplayKit_t1483172299, ____IsRecodingAvailableToShare_10)); }
	inline bool get__IsRecodingAvailableToShare_10() const { return ____IsRecodingAvailableToShare_10; }
	inline bool* get_address_of__IsRecodingAvailableToShare_10() { return &____IsRecodingAvailableToShare_10; }
	inline void set__IsRecodingAvailableToShare_10(bool value)
	{
		____IsRecodingAvailableToShare_10 = value;
	}
};

struct ISN_ReplayKit_t1483172299_StaticFields
{
public:
	// System.Action`1<SA.Common.Models.Result> ISN_ReplayKit::ActionRecordStarted
	Action_1_t47902017 * ___ActionRecordStarted_4;
	// System.Action`1<SA.Common.Models.Result> ISN_ReplayKit::ActionRecordStoped
	Action_1_t47902017 * ___ActionRecordStoped_5;
	// System.Action`1<ReplayKitVideoShareResult> ISN_ReplayKit::ActionShareDialogFinished
	Action_1_t845705176 * ___ActionShareDialogFinished_6;
	// System.Action`1<SA.Common.Models.Error> ISN_ReplayKit::ActionRecordInterrupted
	Action_1_t513010639 * ___ActionRecordInterrupted_7;
	// System.Action`1<System.Boolean> ISN_ReplayKit::ActionRecorderDidChangeAvailability
	Action_1_t269755560 * ___ActionRecorderDidChangeAvailability_8;
	// System.Action ISN_ReplayKit::ActionRecordDiscard
	Action_t1264377477 * ___ActionRecordDiscard_9;

public:
	inline static int32_t get_offset_of_ActionRecordStarted_4() { return static_cast<int32_t>(offsetof(ISN_ReplayKit_t1483172299_StaticFields, ___ActionRecordStarted_4)); }
	inline Action_1_t47902017 * get_ActionRecordStarted_4() const { return ___ActionRecordStarted_4; }
	inline Action_1_t47902017 ** get_address_of_ActionRecordStarted_4() { return &___ActionRecordStarted_4; }
	inline void set_ActionRecordStarted_4(Action_1_t47902017 * value)
	{
		___ActionRecordStarted_4 = value;
		Il2CppCodeGenWriteBarrier((&___ActionRecordStarted_4), value);
	}

	inline static int32_t get_offset_of_ActionRecordStoped_5() { return static_cast<int32_t>(offsetof(ISN_ReplayKit_t1483172299_StaticFields, ___ActionRecordStoped_5)); }
	inline Action_1_t47902017 * get_ActionRecordStoped_5() const { return ___ActionRecordStoped_5; }
	inline Action_1_t47902017 ** get_address_of_ActionRecordStoped_5() { return &___ActionRecordStoped_5; }
	inline void set_ActionRecordStoped_5(Action_1_t47902017 * value)
	{
		___ActionRecordStoped_5 = value;
		Il2CppCodeGenWriteBarrier((&___ActionRecordStoped_5), value);
	}

	inline static int32_t get_offset_of_ActionShareDialogFinished_6() { return static_cast<int32_t>(offsetof(ISN_ReplayKit_t1483172299_StaticFields, ___ActionShareDialogFinished_6)); }
	inline Action_1_t845705176 * get_ActionShareDialogFinished_6() const { return ___ActionShareDialogFinished_6; }
	inline Action_1_t845705176 ** get_address_of_ActionShareDialogFinished_6() { return &___ActionShareDialogFinished_6; }
	inline void set_ActionShareDialogFinished_6(Action_1_t845705176 * value)
	{
		___ActionShareDialogFinished_6 = value;
		Il2CppCodeGenWriteBarrier((&___ActionShareDialogFinished_6), value);
	}

	inline static int32_t get_offset_of_ActionRecordInterrupted_7() { return static_cast<int32_t>(offsetof(ISN_ReplayKit_t1483172299_StaticFields, ___ActionRecordInterrupted_7)); }
	inline Action_1_t513010639 * get_ActionRecordInterrupted_7() const { return ___ActionRecordInterrupted_7; }
	inline Action_1_t513010639 ** get_address_of_ActionRecordInterrupted_7() { return &___ActionRecordInterrupted_7; }
	inline void set_ActionRecordInterrupted_7(Action_1_t513010639 * value)
	{
		___ActionRecordInterrupted_7 = value;
		Il2CppCodeGenWriteBarrier((&___ActionRecordInterrupted_7), value);
	}

	inline static int32_t get_offset_of_ActionRecorderDidChangeAvailability_8() { return static_cast<int32_t>(offsetof(ISN_ReplayKit_t1483172299_StaticFields, ___ActionRecorderDidChangeAvailability_8)); }
	inline Action_1_t269755560 * get_ActionRecorderDidChangeAvailability_8() const { return ___ActionRecorderDidChangeAvailability_8; }
	inline Action_1_t269755560 ** get_address_of_ActionRecorderDidChangeAvailability_8() { return &___ActionRecorderDidChangeAvailability_8; }
	inline void set_ActionRecorderDidChangeAvailability_8(Action_1_t269755560 * value)
	{
		___ActionRecorderDidChangeAvailability_8 = value;
		Il2CppCodeGenWriteBarrier((&___ActionRecorderDidChangeAvailability_8), value);
	}

	inline static int32_t get_offset_of_ActionRecordDiscard_9() { return static_cast<int32_t>(offsetof(ISN_ReplayKit_t1483172299_StaticFields, ___ActionRecordDiscard_9)); }
	inline Action_t1264377477 * get_ActionRecordDiscard_9() const { return ___ActionRecordDiscard_9; }
	inline Action_t1264377477 ** get_address_of_ActionRecordDiscard_9() { return &___ActionRecordDiscard_9; }
	inline void set_ActionRecordDiscard_9(Action_t1264377477 * value)
	{
		___ActionRecordDiscard_9 = value;
		Il2CppCodeGenWriteBarrier((&___ActionRecordDiscard_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISN_REPLAYKIT_T1483172299_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2400 = { sizeof (GK_Score_t215322251), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2400[7] = 
{
	GK_Score_t215322251::get_offset_of__Rank_0(),
	GK_Score_t215322251::get_offset_of__Score_1(),
	GK_Score_t215322251::get_offset_of__Context_2(),
	GK_Score_t215322251::get_offset_of__PlayerId_3(),
	GK_Score_t215322251::get_offset_of__LeaderboardId_4(),
	GK_Score_t215322251::get_offset_of__Collection_5(),
	GK_Score_t215322251::get_offset_of__TimeSpan_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2401 = { sizeof (GK_ScoreCollection_t3739344104), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2401[3] = 
{
	GK_ScoreCollection_t3739344104::get_offset_of_AllTimeScores_0(),
	GK_ScoreCollection_t3739344104::get_offset_of_WeekScores_1(),
	GK_ScoreCollection_t3739344104::get_offset_of_TodayScores_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2402 = { sizeof (GK_SavedGame_t3445071760), -1, sizeof(GK_SavedGame_t3445071760_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2402[9] = 
{
	GK_SavedGame_t3445071760::get_offset_of_ActionDataLoaded_0(),
	GK_SavedGame_t3445071760::get_offset_of__Id_1(),
	GK_SavedGame_t3445071760::get_offset_of__Name_2(),
	GK_SavedGame_t3445071760::get_offset_of__DeviceName_3(),
	GK_SavedGame_t3445071760::get_offset_of__ModificationDate_4(),
	GK_SavedGame_t3445071760::get_offset_of__OriginalDateString_5(),
	GK_SavedGame_t3445071760::get_offset_of__Data_6(),
	GK_SavedGame_t3445071760::get_offset_of__IsDataLoaded_7(),
	GK_SavedGame_t3445071760_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2403 = { sizeof (GK_FetchResult_t82030703), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2403[1] = 
{
	GK_FetchResult_t82030703::get_offset_of__SavedGames_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2404 = { sizeof (GK_SaveDataLoaded_t72329421), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2404[1] = 
{
	GK_SaveDataLoaded_t72329421::get_offset_of__SavedGame_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2405 = { sizeof (GK_SaveRemoveResult_t1262993476), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2405[1] = 
{
	GK_SaveRemoveResult_t1262993476::get_offset_of__SaveName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2406 = { sizeof (GK_SaveResult_t325278800), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2406[1] = 
{
	GK_SaveResult_t325278800::get_offset_of__SavedGame_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2407 = { sizeof (GK_SavesResolveResult_t3961500742), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2407[1] = 
{
	GK_SavesResolveResult_t3961500742::get_offset_of__ResolvedSaves_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2408 = { sizeof (GK_MatchSendDataMode_t148239142)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2408[3] = 
{
	GK_MatchSendDataMode_t148239142::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2409 = { sizeof (GK_PlayerConnectionState_t3314454918)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2409[4] = 
{
	GK_PlayerConnectionState_t3314454918::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2410 = { sizeof (GK_RTM_MatchStartedResult_t3577638810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2410[1] = 
{
	GK_RTM_MatchStartedResult_t3577638810::get_offset_of__Match_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2411 = { sizeof (GK_RTM_QueryActivityResult_t3645624704), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2411[1] = 
{
	GK_RTM_QueryActivityResult_t3645624704::get_offset_of__Activity_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2412 = { sizeof (GK_RTM_Match_t949836071), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2412[2] = 
{
	GK_RTM_Match_t949836071::get_offset_of__ExpectedPlayerCount_0(),
	GK_RTM_Match_t949836071::get_offset_of__Players_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2413 = { sizeof (GK_TurnBasedMatchOutcome_t2945463772)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2413[11] = 
{
	GK_TurnBasedMatchOutcome_t2945463772::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2414 = { sizeof (GK_TurnBasedMatchStatus_t3641047812)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2414[5] = 
{
	GK_TurnBasedMatchStatus_t3641047812::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2415 = { sizeof (GK_TurnBasedParticipantStatus_t2860676061)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2415[7] = 
{
	GK_TurnBasedParticipantStatus_t2860676061::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2416 = { sizeof (GK_TBM_EndTrunResult_t3483094632), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2416[1] = 
{
	GK_TBM_EndTrunResult_t3483094632::get_offset_of__Match_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2417 = { sizeof (GK_TBM_LoadMatchesResult_t4223942821), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2417[1] = 
{
	GK_TBM_LoadMatchesResult_t4223942821::get_offset_of_LoadedMatches_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2418 = { sizeof (GK_TBM_LoadMatchResult_t1229115337), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2418[1] = 
{
	GK_TBM_LoadMatchResult_t1229115337::get_offset_of__Match_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2419 = { sizeof (GK_TBM_MatchDataUpdateResult_t51603500), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2419[1] = 
{
	GK_TBM_MatchDataUpdateResult_t51603500::get_offset_of__Match_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2420 = { sizeof (GK_TBM_MatchEndResult_t3737364521), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2420[1] = 
{
	GK_TBM_MatchEndResult_t3737364521::get_offset_of__Match_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2421 = { sizeof (GK_TBM_MatchInitResult_t1763010693), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2421[1] = 
{
	GK_TBM_MatchInitResult_t1763010693::get_offset_of__Match_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2422 = { sizeof (GK_TBM_MatchQuitResult_t1382588157), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2422[1] = 
{
	GK_TBM_MatchQuitResult_t1382588157::get_offset_of__MatchId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2423 = { sizeof (GK_TBM_MatchRemovedResult_t392638254), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2423[1] = 
{
	GK_TBM_MatchRemovedResult_t392638254::get_offset_of__MatchId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2424 = { sizeof (GK_TBM_MatchTurnResult_t2275859377), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2424[1] = 
{
	GK_TBM_MatchTurnResult_t2275859377::get_offset_of__Match_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2425 = { sizeof (GK_TBM_RematchResult_t1046172076), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2425[1] = 
{
	GK_TBM_RematchResult_t1046172076::get_offset_of__Match_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2426 = { sizeof (GK_TBM_Match_t83581881), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2426[7] = 
{
	GK_TBM_Match_t83581881::get_offset_of_Id_0(),
	GK_TBM_Match_t83581881::get_offset_of_Message_1(),
	GK_TBM_Match_t83581881::get_offset_of_CurrentParticipant_2(),
	GK_TBM_Match_t83581881::get_offset_of_CreationTimestamp_3(),
	GK_TBM_Match_t83581881::get_offset_of_Data_4(),
	GK_TBM_Match_t83581881::get_offset_of_Status_5(),
	GK_TBM_Match_t83581881::get_offset_of_Participants_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2427 = { sizeof (GK_TBM_Participant_t2994240226), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2427[6] = 
{
	GK_TBM_Participant_t2994240226::get_offset_of__PlayerId_0(),
	GK_TBM_Participant_t2994240226::get_offset_of__MatchId_1(),
	GK_TBM_Participant_t2994240226::get_offset_of__TimeoutDate_2(),
	GK_TBM_Participant_t2994240226::get_offset_of__LastTurnDate_3(),
	GK_TBM_Participant_t2994240226::get_offset_of__Status_4(),
	GK_TBM_Participant_t2994240226::get_offset_of__MatchOutcome_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2428 = { sizeof (iCloudManager_t2877946005), -1, sizeof(iCloudManager_t2877946005_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2428[4] = 
{
	iCloudManager_t2877946005_StaticFields::get_offset_of_OnCloudInitAction_4(),
	iCloudManager_t2877946005_StaticFields::get_offset_of_OnCloudDataReceivedAction_5(),
	iCloudManager_t2877946005_StaticFields::get_offset_of_OnStoreDidChangeExternally_6(),
	iCloudManager_t2877946005::get_offset_of_s_requestDataCallbacks_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2429 = { sizeof (ISN_CloudKit_t3134663278), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2429[4] = 
{
	ISN_CloudKit_t3134663278::get_offset_of__PrivateDB_4(),
	ISN_CloudKit_t3134663278::get_offset_of__PublicDB_5(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2430 = { sizeof (CK_Database_t2819592330), -1, sizeof(CK_Database_t2819592330_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2430[10] = 
{
	CK_Database_t2819592330::get_offset_of_ActionRecordSaved_0(),
	CK_Database_t2819592330::get_offset_of_ActionRecordFetchComplete_1(),
	CK_Database_t2819592330::get_offset_of_ActionRecordDeleted_2(),
	CK_Database_t2819592330::get_offset_of_ActionQueryComplete_3(),
	CK_Database_t2819592330_StaticFields::get_offset_of__Databases_4(),
	CK_Database_t2819592330::get_offset_of__InternalId_5(),
	CK_Database_t2819592330_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_6(),
	CK_Database_t2819592330_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_7(),
	CK_Database_t2819592330_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_8(),
	CK_Database_t2819592330_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2431 = { sizeof (CK_Query_t2012057349), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2431[2] = 
{
	CK_Query_t2012057349::get_offset_of__Predicate_0(),
	CK_Query_t2012057349::get_offset_of__RecordType_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2432 = { sizeof (CK_Record_t4197524344), -1, sizeof(CK_Record_t4197524344_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2432[5] = 
{
	CK_Record_t4197524344_StaticFields::get_offset_of__Records_0(),
	CK_Record_t4197524344::get_offset_of__Id_1(),
	CK_Record_t4197524344::get_offset_of__Type_2(),
	CK_Record_t4197524344::get_offset_of__Data_3(),
	CK_Record_t4197524344::get_offset_of__internalId_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2433 = { sizeof (CK_RecordID_t3242371914), -1, sizeof(CK_RecordID_t3242371914_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2433[3] = 
{
	CK_RecordID_t3242371914::get_offset_of__internalId_0(),
	CK_RecordID_t3242371914::get_offset_of__Name_1(),
	CK_RecordID_t3242371914_StaticFields::get_offset_of__Ids_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2434 = { sizeof (CK_QueryResult_t1037223848), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2434[2] = 
{
	CK_QueryResult_t1037223848::get_offset_of__Database_1(),
	CK_QueryResult_t1037223848::get_offset_of__Records_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2435 = { sizeof (CK_RecordDeleteResult_t2457932905), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2435[2] = 
{
	CK_RecordDeleteResult_t2457932905::get_offset_of__RecordID_1(),
	CK_RecordDeleteResult_t2457932905::get_offset_of__Database_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2436 = { sizeof (CK_RecordResult_t4009498549), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2436[2] = 
{
	CK_RecordResult_t4009498549::get_offset_of__Record_1(),
	CK_RecordResult_t4009498549::get_offset_of__Database_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2437 = { sizeof (iCloudData_t94805902), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2437[3] = 
{
	iCloudData_t94805902::get_offset_of_m_key_0(),
	iCloudData_t94805902::get_offset_of_m_val_1(),
	iCloudData_t94805902::get_offset_of_m_IsEmpty_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2438 = { sizeof (IOSCamera_t2024555079), -1, sizeof(IOSCamera_t2024555079_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2438[5] = 
{
	IOSCamera_t2024555079_StaticFields::get_offset_of_OnImagePicked_4(),
	IOSCamera_t2024555079_StaticFields::get_offset_of_OnImageSaved_5(),
	IOSCamera_t2024555079_StaticFields::get_offset_of_OnVideoPathPicked_6(),
	IOSCamera_t2024555079::get_offset_of__IsWaitngForResponce_7(),
	IOSCamera_t2024555079::get_offset_of__IsInitialized_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2439 = { sizeof (U3CSaveScreenshotU3Ec__Iterator0_t775223138), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2439[7] = 
{
	U3CSaveScreenshotU3Ec__Iterator0_t775223138::get_offset_of_U3CwidthU3E__0_0(),
	U3CSaveScreenshotU3Ec__Iterator0_t775223138::get_offset_of_U3CheightU3E__0_1(),
	U3CSaveScreenshotU3Ec__Iterator0_t775223138::get_offset_of_U3CtexU3E__0_2(),
	U3CSaveScreenshotU3Ec__Iterator0_t775223138::get_offset_of_U24this_3(),
	U3CSaveScreenshotU3Ec__Iterator0_t775223138::get_offset_of_U24current_4(),
	U3CSaveScreenshotU3Ec__Iterator0_t775223138::get_offset_of_U24disposing_5(),
	U3CSaveScreenshotU3Ec__Iterator0_t775223138::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2440 = { sizeof (ISN_ImageSource_t3439168966)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2440[4] = 
{
	ISN_ImageSource_t3439168966::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2441 = { sizeof (IOSImagePickResult_t4120145150), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2441[1] = 
{
	IOSImagePickResult_t4120145150::get_offset_of__image_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2442 = { sizeof (ISN_FilePicker_t2788391163), -1, sizeof(ISN_FilePicker_t2788391163_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2442[1] = 
{
	ISN_FilePicker_t2788391163_StaticFields::get_offset_of_MediaPickFinished_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2443 = { sizeof (IOSGalleryLoadImageFormat_t3491991360)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2443[3] = 
{
	IOSGalleryLoadImageFormat_t3491991360::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2444 = { sizeof (ISN_FilePickerResult_t2483255090), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2444[1] = 
{
	ISN_FilePickerResult_t2483255090::get_offset_of_PickedImages_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2445 = { sizeof (ISN_MediaController_t2009090688), -1, sizeof(ISN_MediaController_t2009090688_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2445[7] = 
{
	ISN_MediaController_t2009090688::get_offset_of__NowPlayingItem_4(),
	ISN_MediaController_t2009090688::get_offset_of__State_5(),
	ISN_MediaController_t2009090688::get_offset_of__CurrentQueue_6(),
	ISN_MediaController_t2009090688_StaticFields::get_offset_of_ActionMediaPickerResult_7(),
	ISN_MediaController_t2009090688_StaticFields::get_offset_of_ActionQueueUpdated_8(),
	ISN_MediaController_t2009090688_StaticFields::get_offset_of_ActionNowPlayingItemChanged_9(),
	ISN_MediaController_t2009090688_StaticFields::get_offset_of_ActionPlaybackStateChanged_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2446 = { sizeof (MP_MusicPlaybackState_t1009348098)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2446[7] = 
{
	MP_MusicPlaybackState_t1009348098::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2447 = { sizeof (MP_MusicRepeatMode_t1738310452)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2447[5] = 
{
	MP_MusicRepeatMode_t1738310452::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2448 = { sizeof (MP_MusicShuffleMode_t1928103744)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2448[5] = 
{
	MP_MusicShuffleMode_t1928103744::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2449 = { sizeof (MP_MediaItem_t2497507924), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2449[8] = 
{
	MP_MediaItem_t2497507924::get_offset_of__Id_0(),
	MP_MediaItem_t2497507924::get_offset_of__Title_1(),
	MP_MediaItem_t2497507924::get_offset_of__Artist_2(),
	MP_MediaItem_t2497507924::get_offset_of__AlbumTitle_3(),
	MP_MediaItem_t2497507924::get_offset_of__AlbumArtist_4(),
	MP_MediaItem_t2497507924::get_offset_of__Genre_5(),
	MP_MediaItem_t2497507924::get_offset_of__PlaybackDuration_6(),
	MP_MediaItem_t2497507924::get_offset_of__Composer_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2450 = { sizeof (MP_MediaPickerResult_t632246977), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2450[1] = 
{
	MP_MediaPickerResult_t632246977::get_offset_of__SelectedmediaItems_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2451 = { sizeof (IOSVideoManager_t1926991955), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2452 = { sizeof (ISN_ReplayKit_t1483172299), -1, sizeof(ISN_ReplayKit_t1483172299_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2452[7] = 
{
	ISN_ReplayKit_t1483172299_StaticFields::get_offset_of_ActionRecordStarted_4(),
	ISN_ReplayKit_t1483172299_StaticFields::get_offset_of_ActionRecordStoped_5(),
	ISN_ReplayKit_t1483172299_StaticFields::get_offset_of_ActionShareDialogFinished_6(),
	ISN_ReplayKit_t1483172299_StaticFields::get_offset_of_ActionRecordInterrupted_7(),
	ISN_ReplayKit_t1483172299_StaticFields::get_offset_of_ActionRecorderDidChangeAvailability_8(),
	ISN_ReplayKit_t1483172299_StaticFields::get_offset_of_ActionRecordDiscard_9(),
	ISN_ReplayKit_t1483172299::get_offset_of__IsRecodingAvailableToShare_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2453 = { sizeof (ReplayKitVideoShareResult_t673237581), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2453[1] = 
{
	ReplayKitVideoShareResult_t673237581::get_offset_of__Sources_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2454 = { sizeof (ISN_LocalNotificationsController_t3493627228), -1, sizeof(ISN_LocalNotificationsController_t3493627228_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2454[5] = 
{
	0,
	0,
	ISN_LocalNotificationsController_t3493627228::get_offset_of__LaunchNotification_6(),
	ISN_LocalNotificationsController_t3493627228_StaticFields::get_offset_of_OnNotificationScheduleResult_7(),
	ISN_LocalNotificationsController_t3493627228_StaticFields::get_offset_of_OnLocalNotificationReceived_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2455 = { sizeof (ISN_RemoteNotificationsController_t2928859492), -1, sizeof(ISN_RemoteNotificationsController_t2928859492_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2455[3] = 
{
	ISN_RemoteNotificationsController_t2928859492_StaticFields::get_offset_of__RegistrationCallback_4(),
	ISN_RemoteNotificationsController_t2928859492::get_offset_of__LaunchNotification_5(),
	ISN_RemoteNotificationsController_t2928859492_StaticFields::get_offset_of_OnRemoteNotificationReceived_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2456 = { sizeof (ISN_NotificationType_t2610276734), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2456[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2457 = { sizeof (ISN_DeviceToken_t822270907), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2457[2] = 
{
	ISN_DeviceToken_t822270907::get_offset_of__tokenString_0(),
	ISN_DeviceToken_t822270907::get_offset_of__tokenBytes_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2458 = { sizeof (ISN_LocalNotification_t2969587787), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2458[8] = 
{
	ISN_LocalNotification_t2969587787::get_offset_of__Id_0(),
	ISN_LocalNotification_t2969587787::get_offset_of__Date_1(),
	ISN_LocalNotification_t2969587787::get_offset_of__Message_2(),
	ISN_LocalNotification_t2969587787::get_offset_of__UseSound_3(),
	ISN_LocalNotification_t2969587787::get_offset_of__Badges_4(),
	ISN_LocalNotification_t2969587787::get_offset_of__Data_5(),
	ISN_LocalNotification_t2969587787::get_offset_of__SoundName_6(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2459 = { sizeof (ISN_RemoteNotification_t3991987818), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2459[1] = 
{
	ISN_RemoteNotification_t3991987818::get_offset_of__Body_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2460 = { sizeof (ISN_RemoteNotificationsRegistrationResult_t930960218), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2460[1] = 
{
	ISN_RemoteNotificationsRegistrationResult_t930960218::get_offset_of__Token_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2461 = { sizeof (IOSNativePopUpManager_t203992631), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2462 = { sizeof (BaseIOSPopup_t1731871609), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2462[2] = 
{
	BaseIOSPopup_t1731871609::get_offset_of_title_2(),
	BaseIOSPopup_t1731871609::get_offset_of_message_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2463 = { sizeof (IOSDialog_t662133956), -1, sizeof(IOSDialog_t662133956_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2463[4] = 
{
	IOSDialog_t662133956::get_offset_of_yes_4(),
	IOSDialog_t662133956::get_offset_of_no_5(),
	IOSDialog_t662133956::get_offset_of_OnComplete_6(),
	IOSDialog_t662133956_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2464 = { sizeof (IOSMessage_t148091823), -1, sizeof(IOSMessage_t148091823_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2464[3] = 
{
	IOSMessage_t148091823::get_offset_of_ok_4(),
	IOSMessage_t148091823::get_offset_of_OnComplete_5(),
	IOSMessage_t148091823_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2465 = { sizeof (IOSRateUsPopUp_t2981493649), -1, sizeof(IOSRateUsPopUp_t2981493649_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2465[5] = 
{
	IOSRateUsPopUp_t2981493649::get_offset_of_rate_4(),
	IOSRateUsPopUp_t2981493649::get_offset_of_remind_5(),
	IOSRateUsPopUp_t2981493649::get_offset_of_declined_6(),
	IOSRateUsPopUp_t2981493649::get_offset_of_OnComplete_7(),
	IOSRateUsPopUp_t2981493649_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2466 = { sizeof (IOSDialogResult_t243976279)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2466[6] = 
{
	IOSDialogResult_t243976279::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2467 = { sizeof (NativeReceiver_t2681933426), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2468 = { sizeof (NotificationCenter_t1582563873), -1, sizeof(NotificationCenter_t1582563873_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2468[5] = 
{
	NotificationCenter_t1582563873_StaticFields::get_offset_of_OnCallbackDictionary_0(),
	NotificationCenter_t1582563873_StaticFields::get_offset_of_OnPendingNotificationsCallback_1(),
	NotificationCenter_t1582563873_StaticFields::get_offset_of_RequestPermissionsCallback_2(),
	NotificationCenter_t1582563873_StaticFields::get_offset_of_OnWillPresentNotification_3(),
	NotificationCenter_t1582563873_StaticFields::get_offset_of_LastNotificationRequest_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2469 = { sizeof (AlertStyle_t1308137552)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2469[4] = 
{
	AlertStyle_t1308137552::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2470 = { sizeof (AuthorizationStatus_t3886484108)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2470[4] = 
{
	AuthorizationStatus_t3886484108::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2471 = { sizeof (NotificationStatus_t26805291)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2471[4] = 
{
	NotificationStatus_t26805291::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2472 = { sizeof (NotificationContent_t1589641823), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2472[7] = 
{
	NotificationContent_t1589641823::get_offset_of_Title_0(),
	NotificationContent_t1589641823::get_offset_of_Subtitle_1(),
	NotificationContent_t1589641823::get_offset_of_Body_2(),
	NotificationContent_t1589641823::get_offset_of_Sound_3(),
	NotificationContent_t1589641823::get_offset_of_Badge_4(),
	NotificationContent_t1589641823::get_offset_of_LaunchImageName_5(),
	NotificationContent_t1589641823::get_offset_of_UserInfo_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2473 = { sizeof (NotificationRequest_t1622111933), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2473[3] = 
{
	NotificationRequest_t1622111933::get_offset_of__Id_0(),
	NotificationRequest_t1622111933::get_offset_of__Content_1(),
	NotificationRequest_t1622111933::get_offset_of__Trigger_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2474 = { sizeof (NotificationTrigger_t2337490352), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2474[1] = 
{
	NotificationTrigger_t2337490352::get_offset_of_repeated_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2475 = { sizeof (DateComponents_t2005439303), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2475[8] = 
{
	DateComponents_t2005439303::get_offset_of_Year_0(),
	DateComponents_t2005439303::get_offset_of_Month_1(),
	DateComponents_t2005439303::get_offset_of_Day_2(),
	DateComponents_t2005439303::get_offset_of_Hour_3(),
	DateComponents_t2005439303::get_offset_of_Minute_4(),
	DateComponents_t2005439303::get_offset_of_Second_5(),
	DateComponents_t2005439303::get_offset_of_Weekday_6(),
	DateComponents_t2005439303::get_offset_of_Quarter_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2476 = { sizeof (CalendarTrigger_t159864792), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2476[1] = 
{
	CalendarTrigger_t159864792::get_offset_of_ComponentsOfDateToFire_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2477 = { sizeof (TimeIntervalTrigger_t2848746056), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2477[1] = 
{
	TimeIntervalTrigger_t2848746056::get_offset_of_intervalToFire_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2478 = { sizeof (IOSSocialManager_t1565743305), -1, sizeof(IOSSocialManager_t1565743305_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2478[9] = 
{
	IOSSocialManager_t1565743305_StaticFields::get_offset_of_OnFacebookPostStart_4(),
	IOSSocialManager_t1565743305_StaticFields::get_offset_of_OnTwitterPostStart_5(),
	IOSSocialManager_t1565743305_StaticFields::get_offset_of_OnInstagramPostStart_6(),
	IOSSocialManager_t1565743305_StaticFields::get_offset_of_OnFacebookPostResult_7(),
	IOSSocialManager_t1565743305_StaticFields::get_offset_of_OnTwitterPostResult_8(),
	IOSSocialManager_t1565743305_StaticFields::get_offset_of_OnInstagramPostResult_9(),
	IOSSocialManager_t1565743305_StaticFields::get_offset_of_OnMailResult_10(),
	IOSSocialManager_t1565743305_StaticFields::get_offset_of_OnTextMessageResult_11(),
	IOSSocialManager_t1565743305_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2479 = { sizeof (TextMessageComposeResult_t1393453087)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2479[5] = 
{
	TextMessageComposeResult_t1393453087::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2480 = { sizeof (BillingInitChecker_t1575809803), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2480[1] = 
{
	BillingInitChecker_t1575809803::get_offset_of__listener_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2481 = { sizeof (BillingInitListener_t1231368121), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2482 = { sizeof (BillingNativeBridge_t1474950826), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2483 = { sizeof (PaymentManager_t249135127), -1, sizeof(PaymentManager_t249135127_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2483[14] = 
{
	0,
	0,
	PaymentManager_t249135127_StaticFields::get_offset_of_OnStoreKitInitComplete_6(),
	PaymentManager_t249135127_StaticFields::get_offset_of_OnRestoreStarted_7(),
	PaymentManager_t249135127_StaticFields::get_offset_of_OnRestoreComplete_8(),
	PaymentManager_t249135127_StaticFields::get_offset_of_OnTransactionStarted_9(),
	PaymentManager_t249135127_StaticFields::get_offset_of_OnTransactionComplete_10(),
	PaymentManager_t249135127_StaticFields::get_offset_of_OnProductPurchasedExternally_11(),
	PaymentManager_t249135127_StaticFields::get_offset_of_OnVerificationComplete_12(),
	PaymentManager_t249135127::get_offset_of__IsStoreLoaded_13(),
	PaymentManager_t249135127::get_offset_of__IsWaitingLoadResult_14(),
	PaymentManager_t249135127_StaticFields::get_offset_of__nextId_15(),
	PaymentManager_t249135127::get_offset_of__productsView_16(),
	PaymentManager_t249135127_StaticFields::get_offset_of_lastPurchasedProduct_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2484 = { sizeof (PriceTier_t132027085)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2484[88] = 
{
	PriceTier_t132027085::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2485 = { sizeof (ProductType_t773894259)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2485[3] = 
{
	ProductType_t773894259::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2486 = { sizeof (PurchaseState_t1124174412)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2486[5] = 
{
	PurchaseState_t1124174412::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2487 = { sizeof (TransactionErrorCode_t4212012852)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2487[10] = 
{
	TransactionErrorCode_t4212012852::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2488 = { sizeof (TransactionsHandlingMode_t1662146145)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2488[3] = 
{
	TransactionsHandlingMode_t1662146145::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2489 = { sizeof (SK_Util_t1571557834), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2490 = { sizeof (StoreProductView_t2572130870), -1, sizeof(StoreProductView_t2572130870_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2490[10] = 
{
	StoreProductView_t2572130870::get_offset_of_Loaded_0(),
	StoreProductView_t2572130870::get_offset_of_LoadFailed_1(),
	StoreProductView_t2572130870::get_offset_of_Appeared_2(),
	StoreProductView_t2572130870::get_offset_of_Dismissed_3(),
	StoreProductView_t2572130870::get_offset_of__id_4(),
	StoreProductView_t2572130870::get_offset_of__ids_5(),
	StoreProductView_t2572130870_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_6(),
	StoreProductView_t2572130870_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_7(),
	StoreProductView_t2572130870_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_8(),
	StoreProductView_t2572130870_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2491 = { sizeof (SK_CloudService_t1139212890), -1, sizeof(SK_CloudService_t1139212890_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2491[3] = 
{
	SK_CloudService_t1139212890_StaticFields::get_offset_of_OnAuthorizationFinished_4(),
	SK_CloudService_t1139212890_StaticFields::get_offset_of_OnCapabilitiesRequestFinished_5(),
	SK_CloudService_t1139212890_StaticFields::get_offset_of_OnStorefrontIdentifierRequestFinished_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2492 = { sizeof (SK_CloudServiceAuthorizationStatus_t2788808645)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2492[5] = 
{
	SK_CloudServiceAuthorizationStatus_t2788808645::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2493 = { sizeof (SK_CloudServiceCapability_t967689214)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2493[4] = 
{
	SK_CloudServiceCapability_t967689214::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2494 = { sizeof (SK_AuthorizationResult_t3504590035), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2494[1] = 
{
	SK_AuthorizationResult_t3504590035::get_offset_of__AuthorizationStatus_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2495 = { sizeof (SK_RequestCapabilitieResult_t4153260424), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2495[1] = 
{
	SK_RequestCapabilitieResult_t4153260424::get_offset_of__Capability_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2496 = { sizeof (SK_RequestStorefrontIdentifierResult_t833766923), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2496[1] = 
{
	SK_RequestStorefrontIdentifierResult_t833766923::get_offset_of__StorefrontIdentifier_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2497 = { sizeof (SK_StoreReviewController_t56888101), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2498 = { sizeof (Product_t3117358678), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2498[12] = 
{
	Product_t3117358678::get_offset_of_IsOpen_0(),
	Product_t3117358678::get_offset_of__IsAvailable_1(),
	Product_t3117358678::get_offset_of__Id_2(),
	Product_t3117358678::get_offset_of__DisplayName_3(),
	Product_t3117358678::get_offset_of__Description_4(),
	Product_t3117358678::get_offset_of__Price_5(),
	Product_t3117358678::get_offset_of__LocalizedPrice_6(),
	Product_t3117358678::get_offset_of__CurrencySymbol_7(),
	Product_t3117358678::get_offset_of__CurrencyCode_8(),
	Product_t3117358678::get_offset_of__Texture_9(),
	Product_t3117358678::get_offset_of__ProductType_10(),
	Product_t3117358678::get_offset_of__PriceTier_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2499 = { sizeof (PurchaseResult_t3226678372), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2499[5] = 
{
	PurchaseResult_t3226678372::get_offset_of__ProductIdentifier_1(),
	PurchaseResult_t3226678372::get_offset_of__State_2(),
	PurchaseResult_t3226678372::get_offset_of__Receipt_3(),
	PurchaseResult_t3226678372::get_offset_of__TransactionIdentifier_4(),
	PurchaseResult_t3226678372::get_offset_of__ApplicationUsername_5(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
