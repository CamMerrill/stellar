﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// SA.Common.Models.ScreenshotMaker
struct ScreenshotMaker_t2906260841;
// UnityEngine.WWW
struct WWW_t3688466362;
// SA.Common.Models.WWWTextureLoader
struct WWWTextureLoader_t1987695874;
// System.String
struct String_t;
// System.Action`1<SA.IOSNative.StoreKit.VerificationResponse>
struct Action_1_t3441667023;
// System.Action`1<SA.Common.Models.Result>
struct Action_1_t47902017;
// System.Action`1<SA.IOSNative.StoreKit.PurchaseResult>
struct Action_1_t3399145967;
// System.Action`1<SA.IOSNative.StoreKit.RestoreResult>
struct Action_1_t1653158450;
// UnityEngine.ResourceRequest
struct ResourceRequest_t3109103591;
// SA.Common.Models.PrefabAsyncLoader
struct PrefabAsyncLoader_t3137627864;
// SA.Common.Models.Error
struct Error_t340543044;
// System.Action`1<System.DateTime>
struct Action_1_t3910997380;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t2865362463;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// MarketExample
struct MarketExample_t1239190970;
// SA.Common.Animation.SA_iTween
struct SA_iTween_t1550711905;
// System.IO.StringReader
struct StringReader_t3465604688;
// System.Text.StringBuilder
struct StringBuilder_t;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// System.IO.MemoryStream
struct MemoryStream_t94973147;
// System.IO.BinaryWriter
struct BinaryWriter_t3992595042;
// System.Uri
struct Uri_t100236324;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Collections.Generic.Dictionary`2<System.String,System.Action`1<SA.IOSNative.Privacy.PermissionStatus>>
struct Dictionary_2_t898888037;
// System.Collections.Generic.List`1<SA.IOSNative.Contacts.PhoneNumber>
struct List_1_t2611822117;
// System.Collections.Generic.List`1<SA.IOSNative.Contacts.Contact>
struct List_1_t3859513436;
// System.Void
struct Void_t1185182177;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// ISN_DeviceGUID
struct ISN_DeviceGUID_t3668829792;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// UnityEngine.GUIStyle
struct GUIStyle_t3956901511;
// SA.IOSNative.UIKit.NativeReceiver
struct NativeReceiver_t3281330226;
// GK_Leaderboard
struct GK_Leaderboard_t591378496;
// System.Action`1<UnityEngine.Texture2D>
struct Action_1_t4012913780;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Texture2D>
struct Dictionary_2_t3625702484;
// ISN_Logger
struct ISN_Logger_t4052948437;
// SA.IOSNative.Gestures.ForceTouch
struct ForceTouch_t2199614965;
// ISN_GestureRecognizer
struct ISN_GestureRecognizer_t3385868019;
// SA.IOSNative.Privacy.NativeReceiver
struct NativeReceiver_t107704069;
// IOSNativeUtility
struct IOSNativeUtility_t3735947664;
// SA.IOSNative.Contacts.ContactStore
struct ContactStore_t1420756449;
// ISN_Security
struct ISN_Security_t2783094519;
// System.Collections.Generic.List`1<System.Collections.Hashtable>
struct List_1_t3325964508;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// SA.Common.Animation.SA_iTween/EasingFunction
struct EasingFunction_t2379729698;
// SA.Common.Animation.SA_iTween/ApplyTween
struct ApplyTween_t3100151106;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1457185986;
// UnityEngine.Color[0...,0...]
struct ColorU5B0___U2C0___U5D_t941916414;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// UnityEngine.Rect[]
struct RectU5BU5D_t2936723554;
// SA.Common.Animation.SA_iTween/CRSpline
struct CRSpline_t2258853177;
// UnityEngine.Transform
struct Transform_t3600365921;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;
// System.Action
struct Action_t1264377477;
// System.Action`1<System.Single>
struct Action_1_t1569734369;
// System.Action`1<UnityEngine.Vector3>
struct Action_1_t3894781059;
// System.Action`1<iCloudData>
struct Action_1_t267273497;
// System.Action`1<TextMessageComposeResult>
struct Action_1_t1565920682;
// System.Action`1<UnityEngine.GameObject>
struct Action_1_t1286104214;
// DisconnectButton
struct DisconnectButton_t3682185937;
// ConnectionButton
struct ConnectionButton_t3407155931;
// ClickManagerExample
struct ClickManagerExample_t3328835271;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;
// System.Action`1<SA.IOSNative.Contacts.ContactsResult>
struct Action_1_t1323995290;
// System.Action`1<ISN_SwipeDirection>
struct Action_1_t3227987809;
// System.Action`1<SA.IOSNative.Gestures.ForceInfo>
struct Action_1_t4106855485;
// System.Action`1<System.String>
struct Action_1_t2019918284;
// System.Action`1<SA.IOSNative.Privacy.PermissionStatus>
struct Action_1_t1113631738;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>>
struct Dictionary_2_t407435505;
// IOSNativePreviewBackButton
struct IOSNativePreviewBackButton_t1249685067;
// System.Action`1<ISN_Locale>
struct Action_1_t3048335611;
// System.Action`1<System.Boolean>
struct Action_1_t269755560;
// System.Action`1<ISN_LocalReceiptResult>
struct Action_1_t1334024509;
// System.Action`1<SA.IOSNative.UserNotifications.NotificationRequest>
struct Action_1_t1794579528;
// System.Action`1<ISN_RemoteNotificationsRegistrationResult>
struct Action_1_t1103427813;
// System.Action`1<System.Collections.Generic.List`1<SA.IOSNative.UserNotifications.NotificationRequest>>
struct Action_1_t3266654270;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CSAVESCREENSHOTU3EC__ITERATOR0_T367501136_H
#define U3CSAVESCREENSHOTU3EC__ITERATOR0_T367501136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Models.ScreenshotMaker/<SaveScreenshot>c__Iterator0
struct  U3CSaveScreenshotU3Ec__Iterator0_t367501136  : public RuntimeObject
{
public:
	// System.Int32 SA.Common.Models.ScreenshotMaker/<SaveScreenshot>c__Iterator0::<width>__0
	int32_t ___U3CwidthU3E__0_0;
	// System.Int32 SA.Common.Models.ScreenshotMaker/<SaveScreenshot>c__Iterator0::<height>__0
	int32_t ___U3CheightU3E__0_1;
	// UnityEngine.Texture2D SA.Common.Models.ScreenshotMaker/<SaveScreenshot>c__Iterator0::<tex>__0
	Texture2D_t3840446185 * ___U3CtexU3E__0_2;
	// SA.Common.Models.ScreenshotMaker SA.Common.Models.ScreenshotMaker/<SaveScreenshot>c__Iterator0::$this
	ScreenshotMaker_t2906260841 * ___U24this_3;
	// System.Object SA.Common.Models.ScreenshotMaker/<SaveScreenshot>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean SA.Common.Models.ScreenshotMaker/<SaveScreenshot>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 SA.Common.Models.ScreenshotMaker/<SaveScreenshot>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CwidthU3E__0_0() { return static_cast<int32_t>(offsetof(U3CSaveScreenshotU3Ec__Iterator0_t367501136, ___U3CwidthU3E__0_0)); }
	inline int32_t get_U3CwidthU3E__0_0() const { return ___U3CwidthU3E__0_0; }
	inline int32_t* get_address_of_U3CwidthU3E__0_0() { return &___U3CwidthU3E__0_0; }
	inline void set_U3CwidthU3E__0_0(int32_t value)
	{
		___U3CwidthU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CheightU3E__0_1() { return static_cast<int32_t>(offsetof(U3CSaveScreenshotU3Ec__Iterator0_t367501136, ___U3CheightU3E__0_1)); }
	inline int32_t get_U3CheightU3E__0_1() const { return ___U3CheightU3E__0_1; }
	inline int32_t* get_address_of_U3CheightU3E__0_1() { return &___U3CheightU3E__0_1; }
	inline void set_U3CheightU3E__0_1(int32_t value)
	{
		___U3CheightU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CtexU3E__0_2() { return static_cast<int32_t>(offsetof(U3CSaveScreenshotU3Ec__Iterator0_t367501136, ___U3CtexU3E__0_2)); }
	inline Texture2D_t3840446185 * get_U3CtexU3E__0_2() const { return ___U3CtexU3E__0_2; }
	inline Texture2D_t3840446185 ** get_address_of_U3CtexU3E__0_2() { return &___U3CtexU3E__0_2; }
	inline void set_U3CtexU3E__0_2(Texture2D_t3840446185 * value)
	{
		___U3CtexU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtexU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CSaveScreenshotU3Ec__Iterator0_t367501136, ___U24this_3)); }
	inline ScreenshotMaker_t2906260841 * get_U24this_3() const { return ___U24this_3; }
	inline ScreenshotMaker_t2906260841 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(ScreenshotMaker_t2906260841 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CSaveScreenshotU3Ec__Iterator0_t367501136, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CSaveScreenshotU3Ec__Iterator0_t367501136, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CSaveScreenshotU3Ec__Iterator0_t367501136, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSAVESCREENSHOTU3EC__ITERATOR0_T367501136_H
#ifndef U3CLOADCOROUTINU3EC__ITERATOR0_T2840076136_H
#define U3CLOADCOROUTINU3EC__ITERATOR0_T2840076136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Models.WWWTextureLoader/<LoadCoroutin>c__Iterator0
struct  U3CLoadCoroutinU3Ec__Iterator0_t2840076136  : public RuntimeObject
{
public:
	// UnityEngine.WWW SA.Common.Models.WWWTextureLoader/<LoadCoroutin>c__Iterator0::<www>__0
	WWW_t3688466362 * ___U3CwwwU3E__0_0;
	// SA.Common.Models.WWWTextureLoader SA.Common.Models.WWWTextureLoader/<LoadCoroutin>c__Iterator0::$this
	WWWTextureLoader_t1987695874 * ___U24this_1;
	// System.Object SA.Common.Models.WWWTextureLoader/<LoadCoroutin>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean SA.Common.Models.WWWTextureLoader/<LoadCoroutin>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 SA.Common.Models.WWWTextureLoader/<LoadCoroutin>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CwwwU3E__0_0() { return static_cast<int32_t>(offsetof(U3CLoadCoroutinU3Ec__Iterator0_t2840076136, ___U3CwwwU3E__0_0)); }
	inline WWW_t3688466362 * get_U3CwwwU3E__0_0() const { return ___U3CwwwU3E__0_0; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwU3E__0_0() { return &___U3CwwwU3E__0_0; }
	inline void set_U3CwwwU3E__0_0(WWW_t3688466362 * value)
	{
		___U3CwwwU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CLoadCoroutinU3Ec__Iterator0_t2840076136, ___U24this_1)); }
	inline WWWTextureLoader_t1987695874 * get_U24this_1() const { return ___U24this_1; }
	inline WWWTextureLoader_t1987695874 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(WWWTextureLoader_t1987695874 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CLoadCoroutinU3Ec__Iterator0_t2840076136, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CLoadCoroutinU3Ec__Iterator0_t2840076136, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CLoadCoroutinU3Ec__Iterator0_t2840076136, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADCOROUTINU3EC__ITERATOR0_T2840076136_H
#ifndef PAYMENTMANAGEREXAMPLE_T503931474_H
#define PAYMENTMANAGEREXAMPLE_T503931474_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PaymentManagerExample
struct  PaymentManagerExample_t503931474  : public RuntimeObject
{
public:
	// System.String PaymentManagerExample::lastTransactionProdudctId
	String_t* ___lastTransactionProdudctId_2;

public:
	inline static int32_t get_offset_of_lastTransactionProdudctId_2() { return static_cast<int32_t>(offsetof(PaymentManagerExample_t503931474, ___lastTransactionProdudctId_2)); }
	inline String_t* get_lastTransactionProdudctId_2() const { return ___lastTransactionProdudctId_2; }
	inline String_t** get_address_of_lastTransactionProdudctId_2() { return &___lastTransactionProdudctId_2; }
	inline void set_lastTransactionProdudctId_2(String_t* value)
	{
		___lastTransactionProdudctId_2 = value;
		Il2CppCodeGenWriteBarrier((&___lastTransactionProdudctId_2), value);
	}
};

struct PaymentManagerExample_t503931474_StaticFields
{
public:
	// System.Boolean PaymentManagerExample::IsInitialized
	bool ___IsInitialized_3;
	// System.Action`1<SA.IOSNative.StoreKit.VerificationResponse> PaymentManagerExample::<>f__mg$cache0
	Action_1_t3441667023 * ___U3CU3Ef__mgU24cache0_4;
	// System.Action`1<SA.Common.Models.Result> PaymentManagerExample::<>f__mg$cache1
	Action_1_t47902017 * ___U3CU3Ef__mgU24cache1_5;
	// System.Action`1<SA.IOSNative.StoreKit.PurchaseResult> PaymentManagerExample::<>f__mg$cache2
	Action_1_t3399145967 * ___U3CU3Ef__mgU24cache2_6;
	// System.Action`1<SA.IOSNative.StoreKit.RestoreResult> PaymentManagerExample::<>f__mg$cache3
	Action_1_t1653158450 * ___U3CU3Ef__mgU24cache3_7;

public:
	inline static int32_t get_offset_of_IsInitialized_3() { return static_cast<int32_t>(offsetof(PaymentManagerExample_t503931474_StaticFields, ___IsInitialized_3)); }
	inline bool get_IsInitialized_3() const { return ___IsInitialized_3; }
	inline bool* get_address_of_IsInitialized_3() { return &___IsInitialized_3; }
	inline void set_IsInitialized_3(bool value)
	{
		___IsInitialized_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_4() { return static_cast<int32_t>(offsetof(PaymentManagerExample_t503931474_StaticFields, ___U3CU3Ef__mgU24cache0_4)); }
	inline Action_1_t3441667023 * get_U3CU3Ef__mgU24cache0_4() const { return ___U3CU3Ef__mgU24cache0_4; }
	inline Action_1_t3441667023 ** get_address_of_U3CU3Ef__mgU24cache0_4() { return &___U3CU3Ef__mgU24cache0_4; }
	inline void set_U3CU3Ef__mgU24cache0_4(Action_1_t3441667023 * value)
	{
		___U3CU3Ef__mgU24cache0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_5() { return static_cast<int32_t>(offsetof(PaymentManagerExample_t503931474_StaticFields, ___U3CU3Ef__mgU24cache1_5)); }
	inline Action_1_t47902017 * get_U3CU3Ef__mgU24cache1_5() const { return ___U3CU3Ef__mgU24cache1_5; }
	inline Action_1_t47902017 ** get_address_of_U3CU3Ef__mgU24cache1_5() { return &___U3CU3Ef__mgU24cache1_5; }
	inline void set_U3CU3Ef__mgU24cache1_5(Action_1_t47902017 * value)
	{
		___U3CU3Ef__mgU24cache1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_6() { return static_cast<int32_t>(offsetof(PaymentManagerExample_t503931474_StaticFields, ___U3CU3Ef__mgU24cache2_6)); }
	inline Action_1_t3399145967 * get_U3CU3Ef__mgU24cache2_6() const { return ___U3CU3Ef__mgU24cache2_6; }
	inline Action_1_t3399145967 ** get_address_of_U3CU3Ef__mgU24cache2_6() { return &___U3CU3Ef__mgU24cache2_6; }
	inline void set_U3CU3Ef__mgU24cache2_6(Action_1_t3399145967 * value)
	{
		___U3CU3Ef__mgU24cache2_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache3_7() { return static_cast<int32_t>(offsetof(PaymentManagerExample_t503931474_StaticFields, ___U3CU3Ef__mgU24cache3_7)); }
	inline Action_1_t1653158450 * get_U3CU3Ef__mgU24cache3_7() const { return ___U3CU3Ef__mgU24cache3_7; }
	inline Action_1_t1653158450 ** get_address_of_U3CU3Ef__mgU24cache3_7() { return &___U3CU3Ef__mgU24cache3_7; }
	inline void set_U3CU3Ef__mgU24cache3_7(Action_1_t1653158450 * value)
	{
		___U3CU3Ef__mgU24cache3_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache3_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAYMENTMANAGEREXAMPLE_T503931474_H
#ifndef U3CLOADU3EC__ITERATOR0_T2595626094_H
#define U3CLOADU3EC__ITERATOR0_T2595626094_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Models.PrefabAsyncLoader/<Load>c__Iterator0
struct  U3CLoadU3Ec__Iterator0_t2595626094  : public RuntimeObject
{
public:
	// UnityEngine.ResourceRequest SA.Common.Models.PrefabAsyncLoader/<Load>c__Iterator0::<request>__0
	ResourceRequest_t3109103591 * ___U3CrequestU3E__0_0;
	// SA.Common.Models.PrefabAsyncLoader SA.Common.Models.PrefabAsyncLoader/<Load>c__Iterator0::$this
	PrefabAsyncLoader_t3137627864 * ___U24this_1;
	// System.Object SA.Common.Models.PrefabAsyncLoader/<Load>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean SA.Common.Models.PrefabAsyncLoader/<Load>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 SA.Common.Models.PrefabAsyncLoader/<Load>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CLoadU3Ec__Iterator0_t2595626094, ___U3CrequestU3E__0_0)); }
	inline ResourceRequest_t3109103591 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline ResourceRequest_t3109103591 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(ResourceRequest_t3109103591 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CLoadU3Ec__Iterator0_t2595626094, ___U24this_1)); }
	inline PrefabAsyncLoader_t3137627864 * get_U24this_1() const { return ___U24this_1; }
	inline PrefabAsyncLoader_t3137627864 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(PrefabAsyncLoader_t3137627864 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CLoadU3Ec__Iterator0_t2595626094, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CLoadU3Ec__Iterator0_t2595626094, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CLoadU3Ec__Iterator0_t2595626094, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADU3EC__ITERATOR0_T2595626094_H
#ifndef RESULT_T4170401718_H
#define RESULT_T4170401718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Models.Result
struct  Result_t4170401718  : public RuntimeObject
{
public:
	// SA.Common.Models.Error SA.Common.Models.Result::_Error
	Error_t340543044 * ____Error_0;

public:
	inline static int32_t get_offset_of__Error_0() { return static_cast<int32_t>(offsetof(Result_t4170401718, ____Error_0)); }
	inline Error_t340543044 * get__Error_0() const { return ____Error_0; }
	inline Error_t340543044 ** get_address_of__Error_0() { return &____Error_0; }
	inline void set__Error_0(Error_t340543044 * value)
	{
		____Error_0 = value;
		Il2CppCodeGenWriteBarrier((&____Error_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESULT_T4170401718_H
#ifndef ERROR_T340543044_H
#define ERROR_T340543044_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Models.Error
struct  Error_t340543044  : public RuntimeObject
{
public:
	// System.Int32 SA.Common.Models.Error::_Code
	int32_t ____Code_0;
	// System.String SA.Common.Models.Error::_Messgae
	String_t* ____Messgae_1;

public:
	inline static int32_t get_offset_of__Code_0() { return static_cast<int32_t>(offsetof(Error_t340543044, ____Code_0)); }
	inline int32_t get__Code_0() const { return ____Code_0; }
	inline int32_t* get_address_of__Code_0() { return &____Code_0; }
	inline void set__Code_0(int32_t value)
	{
		____Code_0 = value;
	}

	inline static int32_t get_offset_of__Messgae_1() { return static_cast<int32_t>(offsetof(Error_t340543044, ____Messgae_1)); }
	inline String_t* get__Messgae_1() const { return ____Messgae_1; }
	inline String_t** get_address_of__Messgae_1() { return &____Messgae_1; }
	inline void set__Messgae_1(String_t* value)
	{
		____Messgae_1 = value;
		Il2CppCodeGenWriteBarrier((&____Messgae_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERROR_T340543044_H
#ifndef CONVERTER_T2106512330_H
#define CONVERTER_T2106512330_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Data.Converter
struct  Converter_t2106512330  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONVERTER_T2106512330_H
#ifndef CALENDAR_T3271124058_H
#define CALENDAR_T3271124058_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSNative.UIKit.Calendar
struct  Calendar_t3271124058  : public RuntimeObject
{
public:

public:
};

struct Calendar_t3271124058_StaticFields
{
public:
	// System.Action`1<System.DateTime> SA.IOSNative.UIKit.Calendar::OnCalendarClosed
	Action_1_t3910997380 * ___OnCalendarClosed_0;

public:
	inline static int32_t get_offset_of_OnCalendarClosed_0() { return static_cast<int32_t>(offsetof(Calendar_t3271124058_StaticFields, ___OnCalendarClosed_0)); }
	inline Action_1_t3910997380 * get_OnCalendarClosed_0() const { return ___OnCalendarClosed_0; }
	inline Action_1_t3910997380 ** get_address_of_OnCalendarClosed_0() { return &___OnCalendarClosed_0; }
	inline void set_OnCalendarClosed_0(Action_1_t3910997380 * value)
	{
		___OnCalendarClosed_0 = value;
		Il2CppCodeGenWriteBarrier((&___OnCalendarClosed_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALENDAR_T3271124058_H
#ifndef ISN_LOCALE_T2875868016_H
#define ISN_LOCALE_T2875868016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ISN_Locale
struct  ISN_Locale_t2875868016  : public RuntimeObject
{
public:
	// System.String ISN_Locale::_CountryCode
	String_t* ____CountryCode_0;
	// System.String ISN_Locale::_DisplayCountry
	String_t* ____DisplayCountry_1;
	// System.String ISN_Locale::_LanguageCode
	String_t* ____LanguageCode_2;
	// System.String ISN_Locale::_DisplayLanguage
	String_t* ____DisplayLanguage_3;

public:
	inline static int32_t get_offset_of__CountryCode_0() { return static_cast<int32_t>(offsetof(ISN_Locale_t2875868016, ____CountryCode_0)); }
	inline String_t* get__CountryCode_0() const { return ____CountryCode_0; }
	inline String_t** get_address_of__CountryCode_0() { return &____CountryCode_0; }
	inline void set__CountryCode_0(String_t* value)
	{
		____CountryCode_0 = value;
		Il2CppCodeGenWriteBarrier((&____CountryCode_0), value);
	}

	inline static int32_t get_offset_of__DisplayCountry_1() { return static_cast<int32_t>(offsetof(ISN_Locale_t2875868016, ____DisplayCountry_1)); }
	inline String_t* get__DisplayCountry_1() const { return ____DisplayCountry_1; }
	inline String_t** get_address_of__DisplayCountry_1() { return &____DisplayCountry_1; }
	inline void set__DisplayCountry_1(String_t* value)
	{
		____DisplayCountry_1 = value;
		Il2CppCodeGenWriteBarrier((&____DisplayCountry_1), value);
	}

	inline static int32_t get_offset_of__LanguageCode_2() { return static_cast<int32_t>(offsetof(ISN_Locale_t2875868016, ____LanguageCode_2)); }
	inline String_t* get__LanguageCode_2() const { return ____LanguageCode_2; }
	inline String_t** get_address_of__LanguageCode_2() { return &____LanguageCode_2; }
	inline void set__LanguageCode_2(String_t* value)
	{
		____LanguageCode_2 = value;
		Il2CppCodeGenWriteBarrier((&____LanguageCode_2), value);
	}

	inline static int32_t get_offset_of__DisplayLanguage_3() { return static_cast<int32_t>(offsetof(ISN_Locale_t2875868016, ____DisplayLanguage_3)); }
	inline String_t* get__DisplayLanguage_3() const { return ____DisplayLanguage_3; }
	inline String_t** get_address_of__DisplayLanguage_3() { return &____DisplayLanguage_3; }
	inline void set__DisplayLanguage_3(String_t* value)
	{
		____DisplayLanguage_3 = value;
		Il2CppCodeGenWriteBarrier((&____DisplayLanguage_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISN_LOCALE_T2875868016_H
#ifndef U3CSENDREQUESTU3EC__ITERATOR0_T2526093774_H
#define U3CSENDREQUESTU3EC__ITERATOR0_T2526093774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MarketExample/<SendRequest>c__Iterator0
struct  U3CSendRequestU3Ec__Iterator0_t2526093774  : public RuntimeObject
{
public:
	// System.String MarketExample/<SendRequest>c__Iterator0::<base64string>__0
	String_t* ___U3Cbase64stringU3E__0_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> MarketExample/<SendRequest>c__Iterator0::<OriginalJSON>__0
	Dictionary_2_t2865362463 * ___U3COriginalJSONU3E__0_1;
	// System.String MarketExample/<SendRequest>c__Iterator0::<data>__0
	String_t* ___U3CdataU3E__0_2;
	// System.Byte[] MarketExample/<SendRequest>c__Iterator0::<binaryData>__0
	ByteU5BU5D_t4116647657* ___U3CbinaryDataU3E__0_3;
	// UnityEngine.WWW MarketExample/<SendRequest>c__Iterator0::<www>__0
	WWW_t3688466362 * ___U3CwwwU3E__0_4;
	// MarketExample MarketExample/<SendRequest>c__Iterator0::$this
	MarketExample_t1239190970 * ___U24this_5;
	// System.Object MarketExample/<SendRequest>c__Iterator0::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean MarketExample/<SendRequest>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 MarketExample/<SendRequest>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3Cbase64stringU3E__0_0() { return static_cast<int32_t>(offsetof(U3CSendRequestU3Ec__Iterator0_t2526093774, ___U3Cbase64stringU3E__0_0)); }
	inline String_t* get_U3Cbase64stringU3E__0_0() const { return ___U3Cbase64stringU3E__0_0; }
	inline String_t** get_address_of_U3Cbase64stringU3E__0_0() { return &___U3Cbase64stringU3E__0_0; }
	inline void set_U3Cbase64stringU3E__0_0(String_t* value)
	{
		___U3Cbase64stringU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cbase64stringU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3COriginalJSONU3E__0_1() { return static_cast<int32_t>(offsetof(U3CSendRequestU3Ec__Iterator0_t2526093774, ___U3COriginalJSONU3E__0_1)); }
	inline Dictionary_2_t2865362463 * get_U3COriginalJSONU3E__0_1() const { return ___U3COriginalJSONU3E__0_1; }
	inline Dictionary_2_t2865362463 ** get_address_of_U3COriginalJSONU3E__0_1() { return &___U3COriginalJSONU3E__0_1; }
	inline void set_U3COriginalJSONU3E__0_1(Dictionary_2_t2865362463 * value)
	{
		___U3COriginalJSONU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3COriginalJSONU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CdataU3E__0_2() { return static_cast<int32_t>(offsetof(U3CSendRequestU3Ec__Iterator0_t2526093774, ___U3CdataU3E__0_2)); }
	inline String_t* get_U3CdataU3E__0_2() const { return ___U3CdataU3E__0_2; }
	inline String_t** get_address_of_U3CdataU3E__0_2() { return &___U3CdataU3E__0_2; }
	inline void set_U3CdataU3E__0_2(String_t* value)
	{
		___U3CdataU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdataU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CbinaryDataU3E__0_3() { return static_cast<int32_t>(offsetof(U3CSendRequestU3Ec__Iterator0_t2526093774, ___U3CbinaryDataU3E__0_3)); }
	inline ByteU5BU5D_t4116647657* get_U3CbinaryDataU3E__0_3() const { return ___U3CbinaryDataU3E__0_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_U3CbinaryDataU3E__0_3() { return &___U3CbinaryDataU3E__0_3; }
	inline void set_U3CbinaryDataU3E__0_3(ByteU5BU5D_t4116647657* value)
	{
		___U3CbinaryDataU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbinaryDataU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_4() { return static_cast<int32_t>(offsetof(U3CSendRequestU3Ec__Iterator0_t2526093774, ___U3CwwwU3E__0_4)); }
	inline WWW_t3688466362 * get_U3CwwwU3E__0_4() const { return ___U3CwwwU3E__0_4; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwU3E__0_4() { return &___U3CwwwU3E__0_4; }
	inline void set_U3CwwwU3E__0_4(WWW_t3688466362 * value)
	{
		___U3CwwwU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_4), value);
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CSendRequestU3Ec__Iterator0_t2526093774, ___U24this_5)); }
	inline MarketExample_t1239190970 * get_U24this_5() const { return ___U24this_5; }
	inline MarketExample_t1239190970 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(MarketExample_t1239190970 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CSendRequestU3Ec__Iterator0_t2526093774, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CSendRequestU3Ec__Iterator0_t2526093774, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CSendRequestU3Ec__Iterator0_t2526093774, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSENDREQUESTU3EC__ITERATOR0_T2526093774_H
#ifndef CONFIG_T3619392629_H
#define CONFIG_T3619392629_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Config
struct  Config_t3619392629  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIG_T3619392629_H
#ifndef SA_TWEENEXTENSIONS_T1686218418_H
#define SA_TWEENEXTENSIONS_T1686218418_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Extensions.SA_TweenExtensions
struct  SA_TweenExtensions_t1686218418  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SA_TWEENEXTENSIONS_T1686218418_H
#ifndef U3CSTARTU3EC__ITERATOR2_T3890060288_H
#define U3CSTARTU3EC__ITERATOR2_T3890060288_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Animation.SA_iTween/<Start>c__Iterator2
struct  U3CStartU3Ec__Iterator2_t3890060288  : public RuntimeObject
{
public:
	// SA.Common.Animation.SA_iTween SA.Common.Animation.SA_iTween/<Start>c__Iterator2::$this
	SA_iTween_t1550711905 * ___U24this_0;
	// System.Object SA.Common.Animation.SA_iTween/<Start>c__Iterator2::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean SA.Common.Animation.SA_iTween/<Start>c__Iterator2::$disposing
	bool ___U24disposing_2;
	// System.Int32 SA.Common.Animation.SA_iTween/<Start>c__Iterator2::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator2_t3890060288, ___U24this_0)); }
	inline SA_iTween_t1550711905 * get_U24this_0() const { return ___U24this_0; }
	inline SA_iTween_t1550711905 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(SA_iTween_t1550711905 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator2_t3890060288, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator2_t3890060288, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator2_t3890060288, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR2_T3890060288_H
#ifndef U3CPOSTTWITTERSCREENSHOTU3EC__ITERATOR2_T1119173099_H
#define U3CPOSTTWITTERSCREENSHOTU3EC__ITERATOR2_T1119173099_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IOSSocialUseExample/<PostTwitterScreenshot>c__Iterator2
struct  U3CPostTwitterScreenshotU3Ec__Iterator2_t1119173099  : public RuntimeObject
{
public:
	// System.Int32 IOSSocialUseExample/<PostTwitterScreenshot>c__Iterator2::<width>__0
	int32_t ___U3CwidthU3E__0_0;
	// System.Int32 IOSSocialUseExample/<PostTwitterScreenshot>c__Iterator2::<height>__0
	int32_t ___U3CheightU3E__0_1;
	// UnityEngine.Texture2D IOSSocialUseExample/<PostTwitterScreenshot>c__Iterator2::<tex>__0
	Texture2D_t3840446185 * ___U3CtexU3E__0_2;
	// System.Object IOSSocialUseExample/<PostTwitterScreenshot>c__Iterator2::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean IOSSocialUseExample/<PostTwitterScreenshot>c__Iterator2::$disposing
	bool ___U24disposing_4;
	// System.Int32 IOSSocialUseExample/<PostTwitterScreenshot>c__Iterator2::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CwidthU3E__0_0() { return static_cast<int32_t>(offsetof(U3CPostTwitterScreenshotU3Ec__Iterator2_t1119173099, ___U3CwidthU3E__0_0)); }
	inline int32_t get_U3CwidthU3E__0_0() const { return ___U3CwidthU3E__0_0; }
	inline int32_t* get_address_of_U3CwidthU3E__0_0() { return &___U3CwidthU3E__0_0; }
	inline void set_U3CwidthU3E__0_0(int32_t value)
	{
		___U3CwidthU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CheightU3E__0_1() { return static_cast<int32_t>(offsetof(U3CPostTwitterScreenshotU3Ec__Iterator2_t1119173099, ___U3CheightU3E__0_1)); }
	inline int32_t get_U3CheightU3E__0_1() const { return ___U3CheightU3E__0_1; }
	inline int32_t* get_address_of_U3CheightU3E__0_1() { return &___U3CheightU3E__0_1; }
	inline void set_U3CheightU3E__0_1(int32_t value)
	{
		___U3CheightU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CtexU3E__0_2() { return static_cast<int32_t>(offsetof(U3CPostTwitterScreenshotU3Ec__Iterator2_t1119173099, ___U3CtexU3E__0_2)); }
	inline Texture2D_t3840446185 * get_U3CtexU3E__0_2() const { return ___U3CtexU3E__0_2; }
	inline Texture2D_t3840446185 ** get_address_of_U3CtexU3E__0_2() { return &___U3CtexU3E__0_2; }
	inline void set_U3CtexU3E__0_2(Texture2D_t3840446185 * value)
	{
		___U3CtexU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtexU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CPostTwitterScreenshotU3Ec__Iterator2_t1119173099, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CPostTwitterScreenshotU3Ec__Iterator2_t1119173099, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CPostTwitterScreenshotU3Ec__Iterator2_t1119173099, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPOSTTWITTERSCREENSHOTU3EC__ITERATOR2_T1119173099_H
#ifndef U3CPOSTSCREENSHOTU3EC__ITERATOR1_T386821259_H
#define U3CPOSTSCREENSHOTU3EC__ITERATOR1_T386821259_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IOSSocialUseExample/<PostScreenshot>c__Iterator1
struct  U3CPostScreenshotU3Ec__Iterator1_t386821259  : public RuntimeObject
{
public:
	// System.Int32 IOSSocialUseExample/<PostScreenshot>c__Iterator1::<width>__0
	int32_t ___U3CwidthU3E__0_0;
	// System.Int32 IOSSocialUseExample/<PostScreenshot>c__Iterator1::<height>__0
	int32_t ___U3CheightU3E__0_1;
	// UnityEngine.Texture2D IOSSocialUseExample/<PostScreenshot>c__Iterator1::<tex>__0
	Texture2D_t3840446185 * ___U3CtexU3E__0_2;
	// System.Object IOSSocialUseExample/<PostScreenshot>c__Iterator1::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean IOSSocialUseExample/<PostScreenshot>c__Iterator1::$disposing
	bool ___U24disposing_4;
	// System.Int32 IOSSocialUseExample/<PostScreenshot>c__Iterator1::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CwidthU3E__0_0() { return static_cast<int32_t>(offsetof(U3CPostScreenshotU3Ec__Iterator1_t386821259, ___U3CwidthU3E__0_0)); }
	inline int32_t get_U3CwidthU3E__0_0() const { return ___U3CwidthU3E__0_0; }
	inline int32_t* get_address_of_U3CwidthU3E__0_0() { return &___U3CwidthU3E__0_0; }
	inline void set_U3CwidthU3E__0_0(int32_t value)
	{
		___U3CwidthU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CheightU3E__0_1() { return static_cast<int32_t>(offsetof(U3CPostScreenshotU3Ec__Iterator1_t386821259, ___U3CheightU3E__0_1)); }
	inline int32_t get_U3CheightU3E__0_1() const { return ___U3CheightU3E__0_1; }
	inline int32_t* get_address_of_U3CheightU3E__0_1() { return &___U3CheightU3E__0_1; }
	inline void set_U3CheightU3E__0_1(int32_t value)
	{
		___U3CheightU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CtexU3E__0_2() { return static_cast<int32_t>(offsetof(U3CPostScreenshotU3Ec__Iterator1_t386821259, ___U3CtexU3E__0_2)); }
	inline Texture2D_t3840446185 * get_U3CtexU3E__0_2() const { return ___U3CtexU3E__0_2; }
	inline Texture2D_t3840446185 ** get_address_of_U3CtexU3E__0_2() { return &___U3CtexU3E__0_2; }
	inline void set_U3CtexU3E__0_2(Texture2D_t3840446185 * value)
	{
		___U3CtexU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtexU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CPostScreenshotU3Ec__Iterator1_t386821259, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CPostScreenshotU3Ec__Iterator1_t386821259, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CPostScreenshotU3Ec__Iterator1_t386821259, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPOSTSCREENSHOTU3EC__ITERATOR1_T386821259_H
#ifndef U3CPOSTSCREENSHOTINSTAGRAMU3EC__ITERATOR0_T1032474826_H
#define U3CPOSTSCREENSHOTINSTAGRAMU3EC__ITERATOR0_T1032474826_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IOSSocialUseExample/<PostScreenshotInstagram>c__Iterator0
struct  U3CPostScreenshotInstagramU3Ec__Iterator0_t1032474826  : public RuntimeObject
{
public:
	// System.Int32 IOSSocialUseExample/<PostScreenshotInstagram>c__Iterator0::<width>__0
	int32_t ___U3CwidthU3E__0_0;
	// System.Int32 IOSSocialUseExample/<PostScreenshotInstagram>c__Iterator0::<height>__0
	int32_t ___U3CheightU3E__0_1;
	// UnityEngine.Texture2D IOSSocialUseExample/<PostScreenshotInstagram>c__Iterator0::<tex>__0
	Texture2D_t3840446185 * ___U3CtexU3E__0_2;
	// System.Object IOSSocialUseExample/<PostScreenshotInstagram>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean IOSSocialUseExample/<PostScreenshotInstagram>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 IOSSocialUseExample/<PostScreenshotInstagram>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CwidthU3E__0_0() { return static_cast<int32_t>(offsetof(U3CPostScreenshotInstagramU3Ec__Iterator0_t1032474826, ___U3CwidthU3E__0_0)); }
	inline int32_t get_U3CwidthU3E__0_0() const { return ___U3CwidthU3E__0_0; }
	inline int32_t* get_address_of_U3CwidthU3E__0_0() { return &___U3CwidthU3E__0_0; }
	inline void set_U3CwidthU3E__0_0(int32_t value)
	{
		___U3CwidthU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CheightU3E__0_1() { return static_cast<int32_t>(offsetof(U3CPostScreenshotInstagramU3Ec__Iterator0_t1032474826, ___U3CheightU3E__0_1)); }
	inline int32_t get_U3CheightU3E__0_1() const { return ___U3CheightU3E__0_1; }
	inline int32_t* get_address_of_U3CheightU3E__0_1() { return &___U3CheightU3E__0_1; }
	inline void set_U3CheightU3E__0_1(int32_t value)
	{
		___U3CheightU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CtexU3E__0_2() { return static_cast<int32_t>(offsetof(U3CPostScreenshotInstagramU3Ec__Iterator0_t1032474826, ___U3CtexU3E__0_2)); }
	inline Texture2D_t3840446185 * get_U3CtexU3E__0_2() const { return ___U3CtexU3E__0_2; }
	inline Texture2D_t3840446185 ** get_address_of_U3CtexU3E__0_2() { return &___U3CtexU3E__0_2; }
	inline void set_U3CtexU3E__0_2(Texture2D_t3840446185 * value)
	{
		___U3CtexU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtexU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CPostScreenshotInstagramU3Ec__Iterator0_t1032474826, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CPostScreenshotInstagramU3Ec__Iterator0_t1032474826, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CPostScreenshotInstagramU3Ec__Iterator0_t1032474826, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPOSTSCREENSHOTINSTAGRAMU3EC__ITERATOR0_T1032474826_H
#ifndef JSON_T4217458989_H
#define JSON_T4217458989_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Data.Json
struct  Json_t4217458989  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSON_T4217458989_H
#ifndef PARSER_T3245694681_H
#define PARSER_T3245694681_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Data.Json/Parser
struct  Parser_t3245694681  : public RuntimeObject
{
public:
	// System.IO.StringReader SA.Common.Data.Json/Parser::json
	StringReader_t3465604688 * ___json_2;

public:
	inline static int32_t get_offset_of_json_2() { return static_cast<int32_t>(offsetof(Parser_t3245694681, ___json_2)); }
	inline StringReader_t3465604688 * get_json_2() const { return ___json_2; }
	inline StringReader_t3465604688 ** get_address_of_json_2() { return &___json_2; }
	inline void set_json_2(StringReader_t3465604688 * value)
	{
		___json_2 = value;
		Il2CppCodeGenWriteBarrier((&___json_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSER_T3245694681_H
#ifndef U3CPOSTFBSCREENSHOTU3EC__ITERATOR3_T388533558_H
#define U3CPOSTFBSCREENSHOTU3EC__ITERATOR3_T388533558_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IOSSocialUseExample/<PostFBScreenshot>c__Iterator3
struct  U3CPostFBScreenshotU3Ec__Iterator3_t388533558  : public RuntimeObject
{
public:
	// System.Int32 IOSSocialUseExample/<PostFBScreenshot>c__Iterator3::<width>__0
	int32_t ___U3CwidthU3E__0_0;
	// System.Int32 IOSSocialUseExample/<PostFBScreenshot>c__Iterator3::<height>__0
	int32_t ___U3CheightU3E__0_1;
	// UnityEngine.Texture2D IOSSocialUseExample/<PostFBScreenshot>c__Iterator3::<tex>__0
	Texture2D_t3840446185 * ___U3CtexU3E__0_2;
	// System.Object IOSSocialUseExample/<PostFBScreenshot>c__Iterator3::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean IOSSocialUseExample/<PostFBScreenshot>c__Iterator3::$disposing
	bool ___U24disposing_4;
	// System.Int32 IOSSocialUseExample/<PostFBScreenshot>c__Iterator3::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CwidthU3E__0_0() { return static_cast<int32_t>(offsetof(U3CPostFBScreenshotU3Ec__Iterator3_t388533558, ___U3CwidthU3E__0_0)); }
	inline int32_t get_U3CwidthU3E__0_0() const { return ___U3CwidthU3E__0_0; }
	inline int32_t* get_address_of_U3CwidthU3E__0_0() { return &___U3CwidthU3E__0_0; }
	inline void set_U3CwidthU3E__0_0(int32_t value)
	{
		___U3CwidthU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CheightU3E__0_1() { return static_cast<int32_t>(offsetof(U3CPostFBScreenshotU3Ec__Iterator3_t388533558, ___U3CheightU3E__0_1)); }
	inline int32_t get_U3CheightU3E__0_1() const { return ___U3CheightU3E__0_1; }
	inline int32_t* get_address_of_U3CheightU3E__0_1() { return &___U3CheightU3E__0_1; }
	inline void set_U3CheightU3E__0_1(int32_t value)
	{
		___U3CheightU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CtexU3E__0_2() { return static_cast<int32_t>(offsetof(U3CPostFBScreenshotU3Ec__Iterator3_t388533558, ___U3CtexU3E__0_2)); }
	inline Texture2D_t3840446185 * get_U3CtexU3E__0_2() const { return ___U3CtexU3E__0_2; }
	inline Texture2D_t3840446185 ** get_address_of_U3CtexU3E__0_2() { return &___U3CtexU3E__0_2; }
	inline void set_U3CtexU3E__0_2(Texture2D_t3840446185 * value)
	{
		___U3CtexU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtexU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CPostFBScreenshotU3Ec__Iterator3_t388533558, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CPostFBScreenshotU3Ec__Iterator3_t388533558, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CPostFBScreenshotU3Ec__Iterator3_t388533558, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPOSTFBSCREENSHOTU3EC__ITERATOR3_T388533558_H
#ifndef SERIALIZER_T2172110415_H
#define SERIALIZER_T2172110415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Data.Json/Serializer
struct  Serializer_t2172110415  : public RuntimeObject
{
public:
	// System.Text.StringBuilder SA.Common.Data.Json/Serializer::builder
	StringBuilder_t * ___builder_0;

public:
	inline static int32_t get_offset_of_builder_0() { return static_cast<int32_t>(offsetof(Serializer_t2172110415, ___builder_0)); }
	inline StringBuilder_t * get_builder_0() const { return ___builder_0; }
	inline StringBuilder_t ** get_address_of_builder_0() { return &___builder_0; }
	inline void set_builder_0(StringBuilder_t * value)
	{
		___builder_0 = value;
		Il2CppCodeGenWriteBarrier((&___builder_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZER_T2172110415_H
#ifndef U3CTWEENDELAYU3EC__ITERATOR0_T2393992106_H
#define U3CTWEENDELAYU3EC__ITERATOR0_T2393992106_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Animation.SA_iTween/<TweenDelay>c__Iterator0
struct  U3CTweenDelayU3Ec__Iterator0_t2393992106  : public RuntimeObject
{
public:
	// SA.Common.Animation.SA_iTween SA.Common.Animation.SA_iTween/<TweenDelay>c__Iterator0::$this
	SA_iTween_t1550711905 * ___U24this_0;
	// System.Object SA.Common.Animation.SA_iTween/<TweenDelay>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean SA.Common.Animation.SA_iTween/<TweenDelay>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 SA.Common.Animation.SA_iTween/<TweenDelay>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CTweenDelayU3Ec__Iterator0_t2393992106, ___U24this_0)); }
	inline SA_iTween_t1550711905 * get_U24this_0() const { return ___U24this_0; }
	inline SA_iTween_t1550711905 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(SA_iTween_t1550711905 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CTweenDelayU3Ec__Iterator0_t2393992106, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CTweenDelayU3Ec__Iterator0_t2393992106, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CTweenDelayU3Ec__Iterator0_t2393992106, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTWEENDELAYU3EC__ITERATOR0_T2393992106_H
#ifndef NETWORKMANAGEREXAMPLE_T3775854059_H
#define NETWORKMANAGEREXAMPLE_T3775854059_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NetworkManagerExample
struct  NetworkManagerExample_t3775854059  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKMANAGEREXAMPLE_T3775854059_H
#ifndef U3CTWEENRESTARTU3EC__ITERATOR1_T3512026842_H
#define U3CTWEENRESTARTU3EC__ITERATOR1_T3512026842_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Animation.SA_iTween/<TweenRestart>c__Iterator1
struct  U3CTweenRestartU3Ec__Iterator1_t3512026842  : public RuntimeObject
{
public:
	// SA.Common.Animation.SA_iTween SA.Common.Animation.SA_iTween/<TweenRestart>c__Iterator1::$this
	SA_iTween_t1550711905 * ___U24this_0;
	// System.Object SA.Common.Animation.SA_iTween/<TweenRestart>c__Iterator1::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean SA.Common.Animation.SA_iTween/<TweenRestart>c__Iterator1::$disposing
	bool ___U24disposing_2;
	// System.Int32 SA.Common.Animation.SA_iTween/<TweenRestart>c__Iterator1::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CTweenRestartU3Ec__Iterator1_t3512026842, ___U24this_0)); }
	inline SA_iTween_t1550711905 * get_U24this_0() const { return ___U24this_0; }
	inline SA_iTween_t1550711905 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(SA_iTween_t1550711905 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CTweenRestartU3Ec__Iterator1_t3512026842, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CTweenRestartU3Ec__Iterator1_t3512026842, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CTweenRestartU3Ec__Iterator1_t3512026842, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTWEENRESTARTU3EC__ITERATOR1_T3512026842_H
#ifndef CRSPLINE_T2258853177_H
#define CRSPLINE_T2258853177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Animation.SA_iTween/CRSpline
struct  CRSpline_t2258853177  : public RuntimeObject
{
public:
	// UnityEngine.Vector3[] SA.Common.Animation.SA_iTween/CRSpline::pts
	Vector3U5BU5D_t1718750761* ___pts_0;

public:
	inline static int32_t get_offset_of_pts_0() { return static_cast<int32_t>(offsetof(CRSpline_t2258853177, ___pts_0)); }
	inline Vector3U5BU5D_t1718750761* get_pts_0() const { return ___pts_0; }
	inline Vector3U5BU5D_t1718750761** get_address_of_pts_0() { return &___pts_0; }
	inline void set_pts_0(Vector3U5BU5D_t1718750761* value)
	{
		___pts_0 = value;
		Il2CppCodeGenWriteBarrier((&___pts_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRSPLINE_T2258853177_H
#ifndef BYTEBUFFER_T4262155383_H
#define BYTEBUFFER_T4262155383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ByteBuffer
struct  ByteBuffer_t4262155383  : public RuntimeObject
{
public:
	// System.Byte[] ByteBuffer::buffer
	ByteU5BU5D_t4116647657* ___buffer_0;
	// System.Int32 ByteBuffer::pointer
	int32_t ___pointer_1;

public:
	inline static int32_t get_offset_of_buffer_0() { return static_cast<int32_t>(offsetof(ByteBuffer_t4262155383, ___buffer_0)); }
	inline ByteU5BU5D_t4116647657* get_buffer_0() const { return ___buffer_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_buffer_0() { return &___buffer_0; }
	inline void set_buffer_0(ByteU5BU5D_t4116647657* value)
	{
		___buffer_0 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_0), value);
	}

	inline static int32_t get_offset_of_pointer_1() { return static_cast<int32_t>(offsetof(ByteBuffer_t4262155383, ___pointer_1)); }
	inline int32_t get_pointer_1() const { return ___pointer_1; }
	inline int32_t* get_address_of_pointer_1() { return &___pointer_1; }
	inline void set_pointer_1(int32_t value)
	{
		___pointer_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTEBUFFER_T4262155383_H
#ifndef BASEPACKAGE_T3538619746_H
#define BASEPACKAGE_T3538619746_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BasePackage
struct  BasePackage_t3538619746  : public RuntimeObject
{
public:
	// System.IO.MemoryStream BasePackage::buffer
	MemoryStream_t94973147 * ___buffer_0;
	// System.IO.BinaryWriter BasePackage::writer
	BinaryWriter_t3992595042 * ___writer_1;

public:
	inline static int32_t get_offset_of_buffer_0() { return static_cast<int32_t>(offsetof(BasePackage_t3538619746, ___buffer_0)); }
	inline MemoryStream_t94973147 * get_buffer_0() const { return ___buffer_0; }
	inline MemoryStream_t94973147 ** get_address_of_buffer_0() { return &___buffer_0; }
	inline void set_buffer_0(MemoryStream_t94973147 * value)
	{
		___buffer_0 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_0), value);
	}

	inline static int32_t get_offset_of_writer_1() { return static_cast<int32_t>(offsetof(BasePackage_t3538619746, ___writer_1)); }
	inline BinaryWriter_t3992595042 * get_writer_1() const { return ___writer_1; }
	inline BinaryWriter_t3992595042 ** get_address_of_writer_1() { return &___writer_1; }
	inline void set_writer_1(BinaryWriter_t3992595042 * value)
	{
		___writer_1 = value;
		Il2CppCodeGenWriteBarrier((&___writer_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEPACKAGE_T3538619746_H
#ifndef APPCACHE_T1657377921_H
#define APPCACHE_T1657377921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSNative.Storage.AppCache
struct  AppCache_t1657377921  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPCACHE_T1657377921_H
#ifndef LAUNCHURL_T1254559570_H
#define LAUNCHURL_T1254559570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSNative.Models.LaunchUrl
struct  LaunchUrl_t1254559570  : public RuntimeObject
{
public:
	// System.Uri SA.IOSNative.Models.LaunchUrl::_URI
	Uri_t100236324 * ____URI_0;
	// System.String SA.IOSNative.Models.LaunchUrl::_AbsoluteUrl
	String_t* ____AbsoluteUrl_1;
	// System.String SA.IOSNative.Models.LaunchUrl::_SourceApplication
	String_t* ____SourceApplication_2;

public:
	inline static int32_t get_offset_of__URI_0() { return static_cast<int32_t>(offsetof(LaunchUrl_t1254559570, ____URI_0)); }
	inline Uri_t100236324 * get__URI_0() const { return ____URI_0; }
	inline Uri_t100236324 ** get_address_of__URI_0() { return &____URI_0; }
	inline void set__URI_0(Uri_t100236324 * value)
	{
		____URI_0 = value;
		Il2CppCodeGenWriteBarrier((&____URI_0), value);
	}

	inline static int32_t get_offset_of__AbsoluteUrl_1() { return static_cast<int32_t>(offsetof(LaunchUrl_t1254559570, ____AbsoluteUrl_1)); }
	inline String_t* get__AbsoluteUrl_1() const { return ____AbsoluteUrl_1; }
	inline String_t** get_address_of__AbsoluteUrl_1() { return &____AbsoluteUrl_1; }
	inline void set__AbsoluteUrl_1(String_t* value)
	{
		____AbsoluteUrl_1 = value;
		Il2CppCodeGenWriteBarrier((&____AbsoluteUrl_1), value);
	}

	inline static int32_t get_offset_of__SourceApplication_2() { return static_cast<int32_t>(offsetof(LaunchUrl_t1254559570, ____SourceApplication_2)); }
	inline String_t* get__SourceApplication_2() const { return ____SourceApplication_2; }
	inline String_t** get_address_of__SourceApplication_2() { return &____SourceApplication_2; }
	inline void set__SourceApplication_2(String_t* value)
	{
		____SourceApplication_2 = value;
		Il2CppCodeGenWriteBarrier((&____SourceApplication_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAUNCHURL_T1254559570_H
#ifndef FORCETOUCHMENUITEM_T113152519_H
#define FORCETOUCHMENUITEM_T113152519_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSNative.Gestures.ForceTouchMenuItem
struct  ForceTouchMenuItem_t113152519  : public RuntimeObject
{
public:
	// System.Boolean SA.IOSNative.Gestures.ForceTouchMenuItem::IsOpen
	bool ___IsOpen_0;
	// System.String SA.IOSNative.Gestures.ForceTouchMenuItem::Title
	String_t* ___Title_1;
	// System.String SA.IOSNative.Gestures.ForceTouchMenuItem::Subtitle
	String_t* ___Subtitle_2;
	// System.String SA.IOSNative.Gestures.ForceTouchMenuItem::Action
	String_t* ___Action_3;

public:
	inline static int32_t get_offset_of_IsOpen_0() { return static_cast<int32_t>(offsetof(ForceTouchMenuItem_t113152519, ___IsOpen_0)); }
	inline bool get_IsOpen_0() const { return ___IsOpen_0; }
	inline bool* get_address_of_IsOpen_0() { return &___IsOpen_0; }
	inline void set_IsOpen_0(bool value)
	{
		___IsOpen_0 = value;
	}

	inline static int32_t get_offset_of_Title_1() { return static_cast<int32_t>(offsetof(ForceTouchMenuItem_t113152519, ___Title_1)); }
	inline String_t* get_Title_1() const { return ___Title_1; }
	inline String_t** get_address_of_Title_1() { return &___Title_1; }
	inline void set_Title_1(String_t* value)
	{
		___Title_1 = value;
		Il2CppCodeGenWriteBarrier((&___Title_1), value);
	}

	inline static int32_t get_offset_of_Subtitle_2() { return static_cast<int32_t>(offsetof(ForceTouchMenuItem_t113152519, ___Subtitle_2)); }
	inline String_t* get_Subtitle_2() const { return ___Subtitle_2; }
	inline String_t** get_address_of_Subtitle_2() { return &___Subtitle_2; }
	inline void set_Subtitle_2(String_t* value)
	{
		___Subtitle_2 = value;
		Il2CppCodeGenWriteBarrier((&___Subtitle_2), value);
	}

	inline static int32_t get_offset_of_Action_3() { return static_cast<int32_t>(offsetof(ForceTouchMenuItem_t113152519, ___Action_3)); }
	inline String_t* get_Action_3() const { return ___Action_3; }
	inline String_t** get_address_of_Action_3() { return &___Action_3; }
	inline void set_Action_3(String_t* value)
	{
		___Action_3 = value;
		Il2CppCodeGenWriteBarrier((&___Action_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORCETOUCHMENUITEM_T113152519_H
#ifndef URLTYPE_T2717836817_H
#define URLTYPE_T2717836817_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSNative.Models.UrlType
struct  UrlType_t2717836817  : public RuntimeObject
{
public:
	// System.String SA.IOSNative.Models.UrlType::Identifier
	String_t* ___Identifier_0;
	// System.Collections.Generic.List`1<System.String> SA.IOSNative.Models.UrlType::Schemes
	List_1_t3319525431 * ___Schemes_1;
	// System.Boolean SA.IOSNative.Models.UrlType::IsOpen
	bool ___IsOpen_2;

public:
	inline static int32_t get_offset_of_Identifier_0() { return static_cast<int32_t>(offsetof(UrlType_t2717836817, ___Identifier_0)); }
	inline String_t* get_Identifier_0() const { return ___Identifier_0; }
	inline String_t** get_address_of_Identifier_0() { return &___Identifier_0; }
	inline void set_Identifier_0(String_t* value)
	{
		___Identifier_0 = value;
		Il2CppCodeGenWriteBarrier((&___Identifier_0), value);
	}

	inline static int32_t get_offset_of_Schemes_1() { return static_cast<int32_t>(offsetof(UrlType_t2717836817, ___Schemes_1)); }
	inline List_1_t3319525431 * get_Schemes_1() const { return ___Schemes_1; }
	inline List_1_t3319525431 ** get_address_of_Schemes_1() { return &___Schemes_1; }
	inline void set_Schemes_1(List_1_t3319525431 * value)
	{
		___Schemes_1 = value;
		Il2CppCodeGenWriteBarrier((&___Schemes_1), value);
	}

	inline static int32_t get_offset_of_IsOpen_2() { return static_cast<int32_t>(offsetof(UrlType_t2717836817, ___IsOpen_2)); }
	inline bool get_IsOpen_2() const { return ___IsOpen_2; }
	inline bool* get_address_of_IsOpen_2() { return &___IsOpen_2; }
	inline void set_IsOpen_2(bool value)
	{
		___IsOpen_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URLTYPE_T2717836817_H
#ifndef UNIVERSALLINK_T4104582537_H
#define UNIVERSALLINK_T4104582537_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSNative.Models.UniversalLink
struct  UniversalLink_t4104582537  : public RuntimeObject
{
public:
	// System.Uri SA.IOSNative.Models.UniversalLink::_URI
	Uri_t100236324 * ____URI_0;
	// System.String SA.IOSNative.Models.UniversalLink::_AbsoluteUrl
	String_t* ____AbsoluteUrl_1;

public:
	inline static int32_t get_offset_of__URI_0() { return static_cast<int32_t>(offsetof(UniversalLink_t4104582537, ____URI_0)); }
	inline Uri_t100236324 * get__URI_0() const { return ____URI_0; }
	inline Uri_t100236324 ** get_address_of__URI_0() { return &____URI_0; }
	inline void set__URI_0(Uri_t100236324 * value)
	{
		____URI_0 = value;
		Il2CppCodeGenWriteBarrier((&____URI_0), value);
	}

	inline static int32_t get_offset_of__AbsoluteUrl_1() { return static_cast<int32_t>(offsetof(UniversalLink_t4104582537, ____AbsoluteUrl_1)); }
	inline String_t* get__AbsoluteUrl_1() const { return ____AbsoluteUrl_1; }
	inline String_t** get_address_of__AbsoluteUrl_1() { return &____AbsoluteUrl_1; }
	inline void set__AbsoluteUrl_1(String_t* value)
	{
		____AbsoluteUrl_1 = value;
		Il2CppCodeGenWriteBarrier((&____AbsoluteUrl_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIVERSALLINK_T4104582537_H
#ifndef PHONENUMBER_T1139747375_H
#define PHONENUMBER_T1139747375_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSNative.Contacts.PhoneNumber
struct  PhoneNumber_t1139747375  : public RuntimeObject
{
public:
	// System.String SA.IOSNative.Contacts.PhoneNumber::CountryCode
	String_t* ___CountryCode_0;
	// System.String SA.IOSNative.Contacts.PhoneNumber::Digits
	String_t* ___Digits_1;

public:
	inline static int32_t get_offset_of_CountryCode_0() { return static_cast<int32_t>(offsetof(PhoneNumber_t1139747375, ___CountryCode_0)); }
	inline String_t* get_CountryCode_0() const { return ___CountryCode_0; }
	inline String_t** get_address_of_CountryCode_0() { return &___CountryCode_0; }
	inline void set_CountryCode_0(String_t* value)
	{
		___CountryCode_0 = value;
		Il2CppCodeGenWriteBarrier((&___CountryCode_0), value);
	}

	inline static int32_t get_offset_of_Digits_1() { return static_cast<int32_t>(offsetof(PhoneNumber_t1139747375, ___Digits_1)); }
	inline String_t* get_Digits_1() const { return ___Digits_1; }
	inline String_t** get_address_of_Digits_1() { return &___Digits_1; }
	inline void set_Digits_1(String_t* value)
	{
		___Digits_1 = value;
		Il2CppCodeGenWriteBarrier((&___Digits_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHONENUMBER_T1139747375_H
#ifndef ISN_DEVICEGUID_T3668829792_H
#define ISN_DEVICEGUID_T3668829792_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ISN_DeviceGUID
struct  ISN_DeviceGUID_t3668829792  : public RuntimeObject
{
public:
	// System.Byte[] ISN_DeviceGUID::_Bytes
	ByteU5BU5D_t4116647657* ____Bytes_0;
	// System.String ISN_DeviceGUID::_Base64
	String_t* ____Base64_1;

public:
	inline static int32_t get_offset_of__Bytes_0() { return static_cast<int32_t>(offsetof(ISN_DeviceGUID_t3668829792, ____Bytes_0)); }
	inline ByteU5BU5D_t4116647657* get__Bytes_0() const { return ____Bytes_0; }
	inline ByteU5BU5D_t4116647657** get_address_of__Bytes_0() { return &____Bytes_0; }
	inline void set__Bytes_0(ByteU5BU5D_t4116647657* value)
	{
		____Bytes_0 = value;
		Il2CppCodeGenWriteBarrier((&____Bytes_0), value);
	}

	inline static int32_t get_offset_of__Base64_1() { return static_cast<int32_t>(offsetof(ISN_DeviceGUID_t3668829792, ____Base64_1)); }
	inline String_t* get__Base64_1() const { return ____Base64_1; }
	inline String_t** get_address_of__Base64_1() { return &____Base64_1; }
	inline void set__Base64_1(String_t* value)
	{
		____Base64_1 = value;
		Il2CppCodeGenWriteBarrier((&____Base64_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISN_DEVICEGUID_T3668829792_H
#ifndef ISN_TIMEZONE_T2672557812_H
#define ISN_TIMEZONE_T2672557812_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ISN_TimeZone
struct  ISN_TimeZone_t2672557812  : public RuntimeObject
{
public:
	// System.Int64 ISN_TimeZone::_SecondsFromGMT
	int64_t ____SecondsFromGMT_0;
	// System.String ISN_TimeZone::_Name
	String_t* ____Name_1;

public:
	inline static int32_t get_offset_of__SecondsFromGMT_0() { return static_cast<int32_t>(offsetof(ISN_TimeZone_t2672557812, ____SecondsFromGMT_0)); }
	inline int64_t get__SecondsFromGMT_0() const { return ____SecondsFromGMT_0; }
	inline int64_t* get_address_of__SecondsFromGMT_0() { return &____SecondsFromGMT_0; }
	inline void set__SecondsFromGMT_0(int64_t value)
	{
		____SecondsFromGMT_0 = value;
	}

	inline static int32_t get_offset_of__Name_1() { return static_cast<int32_t>(offsetof(ISN_TimeZone_t2672557812, ____Name_1)); }
	inline String_t* get__Name_1() const { return ____Name_1; }
	inline String_t** get_address_of__Name_1() { return &____Name_1; }
	inline void set__Name_1(String_t* value)
	{
		____Name_1 = value;
		Il2CppCodeGenWriteBarrier((&____Name_1), value);
	}
};

struct ISN_TimeZone_t2672557812_StaticFields
{
public:
	// ISN_TimeZone ISN_TimeZone::_LocalTimeZone
	ISN_TimeZone_t2672557812 * ____LocalTimeZone_2;

public:
	inline static int32_t get_offset_of__LocalTimeZone_2() { return static_cast<int32_t>(offsetof(ISN_TimeZone_t2672557812_StaticFields, ____LocalTimeZone_2)); }
	inline ISN_TimeZone_t2672557812 * get__LocalTimeZone_2() const { return ____LocalTimeZone_2; }
	inline ISN_TimeZone_t2672557812 ** get_address_of__LocalTimeZone_2() { return &____LocalTimeZone_2; }
	inline void set__LocalTimeZone_2(ISN_TimeZone_t2672557812 * value)
	{
		____LocalTimeZone_2 = value;
		Il2CppCodeGenWriteBarrier((&____LocalTimeZone_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISN_TIMEZONE_T2672557812_H
#ifndef ISN_BUILD_T2176122696_H
#define ISN_BUILD_T2176122696_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ISN_Build
struct  ISN_Build_t2176122696  : public RuntimeObject
{
public:
	// System.String ISN_Build::_Version
	String_t* ____Version_0;
	// System.Int32 ISN_Build::_Number
	int32_t ____Number_1;

public:
	inline static int32_t get_offset_of__Version_0() { return static_cast<int32_t>(offsetof(ISN_Build_t2176122696, ____Version_0)); }
	inline String_t* get__Version_0() const { return ____Version_0; }
	inline String_t** get_address_of__Version_0() { return &____Version_0; }
	inline void set__Version_0(String_t* value)
	{
		____Version_0 = value;
		Il2CppCodeGenWriteBarrier((&____Version_0), value);
	}

	inline static int32_t get_offset_of__Number_1() { return static_cast<int32_t>(offsetof(ISN_Build_t2176122696, ____Number_1)); }
	inline int32_t get__Number_1() const { return ____Number_1; }
	inline int32_t* get_address_of__Number_1() { return &____Number_1; }
	inline void set__Number_1(int32_t value)
	{
		____Number_1 = value;
	}
};

struct ISN_Build_t2176122696_StaticFields
{
public:
	// ISN_Build ISN_Build::_Current
	ISN_Build_t2176122696 * ____Current_2;

public:
	inline static int32_t get_offset_of__Current_2() { return static_cast<int32_t>(offsetof(ISN_Build_t2176122696_StaticFields, ____Current_2)); }
	inline ISN_Build_t2176122696 * get__Current_2() const { return ____Current_2; }
	inline ISN_Build_t2176122696 ** get_address_of__Current_2() { return &____Current_2; }
	inline void set__Current_2(ISN_Build_t2176122696 * value)
	{
		____Current_2 = value;
		Il2CppCodeGenWriteBarrier((&____Current_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISN_BUILD_T2176122696_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef FORCEINFO_T3934387890_H
#define FORCEINFO_T3934387890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSNative.Gestures.ForceInfo
struct  ForceInfo_t3934387890  : public RuntimeObject
{
public:
	// System.Single SA.IOSNative.Gestures.ForceInfo::_Force
	float ____Force_0;
	// System.Single SA.IOSNative.Gestures.ForceInfo::_MaxForce
	float ____MaxForce_1;

public:
	inline static int32_t get_offset_of__Force_0() { return static_cast<int32_t>(offsetof(ForceInfo_t3934387890, ____Force_0)); }
	inline float get__Force_0() const { return ____Force_0; }
	inline float* get_address_of__Force_0() { return &____Force_0; }
	inline void set__Force_0(float value)
	{
		____Force_0 = value;
	}

	inline static int32_t get_offset_of__MaxForce_1() { return static_cast<int32_t>(offsetof(ForceInfo_t3934387890, ____MaxForce_1)); }
	inline float get__MaxForce_1() const { return ____MaxForce_1; }
	inline float* get_address_of__MaxForce_1() { return &____MaxForce_1; }
	inline void set__MaxForce_1(float value)
	{
		____MaxForce_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORCEINFO_T3934387890_H
#ifndef VERIFICATIONRESPONSE_T3269199428_H
#define VERIFICATIONRESPONSE_T3269199428_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSNative.StoreKit.VerificationResponse
struct  VerificationResponse_t3269199428  : public RuntimeObject
{
public:
	// System.Int32 SA.IOSNative.StoreKit.VerificationResponse::_Status
	int32_t ____Status_0;
	// System.String SA.IOSNative.StoreKit.VerificationResponse::_Receipt
	String_t* ____Receipt_1;
	// System.String SA.IOSNative.StoreKit.VerificationResponse::_ProductIdentifier
	String_t* ____ProductIdentifier_2;
	// System.String SA.IOSNative.StoreKit.VerificationResponse::_OriginalJSON
	String_t* ____OriginalJSON_3;

public:
	inline static int32_t get_offset_of__Status_0() { return static_cast<int32_t>(offsetof(VerificationResponse_t3269199428, ____Status_0)); }
	inline int32_t get__Status_0() const { return ____Status_0; }
	inline int32_t* get_address_of__Status_0() { return &____Status_0; }
	inline void set__Status_0(int32_t value)
	{
		____Status_0 = value;
	}

	inline static int32_t get_offset_of__Receipt_1() { return static_cast<int32_t>(offsetof(VerificationResponse_t3269199428, ____Receipt_1)); }
	inline String_t* get__Receipt_1() const { return ____Receipt_1; }
	inline String_t** get_address_of__Receipt_1() { return &____Receipt_1; }
	inline void set__Receipt_1(String_t* value)
	{
		____Receipt_1 = value;
		Il2CppCodeGenWriteBarrier((&____Receipt_1), value);
	}

	inline static int32_t get_offset_of__ProductIdentifier_2() { return static_cast<int32_t>(offsetof(VerificationResponse_t3269199428, ____ProductIdentifier_2)); }
	inline String_t* get__ProductIdentifier_2() const { return ____ProductIdentifier_2; }
	inline String_t** get_address_of__ProductIdentifier_2() { return &____ProductIdentifier_2; }
	inline void set__ProductIdentifier_2(String_t* value)
	{
		____ProductIdentifier_2 = value;
		Il2CppCodeGenWriteBarrier((&____ProductIdentifier_2), value);
	}

	inline static int32_t get_offset_of__OriginalJSON_3() { return static_cast<int32_t>(offsetof(VerificationResponse_t3269199428, ____OriginalJSON_3)); }
	inline String_t* get__OriginalJSON_3() const { return ____OriginalJSON_3; }
	inline String_t** get_address_of__OriginalJSON_3() { return &____OriginalJSON_3; }
	inline void set__OriginalJSON_3(String_t* value)
	{
		____OriginalJSON_3 = value;
		Il2CppCodeGenWriteBarrier((&____OriginalJSON_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERIFICATIONRESPONSE_T3269199428_H
#ifndef SHAREDAPPLICATION_T2880145764_H
#define SHAREDAPPLICATION_T2880145764_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSNative.System.SharedApplication
struct  SharedApplication_t2880145764  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHAREDAPPLICATION_T2880145764_H
#ifndef DATETIMEPICKER_T2880553240_H
#define DATETIMEPICKER_T2880553240_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSNative.UIKit.DateTimePicker
struct  DateTimePicker_t2880553240  : public RuntimeObject
{
public:

public:
};

struct DateTimePicker_t2880553240_StaticFields
{
public:
	// System.Action`1<System.DateTime> SA.IOSNative.UIKit.DateTimePicker::OnPickerClosed
	Action_1_t3910997380 * ___OnPickerClosed_0;
	// System.Action`1<System.DateTime> SA.IOSNative.UIKit.DateTimePicker::OnPickerDateChanged
	Action_1_t3910997380 * ___OnPickerDateChanged_1;

public:
	inline static int32_t get_offset_of_OnPickerClosed_0() { return static_cast<int32_t>(offsetof(DateTimePicker_t2880553240_StaticFields, ___OnPickerClosed_0)); }
	inline Action_1_t3910997380 * get_OnPickerClosed_0() const { return ___OnPickerClosed_0; }
	inline Action_1_t3910997380 ** get_address_of_OnPickerClosed_0() { return &___OnPickerClosed_0; }
	inline void set_OnPickerClosed_0(Action_1_t3910997380 * value)
	{
		___OnPickerClosed_0 = value;
		Il2CppCodeGenWriteBarrier((&___OnPickerClosed_0), value);
	}

	inline static int32_t get_offset_of_OnPickerDateChanged_1() { return static_cast<int32_t>(offsetof(DateTimePicker_t2880553240_StaticFields, ___OnPickerDateChanged_1)); }
	inline Action_1_t3910997380 * get_OnPickerDateChanged_1() const { return ___OnPickerDateChanged_1; }
	inline Action_1_t3910997380 ** get_address_of_OnPickerDateChanged_1() { return &___OnPickerDateChanged_1; }
	inline void set_OnPickerDateChanged_1(Action_1_t3910997380 * value)
	{
		___OnPickerDateChanged_1 = value;
		Il2CppCodeGenWriteBarrier((&___OnPickerDateChanged_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEPICKER_T2880553240_H
#ifndef PERMISSIONSMANAGER_T2458142426_H
#define PERMISSIONSMANAGER_T2458142426_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSNative.Privacy.PermissionsManager
struct  PermissionsManager_t2458142426  : public RuntimeObject
{
public:

public:
};

struct PermissionsManager_t2458142426_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Action`1<SA.IOSNative.Privacy.PermissionStatus>> SA.IOSNative.Privacy.PermissionsManager::OnResponseDictionary
	Dictionary_2_t898888037 * ___OnResponseDictionary_0;

public:
	inline static int32_t get_offset_of_OnResponseDictionary_0() { return static_cast<int32_t>(offsetof(PermissionsManager_t2458142426_StaticFields, ___OnResponseDictionary_0)); }
	inline Dictionary_2_t898888037 * get_OnResponseDictionary_0() const { return ___OnResponseDictionary_0; }
	inline Dictionary_2_t898888037 ** get_address_of_OnResponseDictionary_0() { return &___OnResponseDictionary_0; }
	inline void set_OnResponseDictionary_0(Dictionary_2_t898888037 * value)
	{
		___OnResponseDictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___OnResponseDictionary_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PERMISSIONSMANAGER_T2458142426_H
#ifndef ISN_LOCALRECEIPTRESULT_T1161556914_H
#define ISN_LOCALRECEIPTRESULT_T1161556914_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ISN_LocalReceiptResult
struct  ISN_LocalReceiptResult_t1161556914  : public RuntimeObject
{
public:
	// System.Byte[] ISN_LocalReceiptResult::_Receipt
	ByteU5BU5D_t4116647657* ____Receipt_0;
	// System.String ISN_LocalReceiptResult::_ReceiptString
	String_t* ____ReceiptString_1;

public:
	inline static int32_t get_offset_of__Receipt_0() { return static_cast<int32_t>(offsetof(ISN_LocalReceiptResult_t1161556914, ____Receipt_0)); }
	inline ByteU5BU5D_t4116647657* get__Receipt_0() const { return ____Receipt_0; }
	inline ByteU5BU5D_t4116647657** get_address_of__Receipt_0() { return &____Receipt_0; }
	inline void set__Receipt_0(ByteU5BU5D_t4116647657* value)
	{
		____Receipt_0 = value;
		Il2CppCodeGenWriteBarrier((&____Receipt_0), value);
	}

	inline static int32_t get_offset_of__ReceiptString_1() { return static_cast<int32_t>(offsetof(ISN_LocalReceiptResult_t1161556914, ____ReceiptString_1)); }
	inline String_t* get__ReceiptString_1() const { return ____ReceiptString_1; }
	inline String_t** get_address_of__ReceiptString_1() { return &____ReceiptString_1; }
	inline void set__ReceiptString_1(String_t* value)
	{
		____ReceiptString_1 = value;
		Il2CppCodeGenWriteBarrier((&____ReceiptString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISN_LOCALRECEIPTRESULT_T1161556914_H
#ifndef CONTACT_T2387438694_H
#define CONTACT_T2387438694_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSNative.Contacts.Contact
struct  Contact_t2387438694  : public RuntimeObject
{
public:
	// System.String SA.IOSNative.Contacts.Contact::GivenName
	String_t* ___GivenName_0;
	// System.String SA.IOSNative.Contacts.Contact::FamilyName
	String_t* ___FamilyName_1;
	// System.Collections.Generic.List`1<System.String> SA.IOSNative.Contacts.Contact::Emails
	List_1_t3319525431 * ___Emails_2;
	// System.Collections.Generic.List`1<SA.IOSNative.Contacts.PhoneNumber> SA.IOSNative.Contacts.Contact::PhoneNumbers
	List_1_t2611822117 * ___PhoneNumbers_3;

public:
	inline static int32_t get_offset_of_GivenName_0() { return static_cast<int32_t>(offsetof(Contact_t2387438694, ___GivenName_0)); }
	inline String_t* get_GivenName_0() const { return ___GivenName_0; }
	inline String_t** get_address_of_GivenName_0() { return &___GivenName_0; }
	inline void set_GivenName_0(String_t* value)
	{
		___GivenName_0 = value;
		Il2CppCodeGenWriteBarrier((&___GivenName_0), value);
	}

	inline static int32_t get_offset_of_FamilyName_1() { return static_cast<int32_t>(offsetof(Contact_t2387438694, ___FamilyName_1)); }
	inline String_t* get_FamilyName_1() const { return ___FamilyName_1; }
	inline String_t** get_address_of_FamilyName_1() { return &___FamilyName_1; }
	inline void set_FamilyName_1(String_t* value)
	{
		___FamilyName_1 = value;
		Il2CppCodeGenWriteBarrier((&___FamilyName_1), value);
	}

	inline static int32_t get_offset_of_Emails_2() { return static_cast<int32_t>(offsetof(Contact_t2387438694, ___Emails_2)); }
	inline List_1_t3319525431 * get_Emails_2() const { return ___Emails_2; }
	inline List_1_t3319525431 ** get_address_of_Emails_2() { return &___Emails_2; }
	inline void set_Emails_2(List_1_t3319525431 * value)
	{
		___Emails_2 = value;
		Il2CppCodeGenWriteBarrier((&___Emails_2), value);
	}

	inline static int32_t get_offset_of_PhoneNumbers_3() { return static_cast<int32_t>(offsetof(Contact_t2387438694, ___PhoneNumbers_3)); }
	inline List_1_t2611822117 * get_PhoneNumbers_3() const { return ___PhoneNumbers_3; }
	inline List_1_t2611822117 ** get_address_of_PhoneNumbers_3() { return &___PhoneNumbers_3; }
	inline void set_PhoneNumbers_3(List_1_t2611822117 * value)
	{
		___PhoneNumbers_3 = value;
		Il2CppCodeGenWriteBarrier((&___PhoneNumbers_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTACT_T2387438694_H
#ifndef HELLOPACKAGE_T2020835704_H
#define HELLOPACKAGE_T2020835704_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HelloPackage
struct  HelloPackage_t2020835704  : public BasePackage_t3538619746
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HELLOPACKAGE_T2020835704_H
#ifndef OBJECTCREATEPACKAGE_T1844192988_H
#define OBJECTCREATEPACKAGE_T1844192988_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ObjectCreatePackage
struct  ObjectCreatePackage_t1844192988  : public BasePackage_t3538619746
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTCREATEPACKAGE_T1844192988_H
#ifndef CONTACTSRESULT_T1151527695_H
#define CONTACTSRESULT_T1151527695_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSNative.Contacts.ContactsResult
struct  ContactsResult_t1151527695  : public Result_t4170401718
{
public:
	// System.Collections.Generic.List`1<SA.IOSNative.Contacts.Contact> SA.IOSNative.Contacts.ContactsResult::_Contacts
	List_1_t3859513436 * ____Contacts_1;

public:
	inline static int32_t get_offset_of__Contacts_1() { return static_cast<int32_t>(offsetof(ContactsResult_t1151527695, ____Contacts_1)); }
	inline List_1_t3859513436 * get__Contacts_1() const { return ____Contacts_1; }
	inline List_1_t3859513436 ** get_address_of__Contacts_1() { return &____Contacts_1; }
	inline void set__Contacts_1(List_1_t3859513436 * value)
	{
		____Contacts_1 = value;
		Il2CppCodeGenWriteBarrier((&____Contacts_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTACTSRESULT_T1151527695_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef RECT_T2360479859_H
#define RECT_T2360479859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t2360479859 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T2360479859_H
#ifndef RESTORERESULT_T1480690855_H
#define RESTORERESULT_T1480690855_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSNative.StoreKit.RestoreResult
struct  RestoreResult_t1480690855  : public Result_t4170401718
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESTORERESULT_T1480690855_H
#ifndef TIMESPAN_T881159249_H
#define TIMESPAN_T881159249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t881159249 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_8;

public:
	inline static int32_t get_offset_of__ticks_8() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249, ____ticks_8)); }
	inline int64_t get__ticks_8() const { return ____ticks_8; }
	inline int64_t* get_address_of__ticks_8() { return &____ticks_8; }
	inline void set__ticks_8(int64_t value)
	{
		____ticks_8 = value;
	}
};

struct TimeSpan_t881159249_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t881159249  ___MaxValue_5;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t881159249  ___MinValue_6;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t881159249  ___Zero_7;

public:
	inline static int32_t get_offset_of_MaxValue_5() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MaxValue_5)); }
	inline TimeSpan_t881159249  get_MaxValue_5() const { return ___MaxValue_5; }
	inline TimeSpan_t881159249 * get_address_of_MaxValue_5() { return &___MaxValue_5; }
	inline void set_MaxValue_5(TimeSpan_t881159249  value)
	{
		___MaxValue_5 = value;
	}

	inline static int32_t get_offset_of_MinValue_6() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MinValue_6)); }
	inline TimeSpan_t881159249  get_MinValue_6() const { return ___MinValue_6; }
	inline TimeSpan_t881159249 * get_address_of_MinValue_6() { return &___MinValue_6; }
	inline void set_MinValue_6(TimeSpan_t881159249  value)
	{
		___MinValue_6 = value;
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___Zero_7)); }
	inline TimeSpan_t881159249  get_Zero_7() const { return ___Zero_7; }
	inline TimeSpan_t881159249 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(TimeSpan_t881159249  value)
	{
		___Zero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T881159249_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef GK_COLLECTIONTYPE_T4065955501_H
#define GK_COLLECTIONTYPE_T4065955501_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GK_CollectionType
struct  GK_CollectionType_t4065955501 
{
public:
	// System.Int32 GK_CollectionType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GK_CollectionType_t4065955501, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GK_COLLECTIONTYPE_T4065955501_H
#ifndef SPACE_T654135784_H
#define SPACE_T654135784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Space
struct  Space_t654135784 
{
public:
	// System.Int32 UnityEngine.Space::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Space_t654135784, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPACE_T654135784_H
#ifndef LOOPTYPE_T197962080_H
#define LOOPTYPE_T197962080_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Animation.SA_iTween/LoopType
struct  LoopType_t197962080 
{
public:
	// System.Int32 SA.Common.Animation.SA_iTween/LoopType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LoopType_t197962080, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOOPTYPE_T197962080_H
#ifndef NAMEDVALUECOLOR_T1220476254_H
#define NAMEDVALUECOLOR_T1220476254_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Animation.SA_iTween/NamedValueColor
struct  NamedValueColor_t1220476254 
{
public:
	// System.Int32 SA.Common.Animation.SA_iTween/NamedValueColor::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(NamedValueColor_t1220476254, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMEDVALUECOLOR_T1220476254_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef TOKEN_T3446863854_H
#define TOKEN_T3446863854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Data.Json/Parser/TOKEN
struct  TOKEN_t3446863854 
{
public:
	// System.Int32 SA.Common.Data.Json/Parser/TOKEN::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TOKEN_t3446863854, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOKEN_T3446863854_H
#ifndef DATETIMEKIND_T3468814247_H
#define DATETIMEKIND_T3468814247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t3468814247 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t3468814247, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T3468814247_H
#ifndef EASETYPE_T2858385462_H
#define EASETYPE_T2858385462_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Animation.EaseType
struct  EaseType_t2858385462 
{
public:
	// System.Int32 SA.Common.Animation.EaseType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EaseType_t2858385462, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASETYPE_T2858385462_H
#ifndef DATETIMEPICKERMODE_T2532598555_H
#define DATETIMEPICKERMODE_T2532598555_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSNative.UIKit.DateTimePickerMode
struct  DateTimePickerMode_t2532598555 
{
public:
	// System.Int32 SA.IOSNative.UIKit.DateTimePickerMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimePickerMode_t2532598555, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEPICKERMODE_T2532598555_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef PERMISSIONSTATUS_T941164143_H
#define PERMISSIONSTATUS_T941164143_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSNative.Privacy.PermissionStatus
struct  PermissionStatus_t941164143 
{
public:
	// System.Int32 SA.IOSNative.Privacy.PermissionStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PermissionStatus_t941164143, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PERMISSIONSTATUS_T941164143_H
#ifndef ISN_SWIPEDIRECTION_T3055520214_H
#define ISN_SWIPEDIRECTION_T3055520214_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ISN_SwipeDirection
struct  ISN_SwipeDirection_t3055520214 
{
public:
	// System.Int32 ISN_SwipeDirection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ISN_SwipeDirection_t3055520214, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISN_SWIPEDIRECTION_T3055520214_H
#ifndef ISN_INTERFACEIDIOM_T3035749497_H
#define ISN_INTERFACEIDIOM_T3035749497_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ISN_InterfaceIdiom
struct  ISN_InterfaceIdiom_t3035749497 
{
public:
	// System.Int32 ISN_InterfaceIdiom::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ISN_InterfaceIdiom_t3035749497, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISN_INTERFACEIDIOM_T3035749497_H
#ifndef PERMISSION_T3948953700_H
#define PERMISSION_T3948953700_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSNative.Privacy.Permission
struct  Permission_t3948953700 
{
public:
	// System.Int32 SA.IOSNative.Privacy.Permission::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Permission_t3948953700, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PERMISSION_T3948953700_H
#ifndef DEFAULTS_T578422784_H
#define DEFAULTS_T578422784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Animation.SA_iTween/Defaults
struct  Defaults_t578422784  : public RuntimeObject
{
public:

public:
};

struct Defaults_t578422784_StaticFields
{
public:
	// System.Single SA.Common.Animation.SA_iTween/Defaults::time
	float ___time_0;
	// System.Single SA.Common.Animation.SA_iTween/Defaults::delay
	float ___delay_1;
	// SA.Common.Animation.SA_iTween/NamedValueColor SA.Common.Animation.SA_iTween/Defaults::namedColorValue
	int32_t ___namedColorValue_2;
	// SA.Common.Animation.SA_iTween/LoopType SA.Common.Animation.SA_iTween/Defaults::loopType
	int32_t ___loopType_3;
	// SA.Common.Animation.EaseType SA.Common.Animation.SA_iTween/Defaults::easeType
	int32_t ___easeType_4;
	// System.Single SA.Common.Animation.SA_iTween/Defaults::lookSpeed
	float ___lookSpeed_5;
	// System.Boolean SA.Common.Animation.SA_iTween/Defaults::isLocal
	bool ___isLocal_6;
	// UnityEngine.Space SA.Common.Animation.SA_iTween/Defaults::space
	int32_t ___space_7;
	// System.Boolean SA.Common.Animation.SA_iTween/Defaults::orientToPath
	bool ___orientToPath_8;
	// UnityEngine.Color SA.Common.Animation.SA_iTween/Defaults::color
	Color_t2555686324  ___color_9;
	// System.Single SA.Common.Animation.SA_iTween/Defaults::updateTimePercentage
	float ___updateTimePercentage_10;
	// System.Single SA.Common.Animation.SA_iTween/Defaults::updateTime
	float ___updateTime_11;
	// System.Int32 SA.Common.Animation.SA_iTween/Defaults::cameraFadeDepth
	int32_t ___cameraFadeDepth_12;
	// System.Single SA.Common.Animation.SA_iTween/Defaults::lookAhead
	float ___lookAhead_13;
	// System.Boolean SA.Common.Animation.SA_iTween/Defaults::useRealTime
	bool ___useRealTime_14;
	// UnityEngine.Vector3 SA.Common.Animation.SA_iTween/Defaults::up
	Vector3_t3722313464  ___up_15;

public:
	inline static int32_t get_offset_of_time_0() { return static_cast<int32_t>(offsetof(Defaults_t578422784_StaticFields, ___time_0)); }
	inline float get_time_0() const { return ___time_0; }
	inline float* get_address_of_time_0() { return &___time_0; }
	inline void set_time_0(float value)
	{
		___time_0 = value;
	}

	inline static int32_t get_offset_of_delay_1() { return static_cast<int32_t>(offsetof(Defaults_t578422784_StaticFields, ___delay_1)); }
	inline float get_delay_1() const { return ___delay_1; }
	inline float* get_address_of_delay_1() { return &___delay_1; }
	inline void set_delay_1(float value)
	{
		___delay_1 = value;
	}

	inline static int32_t get_offset_of_namedColorValue_2() { return static_cast<int32_t>(offsetof(Defaults_t578422784_StaticFields, ___namedColorValue_2)); }
	inline int32_t get_namedColorValue_2() const { return ___namedColorValue_2; }
	inline int32_t* get_address_of_namedColorValue_2() { return &___namedColorValue_2; }
	inline void set_namedColorValue_2(int32_t value)
	{
		___namedColorValue_2 = value;
	}

	inline static int32_t get_offset_of_loopType_3() { return static_cast<int32_t>(offsetof(Defaults_t578422784_StaticFields, ___loopType_3)); }
	inline int32_t get_loopType_3() const { return ___loopType_3; }
	inline int32_t* get_address_of_loopType_3() { return &___loopType_3; }
	inline void set_loopType_3(int32_t value)
	{
		___loopType_3 = value;
	}

	inline static int32_t get_offset_of_easeType_4() { return static_cast<int32_t>(offsetof(Defaults_t578422784_StaticFields, ___easeType_4)); }
	inline int32_t get_easeType_4() const { return ___easeType_4; }
	inline int32_t* get_address_of_easeType_4() { return &___easeType_4; }
	inline void set_easeType_4(int32_t value)
	{
		___easeType_4 = value;
	}

	inline static int32_t get_offset_of_lookSpeed_5() { return static_cast<int32_t>(offsetof(Defaults_t578422784_StaticFields, ___lookSpeed_5)); }
	inline float get_lookSpeed_5() const { return ___lookSpeed_5; }
	inline float* get_address_of_lookSpeed_5() { return &___lookSpeed_5; }
	inline void set_lookSpeed_5(float value)
	{
		___lookSpeed_5 = value;
	}

	inline static int32_t get_offset_of_isLocal_6() { return static_cast<int32_t>(offsetof(Defaults_t578422784_StaticFields, ___isLocal_6)); }
	inline bool get_isLocal_6() const { return ___isLocal_6; }
	inline bool* get_address_of_isLocal_6() { return &___isLocal_6; }
	inline void set_isLocal_6(bool value)
	{
		___isLocal_6 = value;
	}

	inline static int32_t get_offset_of_space_7() { return static_cast<int32_t>(offsetof(Defaults_t578422784_StaticFields, ___space_7)); }
	inline int32_t get_space_7() const { return ___space_7; }
	inline int32_t* get_address_of_space_7() { return &___space_7; }
	inline void set_space_7(int32_t value)
	{
		___space_7 = value;
	}

	inline static int32_t get_offset_of_orientToPath_8() { return static_cast<int32_t>(offsetof(Defaults_t578422784_StaticFields, ___orientToPath_8)); }
	inline bool get_orientToPath_8() const { return ___orientToPath_8; }
	inline bool* get_address_of_orientToPath_8() { return &___orientToPath_8; }
	inline void set_orientToPath_8(bool value)
	{
		___orientToPath_8 = value;
	}

	inline static int32_t get_offset_of_color_9() { return static_cast<int32_t>(offsetof(Defaults_t578422784_StaticFields, ___color_9)); }
	inline Color_t2555686324  get_color_9() const { return ___color_9; }
	inline Color_t2555686324 * get_address_of_color_9() { return &___color_9; }
	inline void set_color_9(Color_t2555686324  value)
	{
		___color_9 = value;
	}

	inline static int32_t get_offset_of_updateTimePercentage_10() { return static_cast<int32_t>(offsetof(Defaults_t578422784_StaticFields, ___updateTimePercentage_10)); }
	inline float get_updateTimePercentage_10() const { return ___updateTimePercentage_10; }
	inline float* get_address_of_updateTimePercentage_10() { return &___updateTimePercentage_10; }
	inline void set_updateTimePercentage_10(float value)
	{
		___updateTimePercentage_10 = value;
	}

	inline static int32_t get_offset_of_updateTime_11() { return static_cast<int32_t>(offsetof(Defaults_t578422784_StaticFields, ___updateTime_11)); }
	inline float get_updateTime_11() const { return ___updateTime_11; }
	inline float* get_address_of_updateTime_11() { return &___updateTime_11; }
	inline void set_updateTime_11(float value)
	{
		___updateTime_11 = value;
	}

	inline static int32_t get_offset_of_cameraFadeDepth_12() { return static_cast<int32_t>(offsetof(Defaults_t578422784_StaticFields, ___cameraFadeDepth_12)); }
	inline int32_t get_cameraFadeDepth_12() const { return ___cameraFadeDepth_12; }
	inline int32_t* get_address_of_cameraFadeDepth_12() { return &___cameraFadeDepth_12; }
	inline void set_cameraFadeDepth_12(int32_t value)
	{
		___cameraFadeDepth_12 = value;
	}

	inline static int32_t get_offset_of_lookAhead_13() { return static_cast<int32_t>(offsetof(Defaults_t578422784_StaticFields, ___lookAhead_13)); }
	inline float get_lookAhead_13() const { return ___lookAhead_13; }
	inline float* get_address_of_lookAhead_13() { return &___lookAhead_13; }
	inline void set_lookAhead_13(float value)
	{
		___lookAhead_13 = value;
	}

	inline static int32_t get_offset_of_useRealTime_14() { return static_cast<int32_t>(offsetof(Defaults_t578422784_StaticFields, ___useRealTime_14)); }
	inline bool get_useRealTime_14() const { return ___useRealTime_14; }
	inline bool* get_address_of_useRealTime_14() { return &___useRealTime_14; }
	inline void set_useRealTime_14(bool value)
	{
		___useRealTime_14 = value;
	}

	inline static int32_t get_offset_of_up_15() { return static_cast<int32_t>(offsetof(Defaults_t578422784_StaticFields, ___up_15)); }
	inline Vector3_t3722313464  get_up_15() const { return ___up_15; }
	inline Vector3_t3722313464 * get_address_of_up_15() { return &___up_15; }
	inline void set_up_15(Vector3_t3722313464  value)
	{
		___up_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTS_T578422784_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef DATETIME_T3738529785_H
#define DATETIME_T3738529785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t3738529785 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t881159249  ___ticks_10;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_11;

public:
	inline static int32_t get_offset_of_ticks_10() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___ticks_10)); }
	inline TimeSpan_t881159249  get_ticks_10() const { return ___ticks_10; }
	inline TimeSpan_t881159249 * get_address_of_ticks_10() { return &___ticks_10; }
	inline void set_ticks_10(TimeSpan_t881159249  value)
	{
		___ticks_10 = value;
	}

	inline static int32_t get_offset_of_kind_11() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___kind_11)); }
	inline int32_t get_kind_11() const { return ___kind_11; }
	inline int32_t* get_address_of_kind_11() { return &___kind_11; }
	inline void set_kind_11(int32_t value)
	{
		___kind_11 = value;
	}
};

struct DateTime_t3738529785_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t3738529785  ___MaxValue_12;
	// System.DateTime System.DateTime::MinValue
	DateTime_t3738529785  ___MinValue_13;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t1281789340* ___ParseTimeFormats_14;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t1281789340* ___ParseYearDayMonthFormats_15;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t1281789340* ___ParseYearMonthDayFormats_16;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t1281789340* ___ParseDayMonthYearFormats_17;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t1281789340* ___ParseMonthDayYearFormats_18;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t1281789340* ___MonthDayShortFormats_19;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t1281789340* ___DayMonthShortFormats_20;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t385246372* ___daysmonth_21;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t385246372* ___daysmonthleap_22;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_23;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_24;

public:
	inline static int32_t get_offset_of_MaxValue_12() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MaxValue_12)); }
	inline DateTime_t3738529785  get_MaxValue_12() const { return ___MaxValue_12; }
	inline DateTime_t3738529785 * get_address_of_MaxValue_12() { return &___MaxValue_12; }
	inline void set_MaxValue_12(DateTime_t3738529785  value)
	{
		___MaxValue_12 = value;
	}

	inline static int32_t get_offset_of_MinValue_13() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MinValue_13)); }
	inline DateTime_t3738529785  get_MinValue_13() const { return ___MinValue_13; }
	inline DateTime_t3738529785 * get_address_of_MinValue_13() { return &___MinValue_13; }
	inline void set_MinValue_13(DateTime_t3738529785  value)
	{
		___MinValue_13 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_14() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseTimeFormats_14)); }
	inline StringU5BU5D_t1281789340* get_ParseTimeFormats_14() const { return ___ParseTimeFormats_14; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseTimeFormats_14() { return &___ParseTimeFormats_14; }
	inline void set_ParseTimeFormats_14(StringU5BU5D_t1281789340* value)
	{
		___ParseTimeFormats_14 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_14), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_15() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearDayMonthFormats_15)); }
	inline StringU5BU5D_t1281789340* get_ParseYearDayMonthFormats_15() const { return ___ParseYearDayMonthFormats_15; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearDayMonthFormats_15() { return &___ParseYearDayMonthFormats_15; }
	inline void set_ParseYearDayMonthFormats_15(StringU5BU5D_t1281789340* value)
	{
		___ParseYearDayMonthFormats_15 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_15), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_16() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearMonthDayFormats_16)); }
	inline StringU5BU5D_t1281789340* get_ParseYearMonthDayFormats_16() const { return ___ParseYearMonthDayFormats_16; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearMonthDayFormats_16() { return &___ParseYearMonthDayFormats_16; }
	inline void set_ParseYearMonthDayFormats_16(StringU5BU5D_t1281789340* value)
	{
		___ParseYearMonthDayFormats_16 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_16), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_17() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseDayMonthYearFormats_17)); }
	inline StringU5BU5D_t1281789340* get_ParseDayMonthYearFormats_17() const { return ___ParseDayMonthYearFormats_17; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseDayMonthYearFormats_17() { return &___ParseDayMonthYearFormats_17; }
	inline void set_ParseDayMonthYearFormats_17(StringU5BU5D_t1281789340* value)
	{
		___ParseDayMonthYearFormats_17 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_17), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_18() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseMonthDayYearFormats_18)); }
	inline StringU5BU5D_t1281789340* get_ParseMonthDayYearFormats_18() const { return ___ParseMonthDayYearFormats_18; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseMonthDayYearFormats_18() { return &___ParseMonthDayYearFormats_18; }
	inline void set_ParseMonthDayYearFormats_18(StringU5BU5D_t1281789340* value)
	{
		___ParseMonthDayYearFormats_18 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_18), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_19() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MonthDayShortFormats_19)); }
	inline StringU5BU5D_t1281789340* get_MonthDayShortFormats_19() const { return ___MonthDayShortFormats_19; }
	inline StringU5BU5D_t1281789340** get_address_of_MonthDayShortFormats_19() { return &___MonthDayShortFormats_19; }
	inline void set_MonthDayShortFormats_19(StringU5BU5D_t1281789340* value)
	{
		___MonthDayShortFormats_19 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_19), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_20() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___DayMonthShortFormats_20)); }
	inline StringU5BU5D_t1281789340* get_DayMonthShortFormats_20() const { return ___DayMonthShortFormats_20; }
	inline StringU5BU5D_t1281789340** get_address_of_DayMonthShortFormats_20() { return &___DayMonthShortFormats_20; }
	inline void set_DayMonthShortFormats_20(StringU5BU5D_t1281789340* value)
	{
		___DayMonthShortFormats_20 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_20), value);
	}

	inline static int32_t get_offset_of_daysmonth_21() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonth_21)); }
	inline Int32U5BU5D_t385246372* get_daysmonth_21() const { return ___daysmonth_21; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonth_21() { return &___daysmonth_21; }
	inline void set_daysmonth_21(Int32U5BU5D_t385246372* value)
	{
		___daysmonth_21 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_21), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_22() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonthleap_22)); }
	inline Int32U5BU5D_t385246372* get_daysmonthleap_22() const { return ___daysmonthleap_22; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonthleap_22() { return &___daysmonthleap_22; }
	inline void set_daysmonthleap_22(Int32U5BU5D_t385246372* value)
	{
		___daysmonthleap_22 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_22), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_23() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___to_local_time_span_object_23)); }
	inline RuntimeObject * get_to_local_time_span_object_23() const { return ___to_local_time_span_object_23; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_23() { return &___to_local_time_span_object_23; }
	inline void set_to_local_time_span_object_23(RuntimeObject * value)
	{
		___to_local_time_span_object_23 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_23), value);
	}

	inline static int32_t get_offset_of_last_now_24() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___last_now_24)); }
	inline int64_t get_last_now_24() const { return ___last_now_24; }
	inline int64_t* get_address_of_last_now_24() { return &___last_now_24; }
	inline void set_last_now_24(int64_t value)
	{
		___last_now_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T3738529785_H
#ifndef ISN_DEVICE_T1685187044_H
#define ISN_DEVICE_T1685187044_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ISN_Device
struct  ISN_Device_t1685187044  : public RuntimeObject
{
public:
	// System.String ISN_Device::_Name
	String_t* ____Name_1;
	// System.String ISN_Device::_SystemName
	String_t* ____SystemName_2;
	// System.String ISN_Device::_Model
	String_t* ____Model_3;
	// System.String ISN_Device::_LocalizedModel
	String_t* ____LocalizedModel_4;
	// System.String ISN_Device::_SystemVersion
	String_t* ____SystemVersion_5;
	// System.Int32 ISN_Device::_MajorSystemVersion
	int32_t ____MajorSystemVersion_6;
	// System.String ISN_Device::_PreferredLanguage_ISO639_1
	String_t* ____PreferredLanguage_ISO639_1_7;
	// ISN_InterfaceIdiom ISN_Device::_InterfaceIdiom
	int32_t ____InterfaceIdiom_8;
	// ISN_DeviceGUID ISN_Device::_GUID
	ISN_DeviceGUID_t3668829792 * ____GUID_9;

public:
	inline static int32_t get_offset_of__Name_1() { return static_cast<int32_t>(offsetof(ISN_Device_t1685187044, ____Name_1)); }
	inline String_t* get__Name_1() const { return ____Name_1; }
	inline String_t** get_address_of__Name_1() { return &____Name_1; }
	inline void set__Name_1(String_t* value)
	{
		____Name_1 = value;
		Il2CppCodeGenWriteBarrier((&____Name_1), value);
	}

	inline static int32_t get_offset_of__SystemName_2() { return static_cast<int32_t>(offsetof(ISN_Device_t1685187044, ____SystemName_2)); }
	inline String_t* get__SystemName_2() const { return ____SystemName_2; }
	inline String_t** get_address_of__SystemName_2() { return &____SystemName_2; }
	inline void set__SystemName_2(String_t* value)
	{
		____SystemName_2 = value;
		Il2CppCodeGenWriteBarrier((&____SystemName_2), value);
	}

	inline static int32_t get_offset_of__Model_3() { return static_cast<int32_t>(offsetof(ISN_Device_t1685187044, ____Model_3)); }
	inline String_t* get__Model_3() const { return ____Model_3; }
	inline String_t** get_address_of__Model_3() { return &____Model_3; }
	inline void set__Model_3(String_t* value)
	{
		____Model_3 = value;
		Il2CppCodeGenWriteBarrier((&____Model_3), value);
	}

	inline static int32_t get_offset_of__LocalizedModel_4() { return static_cast<int32_t>(offsetof(ISN_Device_t1685187044, ____LocalizedModel_4)); }
	inline String_t* get__LocalizedModel_4() const { return ____LocalizedModel_4; }
	inline String_t** get_address_of__LocalizedModel_4() { return &____LocalizedModel_4; }
	inline void set__LocalizedModel_4(String_t* value)
	{
		____LocalizedModel_4 = value;
		Il2CppCodeGenWriteBarrier((&____LocalizedModel_4), value);
	}

	inline static int32_t get_offset_of__SystemVersion_5() { return static_cast<int32_t>(offsetof(ISN_Device_t1685187044, ____SystemVersion_5)); }
	inline String_t* get__SystemVersion_5() const { return ____SystemVersion_5; }
	inline String_t** get_address_of__SystemVersion_5() { return &____SystemVersion_5; }
	inline void set__SystemVersion_5(String_t* value)
	{
		____SystemVersion_5 = value;
		Il2CppCodeGenWriteBarrier((&____SystemVersion_5), value);
	}

	inline static int32_t get_offset_of__MajorSystemVersion_6() { return static_cast<int32_t>(offsetof(ISN_Device_t1685187044, ____MajorSystemVersion_6)); }
	inline int32_t get__MajorSystemVersion_6() const { return ____MajorSystemVersion_6; }
	inline int32_t* get_address_of__MajorSystemVersion_6() { return &____MajorSystemVersion_6; }
	inline void set__MajorSystemVersion_6(int32_t value)
	{
		____MajorSystemVersion_6 = value;
	}

	inline static int32_t get_offset_of__PreferredLanguage_ISO639_1_7() { return static_cast<int32_t>(offsetof(ISN_Device_t1685187044, ____PreferredLanguage_ISO639_1_7)); }
	inline String_t* get__PreferredLanguage_ISO639_1_7() const { return ____PreferredLanguage_ISO639_1_7; }
	inline String_t** get_address_of__PreferredLanguage_ISO639_1_7() { return &____PreferredLanguage_ISO639_1_7; }
	inline void set__PreferredLanguage_ISO639_1_7(String_t* value)
	{
		____PreferredLanguage_ISO639_1_7 = value;
		Il2CppCodeGenWriteBarrier((&____PreferredLanguage_ISO639_1_7), value);
	}

	inline static int32_t get_offset_of__InterfaceIdiom_8() { return static_cast<int32_t>(offsetof(ISN_Device_t1685187044, ____InterfaceIdiom_8)); }
	inline int32_t get__InterfaceIdiom_8() const { return ____InterfaceIdiom_8; }
	inline int32_t* get_address_of__InterfaceIdiom_8() { return &____InterfaceIdiom_8; }
	inline void set__InterfaceIdiom_8(int32_t value)
	{
		____InterfaceIdiom_8 = value;
	}

	inline static int32_t get_offset_of__GUID_9() { return static_cast<int32_t>(offsetof(ISN_Device_t1685187044, ____GUID_9)); }
	inline ISN_DeviceGUID_t3668829792 * get__GUID_9() const { return ____GUID_9; }
	inline ISN_DeviceGUID_t3668829792 ** get_address_of__GUID_9() { return &____GUID_9; }
	inline void set__GUID_9(ISN_DeviceGUID_t3668829792 * value)
	{
		____GUID_9 = value;
		Il2CppCodeGenWriteBarrier((&____GUID_9), value);
	}
};

struct ISN_Device_t1685187044_StaticFields
{
public:
	// ISN_Device ISN_Device::_CurrentDevice
	ISN_Device_t1685187044 * ____CurrentDevice_0;

public:
	inline static int32_t get_offset_of__CurrentDevice_0() { return static_cast<int32_t>(offsetof(ISN_Device_t1685187044_StaticFields, ____CurrentDevice_0)); }
	inline ISN_Device_t1685187044 * get__CurrentDevice_0() const { return ____CurrentDevice_0; }
	inline ISN_Device_t1685187044 ** get_address_of__CurrentDevice_0() { return &____CurrentDevice_0; }
	inline void set__CurrentDevice_0(ISN_Device_t1685187044 * value)
	{
		____CurrentDevice_0 = value;
		Il2CppCodeGenWriteBarrier((&____CurrentDevice_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISN_DEVICE_T1685187044_H
#ifndef EASINGFUNCTION_T2379729698_H
#define EASINGFUNCTION_T2379729698_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Animation.SA_iTween/EasingFunction
struct  EasingFunction_t2379729698  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASINGFUNCTION_T2379729698_H
#ifndef APPLYTWEEN_T3100151106_H
#define APPLYTWEEN_T3100151106_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Animation.SA_iTween/ApplyTween
struct  ApplyTween_t3100151106  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLYTWEEN_T3100151106_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef BASEIOSFEATUREPREVIEW_T3740726741_H
#define BASEIOSFEATUREPREVIEW_T3740726741_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BaseIOSFeaturePreview
struct  BaseIOSFeaturePreview_t3740726741  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GUIStyle BaseIOSFeaturePreview::style
	GUIStyle_t3956901511 * ___style_2;
	// System.Int32 BaseIOSFeaturePreview::buttonWidth
	int32_t ___buttonWidth_3;
	// System.Int32 BaseIOSFeaturePreview::buttonHeight
	int32_t ___buttonHeight_4;
	// System.Single BaseIOSFeaturePreview::StartY
	float ___StartY_5;
	// System.Single BaseIOSFeaturePreview::StartX
	float ___StartX_6;
	// System.Single BaseIOSFeaturePreview::XStartPos
	float ___XStartPos_7;
	// System.Single BaseIOSFeaturePreview::YStartPos
	float ___YStartPos_8;
	// System.Single BaseIOSFeaturePreview::XButtonStep
	float ___XButtonStep_9;
	// System.Single BaseIOSFeaturePreview::YButtonStep
	float ___YButtonStep_10;
	// System.Single BaseIOSFeaturePreview::YLableStep
	float ___YLableStep_11;

public:
	inline static int32_t get_offset_of_style_2() { return static_cast<int32_t>(offsetof(BaseIOSFeaturePreview_t3740726741, ___style_2)); }
	inline GUIStyle_t3956901511 * get_style_2() const { return ___style_2; }
	inline GUIStyle_t3956901511 ** get_address_of_style_2() { return &___style_2; }
	inline void set_style_2(GUIStyle_t3956901511 * value)
	{
		___style_2 = value;
		Il2CppCodeGenWriteBarrier((&___style_2), value);
	}

	inline static int32_t get_offset_of_buttonWidth_3() { return static_cast<int32_t>(offsetof(BaseIOSFeaturePreview_t3740726741, ___buttonWidth_3)); }
	inline int32_t get_buttonWidth_3() const { return ___buttonWidth_3; }
	inline int32_t* get_address_of_buttonWidth_3() { return &___buttonWidth_3; }
	inline void set_buttonWidth_3(int32_t value)
	{
		___buttonWidth_3 = value;
	}

	inline static int32_t get_offset_of_buttonHeight_4() { return static_cast<int32_t>(offsetof(BaseIOSFeaturePreview_t3740726741, ___buttonHeight_4)); }
	inline int32_t get_buttonHeight_4() const { return ___buttonHeight_4; }
	inline int32_t* get_address_of_buttonHeight_4() { return &___buttonHeight_4; }
	inline void set_buttonHeight_4(int32_t value)
	{
		___buttonHeight_4 = value;
	}

	inline static int32_t get_offset_of_StartY_5() { return static_cast<int32_t>(offsetof(BaseIOSFeaturePreview_t3740726741, ___StartY_5)); }
	inline float get_StartY_5() const { return ___StartY_5; }
	inline float* get_address_of_StartY_5() { return &___StartY_5; }
	inline void set_StartY_5(float value)
	{
		___StartY_5 = value;
	}

	inline static int32_t get_offset_of_StartX_6() { return static_cast<int32_t>(offsetof(BaseIOSFeaturePreview_t3740726741, ___StartX_6)); }
	inline float get_StartX_6() const { return ___StartX_6; }
	inline float* get_address_of_StartX_6() { return &___StartX_6; }
	inline void set_StartX_6(float value)
	{
		___StartX_6 = value;
	}

	inline static int32_t get_offset_of_XStartPos_7() { return static_cast<int32_t>(offsetof(BaseIOSFeaturePreview_t3740726741, ___XStartPos_7)); }
	inline float get_XStartPos_7() const { return ___XStartPos_7; }
	inline float* get_address_of_XStartPos_7() { return &___XStartPos_7; }
	inline void set_XStartPos_7(float value)
	{
		___XStartPos_7 = value;
	}

	inline static int32_t get_offset_of_YStartPos_8() { return static_cast<int32_t>(offsetof(BaseIOSFeaturePreview_t3740726741, ___YStartPos_8)); }
	inline float get_YStartPos_8() const { return ___YStartPos_8; }
	inline float* get_address_of_YStartPos_8() { return &___YStartPos_8; }
	inline void set_YStartPos_8(float value)
	{
		___YStartPos_8 = value;
	}

	inline static int32_t get_offset_of_XButtonStep_9() { return static_cast<int32_t>(offsetof(BaseIOSFeaturePreview_t3740726741, ___XButtonStep_9)); }
	inline float get_XButtonStep_9() const { return ___XButtonStep_9; }
	inline float* get_address_of_XButtonStep_9() { return &___XButtonStep_9; }
	inline void set_XButtonStep_9(float value)
	{
		___XButtonStep_9 = value;
	}

	inline static int32_t get_offset_of_YButtonStep_10() { return static_cast<int32_t>(offsetof(BaseIOSFeaturePreview_t3740726741, ___YButtonStep_10)); }
	inline float get_YButtonStep_10() const { return ___YButtonStep_10; }
	inline float* get_address_of_YButtonStep_10() { return &___YButtonStep_10; }
	inline void set_YButtonStep_10(float value)
	{
		___YButtonStep_10 = value;
	}

	inline static int32_t get_offset_of_YLableStep_11() { return static_cast<int32_t>(offsetof(BaseIOSFeaturePreview_t3740726741, ___YLableStep_11)); }
	inline float get_YLableStep_11() const { return ___YLableStep_11; }
	inline float* get_address_of_YLableStep_11() { return &___YLableStep_11; }
	inline void set_YLableStep_11(float value)
	{
		___YLableStep_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEIOSFEATUREPREVIEW_T3740726741_H
#ifndef SINGLETON_1_T2911102372_H
#define SINGLETON_1_T2911102372_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Pattern.Singleton`1<SA.IOSNative.UIKit.NativeReceiver>
struct  Singleton_1_t2911102372  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Singleton_1_t2911102372_StaticFields
{
public:
	// T SA.Common.Pattern.Singleton`1::_instance
	NativeReceiver_t3281330226 * ____instance_2;
	// System.Boolean SA.Common.Pattern.Singleton`1::applicationIsQuitting
	bool ___applicationIsQuitting_3;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t2911102372_StaticFields, ____instance_2)); }
	inline NativeReceiver_t3281330226 * get__instance_2() const { return ____instance_2; }
	inline NativeReceiver_t3281330226 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(NativeReceiver_t3281330226 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____instance_2), value);
	}

	inline static int32_t get_offset_of_applicationIsQuitting_3() { return static_cast<int32_t>(offsetof(Singleton_1_t2911102372_StaticFields, ___applicationIsQuitting_3)); }
	inline bool get_applicationIsQuitting_3() const { return ___applicationIsQuitting_3; }
	inline bool* get_address_of_applicationIsQuitting_3() { return &___applicationIsQuitting_3; }
	inline void set_applicationIsQuitting_3(bool value)
	{
		___applicationIsQuitting_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T2911102372_H
#ifndef GAMECENTERFRIENDLOADEXAMPLE_T946910862_H
#define GAMECENTERFRIENDLOADEXAMPLE_T946910862_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameCenterFriendLoadExample
struct  GameCenterFriendLoadExample_t946910862  : public MonoBehaviour_t3962482529
{
public:
	// System.String GameCenterFriendLoadExample::ChallengeLeaderboard
	String_t* ___ChallengeLeaderboard_2;
	// System.String GameCenterFriendLoadExample::ChallengeAchievement
	String_t* ___ChallengeAchievement_3;
	// UnityEngine.GUIStyle GameCenterFriendLoadExample::headerStyle
	GUIStyle_t3956901511 * ___headerStyle_4;
	// UnityEngine.GUIStyle GameCenterFriendLoadExample::boardStyle
	GUIStyle_t3956901511 * ___boardStyle_5;
	// System.Boolean GameCenterFriendLoadExample::renderFriendsList
	bool ___renderFriendsList_6;

public:
	inline static int32_t get_offset_of_ChallengeLeaderboard_2() { return static_cast<int32_t>(offsetof(GameCenterFriendLoadExample_t946910862, ___ChallengeLeaderboard_2)); }
	inline String_t* get_ChallengeLeaderboard_2() const { return ___ChallengeLeaderboard_2; }
	inline String_t** get_address_of_ChallengeLeaderboard_2() { return &___ChallengeLeaderboard_2; }
	inline void set_ChallengeLeaderboard_2(String_t* value)
	{
		___ChallengeLeaderboard_2 = value;
		Il2CppCodeGenWriteBarrier((&___ChallengeLeaderboard_2), value);
	}

	inline static int32_t get_offset_of_ChallengeAchievement_3() { return static_cast<int32_t>(offsetof(GameCenterFriendLoadExample_t946910862, ___ChallengeAchievement_3)); }
	inline String_t* get_ChallengeAchievement_3() const { return ___ChallengeAchievement_3; }
	inline String_t** get_address_of_ChallengeAchievement_3() { return &___ChallengeAchievement_3; }
	inline void set_ChallengeAchievement_3(String_t* value)
	{
		___ChallengeAchievement_3 = value;
		Il2CppCodeGenWriteBarrier((&___ChallengeAchievement_3), value);
	}

	inline static int32_t get_offset_of_headerStyle_4() { return static_cast<int32_t>(offsetof(GameCenterFriendLoadExample_t946910862, ___headerStyle_4)); }
	inline GUIStyle_t3956901511 * get_headerStyle_4() const { return ___headerStyle_4; }
	inline GUIStyle_t3956901511 ** get_address_of_headerStyle_4() { return &___headerStyle_4; }
	inline void set_headerStyle_4(GUIStyle_t3956901511 * value)
	{
		___headerStyle_4 = value;
		Il2CppCodeGenWriteBarrier((&___headerStyle_4), value);
	}

	inline static int32_t get_offset_of_boardStyle_5() { return static_cast<int32_t>(offsetof(GameCenterFriendLoadExample_t946910862, ___boardStyle_5)); }
	inline GUIStyle_t3956901511 * get_boardStyle_5() const { return ___boardStyle_5; }
	inline GUIStyle_t3956901511 ** get_address_of_boardStyle_5() { return &___boardStyle_5; }
	inline void set_boardStyle_5(GUIStyle_t3956901511 * value)
	{
		___boardStyle_5 = value;
		Il2CppCodeGenWriteBarrier((&___boardStyle_5), value);
	}

	inline static int32_t get_offset_of_renderFriendsList_6() { return static_cast<int32_t>(offsetof(GameCenterFriendLoadExample_t946910862, ___renderFriendsList_6)); }
	inline bool get_renderFriendsList_6() const { return ___renderFriendsList_6; }
	inline bool* get_address_of_renderFriendsList_6() { return &___renderFriendsList_6; }
	inline void set_renderFriendsList_6(bool value)
	{
		___renderFriendsList_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMECENTERFRIENDLOADEXAMPLE_T946910862_H
#ifndef LEADERBOARDCUSTOMGUIEXAMPLE_T38875209_H
#define LEADERBOARDCUSTOMGUIEXAMPLE_T38875209_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LeaderboardCustomGUIExample
struct  LeaderboardCustomGUIExample_t38875209  : public MonoBehaviour_t3962482529
{
public:
	// System.String LeaderboardCustomGUIExample::leaderboardId_1
	String_t* ___leaderboardId_1_2;
	// System.Int32 LeaderboardCustomGUIExample::hiScore
	int32_t ___hiScore_3;
	// UnityEngine.GUIStyle LeaderboardCustomGUIExample::headerStyle
	GUIStyle_t3956901511 * ___headerStyle_4;
	// UnityEngine.GUIStyle LeaderboardCustomGUIExample::boardStyle
	GUIStyle_t3956901511 * ___boardStyle_5;
	// GK_Leaderboard LeaderboardCustomGUIExample::loadedLeaderboard
	GK_Leaderboard_t591378496 * ___loadedLeaderboard_6;
	// GK_CollectionType LeaderboardCustomGUIExample::displayCollection
	int32_t ___displayCollection_7;

public:
	inline static int32_t get_offset_of_leaderboardId_1_2() { return static_cast<int32_t>(offsetof(LeaderboardCustomGUIExample_t38875209, ___leaderboardId_1_2)); }
	inline String_t* get_leaderboardId_1_2() const { return ___leaderboardId_1_2; }
	inline String_t** get_address_of_leaderboardId_1_2() { return &___leaderboardId_1_2; }
	inline void set_leaderboardId_1_2(String_t* value)
	{
		___leaderboardId_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___leaderboardId_1_2), value);
	}

	inline static int32_t get_offset_of_hiScore_3() { return static_cast<int32_t>(offsetof(LeaderboardCustomGUIExample_t38875209, ___hiScore_3)); }
	inline int32_t get_hiScore_3() const { return ___hiScore_3; }
	inline int32_t* get_address_of_hiScore_3() { return &___hiScore_3; }
	inline void set_hiScore_3(int32_t value)
	{
		___hiScore_3 = value;
	}

	inline static int32_t get_offset_of_headerStyle_4() { return static_cast<int32_t>(offsetof(LeaderboardCustomGUIExample_t38875209, ___headerStyle_4)); }
	inline GUIStyle_t3956901511 * get_headerStyle_4() const { return ___headerStyle_4; }
	inline GUIStyle_t3956901511 ** get_address_of_headerStyle_4() { return &___headerStyle_4; }
	inline void set_headerStyle_4(GUIStyle_t3956901511 * value)
	{
		___headerStyle_4 = value;
		Il2CppCodeGenWriteBarrier((&___headerStyle_4), value);
	}

	inline static int32_t get_offset_of_boardStyle_5() { return static_cast<int32_t>(offsetof(LeaderboardCustomGUIExample_t38875209, ___boardStyle_5)); }
	inline GUIStyle_t3956901511 * get_boardStyle_5() const { return ___boardStyle_5; }
	inline GUIStyle_t3956901511 ** get_address_of_boardStyle_5() { return &___boardStyle_5; }
	inline void set_boardStyle_5(GUIStyle_t3956901511 * value)
	{
		___boardStyle_5 = value;
		Il2CppCodeGenWriteBarrier((&___boardStyle_5), value);
	}

	inline static int32_t get_offset_of_loadedLeaderboard_6() { return static_cast<int32_t>(offsetof(LeaderboardCustomGUIExample_t38875209, ___loadedLeaderboard_6)); }
	inline GK_Leaderboard_t591378496 * get_loadedLeaderboard_6() const { return ___loadedLeaderboard_6; }
	inline GK_Leaderboard_t591378496 ** get_address_of_loadedLeaderboard_6() { return &___loadedLeaderboard_6; }
	inline void set_loadedLeaderboard_6(GK_Leaderboard_t591378496 * value)
	{
		___loadedLeaderboard_6 = value;
		Il2CppCodeGenWriteBarrier((&___loadedLeaderboard_6), value);
	}

	inline static int32_t get_offset_of_displayCollection_7() { return static_cast<int32_t>(offsetof(LeaderboardCustomGUIExample_t38875209, ___displayCollection_7)); }
	inline int32_t get_displayCollection_7() const { return ___displayCollection_7; }
	inline int32_t* get_address_of_displayCollection_7() { return &___displayCollection_7; }
	inline void set_displayCollection_7(int32_t value)
	{
		___displayCollection_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEADERBOARDCUSTOMGUIEXAMPLE_T38875209_H
#ifndef SCREENSHOTMAKER_T2906260841_H
#define SCREENSHOTMAKER_T2906260841_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Models.ScreenshotMaker
struct  ScreenshotMaker_t2906260841  : public MonoBehaviour_t3962482529
{
public:
	// System.Action`1<UnityEngine.Texture2D> SA.Common.Models.ScreenshotMaker::OnScreenshotReady
	Action_1_t4012913780 * ___OnScreenshotReady_2;

public:
	inline static int32_t get_offset_of_OnScreenshotReady_2() { return static_cast<int32_t>(offsetof(ScreenshotMaker_t2906260841, ___OnScreenshotReady_2)); }
	inline Action_1_t4012913780 * get_OnScreenshotReady_2() const { return ___OnScreenshotReady_2; }
	inline Action_1_t4012913780 ** get_address_of_OnScreenshotReady_2() { return &___OnScreenshotReady_2; }
	inline void set_OnScreenshotReady_2(Action_1_t4012913780 * value)
	{
		___OnScreenshotReady_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnScreenshotReady_2), value);
	}
};

struct ScreenshotMaker_t2906260841_StaticFields
{
public:
	// System.Action`1<UnityEngine.Texture2D> SA.Common.Models.ScreenshotMaker::<>f__am$cache0
	Action_1_t4012913780 * ___U3CU3Ef__amU24cache0_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_3() { return static_cast<int32_t>(offsetof(ScreenshotMaker_t2906260841_StaticFields, ___U3CU3Ef__amU24cache0_3)); }
	inline Action_1_t4012913780 * get_U3CU3Ef__amU24cache0_3() const { return ___U3CU3Ef__amU24cache0_3; }
	inline Action_1_t4012913780 ** get_address_of_U3CU3Ef__amU24cache0_3() { return &___U3CU3Ef__amU24cache0_3; }
	inline void set_U3CU3Ef__amU24cache0_3(Action_1_t4012913780 * value)
	{
		___U3CU3Ef__amU24cache0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENSHOTMAKER_T2906260841_H
#ifndef WWWTEXTURELOADER_T1987695874_H
#define WWWTEXTURELOADER_T1987695874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Models.WWWTextureLoader
struct  WWWTextureLoader_t1987695874  : public MonoBehaviour_t3962482529
{
public:
	// System.String SA.Common.Models.WWWTextureLoader::_url
	String_t* ____url_3;
	// System.Action`1<UnityEngine.Texture2D> SA.Common.Models.WWWTextureLoader::OnLoad
	Action_1_t4012913780 * ___OnLoad_4;

public:
	inline static int32_t get_offset_of__url_3() { return static_cast<int32_t>(offsetof(WWWTextureLoader_t1987695874, ____url_3)); }
	inline String_t* get__url_3() const { return ____url_3; }
	inline String_t** get_address_of__url_3() { return &____url_3; }
	inline void set__url_3(String_t* value)
	{
		____url_3 = value;
		Il2CppCodeGenWriteBarrier((&____url_3), value);
	}

	inline static int32_t get_offset_of_OnLoad_4() { return static_cast<int32_t>(offsetof(WWWTextureLoader_t1987695874, ___OnLoad_4)); }
	inline Action_1_t4012913780 * get_OnLoad_4() const { return ___OnLoad_4; }
	inline Action_1_t4012913780 ** get_address_of_OnLoad_4() { return &___OnLoad_4; }
	inline void set_OnLoad_4(Action_1_t4012913780 * value)
	{
		___OnLoad_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnLoad_4), value);
	}
};

struct WWWTextureLoader_t1987695874_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Texture2D> SA.Common.Models.WWWTextureLoader::LocalCache
	Dictionary_2_t3625702484 * ___LocalCache_2;
	// System.Action`1<UnityEngine.Texture2D> SA.Common.Models.WWWTextureLoader::<>f__am$cache0
	Action_1_t4012913780 * ___U3CU3Ef__amU24cache0_5;

public:
	inline static int32_t get_offset_of_LocalCache_2() { return static_cast<int32_t>(offsetof(WWWTextureLoader_t1987695874_StaticFields, ___LocalCache_2)); }
	inline Dictionary_2_t3625702484 * get_LocalCache_2() const { return ___LocalCache_2; }
	inline Dictionary_2_t3625702484 ** get_address_of_LocalCache_2() { return &___LocalCache_2; }
	inline void set_LocalCache_2(Dictionary_2_t3625702484 * value)
	{
		___LocalCache_2 = value;
		Il2CppCodeGenWriteBarrier((&___LocalCache_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_5() { return static_cast<int32_t>(offsetof(WWWTextureLoader_t1987695874_StaticFields, ___U3CU3Ef__amU24cache0_5)); }
	inline Action_1_t4012913780 * get_U3CU3Ef__amU24cache0_5() const { return ___U3CU3Ef__amU24cache0_5; }
	inline Action_1_t4012913780 ** get_address_of_U3CU3Ef__amU24cache0_5() { return &___U3CU3Ef__amU24cache0_5; }
	inline void set_U3CU3Ef__amU24cache0_5(Action_1_t4012913780 * value)
	{
		___U3CU3Ef__amU24cache0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WWWTEXTURELOADER_T1987695874_H
#ifndef SINGLETON_1_T3682720583_H
#define SINGLETON_1_T3682720583_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Pattern.Singleton`1<ISN_Logger>
struct  Singleton_1_t3682720583  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Singleton_1_t3682720583_StaticFields
{
public:
	// T SA.Common.Pattern.Singleton`1::_instance
	ISN_Logger_t4052948437 * ____instance_2;
	// System.Boolean SA.Common.Pattern.Singleton`1::applicationIsQuitting
	bool ___applicationIsQuitting_3;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t3682720583_StaticFields, ____instance_2)); }
	inline ISN_Logger_t4052948437 * get__instance_2() const { return ____instance_2; }
	inline ISN_Logger_t4052948437 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(ISN_Logger_t4052948437 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____instance_2), value);
	}

	inline static int32_t get_offset_of_applicationIsQuitting_3() { return static_cast<int32_t>(offsetof(Singleton_1_t3682720583_StaticFields, ___applicationIsQuitting_3)); }
	inline bool get_applicationIsQuitting_3() const { return ___applicationIsQuitting_3; }
	inline bool* get_address_of_applicationIsQuitting_3() { return &___applicationIsQuitting_3; }
	inline void set_applicationIsQuitting_3(bool value)
	{
		___applicationIsQuitting_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T3682720583_H
#ifndef SINGLETON_1_T1829387111_H
#define SINGLETON_1_T1829387111_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Pattern.Singleton`1<SA.IOSNative.Gestures.ForceTouch>
struct  Singleton_1_t1829387111  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Singleton_1_t1829387111_StaticFields
{
public:
	// T SA.Common.Pattern.Singleton`1::_instance
	ForceTouch_t2199614965 * ____instance_2;
	// System.Boolean SA.Common.Pattern.Singleton`1::applicationIsQuitting
	bool ___applicationIsQuitting_3;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t1829387111_StaticFields, ____instance_2)); }
	inline ForceTouch_t2199614965 * get__instance_2() const { return ____instance_2; }
	inline ForceTouch_t2199614965 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(ForceTouch_t2199614965 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____instance_2), value);
	}

	inline static int32_t get_offset_of_applicationIsQuitting_3() { return static_cast<int32_t>(offsetof(Singleton_1_t1829387111_StaticFields, ___applicationIsQuitting_3)); }
	inline bool get_applicationIsQuitting_3() const { return ___applicationIsQuitting_3; }
	inline bool* get_address_of_applicationIsQuitting_3() { return &___applicationIsQuitting_3; }
	inline void set_applicationIsQuitting_3(bool value)
	{
		___applicationIsQuitting_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T1829387111_H
#ifndef SINGLETON_1_T3015640165_H
#define SINGLETON_1_T3015640165_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Pattern.Singleton`1<ISN_GestureRecognizer>
struct  Singleton_1_t3015640165  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Singleton_1_t3015640165_StaticFields
{
public:
	// T SA.Common.Pattern.Singleton`1::_instance
	ISN_GestureRecognizer_t3385868019 * ____instance_2;
	// System.Boolean SA.Common.Pattern.Singleton`1::applicationIsQuitting
	bool ___applicationIsQuitting_3;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t3015640165_StaticFields, ____instance_2)); }
	inline ISN_GestureRecognizer_t3385868019 * get__instance_2() const { return ____instance_2; }
	inline ISN_GestureRecognizer_t3385868019 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(ISN_GestureRecognizer_t3385868019 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____instance_2), value);
	}

	inline static int32_t get_offset_of_applicationIsQuitting_3() { return static_cast<int32_t>(offsetof(Singleton_1_t3015640165_StaticFields, ___applicationIsQuitting_3)); }
	inline bool get_applicationIsQuitting_3() const { return ___applicationIsQuitting_3; }
	inline bool* get_address_of_applicationIsQuitting_3() { return &___applicationIsQuitting_3; }
	inline void set_applicationIsQuitting_3(bool value)
	{
		___applicationIsQuitting_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T3015640165_H
#ifndef SINGLETON_1_T4032443511_H
#define SINGLETON_1_T4032443511_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Pattern.Singleton`1<SA.IOSNative.Privacy.NativeReceiver>
struct  Singleton_1_t4032443511  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Singleton_1_t4032443511_StaticFields
{
public:
	// T SA.Common.Pattern.Singleton`1::_instance
	NativeReceiver_t107704069 * ____instance_2;
	// System.Boolean SA.Common.Pattern.Singleton`1::applicationIsQuitting
	bool ___applicationIsQuitting_3;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t4032443511_StaticFields, ____instance_2)); }
	inline NativeReceiver_t107704069 * get__instance_2() const { return ____instance_2; }
	inline NativeReceiver_t107704069 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(NativeReceiver_t107704069 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____instance_2), value);
	}

	inline static int32_t get_offset_of_applicationIsQuitting_3() { return static_cast<int32_t>(offsetof(Singleton_1_t4032443511_StaticFields, ___applicationIsQuitting_3)); }
	inline bool get_applicationIsQuitting_3() const { return ___applicationIsQuitting_3; }
	inline bool* get_address_of_applicationIsQuitting_3() { return &___applicationIsQuitting_3; }
	inline void set_applicationIsQuitting_3(bool value)
	{
		___applicationIsQuitting_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T4032443511_H
#ifndef SINGLETON_1_T3365719810_H
#define SINGLETON_1_T3365719810_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Pattern.Singleton`1<IOSNativeUtility>
struct  Singleton_1_t3365719810  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Singleton_1_t3365719810_StaticFields
{
public:
	// T SA.Common.Pattern.Singleton`1::_instance
	IOSNativeUtility_t3735947664 * ____instance_2;
	// System.Boolean SA.Common.Pattern.Singleton`1::applicationIsQuitting
	bool ___applicationIsQuitting_3;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t3365719810_StaticFields, ____instance_2)); }
	inline IOSNativeUtility_t3735947664 * get__instance_2() const { return ____instance_2; }
	inline IOSNativeUtility_t3735947664 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(IOSNativeUtility_t3735947664 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____instance_2), value);
	}

	inline static int32_t get_offset_of_applicationIsQuitting_3() { return static_cast<int32_t>(offsetof(Singleton_1_t3365719810_StaticFields, ___applicationIsQuitting_3)); }
	inline bool get_applicationIsQuitting_3() const { return ___applicationIsQuitting_3; }
	inline bool* get_address_of_applicationIsQuitting_3() { return &___applicationIsQuitting_3; }
	inline void set_applicationIsQuitting_3(bool value)
	{
		___applicationIsQuitting_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T3365719810_H
#ifndef SINGLETON_1_T1050528595_H
#define SINGLETON_1_T1050528595_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Pattern.Singleton`1<SA.IOSNative.Contacts.ContactStore>
struct  Singleton_1_t1050528595  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Singleton_1_t1050528595_StaticFields
{
public:
	// T SA.Common.Pattern.Singleton`1::_instance
	ContactStore_t1420756449 * ____instance_2;
	// System.Boolean SA.Common.Pattern.Singleton`1::applicationIsQuitting
	bool ___applicationIsQuitting_3;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t1050528595_StaticFields, ____instance_2)); }
	inline ContactStore_t1420756449 * get__instance_2() const { return ____instance_2; }
	inline ContactStore_t1420756449 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(ContactStore_t1420756449 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____instance_2), value);
	}

	inline static int32_t get_offset_of_applicationIsQuitting_3() { return static_cast<int32_t>(offsetof(Singleton_1_t1050528595_StaticFields, ___applicationIsQuitting_3)); }
	inline bool get_applicationIsQuitting_3() const { return ___applicationIsQuitting_3; }
	inline bool* get_address_of_applicationIsQuitting_3() { return &___applicationIsQuitting_3; }
	inline void set_applicationIsQuitting_3(bool value)
	{
		___applicationIsQuitting_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T1050528595_H
#ifndef SINGLETON_1_T2412866665_H
#define SINGLETON_1_T2412866665_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Pattern.Singleton`1<ISN_Security>
struct  Singleton_1_t2412866665  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Singleton_1_t2412866665_StaticFields
{
public:
	// T SA.Common.Pattern.Singleton`1::_instance
	ISN_Security_t2783094519 * ____instance_2;
	// System.Boolean SA.Common.Pattern.Singleton`1::applicationIsQuitting
	bool ___applicationIsQuitting_3;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t2412866665_StaticFields, ____instance_2)); }
	inline ISN_Security_t2783094519 * get__instance_2() const { return ____instance_2; }
	inline ISN_Security_t2783094519 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(ISN_Security_t2783094519 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____instance_2), value);
	}

	inline static int32_t get_offset_of_applicationIsQuitting_3() { return static_cast<int32_t>(offsetof(Singleton_1_t2412866665_StaticFields, ___applicationIsQuitting_3)); }
	inline bool get_applicationIsQuitting_3() const { return ___applicationIsQuitting_3; }
	inline bool* get_address_of_applicationIsQuitting_3() { return &___applicationIsQuitting_3; }
	inline void set_applicationIsQuitting_3(bool value)
	{
		___applicationIsQuitting_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T2412866665_H
#ifndef MULTIPLAYERMANAGEREXAMPLE_T1146359991_H
#define MULTIPLAYERMANAGEREXAMPLE_T1146359991_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MultiplayerManagerExample
struct  MultiplayerManagerExample_t1146359991  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTIPLAYERMANAGEREXAMPLE_T1146359991_H
#ifndef SA_ITWEEN_T1550711905_H
#define SA_ITWEEN_T1550711905_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Animation.SA_iTween
struct  SA_iTween_t1550711905  : public MonoBehaviour_t3962482529
{
public:
	// System.String SA.Common.Animation.SA_iTween::id
	String_t* ___id_4;
	// System.String SA.Common.Animation.SA_iTween::type
	String_t* ___type_5;
	// System.String SA.Common.Animation.SA_iTween::method
	String_t* ___method_6;
	// SA.Common.Animation.EaseType SA.Common.Animation.SA_iTween::easeType
	int32_t ___easeType_7;
	// System.Single SA.Common.Animation.SA_iTween::time
	float ___time_8;
	// System.Single SA.Common.Animation.SA_iTween::delay
	float ___delay_9;
	// SA.Common.Animation.SA_iTween/LoopType SA.Common.Animation.SA_iTween::loopType
	int32_t ___loopType_10;
	// System.Boolean SA.Common.Animation.SA_iTween::isRunning
	bool ___isRunning_11;
	// System.Boolean SA.Common.Animation.SA_iTween::isPaused
	bool ___isPaused_12;
	// System.String SA.Common.Animation.SA_iTween::_name
	String_t* ____name_13;
	// System.Single SA.Common.Animation.SA_iTween::runningTime
	float ___runningTime_14;
	// System.Single SA.Common.Animation.SA_iTween::percentage
	float ___percentage_15;
	// System.Single SA.Common.Animation.SA_iTween::delayStarted
	float ___delayStarted_16;
	// System.Boolean SA.Common.Animation.SA_iTween::kinematic
	bool ___kinematic_17;
	// System.Boolean SA.Common.Animation.SA_iTween::isLocal
	bool ___isLocal_18;
	// System.Boolean SA.Common.Animation.SA_iTween::loop
	bool ___loop_19;
	// System.Boolean SA.Common.Animation.SA_iTween::reverse
	bool ___reverse_20;
	// System.Boolean SA.Common.Animation.SA_iTween::wasPaused
	bool ___wasPaused_21;
	// System.Boolean SA.Common.Animation.SA_iTween::physics
	bool ___physics_22;
	// System.Collections.Hashtable SA.Common.Animation.SA_iTween::tweenArguments
	Hashtable_t1853889766 * ___tweenArguments_23;
	// UnityEngine.Space SA.Common.Animation.SA_iTween::space
	int32_t ___space_24;
	// SA.Common.Animation.SA_iTween/EasingFunction SA.Common.Animation.SA_iTween::ease
	EasingFunction_t2379729698 * ___ease_25;
	// SA.Common.Animation.SA_iTween/ApplyTween SA.Common.Animation.SA_iTween::apply
	ApplyTween_t3100151106 * ___apply_26;
	// UnityEngine.AudioSource SA.Common.Animation.SA_iTween::audioSource
	AudioSource_t3935305588 * ___audioSource_27;
	// UnityEngine.Vector3[] SA.Common.Animation.SA_iTween::vector3s
	Vector3U5BU5D_t1718750761* ___vector3s_28;
	// UnityEngine.Vector2[] SA.Common.Animation.SA_iTween::vector2s
	Vector2U5BU5D_t1457185986* ___vector2s_29;
	// UnityEngine.Color[0...,0...] SA.Common.Animation.SA_iTween::colors
	ColorU5B0___U2C0___U5D_t941916414* ___colors_30;
	// System.Single[] SA.Common.Animation.SA_iTween::floats
	SingleU5BU5D_t1444911251* ___floats_31;
	// UnityEngine.Rect[] SA.Common.Animation.SA_iTween::rects
	RectU5BU5D_t2936723554* ___rects_32;
	// SA.Common.Animation.SA_iTween/CRSpline SA.Common.Animation.SA_iTween::path
	CRSpline_t2258853177 * ___path_33;
	// UnityEngine.Vector3 SA.Common.Animation.SA_iTween::preUpdate
	Vector3_t3722313464  ___preUpdate_34;
	// UnityEngine.Vector3 SA.Common.Animation.SA_iTween::postUpdate
	Vector3_t3722313464  ___postUpdate_35;
	// SA.Common.Animation.SA_iTween/NamedValueColor SA.Common.Animation.SA_iTween::namedcolorvalue
	int32_t ___namedcolorvalue_36;
	// System.Single SA.Common.Animation.SA_iTween::lastRealTime
	float ___lastRealTime_37;
	// System.Boolean SA.Common.Animation.SA_iTween::useRealTime
	bool ___useRealTime_38;
	// UnityEngine.Transform SA.Common.Animation.SA_iTween::thisTransform
	Transform_t3600365921 * ___thisTransform_39;

public:
	inline static int32_t get_offset_of_id_4() { return static_cast<int32_t>(offsetof(SA_iTween_t1550711905, ___id_4)); }
	inline String_t* get_id_4() const { return ___id_4; }
	inline String_t** get_address_of_id_4() { return &___id_4; }
	inline void set_id_4(String_t* value)
	{
		___id_4 = value;
		Il2CppCodeGenWriteBarrier((&___id_4), value);
	}

	inline static int32_t get_offset_of_type_5() { return static_cast<int32_t>(offsetof(SA_iTween_t1550711905, ___type_5)); }
	inline String_t* get_type_5() const { return ___type_5; }
	inline String_t** get_address_of_type_5() { return &___type_5; }
	inline void set_type_5(String_t* value)
	{
		___type_5 = value;
		Il2CppCodeGenWriteBarrier((&___type_5), value);
	}

	inline static int32_t get_offset_of_method_6() { return static_cast<int32_t>(offsetof(SA_iTween_t1550711905, ___method_6)); }
	inline String_t* get_method_6() const { return ___method_6; }
	inline String_t** get_address_of_method_6() { return &___method_6; }
	inline void set_method_6(String_t* value)
	{
		___method_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_6), value);
	}

	inline static int32_t get_offset_of_easeType_7() { return static_cast<int32_t>(offsetof(SA_iTween_t1550711905, ___easeType_7)); }
	inline int32_t get_easeType_7() const { return ___easeType_7; }
	inline int32_t* get_address_of_easeType_7() { return &___easeType_7; }
	inline void set_easeType_7(int32_t value)
	{
		___easeType_7 = value;
	}

	inline static int32_t get_offset_of_time_8() { return static_cast<int32_t>(offsetof(SA_iTween_t1550711905, ___time_8)); }
	inline float get_time_8() const { return ___time_8; }
	inline float* get_address_of_time_8() { return &___time_8; }
	inline void set_time_8(float value)
	{
		___time_8 = value;
	}

	inline static int32_t get_offset_of_delay_9() { return static_cast<int32_t>(offsetof(SA_iTween_t1550711905, ___delay_9)); }
	inline float get_delay_9() const { return ___delay_9; }
	inline float* get_address_of_delay_9() { return &___delay_9; }
	inline void set_delay_9(float value)
	{
		___delay_9 = value;
	}

	inline static int32_t get_offset_of_loopType_10() { return static_cast<int32_t>(offsetof(SA_iTween_t1550711905, ___loopType_10)); }
	inline int32_t get_loopType_10() const { return ___loopType_10; }
	inline int32_t* get_address_of_loopType_10() { return &___loopType_10; }
	inline void set_loopType_10(int32_t value)
	{
		___loopType_10 = value;
	}

	inline static int32_t get_offset_of_isRunning_11() { return static_cast<int32_t>(offsetof(SA_iTween_t1550711905, ___isRunning_11)); }
	inline bool get_isRunning_11() const { return ___isRunning_11; }
	inline bool* get_address_of_isRunning_11() { return &___isRunning_11; }
	inline void set_isRunning_11(bool value)
	{
		___isRunning_11 = value;
	}

	inline static int32_t get_offset_of_isPaused_12() { return static_cast<int32_t>(offsetof(SA_iTween_t1550711905, ___isPaused_12)); }
	inline bool get_isPaused_12() const { return ___isPaused_12; }
	inline bool* get_address_of_isPaused_12() { return &___isPaused_12; }
	inline void set_isPaused_12(bool value)
	{
		___isPaused_12 = value;
	}

	inline static int32_t get_offset_of__name_13() { return static_cast<int32_t>(offsetof(SA_iTween_t1550711905, ____name_13)); }
	inline String_t* get__name_13() const { return ____name_13; }
	inline String_t** get_address_of__name_13() { return &____name_13; }
	inline void set__name_13(String_t* value)
	{
		____name_13 = value;
		Il2CppCodeGenWriteBarrier((&____name_13), value);
	}

	inline static int32_t get_offset_of_runningTime_14() { return static_cast<int32_t>(offsetof(SA_iTween_t1550711905, ___runningTime_14)); }
	inline float get_runningTime_14() const { return ___runningTime_14; }
	inline float* get_address_of_runningTime_14() { return &___runningTime_14; }
	inline void set_runningTime_14(float value)
	{
		___runningTime_14 = value;
	}

	inline static int32_t get_offset_of_percentage_15() { return static_cast<int32_t>(offsetof(SA_iTween_t1550711905, ___percentage_15)); }
	inline float get_percentage_15() const { return ___percentage_15; }
	inline float* get_address_of_percentage_15() { return &___percentage_15; }
	inline void set_percentage_15(float value)
	{
		___percentage_15 = value;
	}

	inline static int32_t get_offset_of_delayStarted_16() { return static_cast<int32_t>(offsetof(SA_iTween_t1550711905, ___delayStarted_16)); }
	inline float get_delayStarted_16() const { return ___delayStarted_16; }
	inline float* get_address_of_delayStarted_16() { return &___delayStarted_16; }
	inline void set_delayStarted_16(float value)
	{
		___delayStarted_16 = value;
	}

	inline static int32_t get_offset_of_kinematic_17() { return static_cast<int32_t>(offsetof(SA_iTween_t1550711905, ___kinematic_17)); }
	inline bool get_kinematic_17() const { return ___kinematic_17; }
	inline bool* get_address_of_kinematic_17() { return &___kinematic_17; }
	inline void set_kinematic_17(bool value)
	{
		___kinematic_17 = value;
	}

	inline static int32_t get_offset_of_isLocal_18() { return static_cast<int32_t>(offsetof(SA_iTween_t1550711905, ___isLocal_18)); }
	inline bool get_isLocal_18() const { return ___isLocal_18; }
	inline bool* get_address_of_isLocal_18() { return &___isLocal_18; }
	inline void set_isLocal_18(bool value)
	{
		___isLocal_18 = value;
	}

	inline static int32_t get_offset_of_loop_19() { return static_cast<int32_t>(offsetof(SA_iTween_t1550711905, ___loop_19)); }
	inline bool get_loop_19() const { return ___loop_19; }
	inline bool* get_address_of_loop_19() { return &___loop_19; }
	inline void set_loop_19(bool value)
	{
		___loop_19 = value;
	}

	inline static int32_t get_offset_of_reverse_20() { return static_cast<int32_t>(offsetof(SA_iTween_t1550711905, ___reverse_20)); }
	inline bool get_reverse_20() const { return ___reverse_20; }
	inline bool* get_address_of_reverse_20() { return &___reverse_20; }
	inline void set_reverse_20(bool value)
	{
		___reverse_20 = value;
	}

	inline static int32_t get_offset_of_wasPaused_21() { return static_cast<int32_t>(offsetof(SA_iTween_t1550711905, ___wasPaused_21)); }
	inline bool get_wasPaused_21() const { return ___wasPaused_21; }
	inline bool* get_address_of_wasPaused_21() { return &___wasPaused_21; }
	inline void set_wasPaused_21(bool value)
	{
		___wasPaused_21 = value;
	}

	inline static int32_t get_offset_of_physics_22() { return static_cast<int32_t>(offsetof(SA_iTween_t1550711905, ___physics_22)); }
	inline bool get_physics_22() const { return ___physics_22; }
	inline bool* get_address_of_physics_22() { return &___physics_22; }
	inline void set_physics_22(bool value)
	{
		___physics_22 = value;
	}

	inline static int32_t get_offset_of_tweenArguments_23() { return static_cast<int32_t>(offsetof(SA_iTween_t1550711905, ___tweenArguments_23)); }
	inline Hashtable_t1853889766 * get_tweenArguments_23() const { return ___tweenArguments_23; }
	inline Hashtable_t1853889766 ** get_address_of_tweenArguments_23() { return &___tweenArguments_23; }
	inline void set_tweenArguments_23(Hashtable_t1853889766 * value)
	{
		___tweenArguments_23 = value;
		Il2CppCodeGenWriteBarrier((&___tweenArguments_23), value);
	}

	inline static int32_t get_offset_of_space_24() { return static_cast<int32_t>(offsetof(SA_iTween_t1550711905, ___space_24)); }
	inline int32_t get_space_24() const { return ___space_24; }
	inline int32_t* get_address_of_space_24() { return &___space_24; }
	inline void set_space_24(int32_t value)
	{
		___space_24 = value;
	}

	inline static int32_t get_offset_of_ease_25() { return static_cast<int32_t>(offsetof(SA_iTween_t1550711905, ___ease_25)); }
	inline EasingFunction_t2379729698 * get_ease_25() const { return ___ease_25; }
	inline EasingFunction_t2379729698 ** get_address_of_ease_25() { return &___ease_25; }
	inline void set_ease_25(EasingFunction_t2379729698 * value)
	{
		___ease_25 = value;
		Il2CppCodeGenWriteBarrier((&___ease_25), value);
	}

	inline static int32_t get_offset_of_apply_26() { return static_cast<int32_t>(offsetof(SA_iTween_t1550711905, ___apply_26)); }
	inline ApplyTween_t3100151106 * get_apply_26() const { return ___apply_26; }
	inline ApplyTween_t3100151106 ** get_address_of_apply_26() { return &___apply_26; }
	inline void set_apply_26(ApplyTween_t3100151106 * value)
	{
		___apply_26 = value;
		Il2CppCodeGenWriteBarrier((&___apply_26), value);
	}

	inline static int32_t get_offset_of_audioSource_27() { return static_cast<int32_t>(offsetof(SA_iTween_t1550711905, ___audioSource_27)); }
	inline AudioSource_t3935305588 * get_audioSource_27() const { return ___audioSource_27; }
	inline AudioSource_t3935305588 ** get_address_of_audioSource_27() { return &___audioSource_27; }
	inline void set_audioSource_27(AudioSource_t3935305588 * value)
	{
		___audioSource_27 = value;
		Il2CppCodeGenWriteBarrier((&___audioSource_27), value);
	}

	inline static int32_t get_offset_of_vector3s_28() { return static_cast<int32_t>(offsetof(SA_iTween_t1550711905, ___vector3s_28)); }
	inline Vector3U5BU5D_t1718750761* get_vector3s_28() const { return ___vector3s_28; }
	inline Vector3U5BU5D_t1718750761** get_address_of_vector3s_28() { return &___vector3s_28; }
	inline void set_vector3s_28(Vector3U5BU5D_t1718750761* value)
	{
		___vector3s_28 = value;
		Il2CppCodeGenWriteBarrier((&___vector3s_28), value);
	}

	inline static int32_t get_offset_of_vector2s_29() { return static_cast<int32_t>(offsetof(SA_iTween_t1550711905, ___vector2s_29)); }
	inline Vector2U5BU5D_t1457185986* get_vector2s_29() const { return ___vector2s_29; }
	inline Vector2U5BU5D_t1457185986** get_address_of_vector2s_29() { return &___vector2s_29; }
	inline void set_vector2s_29(Vector2U5BU5D_t1457185986* value)
	{
		___vector2s_29 = value;
		Il2CppCodeGenWriteBarrier((&___vector2s_29), value);
	}

	inline static int32_t get_offset_of_colors_30() { return static_cast<int32_t>(offsetof(SA_iTween_t1550711905, ___colors_30)); }
	inline ColorU5B0___U2C0___U5D_t941916414* get_colors_30() const { return ___colors_30; }
	inline ColorU5B0___U2C0___U5D_t941916414** get_address_of_colors_30() { return &___colors_30; }
	inline void set_colors_30(ColorU5B0___U2C0___U5D_t941916414* value)
	{
		___colors_30 = value;
		Il2CppCodeGenWriteBarrier((&___colors_30), value);
	}

	inline static int32_t get_offset_of_floats_31() { return static_cast<int32_t>(offsetof(SA_iTween_t1550711905, ___floats_31)); }
	inline SingleU5BU5D_t1444911251* get_floats_31() const { return ___floats_31; }
	inline SingleU5BU5D_t1444911251** get_address_of_floats_31() { return &___floats_31; }
	inline void set_floats_31(SingleU5BU5D_t1444911251* value)
	{
		___floats_31 = value;
		Il2CppCodeGenWriteBarrier((&___floats_31), value);
	}

	inline static int32_t get_offset_of_rects_32() { return static_cast<int32_t>(offsetof(SA_iTween_t1550711905, ___rects_32)); }
	inline RectU5BU5D_t2936723554* get_rects_32() const { return ___rects_32; }
	inline RectU5BU5D_t2936723554** get_address_of_rects_32() { return &___rects_32; }
	inline void set_rects_32(RectU5BU5D_t2936723554* value)
	{
		___rects_32 = value;
		Il2CppCodeGenWriteBarrier((&___rects_32), value);
	}

	inline static int32_t get_offset_of_path_33() { return static_cast<int32_t>(offsetof(SA_iTween_t1550711905, ___path_33)); }
	inline CRSpline_t2258853177 * get_path_33() const { return ___path_33; }
	inline CRSpline_t2258853177 ** get_address_of_path_33() { return &___path_33; }
	inline void set_path_33(CRSpline_t2258853177 * value)
	{
		___path_33 = value;
		Il2CppCodeGenWriteBarrier((&___path_33), value);
	}

	inline static int32_t get_offset_of_preUpdate_34() { return static_cast<int32_t>(offsetof(SA_iTween_t1550711905, ___preUpdate_34)); }
	inline Vector3_t3722313464  get_preUpdate_34() const { return ___preUpdate_34; }
	inline Vector3_t3722313464 * get_address_of_preUpdate_34() { return &___preUpdate_34; }
	inline void set_preUpdate_34(Vector3_t3722313464  value)
	{
		___preUpdate_34 = value;
	}

	inline static int32_t get_offset_of_postUpdate_35() { return static_cast<int32_t>(offsetof(SA_iTween_t1550711905, ___postUpdate_35)); }
	inline Vector3_t3722313464  get_postUpdate_35() const { return ___postUpdate_35; }
	inline Vector3_t3722313464 * get_address_of_postUpdate_35() { return &___postUpdate_35; }
	inline void set_postUpdate_35(Vector3_t3722313464  value)
	{
		___postUpdate_35 = value;
	}

	inline static int32_t get_offset_of_namedcolorvalue_36() { return static_cast<int32_t>(offsetof(SA_iTween_t1550711905, ___namedcolorvalue_36)); }
	inline int32_t get_namedcolorvalue_36() const { return ___namedcolorvalue_36; }
	inline int32_t* get_address_of_namedcolorvalue_36() { return &___namedcolorvalue_36; }
	inline void set_namedcolorvalue_36(int32_t value)
	{
		___namedcolorvalue_36 = value;
	}

	inline static int32_t get_offset_of_lastRealTime_37() { return static_cast<int32_t>(offsetof(SA_iTween_t1550711905, ___lastRealTime_37)); }
	inline float get_lastRealTime_37() const { return ___lastRealTime_37; }
	inline float* get_address_of_lastRealTime_37() { return &___lastRealTime_37; }
	inline void set_lastRealTime_37(float value)
	{
		___lastRealTime_37 = value;
	}

	inline static int32_t get_offset_of_useRealTime_38() { return static_cast<int32_t>(offsetof(SA_iTween_t1550711905, ___useRealTime_38)); }
	inline bool get_useRealTime_38() const { return ___useRealTime_38; }
	inline bool* get_address_of_useRealTime_38() { return &___useRealTime_38; }
	inline void set_useRealTime_38(bool value)
	{
		___useRealTime_38 = value;
	}

	inline static int32_t get_offset_of_thisTransform_39() { return static_cast<int32_t>(offsetof(SA_iTween_t1550711905, ___thisTransform_39)); }
	inline Transform_t3600365921 * get_thisTransform_39() const { return ___thisTransform_39; }
	inline Transform_t3600365921 ** get_address_of_thisTransform_39() { return &___thisTransform_39; }
	inline void set_thisTransform_39(Transform_t3600365921 * value)
	{
		___thisTransform_39 = value;
		Il2CppCodeGenWriteBarrier((&___thisTransform_39), value);
	}
};

struct SA_iTween_t1550711905_StaticFields
{
public:
	// System.Collections.Generic.List`1<System.Collections.Hashtable> SA.Common.Animation.SA_iTween::tweens
	List_1_t3325964508 * ___tweens_2;
	// UnityEngine.GameObject SA.Common.Animation.SA_iTween::cameraFade
	GameObject_t1113636619 * ___cameraFade_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> SA.Common.Animation.SA_iTween::<>f__switch$map2
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map2_40;

public:
	inline static int32_t get_offset_of_tweens_2() { return static_cast<int32_t>(offsetof(SA_iTween_t1550711905_StaticFields, ___tweens_2)); }
	inline List_1_t3325964508 * get_tweens_2() const { return ___tweens_2; }
	inline List_1_t3325964508 ** get_address_of_tweens_2() { return &___tweens_2; }
	inline void set_tweens_2(List_1_t3325964508 * value)
	{
		___tweens_2 = value;
		Il2CppCodeGenWriteBarrier((&___tweens_2), value);
	}

	inline static int32_t get_offset_of_cameraFade_3() { return static_cast<int32_t>(offsetof(SA_iTween_t1550711905_StaticFields, ___cameraFade_3)); }
	inline GameObject_t1113636619 * get_cameraFade_3() const { return ___cameraFade_3; }
	inline GameObject_t1113636619 ** get_address_of_cameraFade_3() { return &___cameraFade_3; }
	inline void set_cameraFade_3(GameObject_t1113636619 * value)
	{
		___cameraFade_3 = value;
		Il2CppCodeGenWriteBarrier((&___cameraFade_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map2_40() { return static_cast<int32_t>(offsetof(SA_iTween_t1550711905_StaticFields, ___U3CU3Ef__switchU24map2_40)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map2_40() const { return ___U3CU3Ef__switchU24map2_40; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map2_40() { return &___U3CU3Ef__switchU24map2_40; }
	inline void set_U3CU3Ef__switchU24map2_40(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map2_40 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map2_40), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SA_ITWEEN_T1550711905_H
#ifndef VALUESTWEEN_T2434473678_H
#define VALUESTWEEN_T2434473678_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Animation.ValuesTween
struct  ValuesTween_t2434473678  : public MonoBehaviour_t3962482529
{
public:
	// System.Action SA.Common.Animation.ValuesTween::OnComplete
	Action_t1264377477 * ___OnComplete_2;
	// System.Action`1<System.Single> SA.Common.Animation.ValuesTween::OnValueChanged
	Action_1_t1569734369 * ___OnValueChanged_3;
	// System.Action`1<UnityEngine.Vector3> SA.Common.Animation.ValuesTween::OnVectorValueChanged
	Action_1_t3894781059 * ___OnVectorValueChanged_4;
	// System.Boolean SA.Common.Animation.ValuesTween::DestoryGameObjectOnComplete
	bool ___DestoryGameObjectOnComplete_5;
	// System.Single SA.Common.Animation.ValuesTween::FinalFloatValue
	float ___FinalFloatValue_6;
	// UnityEngine.Vector3 SA.Common.Animation.ValuesTween::FinalVectorValue
	Vector3_t3722313464  ___FinalVectorValue_7;

public:
	inline static int32_t get_offset_of_OnComplete_2() { return static_cast<int32_t>(offsetof(ValuesTween_t2434473678, ___OnComplete_2)); }
	inline Action_t1264377477 * get_OnComplete_2() const { return ___OnComplete_2; }
	inline Action_t1264377477 ** get_address_of_OnComplete_2() { return &___OnComplete_2; }
	inline void set_OnComplete_2(Action_t1264377477 * value)
	{
		___OnComplete_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnComplete_2), value);
	}

	inline static int32_t get_offset_of_OnValueChanged_3() { return static_cast<int32_t>(offsetof(ValuesTween_t2434473678, ___OnValueChanged_3)); }
	inline Action_1_t1569734369 * get_OnValueChanged_3() const { return ___OnValueChanged_3; }
	inline Action_1_t1569734369 ** get_address_of_OnValueChanged_3() { return &___OnValueChanged_3; }
	inline void set_OnValueChanged_3(Action_1_t1569734369 * value)
	{
		___OnValueChanged_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnValueChanged_3), value);
	}

	inline static int32_t get_offset_of_OnVectorValueChanged_4() { return static_cast<int32_t>(offsetof(ValuesTween_t2434473678, ___OnVectorValueChanged_4)); }
	inline Action_1_t3894781059 * get_OnVectorValueChanged_4() const { return ___OnVectorValueChanged_4; }
	inline Action_1_t3894781059 ** get_address_of_OnVectorValueChanged_4() { return &___OnVectorValueChanged_4; }
	inline void set_OnVectorValueChanged_4(Action_1_t3894781059 * value)
	{
		___OnVectorValueChanged_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnVectorValueChanged_4), value);
	}

	inline static int32_t get_offset_of_DestoryGameObjectOnComplete_5() { return static_cast<int32_t>(offsetof(ValuesTween_t2434473678, ___DestoryGameObjectOnComplete_5)); }
	inline bool get_DestoryGameObjectOnComplete_5() const { return ___DestoryGameObjectOnComplete_5; }
	inline bool* get_address_of_DestoryGameObjectOnComplete_5() { return &___DestoryGameObjectOnComplete_5; }
	inline void set_DestoryGameObjectOnComplete_5(bool value)
	{
		___DestoryGameObjectOnComplete_5 = value;
	}

	inline static int32_t get_offset_of_FinalFloatValue_6() { return static_cast<int32_t>(offsetof(ValuesTween_t2434473678, ___FinalFloatValue_6)); }
	inline float get_FinalFloatValue_6() const { return ___FinalFloatValue_6; }
	inline float* get_address_of_FinalFloatValue_6() { return &___FinalFloatValue_6; }
	inline void set_FinalFloatValue_6(float value)
	{
		___FinalFloatValue_6 = value;
	}

	inline static int32_t get_offset_of_FinalVectorValue_7() { return static_cast<int32_t>(offsetof(ValuesTween_t2434473678, ___FinalVectorValue_7)); }
	inline Vector3_t3722313464  get_FinalVectorValue_7() const { return ___FinalVectorValue_7; }
	inline Vector3_t3722313464 * get_address_of_FinalVectorValue_7() { return &___FinalVectorValue_7; }
	inline void set_FinalVectorValue_7(Vector3_t3722313464  value)
	{
		___FinalVectorValue_7 = value;
	}
};

struct ValuesTween_t2434473678_StaticFields
{
public:
	// System.Action SA.Common.Animation.ValuesTween::<>f__am$cache0
	Action_t1264377477 * ___U3CU3Ef__amU24cache0_8;
	// System.Action`1<System.Single> SA.Common.Animation.ValuesTween::<>f__am$cache1
	Action_1_t1569734369 * ___U3CU3Ef__amU24cache1_9;
	// System.Action`1<UnityEngine.Vector3> SA.Common.Animation.ValuesTween::<>f__am$cache2
	Action_1_t3894781059 * ___U3CU3Ef__amU24cache2_10;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_8() { return static_cast<int32_t>(offsetof(ValuesTween_t2434473678_StaticFields, ___U3CU3Ef__amU24cache0_8)); }
	inline Action_t1264377477 * get_U3CU3Ef__amU24cache0_8() const { return ___U3CU3Ef__amU24cache0_8; }
	inline Action_t1264377477 ** get_address_of_U3CU3Ef__amU24cache0_8() { return &___U3CU3Ef__amU24cache0_8; }
	inline void set_U3CU3Ef__amU24cache0_8(Action_t1264377477 * value)
	{
		___U3CU3Ef__amU24cache0_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_8), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_9() { return static_cast<int32_t>(offsetof(ValuesTween_t2434473678_StaticFields, ___U3CU3Ef__amU24cache1_9)); }
	inline Action_1_t1569734369 * get_U3CU3Ef__amU24cache1_9() const { return ___U3CU3Ef__amU24cache1_9; }
	inline Action_1_t1569734369 ** get_address_of_U3CU3Ef__amU24cache1_9() { return &___U3CU3Ef__amU24cache1_9; }
	inline void set_U3CU3Ef__amU24cache1_9(Action_1_t1569734369 * value)
	{
		___U3CU3Ef__amU24cache1_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_9), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_10() { return static_cast<int32_t>(offsetof(ValuesTween_t2434473678_StaticFields, ___U3CU3Ef__amU24cache2_10)); }
	inline Action_1_t3894781059 * get_U3CU3Ef__amU24cache2_10() const { return ___U3CU3Ef__amU24cache2_10; }
	inline Action_1_t3894781059 ** get_address_of_U3CU3Ef__amU24cache2_10() { return &___U3CU3Ef__amU24cache2_10; }
	inline void set_U3CU3Ef__amU24cache2_10(Action_1_t3894781059 * value)
	{
		___U3CU3Ef__amU24cache2_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALUESTWEEN_T2434473678_H
#ifndef APPEVENTHANDLEREXAMPLE_T2737898202_H
#define APPEVENTHANDLEREXAMPLE_T2737898202_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AppEventHandlerExample
struct  AppEventHandlerExample_t2737898202  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPEVENTHANDLEREXAMPLE_T2737898202_H
#ifndef GAMECENTERTVOSEXAMPLE_T1376821359_H
#define GAMECENTERTVOSEXAMPLE_T1376821359_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameCenterTvOSExample
struct  GameCenterTvOSExample_t1376821359  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 GameCenterTvOSExample::hiScore
	int32_t ___hiScore_2;
	// System.Boolean GameCenterTvOSExample::_IsUILocked
	bool ____IsUILocked_3;
	// System.String GameCenterTvOSExample::TEST_LEADERBOARD_1
	String_t* ___TEST_LEADERBOARD_1_4;
	// System.String GameCenterTvOSExample::TEST_ACHIEVEMENT_1_ID
	String_t* ___TEST_ACHIEVEMENT_1_ID_5;
	// System.String GameCenterTvOSExample::TEST_ACHIEVEMENT_2_ID
	String_t* ___TEST_ACHIEVEMENT_2_ID_6;

public:
	inline static int32_t get_offset_of_hiScore_2() { return static_cast<int32_t>(offsetof(GameCenterTvOSExample_t1376821359, ___hiScore_2)); }
	inline int32_t get_hiScore_2() const { return ___hiScore_2; }
	inline int32_t* get_address_of_hiScore_2() { return &___hiScore_2; }
	inline void set_hiScore_2(int32_t value)
	{
		___hiScore_2 = value;
	}

	inline static int32_t get_offset_of__IsUILocked_3() { return static_cast<int32_t>(offsetof(GameCenterTvOSExample_t1376821359, ____IsUILocked_3)); }
	inline bool get__IsUILocked_3() const { return ____IsUILocked_3; }
	inline bool* get_address_of__IsUILocked_3() { return &____IsUILocked_3; }
	inline void set__IsUILocked_3(bool value)
	{
		____IsUILocked_3 = value;
	}

	inline static int32_t get_offset_of_TEST_LEADERBOARD_1_4() { return static_cast<int32_t>(offsetof(GameCenterTvOSExample_t1376821359, ___TEST_LEADERBOARD_1_4)); }
	inline String_t* get_TEST_LEADERBOARD_1_4() const { return ___TEST_LEADERBOARD_1_4; }
	inline String_t** get_address_of_TEST_LEADERBOARD_1_4() { return &___TEST_LEADERBOARD_1_4; }
	inline void set_TEST_LEADERBOARD_1_4(String_t* value)
	{
		___TEST_LEADERBOARD_1_4 = value;
		Il2CppCodeGenWriteBarrier((&___TEST_LEADERBOARD_1_4), value);
	}

	inline static int32_t get_offset_of_TEST_ACHIEVEMENT_1_ID_5() { return static_cast<int32_t>(offsetof(GameCenterTvOSExample_t1376821359, ___TEST_ACHIEVEMENT_1_ID_5)); }
	inline String_t* get_TEST_ACHIEVEMENT_1_ID_5() const { return ___TEST_ACHIEVEMENT_1_ID_5; }
	inline String_t** get_address_of_TEST_ACHIEVEMENT_1_ID_5() { return &___TEST_ACHIEVEMENT_1_ID_5; }
	inline void set_TEST_ACHIEVEMENT_1_ID_5(String_t* value)
	{
		___TEST_ACHIEVEMENT_1_ID_5 = value;
		Il2CppCodeGenWriteBarrier((&___TEST_ACHIEVEMENT_1_ID_5), value);
	}

	inline static int32_t get_offset_of_TEST_ACHIEVEMENT_2_ID_6() { return static_cast<int32_t>(offsetof(GameCenterTvOSExample_t1376821359, ___TEST_ACHIEVEMENT_2_ID_6)); }
	inline String_t* get_TEST_ACHIEVEMENT_2_ID_6() const { return ___TEST_ACHIEVEMENT_2_ID_6; }
	inline String_t** get_address_of_TEST_ACHIEVEMENT_2_ID_6() { return &___TEST_ACHIEVEMENT_2_ID_6; }
	inline void set_TEST_ACHIEVEMENT_2_ID_6(String_t* value)
	{
		___TEST_ACHIEVEMENT_2_ID_6 = value;
		Il2CppCodeGenWriteBarrier((&___TEST_ACHIEVEMENT_2_ID_6), value);
	}
};

struct GameCenterTvOSExample_t1376821359_StaticFields
{
public:
	// System.Action`1<SA.Common.Models.Result> GameCenterTvOSExample::<>f__am$cache0
	Action_1_t47902017 * ___U3CU3Ef__amU24cache0_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_7() { return static_cast<int32_t>(offsetof(GameCenterTvOSExample_t1376821359_StaticFields, ___U3CU3Ef__amU24cache0_7)); }
	inline Action_1_t47902017 * get_U3CU3Ef__amU24cache0_7() const { return ___U3CU3Ef__amU24cache0_7; }
	inline Action_1_t47902017 ** get_address_of_U3CU3Ef__amU24cache0_7() { return &___U3CU3Ef__amU24cache0_7; }
	inline void set_U3CU3Ef__amU24cache0_7(Action_1_t47902017 * value)
	{
		___U3CU3Ef__amU24cache0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMECENTERTVOSEXAMPLE_T1376821359_H
#ifndef LOCALSTORAGEUSEEXAMPLE_T854350395_H
#define LOCALSTORAGEUSEEXAMPLE_T854350395_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LocalStorageUseExample
struct  LocalStorageUseExample_t854350395  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCALSTORAGEUSEEXAMPLE_T854350395_H
#ifndef TVOSINAPPSEXAMPLE_T3613881217_H
#define TVOSINAPPSEXAMPLE_T3613881217_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TvOSInAppsExample
struct  TvOSInAppsExample_t3613881217  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TVOSINAPPSEXAMPLE_T3613881217_H
#ifndef TVOSCLOUDEXAMPLE_T4075801133_H
#define TVOSCLOUDEXAMPLE_T4075801133_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TvOsCloudExample
struct  TvOsCloudExample_t4075801133  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct TvOsCloudExample_t4075801133_StaticFields
{
public:
	// System.Action`1<iCloudData> TvOsCloudExample::<>f__am$cache0
	Action_1_t267273497 * ___U3CU3Ef__amU24cache0_2;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_2() { return static_cast<int32_t>(offsetof(TvOsCloudExample_t4075801133_StaticFields, ___U3CU3Ef__amU24cache0_2)); }
	inline Action_1_t267273497 * get_U3CU3Ef__amU24cache0_2() const { return ___U3CU3Ef__amU24cache0_2; }
	inline Action_1_t267273497 ** get_address_of_U3CU3Ef__amU24cache0_2() { return &___U3CU3Ef__amU24cache0_2; }
	inline void set_U3CU3Ef__amU24cache0_2(Action_1_t267273497 * value)
	{
		___U3CU3Ef__amU24cache0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TVOSCLOUDEXAMPLE_T4075801133_H
#ifndef IOSSOCIALUSEEXAMPLE_T1226332829_H
#define IOSSOCIALUSEEXAMPLE_T1226332829_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IOSSocialUseExample
struct  IOSSocialUseExample_t1226332829  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GUIStyle IOSSocialUseExample::style
	GUIStyle_t3956901511 * ___style_2;
	// UnityEngine.GUIStyle IOSSocialUseExample::style2
	GUIStyle_t3956901511 * ___style2_3;
	// UnityEngine.Texture2D IOSSocialUseExample::drawTexture
	Texture2D_t3840446185 * ___drawTexture_4;
	// UnityEngine.Texture2D IOSSocialUseExample::textureForPost
	Texture2D_t3840446185 * ___textureForPost_5;

public:
	inline static int32_t get_offset_of_style_2() { return static_cast<int32_t>(offsetof(IOSSocialUseExample_t1226332829, ___style_2)); }
	inline GUIStyle_t3956901511 * get_style_2() const { return ___style_2; }
	inline GUIStyle_t3956901511 ** get_address_of_style_2() { return &___style_2; }
	inline void set_style_2(GUIStyle_t3956901511 * value)
	{
		___style_2 = value;
		Il2CppCodeGenWriteBarrier((&___style_2), value);
	}

	inline static int32_t get_offset_of_style2_3() { return static_cast<int32_t>(offsetof(IOSSocialUseExample_t1226332829, ___style2_3)); }
	inline GUIStyle_t3956901511 * get_style2_3() const { return ___style2_3; }
	inline GUIStyle_t3956901511 ** get_address_of_style2_3() { return &___style2_3; }
	inline void set_style2_3(GUIStyle_t3956901511 * value)
	{
		___style2_3 = value;
		Il2CppCodeGenWriteBarrier((&___style2_3), value);
	}

	inline static int32_t get_offset_of_drawTexture_4() { return static_cast<int32_t>(offsetof(IOSSocialUseExample_t1226332829, ___drawTexture_4)); }
	inline Texture2D_t3840446185 * get_drawTexture_4() const { return ___drawTexture_4; }
	inline Texture2D_t3840446185 ** get_address_of_drawTexture_4() { return &___drawTexture_4; }
	inline void set_drawTexture_4(Texture2D_t3840446185 * value)
	{
		___drawTexture_4 = value;
		Il2CppCodeGenWriteBarrier((&___drawTexture_4), value);
	}

	inline static int32_t get_offset_of_textureForPost_5() { return static_cast<int32_t>(offsetof(IOSSocialUseExample_t1226332829, ___textureForPost_5)); }
	inline Texture2D_t3840446185 * get_textureForPost_5() const { return ___textureForPost_5; }
	inline Texture2D_t3840446185 ** get_address_of_textureForPost_5() { return &___textureForPost_5; }
	inline void set_textureForPost_5(Texture2D_t3840446185 * value)
	{
		___textureForPost_5 = value;
		Il2CppCodeGenWriteBarrier((&___textureForPost_5), value);
	}
};

struct IOSSocialUseExample_t1226332829_StaticFields
{
public:
	// System.Action`1<TextMessageComposeResult> IOSSocialUseExample::<>f__am$cache0
	Action_1_t1565920682 * ___U3CU3Ef__amU24cache0_6;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_6() { return static_cast<int32_t>(offsetof(IOSSocialUseExample_t1226332829_StaticFields, ___U3CU3Ef__amU24cache0_6)); }
	inline Action_1_t1565920682 * get_U3CU3Ef__amU24cache0_6() const { return ___U3CU3Ef__amU24cache0_6; }
	inline Action_1_t1565920682 ** get_address_of_U3CU3Ef__amU24cache0_6() { return &___U3CU3Ef__amU24cache0_6; }
	inline void set_U3CU3Ef__amU24cache0_6(Action_1_t1565920682 * value)
	{
		___U3CU3Ef__amU24cache0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSSOCIALUSEEXAMPLE_T1226332829_H
#ifndef PREFABASYNCLOADER_T3137627864_H
#define PREFABASYNCLOADER_T3137627864_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Models.PrefabAsyncLoader
struct  PrefabAsyncLoader_t3137627864  : public MonoBehaviour_t3962482529
{
public:
	// System.String SA.Common.Models.PrefabAsyncLoader::PrefabPath
	String_t* ___PrefabPath_2;
	// System.Action`1<UnityEngine.GameObject> SA.Common.Models.PrefabAsyncLoader::ObjectLoadedAction
	Action_1_t1286104214 * ___ObjectLoadedAction_3;

public:
	inline static int32_t get_offset_of_PrefabPath_2() { return static_cast<int32_t>(offsetof(PrefabAsyncLoader_t3137627864, ___PrefabPath_2)); }
	inline String_t* get_PrefabPath_2() const { return ___PrefabPath_2; }
	inline String_t** get_address_of_PrefabPath_2() { return &___PrefabPath_2; }
	inline void set_PrefabPath_2(String_t* value)
	{
		___PrefabPath_2 = value;
		Il2CppCodeGenWriteBarrier((&___PrefabPath_2), value);
	}

	inline static int32_t get_offset_of_ObjectLoadedAction_3() { return static_cast<int32_t>(offsetof(PrefabAsyncLoader_t3137627864, ___ObjectLoadedAction_3)); }
	inline Action_1_t1286104214 * get_ObjectLoadedAction_3() const { return ___ObjectLoadedAction_3; }
	inline Action_1_t1286104214 ** get_address_of_ObjectLoadedAction_3() { return &___ObjectLoadedAction_3; }
	inline void set_ObjectLoadedAction_3(Action_1_t1286104214 * value)
	{
		___ObjectLoadedAction_3 = value;
		Il2CppCodeGenWriteBarrier((&___ObjectLoadedAction_3), value);
	}
};

struct PrefabAsyncLoader_t3137627864_StaticFields
{
public:
	// System.Action`1<UnityEngine.GameObject> SA.Common.Models.PrefabAsyncLoader::<>f__am$cache0
	Action_1_t1286104214 * ___U3CU3Ef__amU24cache0_4;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_4() { return static_cast<int32_t>(offsetof(PrefabAsyncLoader_t3137627864_StaticFields, ___U3CU3Ef__amU24cache0_4)); }
	inline Action_1_t1286104214 * get_U3CU3Ef__amU24cache0_4() const { return ___U3CU3Ef__amU24cache0_4; }
	inline Action_1_t1286104214 ** get_address_of_U3CU3Ef__amU24cache0_4() { return &___U3CU3Ef__amU24cache0_4; }
	inline void set_U3CU3Ef__amU24cache0_4(Action_1_t1286104214 * value)
	{
		___U3CU3Ef__amU24cache0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREFABASYNCLOADER_T3137627864_H
#ifndef CONNECTIONBUTTON_T3407155931_H
#define CONNECTIONBUTTON_T3407155931_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConnectionButton
struct  ConnectionButton_t3407155931  : public MonoBehaviour_t3962482529
{
public:
	// System.Single ConnectionButton::w
	float ___w_2;
	// System.Single ConnectionButton::h
	float ___h_3;
	// UnityEngine.Rect ConnectionButton::r
	Rect_t2360479859  ___r_4;

public:
	inline static int32_t get_offset_of_w_2() { return static_cast<int32_t>(offsetof(ConnectionButton_t3407155931, ___w_2)); }
	inline float get_w_2() const { return ___w_2; }
	inline float* get_address_of_w_2() { return &___w_2; }
	inline void set_w_2(float value)
	{
		___w_2 = value;
	}

	inline static int32_t get_offset_of_h_3() { return static_cast<int32_t>(offsetof(ConnectionButton_t3407155931, ___h_3)); }
	inline float get_h_3() const { return ___h_3; }
	inline float* get_address_of_h_3() { return &___h_3; }
	inline void set_h_3(float value)
	{
		___h_3 = value;
	}

	inline static int32_t get_offset_of_r_4() { return static_cast<int32_t>(offsetof(ConnectionButton_t3407155931, ___r_4)); }
	inline Rect_t2360479859  get_r_4() const { return ___r_4; }
	inline Rect_t2360479859 * get_address_of_r_4() { return &___r_4; }
	inline void set_r_4(Rect_t2360479859  value)
	{
		___r_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTIONBUTTON_T3407155931_H
#ifndef INVOKER_T4214570440_H
#define INVOKER_T4214570440_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Models.Invoker
struct  Invoker_t4214570440  : public MonoBehaviour_t3962482529
{
public:
	// System.Action SA.Common.Models.Invoker::_callback
	Action_t1264377477 * ____callback_2;

public:
	inline static int32_t get_offset_of__callback_2() { return static_cast<int32_t>(offsetof(Invoker_t4214570440, ____callback_2)); }
	inline Action_t1264377477 * get__callback_2() const { return ____callback_2; }
	inline Action_t1264377477 ** get_address_of__callback_2() { return &____callback_2; }
	inline void set__callback_2(Action_t1264377477 * value)
	{
		____callback_2 = value;
		Il2CppCodeGenWriteBarrier((&____callback_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVOKER_T4214570440_H
#ifndef PTPGAMECONTROLLER_T1530419951_H
#define PTPGAMECONTROLLER_T1530419951_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PTPGameController
struct  PTPGameController_t1530419951  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject PTPGameController::pref
	GameObject_t1113636619 * ___pref_2;
	// DisconnectButton PTPGameController::d
	DisconnectButton_t3682185937 * ___d_3;
	// ConnectionButton PTPGameController::b
	ConnectionButton_t3407155931 * ___b_4;
	// ClickManagerExample PTPGameController::m
	ClickManagerExample_t3328835271 * ___m_5;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> PTPGameController::spheres
	List_1_t2585711361 * ___spheres_7;

public:
	inline static int32_t get_offset_of_pref_2() { return static_cast<int32_t>(offsetof(PTPGameController_t1530419951, ___pref_2)); }
	inline GameObject_t1113636619 * get_pref_2() const { return ___pref_2; }
	inline GameObject_t1113636619 ** get_address_of_pref_2() { return &___pref_2; }
	inline void set_pref_2(GameObject_t1113636619 * value)
	{
		___pref_2 = value;
		Il2CppCodeGenWriteBarrier((&___pref_2), value);
	}

	inline static int32_t get_offset_of_d_3() { return static_cast<int32_t>(offsetof(PTPGameController_t1530419951, ___d_3)); }
	inline DisconnectButton_t3682185937 * get_d_3() const { return ___d_3; }
	inline DisconnectButton_t3682185937 ** get_address_of_d_3() { return &___d_3; }
	inline void set_d_3(DisconnectButton_t3682185937 * value)
	{
		___d_3 = value;
		Il2CppCodeGenWriteBarrier((&___d_3), value);
	}

	inline static int32_t get_offset_of_b_4() { return static_cast<int32_t>(offsetof(PTPGameController_t1530419951, ___b_4)); }
	inline ConnectionButton_t3407155931 * get_b_4() const { return ___b_4; }
	inline ConnectionButton_t3407155931 ** get_address_of_b_4() { return &___b_4; }
	inline void set_b_4(ConnectionButton_t3407155931 * value)
	{
		___b_4 = value;
		Il2CppCodeGenWriteBarrier((&___b_4), value);
	}

	inline static int32_t get_offset_of_m_5() { return static_cast<int32_t>(offsetof(PTPGameController_t1530419951, ___m_5)); }
	inline ClickManagerExample_t3328835271 * get_m_5() const { return ___m_5; }
	inline ClickManagerExample_t3328835271 ** get_address_of_m_5() { return &___m_5; }
	inline void set_m_5(ClickManagerExample_t3328835271 * value)
	{
		___m_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_5), value);
	}

	inline static int32_t get_offset_of_spheres_7() { return static_cast<int32_t>(offsetof(PTPGameController_t1530419951, ___spheres_7)); }
	inline List_1_t2585711361 * get_spheres_7() const { return ___spheres_7; }
	inline List_1_t2585711361 ** get_address_of_spheres_7() { return &___spheres_7; }
	inline void set_spheres_7(List_1_t2585711361 * value)
	{
		___spheres_7 = value;
		Il2CppCodeGenWriteBarrier((&___spheres_7), value);
	}
};

struct PTPGameController_t1530419951_StaticFields
{
public:
	// PTPGameController PTPGameController::instance
	PTPGameController_t1530419951 * ___instance_6;

public:
	inline static int32_t get_offset_of_instance_6() { return static_cast<int32_t>(offsetof(PTPGameController_t1530419951_StaticFields, ___instance_6)); }
	inline PTPGameController_t1530419951 * get_instance_6() const { return ___instance_6; }
	inline PTPGameController_t1530419951 ** get_address_of_instance_6() { return &___instance_6; }
	inline void set_instance_6(PTPGameController_t1530419951 * value)
	{
		___instance_6 = value;
		Il2CppCodeGenWriteBarrier((&___instance_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PTPGAMECONTROLLER_T1530419951_H
#ifndef CLICKMANAGEREXAMPLE_T3328835271_H
#define CLICKMANAGEREXAMPLE_T3328835271_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClickManagerExample
struct  ClickManagerExample_t3328835271  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLICKMANAGEREXAMPLE_T3328835271_H
#ifndef DISCONNECTBUTTON_T3682185937_H
#define DISCONNECTBUTTON_T3682185937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DisconnectButton
struct  DisconnectButton_t3682185937  : public MonoBehaviour_t3962482529
{
public:
	// System.Single DisconnectButton::w
	float ___w_2;
	// System.Single DisconnectButton::h
	float ___h_3;
	// UnityEngine.Rect DisconnectButton::r
	Rect_t2360479859  ___r_4;

public:
	inline static int32_t get_offset_of_w_2() { return static_cast<int32_t>(offsetof(DisconnectButton_t3682185937, ___w_2)); }
	inline float get_w_2() const { return ___w_2; }
	inline float* get_address_of_w_2() { return &___w_2; }
	inline void set_w_2(float value)
	{
		___w_2 = value;
	}

	inline static int32_t get_offset_of_h_3() { return static_cast<int32_t>(offsetof(DisconnectButton_t3682185937, ___h_3)); }
	inline float get_h_3() const { return ___h_3; }
	inline float* get_address_of_h_3() { return &___h_3; }
	inline void set_h_3(float value)
	{
		___h_3 = value;
	}

	inline static int32_t get_offset_of_r_4() { return static_cast<int32_t>(offsetof(DisconnectButton_t3682185937, ___r_4)); }
	inline Rect_t2360479859  get_r_4() const { return ___r_4; }
	inline Rect_t2360479859 * get_address_of_r_4() { return &___r_4; }
	inline void set_r_4(Rect_t2360479859  value)
	{
		___r_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISCONNECTBUTTON_T3682185937_H
#ifndef CONTACTSTORE_T1420756449_H
#define CONTACTSTORE_T1420756449_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSNative.Contacts.ContactStore
struct  ContactStore_t1420756449  : public Singleton_1_t1050528595
{
public:
	// System.Action`1<SA.IOSNative.Contacts.ContactsResult> SA.IOSNative.Contacts.ContactStore::ContactsLoadResult
	Action_1_t1323995290 * ___ContactsLoadResult_4;
	// System.Action`1<SA.IOSNative.Contacts.ContactsResult> SA.IOSNative.Contacts.ContactStore::ContactsPickResult
	Action_1_t1323995290 * ___ContactsPickResult_5;

public:
	inline static int32_t get_offset_of_ContactsLoadResult_4() { return static_cast<int32_t>(offsetof(ContactStore_t1420756449, ___ContactsLoadResult_4)); }
	inline Action_1_t1323995290 * get_ContactsLoadResult_4() const { return ___ContactsLoadResult_4; }
	inline Action_1_t1323995290 ** get_address_of_ContactsLoadResult_4() { return &___ContactsLoadResult_4; }
	inline void set_ContactsLoadResult_4(Action_1_t1323995290 * value)
	{
		___ContactsLoadResult_4 = value;
		Il2CppCodeGenWriteBarrier((&___ContactsLoadResult_4), value);
	}

	inline static int32_t get_offset_of_ContactsPickResult_5() { return static_cast<int32_t>(offsetof(ContactStore_t1420756449, ___ContactsPickResult_5)); }
	inline Action_1_t1323995290 * get_ContactsPickResult_5() const { return ___ContactsPickResult_5; }
	inline Action_1_t1323995290 ** get_address_of_ContactsPickResult_5() { return &___ContactsPickResult_5; }
	inline void set_ContactsPickResult_5(Action_1_t1323995290 * value)
	{
		___ContactsPickResult_5 = value;
		Il2CppCodeGenWriteBarrier((&___ContactsPickResult_5), value);
	}
};

struct ContactStore_t1420756449_StaticFields
{
public:
	// System.Action`1<SA.IOSNative.Contacts.ContactsResult> SA.IOSNative.Contacts.ContactStore::<>f__am$cache0
	Action_1_t1323995290 * ___U3CU3Ef__amU24cache0_6;
	// System.Action`1<SA.IOSNative.Contacts.ContactsResult> SA.IOSNative.Contacts.ContactStore::<>f__am$cache1
	Action_1_t1323995290 * ___U3CU3Ef__amU24cache1_7;
	// System.Action`1<SA.IOSNative.Contacts.ContactsResult> SA.IOSNative.Contacts.ContactStore::<>f__am$cache2
	Action_1_t1323995290 * ___U3CU3Ef__amU24cache2_8;
	// System.Action`1<SA.IOSNative.Contacts.ContactsResult> SA.IOSNative.Contacts.ContactStore::<>f__am$cache3
	Action_1_t1323995290 * ___U3CU3Ef__amU24cache3_9;
	// System.Action`1<SA.IOSNative.Contacts.ContactsResult> SA.IOSNative.Contacts.ContactStore::<>f__am$cache4
	Action_1_t1323995290 * ___U3CU3Ef__amU24cache4_10;
	// System.Action`1<SA.IOSNative.Contacts.ContactsResult> SA.IOSNative.Contacts.ContactStore::<>f__am$cache5
	Action_1_t1323995290 * ___U3CU3Ef__amU24cache5_11;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_6() { return static_cast<int32_t>(offsetof(ContactStore_t1420756449_StaticFields, ___U3CU3Ef__amU24cache0_6)); }
	inline Action_1_t1323995290 * get_U3CU3Ef__amU24cache0_6() const { return ___U3CU3Ef__amU24cache0_6; }
	inline Action_1_t1323995290 ** get_address_of_U3CU3Ef__amU24cache0_6() { return &___U3CU3Ef__amU24cache0_6; }
	inline void set_U3CU3Ef__amU24cache0_6(Action_1_t1323995290 * value)
	{
		___U3CU3Ef__amU24cache0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_7() { return static_cast<int32_t>(offsetof(ContactStore_t1420756449_StaticFields, ___U3CU3Ef__amU24cache1_7)); }
	inline Action_1_t1323995290 * get_U3CU3Ef__amU24cache1_7() const { return ___U3CU3Ef__amU24cache1_7; }
	inline Action_1_t1323995290 ** get_address_of_U3CU3Ef__amU24cache1_7() { return &___U3CU3Ef__amU24cache1_7; }
	inline void set_U3CU3Ef__amU24cache1_7(Action_1_t1323995290 * value)
	{
		___U3CU3Ef__amU24cache1_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_8() { return static_cast<int32_t>(offsetof(ContactStore_t1420756449_StaticFields, ___U3CU3Ef__amU24cache2_8)); }
	inline Action_1_t1323995290 * get_U3CU3Ef__amU24cache2_8() const { return ___U3CU3Ef__amU24cache2_8; }
	inline Action_1_t1323995290 ** get_address_of_U3CU3Ef__amU24cache2_8() { return &___U3CU3Ef__amU24cache2_8; }
	inline void set_U3CU3Ef__amU24cache2_8(Action_1_t1323995290 * value)
	{
		___U3CU3Ef__amU24cache2_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_8), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_9() { return static_cast<int32_t>(offsetof(ContactStore_t1420756449_StaticFields, ___U3CU3Ef__amU24cache3_9)); }
	inline Action_1_t1323995290 * get_U3CU3Ef__amU24cache3_9() const { return ___U3CU3Ef__amU24cache3_9; }
	inline Action_1_t1323995290 ** get_address_of_U3CU3Ef__amU24cache3_9() { return &___U3CU3Ef__amU24cache3_9; }
	inline void set_U3CU3Ef__amU24cache3_9(Action_1_t1323995290 * value)
	{
		___U3CU3Ef__amU24cache3_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_9), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_10() { return static_cast<int32_t>(offsetof(ContactStore_t1420756449_StaticFields, ___U3CU3Ef__amU24cache4_10)); }
	inline Action_1_t1323995290 * get_U3CU3Ef__amU24cache4_10() const { return ___U3CU3Ef__amU24cache4_10; }
	inline Action_1_t1323995290 ** get_address_of_U3CU3Ef__amU24cache4_10() { return &___U3CU3Ef__amU24cache4_10; }
	inline void set_U3CU3Ef__amU24cache4_10(Action_1_t1323995290 * value)
	{
		___U3CU3Ef__amU24cache4_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache4_10), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_11() { return static_cast<int32_t>(offsetof(ContactStore_t1420756449_StaticFields, ___U3CU3Ef__amU24cache5_11)); }
	inline Action_1_t1323995290 * get_U3CU3Ef__amU24cache5_11() const { return ___U3CU3Ef__amU24cache5_11; }
	inline Action_1_t1323995290 ** get_address_of_U3CU3Ef__amU24cache5_11() { return &___U3CU3Ef__amU24cache5_11; }
	inline void set_U3CU3Ef__amU24cache5_11(Action_1_t1323995290 * value)
	{
		___U3CU3Ef__amU24cache5_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache5_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTACTSTORE_T1420756449_H
#ifndef NATIVEIOSACTIONSEXAMPLE_T2698905075_H
#define NATIVEIOSACTIONSEXAMPLE_T2698905075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NativeIOSActionsExample
struct  NativeIOSActionsExample_t2698905075  : public BaseIOSFeaturePreview_t3740726741
{
public:
	// UnityEngine.Texture2D NativeIOSActionsExample::hello_texture
	Texture2D_t3840446185 * ___hello_texture_12;
	// UnityEngine.Texture2D NativeIOSActionsExample::drawTexture
	Texture2D_t3840446185 * ___drawTexture_13;
	// System.DateTime NativeIOSActionsExample::time
	DateTime_t3738529785  ___time_14;

public:
	inline static int32_t get_offset_of_hello_texture_12() { return static_cast<int32_t>(offsetof(NativeIOSActionsExample_t2698905075, ___hello_texture_12)); }
	inline Texture2D_t3840446185 * get_hello_texture_12() const { return ___hello_texture_12; }
	inline Texture2D_t3840446185 ** get_address_of_hello_texture_12() { return &___hello_texture_12; }
	inline void set_hello_texture_12(Texture2D_t3840446185 * value)
	{
		___hello_texture_12 = value;
		Il2CppCodeGenWriteBarrier((&___hello_texture_12), value);
	}

	inline static int32_t get_offset_of_drawTexture_13() { return static_cast<int32_t>(offsetof(NativeIOSActionsExample_t2698905075, ___drawTexture_13)); }
	inline Texture2D_t3840446185 * get_drawTexture_13() const { return ___drawTexture_13; }
	inline Texture2D_t3840446185 ** get_address_of_drawTexture_13() { return &___drawTexture_13; }
	inline void set_drawTexture_13(Texture2D_t3840446185 * value)
	{
		___drawTexture_13 = value;
		Il2CppCodeGenWriteBarrier((&___drawTexture_13), value);
	}

	inline static int32_t get_offset_of_time_14() { return static_cast<int32_t>(offsetof(NativeIOSActionsExample_t2698905075, ___time_14)); }
	inline DateTime_t3738529785  get_time_14() const { return ___time_14; }
	inline DateTime_t3738529785 * get_address_of_time_14() { return &___time_14; }
	inline void set_time_14(DateTime_t3738529785  value)
	{
		___time_14 = value;
	}
};

struct NativeIOSActionsExample_t2698905075_StaticFields
{
public:
	// System.Action`1<ISN_SwipeDirection> NativeIOSActionsExample::<>f__am$cache0
	Action_1_t3227987809 * ___U3CU3Ef__amU24cache0_15;
	// System.Action NativeIOSActionsExample::<>f__am$cache1
	Action_t1264377477 * ___U3CU3Ef__amU24cache1_16;
	// System.Action`1<SA.IOSNative.Gestures.ForceInfo> NativeIOSActionsExample::<>f__am$cache2
	Action_1_t4106855485 * ___U3CU3Ef__amU24cache2_17;
	// System.Action NativeIOSActionsExample::<>f__am$cache3
	Action_t1264377477 * ___U3CU3Ef__amU24cache3_18;
	// System.Action`1<System.String> NativeIOSActionsExample::<>f__am$cache4
	Action_1_t2019918284 * ___U3CU3Ef__amU24cache4_19;
	// System.Action`1<SA.IOSNative.Contacts.ContactsResult> NativeIOSActionsExample::<>f__am$cache5
	Action_1_t1323995290 * ___U3CU3Ef__amU24cache5_20;
	// System.Action`1<SA.IOSNative.Contacts.ContactsResult> NativeIOSActionsExample::<>f__am$cache6
	Action_1_t1323995290 * ___U3CU3Ef__amU24cache6_21;
	// System.Action`1<System.DateTime> NativeIOSActionsExample::<>f__am$cache7
	Action_1_t3910997380 * ___U3CU3Ef__amU24cache7_22;
	// System.Action`1<System.DateTime> NativeIOSActionsExample::<>f__am$cache8
	Action_1_t3910997380 * ___U3CU3Ef__amU24cache8_23;
	// System.Action`1<System.DateTime> NativeIOSActionsExample::<>f__am$cache9
	Action_1_t3910997380 * ___U3CU3Ef__amU24cache9_24;
	// System.Action`1<System.DateTime> NativeIOSActionsExample::<>f__am$cacheA
	Action_1_t3910997380 * ___U3CU3Ef__amU24cacheA_25;
	// System.Action`1<System.DateTime> NativeIOSActionsExample::<>f__am$cacheB
	Action_1_t3910997380 * ___U3CU3Ef__amU24cacheB_26;
	// System.Action`1<System.DateTime> NativeIOSActionsExample::<>f__am$cacheC
	Action_1_t3910997380 * ___U3CU3Ef__amU24cacheC_27;
	// System.Action`1<SA.IOSNative.Privacy.PermissionStatus> NativeIOSActionsExample::<>f__am$cacheD
	Action_1_t1113631738 * ___U3CU3Ef__amU24cacheD_28;
	// System.Action`1<SA.IOSNative.Privacy.PermissionStatus> NativeIOSActionsExample::<>f__am$cacheE
	Action_1_t1113631738 * ___U3CU3Ef__amU24cacheE_29;
	// System.Action`1<SA.IOSNative.Privacy.PermissionStatus> NativeIOSActionsExample::<>f__am$cacheF
	Action_1_t1113631738 * ___U3CU3Ef__amU24cacheF_30;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_15() { return static_cast<int32_t>(offsetof(NativeIOSActionsExample_t2698905075_StaticFields, ___U3CU3Ef__amU24cache0_15)); }
	inline Action_1_t3227987809 * get_U3CU3Ef__amU24cache0_15() const { return ___U3CU3Ef__amU24cache0_15; }
	inline Action_1_t3227987809 ** get_address_of_U3CU3Ef__amU24cache0_15() { return &___U3CU3Ef__amU24cache0_15; }
	inline void set_U3CU3Ef__amU24cache0_15(Action_1_t3227987809 * value)
	{
		___U3CU3Ef__amU24cache0_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_15), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_16() { return static_cast<int32_t>(offsetof(NativeIOSActionsExample_t2698905075_StaticFields, ___U3CU3Ef__amU24cache1_16)); }
	inline Action_t1264377477 * get_U3CU3Ef__amU24cache1_16() const { return ___U3CU3Ef__amU24cache1_16; }
	inline Action_t1264377477 ** get_address_of_U3CU3Ef__amU24cache1_16() { return &___U3CU3Ef__amU24cache1_16; }
	inline void set_U3CU3Ef__amU24cache1_16(Action_t1264377477 * value)
	{
		___U3CU3Ef__amU24cache1_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_16), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_17() { return static_cast<int32_t>(offsetof(NativeIOSActionsExample_t2698905075_StaticFields, ___U3CU3Ef__amU24cache2_17)); }
	inline Action_1_t4106855485 * get_U3CU3Ef__amU24cache2_17() const { return ___U3CU3Ef__amU24cache2_17; }
	inline Action_1_t4106855485 ** get_address_of_U3CU3Ef__amU24cache2_17() { return &___U3CU3Ef__amU24cache2_17; }
	inline void set_U3CU3Ef__amU24cache2_17(Action_1_t4106855485 * value)
	{
		___U3CU3Ef__amU24cache2_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_17), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_18() { return static_cast<int32_t>(offsetof(NativeIOSActionsExample_t2698905075_StaticFields, ___U3CU3Ef__amU24cache3_18)); }
	inline Action_t1264377477 * get_U3CU3Ef__amU24cache3_18() const { return ___U3CU3Ef__amU24cache3_18; }
	inline Action_t1264377477 ** get_address_of_U3CU3Ef__amU24cache3_18() { return &___U3CU3Ef__amU24cache3_18; }
	inline void set_U3CU3Ef__amU24cache3_18(Action_t1264377477 * value)
	{
		___U3CU3Ef__amU24cache3_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_18), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_19() { return static_cast<int32_t>(offsetof(NativeIOSActionsExample_t2698905075_StaticFields, ___U3CU3Ef__amU24cache4_19)); }
	inline Action_1_t2019918284 * get_U3CU3Ef__amU24cache4_19() const { return ___U3CU3Ef__amU24cache4_19; }
	inline Action_1_t2019918284 ** get_address_of_U3CU3Ef__amU24cache4_19() { return &___U3CU3Ef__amU24cache4_19; }
	inline void set_U3CU3Ef__amU24cache4_19(Action_1_t2019918284 * value)
	{
		___U3CU3Ef__amU24cache4_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache4_19), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_20() { return static_cast<int32_t>(offsetof(NativeIOSActionsExample_t2698905075_StaticFields, ___U3CU3Ef__amU24cache5_20)); }
	inline Action_1_t1323995290 * get_U3CU3Ef__amU24cache5_20() const { return ___U3CU3Ef__amU24cache5_20; }
	inline Action_1_t1323995290 ** get_address_of_U3CU3Ef__amU24cache5_20() { return &___U3CU3Ef__amU24cache5_20; }
	inline void set_U3CU3Ef__amU24cache5_20(Action_1_t1323995290 * value)
	{
		___U3CU3Ef__amU24cache5_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache5_20), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_21() { return static_cast<int32_t>(offsetof(NativeIOSActionsExample_t2698905075_StaticFields, ___U3CU3Ef__amU24cache6_21)); }
	inline Action_1_t1323995290 * get_U3CU3Ef__amU24cache6_21() const { return ___U3CU3Ef__amU24cache6_21; }
	inline Action_1_t1323995290 ** get_address_of_U3CU3Ef__amU24cache6_21() { return &___U3CU3Ef__amU24cache6_21; }
	inline void set_U3CU3Ef__amU24cache6_21(Action_1_t1323995290 * value)
	{
		___U3CU3Ef__amU24cache6_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache6_21), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_22() { return static_cast<int32_t>(offsetof(NativeIOSActionsExample_t2698905075_StaticFields, ___U3CU3Ef__amU24cache7_22)); }
	inline Action_1_t3910997380 * get_U3CU3Ef__amU24cache7_22() const { return ___U3CU3Ef__amU24cache7_22; }
	inline Action_1_t3910997380 ** get_address_of_U3CU3Ef__amU24cache7_22() { return &___U3CU3Ef__amU24cache7_22; }
	inline void set_U3CU3Ef__amU24cache7_22(Action_1_t3910997380 * value)
	{
		___U3CU3Ef__amU24cache7_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache7_22), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache8_23() { return static_cast<int32_t>(offsetof(NativeIOSActionsExample_t2698905075_StaticFields, ___U3CU3Ef__amU24cache8_23)); }
	inline Action_1_t3910997380 * get_U3CU3Ef__amU24cache8_23() const { return ___U3CU3Ef__amU24cache8_23; }
	inline Action_1_t3910997380 ** get_address_of_U3CU3Ef__amU24cache8_23() { return &___U3CU3Ef__amU24cache8_23; }
	inline void set_U3CU3Ef__amU24cache8_23(Action_1_t3910997380 * value)
	{
		___U3CU3Ef__amU24cache8_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache8_23), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache9_24() { return static_cast<int32_t>(offsetof(NativeIOSActionsExample_t2698905075_StaticFields, ___U3CU3Ef__amU24cache9_24)); }
	inline Action_1_t3910997380 * get_U3CU3Ef__amU24cache9_24() const { return ___U3CU3Ef__amU24cache9_24; }
	inline Action_1_t3910997380 ** get_address_of_U3CU3Ef__amU24cache9_24() { return &___U3CU3Ef__amU24cache9_24; }
	inline void set_U3CU3Ef__amU24cache9_24(Action_1_t3910997380 * value)
	{
		___U3CU3Ef__amU24cache9_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache9_24), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheA_25() { return static_cast<int32_t>(offsetof(NativeIOSActionsExample_t2698905075_StaticFields, ___U3CU3Ef__amU24cacheA_25)); }
	inline Action_1_t3910997380 * get_U3CU3Ef__amU24cacheA_25() const { return ___U3CU3Ef__amU24cacheA_25; }
	inline Action_1_t3910997380 ** get_address_of_U3CU3Ef__amU24cacheA_25() { return &___U3CU3Ef__amU24cacheA_25; }
	inline void set_U3CU3Ef__amU24cacheA_25(Action_1_t3910997380 * value)
	{
		___U3CU3Ef__amU24cacheA_25 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheA_25), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_26() { return static_cast<int32_t>(offsetof(NativeIOSActionsExample_t2698905075_StaticFields, ___U3CU3Ef__amU24cacheB_26)); }
	inline Action_1_t3910997380 * get_U3CU3Ef__amU24cacheB_26() const { return ___U3CU3Ef__amU24cacheB_26; }
	inline Action_1_t3910997380 ** get_address_of_U3CU3Ef__amU24cacheB_26() { return &___U3CU3Ef__amU24cacheB_26; }
	inline void set_U3CU3Ef__amU24cacheB_26(Action_1_t3910997380 * value)
	{
		___U3CU3Ef__amU24cacheB_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_26), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheC_27() { return static_cast<int32_t>(offsetof(NativeIOSActionsExample_t2698905075_StaticFields, ___U3CU3Ef__amU24cacheC_27)); }
	inline Action_1_t3910997380 * get_U3CU3Ef__amU24cacheC_27() const { return ___U3CU3Ef__amU24cacheC_27; }
	inline Action_1_t3910997380 ** get_address_of_U3CU3Ef__amU24cacheC_27() { return &___U3CU3Ef__amU24cacheC_27; }
	inline void set_U3CU3Ef__amU24cacheC_27(Action_1_t3910997380 * value)
	{
		___U3CU3Ef__amU24cacheC_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheC_27), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheD_28() { return static_cast<int32_t>(offsetof(NativeIOSActionsExample_t2698905075_StaticFields, ___U3CU3Ef__amU24cacheD_28)); }
	inline Action_1_t1113631738 * get_U3CU3Ef__amU24cacheD_28() const { return ___U3CU3Ef__amU24cacheD_28; }
	inline Action_1_t1113631738 ** get_address_of_U3CU3Ef__amU24cacheD_28() { return &___U3CU3Ef__amU24cacheD_28; }
	inline void set_U3CU3Ef__amU24cacheD_28(Action_1_t1113631738 * value)
	{
		___U3CU3Ef__amU24cacheD_28 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheD_28), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheE_29() { return static_cast<int32_t>(offsetof(NativeIOSActionsExample_t2698905075_StaticFields, ___U3CU3Ef__amU24cacheE_29)); }
	inline Action_1_t1113631738 * get_U3CU3Ef__amU24cacheE_29() const { return ___U3CU3Ef__amU24cacheE_29; }
	inline Action_1_t1113631738 ** get_address_of_U3CU3Ef__amU24cacheE_29() { return &___U3CU3Ef__amU24cacheE_29; }
	inline void set_U3CU3Ef__amU24cacheE_29(Action_1_t1113631738 * value)
	{
		___U3CU3Ef__amU24cacheE_29 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheE_29), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheF_30() { return static_cast<int32_t>(offsetof(NativeIOSActionsExample_t2698905075_StaticFields, ___U3CU3Ef__amU24cacheF_30)); }
	inline Action_1_t1113631738 * get_U3CU3Ef__amU24cacheF_30() const { return ___U3CU3Ef__amU24cacheF_30; }
	inline Action_1_t1113631738 ** get_address_of_U3CU3Ef__amU24cacheF_30() { return &___U3CU3Ef__amU24cacheF_30; }
	inline void set_U3CU3Ef__amU24cacheF_30(Action_1_t1113631738 * value)
	{
		___U3CU3Ef__amU24cacheF_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheF_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVEIOSACTIONSEXAMPLE_T2698905075_H
#ifndef NATIVERECEIVER_T107704069_H
#define NATIVERECEIVER_T107704069_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSNative.Privacy.NativeReceiver
struct  NativeReceiver_t107704069  : public Singleton_1_t4032443511
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVERECEIVER_T107704069_H
#ifndef ISN_LOGGER_T4052948437_H
#define ISN_LOGGER_T4052948437_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ISN_Logger
struct  ISN_Logger_t4052948437  : public Singleton_1_t3682720583
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISN_LOGGER_T4052948437_H
#ifndef FORCETOUCH_T2199614965_H
#define FORCETOUCH_T2199614965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSNative.Gestures.ForceTouch
struct  ForceTouch_t2199614965  : public Singleton_1_t1829387111
{
public:
	// System.Action SA.IOSNative.Gestures.ForceTouch::OnForceTouchStarted
	Action_t1264377477 * ___OnForceTouchStarted_4;
	// System.Action SA.IOSNative.Gestures.ForceTouch::OnForceTouchFinished
	Action_t1264377477 * ___OnForceTouchFinished_5;
	// System.Action`1<SA.IOSNative.Gestures.ForceInfo> SA.IOSNative.Gestures.ForceTouch::OnForceChanged
	Action_1_t4106855485 * ___OnForceChanged_6;
	// System.Action`1<System.String> SA.IOSNative.Gestures.ForceTouch::OnAppShortcutClick
	Action_1_t2019918284 * ___OnAppShortcutClick_7;

public:
	inline static int32_t get_offset_of_OnForceTouchStarted_4() { return static_cast<int32_t>(offsetof(ForceTouch_t2199614965, ___OnForceTouchStarted_4)); }
	inline Action_t1264377477 * get_OnForceTouchStarted_4() const { return ___OnForceTouchStarted_4; }
	inline Action_t1264377477 ** get_address_of_OnForceTouchStarted_4() { return &___OnForceTouchStarted_4; }
	inline void set_OnForceTouchStarted_4(Action_t1264377477 * value)
	{
		___OnForceTouchStarted_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnForceTouchStarted_4), value);
	}

	inline static int32_t get_offset_of_OnForceTouchFinished_5() { return static_cast<int32_t>(offsetof(ForceTouch_t2199614965, ___OnForceTouchFinished_5)); }
	inline Action_t1264377477 * get_OnForceTouchFinished_5() const { return ___OnForceTouchFinished_5; }
	inline Action_t1264377477 ** get_address_of_OnForceTouchFinished_5() { return &___OnForceTouchFinished_5; }
	inline void set_OnForceTouchFinished_5(Action_t1264377477 * value)
	{
		___OnForceTouchFinished_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnForceTouchFinished_5), value);
	}

	inline static int32_t get_offset_of_OnForceChanged_6() { return static_cast<int32_t>(offsetof(ForceTouch_t2199614965, ___OnForceChanged_6)); }
	inline Action_1_t4106855485 * get_OnForceChanged_6() const { return ___OnForceChanged_6; }
	inline Action_1_t4106855485 ** get_address_of_OnForceChanged_6() { return &___OnForceChanged_6; }
	inline void set_OnForceChanged_6(Action_1_t4106855485 * value)
	{
		___OnForceChanged_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnForceChanged_6), value);
	}

	inline static int32_t get_offset_of_OnAppShortcutClick_7() { return static_cast<int32_t>(offsetof(ForceTouch_t2199614965, ___OnAppShortcutClick_7)); }
	inline Action_1_t2019918284 * get_OnAppShortcutClick_7() const { return ___OnAppShortcutClick_7; }
	inline Action_1_t2019918284 ** get_address_of_OnAppShortcutClick_7() { return &___OnAppShortcutClick_7; }
	inline void set_OnAppShortcutClick_7(Action_1_t2019918284 * value)
	{
		___OnAppShortcutClick_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnAppShortcutClick_7), value);
	}
};

struct ForceTouch_t2199614965_StaticFields
{
public:
	// System.Boolean SA.IOSNative.Gestures.ForceTouch::_IsTouchTrigerred
	bool ____IsTouchTrigerred_8;
	// System.Action SA.IOSNative.Gestures.ForceTouch::<>f__am$cache0
	Action_t1264377477 * ___U3CU3Ef__amU24cache0_9;
	// System.Action SA.IOSNative.Gestures.ForceTouch::<>f__am$cache1
	Action_t1264377477 * ___U3CU3Ef__amU24cache1_10;
	// System.Action`1<SA.IOSNative.Gestures.ForceInfo> SA.IOSNative.Gestures.ForceTouch::<>f__am$cache2
	Action_1_t4106855485 * ___U3CU3Ef__amU24cache2_11;
	// System.Action`1<System.String> SA.IOSNative.Gestures.ForceTouch::<>f__am$cache3
	Action_1_t2019918284 * ___U3CU3Ef__amU24cache3_12;

public:
	inline static int32_t get_offset_of__IsTouchTrigerred_8() { return static_cast<int32_t>(offsetof(ForceTouch_t2199614965_StaticFields, ____IsTouchTrigerred_8)); }
	inline bool get__IsTouchTrigerred_8() const { return ____IsTouchTrigerred_8; }
	inline bool* get_address_of__IsTouchTrigerred_8() { return &____IsTouchTrigerred_8; }
	inline void set__IsTouchTrigerred_8(bool value)
	{
		____IsTouchTrigerred_8 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_9() { return static_cast<int32_t>(offsetof(ForceTouch_t2199614965_StaticFields, ___U3CU3Ef__amU24cache0_9)); }
	inline Action_t1264377477 * get_U3CU3Ef__amU24cache0_9() const { return ___U3CU3Ef__amU24cache0_9; }
	inline Action_t1264377477 ** get_address_of_U3CU3Ef__amU24cache0_9() { return &___U3CU3Ef__amU24cache0_9; }
	inline void set_U3CU3Ef__amU24cache0_9(Action_t1264377477 * value)
	{
		___U3CU3Ef__amU24cache0_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_9), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_10() { return static_cast<int32_t>(offsetof(ForceTouch_t2199614965_StaticFields, ___U3CU3Ef__amU24cache1_10)); }
	inline Action_t1264377477 * get_U3CU3Ef__amU24cache1_10() const { return ___U3CU3Ef__amU24cache1_10; }
	inline Action_t1264377477 ** get_address_of_U3CU3Ef__amU24cache1_10() { return &___U3CU3Ef__amU24cache1_10; }
	inline void set_U3CU3Ef__amU24cache1_10(Action_t1264377477 * value)
	{
		___U3CU3Ef__amU24cache1_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_10), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_11() { return static_cast<int32_t>(offsetof(ForceTouch_t2199614965_StaticFields, ___U3CU3Ef__amU24cache2_11)); }
	inline Action_1_t4106855485 * get_U3CU3Ef__amU24cache2_11() const { return ___U3CU3Ef__amU24cache2_11; }
	inline Action_1_t4106855485 ** get_address_of_U3CU3Ef__amU24cache2_11() { return &___U3CU3Ef__amU24cache2_11; }
	inline void set_U3CU3Ef__amU24cache2_11(Action_1_t4106855485 * value)
	{
		___U3CU3Ef__amU24cache2_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_11), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_12() { return static_cast<int32_t>(offsetof(ForceTouch_t2199614965_StaticFields, ___U3CU3Ef__amU24cache3_12)); }
	inline Action_1_t2019918284 * get_U3CU3Ef__amU24cache3_12() const { return ___U3CU3Ef__amU24cache3_12; }
	inline Action_1_t2019918284 ** get_address_of_U3CU3Ef__amU24cache3_12() { return &___U3CU3Ef__amU24cache3_12; }
	inline void set_U3CU3Ef__amU24cache3_12(Action_1_t2019918284 * value)
	{
		___U3CU3Ef__amU24cache3_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORCETOUCH_T2199614965_H
#ifndef ISN_GESTURERECOGNIZER_T3385868019_H
#define ISN_GESTURERECOGNIZER_T3385868019_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ISN_GestureRecognizer
struct  ISN_GestureRecognizer_t3385868019  : public Singleton_1_t3015640165
{
public:
	// System.Action`1<ISN_SwipeDirection> ISN_GestureRecognizer::OnSwipe
	Action_1_t3227987809 * ___OnSwipe_4;

public:
	inline static int32_t get_offset_of_OnSwipe_4() { return static_cast<int32_t>(offsetof(ISN_GestureRecognizer_t3385868019, ___OnSwipe_4)); }
	inline Action_1_t3227987809 * get_OnSwipe_4() const { return ___OnSwipe_4; }
	inline Action_1_t3227987809 ** get_address_of_OnSwipe_4() { return &___OnSwipe_4; }
	inline void set_OnSwipe_4(Action_1_t3227987809 * value)
	{
		___OnSwipe_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnSwipe_4), value);
	}
};

struct ISN_GestureRecognizer_t3385868019_StaticFields
{
public:
	// System.Action`1<ISN_SwipeDirection> ISN_GestureRecognizer::<>f__am$cache0
	Action_1_t3227987809 * ___U3CU3Ef__amU24cache0_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_5() { return static_cast<int32_t>(offsetof(ISN_GestureRecognizer_t3385868019_StaticFields, ___U3CU3Ef__amU24cache0_5)); }
	inline Action_1_t3227987809 * get_U3CU3Ef__amU24cache0_5() const { return ___U3CU3Ef__amU24cache0_5; }
	inline Action_1_t3227987809 ** get_address_of_U3CU3Ef__amU24cache0_5() { return &___U3CU3Ef__amU24cache0_5; }
	inline void set_U3CU3Ef__amU24cache0_5(Action_1_t3227987809 * value)
	{
		___U3CU3Ef__amU24cache0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISN_GESTURERECOGNIZER_T3385868019_H
#ifndef ISNMEDIAEXAMPLE_T1939259784_H
#define ISNMEDIAEXAMPLE_T1939259784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ISNMediaExample
struct  ISNMediaExample_t1939259784  : public BaseIOSFeaturePreview_t3740726741
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISNMEDIAEXAMPLE_T1939259784_H
#ifndef ICLOUDUSEEXAMPLE_T3965519368_H
#define ICLOUDUSEEXAMPLE_T3965519368_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iCloudUseExample
struct  iCloudUseExample_t3965519368  : public BaseIOSFeaturePreview_t3740726741
{
public:
	// System.Single iCloudUseExample::v
	float ___v_12;

public:
	inline static int32_t get_offset_of_v_12() { return static_cast<int32_t>(offsetof(iCloudUseExample_t3965519368, ___v_12)); }
	inline float get_v_12() const { return ___v_12; }
	inline float* get_address_of_v_12() { return &___v_12; }
	inline void set_v_12(float value)
	{
		___v_12 = value;
	}
};

struct iCloudUseExample_t3965519368_StaticFields
{
public:
	// System.Action`1<iCloudData> iCloudUseExample::<>f__am$cache0
	Action_1_t267273497 * ___U3CU3Ef__amU24cache0_13;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_13() { return static_cast<int32_t>(offsetof(iCloudUseExample_t3965519368_StaticFields, ___U3CU3Ef__amU24cache0_13)); }
	inline Action_1_t267273497 * get_U3CU3Ef__amU24cache0_13() const { return ___U3CU3Ef__amU24cache0_13; }
	inline Action_1_t267273497 ** get_address_of_U3CU3Ef__amU24cache0_13() { return &___U3CU3Ef__amU24cache0_13; }
	inline void set_U3CU3Ef__amU24cache0_13(Action_1_t267273497 * value)
	{
		___U3CU3Ef__amU24cache0_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ICLOUDUSEEXAMPLE_T3965519368_H
#ifndef GAMESAVESEXAMPLE_T2436371863_H
#define GAMESAVESEXAMPLE_T2436371863_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSavesExample
struct  GameSavesExample_t2436371863  : public BaseIOSFeaturePreview_t3740726741
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>> GameSavesExample::GameSaves
	Dictionary_2_t407435505 * ___GameSaves_12;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>> GameSavesExample::SavesConflicts
	Dictionary_2_t407435505 * ___SavesConflicts_13;
	// System.String GameSavesExample::test_name
	String_t* ___test_name_14;
	// System.String GameSavesExample::test_name_2
	String_t* ___test_name_2_15;

public:
	inline static int32_t get_offset_of_GameSaves_12() { return static_cast<int32_t>(offsetof(GameSavesExample_t2436371863, ___GameSaves_12)); }
	inline Dictionary_2_t407435505 * get_GameSaves_12() const { return ___GameSaves_12; }
	inline Dictionary_2_t407435505 ** get_address_of_GameSaves_12() { return &___GameSaves_12; }
	inline void set_GameSaves_12(Dictionary_2_t407435505 * value)
	{
		___GameSaves_12 = value;
		Il2CppCodeGenWriteBarrier((&___GameSaves_12), value);
	}

	inline static int32_t get_offset_of_SavesConflicts_13() { return static_cast<int32_t>(offsetof(GameSavesExample_t2436371863, ___SavesConflicts_13)); }
	inline Dictionary_2_t407435505 * get_SavesConflicts_13() const { return ___SavesConflicts_13; }
	inline Dictionary_2_t407435505 ** get_address_of_SavesConflicts_13() { return &___SavesConflicts_13; }
	inline void set_SavesConflicts_13(Dictionary_2_t407435505 * value)
	{
		___SavesConflicts_13 = value;
		Il2CppCodeGenWriteBarrier((&___SavesConflicts_13), value);
	}

	inline static int32_t get_offset_of_test_name_14() { return static_cast<int32_t>(offsetof(GameSavesExample_t2436371863, ___test_name_14)); }
	inline String_t* get_test_name_14() const { return ___test_name_14; }
	inline String_t** get_address_of_test_name_14() { return &___test_name_14; }
	inline void set_test_name_14(String_t* value)
	{
		___test_name_14 = value;
		Il2CppCodeGenWriteBarrier((&___test_name_14), value);
	}

	inline static int32_t get_offset_of_test_name_2_15() { return static_cast<int32_t>(offsetof(GameSavesExample_t2436371863, ___test_name_2_15)); }
	inline String_t* get_test_name_2_15() const { return ___test_name_2_15; }
	inline String_t** get_address_of_test_name_2_15() { return &___test_name_2_15; }
	inline void set_test_name_2_15(String_t* value)
	{
		___test_name_2_15 = value;
		Il2CppCodeGenWriteBarrier((&___test_name_2_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMESAVESEXAMPLE_T2436371863_H
#ifndef IOSNATIVEPREVIEWBACKBUTTON_T1249685067_H
#define IOSNATIVEPREVIEWBACKBUTTON_T1249685067_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IOSNativePreviewBackButton
struct  IOSNativePreviewBackButton_t1249685067  : public BaseIOSFeaturePreview_t3740726741
{
public:
	// System.String IOSNativePreviewBackButton::initialSceneName
	String_t* ___initialSceneName_12;

public:
	inline static int32_t get_offset_of_initialSceneName_12() { return static_cast<int32_t>(offsetof(IOSNativePreviewBackButton_t1249685067, ___initialSceneName_12)); }
	inline String_t* get_initialSceneName_12() const { return ___initialSceneName_12; }
	inline String_t** get_address_of_initialSceneName_12() { return &___initialSceneName_12; }
	inline void set_initialSceneName_12(String_t* value)
	{
		___initialSceneName_12 = value;
		Il2CppCodeGenWriteBarrier((&___initialSceneName_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSNATIVEPREVIEWBACKBUTTON_T1249685067_H
#ifndef POPUPEXAMPLES_T1488449073_H
#define POPUPEXAMPLES_T1488449073_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PopUpExamples
struct  PopUpExamples_t1488449073  : public BaseIOSFeaturePreview_t3740726741
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POPUPEXAMPLES_T1488449073_H
#ifndef REPLAYKITUSEEXAMPLE_T459666869_H
#define REPLAYKITUSEEXAMPLE_T459666869_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReplayKitUseExample
struct  ReplayKitUseExample_t459666869  : public BaseIOSFeaturePreview_t3740726741
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REPLAYKITUSEEXAMPLE_T459666869_H
#ifndef IOSNATIVEFEATURESPREVIEW_T2085497528_H
#define IOSNATIVEFEATURESPREVIEW_T2085497528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IOSNativeFeaturesPreview
struct  IOSNativeFeaturesPreview_t2085497528  : public BaseIOSFeaturePreview_t3740726741
{
public:
	// System.Single IOSNativeFeaturesPreview::x
	float ___x_12;

public:
	inline static int32_t get_offset_of_x_12() { return static_cast<int32_t>(offsetof(IOSNativeFeaturesPreview_t2085497528, ___x_12)); }
	inline float get_x_12() const { return ___x_12; }
	inline float* get_address_of_x_12() { return &___x_12; }
	inline void set_x_12(float value)
	{
		___x_12 = value;
	}
};

struct IOSNativeFeaturesPreview_t2085497528_StaticFields
{
public:
	// IOSNativePreviewBackButton IOSNativeFeaturesPreview::back
	IOSNativePreviewBackButton_t1249685067 * ___back_13;

public:
	inline static int32_t get_offset_of_back_13() { return static_cast<int32_t>(offsetof(IOSNativeFeaturesPreview_t2085497528_StaticFields, ___back_13)); }
	inline IOSNativePreviewBackButton_t1249685067 * get_back_13() const { return ___back_13; }
	inline IOSNativePreviewBackButton_t1249685067 ** get_address_of_back_13() { return &___back_13; }
	inline void set_back_13(IOSNativePreviewBackButton_t1249685067 * value)
	{
		___back_13 = value;
		Il2CppCodeGenWriteBarrier((&___back_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSNATIVEFEATURESPREVIEW_T2085497528_H
#ifndef CLOUDKITUSEEXAMPLE_T417999627_H
#define CLOUDKITUSEEXAMPLE_T417999627_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CloudKitUseExample
struct  CloudKitUseExample_t417999627  : public BaseIOSFeaturePreview_t3740726741
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOUDKITUSEEXAMPLE_T417999627_H
#ifndef NATIVERECEIVER_T3281330226_H
#define NATIVERECEIVER_T3281330226_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSNative.UIKit.NativeReceiver
struct  NativeReceiver_t3281330226  : public Singleton_1_t2911102372
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVERECEIVER_T3281330226_H
#ifndef IOSNATIVEUTILITY_T3735947664_H
#define IOSNATIVEUTILITY_T3735947664_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IOSNativeUtility
struct  IOSNativeUtility_t3735947664  : public Singleton_1_t3365719810
{
public:

public:
};

struct IOSNativeUtility_t3735947664_StaticFields
{
public:
	// System.Action`1<ISN_Locale> IOSNativeUtility::OnLocaleLoaded
	Action_1_t3048335611 * ___OnLocaleLoaded_4;
	// System.Action`1<System.Boolean> IOSNativeUtility::GuidedAccessSessionRequestResult
	Action_1_t269755560 * ___GuidedAccessSessionRequestResult_5;

public:
	inline static int32_t get_offset_of_OnLocaleLoaded_4() { return static_cast<int32_t>(offsetof(IOSNativeUtility_t3735947664_StaticFields, ___OnLocaleLoaded_4)); }
	inline Action_1_t3048335611 * get_OnLocaleLoaded_4() const { return ___OnLocaleLoaded_4; }
	inline Action_1_t3048335611 ** get_address_of_OnLocaleLoaded_4() { return &___OnLocaleLoaded_4; }
	inline void set_OnLocaleLoaded_4(Action_1_t3048335611 * value)
	{
		___OnLocaleLoaded_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnLocaleLoaded_4), value);
	}

	inline static int32_t get_offset_of_GuidedAccessSessionRequestResult_5() { return static_cast<int32_t>(offsetof(IOSNativeUtility_t3735947664_StaticFields, ___GuidedAccessSessionRequestResult_5)); }
	inline Action_1_t269755560 * get_GuidedAccessSessionRequestResult_5() const { return ___GuidedAccessSessionRequestResult_5; }
	inline Action_1_t269755560 ** get_address_of_GuidedAccessSessionRequestResult_5() { return &___GuidedAccessSessionRequestResult_5; }
	inline void set_GuidedAccessSessionRequestResult_5(Action_1_t269755560 * value)
	{
		___GuidedAccessSessionRequestResult_5 = value;
		Il2CppCodeGenWriteBarrier((&___GuidedAccessSessionRequestResult_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSNATIVEUTILITY_T3735947664_H
#ifndef ISN_SECURITY_T2783094519_H
#define ISN_SECURITY_T2783094519_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ISN_Security
struct  ISN_Security_t2783094519  : public Singleton_1_t2412866665
{
public:

public:
};

struct ISN_Security_t2783094519_StaticFields
{
public:
	// System.Action`1<ISN_LocalReceiptResult> ISN_Security::OnReceiptLoaded
	Action_1_t1334024509 * ___OnReceiptLoaded_4;
	// System.Action`1<SA.Common.Models.Result> ISN_Security::OnReceiptRefreshComplete
	Action_1_t47902017 * ___OnReceiptRefreshComplete_5;

public:
	inline static int32_t get_offset_of_OnReceiptLoaded_4() { return static_cast<int32_t>(offsetof(ISN_Security_t2783094519_StaticFields, ___OnReceiptLoaded_4)); }
	inline Action_1_t1334024509 * get_OnReceiptLoaded_4() const { return ___OnReceiptLoaded_4; }
	inline Action_1_t1334024509 ** get_address_of_OnReceiptLoaded_4() { return &___OnReceiptLoaded_4; }
	inline void set_OnReceiptLoaded_4(Action_1_t1334024509 * value)
	{
		___OnReceiptLoaded_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnReceiptLoaded_4), value);
	}

	inline static int32_t get_offset_of_OnReceiptRefreshComplete_5() { return static_cast<int32_t>(offsetof(ISN_Security_t2783094519_StaticFields, ___OnReceiptRefreshComplete_5)); }
	inline Action_1_t47902017 * get_OnReceiptRefreshComplete_5() const { return ___OnReceiptRefreshComplete_5; }
	inline Action_1_t47902017 ** get_address_of_OnReceiptRefreshComplete_5() { return &___OnReceiptRefreshComplete_5; }
	inline void set_OnReceiptRefreshComplete_5(Action_1_t47902017 * value)
	{
		___OnReceiptRefreshComplete_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnReceiptRefreshComplete_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISN_SECURITY_T2783094519_H
#ifndef MARKETEXAMPLE_T1239190970_H
#define MARKETEXAMPLE_T1239190970_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MarketExample
struct  MarketExample_t1239190970  : public BaseIOSFeaturePreview_t3740726741
{
public:
	// System.Byte[] MarketExample::ReceiptData
	ByteU5BU5D_t4116647657* ___ReceiptData_12;

public:
	inline static int32_t get_offset_of_ReceiptData_12() { return static_cast<int32_t>(offsetof(MarketExample_t1239190970, ___ReceiptData_12)); }
	inline ByteU5BU5D_t4116647657* get_ReceiptData_12() const { return ___ReceiptData_12; }
	inline ByteU5BU5D_t4116647657** get_address_of_ReceiptData_12() { return &___ReceiptData_12; }
	inline void set_ReceiptData_12(ByteU5BU5D_t4116647657* value)
	{
		___ReceiptData_12 = value;
		Il2CppCodeGenWriteBarrier((&___ReceiptData_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARKETEXAMPLE_T1239190970_H
#ifndef TBM_MULTIPLAYER_EXAMPLE_T1581519891_H
#define TBM_MULTIPLAYER_EXAMPLE_T1581519891_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TBM_Multiplayer_Example
struct  TBM_Multiplayer_Example_t1581519891  : public BaseIOSFeaturePreview_t3740726741
{
public:

public:
};

struct TBM_Multiplayer_Example_t1581519891_StaticFields
{
public:
	// System.Boolean TBM_Multiplayer_Example::IsInitialized
	bool ___IsInitialized_12;

public:
	inline static int32_t get_offset_of_IsInitialized_12() { return static_cast<int32_t>(offsetof(TBM_Multiplayer_Example_t1581519891_StaticFields, ___IsInitialized_12)); }
	inline bool get_IsInitialized_12() const { return ___IsInitialized_12; }
	inline bool* get_address_of_IsInitialized_12() { return &___IsInitialized_12; }
	inline void set_IsInitialized_12(bool value)
	{
		___IsInitialized_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TBM_MULTIPLAYER_EXAMPLE_T1581519891_H
#ifndef NOTIFICATIONEXAMPLE_T1809402525_H
#define NOTIFICATIONEXAMPLE_T1809402525_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NotificationExample
struct  NotificationExample_t1809402525  : public BaseIOSFeaturePreview_t3740726741
{
public:
	// System.Int32 NotificationExample::lastNotificationId
	int32_t ___lastNotificationId_12;

public:
	inline static int32_t get_offset_of_lastNotificationId_12() { return static_cast<int32_t>(offsetof(NotificationExample_t1809402525, ___lastNotificationId_12)); }
	inline int32_t get_lastNotificationId_12() const { return ___lastNotificationId_12; }
	inline int32_t* get_address_of_lastNotificationId_12() { return &___lastNotificationId_12; }
	inline void set_lastNotificationId_12(int32_t value)
	{
		___lastNotificationId_12 = value;
	}
};

struct NotificationExample_t1809402525_StaticFields
{
public:
	// System.Action`1<SA.IOSNative.UserNotifications.NotificationRequest> NotificationExample::<>f__am$cache0
	Action_1_t1794579528 * ___U3CU3Ef__amU24cache0_13;
	// System.Action`1<ISN_RemoteNotificationsRegistrationResult> NotificationExample::<>f__am$cache1
	Action_1_t1103427813 * ___U3CU3Ef__amU24cache1_14;
	// System.Action`1<SA.Common.Models.Result> NotificationExample::<>f__am$cache2
	Action_1_t47902017 * ___U3CU3Ef__amU24cache2_15;
	// System.Action`1<SA.Common.Models.Result> NotificationExample::<>f__am$cache3
	Action_1_t47902017 * ___U3CU3Ef__amU24cache3_16;
	// System.Action`1<SA.Common.Models.Result> NotificationExample::<>f__am$cache4
	Action_1_t47902017 * ___U3CU3Ef__amU24cache4_17;
	// System.Action`1<SA.Common.Models.Result> NotificationExample::<>f__am$cache5
	Action_1_t47902017 * ___U3CU3Ef__amU24cache5_18;
	// System.Action`1<System.Collections.Generic.List`1<SA.IOSNative.UserNotifications.NotificationRequest>> NotificationExample::<>f__am$cache6
	Action_1_t3266654270 * ___U3CU3Ef__amU24cache6_19;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_13() { return static_cast<int32_t>(offsetof(NotificationExample_t1809402525_StaticFields, ___U3CU3Ef__amU24cache0_13)); }
	inline Action_1_t1794579528 * get_U3CU3Ef__amU24cache0_13() const { return ___U3CU3Ef__amU24cache0_13; }
	inline Action_1_t1794579528 ** get_address_of_U3CU3Ef__amU24cache0_13() { return &___U3CU3Ef__amU24cache0_13; }
	inline void set_U3CU3Ef__amU24cache0_13(Action_1_t1794579528 * value)
	{
		___U3CU3Ef__amU24cache0_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_13), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_14() { return static_cast<int32_t>(offsetof(NotificationExample_t1809402525_StaticFields, ___U3CU3Ef__amU24cache1_14)); }
	inline Action_1_t1103427813 * get_U3CU3Ef__amU24cache1_14() const { return ___U3CU3Ef__amU24cache1_14; }
	inline Action_1_t1103427813 ** get_address_of_U3CU3Ef__amU24cache1_14() { return &___U3CU3Ef__amU24cache1_14; }
	inline void set_U3CU3Ef__amU24cache1_14(Action_1_t1103427813 * value)
	{
		___U3CU3Ef__amU24cache1_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_14), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_15() { return static_cast<int32_t>(offsetof(NotificationExample_t1809402525_StaticFields, ___U3CU3Ef__amU24cache2_15)); }
	inline Action_1_t47902017 * get_U3CU3Ef__amU24cache2_15() const { return ___U3CU3Ef__amU24cache2_15; }
	inline Action_1_t47902017 ** get_address_of_U3CU3Ef__amU24cache2_15() { return &___U3CU3Ef__amU24cache2_15; }
	inline void set_U3CU3Ef__amU24cache2_15(Action_1_t47902017 * value)
	{
		___U3CU3Ef__amU24cache2_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_15), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_16() { return static_cast<int32_t>(offsetof(NotificationExample_t1809402525_StaticFields, ___U3CU3Ef__amU24cache3_16)); }
	inline Action_1_t47902017 * get_U3CU3Ef__amU24cache3_16() const { return ___U3CU3Ef__amU24cache3_16; }
	inline Action_1_t47902017 ** get_address_of_U3CU3Ef__amU24cache3_16() { return &___U3CU3Ef__amU24cache3_16; }
	inline void set_U3CU3Ef__amU24cache3_16(Action_1_t47902017 * value)
	{
		___U3CU3Ef__amU24cache3_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_16), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_17() { return static_cast<int32_t>(offsetof(NotificationExample_t1809402525_StaticFields, ___U3CU3Ef__amU24cache4_17)); }
	inline Action_1_t47902017 * get_U3CU3Ef__amU24cache4_17() const { return ___U3CU3Ef__amU24cache4_17; }
	inline Action_1_t47902017 ** get_address_of_U3CU3Ef__amU24cache4_17() { return &___U3CU3Ef__amU24cache4_17; }
	inline void set_U3CU3Ef__amU24cache4_17(Action_1_t47902017 * value)
	{
		___U3CU3Ef__amU24cache4_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache4_17), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_18() { return static_cast<int32_t>(offsetof(NotificationExample_t1809402525_StaticFields, ___U3CU3Ef__amU24cache5_18)); }
	inline Action_1_t47902017 * get_U3CU3Ef__amU24cache5_18() const { return ___U3CU3Ef__amU24cache5_18; }
	inline Action_1_t47902017 ** get_address_of_U3CU3Ef__amU24cache5_18() { return &___U3CU3Ef__amU24cache5_18; }
	inline void set_U3CU3Ef__amU24cache5_18(Action_1_t47902017 * value)
	{
		___U3CU3Ef__amU24cache5_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache5_18), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_19() { return static_cast<int32_t>(offsetof(NotificationExample_t1809402525_StaticFields, ___U3CU3Ef__amU24cache6_19)); }
	inline Action_1_t3266654270 * get_U3CU3Ef__amU24cache6_19() const { return ___U3CU3Ef__amU24cache6_19; }
	inline Action_1_t3266654270 ** get_address_of_U3CU3Ef__amU24cache6_19() { return &___U3CU3Ef__amU24cache6_19; }
	inline void set_U3CU3Ef__amU24cache6_19(Action_1_t3266654270 * value)
	{
		___U3CU3Ef__amU24cache6_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache6_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIFICATIONEXAMPLE_T1809402525_H
#ifndef GAMECENTEREXAMPLE_T2173025316_H
#define GAMECENTEREXAMPLE_T2173025316_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameCenterExample
struct  GameCenterExample_t2173025316  : public BaseIOSFeaturePreview_t3740726741
{
public:
	// System.Int32 GameCenterExample::hiScore
	int32_t ___hiScore_12;
	// System.String GameCenterExample::TEST_LEADERBOARD_1
	String_t* ___TEST_LEADERBOARD_1_13;
	// System.String GameCenterExample::TEST_LEADERBOARD_2
	String_t* ___TEST_LEADERBOARD_2_14;
	// System.String GameCenterExample::TEST_ACHIEVEMENT_1_ID
	String_t* ___TEST_ACHIEVEMENT_1_ID_15;
	// System.String GameCenterExample::TEST_ACHIEVEMENT_2_ID
	String_t* ___TEST_ACHIEVEMENT_2_ID_16;

public:
	inline static int32_t get_offset_of_hiScore_12() { return static_cast<int32_t>(offsetof(GameCenterExample_t2173025316, ___hiScore_12)); }
	inline int32_t get_hiScore_12() const { return ___hiScore_12; }
	inline int32_t* get_address_of_hiScore_12() { return &___hiScore_12; }
	inline void set_hiScore_12(int32_t value)
	{
		___hiScore_12 = value;
	}

	inline static int32_t get_offset_of_TEST_LEADERBOARD_1_13() { return static_cast<int32_t>(offsetof(GameCenterExample_t2173025316, ___TEST_LEADERBOARD_1_13)); }
	inline String_t* get_TEST_LEADERBOARD_1_13() const { return ___TEST_LEADERBOARD_1_13; }
	inline String_t** get_address_of_TEST_LEADERBOARD_1_13() { return &___TEST_LEADERBOARD_1_13; }
	inline void set_TEST_LEADERBOARD_1_13(String_t* value)
	{
		___TEST_LEADERBOARD_1_13 = value;
		Il2CppCodeGenWriteBarrier((&___TEST_LEADERBOARD_1_13), value);
	}

	inline static int32_t get_offset_of_TEST_LEADERBOARD_2_14() { return static_cast<int32_t>(offsetof(GameCenterExample_t2173025316, ___TEST_LEADERBOARD_2_14)); }
	inline String_t* get_TEST_LEADERBOARD_2_14() const { return ___TEST_LEADERBOARD_2_14; }
	inline String_t** get_address_of_TEST_LEADERBOARD_2_14() { return &___TEST_LEADERBOARD_2_14; }
	inline void set_TEST_LEADERBOARD_2_14(String_t* value)
	{
		___TEST_LEADERBOARD_2_14 = value;
		Il2CppCodeGenWriteBarrier((&___TEST_LEADERBOARD_2_14), value);
	}

	inline static int32_t get_offset_of_TEST_ACHIEVEMENT_1_ID_15() { return static_cast<int32_t>(offsetof(GameCenterExample_t2173025316, ___TEST_ACHIEVEMENT_1_ID_15)); }
	inline String_t* get_TEST_ACHIEVEMENT_1_ID_15() const { return ___TEST_ACHIEVEMENT_1_ID_15; }
	inline String_t** get_address_of_TEST_ACHIEVEMENT_1_ID_15() { return &___TEST_ACHIEVEMENT_1_ID_15; }
	inline void set_TEST_ACHIEVEMENT_1_ID_15(String_t* value)
	{
		___TEST_ACHIEVEMENT_1_ID_15 = value;
		Il2CppCodeGenWriteBarrier((&___TEST_ACHIEVEMENT_1_ID_15), value);
	}

	inline static int32_t get_offset_of_TEST_ACHIEVEMENT_2_ID_16() { return static_cast<int32_t>(offsetof(GameCenterExample_t2173025316, ___TEST_ACHIEVEMENT_2_ID_16)); }
	inline String_t* get_TEST_ACHIEVEMENT_2_ID_16() const { return ___TEST_ACHIEVEMENT_2_ID_16; }
	inline String_t** get_address_of_TEST_ACHIEVEMENT_2_ID_16() { return &___TEST_ACHIEVEMENT_2_ID_16; }
	inline void set_TEST_ACHIEVEMENT_2_ID_16(String_t* value)
	{
		___TEST_ACHIEVEMENT_2_ID_16 = value;
		Il2CppCodeGenWriteBarrier((&___TEST_ACHIEVEMENT_2_ID_16), value);
	}
};

struct GameCenterExample_t2173025316_StaticFields
{
public:
	// System.Boolean GameCenterExample::IsInitialized
	bool ___IsInitialized_17;
	// System.Int64 GameCenterExample::LB2BestScores
	int64_t ___LB2BestScores_18;

public:
	inline static int32_t get_offset_of_IsInitialized_17() { return static_cast<int32_t>(offsetof(GameCenterExample_t2173025316_StaticFields, ___IsInitialized_17)); }
	inline bool get_IsInitialized_17() const { return ___IsInitialized_17; }
	inline bool* get_address_of_IsInitialized_17() { return &___IsInitialized_17; }
	inline void set_IsInitialized_17(bool value)
	{
		___IsInitialized_17 = value;
	}

	inline static int32_t get_offset_of_LB2BestScores_18() { return static_cast<int32_t>(offsetof(GameCenterExample_t2173025316_StaticFields, ___LB2BestScores_18)); }
	inline int64_t get_LB2BestScores_18() const { return ___LB2BestScores_18; }
	inline int64_t* get_address_of_LB2BestScores_18() { return &___LB2BestScores_18; }
	inline void set_LB2BestScores_18(int64_t value)
	{
		___LB2BestScores_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMECENTEREXAMPLE_T2173025316_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2500 = { sizeof (RestoreResult_t1480690855), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2501 = { sizeof (VerificationResponse_t3269199428), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2501[4] = 
{
	VerificationResponse_t3269199428::get_offset_of__Status_0(),
	VerificationResponse_t3269199428::get_offset_of__Receipt_1(),
	VerificationResponse_t3269199428::get_offset_of__ProductIdentifier_2(),
	VerificationResponse_t3269199428::get_offset_of__OriginalJSON_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2502 = { sizeof (Contact_t2387438694), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2502[4] = 
{
	Contact_t2387438694::get_offset_of_GivenName_0(),
	Contact_t2387438694::get_offset_of_FamilyName_1(),
	Contact_t2387438694::get_offset_of_Emails_2(),
	Contact_t2387438694::get_offset_of_PhoneNumbers_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2503 = { sizeof (ContactsResult_t1151527695), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2503[1] = 
{
	ContactsResult_t1151527695::get_offset_of__Contacts_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2504 = { sizeof (ContactStore_t1420756449), -1, sizeof(ContactStore_t1420756449_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2504[8] = 
{
	ContactStore_t1420756449::get_offset_of_ContactsLoadResult_4(),
	ContactStore_t1420756449::get_offset_of_ContactsPickResult_5(),
	ContactStore_t1420756449_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_6(),
	ContactStore_t1420756449_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_7(),
	ContactStore_t1420756449_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_8(),
	ContactStore_t1420756449_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_9(),
	ContactStore_t1420756449_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_10(),
	ContactStore_t1420756449_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2505 = { sizeof (PhoneNumber_t1139747375), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2505[2] = 
{
	PhoneNumber_t1139747375::get_offset_of_CountryCode_0(),
	PhoneNumber_t1139747375::get_offset_of_Digits_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2506 = { sizeof (ISN_Build_t2176122696), -1, sizeof(ISN_Build_t2176122696_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2506[3] = 
{
	ISN_Build_t2176122696::get_offset_of__Version_0(),
	ISN_Build_t2176122696::get_offset_of__Number_1(),
	ISN_Build_t2176122696_StaticFields::get_offset_of__Current_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2507 = { sizeof (ISN_Device_t1685187044), -1, sizeof(ISN_Device_t1685187044_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2507[10] = 
{
	ISN_Device_t1685187044_StaticFields::get_offset_of__CurrentDevice_0(),
	ISN_Device_t1685187044::get_offset_of__Name_1(),
	ISN_Device_t1685187044::get_offset_of__SystemName_2(),
	ISN_Device_t1685187044::get_offset_of__Model_3(),
	ISN_Device_t1685187044::get_offset_of__LocalizedModel_4(),
	ISN_Device_t1685187044::get_offset_of__SystemVersion_5(),
	ISN_Device_t1685187044::get_offset_of__MajorSystemVersion_6(),
	ISN_Device_t1685187044::get_offset_of__PreferredLanguage_ISO639_1_7(),
	ISN_Device_t1685187044::get_offset_of__InterfaceIdiom_8(),
	ISN_Device_t1685187044::get_offset_of__GUID_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2508 = { sizeof (ISN_DeviceGUID_t3668829792), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2508[2] = 
{
	ISN_DeviceGUID_t3668829792::get_offset_of__Bytes_0(),
	ISN_DeviceGUID_t3668829792::get_offset_of__Base64_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2509 = { sizeof (ISN_InterfaceIdiom_t3035749497)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2509[4] = 
{
	ISN_InterfaceIdiom_t3035749497::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2510 = { sizeof (ISN_TimeZone_t2672557812), -1, sizeof(ISN_TimeZone_t2672557812_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2510[3] = 
{
	ISN_TimeZone_t2672557812::get_offset_of__SecondsFromGMT_0(),
	ISN_TimeZone_t2672557812::get_offset_of__Name_1(),
	ISN_TimeZone_t2672557812_StaticFields::get_offset_of__LocalTimeZone_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2511 = { sizeof (ForceInfo_t3934387890), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2511[2] = 
{
	ForceInfo_t3934387890::get_offset_of__Force_0(),
	ForceInfo_t3934387890::get_offset_of__MaxForce_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2512 = { sizeof (ForceTouch_t2199614965), -1, sizeof(ForceTouch_t2199614965_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2512[9] = 
{
	ForceTouch_t2199614965::get_offset_of_OnForceTouchStarted_4(),
	ForceTouch_t2199614965::get_offset_of_OnForceTouchFinished_5(),
	ForceTouch_t2199614965::get_offset_of_OnForceChanged_6(),
	ForceTouch_t2199614965::get_offset_of_OnAppShortcutClick_7(),
	ForceTouch_t2199614965_StaticFields::get_offset_of__IsTouchTrigerred_8(),
	ForceTouch_t2199614965_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_9(),
	ForceTouch_t2199614965_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_10(),
	ForceTouch_t2199614965_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_11(),
	ForceTouch_t2199614965_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2513 = { sizeof (ISN_GestureRecognizer_t3385868019), -1, sizeof(ISN_GestureRecognizer_t3385868019_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2513[2] = 
{
	ISN_GestureRecognizer_t3385868019::get_offset_of_OnSwipe_4(),
	ISN_GestureRecognizer_t3385868019_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2514 = { sizeof (ISN_SwipeDirection_t3055520214)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2514[5] = 
{
	ISN_SwipeDirection_t3055520214::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2515 = { sizeof (ISN_Logger_t4052948437), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2516 = { sizeof (ForceTouchMenuItem_t113152519), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2516[4] = 
{
	ForceTouchMenuItem_t113152519::get_offset_of_IsOpen_0(),
	ForceTouchMenuItem_t113152519::get_offset_of_Title_1(),
	ForceTouchMenuItem_t113152519::get_offset_of_Subtitle_2(),
	ForceTouchMenuItem_t113152519::get_offset_of_Action_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2517 = { sizeof (LaunchUrl_t1254559570), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2517[3] = 
{
	LaunchUrl_t1254559570::get_offset_of__URI_0(),
	LaunchUrl_t1254559570::get_offset_of__AbsoluteUrl_1(),
	LaunchUrl_t1254559570::get_offset_of__SourceApplication_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2518 = { sizeof (UniversalLink_t4104582537), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2518[2] = 
{
	UniversalLink_t4104582537::get_offset_of__URI_0(),
	UniversalLink_t4104582537::get_offset_of__AbsoluteUrl_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2519 = { sizeof (UrlType_t2717836817), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2519[3] = 
{
	UrlType_t2717836817::get_offset_of_Identifier_0(),
	UrlType_t2717836817::get_offset_of_Schemes_1(),
	UrlType_t2717836817::get_offset_of_IsOpen_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2520 = { sizeof (NativeReceiver_t107704069), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2521 = { sizeof (PermissionsManager_t2458142426), -1, sizeof(PermissionsManager_t2458142426_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2521[1] = 
{
	PermissionsManager_t2458142426_StaticFields::get_offset_of_OnResponseDictionary_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2522 = { sizeof (PermissionStatus_t941164143)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2522[5] = 
{
	PermissionStatus_t941164143::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2523 = { sizeof (Permission_t3948953700)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2523[9] = 
{
	Permission_t3948953700::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2524 = { sizeof (ISN_LocalReceiptResult_t1161556914), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2524[2] = 
{
	ISN_LocalReceiptResult_t1161556914::get_offset_of__Receipt_0(),
	ISN_LocalReceiptResult_t1161556914::get_offset_of__ReceiptString_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2525 = { sizeof (ISN_Security_t2783094519), -1, sizeof(ISN_Security_t2783094519_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2525[2] = 
{
	ISN_Security_t2783094519_StaticFields::get_offset_of_OnReceiptLoaded_4(),
	ISN_Security_t2783094519_StaticFields::get_offset_of_OnReceiptRefreshComplete_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2526 = { sizeof (SharedApplication_t2880145764), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2527 = { sizeof (AppCache_t1657377921), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2528 = { sizeof (IOSNativeUtility_t3735947664), -1, sizeof(IOSNativeUtility_t3735947664_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2528[2] = 
{
	IOSNativeUtility_t3735947664_StaticFields::get_offset_of_OnLocaleLoaded_4(),
	IOSNativeUtility_t3735947664_StaticFields::get_offset_of_GuidedAccessSessionRequestResult_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2529 = { sizeof (ISN_Locale_t2875868016), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2529[4] = 
{
	ISN_Locale_t2875868016::get_offset_of__CountryCode_0(),
	ISN_Locale_t2875868016::get_offset_of__DisplayCountry_1(),
	ISN_Locale_t2875868016::get_offset_of__LanguageCode_2(),
	ISN_Locale_t2875868016::get_offset_of__DisplayLanguage_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2530 = { sizeof (Calendar_t3271124058), -1, sizeof(Calendar_t3271124058_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2530[1] = 
{
	Calendar_t3271124058_StaticFields::get_offset_of_OnCalendarClosed_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2531 = { sizeof (DateTimePicker_t2880553240), -1, sizeof(DateTimePicker_t2880553240_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2531[2] = 
{
	DateTimePicker_t2880553240_StaticFields::get_offset_of_OnPickerClosed_0(),
	DateTimePicker_t2880553240_StaticFields::get_offset_of_OnPickerDateChanged_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2532 = { sizeof (DateTimePickerMode_t2532598555)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2532[5] = 
{
	DateTimePickerMode_t2532598555::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2533 = { sizeof (NativeReceiver_t3281330226), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2534 = { sizeof (BaseIOSFeaturePreview_t3740726741), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2534[10] = 
{
	BaseIOSFeaturePreview_t3740726741::get_offset_of_style_2(),
	BaseIOSFeaturePreview_t3740726741::get_offset_of_buttonWidth_3(),
	BaseIOSFeaturePreview_t3740726741::get_offset_of_buttonHeight_4(),
	BaseIOSFeaturePreview_t3740726741::get_offset_of_StartY_5(),
	BaseIOSFeaturePreview_t3740726741::get_offset_of_StartX_6(),
	BaseIOSFeaturePreview_t3740726741::get_offset_of_XStartPos_7(),
	BaseIOSFeaturePreview_t3740726741::get_offset_of_YStartPos_8(),
	BaseIOSFeaturePreview_t3740726741::get_offset_of_XButtonStep_9(),
	BaseIOSFeaturePreview_t3740726741::get_offset_of_YButtonStep_10(),
	BaseIOSFeaturePreview_t3740726741::get_offset_of_YLableStep_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2535 = { sizeof (MarketExample_t1239190970), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2535[1] = 
{
	MarketExample_t1239190970::get_offset_of_ReceiptData_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2536 = { sizeof (U3CSendRequestU3Ec__Iterator0_t2526093774), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2536[9] = 
{
	U3CSendRequestU3Ec__Iterator0_t2526093774::get_offset_of_U3Cbase64stringU3E__0_0(),
	U3CSendRequestU3Ec__Iterator0_t2526093774::get_offset_of_U3COriginalJSONU3E__0_1(),
	U3CSendRequestU3Ec__Iterator0_t2526093774::get_offset_of_U3CdataU3E__0_2(),
	U3CSendRequestU3Ec__Iterator0_t2526093774::get_offset_of_U3CbinaryDataU3E__0_3(),
	U3CSendRequestU3Ec__Iterator0_t2526093774::get_offset_of_U3CwwwU3E__0_4(),
	U3CSendRequestU3Ec__Iterator0_t2526093774::get_offset_of_U24this_5(),
	U3CSendRequestU3Ec__Iterator0_t2526093774::get_offset_of_U24current_6(),
	U3CSendRequestU3Ec__Iterator0_t2526093774::get_offset_of_U24disposing_7(),
	U3CSendRequestU3Ec__Iterator0_t2526093774::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2537 = { sizeof (PaymentManagerExample_t503931474), -1, sizeof(PaymentManagerExample_t503931474_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2537[8] = 
{
	0,
	0,
	PaymentManagerExample_t503931474::get_offset_of_lastTransactionProdudctId_2(),
	PaymentManagerExample_t503931474_StaticFields::get_offset_of_IsInitialized_3(),
	PaymentManagerExample_t503931474_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_4(),
	PaymentManagerExample_t503931474_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_5(),
	PaymentManagerExample_t503931474_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_6(),
	PaymentManagerExample_t503931474_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2538 = { sizeof (GameCenterExample_t2173025316), -1, sizeof(GameCenterExample_t2173025316_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2538[7] = 
{
	GameCenterExample_t2173025316::get_offset_of_hiScore_12(),
	GameCenterExample_t2173025316::get_offset_of_TEST_LEADERBOARD_1_13(),
	GameCenterExample_t2173025316::get_offset_of_TEST_LEADERBOARD_2_14(),
	GameCenterExample_t2173025316::get_offset_of_TEST_ACHIEVEMENT_1_ID_15(),
	GameCenterExample_t2173025316::get_offset_of_TEST_ACHIEVEMENT_2_ID_16(),
	GameCenterExample_t2173025316_StaticFields::get_offset_of_IsInitialized_17(),
	GameCenterExample_t2173025316_StaticFields::get_offset_of_LB2BestScores_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2539 = { sizeof (GameCenterFriendLoadExample_t946910862), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2539[5] = 
{
	GameCenterFriendLoadExample_t946910862::get_offset_of_ChallengeLeaderboard_2(),
	GameCenterFriendLoadExample_t946910862::get_offset_of_ChallengeAchievement_3(),
	GameCenterFriendLoadExample_t946910862::get_offset_of_headerStyle_4(),
	GameCenterFriendLoadExample_t946910862::get_offset_of_boardStyle_5(),
	GameCenterFriendLoadExample_t946910862::get_offset_of_renderFriendsList_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2540 = { sizeof (LeaderboardCustomGUIExample_t38875209), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2540[6] = 
{
	LeaderboardCustomGUIExample_t38875209::get_offset_of_leaderboardId_1_2(),
	LeaderboardCustomGUIExample_t38875209::get_offset_of_hiScore_3(),
	LeaderboardCustomGUIExample_t38875209::get_offset_of_headerStyle_4(),
	LeaderboardCustomGUIExample_t38875209::get_offset_of_boardStyle_5(),
	LeaderboardCustomGUIExample_t38875209::get_offset_of_loadedLeaderboard_6(),
	LeaderboardCustomGUIExample_t38875209::get_offset_of_displayCollection_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2541 = { sizeof (MultiplayerManagerExample_t1146359991), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2542 = { sizeof (ConnectionButton_t3407155931), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2542[3] = 
{
	ConnectionButton_t3407155931::get_offset_of_w_2(),
	ConnectionButton_t3407155931::get_offset_of_h_3(),
	ConnectionButton_t3407155931::get_offset_of_r_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2543 = { sizeof (DisconnectButton_t3682185937), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2543[3] = 
{
	DisconnectButton_t3682185937::get_offset_of_w_2(),
	DisconnectButton_t3682185937::get_offset_of_h_3(),
	DisconnectButton_t3682185937::get_offset_of_r_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2544 = { sizeof (ClickManagerExample_t3328835271), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2545 = { sizeof (NetworkManagerExample_t3775854059), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2546 = { sizeof (PTPGameController_t1530419951), -1, sizeof(PTPGameController_t1530419951_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2546[6] = 
{
	PTPGameController_t1530419951::get_offset_of_pref_2(),
	PTPGameController_t1530419951::get_offset_of_d_3(),
	PTPGameController_t1530419951::get_offset_of_b_4(),
	PTPGameController_t1530419951::get_offset_of_m_5(),
	PTPGameController_t1530419951_StaticFields::get_offset_of_instance_6(),
	PTPGameController_t1530419951::get_offset_of_spheres_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2547 = { sizeof (BasePackage_t3538619746), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2547[2] = 
{
	BasePackage_t3538619746::get_offset_of_buffer_0(),
	BasePackage_t3538619746::get_offset_of_writer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2548 = { sizeof (HelloPackage_t2020835704), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2549 = { sizeof (ObjectCreatePackage_t1844192988), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2550 = { sizeof (ByteBuffer_t4262155383), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2550[2] = 
{
	ByteBuffer_t4262155383::get_offset_of_buffer_0(),
	ByteBuffer_t4262155383::get_offset_of_pointer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2551 = { sizeof (TBM_Multiplayer_Example_t1581519891), -1, sizeof(TBM_Multiplayer_Example_t1581519891_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2551[1] = 
{
	TBM_Multiplayer_Example_t1581519891_StaticFields::get_offset_of_IsInitialized_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2552 = { sizeof (AppEventHandlerExample_t2737898202), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2553 = { sizeof (CloudKitUseExample_t417999627), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2554 = { sizeof (GameSavesExample_t2436371863), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2554[4] = 
{
	GameSavesExample_t2436371863::get_offset_of_GameSaves_12(),
	GameSavesExample_t2436371863::get_offset_of_SavesConflicts_13(),
	GameSavesExample_t2436371863::get_offset_of_test_name_14(),
	GameSavesExample_t2436371863::get_offset_of_test_name_2_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2555 = { sizeof (iCloudUseExample_t3965519368), -1, sizeof(iCloudUseExample_t3965519368_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2555[2] = 
{
	iCloudUseExample_t3965519368::get_offset_of_v_12(),
	iCloudUseExample_t3965519368_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2556 = { sizeof (IOSSocialUseExample_t1226332829), -1, sizeof(IOSSocialUseExample_t1226332829_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2556[5] = 
{
	IOSSocialUseExample_t1226332829::get_offset_of_style_2(),
	IOSSocialUseExample_t1226332829::get_offset_of_style2_3(),
	IOSSocialUseExample_t1226332829::get_offset_of_drawTexture_4(),
	IOSSocialUseExample_t1226332829::get_offset_of_textureForPost_5(),
	IOSSocialUseExample_t1226332829_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2557 = { sizeof (U3CPostScreenshotInstagramU3Ec__Iterator0_t1032474826), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2557[6] = 
{
	U3CPostScreenshotInstagramU3Ec__Iterator0_t1032474826::get_offset_of_U3CwidthU3E__0_0(),
	U3CPostScreenshotInstagramU3Ec__Iterator0_t1032474826::get_offset_of_U3CheightU3E__0_1(),
	U3CPostScreenshotInstagramU3Ec__Iterator0_t1032474826::get_offset_of_U3CtexU3E__0_2(),
	U3CPostScreenshotInstagramU3Ec__Iterator0_t1032474826::get_offset_of_U24current_3(),
	U3CPostScreenshotInstagramU3Ec__Iterator0_t1032474826::get_offset_of_U24disposing_4(),
	U3CPostScreenshotInstagramU3Ec__Iterator0_t1032474826::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2558 = { sizeof (U3CPostScreenshotU3Ec__Iterator1_t386821259), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2558[6] = 
{
	U3CPostScreenshotU3Ec__Iterator1_t386821259::get_offset_of_U3CwidthU3E__0_0(),
	U3CPostScreenshotU3Ec__Iterator1_t386821259::get_offset_of_U3CheightU3E__0_1(),
	U3CPostScreenshotU3Ec__Iterator1_t386821259::get_offset_of_U3CtexU3E__0_2(),
	U3CPostScreenshotU3Ec__Iterator1_t386821259::get_offset_of_U24current_3(),
	U3CPostScreenshotU3Ec__Iterator1_t386821259::get_offset_of_U24disposing_4(),
	U3CPostScreenshotU3Ec__Iterator1_t386821259::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2559 = { sizeof (U3CPostTwitterScreenshotU3Ec__Iterator2_t1119173099), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2559[6] = 
{
	U3CPostTwitterScreenshotU3Ec__Iterator2_t1119173099::get_offset_of_U3CwidthU3E__0_0(),
	U3CPostTwitterScreenshotU3Ec__Iterator2_t1119173099::get_offset_of_U3CheightU3E__0_1(),
	U3CPostTwitterScreenshotU3Ec__Iterator2_t1119173099::get_offset_of_U3CtexU3E__0_2(),
	U3CPostTwitterScreenshotU3Ec__Iterator2_t1119173099::get_offset_of_U24current_3(),
	U3CPostTwitterScreenshotU3Ec__Iterator2_t1119173099::get_offset_of_U24disposing_4(),
	U3CPostTwitterScreenshotU3Ec__Iterator2_t1119173099::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2560 = { sizeof (U3CPostFBScreenshotU3Ec__Iterator3_t388533558), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2560[6] = 
{
	U3CPostFBScreenshotU3Ec__Iterator3_t388533558::get_offset_of_U3CwidthU3E__0_0(),
	U3CPostFBScreenshotU3Ec__Iterator3_t388533558::get_offset_of_U3CheightU3E__0_1(),
	U3CPostFBScreenshotU3Ec__Iterator3_t388533558::get_offset_of_U3CtexU3E__0_2(),
	U3CPostFBScreenshotU3Ec__Iterator3_t388533558::get_offset_of_U24current_3(),
	U3CPostFBScreenshotU3Ec__Iterator3_t388533558::get_offset_of_U24disposing_4(),
	U3CPostFBScreenshotU3Ec__Iterator3_t388533558::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2561 = { sizeof (ISNMediaExample_t1939259784), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2562 = { sizeof (LocalStorageUseExample_t854350395), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2563 = { sizeof (NativeIOSActionsExample_t2698905075), -1, sizeof(NativeIOSActionsExample_t2698905075_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2563[19] = 
{
	NativeIOSActionsExample_t2698905075::get_offset_of_hello_texture_12(),
	NativeIOSActionsExample_t2698905075::get_offset_of_drawTexture_13(),
	NativeIOSActionsExample_t2698905075::get_offset_of_time_14(),
	NativeIOSActionsExample_t2698905075_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_15(),
	NativeIOSActionsExample_t2698905075_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_16(),
	NativeIOSActionsExample_t2698905075_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_17(),
	NativeIOSActionsExample_t2698905075_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_18(),
	NativeIOSActionsExample_t2698905075_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_19(),
	NativeIOSActionsExample_t2698905075_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_20(),
	NativeIOSActionsExample_t2698905075_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_21(),
	NativeIOSActionsExample_t2698905075_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_22(),
	NativeIOSActionsExample_t2698905075_StaticFields::get_offset_of_U3CU3Ef__amU24cache8_23(),
	NativeIOSActionsExample_t2698905075_StaticFields::get_offset_of_U3CU3Ef__amU24cache9_24(),
	NativeIOSActionsExample_t2698905075_StaticFields::get_offset_of_U3CU3Ef__amU24cacheA_25(),
	NativeIOSActionsExample_t2698905075_StaticFields::get_offset_of_U3CU3Ef__amU24cacheB_26(),
	NativeIOSActionsExample_t2698905075_StaticFields::get_offset_of_U3CU3Ef__amU24cacheC_27(),
	NativeIOSActionsExample_t2698905075_StaticFields::get_offset_of_U3CU3Ef__amU24cacheD_28(),
	NativeIOSActionsExample_t2698905075_StaticFields::get_offset_of_U3CU3Ef__amU24cacheE_29(),
	NativeIOSActionsExample_t2698905075_StaticFields::get_offset_of_U3CU3Ef__amU24cacheF_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2564 = { sizeof (NotificationExample_t1809402525), -1, sizeof(NotificationExample_t1809402525_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2564[8] = 
{
	NotificationExample_t1809402525::get_offset_of_lastNotificationId_12(),
	NotificationExample_t1809402525_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_13(),
	NotificationExample_t1809402525_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_14(),
	NotificationExample_t1809402525_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_15(),
	NotificationExample_t1809402525_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_16(),
	NotificationExample_t1809402525_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_17(),
	NotificationExample_t1809402525_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_18(),
	NotificationExample_t1809402525_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2565 = { sizeof (PopUpExamples_t1488449073), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2566 = { sizeof (ReplayKitUseExample_t459666869), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2567 = { sizeof (IOSNativeFeaturesPreview_t2085497528), -1, sizeof(IOSNativeFeaturesPreview_t2085497528_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2567[2] = 
{
	IOSNativeFeaturesPreview_t2085497528::get_offset_of_x_12(),
	IOSNativeFeaturesPreview_t2085497528_StaticFields::get_offset_of_back_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2568 = { sizeof (IOSNativePreviewBackButton_t1249685067), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2568[1] = 
{
	IOSNativePreviewBackButton_t1249685067::get_offset_of_initialSceneName_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2569 = { sizeof (GameCenterTvOSExample_t1376821359), -1, sizeof(GameCenterTvOSExample_t1376821359_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2569[6] = 
{
	GameCenterTvOSExample_t1376821359::get_offset_of_hiScore_2(),
	GameCenterTvOSExample_t1376821359::get_offset_of__IsUILocked_3(),
	GameCenterTvOSExample_t1376821359::get_offset_of_TEST_LEADERBOARD_1_4(),
	GameCenterTvOSExample_t1376821359::get_offset_of_TEST_ACHIEVEMENT_1_ID_5(),
	GameCenterTvOSExample_t1376821359::get_offset_of_TEST_ACHIEVEMENT_2_ID_6(),
	GameCenterTvOSExample_t1376821359_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2570 = { sizeof (TvOsCloudExample_t4075801133), -1, sizeof(TvOsCloudExample_t4075801133_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2570[1] = 
{
	TvOsCloudExample_t4075801133_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2571 = { sizeof (TvOSInAppsExample_t3613881217), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2572 = { sizeof (Converter_t2106512330), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2572[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2573 = { sizeof (Json_t4217458989), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2574 = { sizeof (Parser_t3245694681), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2574[3] = 
{
	0,
	0,
	Parser_t3245694681::get_offset_of_json_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2575 = { sizeof (TOKEN_t3446863854)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2575[13] = 
{
	TOKEN_t3446863854::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2576 = { sizeof (Serializer_t2172110415), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2576[1] = 
{
	Serializer_t2172110415::get_offset_of_builder_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2577 = { sizeof (EaseType_t2858385462)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2577[34] = 
{
	EaseType_t2858385462::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2578 = { sizeof (ValuesTween_t2434473678), -1, sizeof(ValuesTween_t2434473678_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2578[9] = 
{
	ValuesTween_t2434473678::get_offset_of_OnComplete_2(),
	ValuesTween_t2434473678::get_offset_of_OnValueChanged_3(),
	ValuesTween_t2434473678::get_offset_of_OnVectorValueChanged_4(),
	ValuesTween_t2434473678::get_offset_of_DestoryGameObjectOnComplete_5(),
	ValuesTween_t2434473678::get_offset_of_FinalFloatValue_6(),
	ValuesTween_t2434473678::get_offset_of_FinalVectorValue_7(),
	ValuesTween_t2434473678_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_8(),
	ValuesTween_t2434473678_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_9(),
	ValuesTween_t2434473678_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2579 = { sizeof (SA_iTween_t1550711905), -1, sizeof(SA_iTween_t1550711905_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2579[39] = 
{
	SA_iTween_t1550711905_StaticFields::get_offset_of_tweens_2(),
	SA_iTween_t1550711905_StaticFields::get_offset_of_cameraFade_3(),
	SA_iTween_t1550711905::get_offset_of_id_4(),
	SA_iTween_t1550711905::get_offset_of_type_5(),
	SA_iTween_t1550711905::get_offset_of_method_6(),
	SA_iTween_t1550711905::get_offset_of_easeType_7(),
	SA_iTween_t1550711905::get_offset_of_time_8(),
	SA_iTween_t1550711905::get_offset_of_delay_9(),
	SA_iTween_t1550711905::get_offset_of_loopType_10(),
	SA_iTween_t1550711905::get_offset_of_isRunning_11(),
	SA_iTween_t1550711905::get_offset_of_isPaused_12(),
	SA_iTween_t1550711905::get_offset_of__name_13(),
	SA_iTween_t1550711905::get_offset_of_runningTime_14(),
	SA_iTween_t1550711905::get_offset_of_percentage_15(),
	SA_iTween_t1550711905::get_offset_of_delayStarted_16(),
	SA_iTween_t1550711905::get_offset_of_kinematic_17(),
	SA_iTween_t1550711905::get_offset_of_isLocal_18(),
	SA_iTween_t1550711905::get_offset_of_loop_19(),
	SA_iTween_t1550711905::get_offset_of_reverse_20(),
	SA_iTween_t1550711905::get_offset_of_wasPaused_21(),
	SA_iTween_t1550711905::get_offset_of_physics_22(),
	SA_iTween_t1550711905::get_offset_of_tweenArguments_23(),
	SA_iTween_t1550711905::get_offset_of_space_24(),
	SA_iTween_t1550711905::get_offset_of_ease_25(),
	SA_iTween_t1550711905::get_offset_of_apply_26(),
	SA_iTween_t1550711905::get_offset_of_audioSource_27(),
	SA_iTween_t1550711905::get_offset_of_vector3s_28(),
	SA_iTween_t1550711905::get_offset_of_vector2s_29(),
	SA_iTween_t1550711905::get_offset_of_colors_30(),
	SA_iTween_t1550711905::get_offset_of_floats_31(),
	SA_iTween_t1550711905::get_offset_of_rects_32(),
	SA_iTween_t1550711905::get_offset_of_path_33(),
	SA_iTween_t1550711905::get_offset_of_preUpdate_34(),
	SA_iTween_t1550711905::get_offset_of_postUpdate_35(),
	SA_iTween_t1550711905::get_offset_of_namedcolorvalue_36(),
	SA_iTween_t1550711905::get_offset_of_lastRealTime_37(),
	SA_iTween_t1550711905::get_offset_of_useRealTime_38(),
	SA_iTween_t1550711905::get_offset_of_thisTransform_39(),
	SA_iTween_t1550711905_StaticFields::get_offset_of_U3CU3Ef__switchU24map2_40(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2580 = { sizeof (EasingFunction_t2379729698), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2581 = { sizeof (ApplyTween_t3100151106), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2582 = { sizeof (LoopType_t197962080)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2582[4] = 
{
	LoopType_t197962080::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2583 = { sizeof (NamedValueColor_t1220476254)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2583[5] = 
{
	NamedValueColor_t1220476254::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2584 = { sizeof (Defaults_t578422784), -1, sizeof(Defaults_t578422784_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2584[16] = 
{
	Defaults_t578422784_StaticFields::get_offset_of_time_0(),
	Defaults_t578422784_StaticFields::get_offset_of_delay_1(),
	Defaults_t578422784_StaticFields::get_offset_of_namedColorValue_2(),
	Defaults_t578422784_StaticFields::get_offset_of_loopType_3(),
	Defaults_t578422784_StaticFields::get_offset_of_easeType_4(),
	Defaults_t578422784_StaticFields::get_offset_of_lookSpeed_5(),
	Defaults_t578422784_StaticFields::get_offset_of_isLocal_6(),
	Defaults_t578422784_StaticFields::get_offset_of_space_7(),
	Defaults_t578422784_StaticFields::get_offset_of_orientToPath_8(),
	Defaults_t578422784_StaticFields::get_offset_of_color_9(),
	Defaults_t578422784_StaticFields::get_offset_of_updateTimePercentage_10(),
	Defaults_t578422784_StaticFields::get_offset_of_updateTime_11(),
	Defaults_t578422784_StaticFields::get_offset_of_cameraFadeDepth_12(),
	Defaults_t578422784_StaticFields::get_offset_of_lookAhead_13(),
	Defaults_t578422784_StaticFields::get_offset_of_useRealTime_14(),
	Defaults_t578422784_StaticFields::get_offset_of_up_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2585 = { sizeof (CRSpline_t2258853177), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2585[1] = 
{
	CRSpline_t2258853177::get_offset_of_pts_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2586 = { sizeof (U3CTweenDelayU3Ec__Iterator0_t2393992106), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2586[4] = 
{
	U3CTweenDelayU3Ec__Iterator0_t2393992106::get_offset_of_U24this_0(),
	U3CTweenDelayU3Ec__Iterator0_t2393992106::get_offset_of_U24current_1(),
	U3CTweenDelayU3Ec__Iterator0_t2393992106::get_offset_of_U24disposing_2(),
	U3CTweenDelayU3Ec__Iterator0_t2393992106::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2587 = { sizeof (U3CTweenRestartU3Ec__Iterator1_t3512026842), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2587[4] = 
{
	U3CTweenRestartU3Ec__Iterator1_t3512026842::get_offset_of_U24this_0(),
	U3CTweenRestartU3Ec__Iterator1_t3512026842::get_offset_of_U24current_1(),
	U3CTweenRestartU3Ec__Iterator1_t3512026842::get_offset_of_U24disposing_2(),
	U3CTweenRestartU3Ec__Iterator1_t3512026842::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2588 = { sizeof (U3CStartU3Ec__Iterator2_t3890060288), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2588[4] = 
{
	U3CStartU3Ec__Iterator2_t3890060288::get_offset_of_U24this_0(),
	U3CStartU3Ec__Iterator2_t3890060288::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator2_t3890060288::get_offset_of_U24disposing_2(),
	U3CStartU3Ec__Iterator2_t3890060288::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2589 = { sizeof (Error_t340543044), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2589[2] = 
{
	Error_t340543044::get_offset_of__Code_0(),
	Error_t340543044::get_offset_of__Messgae_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2590 = { sizeof (Invoker_t4214570440), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2590[1] = 
{
	Invoker_t4214570440::get_offset_of__callback_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2591 = { sizeof (PrefabAsyncLoader_t3137627864), -1, sizeof(PrefabAsyncLoader_t3137627864_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2591[3] = 
{
	PrefabAsyncLoader_t3137627864::get_offset_of_PrefabPath_2(),
	PrefabAsyncLoader_t3137627864::get_offset_of_ObjectLoadedAction_3(),
	PrefabAsyncLoader_t3137627864_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2592 = { sizeof (U3CLoadU3Ec__Iterator0_t2595626094), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2592[5] = 
{
	U3CLoadU3Ec__Iterator0_t2595626094::get_offset_of_U3CrequestU3E__0_0(),
	U3CLoadU3Ec__Iterator0_t2595626094::get_offset_of_U24this_1(),
	U3CLoadU3Ec__Iterator0_t2595626094::get_offset_of_U24current_2(),
	U3CLoadU3Ec__Iterator0_t2595626094::get_offset_of_U24disposing_3(),
	U3CLoadU3Ec__Iterator0_t2595626094::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2593 = { sizeof (Result_t4170401718), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2593[1] = 
{
	Result_t4170401718::get_offset_of__Error_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2594 = { sizeof (ScreenshotMaker_t2906260841), -1, sizeof(ScreenshotMaker_t2906260841_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2594[2] = 
{
	ScreenshotMaker_t2906260841::get_offset_of_OnScreenshotReady_2(),
	ScreenshotMaker_t2906260841_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2595 = { sizeof (U3CSaveScreenshotU3Ec__Iterator0_t367501136), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2595[7] = 
{
	U3CSaveScreenshotU3Ec__Iterator0_t367501136::get_offset_of_U3CwidthU3E__0_0(),
	U3CSaveScreenshotU3Ec__Iterator0_t367501136::get_offset_of_U3CheightU3E__0_1(),
	U3CSaveScreenshotU3Ec__Iterator0_t367501136::get_offset_of_U3CtexU3E__0_2(),
	U3CSaveScreenshotU3Ec__Iterator0_t367501136::get_offset_of_U24this_3(),
	U3CSaveScreenshotU3Ec__Iterator0_t367501136::get_offset_of_U24current_4(),
	U3CSaveScreenshotU3Ec__Iterator0_t367501136::get_offset_of_U24disposing_5(),
	U3CSaveScreenshotU3Ec__Iterator0_t367501136::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2596 = { sizeof (WWWTextureLoader_t1987695874), -1, sizeof(WWWTextureLoader_t1987695874_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2596[4] = 
{
	WWWTextureLoader_t1987695874_StaticFields::get_offset_of_LocalCache_2(),
	WWWTextureLoader_t1987695874::get_offset_of__url_3(),
	WWWTextureLoader_t1987695874::get_offset_of_OnLoad_4(),
	WWWTextureLoader_t1987695874_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2597 = { sizeof (U3CLoadCoroutinU3Ec__Iterator0_t2840076136), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2597[5] = 
{
	U3CLoadCoroutinU3Ec__Iterator0_t2840076136::get_offset_of_U3CwwwU3E__0_0(),
	U3CLoadCoroutinU3Ec__Iterator0_t2840076136::get_offset_of_U24this_1(),
	U3CLoadCoroutinU3Ec__Iterator0_t2840076136::get_offset_of_U24current_2(),
	U3CLoadCoroutinU3Ec__Iterator0_t2840076136::get_offset_of_U24disposing_3(),
	U3CLoadCoroutinU3Ec__Iterator0_t2840076136::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2598 = { sizeof (Config_t3619392629), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2598[26] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2599 = { sizeof (SA_TweenExtensions_t1686218418), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
