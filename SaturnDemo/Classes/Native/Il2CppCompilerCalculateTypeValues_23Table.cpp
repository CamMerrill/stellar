﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.String[]
struct StringU5BU5D_t1281789340;
// System.String
struct String_t;
// GK_Leaderboard
struct GK_Leaderboard_t591378496;
// UnityEditor.XCodeEditor.PBXDictionary
struct PBXDictionary_t2078602241;
// UnityEditor.XCodeEditor.PBXResolver/PBXResolverReverseIndex
struct PBXResolverReverseIndex_t3234151459;
// SA.IOSDeploy.Variable
struct Variable_t1035990833;
// UnityEditor.XCodeEditor.PBXGroup
struct PBXGroup_t2128436724;
// System.IO.FileInfo
struct FileInfo_t1169991790;
// UnityEditor.XCodeEditor.PBXSortedDictionary`1<UnityEditor.XCodeEditor.PBXBuildFile>
struct PBXSortedDictionary_1_t2756258120;
// UnityEditor.XCodeEditor.PBXSortedDictionary`1<UnityEditor.XCodeEditor.PBXGroup>
struct PBXSortedDictionary_1_t2274302150;
// UnityEditor.XCodeEditor.PBXSortedDictionary`1<UnityEditor.XCodeEditor.PBXFileReference>
struct PBXSortedDictionary_1_t637646383;
// UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXNativeTarget>
struct PBXDictionary_1_t3538108837;
// UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXFrameworksBuildPhase>
struct PBXDictionary_1_t125107989;
// UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXResourcesBuildPhase>
struct PBXDictionary_1_t1783080000;
// UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXShellScriptBuildPhase>
struct PBXDictionary_1_t1977435415;
// UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXSourcesBuildPhase>
struct PBXDictionary_1_t2932651977;
// UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXCopyFilesBuildPhase>
struct PBXDictionary_1_t2617057394;
// UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXVariantGroup>
struct PBXDictionary_1_t1414031086;
// UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.XCBuildConfiguration>
struct PBXDictionary_1_t4195780175;
// UnityEditor.XCodeEditor.PBXSortedDictionary`1<UnityEditor.XCodeEditor.XCConfigurationList>
struct PBXSortedDictionary_1_t1246395450;
// UnityEditor.XCodeEditor.PBXProject
struct PBXProject_t4035248171;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.Collections.ArrayList
struct ArrayList_t2718874744;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// System.Collections.Generic.List`1<System.Byte>
struct List_1_t2606371118;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// UnityEngine.Object
struct Object_t631007953;
// System.Char[]
struct CharU5BU5D_t3528271667;
// UnityEditor.XCodeEditor.PBXResolver
struct PBXResolver_t144121547;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// GK_Player
struct GK_Player_t3461352791;
// System.Collections.Generic.RBTree
struct RBTree_t4095273678;
// System.Collections.Generic.SortedDictionary`2/NodeHelper<System.String,System.Object>
struct NodeHelper_t2003304144;
// SA.Common.Models.Error
struct Error_t340543044;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t964245573;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t3954782707;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t950877179;
// System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.String,System.Collections.DictionaryEntry>
struct Transform_1_t132201056;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// GK_ScoreCollection
struct GK_ScoreCollection_t3739344104;
// System.Collections.Generic.List`1<GK_Score>
struct List_1_t1687396993;
// System.Collections.Generic.Dictionary`2<System.Int32,GK_LocalPlayerScoreUpdateListener>
struct Dictionary_2_t3801404807;
// GK_LeaderBoardInfo
struct GK_LeaderBoardInfo_t323632070;
// System.Collections.Generic.List`1<GK_LeaderBoardInfo>
struct List_1_t1795706812;
// System.Action`1<ISN_LoadSetLeaderboardsInfoResult>
struct Action_1_t1927089056;
// System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.Object,System.Collections.DictionaryEntry>
struct Transform_1_t1694351041;
// System.Action`1<GK_UserPhotoLoadResult>
struct Action_1_t4091228422;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Texture2D>
struct Dictionary_2_t3625702484;
// System.Void
struct Void_t1185182177;
// GK_LeaderboardSet
struct GK_LeaderboardSet_t3182444842;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// GK_AchievementTemplate
struct GK_AchievementTemplate_t3624697896;
// System.Collections.Generic.Dictionary`2<UnityEditor.XCodeEditor.TreeEnum,System.String>
struct Dictionary_2_t1851544083;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t1632706988;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t899420910;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t4072576034;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t3628304265;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t496136383;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// UnityEngine.Collider2D
struct Collider2D_t2806799626;
// UnityEngine.Collider
struct Collider_t1773347010;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t1690781147;
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t4286651560;
// System.Collections.Generic.List`1<SA.IOSNative.StoreKit.Product>
struct List_1_t294466124;
// System.Collections.Generic.List`1<GK_Leaderboard>
struct List_1_t2063453238;
// System.Collections.Generic.List`1<GK_AchievementTemplate>
struct List_1_t801805342;
// System.Collections.Generic.List`1<SA.IOSNative.Models.UrlType>
struct List_1_t4189911559;
// System.Collections.Generic.List`1<SA.IOSNative.Gestures.ForceTouchMenuItem>
struct List_1_t1585227261;
// System.Collections.Generic.List`1<SA.IOSDeploy.Framework>
struct List_1_t572364847;
// System.Collections.Generic.List`1<SA.IOSDeploy.Lib>
struct List_1_t1034763099;
// System.Collections.Generic.List`1<SA.IOSDeploy.Flag>
struct List_1_t554530830;
// System.Collections.Generic.List`1<SA.IOSDeploy.Variable>
struct List_1_t2508065575;
// System.Collections.Generic.List`1<SA.IOSDeploy.VariableId>
struct List_1_t1694503567;
// System.Collections.Generic.List`1<SA.IOSDeploy.AssetFile>
struct List_1_t1536090105;
// GameCenter_RTM
struct GameCenter_RTM_t1714402562;
// ISN_SoomlaGrow
struct ISN_SoomlaGrow_t35334059;
// GameCenter_TBM
struct GameCenter_TBM_t2861413648;
// GameCenterInvitations
struct GameCenterInvitations_t630122588;
// System.Action`1<SA.Common.Models.Result>
struct Action_1_t47902017;
// System.Action`1<GK_LeaderboardResult>
struct Action_1_t1630030377;
// System.Action`1<GK_AchievementProgressResult>
struct Action_1_t623946033;
// System.Action
struct Action_t1264377477;
// System.Action`1<GK_UserInfoLoadResult>
struct Action_1_t3760022062;
// System.Action`1<GK_PlayerSignatureResult>
struct Action_1_t1202011752;
// System.Collections.Generic.Dictionary`2<System.String,GK_Player>
struct Dictionary_2_t3246609090;
// System.Collections.Generic.List`1<GK_LeaderboardSet>
struct List_1_t359552288;
// System.Collections.Generic.Dictionary`2<System.Int32,GK_FriendRequest>
struct Dictionary_2_t1386088385;
// ISN_GameSaves
struct ISN_GameSaves_t3595065519;
// SA.IOSNative.Core.AppController
struct AppController_t1810425167;
// System.Action`1<GK_SaveRemoveResult>
struct Action_1_t1435461071;
// System.Action`1<GK_SaveResult>
struct Action_1_t497746395;
// System.Action`1<GK_FetchResult>
struct Action_1_t254498298;
// System.Action`1<GK_SavesResolveResult>
struct Action_1_t4133968337;
// System.Collections.Generic.Dictionary`2<System.String,GK_SavedGame>
struct Dictionary_2_t3230328059;
// System.Action`1<SA.IOSNative.Models.LaunchUrl>
struct Action_1_t1427027165;
// System.Action`1<SA.IOSNative.Models.UniversalLink>
struct Action_1_t4277050132;
// GK_RTM_Match
struct GK_RTM_Match_t949836071;
// System.Action`1<GK_RTM_MatchStartedResult>
struct Action_1_t3750106405;
// System.Action`1<SA.Common.Models.Error>
struct Action_1_t513010639;
// System.Action`2<GK_Player,System.Boolean>
struct Action_2_t3922627552;
// System.Action`1<GK_RTM_QueryActivityResult>
struct Action_1_t3818092299;
// System.Action`2<GK_Player,System.Byte[]>
struct Action_2_t3647019948;
// System.Action`3<GK_Player,GK_PlayerConnectionState,GK_RTM_Match>
struct Action_3_t2719518085;
// System.Action`1<GK_Player>
struct Action_1_t3633820386;
// System.Action`1<GK_TBM_LoadMatchResult>
struct Action_1_t1401582932;
// System.Action`1<GK_TBM_LoadMatchesResult>
struct Action_1_t101443120;
// System.Action`1<GK_TBM_MatchDataUpdateResult>
struct Action_1_t224071095;
// System.Action`1<GK_TBM_MatchInitResult>
struct Action_1_t1935478288;
// System.Action`1<GK_TBM_MatchQuitResult>
struct Action_1_t1555055752;
// System.Action`1<GK_TBM_EndTrunResult>
struct Action_1_t3655562227;
// System.Action`1<GK_TBM_MatchEndResult>
struct Action_1_t3909832116;
// System.Action`1<GK_TBM_RematchResult>
struct Action_1_t1218639671;
// System.Action`1<GK_TBM_MatchRemovedResult>
struct Action_1_t565105849;
// System.Action`1<GK_TBM_Match>
struct Action_1_t256049476;
// System.Action`1<GK_TBM_MatchTurnResult>
struct Action_1_t2448326972;
// System.Collections.Generic.Dictionary`2<System.String,GK_TBM_Match>
struct Dictionary_2_t4163805476;
// System.Action`2<GK_Player,GK_InviteRecipientResponse>
struct Action_2_t923696397;
// System.Action`2<GK_MatchType,GK_Invite>
struct Action_2_t493924140;
// System.Action`3<GK_MatchType,System.String[],GK_Player[]>
struct Action_3_t4118644065;
// UnityEngine.UI.Graphic
struct Graphic_t1660335611;




#ifndef U3CMODULEU3E_T692745548_H
#define U3CMODULEU3E_T692745548_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745548 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745548_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CGETIMPORTEDFRAMEWORKSU3EC__ANONSTOREY0_T3484212793_H
#define U3CGETIMPORTEDFRAMEWORKSU3EC__ANONSTOREY0_T3484212793_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSDeploy.ISD_FrameworkHandler/<GetImportedFrameworks>c__AnonStorey0
struct  U3CGetImportedFrameworksU3Ec__AnonStorey0_t3484212793  : public RuntimeObject
{
public:
	// System.String[] SA.IOSDeploy.ISD_FrameworkHandler/<GetImportedFrameworks>c__AnonStorey0::dirrExtensions
	StringU5BU5D_t1281789340* ___dirrExtensions_0;

public:
	inline static int32_t get_offset_of_dirrExtensions_0() { return static_cast<int32_t>(offsetof(U3CGetImportedFrameworksU3Ec__AnonStorey0_t3484212793, ___dirrExtensions_0)); }
	inline StringU5BU5D_t1281789340* get_dirrExtensions_0() const { return ___dirrExtensions_0; }
	inline StringU5BU5D_t1281789340** get_address_of_dirrExtensions_0() { return &___dirrExtensions_0; }
	inline void set_dirrExtensions_0(StringU5BU5D_t1281789340* value)
	{
		___dirrExtensions_0 = value;
		Il2CppCodeGenWriteBarrier((&___dirrExtensions_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETIMPORTEDFRAMEWORKSU3EC__ANONSTOREY0_T3484212793_H
#ifndef U3CLOADLEADERBOARDINFOLOCALU3EC__ITERATOR0_T812827298_H
#define U3CLOADLEADERBOARDINFOLOCALU3EC__ITERATOR0_T812827298_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameCenterManager/<LoadLeaderboardInfoLocal>c__Iterator0
struct  U3CLoadLeaderboardInfoLocalU3Ec__Iterator0_t812827298  : public RuntimeObject
{
public:
	// System.Int32 GameCenterManager/<LoadLeaderboardInfoLocal>c__Iterator0::<requestId>__0
	int32_t ___U3CrequestIdU3E__0_0;
	// System.String GameCenterManager/<LoadLeaderboardInfoLocal>c__Iterator0::leaderboardId
	String_t* ___leaderboardId_1;
	// GK_Leaderboard GameCenterManager/<LoadLeaderboardInfoLocal>c__Iterator0::<leaderboard>__0
	GK_Leaderboard_t591378496 * ___U3CleaderboardU3E__0_2;
	// System.Object GameCenterManager/<LoadLeaderboardInfoLocal>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean GameCenterManager/<LoadLeaderboardInfoLocal>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 GameCenterManager/<LoadLeaderboardInfoLocal>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CrequestIdU3E__0_0() { return static_cast<int32_t>(offsetof(U3CLoadLeaderboardInfoLocalU3Ec__Iterator0_t812827298, ___U3CrequestIdU3E__0_0)); }
	inline int32_t get_U3CrequestIdU3E__0_0() const { return ___U3CrequestIdU3E__0_0; }
	inline int32_t* get_address_of_U3CrequestIdU3E__0_0() { return &___U3CrequestIdU3E__0_0; }
	inline void set_U3CrequestIdU3E__0_0(int32_t value)
	{
		___U3CrequestIdU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_leaderboardId_1() { return static_cast<int32_t>(offsetof(U3CLoadLeaderboardInfoLocalU3Ec__Iterator0_t812827298, ___leaderboardId_1)); }
	inline String_t* get_leaderboardId_1() const { return ___leaderboardId_1; }
	inline String_t** get_address_of_leaderboardId_1() { return &___leaderboardId_1; }
	inline void set_leaderboardId_1(String_t* value)
	{
		___leaderboardId_1 = value;
		Il2CppCodeGenWriteBarrier((&___leaderboardId_1), value);
	}

	inline static int32_t get_offset_of_U3CleaderboardU3E__0_2() { return static_cast<int32_t>(offsetof(U3CLoadLeaderboardInfoLocalU3Ec__Iterator0_t812827298, ___U3CleaderboardU3E__0_2)); }
	inline GK_Leaderboard_t591378496 * get_U3CleaderboardU3E__0_2() const { return ___U3CleaderboardU3E__0_2; }
	inline GK_Leaderboard_t591378496 ** get_address_of_U3CleaderboardU3E__0_2() { return &___U3CleaderboardU3E__0_2; }
	inline void set_U3CleaderboardU3E__0_2(GK_Leaderboard_t591378496 * value)
	{
		___U3CleaderboardU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CleaderboardU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CLoadLeaderboardInfoLocalU3Ec__Iterator0_t812827298, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CLoadLeaderboardInfoLocalU3Ec__Iterator0_t812827298, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CLoadLeaderboardInfoLocalU3Ec__Iterator0_t812827298, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADLEADERBOARDINFOLOCALU3EC__ITERATOR0_T812827298_H
#ifndef PBXRESOLVER_T144121547_H
#define PBXRESOLVER_T144121547_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.PBXResolver
struct  PBXResolver_t144121547  : public RuntimeObject
{
public:
	// UnityEditor.XCodeEditor.PBXDictionary UnityEditor.XCodeEditor.PBXResolver::objects
	PBXDictionary_t2078602241 * ___objects_0;
	// System.String UnityEditor.XCodeEditor.PBXResolver::rootObject
	String_t* ___rootObject_1;
	// UnityEditor.XCodeEditor.PBXResolver/PBXResolverReverseIndex UnityEditor.XCodeEditor.PBXResolver::index
	PBXResolverReverseIndex_t3234151459 * ___index_2;

public:
	inline static int32_t get_offset_of_objects_0() { return static_cast<int32_t>(offsetof(PBXResolver_t144121547, ___objects_0)); }
	inline PBXDictionary_t2078602241 * get_objects_0() const { return ___objects_0; }
	inline PBXDictionary_t2078602241 ** get_address_of_objects_0() { return &___objects_0; }
	inline void set_objects_0(PBXDictionary_t2078602241 * value)
	{
		___objects_0 = value;
		Il2CppCodeGenWriteBarrier((&___objects_0), value);
	}

	inline static int32_t get_offset_of_rootObject_1() { return static_cast<int32_t>(offsetof(PBXResolver_t144121547, ___rootObject_1)); }
	inline String_t* get_rootObject_1() const { return ___rootObject_1; }
	inline String_t** get_address_of_rootObject_1() { return &___rootObject_1; }
	inline void set_rootObject_1(String_t* value)
	{
		___rootObject_1 = value;
		Il2CppCodeGenWriteBarrier((&___rootObject_1), value);
	}

	inline static int32_t get_offset_of_index_2() { return static_cast<int32_t>(offsetof(PBXResolver_t144121547, ___index_2)); }
	inline PBXResolverReverseIndex_t3234151459 * get_index_2() const { return ___index_2; }
	inline PBXResolverReverseIndex_t3234151459 ** get_address_of_index_2() { return &___index_2; }
	inline void set_index_2(PBXResolverReverseIndex_t3234151459 * value)
	{
		___index_2 = value;
		Il2CppCodeGenWriteBarrier((&___index_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBXRESOLVER_T144121547_H
#ifndef U3CGETIMPORTEDLIBRARIESU3EC__ANONSTOREY1_T3892267358_H
#define U3CGETIMPORTEDLIBRARIESU3EC__ANONSTOREY1_T3892267358_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSDeploy.ISD_FrameworkHandler/<GetImportedLibraries>c__AnonStorey1
struct  U3CGetImportedLibrariesU3Ec__AnonStorey1_t3892267358  : public RuntimeObject
{
public:
	// System.String[] SA.IOSDeploy.ISD_FrameworkHandler/<GetImportedLibraries>c__AnonStorey1::fileExtensions
	StringU5BU5D_t1281789340* ___fileExtensions_0;

public:
	inline static int32_t get_offset_of_fileExtensions_0() { return static_cast<int32_t>(offsetof(U3CGetImportedLibrariesU3Ec__AnonStorey1_t3892267358, ___fileExtensions_0)); }
	inline StringU5BU5D_t1281789340* get_fileExtensions_0() const { return ___fileExtensions_0; }
	inline StringU5BU5D_t1281789340** get_address_of_fileExtensions_0() { return &___fileExtensions_0; }
	inline void set_fileExtensions_0(StringU5BU5D_t1281789340* value)
	{
		___fileExtensions_0 = value;
		Il2CppCodeGenWriteBarrier((&___fileExtensions_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETIMPORTEDLIBRARIESU3EC__ANONSTOREY1_T3892267358_H
#ifndef VARIABLEID_T222428825_H
#define VARIABLEID_T222428825_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSDeploy.VariableId
struct  VariableId_t222428825  : public RuntimeObject
{
public:
	// SA.IOSDeploy.Variable SA.IOSDeploy.VariableId::VariableValue
	Variable_t1035990833 * ___VariableValue_0;
	// System.String SA.IOSDeploy.VariableId::uniqueIdKey
	String_t* ___uniqueIdKey_1;

public:
	inline static int32_t get_offset_of_VariableValue_0() { return static_cast<int32_t>(offsetof(VariableId_t222428825, ___VariableValue_0)); }
	inline Variable_t1035990833 * get_VariableValue_0() const { return ___VariableValue_0; }
	inline Variable_t1035990833 ** get_address_of_VariableValue_0() { return &___VariableValue_0; }
	inline void set_VariableValue_0(Variable_t1035990833 * value)
	{
		___VariableValue_0 = value;
		Il2CppCodeGenWriteBarrier((&___VariableValue_0), value);
	}

	inline static int32_t get_offset_of_uniqueIdKey_1() { return static_cast<int32_t>(offsetof(VariableId_t222428825, ___uniqueIdKey_1)); }
	inline String_t* get_uniqueIdKey_1() const { return ___uniqueIdKey_1; }
	inline String_t** get_address_of_uniqueIdKey_1() { return &___uniqueIdKey_1; }
	inline void set_uniqueIdKey_1(String_t* value)
	{
		___uniqueIdKey_1 = value;
		Il2CppCodeGenWriteBarrier((&___uniqueIdKey_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VARIABLEID_T222428825_H
#ifndef MINIJSON_T211875846_H
#define MINIJSON_T211875846_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// XUPorterJSON.MiniJSON
struct  MiniJSON_t211875846  : public RuntimeObject
{
public:

public:
};

struct MiniJSON_t211875846_StaticFields
{
public:
	// System.Int32 XUPorterJSON.MiniJSON::lastErrorIndex
	int32_t ___lastErrorIndex_13;
	// System.String XUPorterJSON.MiniJSON::lastDecode
	String_t* ___lastDecode_14;

public:
	inline static int32_t get_offset_of_lastErrorIndex_13() { return static_cast<int32_t>(offsetof(MiniJSON_t211875846_StaticFields, ___lastErrorIndex_13)); }
	inline int32_t get_lastErrorIndex_13() const { return ___lastErrorIndex_13; }
	inline int32_t* get_address_of_lastErrorIndex_13() { return &___lastErrorIndex_13; }
	inline void set_lastErrorIndex_13(int32_t value)
	{
		___lastErrorIndex_13 = value;
	}

	inline static int32_t get_offset_of_lastDecode_14() { return static_cast<int32_t>(offsetof(MiniJSON_t211875846_StaticFields, ___lastDecode_14)); }
	inline String_t* get_lastDecode_14() const { return ___lastDecode_14; }
	inline String_t** get_address_of_lastDecode_14() { return &___lastDecode_14; }
	inline void set_lastDecode_14(String_t* value)
	{
		___lastDecode_14 = value;
		Il2CppCodeGenWriteBarrier((&___lastDecode_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINIJSON_T211875846_H
#ifndef MINIJSONEXTENSIONS_T1989422878_H
#define MINIJSONEXTENSIONS_T1989422878_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// XUPorterJSON.MiniJsonExtensions
struct  MiniJsonExtensions_t1989422878  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINIJSONEXTENSIONS_T1989422878_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef XCPROJECT_T3157646134_H
#define XCPROJECT_T3157646134_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.XCProject
struct  XCProject_t3157646134  : public RuntimeObject
{
public:
	// UnityEditor.XCodeEditor.PBXDictionary UnityEditor.XCodeEditor.XCProject::_datastore
	PBXDictionary_t2078602241 * ____datastore_0;
	// UnityEditor.XCodeEditor.PBXDictionary UnityEditor.XCodeEditor.XCProject::_objects
	PBXDictionary_t2078602241 * ____objects_1;
	// UnityEditor.XCodeEditor.PBXGroup UnityEditor.XCodeEditor.XCProject::_rootGroup
	PBXGroup_t2128436724 * ____rootGroup_2;
	// System.String UnityEditor.XCodeEditor.XCProject::_rootObjectKey
	String_t* ____rootObjectKey_3;
	// System.String UnityEditor.XCodeEditor.XCProject::<projectRootPath>k__BackingField
	String_t* ___U3CprojectRootPathU3Ek__BackingField_4;
	// System.IO.FileInfo UnityEditor.XCodeEditor.XCProject::projectFileInfo
	FileInfo_t1169991790 * ___projectFileInfo_5;
	// System.String UnityEditor.XCodeEditor.XCProject::<filePath>k__BackingField
	String_t* ___U3CfilePathU3Ek__BackingField_6;
	// System.Boolean UnityEditor.XCodeEditor.XCProject::modified
	bool ___modified_7;
	// UnityEditor.XCodeEditor.PBXSortedDictionary`1<UnityEditor.XCodeEditor.PBXBuildFile> UnityEditor.XCodeEditor.XCProject::_buildFiles
	PBXSortedDictionary_1_t2756258120 * ____buildFiles_8;
	// UnityEditor.XCodeEditor.PBXSortedDictionary`1<UnityEditor.XCodeEditor.PBXGroup> UnityEditor.XCodeEditor.XCProject::_groups
	PBXSortedDictionary_1_t2274302150 * ____groups_9;
	// UnityEditor.XCodeEditor.PBXSortedDictionary`1<UnityEditor.XCodeEditor.PBXFileReference> UnityEditor.XCodeEditor.XCProject::_fileReferences
	PBXSortedDictionary_1_t637646383 * ____fileReferences_10;
	// UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXNativeTarget> UnityEditor.XCodeEditor.XCProject::_nativeTargets
	PBXDictionary_1_t3538108837 * ____nativeTargets_11;
	// UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXFrameworksBuildPhase> UnityEditor.XCodeEditor.XCProject::_frameworkBuildPhases
	PBXDictionary_1_t125107989 * ____frameworkBuildPhases_12;
	// UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXResourcesBuildPhase> UnityEditor.XCodeEditor.XCProject::_resourcesBuildPhases
	PBXDictionary_1_t1783080000 * ____resourcesBuildPhases_13;
	// UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXShellScriptBuildPhase> UnityEditor.XCodeEditor.XCProject::_shellScriptBuildPhases
	PBXDictionary_1_t1977435415 * ____shellScriptBuildPhases_14;
	// UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXSourcesBuildPhase> UnityEditor.XCodeEditor.XCProject::_sourcesBuildPhases
	PBXDictionary_1_t2932651977 * ____sourcesBuildPhases_15;
	// UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXCopyFilesBuildPhase> UnityEditor.XCodeEditor.XCProject::_copyBuildPhases
	PBXDictionary_1_t2617057394 * ____copyBuildPhases_16;
	// UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXVariantGroup> UnityEditor.XCodeEditor.XCProject::_variantGroups
	PBXDictionary_1_t1414031086 * ____variantGroups_17;
	// UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.XCBuildConfiguration> UnityEditor.XCodeEditor.XCProject::_buildConfigurations
	PBXDictionary_1_t4195780175 * ____buildConfigurations_18;
	// UnityEditor.XCodeEditor.PBXSortedDictionary`1<UnityEditor.XCodeEditor.XCConfigurationList> UnityEditor.XCodeEditor.XCProject::_configurationLists
	PBXSortedDictionary_1_t1246395450 * ____configurationLists_19;
	// UnityEditor.XCodeEditor.PBXProject UnityEditor.XCodeEditor.XCProject::_project
	PBXProject_t4035248171 * ____project_20;

public:
	inline static int32_t get_offset_of__datastore_0() { return static_cast<int32_t>(offsetof(XCProject_t3157646134, ____datastore_0)); }
	inline PBXDictionary_t2078602241 * get__datastore_0() const { return ____datastore_0; }
	inline PBXDictionary_t2078602241 ** get_address_of__datastore_0() { return &____datastore_0; }
	inline void set__datastore_0(PBXDictionary_t2078602241 * value)
	{
		____datastore_0 = value;
		Il2CppCodeGenWriteBarrier((&____datastore_0), value);
	}

	inline static int32_t get_offset_of__objects_1() { return static_cast<int32_t>(offsetof(XCProject_t3157646134, ____objects_1)); }
	inline PBXDictionary_t2078602241 * get__objects_1() const { return ____objects_1; }
	inline PBXDictionary_t2078602241 ** get_address_of__objects_1() { return &____objects_1; }
	inline void set__objects_1(PBXDictionary_t2078602241 * value)
	{
		____objects_1 = value;
		Il2CppCodeGenWriteBarrier((&____objects_1), value);
	}

	inline static int32_t get_offset_of__rootGroup_2() { return static_cast<int32_t>(offsetof(XCProject_t3157646134, ____rootGroup_2)); }
	inline PBXGroup_t2128436724 * get__rootGroup_2() const { return ____rootGroup_2; }
	inline PBXGroup_t2128436724 ** get_address_of__rootGroup_2() { return &____rootGroup_2; }
	inline void set__rootGroup_2(PBXGroup_t2128436724 * value)
	{
		____rootGroup_2 = value;
		Il2CppCodeGenWriteBarrier((&____rootGroup_2), value);
	}

	inline static int32_t get_offset_of__rootObjectKey_3() { return static_cast<int32_t>(offsetof(XCProject_t3157646134, ____rootObjectKey_3)); }
	inline String_t* get__rootObjectKey_3() const { return ____rootObjectKey_3; }
	inline String_t** get_address_of__rootObjectKey_3() { return &____rootObjectKey_3; }
	inline void set__rootObjectKey_3(String_t* value)
	{
		____rootObjectKey_3 = value;
		Il2CppCodeGenWriteBarrier((&____rootObjectKey_3), value);
	}

	inline static int32_t get_offset_of_U3CprojectRootPathU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(XCProject_t3157646134, ___U3CprojectRootPathU3Ek__BackingField_4)); }
	inline String_t* get_U3CprojectRootPathU3Ek__BackingField_4() const { return ___U3CprojectRootPathU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CprojectRootPathU3Ek__BackingField_4() { return &___U3CprojectRootPathU3Ek__BackingField_4; }
	inline void set_U3CprojectRootPathU3Ek__BackingField_4(String_t* value)
	{
		___U3CprojectRootPathU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CprojectRootPathU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_projectFileInfo_5() { return static_cast<int32_t>(offsetof(XCProject_t3157646134, ___projectFileInfo_5)); }
	inline FileInfo_t1169991790 * get_projectFileInfo_5() const { return ___projectFileInfo_5; }
	inline FileInfo_t1169991790 ** get_address_of_projectFileInfo_5() { return &___projectFileInfo_5; }
	inline void set_projectFileInfo_5(FileInfo_t1169991790 * value)
	{
		___projectFileInfo_5 = value;
		Il2CppCodeGenWriteBarrier((&___projectFileInfo_5), value);
	}

	inline static int32_t get_offset_of_U3CfilePathU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(XCProject_t3157646134, ___U3CfilePathU3Ek__BackingField_6)); }
	inline String_t* get_U3CfilePathU3Ek__BackingField_6() const { return ___U3CfilePathU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CfilePathU3Ek__BackingField_6() { return &___U3CfilePathU3Ek__BackingField_6; }
	inline void set_U3CfilePathU3Ek__BackingField_6(String_t* value)
	{
		___U3CfilePathU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CfilePathU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_modified_7() { return static_cast<int32_t>(offsetof(XCProject_t3157646134, ___modified_7)); }
	inline bool get_modified_7() const { return ___modified_7; }
	inline bool* get_address_of_modified_7() { return &___modified_7; }
	inline void set_modified_7(bool value)
	{
		___modified_7 = value;
	}

	inline static int32_t get_offset_of__buildFiles_8() { return static_cast<int32_t>(offsetof(XCProject_t3157646134, ____buildFiles_8)); }
	inline PBXSortedDictionary_1_t2756258120 * get__buildFiles_8() const { return ____buildFiles_8; }
	inline PBXSortedDictionary_1_t2756258120 ** get_address_of__buildFiles_8() { return &____buildFiles_8; }
	inline void set__buildFiles_8(PBXSortedDictionary_1_t2756258120 * value)
	{
		____buildFiles_8 = value;
		Il2CppCodeGenWriteBarrier((&____buildFiles_8), value);
	}

	inline static int32_t get_offset_of__groups_9() { return static_cast<int32_t>(offsetof(XCProject_t3157646134, ____groups_9)); }
	inline PBXSortedDictionary_1_t2274302150 * get__groups_9() const { return ____groups_9; }
	inline PBXSortedDictionary_1_t2274302150 ** get_address_of__groups_9() { return &____groups_9; }
	inline void set__groups_9(PBXSortedDictionary_1_t2274302150 * value)
	{
		____groups_9 = value;
		Il2CppCodeGenWriteBarrier((&____groups_9), value);
	}

	inline static int32_t get_offset_of__fileReferences_10() { return static_cast<int32_t>(offsetof(XCProject_t3157646134, ____fileReferences_10)); }
	inline PBXSortedDictionary_1_t637646383 * get__fileReferences_10() const { return ____fileReferences_10; }
	inline PBXSortedDictionary_1_t637646383 ** get_address_of__fileReferences_10() { return &____fileReferences_10; }
	inline void set__fileReferences_10(PBXSortedDictionary_1_t637646383 * value)
	{
		____fileReferences_10 = value;
		Il2CppCodeGenWriteBarrier((&____fileReferences_10), value);
	}

	inline static int32_t get_offset_of__nativeTargets_11() { return static_cast<int32_t>(offsetof(XCProject_t3157646134, ____nativeTargets_11)); }
	inline PBXDictionary_1_t3538108837 * get__nativeTargets_11() const { return ____nativeTargets_11; }
	inline PBXDictionary_1_t3538108837 ** get_address_of__nativeTargets_11() { return &____nativeTargets_11; }
	inline void set__nativeTargets_11(PBXDictionary_1_t3538108837 * value)
	{
		____nativeTargets_11 = value;
		Il2CppCodeGenWriteBarrier((&____nativeTargets_11), value);
	}

	inline static int32_t get_offset_of__frameworkBuildPhases_12() { return static_cast<int32_t>(offsetof(XCProject_t3157646134, ____frameworkBuildPhases_12)); }
	inline PBXDictionary_1_t125107989 * get__frameworkBuildPhases_12() const { return ____frameworkBuildPhases_12; }
	inline PBXDictionary_1_t125107989 ** get_address_of__frameworkBuildPhases_12() { return &____frameworkBuildPhases_12; }
	inline void set__frameworkBuildPhases_12(PBXDictionary_1_t125107989 * value)
	{
		____frameworkBuildPhases_12 = value;
		Il2CppCodeGenWriteBarrier((&____frameworkBuildPhases_12), value);
	}

	inline static int32_t get_offset_of__resourcesBuildPhases_13() { return static_cast<int32_t>(offsetof(XCProject_t3157646134, ____resourcesBuildPhases_13)); }
	inline PBXDictionary_1_t1783080000 * get__resourcesBuildPhases_13() const { return ____resourcesBuildPhases_13; }
	inline PBXDictionary_1_t1783080000 ** get_address_of__resourcesBuildPhases_13() { return &____resourcesBuildPhases_13; }
	inline void set__resourcesBuildPhases_13(PBXDictionary_1_t1783080000 * value)
	{
		____resourcesBuildPhases_13 = value;
		Il2CppCodeGenWriteBarrier((&____resourcesBuildPhases_13), value);
	}

	inline static int32_t get_offset_of__shellScriptBuildPhases_14() { return static_cast<int32_t>(offsetof(XCProject_t3157646134, ____shellScriptBuildPhases_14)); }
	inline PBXDictionary_1_t1977435415 * get__shellScriptBuildPhases_14() const { return ____shellScriptBuildPhases_14; }
	inline PBXDictionary_1_t1977435415 ** get_address_of__shellScriptBuildPhases_14() { return &____shellScriptBuildPhases_14; }
	inline void set__shellScriptBuildPhases_14(PBXDictionary_1_t1977435415 * value)
	{
		____shellScriptBuildPhases_14 = value;
		Il2CppCodeGenWriteBarrier((&____shellScriptBuildPhases_14), value);
	}

	inline static int32_t get_offset_of__sourcesBuildPhases_15() { return static_cast<int32_t>(offsetof(XCProject_t3157646134, ____sourcesBuildPhases_15)); }
	inline PBXDictionary_1_t2932651977 * get__sourcesBuildPhases_15() const { return ____sourcesBuildPhases_15; }
	inline PBXDictionary_1_t2932651977 ** get_address_of__sourcesBuildPhases_15() { return &____sourcesBuildPhases_15; }
	inline void set__sourcesBuildPhases_15(PBXDictionary_1_t2932651977 * value)
	{
		____sourcesBuildPhases_15 = value;
		Il2CppCodeGenWriteBarrier((&____sourcesBuildPhases_15), value);
	}

	inline static int32_t get_offset_of__copyBuildPhases_16() { return static_cast<int32_t>(offsetof(XCProject_t3157646134, ____copyBuildPhases_16)); }
	inline PBXDictionary_1_t2617057394 * get__copyBuildPhases_16() const { return ____copyBuildPhases_16; }
	inline PBXDictionary_1_t2617057394 ** get_address_of__copyBuildPhases_16() { return &____copyBuildPhases_16; }
	inline void set__copyBuildPhases_16(PBXDictionary_1_t2617057394 * value)
	{
		____copyBuildPhases_16 = value;
		Il2CppCodeGenWriteBarrier((&____copyBuildPhases_16), value);
	}

	inline static int32_t get_offset_of__variantGroups_17() { return static_cast<int32_t>(offsetof(XCProject_t3157646134, ____variantGroups_17)); }
	inline PBXDictionary_1_t1414031086 * get__variantGroups_17() const { return ____variantGroups_17; }
	inline PBXDictionary_1_t1414031086 ** get_address_of__variantGroups_17() { return &____variantGroups_17; }
	inline void set__variantGroups_17(PBXDictionary_1_t1414031086 * value)
	{
		____variantGroups_17 = value;
		Il2CppCodeGenWriteBarrier((&____variantGroups_17), value);
	}

	inline static int32_t get_offset_of__buildConfigurations_18() { return static_cast<int32_t>(offsetof(XCProject_t3157646134, ____buildConfigurations_18)); }
	inline PBXDictionary_1_t4195780175 * get__buildConfigurations_18() const { return ____buildConfigurations_18; }
	inline PBXDictionary_1_t4195780175 ** get_address_of__buildConfigurations_18() { return &____buildConfigurations_18; }
	inline void set__buildConfigurations_18(PBXDictionary_1_t4195780175 * value)
	{
		____buildConfigurations_18 = value;
		Il2CppCodeGenWriteBarrier((&____buildConfigurations_18), value);
	}

	inline static int32_t get_offset_of__configurationLists_19() { return static_cast<int32_t>(offsetof(XCProject_t3157646134, ____configurationLists_19)); }
	inline PBXSortedDictionary_1_t1246395450 * get__configurationLists_19() const { return ____configurationLists_19; }
	inline PBXSortedDictionary_1_t1246395450 ** get_address_of__configurationLists_19() { return &____configurationLists_19; }
	inline void set__configurationLists_19(PBXSortedDictionary_1_t1246395450 * value)
	{
		____configurationLists_19 = value;
		Il2CppCodeGenWriteBarrier((&____configurationLists_19), value);
	}

	inline static int32_t get_offset_of__project_20() { return static_cast<int32_t>(offsetof(XCProject_t3157646134, ____project_20)); }
	inline PBXProject_t4035248171 * get__project_20() const { return ____project_20; }
	inline PBXProject_t4035248171 ** get_address_of__project_20() { return &____project_20; }
	inline void set__project_20(PBXProject_t4035248171 * value)
	{
		____project_20 = value;
		Il2CppCodeGenWriteBarrier((&____project_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XCPROJECT_T3157646134_H
#ifndef XCPLIST_T2540119850_H
#define XCPLIST_T2540119850_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.XCPlist
struct  XCPlist_t2540119850  : public RuntimeObject
{
public:
	// System.String UnityEditor.XCodeEditor.XCPlist::plistPath
	String_t* ___plistPath_0;
	// System.Boolean UnityEditor.XCodeEditor.XCPlist::plistModified
	bool ___plistModified_1;

public:
	inline static int32_t get_offset_of_plistPath_0() { return static_cast<int32_t>(offsetof(XCPlist_t2540119850, ___plistPath_0)); }
	inline String_t* get_plistPath_0() const { return ___plistPath_0; }
	inline String_t** get_address_of_plistPath_0() { return &___plistPath_0; }
	inline void set_plistPath_0(String_t* value)
	{
		___plistPath_0 = value;
		Il2CppCodeGenWriteBarrier((&___plistPath_0), value);
	}

	inline static int32_t get_offset_of_plistModified_1() { return static_cast<int32_t>(offsetof(XCPlist_t2540119850, ___plistModified_1)); }
	inline bool get_plistModified_1() const { return ___plistModified_1; }
	inline bool* get_address_of_plistModified_1() { return &___plistModified_1; }
	inline void set_plistModified_1(bool value)
	{
		___plistModified_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XCPLIST_T2540119850_H
#ifndef XCODEPOSTPROCESS_T1429692402_H
#define XCODEPOSTPROCESS_T1429692402_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// XCodePostProcess
struct  XCodePostProcess_t1429692402  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XCODEPOSTPROCESS_T1429692402_H
#ifndef XCMODFILE_T1016819465_H
#define XCMODFILE_T1016819465_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.XCModFile
struct  XCModFile_t1016819465  : public RuntimeObject
{
public:
	// System.String UnityEditor.XCodeEditor.XCModFile::<filePath>k__BackingField
	String_t* ___U3CfilePathU3Ek__BackingField_0;
	// System.Boolean UnityEditor.XCodeEditor.XCModFile::<isWeak>k__BackingField
	bool ___U3CisWeakU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CfilePathU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(XCModFile_t1016819465, ___U3CfilePathU3Ek__BackingField_0)); }
	inline String_t* get_U3CfilePathU3Ek__BackingField_0() const { return ___U3CfilePathU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CfilePathU3Ek__BackingField_0() { return &___U3CfilePathU3Ek__BackingField_0; }
	inline void set_U3CfilePathU3Ek__BackingField_0(String_t* value)
	{
		___U3CfilePathU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CfilePathU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CisWeakU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(XCModFile_t1016819465, ___U3CisWeakU3Ek__BackingField_1)); }
	inline bool get_U3CisWeakU3Ek__BackingField_1() const { return ___U3CisWeakU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CisWeakU3Ek__BackingField_1() { return &___U3CisWeakU3Ek__BackingField_1; }
	inline void set_U3CisWeakU3Ek__BackingField_1(bool value)
	{
		___U3CisWeakU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XCMODFILE_T1016819465_H
#ifndef XCMOD_T1014962890_H
#define XCMOD_T1014962890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.XCMod
struct  XCMod_t1014962890  : public RuntimeObject
{
public:
	// System.Collections.Hashtable UnityEditor.XCodeEditor.XCMod::_datastore
	Hashtable_t1853889766 * ____datastore_0;
	// System.Collections.ArrayList UnityEditor.XCodeEditor.XCMod::_libs
	ArrayList_t2718874744 * ____libs_1;
	// System.String UnityEditor.XCodeEditor.XCMod::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_2;
	// System.String UnityEditor.XCodeEditor.XCMod::<path>k__BackingField
	String_t* ___U3CpathU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of__datastore_0() { return static_cast<int32_t>(offsetof(XCMod_t1014962890, ____datastore_0)); }
	inline Hashtable_t1853889766 * get__datastore_0() const { return ____datastore_0; }
	inline Hashtable_t1853889766 ** get_address_of__datastore_0() { return &____datastore_0; }
	inline void set__datastore_0(Hashtable_t1853889766 * value)
	{
		____datastore_0 = value;
		Il2CppCodeGenWriteBarrier((&____datastore_0), value);
	}

	inline static int32_t get_offset_of__libs_1() { return static_cast<int32_t>(offsetof(XCMod_t1014962890, ____libs_1)); }
	inline ArrayList_t2718874744 * get__libs_1() const { return ____libs_1; }
	inline ArrayList_t2718874744 ** get_address_of__libs_1() { return &____libs_1; }
	inline void set__libs_1(ArrayList_t2718874744 * value)
	{
		____libs_1 = value;
		Il2CppCodeGenWriteBarrier((&____libs_1), value);
	}

	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(XCMod_t1014962890, ___U3CnameU3Ek__BackingField_2)); }
	inline String_t* get_U3CnameU3Ek__BackingField_2() const { return ___U3CnameU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_2() { return &___U3CnameU3Ek__BackingField_2; }
	inline void set_U3CnameU3Ek__BackingField_2(String_t* value)
	{
		___U3CnameU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CpathU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(XCMod_t1014962890, ___U3CpathU3Ek__BackingField_3)); }
	inline String_t* get_U3CpathU3Ek__BackingField_3() const { return ___U3CpathU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CpathU3Ek__BackingField_3() { return &___U3CpathU3Ek__BackingField_3; }
	inline void set_U3CpathU3Ek__BackingField_3(String_t* value)
	{
		___U3CpathU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpathU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XCMOD_T1014962890_H
#ifndef PLISTDATECONVERTER_T1153959355_H
#define PLISTDATECONVERTER_T1153959355_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlistCS.PlistDateConverter
struct  PlistDateConverter_t1153959355  : public RuntimeObject
{
public:

public:
};

struct PlistDateConverter_t1153959355_StaticFields
{
public:
	// System.Int64 PlistCS.PlistDateConverter::timeDifference
	int64_t ___timeDifference_0;

public:
	inline static int32_t get_offset_of_timeDifference_0() { return static_cast<int32_t>(offsetof(PlistDateConverter_t1153959355_StaticFields, ___timeDifference_0)); }
	inline int64_t get_timeDifference_0() const { return ___timeDifference_0; }
	inline int64_t* get_address_of_timeDifference_0() { return &___timeDifference_0; }
	inline void set_timeDifference_0(int64_t value)
	{
		___timeDifference_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLISTDATECONVERTER_T1153959355_H
#ifndef PBXOBJECT_T3397571057_H
#define PBXOBJECT_T3397571057_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.PBXObject
struct  PBXObject_t3397571057  : public RuntimeObject
{
public:
	// System.String UnityEditor.XCodeEditor.PBXObject::_guid
	String_t* ____guid_1;
	// UnityEditor.XCodeEditor.PBXDictionary UnityEditor.XCodeEditor.PBXObject::_data
	PBXDictionary_t2078602241 * ____data_2;

public:
	inline static int32_t get_offset_of__guid_1() { return static_cast<int32_t>(offsetof(PBXObject_t3397571057, ____guid_1)); }
	inline String_t* get__guid_1() const { return ____guid_1; }
	inline String_t** get_address_of__guid_1() { return &____guid_1; }
	inline void set__guid_1(String_t* value)
	{
		____guid_1 = value;
		Il2CppCodeGenWriteBarrier((&____guid_1), value);
	}

	inline static int32_t get_offset_of__data_2() { return static_cast<int32_t>(offsetof(PBXObject_t3397571057, ____data_2)); }
	inline PBXDictionary_t2078602241 * get__data_2() const { return ____data_2; }
	inline PBXDictionary_t2078602241 ** get_address_of__data_2() { return &____data_2; }
	inline void set__data_2(PBXDictionary_t2078602241 * value)
	{
		____data_2 = value;
		Il2CppCodeGenWriteBarrier((&____data_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBXOBJECT_T3397571057_H
#ifndef PLIST_T3259848363_H
#define PLIST_T3259848363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlistCS.Plist
struct  Plist_t3259848363  : public RuntimeObject
{
public:

public:
};

struct Plist_t3259848363_StaticFields
{
public:
	// System.Collections.Generic.List`1<System.Int32> PlistCS.Plist::offsetTable
	List_1_t128053199 * ___offsetTable_0;
	// System.Collections.Generic.List`1<System.Byte> PlistCS.Plist::objectTable
	List_1_t2606371118 * ___objectTable_1;
	// System.Int32 PlistCS.Plist::refCount
	int32_t ___refCount_2;
	// System.Int32 PlistCS.Plist::objRefSize
	int32_t ___objRefSize_3;
	// System.Int32 PlistCS.Plist::offsetByteSize
	int32_t ___offsetByteSize_4;
	// System.Int64 PlistCS.Plist::offsetTableOffset
	int64_t ___offsetTableOffset_5;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> PlistCS.Plist::<>f__switch$map0
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map0_6;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> PlistCS.Plist::<>f__switch$map1
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map1_7;

public:
	inline static int32_t get_offset_of_offsetTable_0() { return static_cast<int32_t>(offsetof(Plist_t3259848363_StaticFields, ___offsetTable_0)); }
	inline List_1_t128053199 * get_offsetTable_0() const { return ___offsetTable_0; }
	inline List_1_t128053199 ** get_address_of_offsetTable_0() { return &___offsetTable_0; }
	inline void set_offsetTable_0(List_1_t128053199 * value)
	{
		___offsetTable_0 = value;
		Il2CppCodeGenWriteBarrier((&___offsetTable_0), value);
	}

	inline static int32_t get_offset_of_objectTable_1() { return static_cast<int32_t>(offsetof(Plist_t3259848363_StaticFields, ___objectTable_1)); }
	inline List_1_t2606371118 * get_objectTable_1() const { return ___objectTable_1; }
	inline List_1_t2606371118 ** get_address_of_objectTable_1() { return &___objectTable_1; }
	inline void set_objectTable_1(List_1_t2606371118 * value)
	{
		___objectTable_1 = value;
		Il2CppCodeGenWriteBarrier((&___objectTable_1), value);
	}

	inline static int32_t get_offset_of_refCount_2() { return static_cast<int32_t>(offsetof(Plist_t3259848363_StaticFields, ___refCount_2)); }
	inline int32_t get_refCount_2() const { return ___refCount_2; }
	inline int32_t* get_address_of_refCount_2() { return &___refCount_2; }
	inline void set_refCount_2(int32_t value)
	{
		___refCount_2 = value;
	}

	inline static int32_t get_offset_of_objRefSize_3() { return static_cast<int32_t>(offsetof(Plist_t3259848363_StaticFields, ___objRefSize_3)); }
	inline int32_t get_objRefSize_3() const { return ___objRefSize_3; }
	inline int32_t* get_address_of_objRefSize_3() { return &___objRefSize_3; }
	inline void set_objRefSize_3(int32_t value)
	{
		___objRefSize_3 = value;
	}

	inline static int32_t get_offset_of_offsetByteSize_4() { return static_cast<int32_t>(offsetof(Plist_t3259848363_StaticFields, ___offsetByteSize_4)); }
	inline int32_t get_offsetByteSize_4() const { return ___offsetByteSize_4; }
	inline int32_t* get_address_of_offsetByteSize_4() { return &___offsetByteSize_4; }
	inline void set_offsetByteSize_4(int32_t value)
	{
		___offsetByteSize_4 = value;
	}

	inline static int32_t get_offset_of_offsetTableOffset_5() { return static_cast<int32_t>(offsetof(Plist_t3259848363_StaticFields, ___offsetTableOffset_5)); }
	inline int64_t get_offsetTableOffset_5() const { return ___offsetTableOffset_5; }
	inline int64_t* get_address_of_offsetTableOffset_5() { return &___offsetTableOffset_5; }
	inline void set_offsetTableOffset_5(int64_t value)
	{
		___offsetTableOffset_5 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map0_6() { return static_cast<int32_t>(offsetof(Plist_t3259848363_StaticFields, ___U3CU3Ef__switchU24map0_6)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map0_6() const { return ___U3CU3Ef__switchU24map0_6; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map0_6() { return &___U3CU3Ef__switchU24map0_6; }
	inline void set_U3CU3Ef__switchU24map0_6(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map0_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1_7() { return static_cast<int32_t>(offsetof(Plist_t3259848363_StaticFields, ___U3CU3Ef__switchU24map1_7)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map1_7() const { return ___U3CU3Ef__switchU24map1_7; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map1_7() { return &___U3CU3Ef__switchU24map1_7; }
	inline void set_U3CU3Ef__switchU24map1_7(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map1_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map1_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLIST_T3259848363_H
#ifndef GK_FRIENDREQUEST_T2497375054_H
#define GK_FRIENDREQUEST_T2497375054_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GK_FriendRequest
struct  GK_FriendRequest_t2497375054  : public RuntimeObject
{
public:
	// System.Int32 GK_FriendRequest::_Id
	int32_t ____Id_0;
	// System.Collections.Generic.List`1<System.String> GK_FriendRequest::_PlayersIds
	List_1_t3319525431 * ____PlayersIds_1;
	// System.Collections.Generic.List`1<System.String> GK_FriendRequest::_Emails
	List_1_t3319525431 * ____Emails_2;

public:
	inline static int32_t get_offset_of__Id_0() { return static_cast<int32_t>(offsetof(GK_FriendRequest_t2497375054, ____Id_0)); }
	inline int32_t get__Id_0() const { return ____Id_0; }
	inline int32_t* get_address_of__Id_0() { return &____Id_0; }
	inline void set__Id_0(int32_t value)
	{
		____Id_0 = value;
	}

	inline static int32_t get_offset_of__PlayersIds_1() { return static_cast<int32_t>(offsetof(GK_FriendRequest_t2497375054, ____PlayersIds_1)); }
	inline List_1_t3319525431 * get__PlayersIds_1() const { return ____PlayersIds_1; }
	inline List_1_t3319525431 ** get_address_of__PlayersIds_1() { return &____PlayersIds_1; }
	inline void set__PlayersIds_1(List_1_t3319525431 * value)
	{
		____PlayersIds_1 = value;
		Il2CppCodeGenWriteBarrier((&____PlayersIds_1), value);
	}

	inline static int32_t get_offset_of__Emails_2() { return static_cast<int32_t>(offsetof(GK_FriendRequest_t2497375054, ____Emails_2)); }
	inline List_1_t3319525431 * get__Emails_2() const { return ____Emails_2; }
	inline List_1_t3319525431 ** get_address_of__Emails_2() { return &____Emails_2; }
	inline void set__Emails_2(List_1_t3319525431 * value)
	{
		____Emails_2 = value;
		Il2CppCodeGenWriteBarrier((&____Emails_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GK_FRIENDREQUEST_T2497375054_H
#ifndef ASSETFILE_T64015363_H
#define ASSETFILE_T64015363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSDeploy.AssetFile
struct  AssetFile_t64015363  : public RuntimeObject
{
public:
	// System.Boolean SA.IOSDeploy.AssetFile::IsOpen
	bool ___IsOpen_0;
	// System.String SA.IOSDeploy.AssetFile::XCodePath
	String_t* ___XCodePath_1;
	// UnityEngine.Object SA.IOSDeploy.AssetFile::Asset
	Object_t631007953 * ___Asset_2;

public:
	inline static int32_t get_offset_of_IsOpen_0() { return static_cast<int32_t>(offsetof(AssetFile_t64015363, ___IsOpen_0)); }
	inline bool get_IsOpen_0() const { return ___IsOpen_0; }
	inline bool* get_address_of_IsOpen_0() { return &___IsOpen_0; }
	inline void set_IsOpen_0(bool value)
	{
		___IsOpen_0 = value;
	}

	inline static int32_t get_offset_of_XCodePath_1() { return static_cast<int32_t>(offsetof(AssetFile_t64015363, ___XCodePath_1)); }
	inline String_t* get_XCodePath_1() const { return ___XCodePath_1; }
	inline String_t** get_address_of_XCodePath_1() { return &___XCodePath_1; }
	inline void set_XCodePath_1(String_t* value)
	{
		___XCodePath_1 = value;
		Il2CppCodeGenWriteBarrier((&___XCodePath_1), value);
	}

	inline static int32_t get_offset_of_Asset_2() { return static_cast<int32_t>(offsetof(AssetFile_t64015363, ___Asset_2)); }
	inline Object_t631007953 * get_Asset_2() const { return ___Asset_2; }
	inline Object_t631007953 ** get_address_of_Asset_2() { return &___Asset_2; }
	inline void set_Asset_2(Object_t631007953 * value)
	{
		___Asset_2 = value;
		Il2CppCodeGenWriteBarrier((&___Asset_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETFILE_T64015363_H
#ifndef PBXPARSER_T1614426892_H
#define PBXPARSER_T1614426892_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.PBXParser
struct  PBXParser_t1614426892  : public RuntimeObject
{
public:
	// System.Char[] UnityEditor.XCodeEditor.PBXParser::data
	CharU5BU5D_t3528271667* ___data_20;
	// System.Int32 UnityEditor.XCodeEditor.PBXParser::index
	int32_t ___index_21;
	// UnityEditor.XCodeEditor.PBXResolver UnityEditor.XCodeEditor.PBXParser::resolver
	PBXResolver_t144121547 * ___resolver_22;
	// System.String UnityEditor.XCodeEditor.PBXParser::marker
	String_t* ___marker_23;

public:
	inline static int32_t get_offset_of_data_20() { return static_cast<int32_t>(offsetof(PBXParser_t1614426892, ___data_20)); }
	inline CharU5BU5D_t3528271667* get_data_20() const { return ___data_20; }
	inline CharU5BU5D_t3528271667** get_address_of_data_20() { return &___data_20; }
	inline void set_data_20(CharU5BU5D_t3528271667* value)
	{
		___data_20 = value;
		Il2CppCodeGenWriteBarrier((&___data_20), value);
	}

	inline static int32_t get_offset_of_index_21() { return static_cast<int32_t>(offsetof(PBXParser_t1614426892, ___index_21)); }
	inline int32_t get_index_21() const { return ___index_21; }
	inline int32_t* get_address_of_index_21() { return &___index_21; }
	inline void set_index_21(int32_t value)
	{
		___index_21 = value;
	}

	inline static int32_t get_offset_of_resolver_22() { return static_cast<int32_t>(offsetof(PBXParser_t1614426892, ___resolver_22)); }
	inline PBXResolver_t144121547 * get_resolver_22() const { return ___resolver_22; }
	inline PBXResolver_t144121547 ** get_address_of_resolver_22() { return &___resolver_22; }
	inline void set_resolver_22(PBXResolver_t144121547 * value)
	{
		___resolver_22 = value;
		Il2CppCodeGenWriteBarrier((&___resolver_22), value);
	}

	inline static int32_t get_offset_of_marker_23() { return static_cast<int32_t>(offsetof(PBXParser_t1614426892, ___marker_23)); }
	inline String_t* get_marker_23() const { return ___marker_23; }
	inline String_t** get_address_of_marker_23() { return &___marker_23; }
	inline void set_marker_23(String_t* value)
	{
		___marker_23 = value;
		Il2CppCodeGenWriteBarrier((&___marker_23), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBXPARSER_T1614426892_H
#ifndef GK_ACHIEVEMENTTEMPLATE_T3624697896_H
#define GK_ACHIEVEMENTTEMPLATE_T3624697896_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GK_AchievementTemplate
struct  GK_AchievementTemplate_t3624697896  : public RuntimeObject
{
public:
	// System.Boolean GK_AchievementTemplate::IsOpen
	bool ___IsOpen_0;
	// System.String GK_AchievementTemplate::Id
	String_t* ___Id_1;
	// System.String GK_AchievementTemplate::Title
	String_t* ___Title_2;
	// System.String GK_AchievementTemplate::Description
	String_t* ___Description_3;
	// System.Single GK_AchievementTemplate::_progress
	float ____progress_4;
	// UnityEngine.Texture2D GK_AchievementTemplate::Texture
	Texture2D_t3840446185 * ___Texture_5;

public:
	inline static int32_t get_offset_of_IsOpen_0() { return static_cast<int32_t>(offsetof(GK_AchievementTemplate_t3624697896, ___IsOpen_0)); }
	inline bool get_IsOpen_0() const { return ___IsOpen_0; }
	inline bool* get_address_of_IsOpen_0() { return &___IsOpen_0; }
	inline void set_IsOpen_0(bool value)
	{
		___IsOpen_0 = value;
	}

	inline static int32_t get_offset_of_Id_1() { return static_cast<int32_t>(offsetof(GK_AchievementTemplate_t3624697896, ___Id_1)); }
	inline String_t* get_Id_1() const { return ___Id_1; }
	inline String_t** get_address_of_Id_1() { return &___Id_1; }
	inline void set_Id_1(String_t* value)
	{
		___Id_1 = value;
		Il2CppCodeGenWriteBarrier((&___Id_1), value);
	}

	inline static int32_t get_offset_of_Title_2() { return static_cast<int32_t>(offsetof(GK_AchievementTemplate_t3624697896, ___Title_2)); }
	inline String_t* get_Title_2() const { return ___Title_2; }
	inline String_t** get_address_of_Title_2() { return &___Title_2; }
	inline void set_Title_2(String_t* value)
	{
		___Title_2 = value;
		Il2CppCodeGenWriteBarrier((&___Title_2), value);
	}

	inline static int32_t get_offset_of_Description_3() { return static_cast<int32_t>(offsetof(GK_AchievementTemplate_t3624697896, ___Description_3)); }
	inline String_t* get_Description_3() const { return ___Description_3; }
	inline String_t** get_address_of_Description_3() { return &___Description_3; }
	inline void set_Description_3(String_t* value)
	{
		___Description_3 = value;
		Il2CppCodeGenWriteBarrier((&___Description_3), value);
	}

	inline static int32_t get_offset_of__progress_4() { return static_cast<int32_t>(offsetof(GK_AchievementTemplate_t3624697896, ____progress_4)); }
	inline float get__progress_4() const { return ____progress_4; }
	inline float* get_address_of__progress_4() { return &____progress_4; }
	inline void set__progress_4(float value)
	{
		____progress_4 = value;
	}

	inline static int32_t get_offset_of_Texture_5() { return static_cast<int32_t>(offsetof(GK_AchievementTemplate_t3624697896, ___Texture_5)); }
	inline Texture2D_t3840446185 * get_Texture_5() const { return ___Texture_5; }
	inline Texture2D_t3840446185 ** get_address_of_Texture_5() { return &___Texture_5; }
	inline void set_Texture_5(Texture2D_t3840446185 * value)
	{
		___Texture_5 = value;
		Il2CppCodeGenWriteBarrier((&___Texture_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GK_ACHIEVEMENTTEMPLATE_T3624697896_H
#ifndef GK_INVITE_T1194874416_H
#define GK_INVITE_T1194874416_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GK_Invite
struct  GK_Invite_t1194874416  : public RuntimeObject
{
public:
	// System.String GK_Invite::_Id
	String_t* ____Id_0;
	// GK_Player GK_Invite::_Sender
	GK_Player_t3461352791 * ____Sender_1;
	// System.Int32 GK_Invite::_PlayerGroup
	int32_t ____PlayerGroup_2;
	// System.Int32 GK_Invite::_PlayerAttributes
	int32_t ____PlayerAttributes_3;

public:
	inline static int32_t get_offset_of__Id_0() { return static_cast<int32_t>(offsetof(GK_Invite_t1194874416, ____Id_0)); }
	inline String_t* get__Id_0() const { return ____Id_0; }
	inline String_t** get_address_of__Id_0() { return &____Id_0; }
	inline void set__Id_0(String_t* value)
	{
		____Id_0 = value;
		Il2CppCodeGenWriteBarrier((&____Id_0), value);
	}

	inline static int32_t get_offset_of__Sender_1() { return static_cast<int32_t>(offsetof(GK_Invite_t1194874416, ____Sender_1)); }
	inline GK_Player_t3461352791 * get__Sender_1() const { return ____Sender_1; }
	inline GK_Player_t3461352791 ** get_address_of__Sender_1() { return &____Sender_1; }
	inline void set__Sender_1(GK_Player_t3461352791 * value)
	{
		____Sender_1 = value;
		Il2CppCodeGenWriteBarrier((&____Sender_1), value);
	}

	inline static int32_t get_offset_of__PlayerGroup_2() { return static_cast<int32_t>(offsetof(GK_Invite_t1194874416, ____PlayerGroup_2)); }
	inline int32_t get__PlayerGroup_2() const { return ____PlayerGroup_2; }
	inline int32_t* get_address_of__PlayerGroup_2() { return &____PlayerGroup_2; }
	inline void set__PlayerGroup_2(int32_t value)
	{
		____PlayerGroup_2 = value;
	}

	inline static int32_t get_offset_of__PlayerAttributes_3() { return static_cast<int32_t>(offsetof(GK_Invite_t1194874416, ____PlayerAttributes_3)); }
	inline int32_t get__PlayerAttributes_3() const { return ____PlayerAttributes_3; }
	inline int32_t* get_address_of__PlayerAttributes_3() { return &____PlayerAttributes_3; }
	inline void set__PlayerAttributes_3(int32_t value)
	{
		____PlayerAttributes_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GK_INVITE_T1194874416_H
#ifndef GK_LEADERBOARDINFO_T323632070_H
#define GK_LEADERBOARDINFO_T323632070_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GK_LeaderBoardInfo
struct  GK_LeaderBoardInfo_t323632070  : public RuntimeObject
{
public:
	// System.String GK_LeaderBoardInfo::Title
	String_t* ___Title_0;
	// System.String GK_LeaderBoardInfo::Description
	String_t* ___Description_1;
	// System.String GK_LeaderBoardInfo::Identifier
	String_t* ___Identifier_2;
	// UnityEngine.Texture2D GK_LeaderBoardInfo::Texture
	Texture2D_t3840446185 * ___Texture_3;
	// System.Int32 GK_LeaderBoardInfo::MaxRange
	int32_t ___MaxRange_4;

public:
	inline static int32_t get_offset_of_Title_0() { return static_cast<int32_t>(offsetof(GK_LeaderBoardInfo_t323632070, ___Title_0)); }
	inline String_t* get_Title_0() const { return ___Title_0; }
	inline String_t** get_address_of_Title_0() { return &___Title_0; }
	inline void set_Title_0(String_t* value)
	{
		___Title_0 = value;
		Il2CppCodeGenWriteBarrier((&___Title_0), value);
	}

	inline static int32_t get_offset_of_Description_1() { return static_cast<int32_t>(offsetof(GK_LeaderBoardInfo_t323632070, ___Description_1)); }
	inline String_t* get_Description_1() const { return ___Description_1; }
	inline String_t** get_address_of_Description_1() { return &___Description_1; }
	inline void set_Description_1(String_t* value)
	{
		___Description_1 = value;
		Il2CppCodeGenWriteBarrier((&___Description_1), value);
	}

	inline static int32_t get_offset_of_Identifier_2() { return static_cast<int32_t>(offsetof(GK_LeaderBoardInfo_t323632070, ___Identifier_2)); }
	inline String_t* get_Identifier_2() const { return ___Identifier_2; }
	inline String_t** get_address_of_Identifier_2() { return &___Identifier_2; }
	inline void set_Identifier_2(String_t* value)
	{
		___Identifier_2 = value;
		Il2CppCodeGenWriteBarrier((&___Identifier_2), value);
	}

	inline static int32_t get_offset_of_Texture_3() { return static_cast<int32_t>(offsetof(GK_LeaderBoardInfo_t323632070, ___Texture_3)); }
	inline Texture2D_t3840446185 * get_Texture_3() const { return ___Texture_3; }
	inline Texture2D_t3840446185 ** get_address_of_Texture_3() { return &___Texture_3; }
	inline void set_Texture_3(Texture2D_t3840446185 * value)
	{
		___Texture_3 = value;
		Il2CppCodeGenWriteBarrier((&___Texture_3), value);
	}

	inline static int32_t get_offset_of_MaxRange_4() { return static_cast<int32_t>(offsetof(GK_LeaderBoardInfo_t323632070, ___MaxRange_4)); }
	inline int32_t get_MaxRange_4() const { return ___MaxRange_4; }
	inline int32_t* get_address_of_MaxRange_4() { return &___MaxRange_4; }
	inline void set_MaxRange_4(int32_t value)
	{
		___MaxRange_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GK_LEADERBOARDINFO_T323632070_H
#ifndef SORTEDDICTIONARY_2_T4287882758_H
#define SORTEDDICTIONARY_2_T4287882758_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.SortedDictionary`2<System.String,System.Object>
struct  SortedDictionary_2_t4287882758  : public RuntimeObject
{
public:
	// System.Collections.Generic.RBTree System.Collections.Generic.SortedDictionary`2::tree
	RBTree_t4095273678 * ___tree_0;
	// System.Collections.Generic.SortedDictionary`2/NodeHelper<TKey,TValue> System.Collections.Generic.SortedDictionary`2::hlp
	NodeHelper_t2003304144 * ___hlp_1;

public:
	inline static int32_t get_offset_of_tree_0() { return static_cast<int32_t>(offsetof(SortedDictionary_2_t4287882758, ___tree_0)); }
	inline RBTree_t4095273678 * get_tree_0() const { return ___tree_0; }
	inline RBTree_t4095273678 ** get_address_of_tree_0() { return &___tree_0; }
	inline void set_tree_0(RBTree_t4095273678 * value)
	{
		___tree_0 = value;
		Il2CppCodeGenWriteBarrier((&___tree_0), value);
	}

	inline static int32_t get_offset_of_hlp_1() { return static_cast<int32_t>(offsetof(SortedDictionary_2_t4287882758, ___hlp_1)); }
	inline NodeHelper_t2003304144 * get_hlp_1() const { return ___hlp_1; }
	inline NodeHelper_t2003304144 ** get_address_of_hlp_1() { return &___hlp_1; }
	inline void set_hlp_1(NodeHelper_t2003304144 * value)
	{
		___hlp_1 = value;
		Il2CppCodeGenWriteBarrier((&___hlp_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SORTEDDICTIONARY_2_T4287882758_H
#ifndef RESULT_T4170401718_H
#define RESULT_T4170401718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Models.Result
struct  Result_t4170401718  : public RuntimeObject
{
public:
	// SA.Common.Models.Error SA.Common.Models.Result::_Error
	Error_t340543044 * ____Error_0;

public:
	inline static int32_t get_offset_of__Error_0() { return static_cast<int32_t>(offsetof(Result_t4170401718, ____Error_0)); }
	inline Error_t340543044 * get__Error_0() const { return ____Error_0; }
	inline Error_t340543044 ** get_address_of__Error_0() { return &____Error_0; }
	inline void set__Error_0(Error_t340543044 * value)
	{
		____Error_0 = value;
		Il2CppCodeGenWriteBarrier((&____Error_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESULT_T4170401718_H
#ifndef DICTIONARY_2_T1632706988_H
#define DICTIONARY_2_T1632706988_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct  Dictionary_2_t1632706988  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t385246372* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t964245573* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	StringU5BU5D_t1281789340* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	StringU5BU5D_t1281789340* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t950877179 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___table_4)); }
	inline Int32U5BU5D_t385246372* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t385246372** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t385246372* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___linkSlots_5)); }
	inline LinkU5BU5D_t964245573* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t964245573** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t964245573* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___keySlots_6)); }
	inline StringU5BU5D_t1281789340* get_keySlots_6() const { return ___keySlots_6; }
	inline StringU5BU5D_t1281789340** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(StringU5BU5D_t1281789340* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___valueSlots_7)); }
	inline StringU5BU5D_t1281789340* get_valueSlots_7() const { return ___valueSlots_7; }
	inline StringU5BU5D_t1281789340** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(StringU5BU5D_t1281789340* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___serialization_info_13)); }
	inline SerializationInfo_t950877179 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t950877179 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t950877179 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t1632706988_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t132201056 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t132201056 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t132201056 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t132201056 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T1632706988_H
#ifndef ARRAYLIST_T2718874744_H
#define ARRAYLIST_T2718874744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.ArrayList
struct  ArrayList_t2718874744  : public RuntimeObject
{
public:
	// System.Int32 System.Collections.ArrayList::_size
	int32_t ____size_1;
	// System.Object[] System.Collections.ArrayList::_items
	ObjectU5BU5D_t2843939325* ____items_2;
	// System.Int32 System.Collections.ArrayList::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__size_1() { return static_cast<int32_t>(offsetof(ArrayList_t2718874744, ____size_1)); }
	inline int32_t get__size_1() const { return ____size_1; }
	inline int32_t* get_address_of__size_1() { return &____size_1; }
	inline void set__size_1(int32_t value)
	{
		____size_1 = value;
	}

	inline static int32_t get_offset_of__items_2() { return static_cast<int32_t>(offsetof(ArrayList_t2718874744, ____items_2)); }
	inline ObjectU5BU5D_t2843939325* get__items_2() const { return ____items_2; }
	inline ObjectU5BU5D_t2843939325** get_address_of__items_2() { return &____items_2; }
	inline void set__items_2(ObjectU5BU5D_t2843939325* value)
	{
		____items_2 = value;
		Il2CppCodeGenWriteBarrier((&____items_2), value);
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(ArrayList_t2718874744, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct ArrayList_t2718874744_StaticFields
{
public:
	// System.Object[] System.Collections.ArrayList::EmptyArray
	ObjectU5BU5D_t2843939325* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(ArrayList_t2718874744_StaticFields, ___EmptyArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYLIST_T2718874744_H
#ifndef BASEVERTEXEFFECT_T2675891272_H
#define BASEVERTEXEFFECT_T2675891272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BaseVertexEffect
struct  BaseVertexEffect_t2675891272  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEVERTEXEFFECT_T2675891272_H
#ifndef GK_LEADERBOARD_T591378496_H
#define GK_LEADERBOARD_T591378496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GK_Leaderboard
struct  GK_Leaderboard_t591378496  : public RuntimeObject
{
public:
	// System.Boolean GK_Leaderboard::IsOpen
	bool ___IsOpen_0;
	// System.Boolean GK_Leaderboard::_CurrentPlayerScoreLoaded
	bool ____CurrentPlayerScoreLoaded_1;
	// GK_ScoreCollection GK_Leaderboard::SocsialCollection
	GK_ScoreCollection_t3739344104 * ___SocsialCollection_2;
	// GK_ScoreCollection GK_Leaderboard::GlobalCollection
	GK_ScoreCollection_t3739344104 * ___GlobalCollection_3;
	// System.Collections.Generic.List`1<GK_Score> GK_Leaderboard::CurrentPlayerScore
	List_1_t1687396993 * ___CurrentPlayerScore_4;
	// System.Collections.Generic.Dictionary`2<System.Int32,GK_LocalPlayerScoreUpdateListener> GK_Leaderboard::ScoreUpdateListners
	Dictionary_2_t3801404807 * ___ScoreUpdateListners_5;
	// GK_LeaderBoardInfo GK_Leaderboard::_info
	GK_LeaderBoardInfo_t323632070 * ____info_6;

public:
	inline static int32_t get_offset_of_IsOpen_0() { return static_cast<int32_t>(offsetof(GK_Leaderboard_t591378496, ___IsOpen_0)); }
	inline bool get_IsOpen_0() const { return ___IsOpen_0; }
	inline bool* get_address_of_IsOpen_0() { return &___IsOpen_0; }
	inline void set_IsOpen_0(bool value)
	{
		___IsOpen_0 = value;
	}

	inline static int32_t get_offset_of__CurrentPlayerScoreLoaded_1() { return static_cast<int32_t>(offsetof(GK_Leaderboard_t591378496, ____CurrentPlayerScoreLoaded_1)); }
	inline bool get__CurrentPlayerScoreLoaded_1() const { return ____CurrentPlayerScoreLoaded_1; }
	inline bool* get_address_of__CurrentPlayerScoreLoaded_1() { return &____CurrentPlayerScoreLoaded_1; }
	inline void set__CurrentPlayerScoreLoaded_1(bool value)
	{
		____CurrentPlayerScoreLoaded_1 = value;
	}

	inline static int32_t get_offset_of_SocsialCollection_2() { return static_cast<int32_t>(offsetof(GK_Leaderboard_t591378496, ___SocsialCollection_2)); }
	inline GK_ScoreCollection_t3739344104 * get_SocsialCollection_2() const { return ___SocsialCollection_2; }
	inline GK_ScoreCollection_t3739344104 ** get_address_of_SocsialCollection_2() { return &___SocsialCollection_2; }
	inline void set_SocsialCollection_2(GK_ScoreCollection_t3739344104 * value)
	{
		___SocsialCollection_2 = value;
		Il2CppCodeGenWriteBarrier((&___SocsialCollection_2), value);
	}

	inline static int32_t get_offset_of_GlobalCollection_3() { return static_cast<int32_t>(offsetof(GK_Leaderboard_t591378496, ___GlobalCollection_3)); }
	inline GK_ScoreCollection_t3739344104 * get_GlobalCollection_3() const { return ___GlobalCollection_3; }
	inline GK_ScoreCollection_t3739344104 ** get_address_of_GlobalCollection_3() { return &___GlobalCollection_3; }
	inline void set_GlobalCollection_3(GK_ScoreCollection_t3739344104 * value)
	{
		___GlobalCollection_3 = value;
		Il2CppCodeGenWriteBarrier((&___GlobalCollection_3), value);
	}

	inline static int32_t get_offset_of_CurrentPlayerScore_4() { return static_cast<int32_t>(offsetof(GK_Leaderboard_t591378496, ___CurrentPlayerScore_4)); }
	inline List_1_t1687396993 * get_CurrentPlayerScore_4() const { return ___CurrentPlayerScore_4; }
	inline List_1_t1687396993 ** get_address_of_CurrentPlayerScore_4() { return &___CurrentPlayerScore_4; }
	inline void set_CurrentPlayerScore_4(List_1_t1687396993 * value)
	{
		___CurrentPlayerScore_4 = value;
		Il2CppCodeGenWriteBarrier((&___CurrentPlayerScore_4), value);
	}

	inline static int32_t get_offset_of_ScoreUpdateListners_5() { return static_cast<int32_t>(offsetof(GK_Leaderboard_t591378496, ___ScoreUpdateListners_5)); }
	inline Dictionary_2_t3801404807 * get_ScoreUpdateListners_5() const { return ___ScoreUpdateListners_5; }
	inline Dictionary_2_t3801404807 ** get_address_of_ScoreUpdateListners_5() { return &___ScoreUpdateListners_5; }
	inline void set_ScoreUpdateListners_5(Dictionary_2_t3801404807 * value)
	{
		___ScoreUpdateListners_5 = value;
		Il2CppCodeGenWriteBarrier((&___ScoreUpdateListners_5), value);
	}

	inline static int32_t get_offset_of__info_6() { return static_cast<int32_t>(offsetof(GK_Leaderboard_t591378496, ____info_6)); }
	inline GK_LeaderBoardInfo_t323632070 * get__info_6() const { return ____info_6; }
	inline GK_LeaderBoardInfo_t323632070 ** get_address_of__info_6() { return &____info_6; }
	inline void set__info_6(GK_LeaderBoardInfo_t323632070 * value)
	{
		____info_6 = value;
		Il2CppCodeGenWriteBarrier((&____info_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GK_LEADERBOARD_T591378496_H
#ifndef GK_LEADERBOARDSET_T3182444842_H
#define GK_LEADERBOARDSET_T3182444842_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GK_LeaderboardSet
struct  GK_LeaderboardSet_t3182444842  : public RuntimeObject
{
public:
	// System.String GK_LeaderboardSet::Title
	String_t* ___Title_0;
	// System.String GK_LeaderboardSet::Identifier
	String_t* ___Identifier_1;
	// System.String GK_LeaderboardSet::GroupIdentifier
	String_t* ___GroupIdentifier_2;
	// System.Collections.Generic.List`1<GK_LeaderBoardInfo> GK_LeaderboardSet::_BoardsInfo
	List_1_t1795706812 * ____BoardsInfo_3;
	// System.Action`1<ISN_LoadSetLeaderboardsInfoResult> GK_LeaderboardSet::OnLoaderboardsInfoLoaded
	Action_1_t1927089056 * ___OnLoaderboardsInfoLoaded_4;

public:
	inline static int32_t get_offset_of_Title_0() { return static_cast<int32_t>(offsetof(GK_LeaderboardSet_t3182444842, ___Title_0)); }
	inline String_t* get_Title_0() const { return ___Title_0; }
	inline String_t** get_address_of_Title_0() { return &___Title_0; }
	inline void set_Title_0(String_t* value)
	{
		___Title_0 = value;
		Il2CppCodeGenWriteBarrier((&___Title_0), value);
	}

	inline static int32_t get_offset_of_Identifier_1() { return static_cast<int32_t>(offsetof(GK_LeaderboardSet_t3182444842, ___Identifier_1)); }
	inline String_t* get_Identifier_1() const { return ___Identifier_1; }
	inline String_t** get_address_of_Identifier_1() { return &___Identifier_1; }
	inline void set_Identifier_1(String_t* value)
	{
		___Identifier_1 = value;
		Il2CppCodeGenWriteBarrier((&___Identifier_1), value);
	}

	inline static int32_t get_offset_of_GroupIdentifier_2() { return static_cast<int32_t>(offsetof(GK_LeaderboardSet_t3182444842, ___GroupIdentifier_2)); }
	inline String_t* get_GroupIdentifier_2() const { return ___GroupIdentifier_2; }
	inline String_t** get_address_of_GroupIdentifier_2() { return &___GroupIdentifier_2; }
	inline void set_GroupIdentifier_2(String_t* value)
	{
		___GroupIdentifier_2 = value;
		Il2CppCodeGenWriteBarrier((&___GroupIdentifier_2), value);
	}

	inline static int32_t get_offset_of__BoardsInfo_3() { return static_cast<int32_t>(offsetof(GK_LeaderboardSet_t3182444842, ____BoardsInfo_3)); }
	inline List_1_t1795706812 * get__BoardsInfo_3() const { return ____BoardsInfo_3; }
	inline List_1_t1795706812 ** get_address_of__BoardsInfo_3() { return &____BoardsInfo_3; }
	inline void set__BoardsInfo_3(List_1_t1795706812 * value)
	{
		____BoardsInfo_3 = value;
		Il2CppCodeGenWriteBarrier((&____BoardsInfo_3), value);
	}

	inline static int32_t get_offset_of_OnLoaderboardsInfoLoaded_4() { return static_cast<int32_t>(offsetof(GK_LeaderboardSet_t3182444842, ___OnLoaderboardsInfoLoaded_4)); }
	inline Action_1_t1927089056 * get_OnLoaderboardsInfoLoaded_4() const { return ___OnLoaderboardsInfoLoaded_4; }
	inline Action_1_t1927089056 ** get_address_of_OnLoaderboardsInfoLoaded_4() { return &___OnLoaderboardsInfoLoaded_4; }
	inline void set_OnLoaderboardsInfoLoaded_4(Action_1_t1927089056 * value)
	{
		___OnLoaderboardsInfoLoaded_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnLoaderboardsInfoLoaded_4), value);
	}
};

struct GK_LeaderboardSet_t3182444842_StaticFields
{
public:
	// System.Action`1<ISN_LoadSetLeaderboardsInfoResult> GK_LeaderboardSet::<>f__am$cache0
	Action_1_t1927089056 * ___U3CU3Ef__amU24cache0_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_5() { return static_cast<int32_t>(offsetof(GK_LeaderboardSet_t3182444842_StaticFields, ___U3CU3Ef__amU24cache0_5)); }
	inline Action_1_t1927089056 * get_U3CU3Ef__amU24cache0_5() const { return ___U3CU3Ef__amU24cache0_5; }
	inline Action_1_t1927089056 ** get_address_of_U3CU3Ef__amU24cache0_5() { return &___U3CU3Ef__amU24cache0_5; }
	inline void set_U3CU3Ef__amU24cache0_5(Action_1_t1927089056 * value)
	{
		___U3CU3Ef__amU24cache0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GK_LEADERBOARDSET_T3182444842_H
#ifndef GK_LOCALPLAYERSCOREUPDATELISTENER_T617724180_H
#define GK_LOCALPLAYERSCOREUPDATELISTENER_T617724180_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GK_LocalPlayerScoreUpdateListener
struct  GK_LocalPlayerScoreUpdateListener_t617724180  : public RuntimeObject
{
public:
	// System.Int32 GK_LocalPlayerScoreUpdateListener::_RequestId
	int32_t ____RequestId_0;
	// System.Boolean GK_LocalPlayerScoreUpdateListener::_IsInternal
	bool ____IsInternal_1;
	// System.String GK_LocalPlayerScoreUpdateListener::_leaderboardId
	String_t* ____leaderboardId_2;
	// System.String GK_LocalPlayerScoreUpdateListener::_ErrorData
	String_t* ____ErrorData_3;
	// System.Collections.Generic.List`1<GK_Score> GK_LocalPlayerScoreUpdateListener::Scores
	List_1_t1687396993 * ___Scores_4;

public:
	inline static int32_t get_offset_of__RequestId_0() { return static_cast<int32_t>(offsetof(GK_LocalPlayerScoreUpdateListener_t617724180, ____RequestId_0)); }
	inline int32_t get__RequestId_0() const { return ____RequestId_0; }
	inline int32_t* get_address_of__RequestId_0() { return &____RequestId_0; }
	inline void set__RequestId_0(int32_t value)
	{
		____RequestId_0 = value;
	}

	inline static int32_t get_offset_of__IsInternal_1() { return static_cast<int32_t>(offsetof(GK_LocalPlayerScoreUpdateListener_t617724180, ____IsInternal_1)); }
	inline bool get__IsInternal_1() const { return ____IsInternal_1; }
	inline bool* get_address_of__IsInternal_1() { return &____IsInternal_1; }
	inline void set__IsInternal_1(bool value)
	{
		____IsInternal_1 = value;
	}

	inline static int32_t get_offset_of__leaderboardId_2() { return static_cast<int32_t>(offsetof(GK_LocalPlayerScoreUpdateListener_t617724180, ____leaderboardId_2)); }
	inline String_t* get__leaderboardId_2() const { return ____leaderboardId_2; }
	inline String_t** get_address_of__leaderboardId_2() { return &____leaderboardId_2; }
	inline void set__leaderboardId_2(String_t* value)
	{
		____leaderboardId_2 = value;
		Il2CppCodeGenWriteBarrier((&____leaderboardId_2), value);
	}

	inline static int32_t get_offset_of__ErrorData_3() { return static_cast<int32_t>(offsetof(GK_LocalPlayerScoreUpdateListener_t617724180, ____ErrorData_3)); }
	inline String_t* get__ErrorData_3() const { return ____ErrorData_3; }
	inline String_t** get_address_of__ErrorData_3() { return &____ErrorData_3; }
	inline void set__ErrorData_3(String_t* value)
	{
		____ErrorData_3 = value;
		Il2CppCodeGenWriteBarrier((&____ErrorData_3), value);
	}

	inline static int32_t get_offset_of_Scores_4() { return static_cast<int32_t>(offsetof(GK_LocalPlayerScoreUpdateListener_t617724180, ___Scores_4)); }
	inline List_1_t1687396993 * get_Scores_4() const { return ___Scores_4; }
	inline List_1_t1687396993 ** get_address_of_Scores_4() { return &___Scores_4; }
	inline void set_Scores_4(List_1_t1687396993 * value)
	{
		___Scores_4 = value;
		Il2CppCodeGenWriteBarrier((&___Scores_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GK_LOCALPLAYERSCOREUPDATELISTENER_T617724180_H
#ifndef DICTIONARY_2_T2865362463_H
#define DICTIONARY_2_T2865362463_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct  Dictionary_2_t2865362463  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t385246372* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t964245573* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	StringU5BU5D_t1281789340* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	ObjectU5BU5D_t2843939325* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t950877179 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___table_4)); }
	inline Int32U5BU5D_t385246372* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t385246372** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t385246372* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___linkSlots_5)); }
	inline LinkU5BU5D_t964245573* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t964245573** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t964245573* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___keySlots_6)); }
	inline StringU5BU5D_t1281789340* get_keySlots_6() const { return ___keySlots_6; }
	inline StringU5BU5D_t1281789340** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(StringU5BU5D_t1281789340* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___valueSlots_7)); }
	inline ObjectU5BU5D_t2843939325* get_valueSlots_7() const { return ___valueSlots_7; }
	inline ObjectU5BU5D_t2843939325** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(ObjectU5BU5D_t2843939325* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___serialization_info_13)); }
	inline SerializationInfo_t950877179 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t950877179 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t950877179 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t2865362463_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t1694351041 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t1694351041 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t1694351041 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t1694351041 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T2865362463_H
#ifndef GK_PLAYER_T3461352791_H
#define GK_PLAYER_T3461352791_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GK_Player
struct  GK_Player_t3461352791  : public RuntimeObject
{
public:
	// System.String GK_Player::_PlayerId
	String_t* ____PlayerId_0;
	// System.String GK_Player::_DisplayName
	String_t* ____DisplayName_1;
	// System.String GK_Player::_Alias
	String_t* ____Alias_2;
	// UnityEngine.Texture2D GK_Player::_SmallPhoto
	Texture2D_t3840446185 * ____SmallPhoto_3;
	// UnityEngine.Texture2D GK_Player::_BigPhoto
	Texture2D_t3840446185 * ____BigPhoto_4;
	// System.Action`1<GK_UserPhotoLoadResult> GK_Player::OnPlayerPhotoLoaded
	Action_1_t4091228422 * ___OnPlayerPhotoLoaded_5;

public:
	inline static int32_t get_offset_of__PlayerId_0() { return static_cast<int32_t>(offsetof(GK_Player_t3461352791, ____PlayerId_0)); }
	inline String_t* get__PlayerId_0() const { return ____PlayerId_0; }
	inline String_t** get_address_of__PlayerId_0() { return &____PlayerId_0; }
	inline void set__PlayerId_0(String_t* value)
	{
		____PlayerId_0 = value;
		Il2CppCodeGenWriteBarrier((&____PlayerId_0), value);
	}

	inline static int32_t get_offset_of__DisplayName_1() { return static_cast<int32_t>(offsetof(GK_Player_t3461352791, ____DisplayName_1)); }
	inline String_t* get__DisplayName_1() const { return ____DisplayName_1; }
	inline String_t** get_address_of__DisplayName_1() { return &____DisplayName_1; }
	inline void set__DisplayName_1(String_t* value)
	{
		____DisplayName_1 = value;
		Il2CppCodeGenWriteBarrier((&____DisplayName_1), value);
	}

	inline static int32_t get_offset_of__Alias_2() { return static_cast<int32_t>(offsetof(GK_Player_t3461352791, ____Alias_2)); }
	inline String_t* get__Alias_2() const { return ____Alias_2; }
	inline String_t** get_address_of__Alias_2() { return &____Alias_2; }
	inline void set__Alias_2(String_t* value)
	{
		____Alias_2 = value;
		Il2CppCodeGenWriteBarrier((&____Alias_2), value);
	}

	inline static int32_t get_offset_of__SmallPhoto_3() { return static_cast<int32_t>(offsetof(GK_Player_t3461352791, ____SmallPhoto_3)); }
	inline Texture2D_t3840446185 * get__SmallPhoto_3() const { return ____SmallPhoto_3; }
	inline Texture2D_t3840446185 ** get_address_of__SmallPhoto_3() { return &____SmallPhoto_3; }
	inline void set__SmallPhoto_3(Texture2D_t3840446185 * value)
	{
		____SmallPhoto_3 = value;
		Il2CppCodeGenWriteBarrier((&____SmallPhoto_3), value);
	}

	inline static int32_t get_offset_of__BigPhoto_4() { return static_cast<int32_t>(offsetof(GK_Player_t3461352791, ____BigPhoto_4)); }
	inline Texture2D_t3840446185 * get__BigPhoto_4() const { return ____BigPhoto_4; }
	inline Texture2D_t3840446185 ** get_address_of__BigPhoto_4() { return &____BigPhoto_4; }
	inline void set__BigPhoto_4(Texture2D_t3840446185 * value)
	{
		____BigPhoto_4 = value;
		Il2CppCodeGenWriteBarrier((&____BigPhoto_4), value);
	}

	inline static int32_t get_offset_of_OnPlayerPhotoLoaded_5() { return static_cast<int32_t>(offsetof(GK_Player_t3461352791, ___OnPlayerPhotoLoaded_5)); }
	inline Action_1_t4091228422 * get_OnPlayerPhotoLoaded_5() const { return ___OnPlayerPhotoLoaded_5; }
	inline Action_1_t4091228422 ** get_address_of_OnPlayerPhotoLoaded_5() { return &___OnPlayerPhotoLoaded_5; }
	inline void set_OnPlayerPhotoLoaded_5(Action_1_t4091228422 * value)
	{
		___OnPlayerPhotoLoaded_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnPlayerPhotoLoaded_5), value);
	}
};

struct GK_Player_t3461352791_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Texture2D> GK_Player::LocalPhotosCache
	Dictionary_2_t3625702484 * ___LocalPhotosCache_6;
	// System.Action`1<GK_UserPhotoLoadResult> GK_Player::<>f__am$cache0
	Action_1_t4091228422 * ___U3CU3Ef__amU24cache0_7;

public:
	inline static int32_t get_offset_of_LocalPhotosCache_6() { return static_cast<int32_t>(offsetof(GK_Player_t3461352791_StaticFields, ___LocalPhotosCache_6)); }
	inline Dictionary_2_t3625702484 * get_LocalPhotosCache_6() const { return ___LocalPhotosCache_6; }
	inline Dictionary_2_t3625702484 ** get_address_of_LocalPhotosCache_6() { return &___LocalPhotosCache_6; }
	inline void set_LocalPhotosCache_6(Dictionary_2_t3625702484 * value)
	{
		___LocalPhotosCache_6 = value;
		Il2CppCodeGenWriteBarrier((&___LocalPhotosCache_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_7() { return static_cast<int32_t>(offsetof(GK_Player_t3461352791_StaticFields, ___U3CU3Ef__amU24cache0_7)); }
	inline Action_1_t4091228422 * get_U3CU3Ef__amU24cache0_7() const { return ___U3CU3Ef__amU24cache0_7; }
	inline Action_1_t4091228422 ** get_address_of_U3CU3Ef__amU24cache0_7() { return &___U3CU3Ef__amU24cache0_7; }
	inline void set_U3CU3Ef__amU24cache0_7(Action_1_t4091228422 * value)
	{
		___U3CU3Ef__amU24cache0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GK_PLAYER_T3461352791_H
#ifndef XCCONFIGURATIONLIST_T1100530024_H
#define XCCONFIGURATIONLIST_T1100530024_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.XCConfigurationList
struct  XCConfigurationList_t1100530024  : public PBXObject_t3397571057
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XCCONFIGURATIONLIST_T1100530024_H
#ifndef PBXSORTEDDICTIONARY_T718889789_H
#define PBXSORTEDDICTIONARY_T718889789_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.PBXSortedDictionary
struct  PBXSortedDictionary_t718889789  : public SortedDictionary_2_t4287882758
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBXSORTEDDICTIONARY_T718889789_H
#ifndef PBXPROJECT_T4035248171_H
#define PBXPROJECT_T4035248171_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.PBXProject
struct  PBXProject_t4035248171  : public PBXObject_t3397571057
{
public:
	// System.String UnityEditor.XCodeEditor.PBXProject::MAINGROUP_KEY
	String_t* ___MAINGROUP_KEY_3;
	// System.String UnityEditor.XCodeEditor.PBXProject::KNOWN_REGIONS_KEY
	String_t* ___KNOWN_REGIONS_KEY_4;
	// System.Boolean UnityEditor.XCodeEditor.PBXProject::_clearedLoc
	bool ____clearedLoc_5;

public:
	inline static int32_t get_offset_of_MAINGROUP_KEY_3() { return static_cast<int32_t>(offsetof(PBXProject_t4035248171, ___MAINGROUP_KEY_3)); }
	inline String_t* get_MAINGROUP_KEY_3() const { return ___MAINGROUP_KEY_3; }
	inline String_t** get_address_of_MAINGROUP_KEY_3() { return &___MAINGROUP_KEY_3; }
	inline void set_MAINGROUP_KEY_3(String_t* value)
	{
		___MAINGROUP_KEY_3 = value;
		Il2CppCodeGenWriteBarrier((&___MAINGROUP_KEY_3), value);
	}

	inline static int32_t get_offset_of_KNOWN_REGIONS_KEY_4() { return static_cast<int32_t>(offsetof(PBXProject_t4035248171, ___KNOWN_REGIONS_KEY_4)); }
	inline String_t* get_KNOWN_REGIONS_KEY_4() const { return ___KNOWN_REGIONS_KEY_4; }
	inline String_t** get_address_of_KNOWN_REGIONS_KEY_4() { return &___KNOWN_REGIONS_KEY_4; }
	inline void set_KNOWN_REGIONS_KEY_4(String_t* value)
	{
		___KNOWN_REGIONS_KEY_4 = value;
		Il2CppCodeGenWriteBarrier((&___KNOWN_REGIONS_KEY_4), value);
	}

	inline static int32_t get_offset_of__clearedLoc_5() { return static_cast<int32_t>(offsetof(PBXProject_t4035248171, ____clearedLoc_5)); }
	inline bool get__clearedLoc_5() const { return ____clearedLoc_5; }
	inline bool* get_address_of__clearedLoc_5() { return &____clearedLoc_5; }
	inline void set__clearedLoc_5(bool value)
	{
		____clearedLoc_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBXPROJECT_T4035248171_H
#ifndef XCBUILDCONFIGURATION_T1451066300_H
#define XCBUILDCONFIGURATION_T1451066300_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.XCBuildConfiguration
struct  XCBuildConfiguration_t1451066300  : public PBXObject_t3397571057
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XCBUILDCONFIGURATION_T1451066300_H
#ifndef GK_LEADERBOARDRESULT_T1457562782_H
#define GK_LEADERBOARDRESULT_T1457562782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GK_LeaderboardResult
struct  GK_LeaderboardResult_t1457562782  : public Result_t4170401718
{
public:
	// GK_Leaderboard GK_LeaderboardResult::_Leaderboard
	GK_Leaderboard_t591378496 * ____Leaderboard_1;

public:
	inline static int32_t get_offset_of__Leaderboard_1() { return static_cast<int32_t>(offsetof(GK_LeaderboardResult_t1457562782, ____Leaderboard_1)); }
	inline GK_Leaderboard_t591378496 * get__Leaderboard_1() const { return ____Leaderboard_1; }
	inline GK_Leaderboard_t591378496 ** get_address_of__Leaderboard_1() { return &____Leaderboard_1; }
	inline void set__Leaderboard_1(GK_Leaderboard_t591378496 * value)
	{
		____Leaderboard_1 = value;
		Il2CppCodeGenWriteBarrier((&____Leaderboard_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GK_LEADERBOARDRESULT_T1457562782_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef VECTOR4_T3319028937_H
#define VECTOR4_T3319028937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t3319028937 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t3319028937_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t3319028937  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t3319028937  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t3319028937  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t3319028937  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___zeroVector_5)); }
	inline Vector4_t3319028937  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t3319028937 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t3319028937  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___oneVector_6)); }
	inline Vector4_t3319028937  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t3319028937 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t3319028937  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t3319028937  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t3319028937 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t3319028937  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t3319028937  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t3319028937 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t3319028937  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T3319028937_H
#ifndef ISN_LOADSETLEADERBOARDSINFORESULT_T1754621461_H
#define ISN_LOADSETLEADERBOARDSINFORESULT_T1754621461_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ISN_LoadSetLeaderboardsInfoResult
struct  ISN_LoadSetLeaderboardsInfoResult_t1754621461  : public Result_t4170401718
{
public:
	// GK_LeaderboardSet ISN_LoadSetLeaderboardsInfoResult::_LeaderBoardsSet
	GK_LeaderboardSet_t3182444842 * ____LeaderBoardsSet_1;

public:
	inline static int32_t get_offset_of__LeaderBoardsSet_1() { return static_cast<int32_t>(offsetof(ISN_LoadSetLeaderboardsInfoResult_t1754621461, ____LeaderBoardsSet_1)); }
	inline GK_LeaderboardSet_t3182444842 * get__LeaderBoardsSet_1() const { return ____LeaderBoardsSet_1; }
	inline GK_LeaderboardSet_t3182444842 ** get_address_of__LeaderBoardsSet_1() { return &____LeaderBoardsSet_1; }
	inline void set__LeaderBoardsSet_1(GK_LeaderboardSet_t3182444842 * value)
	{
		____LeaderBoardsSet_1 = value;
		Il2CppCodeGenWriteBarrier((&____LeaderBoardsSet_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISN_LOADSETLEADERBOARDSINFORESULT_T1754621461_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef GK_USERINFOLOADRESULT_T3587554467_H
#define GK_USERINFOLOADRESULT_T3587554467_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GK_UserInfoLoadResult
struct  GK_UserInfoLoadResult_t3587554467  : public Result_t4170401718
{
public:
	// System.String GK_UserInfoLoadResult::_playerId
	String_t* ____playerId_1;
	// GK_Player GK_UserInfoLoadResult::_tpl
	GK_Player_t3461352791 * ____tpl_2;

public:
	inline static int32_t get_offset_of__playerId_1() { return static_cast<int32_t>(offsetof(GK_UserInfoLoadResult_t3587554467, ____playerId_1)); }
	inline String_t* get__playerId_1() const { return ____playerId_1; }
	inline String_t** get_address_of__playerId_1() { return &____playerId_1; }
	inline void set__playerId_1(String_t* value)
	{
		____playerId_1 = value;
		Il2CppCodeGenWriteBarrier((&____playerId_1), value);
	}

	inline static int32_t get_offset_of__tpl_2() { return static_cast<int32_t>(offsetof(GK_UserInfoLoadResult_t3587554467, ____tpl_2)); }
	inline GK_Player_t3461352791 * get__tpl_2() const { return ____tpl_2; }
	inline GK_Player_t3461352791 ** get_address_of__tpl_2() { return &____tpl_2; }
	inline void set__tpl_2(GK_Player_t3461352791 * value)
	{
		____tpl_2 = value;
		Il2CppCodeGenWriteBarrier((&____tpl_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GK_USERINFOLOADRESULT_T3587554467_H
#ifndef GK_PLAYERSIGNATURERESULT_T1029544157_H
#define GK_PLAYERSIGNATURERESULT_T1029544157_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GK_PlayerSignatureResult
struct  GK_PlayerSignatureResult_t1029544157  : public Result_t4170401718
{
public:
	// System.String GK_PlayerSignatureResult::_PublicKeyUrl
	String_t* ____PublicKeyUrl_1;
	// System.Byte[] GK_PlayerSignatureResult::_Signature
	ByteU5BU5D_t4116647657* ____Signature_2;
	// System.Byte[] GK_PlayerSignatureResult::_Salt
	ByteU5BU5D_t4116647657* ____Salt_3;
	// System.Int64 GK_PlayerSignatureResult::_Timestamp
	int64_t ____Timestamp_4;

public:
	inline static int32_t get_offset_of__PublicKeyUrl_1() { return static_cast<int32_t>(offsetof(GK_PlayerSignatureResult_t1029544157, ____PublicKeyUrl_1)); }
	inline String_t* get__PublicKeyUrl_1() const { return ____PublicKeyUrl_1; }
	inline String_t** get_address_of__PublicKeyUrl_1() { return &____PublicKeyUrl_1; }
	inline void set__PublicKeyUrl_1(String_t* value)
	{
		____PublicKeyUrl_1 = value;
		Il2CppCodeGenWriteBarrier((&____PublicKeyUrl_1), value);
	}

	inline static int32_t get_offset_of__Signature_2() { return static_cast<int32_t>(offsetof(GK_PlayerSignatureResult_t1029544157, ____Signature_2)); }
	inline ByteU5BU5D_t4116647657* get__Signature_2() const { return ____Signature_2; }
	inline ByteU5BU5D_t4116647657** get_address_of__Signature_2() { return &____Signature_2; }
	inline void set__Signature_2(ByteU5BU5D_t4116647657* value)
	{
		____Signature_2 = value;
		Il2CppCodeGenWriteBarrier((&____Signature_2), value);
	}

	inline static int32_t get_offset_of__Salt_3() { return static_cast<int32_t>(offsetof(GK_PlayerSignatureResult_t1029544157, ____Salt_3)); }
	inline ByteU5BU5D_t4116647657* get__Salt_3() const { return ____Salt_3; }
	inline ByteU5BU5D_t4116647657** get_address_of__Salt_3() { return &____Salt_3; }
	inline void set__Salt_3(ByteU5BU5D_t4116647657* value)
	{
		____Salt_3 = value;
		Il2CppCodeGenWriteBarrier((&____Salt_3), value);
	}

	inline static int32_t get_offset_of__Timestamp_4() { return static_cast<int32_t>(offsetof(GK_PlayerSignatureResult_t1029544157, ____Timestamp_4)); }
	inline int64_t get__Timestamp_4() const { return ____Timestamp_4; }
	inline int64_t* get_address_of__Timestamp_4() { return &____Timestamp_4; }
	inline void set__Timestamp_4(int64_t value)
	{
		____Timestamp_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GK_PLAYERSIGNATURERESULT_T1029544157_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef GK_ACHIEVEMENTPROGRESSRESULT_T451478438_H
#define GK_ACHIEVEMENTPROGRESSRESULT_T451478438_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GK_AchievementProgressResult
struct  GK_AchievementProgressResult_t451478438  : public Result_t4170401718
{
public:
	// GK_AchievementTemplate GK_AchievementProgressResult::_tpl
	GK_AchievementTemplate_t3624697896 * ____tpl_1;

public:
	inline static int32_t get_offset_of__tpl_1() { return static_cast<int32_t>(offsetof(GK_AchievementProgressResult_t451478438, ____tpl_1)); }
	inline GK_AchievementTemplate_t3624697896 * get__tpl_1() const { return ____tpl_1; }
	inline GK_AchievementTemplate_t3624697896 ** get_address_of__tpl_1() { return &____tpl_1; }
	inline void set__tpl_1(GK_AchievementTemplate_t3624697896 * value)
	{
		____tpl_1 = value;
		Il2CppCodeGenWriteBarrier((&____tpl_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GK_ACHIEVEMENTPROGRESSRESULT_T451478438_H
#ifndef PBXRESOLVERREVERSEINDEX_T3234151459_H
#define PBXRESOLVERREVERSEINDEX_T3234151459_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.PBXResolver/PBXResolverReverseIndex
struct  PBXResolverReverseIndex_t3234151459  : public Dictionary_2_t1632706988
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBXRESOLVERREVERSEINDEX_T3234151459_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef PBXREFERENCEPROXY_T1939220145_H
#define PBXREFERENCEPROXY_T1939220145_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.PBXReferenceProxy
struct  PBXReferenceProxy_t1939220145  : public PBXObject_t3397571057
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBXREFERENCEPROXY_T1939220145_H
#ifndef PBXCONTAINERITEMPROXY_T3112265051_H
#define PBXCONTAINERITEMPROXY_T3112265051_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.PBXContainerItemProxy
struct  PBXContainerItemProxy_t3112265051  : public PBXObject_t3397571057
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBXCONTAINERITEMPROXY_T3112265051_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef PBXNATIVETARGET_T793394962_H
#define PBXNATIVETARGET_T793394962_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.PBXNativeTarget
struct  PBXNativeTarget_t793394962  : public PBXObject_t3397571057
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBXNATIVETARGET_T793394962_H
#ifndef PBXLIST_T4289188522_H
#define PBXLIST_T4289188522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.PBXList
struct  PBXList_t4289188522  : public ArrayList_t2718874744
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBXLIST_T4289188522_H
#ifndef PBXGROUP_T2128436724_H
#define PBXGROUP_T2128436724_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.PBXGroup
struct  PBXGroup_t2128436724  : public PBXObject_t3397571057
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBXGROUP_T2128436724_H
#ifndef PBXFILEREFERENCE_T491780957_H
#define PBXFILEREFERENCE_T491780957_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.PBXFileReference
struct  PBXFileReference_t491780957  : public PBXObject_t3397571057
{
public:
	// System.String UnityEditor.XCodeEditor.PBXFileReference::compilerFlags
	String_t* ___compilerFlags_9;
	// System.String UnityEditor.XCodeEditor.PBXFileReference::buildPhase
	String_t* ___buildPhase_10;
	// System.Collections.Generic.Dictionary`2<UnityEditor.XCodeEditor.TreeEnum,System.String> UnityEditor.XCodeEditor.PBXFileReference::trees
	Dictionary_2_t1851544083 * ___trees_11;

public:
	inline static int32_t get_offset_of_compilerFlags_9() { return static_cast<int32_t>(offsetof(PBXFileReference_t491780957, ___compilerFlags_9)); }
	inline String_t* get_compilerFlags_9() const { return ___compilerFlags_9; }
	inline String_t** get_address_of_compilerFlags_9() { return &___compilerFlags_9; }
	inline void set_compilerFlags_9(String_t* value)
	{
		___compilerFlags_9 = value;
		Il2CppCodeGenWriteBarrier((&___compilerFlags_9), value);
	}

	inline static int32_t get_offset_of_buildPhase_10() { return static_cast<int32_t>(offsetof(PBXFileReference_t491780957, ___buildPhase_10)); }
	inline String_t* get_buildPhase_10() const { return ___buildPhase_10; }
	inline String_t** get_address_of_buildPhase_10() { return &___buildPhase_10; }
	inline void set_buildPhase_10(String_t* value)
	{
		___buildPhase_10 = value;
		Il2CppCodeGenWriteBarrier((&___buildPhase_10), value);
	}

	inline static int32_t get_offset_of_trees_11() { return static_cast<int32_t>(offsetof(PBXFileReference_t491780957, ___trees_11)); }
	inline Dictionary_2_t1851544083 * get_trees_11() const { return ___trees_11; }
	inline Dictionary_2_t1851544083 ** get_address_of_trees_11() { return &___trees_11; }
	inline void set_trees_11(Dictionary_2_t1851544083 * value)
	{
		___trees_11 = value;
		Il2CppCodeGenWriteBarrier((&___trees_11), value);
	}
};

struct PBXFileReference_t491780957_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.String> UnityEditor.XCodeEditor.PBXFileReference::typeNames
	Dictionary_2_t1632706988 * ___typeNames_12;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> UnityEditor.XCodeEditor.PBXFileReference::typePhases
	Dictionary_2_t1632706988 * ___typePhases_13;

public:
	inline static int32_t get_offset_of_typeNames_12() { return static_cast<int32_t>(offsetof(PBXFileReference_t491780957_StaticFields, ___typeNames_12)); }
	inline Dictionary_2_t1632706988 * get_typeNames_12() const { return ___typeNames_12; }
	inline Dictionary_2_t1632706988 ** get_address_of_typeNames_12() { return &___typeNames_12; }
	inline void set_typeNames_12(Dictionary_2_t1632706988 * value)
	{
		___typeNames_12 = value;
		Il2CppCodeGenWriteBarrier((&___typeNames_12), value);
	}

	inline static int32_t get_offset_of_typePhases_13() { return static_cast<int32_t>(offsetof(PBXFileReference_t491780957_StaticFields, ___typePhases_13)); }
	inline Dictionary_2_t1632706988 * get_typePhases_13() const { return ___typePhases_13; }
	inline Dictionary_2_t1632706988 ** get_address_of_typePhases_13() { return &___typePhases_13; }
	inline void set_typePhases_13(Dictionary_2_t1632706988 * value)
	{
		___typePhases_13 = value;
		Il2CppCodeGenWriteBarrier((&___typePhases_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBXFILEREFERENCE_T491780957_H
#ifndef PBXDICTIONARY_T2078602241_H
#define PBXDICTIONARY_T2078602241_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.PBXDictionary
struct  PBXDictionary_t2078602241  : public Dictionary_2_t2865362463
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBXDICTIONARY_T2078602241_H
#ifndef PBXBUILDFILE_T2610392694_H
#define PBXBUILDFILE_T2610392694_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.PBXBuildFile
struct  PBXBuildFile_t2610392694  : public PBXObject_t3397571057
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBXBUILDFILE_T2610392694_H
#ifndef PBXBUILDPHASE_T3890765868_H
#define PBXBUILDPHASE_T3890765868_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.PBXBuildPhase
struct  PBXBuildPhase_t3890765868  : public PBXObject_t3397571057
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBXBUILDPHASE_T3890765868_H
#ifndef U24ARRAYTYPEU3D12_T2488454196_H
#define U24ARRAYTYPEU3D12_T2488454196_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=12
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D12_t2488454196 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D12_t2488454196__padding[12];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D12_T2488454196_H
#ifndef GK_PHOTOSIZE_T1682985923_H
#define GK_PHOTOSIZE_T1682985923_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GK_PhotoSize
struct  GK_PhotoSize_t1682985923 
{
public:
	// System.Int32 GK_PhotoSize::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GK_PhotoSize_t1682985923, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GK_PHOTOSIZE_T1682985923_H
#ifndef GK_TIMESPAN_T2679980730_H
#define GK_TIMESPAN_T2679980730_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GK_TimeSpan
struct  GK_TimeSpan_t2679980730 
{
public:
	// System.Int32 GK_TimeSpan::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GK_TimeSpan_t2679980730, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GK_TIMESPAN_T2679980730_H
#ifndef PLISTVALUETYPES_T4090938833_H
#define PLISTVALUETYPES_T4090938833_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSDeploy.PlistValueTypes
struct  PlistValueTypes_t4090938833 
{
public:
	// System.Int32 SA.IOSDeploy.PlistValueTypes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PlistValueTypes_t4090938833, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLISTVALUETYPES_T4090938833_H
#ifndef IOSLIBRARY_T2820135979_H
#define IOSLIBRARY_T2820135979_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSDeploy.iOSLibrary
struct  iOSLibrary_t2820135979 
{
public:
	// System.Int32 SA.IOSDeploy.iOSLibrary::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(iOSLibrary_t2820135979, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSLIBRARY_T2820135979_H
#ifndef IOSFRAMEWORK_T87174759_H
#define IOSFRAMEWORK_T87174759_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSDeploy.iOSFramework
struct  iOSFramework_t87174759 
{
public:
	// System.Int32 SA.IOSDeploy.iOSFramework::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(iOSFramework_t87174759, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSFRAMEWORK_T87174759_H
#ifndef TRANSACTIONSHANDLINGMODE_T1662146145_H
#define TRANSACTIONSHANDLINGMODE_T1662146145_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSNative.StoreKit.TransactionsHandlingMode
struct  TransactionsHandlingMode_t1662146145 
{
public:
	// System.Int32 SA.IOSNative.StoreKit.TransactionsHandlingMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TransactionsHandlingMode_t1662146145, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSACTIONSHANDLINGMODE_T1662146145_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255366_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255366  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=12 <PrivateImplementationDetails>::$field-7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46
	U24ArrayTypeU3D12_t2488454196  ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0;

public:
	inline static int32_t get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0)); }
	inline U24ArrayTypeU3D12_t2488454196  get_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() const { return ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline U24ArrayTypeU3D12_t2488454196 * get_address_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return &___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline void set_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(U24ArrayTypeU3D12_t2488454196  value)
	{
		___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255366_H
#ifndef GK_MATCHTYPE_T164054226_H
#define GK_MATCHTYPE_T164054226_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GK_MatchType
struct  GK_MatchType_t164054226 
{
public:
	// System.Int32 GK_MatchType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GK_MatchType_t164054226, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GK_MATCHTYPE_T164054226_H
#ifndef VERTEXHELPER_T2453304189_H
#define VERTEXHELPER_T2453304189_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.VertexHelper
struct  VertexHelper_t2453304189  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Positions
	List_1_t899420910 * ___m_Positions_0;
	// System.Collections.Generic.List`1<UnityEngine.Color32> UnityEngine.UI.VertexHelper::m_Colors
	List_1_t4072576034 * ___m_Colors_1;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv0S
	List_1_t3628304265 * ___m_Uv0S_2;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv1S
	List_1_t3628304265 * ___m_Uv1S_3;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv2S
	List_1_t3628304265 * ___m_Uv2S_4;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv3S
	List_1_t3628304265 * ___m_Uv3S_5;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Normals
	List_1_t899420910 * ___m_Normals_6;
	// System.Collections.Generic.List`1<UnityEngine.Vector4> UnityEngine.UI.VertexHelper::m_Tangents
	List_1_t496136383 * ___m_Tangents_7;
	// System.Collections.Generic.List`1<System.Int32> UnityEngine.UI.VertexHelper::m_Indices
	List_1_t128053199 * ___m_Indices_8;

public:
	inline static int32_t get_offset_of_m_Positions_0() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Positions_0)); }
	inline List_1_t899420910 * get_m_Positions_0() const { return ___m_Positions_0; }
	inline List_1_t899420910 ** get_address_of_m_Positions_0() { return &___m_Positions_0; }
	inline void set_m_Positions_0(List_1_t899420910 * value)
	{
		___m_Positions_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Positions_0), value);
	}

	inline static int32_t get_offset_of_m_Colors_1() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Colors_1)); }
	inline List_1_t4072576034 * get_m_Colors_1() const { return ___m_Colors_1; }
	inline List_1_t4072576034 ** get_address_of_m_Colors_1() { return &___m_Colors_1; }
	inline void set_m_Colors_1(List_1_t4072576034 * value)
	{
		___m_Colors_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Colors_1), value);
	}

	inline static int32_t get_offset_of_m_Uv0S_2() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Uv0S_2)); }
	inline List_1_t3628304265 * get_m_Uv0S_2() const { return ___m_Uv0S_2; }
	inline List_1_t3628304265 ** get_address_of_m_Uv0S_2() { return &___m_Uv0S_2; }
	inline void set_m_Uv0S_2(List_1_t3628304265 * value)
	{
		___m_Uv0S_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv0S_2), value);
	}

	inline static int32_t get_offset_of_m_Uv1S_3() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Uv1S_3)); }
	inline List_1_t3628304265 * get_m_Uv1S_3() const { return ___m_Uv1S_3; }
	inline List_1_t3628304265 ** get_address_of_m_Uv1S_3() { return &___m_Uv1S_3; }
	inline void set_m_Uv1S_3(List_1_t3628304265 * value)
	{
		___m_Uv1S_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv1S_3), value);
	}

	inline static int32_t get_offset_of_m_Uv2S_4() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Uv2S_4)); }
	inline List_1_t3628304265 * get_m_Uv2S_4() const { return ___m_Uv2S_4; }
	inline List_1_t3628304265 ** get_address_of_m_Uv2S_4() { return &___m_Uv2S_4; }
	inline void set_m_Uv2S_4(List_1_t3628304265 * value)
	{
		___m_Uv2S_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv2S_4), value);
	}

	inline static int32_t get_offset_of_m_Uv3S_5() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Uv3S_5)); }
	inline List_1_t3628304265 * get_m_Uv3S_5() const { return ___m_Uv3S_5; }
	inline List_1_t3628304265 ** get_address_of_m_Uv3S_5() { return &___m_Uv3S_5; }
	inline void set_m_Uv3S_5(List_1_t3628304265 * value)
	{
		___m_Uv3S_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv3S_5), value);
	}

	inline static int32_t get_offset_of_m_Normals_6() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Normals_6)); }
	inline List_1_t899420910 * get_m_Normals_6() const { return ___m_Normals_6; }
	inline List_1_t899420910 ** get_address_of_m_Normals_6() { return &___m_Normals_6; }
	inline void set_m_Normals_6(List_1_t899420910 * value)
	{
		___m_Normals_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Normals_6), value);
	}

	inline static int32_t get_offset_of_m_Tangents_7() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Tangents_7)); }
	inline List_1_t496136383 * get_m_Tangents_7() const { return ___m_Tangents_7; }
	inline List_1_t496136383 ** get_address_of_m_Tangents_7() { return &___m_Tangents_7; }
	inline void set_m_Tangents_7(List_1_t496136383 * value)
	{
		___m_Tangents_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Tangents_7), value);
	}

	inline static int32_t get_offset_of_m_Indices_8() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Indices_8)); }
	inline List_1_t128053199 * get_m_Indices_8() const { return ___m_Indices_8; }
	inline List_1_t128053199 ** get_address_of_m_Indices_8() { return &___m_Indices_8; }
	inline void set_m_Indices_8(List_1_t128053199 * value)
	{
		___m_Indices_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Indices_8), value);
	}
};

struct VertexHelper_t2453304189_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.UI.VertexHelper::s_DefaultTangent
	Vector4_t3319028937  ___s_DefaultTangent_9;
	// UnityEngine.Vector3 UnityEngine.UI.VertexHelper::s_DefaultNormal
	Vector3_t3722313464  ___s_DefaultNormal_10;

public:
	inline static int32_t get_offset_of_s_DefaultTangent_9() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189_StaticFields, ___s_DefaultTangent_9)); }
	inline Vector4_t3319028937  get_s_DefaultTangent_9() const { return ___s_DefaultTangent_9; }
	inline Vector4_t3319028937 * get_address_of_s_DefaultTangent_9() { return &___s_DefaultTangent_9; }
	inline void set_s_DefaultTangent_9(Vector4_t3319028937  value)
	{
		___s_DefaultTangent_9 = value;
	}

	inline static int32_t get_offset_of_s_DefaultNormal_10() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189_StaticFields, ___s_DefaultNormal_10)); }
	inline Vector3_t3722313464  get_s_DefaultNormal_10() const { return ___s_DefaultNormal_10; }
	inline Vector3_t3722313464 * get_address_of_s_DefaultNormal_10() { return &___s_DefaultNormal_10; }
	inline void set_s_DefaultNormal_10(Vector3_t3722313464  value)
	{
		___s_DefaultNormal_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXHELPER_T2453304189_H
#ifndef IOSGALLERYLOADIMAGEFORMAT_T3491991360_H
#define IOSGALLERYLOADIMAGEFORMAT_T3491991360_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IOSGalleryLoadImageFormat
struct  IOSGalleryLoadImageFormat_t3491991360 
{
public:
	// System.Int32 IOSGalleryLoadImageFormat::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(IOSGalleryLoadImageFormat_t3491991360, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSGALLERYLOADIMAGEFORMAT_T3491991360_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef RAY_T3785851493_H
#define RAY_T3785851493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_t3785851493 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t3722313464  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t3722313464  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Origin_0)); }
	inline Vector3_t3722313464  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t3722313464 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t3722313464  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Direction_1)); }
	inline Vector3_t3722313464  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t3722313464 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t3722313464  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_T3785851493_H
#ifndef FLAGTYPE_T2855246900_H
#define FLAGTYPE_T2855246900_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSDeploy.FlagType
struct  FlagType_t2855246900 
{
public:
	// System.Int32 SA.IOSDeploy.FlagType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FlagType_t2855246900, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLAGTYPE_T2855246900_H
#ifndef GK_INVITERECIPIENTRESPONSE_T1393324106_H
#define GK_INVITERECIPIENTRESPONSE_T1393324106_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GK_InviteRecipientResponse
struct  GK_InviteRecipientResponse_t1393324106 
{
public:
	// System.Int32 GK_InviteRecipientResponse::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GK_InviteRecipientResponse_t1393324106, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GK_INVITERECIPIENTRESPONSE_T1393324106_H
#ifndef RAYCASTHIT2D_T2279581989_H
#define RAYCASTHIT2D_T2279581989_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit2D
struct  RaycastHit2D_t2279581989 
{
public:
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Centroid
	Vector2_t2156229523  ___m_Centroid_0;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Point
	Vector2_t2156229523  ___m_Point_1;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Normal
	Vector2_t2156229523  ___m_Normal_2;
	// System.Single UnityEngine.RaycastHit2D::m_Distance
	float ___m_Distance_3;
	// System.Single UnityEngine.RaycastHit2D::m_Fraction
	float ___m_Fraction_4;
	// UnityEngine.Collider2D UnityEngine.RaycastHit2D::m_Collider
	Collider2D_t2806799626 * ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Centroid_0() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Centroid_0)); }
	inline Vector2_t2156229523  get_m_Centroid_0() const { return ___m_Centroid_0; }
	inline Vector2_t2156229523 * get_address_of_m_Centroid_0() { return &___m_Centroid_0; }
	inline void set_m_Centroid_0(Vector2_t2156229523  value)
	{
		___m_Centroid_0 = value;
	}

	inline static int32_t get_offset_of_m_Point_1() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Point_1)); }
	inline Vector2_t2156229523  get_m_Point_1() const { return ___m_Point_1; }
	inline Vector2_t2156229523 * get_address_of_m_Point_1() { return &___m_Point_1; }
	inline void set_m_Point_1(Vector2_t2156229523  value)
	{
		___m_Point_1 = value;
	}

	inline static int32_t get_offset_of_m_Normal_2() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Normal_2)); }
	inline Vector2_t2156229523  get_m_Normal_2() const { return ___m_Normal_2; }
	inline Vector2_t2156229523 * get_address_of_m_Normal_2() { return &___m_Normal_2; }
	inline void set_m_Normal_2(Vector2_t2156229523  value)
	{
		___m_Normal_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_Fraction_4() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Fraction_4)); }
	inline float get_m_Fraction_4() const { return ___m_Fraction_4; }
	inline float* get_address_of_m_Fraction_4() { return &___m_Fraction_4; }
	inline void set_m_Fraction_4(float value)
	{
		___m_Fraction_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Collider_5)); }
	inline Collider2D_t2806799626 * get_m_Collider_5() const { return ___m_Collider_5; }
	inline Collider2D_t2806799626 ** get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(Collider2D_t2806799626 * value)
	{
		___m_Collider_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RaycastHit2D
struct RaycastHit2D_t2279581989_marshaled_pinvoke
{
	Vector2_t2156229523  ___m_Centroid_0;
	Vector2_t2156229523  ___m_Point_1;
	Vector2_t2156229523  ___m_Normal_2;
	float ___m_Distance_3;
	float ___m_Fraction_4;
	Collider2D_t2806799626 * ___m_Collider_5;
};
// Native definition for COM marshalling of UnityEngine.RaycastHit2D
struct RaycastHit2D_t2279581989_marshaled_com
{
	Vector2_t2156229523  ___m_Centroid_0;
	Vector2_t2156229523  ___m_Point_1;
	Vector2_t2156229523  ___m_Normal_2;
	float ___m_Distance_3;
	float ___m_Fraction_4;
	Collider2D_t2806799626 * ___m_Collider_5;
};
#endif // RAYCASTHIT2D_T2279581989_H
#ifndef ISN_SOOMLAACTION_T722285317_H
#define ISN_SOOMLAACTION_T722285317_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ISN_SoomlaAction
struct  ISN_SoomlaAction_t722285317 
{
public:
	// System.Int32 ISN_SoomlaAction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ISN_SoomlaAction_t722285317, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISN_SOOMLAACTION_T722285317_H
#ifndef RAYCASTHIT_T1056001966_H
#define RAYCASTHIT_T1056001966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit
struct  RaycastHit_t1056001966 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t3722313464  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t3722313464  ___m_Normal_1;
	// System.Int32 UnityEngine.RaycastHit::m_FaceID
	int32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_t2156229523  ___m_UV_4;
	// UnityEngine.Collider UnityEngine.RaycastHit::m_Collider
	Collider_t1773347010 * ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Point_0)); }
	inline Vector3_t3722313464  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_t3722313464 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_t3722313464  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Normal_1)); }
	inline Vector3_t3722313464  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t3722313464 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t3722313464  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_FaceID_2)); }
	inline int32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline int32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(int32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_UV_4)); }
	inline Vector2_t2156229523  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_t2156229523 * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_t2156229523  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Collider_5)); }
	inline Collider_t1773347010 * get_m_Collider_5() const { return ___m_Collider_5; }
	inline Collider_t1773347010 ** get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(Collider_t1773347010 * value)
	{
		___m_Collider_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RaycastHit
struct RaycastHit_t1056001966_marshaled_pinvoke
{
	Vector3_t3722313464  ___m_Point_0;
	Vector3_t3722313464  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t2156229523  ___m_UV_4;
	Collider_t1773347010 * ___m_Collider_5;
};
// Native definition for COM marshalling of UnityEngine.RaycastHit
struct RaycastHit_t1056001966_marshaled_com
{
	Vector3_t3722313464  ___m_Point_0;
	Vector3_t3722313464  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t2156229523  ___m_UV_4;
	Collider_t1773347010 * ___m_Collider_5;
};
#endif // RAYCASTHIT_T1056001966_H
#ifndef PBXVARIANTGROUP_T2964284507_H
#define PBXVARIANTGROUP_T2964284507_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.PBXVariantGroup
struct  PBXVariantGroup_t2964284507  : public PBXGroup_t2128436724
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBXVARIANTGROUP_T2964284507_H
#ifndef PLISTTYPE_T2342801093_H
#define PLISTTYPE_T2342801093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlistCS.plistType
struct  plistType_t2342801093 
{
public:
	// System.Int32 PlistCS.plistType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(plistType_t2342801093, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLISTTYPE_T2342801093_H
#ifndef TREEENUM_T2111024294_H
#define TREEENUM_T2111024294_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.TreeEnum
struct  TreeEnum_t2111024294 
{
public:
	// System.Int32 UnityEditor.XCodeEditor.TreeEnum::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TreeEnum_t2111024294, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TREEENUM_T2111024294_H
#ifndef PBXFRAMEWORKSBUILDPHASE_T1675361410_H
#define PBXFRAMEWORKSBUILDPHASE_T1675361410_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.PBXFrameworksBuildPhase
struct  PBXFrameworksBuildPhase_t1675361410  : public PBXBuildPhase_t3890765868
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBXFRAMEWORKSBUILDPHASE_T1675361410_H
#ifndef ISN_SOOMLAPROVIDER_T2652759424_H
#define ISN_SOOMLAPROVIDER_T2652759424_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ISN_SoomlaProvider
struct  ISN_SoomlaProvider_t2652759424 
{
public:
	// System.Int32 ISN_SoomlaProvider::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ISN_SoomlaProvider_t2652759424, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISN_SOOMLAPROVIDER_T2652759424_H
#ifndef ISN_SOOMLAEVENT_T315777455_H
#define ISN_SOOMLAEVENT_T315777455_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ISN_SoomlaEvent
struct  ISN_SoomlaEvent_t315777455 
{
public:
	// System.Int32 ISN_SoomlaEvent::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ISN_SoomlaEvent_t315777455, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISN_SOOMLAEVENT_T315777455_H
#ifndef PBXCOPYFILESBUILDPHASE_T4167310815_H
#define PBXCOPYFILESBUILDPHASE_T4167310815_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.PBXCopyFilesBuildPhase
struct  PBXCopyFilesBuildPhase_t4167310815  : public PBXBuildPhase_t3890765868
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBXCOPYFILESBUILDPHASE_T4167310815_H
#ifndef GK_COLLECTIONTYPE_T4065955501_H
#define GK_COLLECTIONTYPE_T4065955501_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GK_CollectionType
struct  GK_CollectionType_t4065955501 
{
public:
	// System.Int32 GK_CollectionType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GK_CollectionType_t4065955501, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GK_COLLECTIONTYPE_T4065955501_H
#ifndef PBXRESOURCESBUILDPHASE_T3333333421_H
#define PBXRESOURCESBUILDPHASE_T3333333421_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.PBXResourcesBuildPhase
struct  PBXResourcesBuildPhase_t3333333421  : public PBXBuildPhase_t3890765868
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBXRESOURCESBUILDPHASE_T3333333421_H
#ifndef PBXSHELLSCRIPTBUILDPHASE_T3527688836_H
#define PBXSHELLSCRIPTBUILDPHASE_T3527688836_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.PBXShellScriptBuildPhase
struct  PBXShellScriptBuildPhase_t3527688836  : public PBXBuildPhase_t3890765868
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBXSHELLSCRIPTBUILDPHASE_T3527688836_H
#ifndef PBXSOURCESBUILDPHASE_T187938102_H
#define PBXSOURCESBUILDPHASE_T187938102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.PBXSourcesBuildPhase
struct  PBXSourcesBuildPhase_t187938102  : public PBXBuildPhase_t3890765868
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBXSOURCESBUILDPHASE_T187938102_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef FRAMEWORK_T3395257401_H
#define FRAMEWORK_T3395257401_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSDeploy.Framework
struct  Framework_t3395257401  : public RuntimeObject
{
public:
	// System.Boolean SA.IOSDeploy.Framework::IsOpen
	bool ___IsOpen_0;
	// SA.IOSDeploy.iOSFramework SA.IOSDeploy.Framework::Type
	int32_t ___Type_1;
	// System.Boolean SA.IOSDeploy.Framework::IsOptional
	bool ___IsOptional_2;
	// System.Boolean SA.IOSDeploy.Framework::IsEmbeded
	bool ___IsEmbeded_3;

public:
	inline static int32_t get_offset_of_IsOpen_0() { return static_cast<int32_t>(offsetof(Framework_t3395257401, ___IsOpen_0)); }
	inline bool get_IsOpen_0() const { return ___IsOpen_0; }
	inline bool* get_address_of_IsOpen_0() { return &___IsOpen_0; }
	inline void set_IsOpen_0(bool value)
	{
		___IsOpen_0 = value;
	}

	inline static int32_t get_offset_of_Type_1() { return static_cast<int32_t>(offsetof(Framework_t3395257401, ___Type_1)); }
	inline int32_t get_Type_1() const { return ___Type_1; }
	inline int32_t* get_address_of_Type_1() { return &___Type_1; }
	inline void set_Type_1(int32_t value)
	{
		___Type_1 = value;
	}

	inline static int32_t get_offset_of_IsOptional_2() { return static_cast<int32_t>(offsetof(Framework_t3395257401, ___IsOptional_2)); }
	inline bool get_IsOptional_2() const { return ___IsOptional_2; }
	inline bool* get_address_of_IsOptional_2() { return &___IsOptional_2; }
	inline void set_IsOptional_2(bool value)
	{
		___IsOptional_2 = value;
	}

	inline static int32_t get_offset_of_IsEmbeded_3() { return static_cast<int32_t>(offsetof(Framework_t3395257401, ___IsEmbeded_3)); }
	inline bool get_IsEmbeded_3() const { return ___IsEmbeded_3; }
	inline bool* get_address_of_IsEmbeded_3() { return &___IsEmbeded_3; }
	inline void set_IsEmbeded_3(bool value)
	{
		___IsEmbeded_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRAMEWORK_T3395257401_H
#ifndef VARIABLE_T1035990833_H
#define VARIABLE_T1035990833_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSDeploy.Variable
struct  Variable_t1035990833  : public RuntimeObject
{
public:
	// System.Boolean SA.IOSDeploy.Variable::IsOpen
	bool ___IsOpen_0;
	// System.Boolean SA.IOSDeploy.Variable::IsListOpen
	bool ___IsListOpen_1;
	// System.String SA.IOSDeploy.Variable::Name
	String_t* ___Name_2;
	// SA.IOSDeploy.PlistValueTypes SA.IOSDeploy.Variable::Type
	int32_t ___Type_3;
	// System.String SA.IOSDeploy.Variable::StringValue
	String_t* ___StringValue_4;
	// System.Int32 SA.IOSDeploy.Variable::IntegerValue
	int32_t ___IntegerValue_5;
	// System.Single SA.IOSDeploy.Variable::FloatValue
	float ___FloatValue_6;
	// System.Boolean SA.IOSDeploy.Variable::BooleanValue
	bool ___BooleanValue_7;
	// System.Collections.Generic.List`1<System.String> SA.IOSDeploy.Variable::ChildrensIds
	List_1_t3319525431 * ___ChildrensIds_8;

public:
	inline static int32_t get_offset_of_IsOpen_0() { return static_cast<int32_t>(offsetof(Variable_t1035990833, ___IsOpen_0)); }
	inline bool get_IsOpen_0() const { return ___IsOpen_0; }
	inline bool* get_address_of_IsOpen_0() { return &___IsOpen_0; }
	inline void set_IsOpen_0(bool value)
	{
		___IsOpen_0 = value;
	}

	inline static int32_t get_offset_of_IsListOpen_1() { return static_cast<int32_t>(offsetof(Variable_t1035990833, ___IsListOpen_1)); }
	inline bool get_IsListOpen_1() const { return ___IsListOpen_1; }
	inline bool* get_address_of_IsListOpen_1() { return &___IsListOpen_1; }
	inline void set_IsListOpen_1(bool value)
	{
		___IsListOpen_1 = value;
	}

	inline static int32_t get_offset_of_Name_2() { return static_cast<int32_t>(offsetof(Variable_t1035990833, ___Name_2)); }
	inline String_t* get_Name_2() const { return ___Name_2; }
	inline String_t** get_address_of_Name_2() { return &___Name_2; }
	inline void set_Name_2(String_t* value)
	{
		___Name_2 = value;
		Il2CppCodeGenWriteBarrier((&___Name_2), value);
	}

	inline static int32_t get_offset_of_Type_3() { return static_cast<int32_t>(offsetof(Variable_t1035990833, ___Type_3)); }
	inline int32_t get_Type_3() const { return ___Type_3; }
	inline int32_t* get_address_of_Type_3() { return &___Type_3; }
	inline void set_Type_3(int32_t value)
	{
		___Type_3 = value;
	}

	inline static int32_t get_offset_of_StringValue_4() { return static_cast<int32_t>(offsetof(Variable_t1035990833, ___StringValue_4)); }
	inline String_t* get_StringValue_4() const { return ___StringValue_4; }
	inline String_t** get_address_of_StringValue_4() { return &___StringValue_4; }
	inline void set_StringValue_4(String_t* value)
	{
		___StringValue_4 = value;
		Il2CppCodeGenWriteBarrier((&___StringValue_4), value);
	}

	inline static int32_t get_offset_of_IntegerValue_5() { return static_cast<int32_t>(offsetof(Variable_t1035990833, ___IntegerValue_5)); }
	inline int32_t get_IntegerValue_5() const { return ___IntegerValue_5; }
	inline int32_t* get_address_of_IntegerValue_5() { return &___IntegerValue_5; }
	inline void set_IntegerValue_5(int32_t value)
	{
		___IntegerValue_5 = value;
	}

	inline static int32_t get_offset_of_FloatValue_6() { return static_cast<int32_t>(offsetof(Variable_t1035990833, ___FloatValue_6)); }
	inline float get_FloatValue_6() const { return ___FloatValue_6; }
	inline float* get_address_of_FloatValue_6() { return &___FloatValue_6; }
	inline void set_FloatValue_6(float value)
	{
		___FloatValue_6 = value;
	}

	inline static int32_t get_offset_of_BooleanValue_7() { return static_cast<int32_t>(offsetof(Variable_t1035990833, ___BooleanValue_7)); }
	inline bool get_BooleanValue_7() const { return ___BooleanValue_7; }
	inline bool* get_address_of_BooleanValue_7() { return &___BooleanValue_7; }
	inline void set_BooleanValue_7(bool value)
	{
		___BooleanValue_7 = value;
	}

	inline static int32_t get_offset_of_ChildrensIds_8() { return static_cast<int32_t>(offsetof(Variable_t1035990833, ___ChildrensIds_8)); }
	inline List_1_t3319525431 * get_ChildrensIds_8() const { return ___ChildrensIds_8; }
	inline List_1_t3319525431 ** get_address_of_ChildrensIds_8() { return &___ChildrensIds_8; }
	inline void set_ChildrensIds_8(List_1_t3319525431 * value)
	{
		___ChildrensIds_8 = value;
		Il2CppCodeGenWriteBarrier((&___ChildrensIds_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VARIABLE_T1035990833_H
#ifndef FLAG_T3377423384_H
#define FLAG_T3377423384_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSDeploy.Flag
struct  Flag_t3377423384  : public RuntimeObject
{
public:
	// System.Boolean SA.IOSDeploy.Flag::IsOpen
	bool ___IsOpen_0;
	// System.String SA.IOSDeploy.Flag::Name
	String_t* ___Name_1;
	// SA.IOSDeploy.FlagType SA.IOSDeploy.Flag::Type
	int32_t ___Type_2;

public:
	inline static int32_t get_offset_of_IsOpen_0() { return static_cast<int32_t>(offsetof(Flag_t3377423384, ___IsOpen_0)); }
	inline bool get_IsOpen_0() const { return ___IsOpen_0; }
	inline bool* get_address_of_IsOpen_0() { return &___IsOpen_0; }
	inline void set_IsOpen_0(bool value)
	{
		___IsOpen_0 = value;
	}

	inline static int32_t get_offset_of_Name_1() { return static_cast<int32_t>(offsetof(Flag_t3377423384, ___Name_1)); }
	inline String_t* get_Name_1() const { return ___Name_1; }
	inline String_t** get_address_of_Name_1() { return &___Name_1; }
	inline void set_Name_1(String_t* value)
	{
		___Name_1 = value;
		Il2CppCodeGenWriteBarrier((&___Name_1), value);
	}

	inline static int32_t get_offset_of_Type_2() { return static_cast<int32_t>(offsetof(Flag_t3377423384, ___Type_2)); }
	inline int32_t get_Type_2() const { return ___Type_2; }
	inline int32_t* get_address_of_Type_2() { return &___Type_2; }
	inline void set_Type_2(int32_t value)
	{
		___Type_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLAG_T3377423384_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef LIB_T3857655653_H
#define LIB_T3857655653_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSDeploy.Lib
struct  Lib_t3857655653  : public RuntimeObject
{
public:
	// System.Boolean SA.IOSDeploy.Lib::IsOpen
	bool ___IsOpen_0;
	// SA.IOSDeploy.iOSLibrary SA.IOSDeploy.Lib::Type
	int32_t ___Type_1;
	// System.Boolean SA.IOSDeploy.Lib::IsOptional
	bool ___IsOptional_2;

public:
	inline static int32_t get_offset_of_IsOpen_0() { return static_cast<int32_t>(offsetof(Lib_t3857655653, ___IsOpen_0)); }
	inline bool get_IsOpen_0() const { return ___IsOpen_0; }
	inline bool* get_address_of_IsOpen_0() { return &___IsOpen_0; }
	inline void set_IsOpen_0(bool value)
	{
		___IsOpen_0 = value;
	}

	inline static int32_t get_offset_of_Type_1() { return static_cast<int32_t>(offsetof(Lib_t3857655653, ___Type_1)); }
	inline int32_t get_Type_1() const { return ___Type_1; }
	inline int32_t* get_address_of_Type_1() { return &___Type_1; }
	inline void set_Type_1(int32_t value)
	{
		___Type_1 = value;
	}

	inline static int32_t get_offset_of_IsOptional_2() { return static_cast<int32_t>(offsetof(Lib_t3857655653, ___IsOptional_2)); }
	inline bool get_IsOptional_2() const { return ___IsOptional_2; }
	inline bool* get_address_of_IsOptional_2() { return &___IsOptional_2; }
	inline void set_IsOptional_2(bool value)
	{
		___IsOptional_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIB_T3857655653_H
#ifndef GK_USERPHOTOLOADRESULT_T3918760827_H
#define GK_USERPHOTOLOADRESULT_T3918760827_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GK_UserPhotoLoadResult
struct  GK_UserPhotoLoadResult_t3918760827  : public Result_t4170401718
{
public:
	// UnityEngine.Texture2D GK_UserPhotoLoadResult::_Photo
	Texture2D_t3840446185 * ____Photo_1;
	// GK_PhotoSize GK_UserPhotoLoadResult::_Size
	int32_t ____Size_2;

public:
	inline static int32_t get_offset_of__Photo_1() { return static_cast<int32_t>(offsetof(GK_UserPhotoLoadResult_t3918760827, ____Photo_1)); }
	inline Texture2D_t3840446185 * get__Photo_1() const { return ____Photo_1; }
	inline Texture2D_t3840446185 ** get_address_of__Photo_1() { return &____Photo_1; }
	inline void set__Photo_1(Texture2D_t3840446185 * value)
	{
		____Photo_1 = value;
		Il2CppCodeGenWriteBarrier((&____Photo_1), value);
	}

	inline static int32_t get_offset_of__Size_2() { return static_cast<int32_t>(offsetof(GK_UserPhotoLoadResult_t3918760827, ____Size_2)); }
	inline int32_t get__Size_2() const { return ____Size_2; }
	inline int32_t* get_address_of__Size_2() { return &____Size_2; }
	inline void set__Size_2(int32_t value)
	{
		____Size_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GK_USERPHOTOLOADRESULT_T3918760827_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef RAYCAST3DCALLBACK_T701940803_H
#define RAYCAST3DCALLBACK_T701940803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/Raycast3DCallback
struct  Raycast3DCallback_t701940803  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCAST3DCALLBACK_T701940803_H
#ifndef RAYCAST2DCALLBACK_T768590915_H
#define RAYCAST2DCALLBACK_T768590915_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/Raycast2DCallback
struct  Raycast2DCallback_t768590915  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCAST2DCALLBACK_T768590915_H
#ifndef RAYCASTALLCALLBACK_T1884415901_H
#define RAYCASTALLCALLBACK_T1884415901_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/RaycastAllCallback
struct  RaycastAllCallback_t1884415901  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTALLCALLBACK_T1884415901_H
#ifndef GETRAYINTERSECTIONALLCALLBACK_T3913627115_H
#define GETRAYINTERSECTIONALLCALLBACK_T3913627115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllCallback
struct  GetRayIntersectionAllCallback_t3913627115  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYINTERSECTIONALLCALLBACK_T3913627115_H
#ifndef GETRAYINTERSECTIONALLNONALLOCCALLBACK_T2311174851_H
#define GETRAYINTERSECTIONALLNONALLOCCALLBACK_T2311174851_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllNonAllocCallback
struct  GetRayIntersectionAllNonAllocCallback_t2311174851  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYINTERSECTIONALLNONALLOCCALLBACK_T2311174851_H
#ifndef IOSNATIVESETTINGS_T531545985_H
#define IOSNATIVESETTINGS_T531545985_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IOSNativeSettings
struct  IOSNativeSettings_t531545985  : public ScriptableObject_t2528358522
{
public:
	// System.Boolean IOSNativeSettings::EnableGameCenterAPI
	bool ___EnableGameCenterAPI_3;
	// System.Boolean IOSNativeSettings::EnableInAppsAPI
	bool ___EnableInAppsAPI_4;
	// System.Boolean IOSNativeSettings::EnableCameraAPI
	bool ___EnableCameraAPI_5;
	// System.Boolean IOSNativeSettings::EnableSocialSharingAPI
	bool ___EnableSocialSharingAPI_6;
	// System.Boolean IOSNativeSettings::EnablePickerAPI
	bool ___EnablePickerAPI_7;
	// System.Boolean IOSNativeSettings::EnableMediaPlayerAPI
	bool ___EnableMediaPlayerAPI_8;
	// System.Boolean IOSNativeSettings::EnableReplayKit
	bool ___EnableReplayKit_9;
	// System.Boolean IOSNativeSettings::EnableCloudKit
	bool ___EnableCloudKit_10;
	// System.Boolean IOSNativeSettings::EnableSoomla
	bool ___EnableSoomla_11;
	// System.Boolean IOSNativeSettings::EnableGestureAPI
	bool ___EnableGestureAPI_12;
	// System.Boolean IOSNativeSettings::EnableForceTouchAPI
	bool ___EnableForceTouchAPI_13;
	// System.Boolean IOSNativeSettings::EnablePushNotificationsAPI
	bool ___EnablePushNotificationsAPI_14;
	// System.Boolean IOSNativeSettings::EnableContactsAPI
	bool ___EnableContactsAPI_15;
	// System.Boolean IOSNativeSettings::EnableAppEventsAPI
	bool ___EnableAppEventsAPI_16;
	// System.Boolean IOSNativeSettings::EnableUserNotificationsAPI
	bool ___EnableUserNotificationsAPI_17;
	// System.Boolean IOSNativeSettings::EnablePermissionAPI
	bool ___EnablePermissionAPI_18;
	// System.String IOSNativeSettings::AppleId
	String_t* ___AppleId_19;
	// System.Int32 IOSNativeSettings::ToolbarIndex
	int32_t ___ToolbarIndex_20;
	// System.Boolean IOSNativeSettings::ExpandMoreActionsMenu
	bool ___ExpandMoreActionsMenu_21;
	// System.Boolean IOSNativeSettings::ExpandModulesSettings
	bool ___ExpandModulesSettings_22;
	// System.Boolean IOSNativeSettings::InAppsEditorTesting
	bool ___InAppsEditorTesting_23;
	// System.Boolean IOSNativeSettings::CheckInternetBeforeLoadRequest
	bool ___CheckInternetBeforeLoadRequest_24;
	// System.Boolean IOSNativeSettings::PromotedPurchaseSupport
	bool ___PromotedPurchaseSupport_25;
	// SA.IOSNative.StoreKit.TransactionsHandlingMode IOSNativeSettings::TransactionsHandlingMode
	int32_t ___TransactionsHandlingMode_26;
	// System.Collections.Generic.List`1<System.String> IOSNativeSettings::DefaultStoreProductsView
	List_1_t3319525431 * ___DefaultStoreProductsView_27;
	// System.Collections.Generic.List`1<SA.IOSNative.StoreKit.Product> IOSNativeSettings::InAppProducts
	List_1_t294466124 * ___InAppProducts_28;
	// System.Boolean IOSNativeSettings::ShowStoreKitProducts
	bool ___ShowStoreKitProducts_29;
	// System.Collections.Generic.List`1<GK_Leaderboard> IOSNativeSettings::Leaderboards
	List_1_t2063453238 * ___Leaderboards_30;
	// System.Collections.Generic.List`1<GK_AchievementTemplate> IOSNativeSettings::Achievements
	List_1_t801805342 * ___Achievements_31;
	// System.Boolean IOSNativeSettings::UseGCRequestCaching
	bool ___UseGCRequestCaching_32;
	// System.Boolean IOSNativeSettings::UsePPForAchievements
	bool ___UsePPForAchievements_33;
	// System.Boolean IOSNativeSettings::AutoLoadUsersSmallImages
	bool ___AutoLoadUsersSmallImages_34;
	// System.Boolean IOSNativeSettings::AutoLoadUsersBigImages
	bool ___AutoLoadUsersBigImages_35;
	// System.Boolean IOSNativeSettings::ShowLeaderboards
	bool ___ShowLeaderboards_36;
	// System.Boolean IOSNativeSettings::ShowAchievementsParams
	bool ___ShowAchievementsParams_37;
	// System.Boolean IOSNativeSettings::AdEditorTesting
	bool ___AdEditorTesting_38;
	// System.Int32 IOSNativeSettings::EditorFillRateIndex
	int32_t ___EditorFillRateIndex_39;
	// System.Int32 IOSNativeSettings::EditorFillRate
	int32_t ___EditorFillRate_40;
	// System.Int32 IOSNativeSettings::MaxImageLoadSize
	int32_t ___MaxImageLoadSize_41;
	// System.Single IOSNativeSettings::JPegCompressionRate
	float ___JPegCompressionRate_42;
	// IOSGalleryLoadImageFormat IOSNativeSettings::GalleryImageFormat
	int32_t ___GalleryImageFormat_43;
	// System.Int32 IOSNativeSettings::RPK_iPadViewType
	int32_t ___RPK_iPadViewType_44;
	// System.String IOSNativeSettings::CameraUsageDescription
	String_t* ___CameraUsageDescription_45;
	// System.String IOSNativeSettings::PhotoLibraryUsageDescription
	String_t* ___PhotoLibraryUsageDescription_46;
	// System.String IOSNativeSettings::AppleMusicUsageDescription
	String_t* ___AppleMusicUsageDescription_47;
	// System.String IOSNativeSettings::ContactsUsageDescription
	String_t* ___ContactsUsageDescription_48;
	// System.Collections.Generic.List`1<SA.IOSNative.Models.UrlType> IOSNativeSettings::UrlTypes
	List_1_t4189911559 * ___UrlTypes_49;
	// System.Collections.Generic.List`1<SA.IOSNative.Models.UrlType> IOSNativeSettings::ApplicationQueriesSchemes
	List_1_t4189911559 * ___ApplicationQueriesSchemes_50;
	// System.Collections.Generic.List`1<SA.IOSNative.Gestures.ForceTouchMenuItem> IOSNativeSettings::ForceTouchMenu
	List_1_t1585227261 * ___ForceTouchMenu_51;
	// System.Boolean IOSNativeSettings::DisablePluginLogs
	bool ___DisablePluginLogs_52;
	// System.String IOSNativeSettings::SoomlaDownloadLink
	String_t* ___SoomlaDownloadLink_53;
	// System.String IOSNativeSettings::SoomlaDocsLink
	String_t* ___SoomlaDocsLink_54;
	// System.String IOSNativeSettings::SoomlaGameKey
	String_t* ___SoomlaGameKey_55;
	// System.String IOSNativeSettings::SoomlaEnvKey
	String_t* ___SoomlaEnvKey_56;
	// System.Boolean IOSNativeSettings::OneSignalEnabled
	bool ___OneSignalEnabled_57;
	// System.String IOSNativeSettings::OneSignalDocsLink
	String_t* ___OneSignalDocsLink_58;

public:
	inline static int32_t get_offset_of_EnableGameCenterAPI_3() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985, ___EnableGameCenterAPI_3)); }
	inline bool get_EnableGameCenterAPI_3() const { return ___EnableGameCenterAPI_3; }
	inline bool* get_address_of_EnableGameCenterAPI_3() { return &___EnableGameCenterAPI_3; }
	inline void set_EnableGameCenterAPI_3(bool value)
	{
		___EnableGameCenterAPI_3 = value;
	}

	inline static int32_t get_offset_of_EnableInAppsAPI_4() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985, ___EnableInAppsAPI_4)); }
	inline bool get_EnableInAppsAPI_4() const { return ___EnableInAppsAPI_4; }
	inline bool* get_address_of_EnableInAppsAPI_4() { return &___EnableInAppsAPI_4; }
	inline void set_EnableInAppsAPI_4(bool value)
	{
		___EnableInAppsAPI_4 = value;
	}

	inline static int32_t get_offset_of_EnableCameraAPI_5() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985, ___EnableCameraAPI_5)); }
	inline bool get_EnableCameraAPI_5() const { return ___EnableCameraAPI_5; }
	inline bool* get_address_of_EnableCameraAPI_5() { return &___EnableCameraAPI_5; }
	inline void set_EnableCameraAPI_5(bool value)
	{
		___EnableCameraAPI_5 = value;
	}

	inline static int32_t get_offset_of_EnableSocialSharingAPI_6() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985, ___EnableSocialSharingAPI_6)); }
	inline bool get_EnableSocialSharingAPI_6() const { return ___EnableSocialSharingAPI_6; }
	inline bool* get_address_of_EnableSocialSharingAPI_6() { return &___EnableSocialSharingAPI_6; }
	inline void set_EnableSocialSharingAPI_6(bool value)
	{
		___EnableSocialSharingAPI_6 = value;
	}

	inline static int32_t get_offset_of_EnablePickerAPI_7() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985, ___EnablePickerAPI_7)); }
	inline bool get_EnablePickerAPI_7() const { return ___EnablePickerAPI_7; }
	inline bool* get_address_of_EnablePickerAPI_7() { return &___EnablePickerAPI_7; }
	inline void set_EnablePickerAPI_7(bool value)
	{
		___EnablePickerAPI_7 = value;
	}

	inline static int32_t get_offset_of_EnableMediaPlayerAPI_8() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985, ___EnableMediaPlayerAPI_8)); }
	inline bool get_EnableMediaPlayerAPI_8() const { return ___EnableMediaPlayerAPI_8; }
	inline bool* get_address_of_EnableMediaPlayerAPI_8() { return &___EnableMediaPlayerAPI_8; }
	inline void set_EnableMediaPlayerAPI_8(bool value)
	{
		___EnableMediaPlayerAPI_8 = value;
	}

	inline static int32_t get_offset_of_EnableReplayKit_9() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985, ___EnableReplayKit_9)); }
	inline bool get_EnableReplayKit_9() const { return ___EnableReplayKit_9; }
	inline bool* get_address_of_EnableReplayKit_9() { return &___EnableReplayKit_9; }
	inline void set_EnableReplayKit_9(bool value)
	{
		___EnableReplayKit_9 = value;
	}

	inline static int32_t get_offset_of_EnableCloudKit_10() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985, ___EnableCloudKit_10)); }
	inline bool get_EnableCloudKit_10() const { return ___EnableCloudKit_10; }
	inline bool* get_address_of_EnableCloudKit_10() { return &___EnableCloudKit_10; }
	inline void set_EnableCloudKit_10(bool value)
	{
		___EnableCloudKit_10 = value;
	}

	inline static int32_t get_offset_of_EnableSoomla_11() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985, ___EnableSoomla_11)); }
	inline bool get_EnableSoomla_11() const { return ___EnableSoomla_11; }
	inline bool* get_address_of_EnableSoomla_11() { return &___EnableSoomla_11; }
	inline void set_EnableSoomla_11(bool value)
	{
		___EnableSoomla_11 = value;
	}

	inline static int32_t get_offset_of_EnableGestureAPI_12() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985, ___EnableGestureAPI_12)); }
	inline bool get_EnableGestureAPI_12() const { return ___EnableGestureAPI_12; }
	inline bool* get_address_of_EnableGestureAPI_12() { return &___EnableGestureAPI_12; }
	inline void set_EnableGestureAPI_12(bool value)
	{
		___EnableGestureAPI_12 = value;
	}

	inline static int32_t get_offset_of_EnableForceTouchAPI_13() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985, ___EnableForceTouchAPI_13)); }
	inline bool get_EnableForceTouchAPI_13() const { return ___EnableForceTouchAPI_13; }
	inline bool* get_address_of_EnableForceTouchAPI_13() { return &___EnableForceTouchAPI_13; }
	inline void set_EnableForceTouchAPI_13(bool value)
	{
		___EnableForceTouchAPI_13 = value;
	}

	inline static int32_t get_offset_of_EnablePushNotificationsAPI_14() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985, ___EnablePushNotificationsAPI_14)); }
	inline bool get_EnablePushNotificationsAPI_14() const { return ___EnablePushNotificationsAPI_14; }
	inline bool* get_address_of_EnablePushNotificationsAPI_14() { return &___EnablePushNotificationsAPI_14; }
	inline void set_EnablePushNotificationsAPI_14(bool value)
	{
		___EnablePushNotificationsAPI_14 = value;
	}

	inline static int32_t get_offset_of_EnableContactsAPI_15() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985, ___EnableContactsAPI_15)); }
	inline bool get_EnableContactsAPI_15() const { return ___EnableContactsAPI_15; }
	inline bool* get_address_of_EnableContactsAPI_15() { return &___EnableContactsAPI_15; }
	inline void set_EnableContactsAPI_15(bool value)
	{
		___EnableContactsAPI_15 = value;
	}

	inline static int32_t get_offset_of_EnableAppEventsAPI_16() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985, ___EnableAppEventsAPI_16)); }
	inline bool get_EnableAppEventsAPI_16() const { return ___EnableAppEventsAPI_16; }
	inline bool* get_address_of_EnableAppEventsAPI_16() { return &___EnableAppEventsAPI_16; }
	inline void set_EnableAppEventsAPI_16(bool value)
	{
		___EnableAppEventsAPI_16 = value;
	}

	inline static int32_t get_offset_of_EnableUserNotificationsAPI_17() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985, ___EnableUserNotificationsAPI_17)); }
	inline bool get_EnableUserNotificationsAPI_17() const { return ___EnableUserNotificationsAPI_17; }
	inline bool* get_address_of_EnableUserNotificationsAPI_17() { return &___EnableUserNotificationsAPI_17; }
	inline void set_EnableUserNotificationsAPI_17(bool value)
	{
		___EnableUserNotificationsAPI_17 = value;
	}

	inline static int32_t get_offset_of_EnablePermissionAPI_18() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985, ___EnablePermissionAPI_18)); }
	inline bool get_EnablePermissionAPI_18() const { return ___EnablePermissionAPI_18; }
	inline bool* get_address_of_EnablePermissionAPI_18() { return &___EnablePermissionAPI_18; }
	inline void set_EnablePermissionAPI_18(bool value)
	{
		___EnablePermissionAPI_18 = value;
	}

	inline static int32_t get_offset_of_AppleId_19() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985, ___AppleId_19)); }
	inline String_t* get_AppleId_19() const { return ___AppleId_19; }
	inline String_t** get_address_of_AppleId_19() { return &___AppleId_19; }
	inline void set_AppleId_19(String_t* value)
	{
		___AppleId_19 = value;
		Il2CppCodeGenWriteBarrier((&___AppleId_19), value);
	}

	inline static int32_t get_offset_of_ToolbarIndex_20() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985, ___ToolbarIndex_20)); }
	inline int32_t get_ToolbarIndex_20() const { return ___ToolbarIndex_20; }
	inline int32_t* get_address_of_ToolbarIndex_20() { return &___ToolbarIndex_20; }
	inline void set_ToolbarIndex_20(int32_t value)
	{
		___ToolbarIndex_20 = value;
	}

	inline static int32_t get_offset_of_ExpandMoreActionsMenu_21() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985, ___ExpandMoreActionsMenu_21)); }
	inline bool get_ExpandMoreActionsMenu_21() const { return ___ExpandMoreActionsMenu_21; }
	inline bool* get_address_of_ExpandMoreActionsMenu_21() { return &___ExpandMoreActionsMenu_21; }
	inline void set_ExpandMoreActionsMenu_21(bool value)
	{
		___ExpandMoreActionsMenu_21 = value;
	}

	inline static int32_t get_offset_of_ExpandModulesSettings_22() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985, ___ExpandModulesSettings_22)); }
	inline bool get_ExpandModulesSettings_22() const { return ___ExpandModulesSettings_22; }
	inline bool* get_address_of_ExpandModulesSettings_22() { return &___ExpandModulesSettings_22; }
	inline void set_ExpandModulesSettings_22(bool value)
	{
		___ExpandModulesSettings_22 = value;
	}

	inline static int32_t get_offset_of_InAppsEditorTesting_23() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985, ___InAppsEditorTesting_23)); }
	inline bool get_InAppsEditorTesting_23() const { return ___InAppsEditorTesting_23; }
	inline bool* get_address_of_InAppsEditorTesting_23() { return &___InAppsEditorTesting_23; }
	inline void set_InAppsEditorTesting_23(bool value)
	{
		___InAppsEditorTesting_23 = value;
	}

	inline static int32_t get_offset_of_CheckInternetBeforeLoadRequest_24() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985, ___CheckInternetBeforeLoadRequest_24)); }
	inline bool get_CheckInternetBeforeLoadRequest_24() const { return ___CheckInternetBeforeLoadRequest_24; }
	inline bool* get_address_of_CheckInternetBeforeLoadRequest_24() { return &___CheckInternetBeforeLoadRequest_24; }
	inline void set_CheckInternetBeforeLoadRequest_24(bool value)
	{
		___CheckInternetBeforeLoadRequest_24 = value;
	}

	inline static int32_t get_offset_of_PromotedPurchaseSupport_25() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985, ___PromotedPurchaseSupport_25)); }
	inline bool get_PromotedPurchaseSupport_25() const { return ___PromotedPurchaseSupport_25; }
	inline bool* get_address_of_PromotedPurchaseSupport_25() { return &___PromotedPurchaseSupport_25; }
	inline void set_PromotedPurchaseSupport_25(bool value)
	{
		___PromotedPurchaseSupport_25 = value;
	}

	inline static int32_t get_offset_of_TransactionsHandlingMode_26() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985, ___TransactionsHandlingMode_26)); }
	inline int32_t get_TransactionsHandlingMode_26() const { return ___TransactionsHandlingMode_26; }
	inline int32_t* get_address_of_TransactionsHandlingMode_26() { return &___TransactionsHandlingMode_26; }
	inline void set_TransactionsHandlingMode_26(int32_t value)
	{
		___TransactionsHandlingMode_26 = value;
	}

	inline static int32_t get_offset_of_DefaultStoreProductsView_27() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985, ___DefaultStoreProductsView_27)); }
	inline List_1_t3319525431 * get_DefaultStoreProductsView_27() const { return ___DefaultStoreProductsView_27; }
	inline List_1_t3319525431 ** get_address_of_DefaultStoreProductsView_27() { return &___DefaultStoreProductsView_27; }
	inline void set_DefaultStoreProductsView_27(List_1_t3319525431 * value)
	{
		___DefaultStoreProductsView_27 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultStoreProductsView_27), value);
	}

	inline static int32_t get_offset_of_InAppProducts_28() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985, ___InAppProducts_28)); }
	inline List_1_t294466124 * get_InAppProducts_28() const { return ___InAppProducts_28; }
	inline List_1_t294466124 ** get_address_of_InAppProducts_28() { return &___InAppProducts_28; }
	inline void set_InAppProducts_28(List_1_t294466124 * value)
	{
		___InAppProducts_28 = value;
		Il2CppCodeGenWriteBarrier((&___InAppProducts_28), value);
	}

	inline static int32_t get_offset_of_ShowStoreKitProducts_29() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985, ___ShowStoreKitProducts_29)); }
	inline bool get_ShowStoreKitProducts_29() const { return ___ShowStoreKitProducts_29; }
	inline bool* get_address_of_ShowStoreKitProducts_29() { return &___ShowStoreKitProducts_29; }
	inline void set_ShowStoreKitProducts_29(bool value)
	{
		___ShowStoreKitProducts_29 = value;
	}

	inline static int32_t get_offset_of_Leaderboards_30() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985, ___Leaderboards_30)); }
	inline List_1_t2063453238 * get_Leaderboards_30() const { return ___Leaderboards_30; }
	inline List_1_t2063453238 ** get_address_of_Leaderboards_30() { return &___Leaderboards_30; }
	inline void set_Leaderboards_30(List_1_t2063453238 * value)
	{
		___Leaderboards_30 = value;
		Il2CppCodeGenWriteBarrier((&___Leaderboards_30), value);
	}

	inline static int32_t get_offset_of_Achievements_31() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985, ___Achievements_31)); }
	inline List_1_t801805342 * get_Achievements_31() const { return ___Achievements_31; }
	inline List_1_t801805342 ** get_address_of_Achievements_31() { return &___Achievements_31; }
	inline void set_Achievements_31(List_1_t801805342 * value)
	{
		___Achievements_31 = value;
		Il2CppCodeGenWriteBarrier((&___Achievements_31), value);
	}

	inline static int32_t get_offset_of_UseGCRequestCaching_32() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985, ___UseGCRequestCaching_32)); }
	inline bool get_UseGCRequestCaching_32() const { return ___UseGCRequestCaching_32; }
	inline bool* get_address_of_UseGCRequestCaching_32() { return &___UseGCRequestCaching_32; }
	inline void set_UseGCRequestCaching_32(bool value)
	{
		___UseGCRequestCaching_32 = value;
	}

	inline static int32_t get_offset_of_UsePPForAchievements_33() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985, ___UsePPForAchievements_33)); }
	inline bool get_UsePPForAchievements_33() const { return ___UsePPForAchievements_33; }
	inline bool* get_address_of_UsePPForAchievements_33() { return &___UsePPForAchievements_33; }
	inline void set_UsePPForAchievements_33(bool value)
	{
		___UsePPForAchievements_33 = value;
	}

	inline static int32_t get_offset_of_AutoLoadUsersSmallImages_34() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985, ___AutoLoadUsersSmallImages_34)); }
	inline bool get_AutoLoadUsersSmallImages_34() const { return ___AutoLoadUsersSmallImages_34; }
	inline bool* get_address_of_AutoLoadUsersSmallImages_34() { return &___AutoLoadUsersSmallImages_34; }
	inline void set_AutoLoadUsersSmallImages_34(bool value)
	{
		___AutoLoadUsersSmallImages_34 = value;
	}

	inline static int32_t get_offset_of_AutoLoadUsersBigImages_35() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985, ___AutoLoadUsersBigImages_35)); }
	inline bool get_AutoLoadUsersBigImages_35() const { return ___AutoLoadUsersBigImages_35; }
	inline bool* get_address_of_AutoLoadUsersBigImages_35() { return &___AutoLoadUsersBigImages_35; }
	inline void set_AutoLoadUsersBigImages_35(bool value)
	{
		___AutoLoadUsersBigImages_35 = value;
	}

	inline static int32_t get_offset_of_ShowLeaderboards_36() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985, ___ShowLeaderboards_36)); }
	inline bool get_ShowLeaderboards_36() const { return ___ShowLeaderboards_36; }
	inline bool* get_address_of_ShowLeaderboards_36() { return &___ShowLeaderboards_36; }
	inline void set_ShowLeaderboards_36(bool value)
	{
		___ShowLeaderboards_36 = value;
	}

	inline static int32_t get_offset_of_ShowAchievementsParams_37() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985, ___ShowAchievementsParams_37)); }
	inline bool get_ShowAchievementsParams_37() const { return ___ShowAchievementsParams_37; }
	inline bool* get_address_of_ShowAchievementsParams_37() { return &___ShowAchievementsParams_37; }
	inline void set_ShowAchievementsParams_37(bool value)
	{
		___ShowAchievementsParams_37 = value;
	}

	inline static int32_t get_offset_of_AdEditorTesting_38() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985, ___AdEditorTesting_38)); }
	inline bool get_AdEditorTesting_38() const { return ___AdEditorTesting_38; }
	inline bool* get_address_of_AdEditorTesting_38() { return &___AdEditorTesting_38; }
	inline void set_AdEditorTesting_38(bool value)
	{
		___AdEditorTesting_38 = value;
	}

	inline static int32_t get_offset_of_EditorFillRateIndex_39() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985, ___EditorFillRateIndex_39)); }
	inline int32_t get_EditorFillRateIndex_39() const { return ___EditorFillRateIndex_39; }
	inline int32_t* get_address_of_EditorFillRateIndex_39() { return &___EditorFillRateIndex_39; }
	inline void set_EditorFillRateIndex_39(int32_t value)
	{
		___EditorFillRateIndex_39 = value;
	}

	inline static int32_t get_offset_of_EditorFillRate_40() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985, ___EditorFillRate_40)); }
	inline int32_t get_EditorFillRate_40() const { return ___EditorFillRate_40; }
	inline int32_t* get_address_of_EditorFillRate_40() { return &___EditorFillRate_40; }
	inline void set_EditorFillRate_40(int32_t value)
	{
		___EditorFillRate_40 = value;
	}

	inline static int32_t get_offset_of_MaxImageLoadSize_41() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985, ___MaxImageLoadSize_41)); }
	inline int32_t get_MaxImageLoadSize_41() const { return ___MaxImageLoadSize_41; }
	inline int32_t* get_address_of_MaxImageLoadSize_41() { return &___MaxImageLoadSize_41; }
	inline void set_MaxImageLoadSize_41(int32_t value)
	{
		___MaxImageLoadSize_41 = value;
	}

	inline static int32_t get_offset_of_JPegCompressionRate_42() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985, ___JPegCompressionRate_42)); }
	inline float get_JPegCompressionRate_42() const { return ___JPegCompressionRate_42; }
	inline float* get_address_of_JPegCompressionRate_42() { return &___JPegCompressionRate_42; }
	inline void set_JPegCompressionRate_42(float value)
	{
		___JPegCompressionRate_42 = value;
	}

	inline static int32_t get_offset_of_GalleryImageFormat_43() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985, ___GalleryImageFormat_43)); }
	inline int32_t get_GalleryImageFormat_43() const { return ___GalleryImageFormat_43; }
	inline int32_t* get_address_of_GalleryImageFormat_43() { return &___GalleryImageFormat_43; }
	inline void set_GalleryImageFormat_43(int32_t value)
	{
		___GalleryImageFormat_43 = value;
	}

	inline static int32_t get_offset_of_RPK_iPadViewType_44() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985, ___RPK_iPadViewType_44)); }
	inline int32_t get_RPK_iPadViewType_44() const { return ___RPK_iPadViewType_44; }
	inline int32_t* get_address_of_RPK_iPadViewType_44() { return &___RPK_iPadViewType_44; }
	inline void set_RPK_iPadViewType_44(int32_t value)
	{
		___RPK_iPadViewType_44 = value;
	}

	inline static int32_t get_offset_of_CameraUsageDescription_45() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985, ___CameraUsageDescription_45)); }
	inline String_t* get_CameraUsageDescription_45() const { return ___CameraUsageDescription_45; }
	inline String_t** get_address_of_CameraUsageDescription_45() { return &___CameraUsageDescription_45; }
	inline void set_CameraUsageDescription_45(String_t* value)
	{
		___CameraUsageDescription_45 = value;
		Il2CppCodeGenWriteBarrier((&___CameraUsageDescription_45), value);
	}

	inline static int32_t get_offset_of_PhotoLibraryUsageDescription_46() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985, ___PhotoLibraryUsageDescription_46)); }
	inline String_t* get_PhotoLibraryUsageDescription_46() const { return ___PhotoLibraryUsageDescription_46; }
	inline String_t** get_address_of_PhotoLibraryUsageDescription_46() { return &___PhotoLibraryUsageDescription_46; }
	inline void set_PhotoLibraryUsageDescription_46(String_t* value)
	{
		___PhotoLibraryUsageDescription_46 = value;
		Il2CppCodeGenWriteBarrier((&___PhotoLibraryUsageDescription_46), value);
	}

	inline static int32_t get_offset_of_AppleMusicUsageDescription_47() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985, ___AppleMusicUsageDescription_47)); }
	inline String_t* get_AppleMusicUsageDescription_47() const { return ___AppleMusicUsageDescription_47; }
	inline String_t** get_address_of_AppleMusicUsageDescription_47() { return &___AppleMusicUsageDescription_47; }
	inline void set_AppleMusicUsageDescription_47(String_t* value)
	{
		___AppleMusicUsageDescription_47 = value;
		Il2CppCodeGenWriteBarrier((&___AppleMusicUsageDescription_47), value);
	}

	inline static int32_t get_offset_of_ContactsUsageDescription_48() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985, ___ContactsUsageDescription_48)); }
	inline String_t* get_ContactsUsageDescription_48() const { return ___ContactsUsageDescription_48; }
	inline String_t** get_address_of_ContactsUsageDescription_48() { return &___ContactsUsageDescription_48; }
	inline void set_ContactsUsageDescription_48(String_t* value)
	{
		___ContactsUsageDescription_48 = value;
		Il2CppCodeGenWriteBarrier((&___ContactsUsageDescription_48), value);
	}

	inline static int32_t get_offset_of_UrlTypes_49() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985, ___UrlTypes_49)); }
	inline List_1_t4189911559 * get_UrlTypes_49() const { return ___UrlTypes_49; }
	inline List_1_t4189911559 ** get_address_of_UrlTypes_49() { return &___UrlTypes_49; }
	inline void set_UrlTypes_49(List_1_t4189911559 * value)
	{
		___UrlTypes_49 = value;
		Il2CppCodeGenWriteBarrier((&___UrlTypes_49), value);
	}

	inline static int32_t get_offset_of_ApplicationQueriesSchemes_50() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985, ___ApplicationQueriesSchemes_50)); }
	inline List_1_t4189911559 * get_ApplicationQueriesSchemes_50() const { return ___ApplicationQueriesSchemes_50; }
	inline List_1_t4189911559 ** get_address_of_ApplicationQueriesSchemes_50() { return &___ApplicationQueriesSchemes_50; }
	inline void set_ApplicationQueriesSchemes_50(List_1_t4189911559 * value)
	{
		___ApplicationQueriesSchemes_50 = value;
		Il2CppCodeGenWriteBarrier((&___ApplicationQueriesSchemes_50), value);
	}

	inline static int32_t get_offset_of_ForceTouchMenu_51() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985, ___ForceTouchMenu_51)); }
	inline List_1_t1585227261 * get_ForceTouchMenu_51() const { return ___ForceTouchMenu_51; }
	inline List_1_t1585227261 ** get_address_of_ForceTouchMenu_51() { return &___ForceTouchMenu_51; }
	inline void set_ForceTouchMenu_51(List_1_t1585227261 * value)
	{
		___ForceTouchMenu_51 = value;
		Il2CppCodeGenWriteBarrier((&___ForceTouchMenu_51), value);
	}

	inline static int32_t get_offset_of_DisablePluginLogs_52() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985, ___DisablePluginLogs_52)); }
	inline bool get_DisablePluginLogs_52() const { return ___DisablePluginLogs_52; }
	inline bool* get_address_of_DisablePluginLogs_52() { return &___DisablePluginLogs_52; }
	inline void set_DisablePluginLogs_52(bool value)
	{
		___DisablePluginLogs_52 = value;
	}

	inline static int32_t get_offset_of_SoomlaDownloadLink_53() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985, ___SoomlaDownloadLink_53)); }
	inline String_t* get_SoomlaDownloadLink_53() const { return ___SoomlaDownloadLink_53; }
	inline String_t** get_address_of_SoomlaDownloadLink_53() { return &___SoomlaDownloadLink_53; }
	inline void set_SoomlaDownloadLink_53(String_t* value)
	{
		___SoomlaDownloadLink_53 = value;
		Il2CppCodeGenWriteBarrier((&___SoomlaDownloadLink_53), value);
	}

	inline static int32_t get_offset_of_SoomlaDocsLink_54() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985, ___SoomlaDocsLink_54)); }
	inline String_t* get_SoomlaDocsLink_54() const { return ___SoomlaDocsLink_54; }
	inline String_t** get_address_of_SoomlaDocsLink_54() { return &___SoomlaDocsLink_54; }
	inline void set_SoomlaDocsLink_54(String_t* value)
	{
		___SoomlaDocsLink_54 = value;
		Il2CppCodeGenWriteBarrier((&___SoomlaDocsLink_54), value);
	}

	inline static int32_t get_offset_of_SoomlaGameKey_55() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985, ___SoomlaGameKey_55)); }
	inline String_t* get_SoomlaGameKey_55() const { return ___SoomlaGameKey_55; }
	inline String_t** get_address_of_SoomlaGameKey_55() { return &___SoomlaGameKey_55; }
	inline void set_SoomlaGameKey_55(String_t* value)
	{
		___SoomlaGameKey_55 = value;
		Il2CppCodeGenWriteBarrier((&___SoomlaGameKey_55), value);
	}

	inline static int32_t get_offset_of_SoomlaEnvKey_56() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985, ___SoomlaEnvKey_56)); }
	inline String_t* get_SoomlaEnvKey_56() const { return ___SoomlaEnvKey_56; }
	inline String_t** get_address_of_SoomlaEnvKey_56() { return &___SoomlaEnvKey_56; }
	inline void set_SoomlaEnvKey_56(String_t* value)
	{
		___SoomlaEnvKey_56 = value;
		Il2CppCodeGenWriteBarrier((&___SoomlaEnvKey_56), value);
	}

	inline static int32_t get_offset_of_OneSignalEnabled_57() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985, ___OneSignalEnabled_57)); }
	inline bool get_OneSignalEnabled_57() const { return ___OneSignalEnabled_57; }
	inline bool* get_address_of_OneSignalEnabled_57() { return &___OneSignalEnabled_57; }
	inline void set_OneSignalEnabled_57(bool value)
	{
		___OneSignalEnabled_57 = value;
	}

	inline static int32_t get_offset_of_OneSignalDocsLink_58() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985, ___OneSignalDocsLink_58)); }
	inline String_t* get_OneSignalDocsLink_58() const { return ___OneSignalDocsLink_58; }
	inline String_t** get_address_of_OneSignalDocsLink_58() { return &___OneSignalDocsLink_58; }
	inline void set_OneSignalDocsLink_58(String_t* value)
	{
		___OneSignalDocsLink_58 = value;
		Il2CppCodeGenWriteBarrier((&___OneSignalDocsLink_58), value);
	}
};

struct IOSNativeSettings_t531545985_StaticFields
{
public:
	// IOSNativeSettings IOSNativeSettings::instance
	IOSNativeSettings_t531545985 * ___instance_61;

public:
	inline static int32_t get_offset_of_instance_61() { return static_cast<int32_t>(offsetof(IOSNativeSettings_t531545985_StaticFields, ___instance_61)); }
	inline IOSNativeSettings_t531545985 * get_instance_61() const { return ___instance_61; }
	inline IOSNativeSettings_t531545985 ** get_address_of_instance_61() { return &___instance_61; }
	inline void set_instance_61(IOSNativeSettings_t531545985 * value)
	{
		___instance_61 = value;
		Il2CppCodeGenWriteBarrier((&___instance_61), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSNATIVESETTINGS_T531545985_H
#ifndef GETRAYCASTNONALLOCCALLBACK_T3841783507_H
#define GETRAYCASTNONALLOCCALLBACK_T3841783507_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/GetRaycastNonAllocCallback
struct  GetRaycastNonAllocCallback_t3841783507  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYCASTNONALLOCCALLBACK_T3841783507_H
#ifndef ISD_SETTINGS_T3533405735_H
#define ISD_SETTINGS_T3533405735_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSDeploy.ISD_Settings
struct  ISD_Settings_t3533405735  : public ScriptableObject_t2528358522
{
public:
	// System.Boolean SA.IOSDeploy.ISD_Settings::IsfwSettingOpen
	bool ___IsfwSettingOpen_3;
	// System.Boolean SA.IOSDeploy.ISD_Settings::IsLibSettingOpen
	bool ___IsLibSettingOpen_4;
	// System.Boolean SA.IOSDeploy.ISD_Settings::IslinkerSettingOpne
	bool ___IslinkerSettingOpne_5;
	// System.Boolean SA.IOSDeploy.ISD_Settings::IscompilerSettingsOpen
	bool ___IscompilerSettingsOpen_6;
	// System.Boolean SA.IOSDeploy.ISD_Settings::IsPlistSettingsOpen
	bool ___IsPlistSettingsOpen_7;
	// System.Boolean SA.IOSDeploy.ISD_Settings::IsLanguageSettingOpen
	bool ___IsLanguageSettingOpen_8;
	// System.Boolean SA.IOSDeploy.ISD_Settings::IsDefFrameworksOpen
	bool ___IsDefFrameworksOpen_9;
	// System.Boolean SA.IOSDeploy.ISD_Settings::IsDefLibrariesOpen
	bool ___IsDefLibrariesOpen_10;
	// System.Boolean SA.IOSDeploy.ISD_Settings::IsBuildSettingsOpen
	bool ___IsBuildSettingsOpen_11;
	// System.Int32 SA.IOSDeploy.ISD_Settings::ToolbarIndex
	int32_t ___ToolbarIndex_12;
	// System.Boolean SA.IOSDeploy.ISD_Settings::enableBitCode
	bool ___enableBitCode_13;
	// System.Boolean SA.IOSDeploy.ISD_Settings::enableTestability
	bool ___enableTestability_14;
	// System.Boolean SA.IOSDeploy.ISD_Settings::generateProfilingCode
	bool ___generateProfilingCode_15;
	// System.Collections.Generic.List`1<SA.IOSDeploy.Framework> SA.IOSDeploy.ISD_Settings::Frameworks
	List_1_t572364847 * ___Frameworks_16;
	// System.Collections.Generic.List`1<SA.IOSDeploy.Lib> SA.IOSDeploy.ISD_Settings::Libraries
	List_1_t1034763099 * ___Libraries_17;
	// System.Collections.Generic.List`1<SA.IOSDeploy.Flag> SA.IOSDeploy.ISD_Settings::Flags
	List_1_t554530830 * ___Flags_18;
	// System.Collections.Generic.List`1<SA.IOSDeploy.Variable> SA.IOSDeploy.ISD_Settings::PlistVariables
	List_1_t2508065575 * ___PlistVariables_19;
	// System.Collections.Generic.List`1<SA.IOSDeploy.VariableId> SA.IOSDeploy.ISD_Settings::VariableDictionary
	List_1_t1694503567 * ___VariableDictionary_20;
	// System.Collections.Generic.List`1<System.String> SA.IOSDeploy.ISD_Settings::langFolders
	List_1_t3319525431 * ___langFolders_21;
	// System.Collections.Generic.List`1<SA.IOSDeploy.AssetFile> SA.IOSDeploy.ISD_Settings::Files
	List_1_t1536090105 * ___Files_22;

public:
	inline static int32_t get_offset_of_IsfwSettingOpen_3() { return static_cast<int32_t>(offsetof(ISD_Settings_t3533405735, ___IsfwSettingOpen_3)); }
	inline bool get_IsfwSettingOpen_3() const { return ___IsfwSettingOpen_3; }
	inline bool* get_address_of_IsfwSettingOpen_3() { return &___IsfwSettingOpen_3; }
	inline void set_IsfwSettingOpen_3(bool value)
	{
		___IsfwSettingOpen_3 = value;
	}

	inline static int32_t get_offset_of_IsLibSettingOpen_4() { return static_cast<int32_t>(offsetof(ISD_Settings_t3533405735, ___IsLibSettingOpen_4)); }
	inline bool get_IsLibSettingOpen_4() const { return ___IsLibSettingOpen_4; }
	inline bool* get_address_of_IsLibSettingOpen_4() { return &___IsLibSettingOpen_4; }
	inline void set_IsLibSettingOpen_4(bool value)
	{
		___IsLibSettingOpen_4 = value;
	}

	inline static int32_t get_offset_of_IslinkerSettingOpne_5() { return static_cast<int32_t>(offsetof(ISD_Settings_t3533405735, ___IslinkerSettingOpne_5)); }
	inline bool get_IslinkerSettingOpne_5() const { return ___IslinkerSettingOpne_5; }
	inline bool* get_address_of_IslinkerSettingOpne_5() { return &___IslinkerSettingOpne_5; }
	inline void set_IslinkerSettingOpne_5(bool value)
	{
		___IslinkerSettingOpne_5 = value;
	}

	inline static int32_t get_offset_of_IscompilerSettingsOpen_6() { return static_cast<int32_t>(offsetof(ISD_Settings_t3533405735, ___IscompilerSettingsOpen_6)); }
	inline bool get_IscompilerSettingsOpen_6() const { return ___IscompilerSettingsOpen_6; }
	inline bool* get_address_of_IscompilerSettingsOpen_6() { return &___IscompilerSettingsOpen_6; }
	inline void set_IscompilerSettingsOpen_6(bool value)
	{
		___IscompilerSettingsOpen_6 = value;
	}

	inline static int32_t get_offset_of_IsPlistSettingsOpen_7() { return static_cast<int32_t>(offsetof(ISD_Settings_t3533405735, ___IsPlistSettingsOpen_7)); }
	inline bool get_IsPlistSettingsOpen_7() const { return ___IsPlistSettingsOpen_7; }
	inline bool* get_address_of_IsPlistSettingsOpen_7() { return &___IsPlistSettingsOpen_7; }
	inline void set_IsPlistSettingsOpen_7(bool value)
	{
		___IsPlistSettingsOpen_7 = value;
	}

	inline static int32_t get_offset_of_IsLanguageSettingOpen_8() { return static_cast<int32_t>(offsetof(ISD_Settings_t3533405735, ___IsLanguageSettingOpen_8)); }
	inline bool get_IsLanguageSettingOpen_8() const { return ___IsLanguageSettingOpen_8; }
	inline bool* get_address_of_IsLanguageSettingOpen_8() { return &___IsLanguageSettingOpen_8; }
	inline void set_IsLanguageSettingOpen_8(bool value)
	{
		___IsLanguageSettingOpen_8 = value;
	}

	inline static int32_t get_offset_of_IsDefFrameworksOpen_9() { return static_cast<int32_t>(offsetof(ISD_Settings_t3533405735, ___IsDefFrameworksOpen_9)); }
	inline bool get_IsDefFrameworksOpen_9() const { return ___IsDefFrameworksOpen_9; }
	inline bool* get_address_of_IsDefFrameworksOpen_9() { return &___IsDefFrameworksOpen_9; }
	inline void set_IsDefFrameworksOpen_9(bool value)
	{
		___IsDefFrameworksOpen_9 = value;
	}

	inline static int32_t get_offset_of_IsDefLibrariesOpen_10() { return static_cast<int32_t>(offsetof(ISD_Settings_t3533405735, ___IsDefLibrariesOpen_10)); }
	inline bool get_IsDefLibrariesOpen_10() const { return ___IsDefLibrariesOpen_10; }
	inline bool* get_address_of_IsDefLibrariesOpen_10() { return &___IsDefLibrariesOpen_10; }
	inline void set_IsDefLibrariesOpen_10(bool value)
	{
		___IsDefLibrariesOpen_10 = value;
	}

	inline static int32_t get_offset_of_IsBuildSettingsOpen_11() { return static_cast<int32_t>(offsetof(ISD_Settings_t3533405735, ___IsBuildSettingsOpen_11)); }
	inline bool get_IsBuildSettingsOpen_11() const { return ___IsBuildSettingsOpen_11; }
	inline bool* get_address_of_IsBuildSettingsOpen_11() { return &___IsBuildSettingsOpen_11; }
	inline void set_IsBuildSettingsOpen_11(bool value)
	{
		___IsBuildSettingsOpen_11 = value;
	}

	inline static int32_t get_offset_of_ToolbarIndex_12() { return static_cast<int32_t>(offsetof(ISD_Settings_t3533405735, ___ToolbarIndex_12)); }
	inline int32_t get_ToolbarIndex_12() const { return ___ToolbarIndex_12; }
	inline int32_t* get_address_of_ToolbarIndex_12() { return &___ToolbarIndex_12; }
	inline void set_ToolbarIndex_12(int32_t value)
	{
		___ToolbarIndex_12 = value;
	}

	inline static int32_t get_offset_of_enableBitCode_13() { return static_cast<int32_t>(offsetof(ISD_Settings_t3533405735, ___enableBitCode_13)); }
	inline bool get_enableBitCode_13() const { return ___enableBitCode_13; }
	inline bool* get_address_of_enableBitCode_13() { return &___enableBitCode_13; }
	inline void set_enableBitCode_13(bool value)
	{
		___enableBitCode_13 = value;
	}

	inline static int32_t get_offset_of_enableTestability_14() { return static_cast<int32_t>(offsetof(ISD_Settings_t3533405735, ___enableTestability_14)); }
	inline bool get_enableTestability_14() const { return ___enableTestability_14; }
	inline bool* get_address_of_enableTestability_14() { return &___enableTestability_14; }
	inline void set_enableTestability_14(bool value)
	{
		___enableTestability_14 = value;
	}

	inline static int32_t get_offset_of_generateProfilingCode_15() { return static_cast<int32_t>(offsetof(ISD_Settings_t3533405735, ___generateProfilingCode_15)); }
	inline bool get_generateProfilingCode_15() const { return ___generateProfilingCode_15; }
	inline bool* get_address_of_generateProfilingCode_15() { return &___generateProfilingCode_15; }
	inline void set_generateProfilingCode_15(bool value)
	{
		___generateProfilingCode_15 = value;
	}

	inline static int32_t get_offset_of_Frameworks_16() { return static_cast<int32_t>(offsetof(ISD_Settings_t3533405735, ___Frameworks_16)); }
	inline List_1_t572364847 * get_Frameworks_16() const { return ___Frameworks_16; }
	inline List_1_t572364847 ** get_address_of_Frameworks_16() { return &___Frameworks_16; }
	inline void set_Frameworks_16(List_1_t572364847 * value)
	{
		___Frameworks_16 = value;
		Il2CppCodeGenWriteBarrier((&___Frameworks_16), value);
	}

	inline static int32_t get_offset_of_Libraries_17() { return static_cast<int32_t>(offsetof(ISD_Settings_t3533405735, ___Libraries_17)); }
	inline List_1_t1034763099 * get_Libraries_17() const { return ___Libraries_17; }
	inline List_1_t1034763099 ** get_address_of_Libraries_17() { return &___Libraries_17; }
	inline void set_Libraries_17(List_1_t1034763099 * value)
	{
		___Libraries_17 = value;
		Il2CppCodeGenWriteBarrier((&___Libraries_17), value);
	}

	inline static int32_t get_offset_of_Flags_18() { return static_cast<int32_t>(offsetof(ISD_Settings_t3533405735, ___Flags_18)); }
	inline List_1_t554530830 * get_Flags_18() const { return ___Flags_18; }
	inline List_1_t554530830 ** get_address_of_Flags_18() { return &___Flags_18; }
	inline void set_Flags_18(List_1_t554530830 * value)
	{
		___Flags_18 = value;
		Il2CppCodeGenWriteBarrier((&___Flags_18), value);
	}

	inline static int32_t get_offset_of_PlistVariables_19() { return static_cast<int32_t>(offsetof(ISD_Settings_t3533405735, ___PlistVariables_19)); }
	inline List_1_t2508065575 * get_PlistVariables_19() const { return ___PlistVariables_19; }
	inline List_1_t2508065575 ** get_address_of_PlistVariables_19() { return &___PlistVariables_19; }
	inline void set_PlistVariables_19(List_1_t2508065575 * value)
	{
		___PlistVariables_19 = value;
		Il2CppCodeGenWriteBarrier((&___PlistVariables_19), value);
	}

	inline static int32_t get_offset_of_VariableDictionary_20() { return static_cast<int32_t>(offsetof(ISD_Settings_t3533405735, ___VariableDictionary_20)); }
	inline List_1_t1694503567 * get_VariableDictionary_20() const { return ___VariableDictionary_20; }
	inline List_1_t1694503567 ** get_address_of_VariableDictionary_20() { return &___VariableDictionary_20; }
	inline void set_VariableDictionary_20(List_1_t1694503567 * value)
	{
		___VariableDictionary_20 = value;
		Il2CppCodeGenWriteBarrier((&___VariableDictionary_20), value);
	}

	inline static int32_t get_offset_of_langFolders_21() { return static_cast<int32_t>(offsetof(ISD_Settings_t3533405735, ___langFolders_21)); }
	inline List_1_t3319525431 * get_langFolders_21() const { return ___langFolders_21; }
	inline List_1_t3319525431 ** get_address_of_langFolders_21() { return &___langFolders_21; }
	inline void set_langFolders_21(List_1_t3319525431 * value)
	{
		___langFolders_21 = value;
		Il2CppCodeGenWriteBarrier((&___langFolders_21), value);
	}

	inline static int32_t get_offset_of_Files_22() { return static_cast<int32_t>(offsetof(ISD_Settings_t3533405735, ___Files_22)); }
	inline List_1_t1536090105 * get_Files_22() const { return ___Files_22; }
	inline List_1_t1536090105 ** get_address_of_Files_22() { return &___Files_22; }
	inline void set_Files_22(List_1_t1536090105 * value)
	{
		___Files_22 = value;
		Il2CppCodeGenWriteBarrier((&___Files_22), value);
	}
};

struct ISD_Settings_t3533405735_StaticFields
{
public:
	// SA.IOSDeploy.ISD_Settings SA.IOSDeploy.ISD_Settings::instance
	ISD_Settings_t3533405735 * ___instance_25;

public:
	inline static int32_t get_offset_of_instance_25() { return static_cast<int32_t>(offsetof(ISD_Settings_t3533405735_StaticFields, ___instance_25)); }
	inline ISD_Settings_t3533405735 * get_instance_25() const { return ___instance_25; }
	inline ISD_Settings_t3533405735 ** get_address_of_instance_25() { return &___instance_25; }
	inline void set_instance_25(ISD_Settings_t3533405735 * value)
	{
		___instance_25 = value;
		Il2CppCodeGenWriteBarrier((&___instance_25), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISD_SETTINGS_T3533405735_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef ISN_CACHEMANAGER_T4161853135_H
#define ISN_CACHEMANAGER_T4161853135_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ISN_CacheManager
struct  ISN_CacheManager_t4161853135  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISN_CACHEMANAGER_T4161853135_H
#ifndef SINGLETON_1_T1344174708_H
#define SINGLETON_1_T1344174708_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Pattern.Singleton`1<GameCenter_RTM>
struct  Singleton_1_t1344174708  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Singleton_1_t1344174708_StaticFields
{
public:
	// T SA.Common.Pattern.Singleton`1::_instance
	GameCenter_RTM_t1714402562 * ____instance_2;
	// System.Boolean SA.Common.Pattern.Singleton`1::applicationIsQuitting
	bool ___applicationIsQuitting_3;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t1344174708_StaticFields, ____instance_2)); }
	inline GameCenter_RTM_t1714402562 * get__instance_2() const { return ____instance_2; }
	inline GameCenter_RTM_t1714402562 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(GameCenter_RTM_t1714402562 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____instance_2), value);
	}

	inline static int32_t get_offset_of_applicationIsQuitting_3() { return static_cast<int32_t>(offsetof(Singleton_1_t1344174708_StaticFields, ___applicationIsQuitting_3)); }
	inline bool get_applicationIsQuitting_3() const { return ___applicationIsQuitting_3; }
	inline bool* get_address_of_applicationIsQuitting_3() { return &___applicationIsQuitting_3; }
	inline void set_applicationIsQuitting_3(bool value)
	{
		___applicationIsQuitting_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T1344174708_H
#ifndef SINGLETON_1_T3960073501_H
#define SINGLETON_1_T3960073501_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Pattern.Singleton`1<ISN_SoomlaGrow>
struct  Singleton_1_t3960073501  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Singleton_1_t3960073501_StaticFields
{
public:
	// T SA.Common.Pattern.Singleton`1::_instance
	ISN_SoomlaGrow_t35334059 * ____instance_2;
	// System.Boolean SA.Common.Pattern.Singleton`1::applicationIsQuitting
	bool ___applicationIsQuitting_3;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t3960073501_StaticFields, ____instance_2)); }
	inline ISN_SoomlaGrow_t35334059 * get__instance_2() const { return ____instance_2; }
	inline ISN_SoomlaGrow_t35334059 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(ISN_SoomlaGrow_t35334059 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____instance_2), value);
	}

	inline static int32_t get_offset_of_applicationIsQuitting_3() { return static_cast<int32_t>(offsetof(Singleton_1_t3960073501_StaticFields, ___applicationIsQuitting_3)); }
	inline bool get_applicationIsQuitting_3() const { return ___applicationIsQuitting_3; }
	inline bool* get_address_of_applicationIsQuitting_3() { return &___applicationIsQuitting_3; }
	inline void set_applicationIsQuitting_3(bool value)
	{
		___applicationIsQuitting_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T3960073501_H
#ifndef SINGLETON_1_T2491185794_H
#define SINGLETON_1_T2491185794_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Pattern.Singleton`1<GameCenter_TBM>
struct  Singleton_1_t2491185794  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Singleton_1_t2491185794_StaticFields
{
public:
	// T SA.Common.Pattern.Singleton`1::_instance
	GameCenter_TBM_t2861413648 * ____instance_2;
	// System.Boolean SA.Common.Pattern.Singleton`1::applicationIsQuitting
	bool ___applicationIsQuitting_3;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t2491185794_StaticFields, ____instance_2)); }
	inline GameCenter_TBM_t2861413648 * get__instance_2() const { return ____instance_2; }
	inline GameCenter_TBM_t2861413648 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(GameCenter_TBM_t2861413648 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____instance_2), value);
	}

	inline static int32_t get_offset_of_applicationIsQuitting_3() { return static_cast<int32_t>(offsetof(Singleton_1_t2491185794_StaticFields, ___applicationIsQuitting_3)); }
	inline bool get_applicationIsQuitting_3() const { return ___applicationIsQuitting_3; }
	inline bool* get_address_of_applicationIsQuitting_3() { return &___applicationIsQuitting_3; }
	inline void set_applicationIsQuitting_3(bool value)
	{
		___applicationIsQuitting_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T2491185794_H
#ifndef SINGLETON_1_T259894734_H
#define SINGLETON_1_T259894734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Pattern.Singleton`1<GameCenterInvitations>
struct  Singleton_1_t259894734  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Singleton_1_t259894734_StaticFields
{
public:
	// T SA.Common.Pattern.Singleton`1::_instance
	GameCenterInvitations_t630122588 * ____instance_2;
	// System.Boolean SA.Common.Pattern.Singleton`1::applicationIsQuitting
	bool ___applicationIsQuitting_3;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t259894734_StaticFields, ____instance_2)); }
	inline GameCenterInvitations_t630122588 * get__instance_2() const { return ____instance_2; }
	inline GameCenterInvitations_t630122588 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(GameCenterInvitations_t630122588 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____instance_2), value);
	}

	inline static int32_t get_offset_of_applicationIsQuitting_3() { return static_cast<int32_t>(offsetof(Singleton_1_t259894734_StaticFields, ___applicationIsQuitting_3)); }
	inline bool get_applicationIsQuitting_3() const { return ___applicationIsQuitting_3; }
	inline bool* get_address_of_applicationIsQuitting_3() { return &___applicationIsQuitting_3; }
	inline void set_applicationIsQuitting_3(bool value)
	{
		___applicationIsQuitting_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T259894734_H
#ifndef ISD_FRAMEWORKHANDLER_T3201591945_H
#define ISD_FRAMEWORKHANDLER_T3201591945_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSDeploy.ISD_FrameworkHandler
struct  ISD_FrameworkHandler_t3201591945  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct ISD_FrameworkHandler_t3201591945_StaticFields
{
public:
	// System.Collections.Generic.List`1<SA.IOSDeploy.Framework> SA.IOSDeploy.ISD_FrameworkHandler::_DefaultFrameworks
	List_1_t572364847 * ____DefaultFrameworks_2;

public:
	inline static int32_t get_offset_of__DefaultFrameworks_2() { return static_cast<int32_t>(offsetof(ISD_FrameworkHandler_t3201591945_StaticFields, ____DefaultFrameworks_2)); }
	inline List_1_t572364847 * get__DefaultFrameworks_2() const { return ____DefaultFrameworks_2; }
	inline List_1_t572364847 ** get_address_of__DefaultFrameworks_2() { return &____DefaultFrameworks_2; }
	inline void set__DefaultFrameworks_2(List_1_t572364847 * value)
	{
		____DefaultFrameworks_2 = value;
		Il2CppCodeGenWriteBarrier((&____DefaultFrameworks_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISD_FRAMEWORKHANDLER_T3201591945_H
#ifndef ISD_LIBHANDLER_T1721854397_H
#define ISD_LIBHANDLER_T1721854397_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSDeploy.ISD_LibHandler
struct  ISD_LibHandler_t1721854397  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISD_LIBHANDLER_T1721854397_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef GAMECENTERMANAGER_T3595206143_H
#define GAMECENTERMANAGER_T3595206143_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameCenterManager
struct  GameCenterManager_t3595206143  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct GameCenterManager_t3595206143_StaticFields
{
public:
	// System.Action`1<SA.Common.Models.Result> GameCenterManager::OnAuthFinished
	Action_1_t47902017 * ___OnAuthFinished_2;
	// System.Action`1<GK_LeaderboardResult> GameCenterManager::OnScoreSubmitted
	Action_1_t1630030377 * ___OnScoreSubmitted_3;
	// System.Action`1<GK_LeaderboardResult> GameCenterManager::OnScoresListLoaded
	Action_1_t1630030377 * ___OnScoresListLoaded_4;
	// System.Action`1<GK_LeaderboardResult> GameCenterManager::OnLeadrboardInfoLoaded
	Action_1_t1630030377 * ___OnLeadrboardInfoLoaded_5;
	// System.Action`1<SA.Common.Models.Result> GameCenterManager::OnLeaderboardSetsInfoLoaded
	Action_1_t47902017 * ___OnLeaderboardSetsInfoLoaded_6;
	// System.Action`1<SA.Common.Models.Result> GameCenterManager::OnAchievementsReset
	Action_1_t47902017 * ___OnAchievementsReset_7;
	// System.Action`1<SA.Common.Models.Result> GameCenterManager::OnAchievementsLoaded
	Action_1_t47902017 * ___OnAchievementsLoaded_8;
	// System.Action`1<GK_AchievementProgressResult> GameCenterManager::OnAchievementsProgress
	Action_1_t623946033 * ___OnAchievementsProgress_9;
	// System.Action GameCenterManager::OnGameCenterViewDismissed
	Action_t1264377477 * ___OnGameCenterViewDismissed_10;
	// System.Action`1<SA.Common.Models.Result> GameCenterManager::OnFriendsListLoaded
	Action_1_t47902017 * ___OnFriendsListLoaded_11;
	// System.Action`1<GK_UserInfoLoadResult> GameCenterManager::OnUserInfoLoaded
	Action_1_t3760022062 * ___OnUserInfoLoaded_12;
	// System.Action`1<GK_PlayerSignatureResult> GameCenterManager::OnPlayerSignatureRetrieveResult
	Action_1_t1202011752 * ___OnPlayerSignatureRetrieveResult_13;
	// System.Boolean GameCenterManager::_IsInitialized
	bool ____IsInitialized_14;
	// System.Boolean GameCenterManager::_IsAchievementsInfoLoaded
	bool ____IsAchievementsInfoLoaded_15;
	// System.Collections.Generic.Dictionary`2<System.String,GK_Player> GameCenterManager::_players
	Dictionary_2_t3246609090 * ____players_16;
	// System.Collections.Generic.List`1<System.String> GameCenterManager::_friendsList
	List_1_t3319525431 * ____friendsList_17;
	// System.Collections.Generic.List`1<GK_LeaderboardSet> GameCenterManager::_LeaderboardSets
	List_1_t359552288 * ____LeaderboardSets_18;
	// System.Collections.Generic.Dictionary`2<System.Int32,GK_FriendRequest> GameCenterManager::_FriendRequests
	Dictionary_2_t1386088385 * ____FriendRequests_19;
	// GK_Player GameCenterManager::_player
	GK_Player_t3461352791 * ____player_20;

public:
	inline static int32_t get_offset_of_OnAuthFinished_2() { return static_cast<int32_t>(offsetof(GameCenterManager_t3595206143_StaticFields, ___OnAuthFinished_2)); }
	inline Action_1_t47902017 * get_OnAuthFinished_2() const { return ___OnAuthFinished_2; }
	inline Action_1_t47902017 ** get_address_of_OnAuthFinished_2() { return &___OnAuthFinished_2; }
	inline void set_OnAuthFinished_2(Action_1_t47902017 * value)
	{
		___OnAuthFinished_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnAuthFinished_2), value);
	}

	inline static int32_t get_offset_of_OnScoreSubmitted_3() { return static_cast<int32_t>(offsetof(GameCenterManager_t3595206143_StaticFields, ___OnScoreSubmitted_3)); }
	inline Action_1_t1630030377 * get_OnScoreSubmitted_3() const { return ___OnScoreSubmitted_3; }
	inline Action_1_t1630030377 ** get_address_of_OnScoreSubmitted_3() { return &___OnScoreSubmitted_3; }
	inline void set_OnScoreSubmitted_3(Action_1_t1630030377 * value)
	{
		___OnScoreSubmitted_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnScoreSubmitted_3), value);
	}

	inline static int32_t get_offset_of_OnScoresListLoaded_4() { return static_cast<int32_t>(offsetof(GameCenterManager_t3595206143_StaticFields, ___OnScoresListLoaded_4)); }
	inline Action_1_t1630030377 * get_OnScoresListLoaded_4() const { return ___OnScoresListLoaded_4; }
	inline Action_1_t1630030377 ** get_address_of_OnScoresListLoaded_4() { return &___OnScoresListLoaded_4; }
	inline void set_OnScoresListLoaded_4(Action_1_t1630030377 * value)
	{
		___OnScoresListLoaded_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnScoresListLoaded_4), value);
	}

	inline static int32_t get_offset_of_OnLeadrboardInfoLoaded_5() { return static_cast<int32_t>(offsetof(GameCenterManager_t3595206143_StaticFields, ___OnLeadrboardInfoLoaded_5)); }
	inline Action_1_t1630030377 * get_OnLeadrboardInfoLoaded_5() const { return ___OnLeadrboardInfoLoaded_5; }
	inline Action_1_t1630030377 ** get_address_of_OnLeadrboardInfoLoaded_5() { return &___OnLeadrboardInfoLoaded_5; }
	inline void set_OnLeadrboardInfoLoaded_5(Action_1_t1630030377 * value)
	{
		___OnLeadrboardInfoLoaded_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnLeadrboardInfoLoaded_5), value);
	}

	inline static int32_t get_offset_of_OnLeaderboardSetsInfoLoaded_6() { return static_cast<int32_t>(offsetof(GameCenterManager_t3595206143_StaticFields, ___OnLeaderboardSetsInfoLoaded_6)); }
	inline Action_1_t47902017 * get_OnLeaderboardSetsInfoLoaded_6() const { return ___OnLeaderboardSetsInfoLoaded_6; }
	inline Action_1_t47902017 ** get_address_of_OnLeaderboardSetsInfoLoaded_6() { return &___OnLeaderboardSetsInfoLoaded_6; }
	inline void set_OnLeaderboardSetsInfoLoaded_6(Action_1_t47902017 * value)
	{
		___OnLeaderboardSetsInfoLoaded_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnLeaderboardSetsInfoLoaded_6), value);
	}

	inline static int32_t get_offset_of_OnAchievementsReset_7() { return static_cast<int32_t>(offsetof(GameCenterManager_t3595206143_StaticFields, ___OnAchievementsReset_7)); }
	inline Action_1_t47902017 * get_OnAchievementsReset_7() const { return ___OnAchievementsReset_7; }
	inline Action_1_t47902017 ** get_address_of_OnAchievementsReset_7() { return &___OnAchievementsReset_7; }
	inline void set_OnAchievementsReset_7(Action_1_t47902017 * value)
	{
		___OnAchievementsReset_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnAchievementsReset_7), value);
	}

	inline static int32_t get_offset_of_OnAchievementsLoaded_8() { return static_cast<int32_t>(offsetof(GameCenterManager_t3595206143_StaticFields, ___OnAchievementsLoaded_8)); }
	inline Action_1_t47902017 * get_OnAchievementsLoaded_8() const { return ___OnAchievementsLoaded_8; }
	inline Action_1_t47902017 ** get_address_of_OnAchievementsLoaded_8() { return &___OnAchievementsLoaded_8; }
	inline void set_OnAchievementsLoaded_8(Action_1_t47902017 * value)
	{
		___OnAchievementsLoaded_8 = value;
		Il2CppCodeGenWriteBarrier((&___OnAchievementsLoaded_8), value);
	}

	inline static int32_t get_offset_of_OnAchievementsProgress_9() { return static_cast<int32_t>(offsetof(GameCenterManager_t3595206143_StaticFields, ___OnAchievementsProgress_9)); }
	inline Action_1_t623946033 * get_OnAchievementsProgress_9() const { return ___OnAchievementsProgress_9; }
	inline Action_1_t623946033 ** get_address_of_OnAchievementsProgress_9() { return &___OnAchievementsProgress_9; }
	inline void set_OnAchievementsProgress_9(Action_1_t623946033 * value)
	{
		___OnAchievementsProgress_9 = value;
		Il2CppCodeGenWriteBarrier((&___OnAchievementsProgress_9), value);
	}

	inline static int32_t get_offset_of_OnGameCenterViewDismissed_10() { return static_cast<int32_t>(offsetof(GameCenterManager_t3595206143_StaticFields, ___OnGameCenterViewDismissed_10)); }
	inline Action_t1264377477 * get_OnGameCenterViewDismissed_10() const { return ___OnGameCenterViewDismissed_10; }
	inline Action_t1264377477 ** get_address_of_OnGameCenterViewDismissed_10() { return &___OnGameCenterViewDismissed_10; }
	inline void set_OnGameCenterViewDismissed_10(Action_t1264377477 * value)
	{
		___OnGameCenterViewDismissed_10 = value;
		Il2CppCodeGenWriteBarrier((&___OnGameCenterViewDismissed_10), value);
	}

	inline static int32_t get_offset_of_OnFriendsListLoaded_11() { return static_cast<int32_t>(offsetof(GameCenterManager_t3595206143_StaticFields, ___OnFriendsListLoaded_11)); }
	inline Action_1_t47902017 * get_OnFriendsListLoaded_11() const { return ___OnFriendsListLoaded_11; }
	inline Action_1_t47902017 ** get_address_of_OnFriendsListLoaded_11() { return &___OnFriendsListLoaded_11; }
	inline void set_OnFriendsListLoaded_11(Action_1_t47902017 * value)
	{
		___OnFriendsListLoaded_11 = value;
		Il2CppCodeGenWriteBarrier((&___OnFriendsListLoaded_11), value);
	}

	inline static int32_t get_offset_of_OnUserInfoLoaded_12() { return static_cast<int32_t>(offsetof(GameCenterManager_t3595206143_StaticFields, ___OnUserInfoLoaded_12)); }
	inline Action_1_t3760022062 * get_OnUserInfoLoaded_12() const { return ___OnUserInfoLoaded_12; }
	inline Action_1_t3760022062 ** get_address_of_OnUserInfoLoaded_12() { return &___OnUserInfoLoaded_12; }
	inline void set_OnUserInfoLoaded_12(Action_1_t3760022062 * value)
	{
		___OnUserInfoLoaded_12 = value;
		Il2CppCodeGenWriteBarrier((&___OnUserInfoLoaded_12), value);
	}

	inline static int32_t get_offset_of_OnPlayerSignatureRetrieveResult_13() { return static_cast<int32_t>(offsetof(GameCenterManager_t3595206143_StaticFields, ___OnPlayerSignatureRetrieveResult_13)); }
	inline Action_1_t1202011752 * get_OnPlayerSignatureRetrieveResult_13() const { return ___OnPlayerSignatureRetrieveResult_13; }
	inline Action_1_t1202011752 ** get_address_of_OnPlayerSignatureRetrieveResult_13() { return &___OnPlayerSignatureRetrieveResult_13; }
	inline void set_OnPlayerSignatureRetrieveResult_13(Action_1_t1202011752 * value)
	{
		___OnPlayerSignatureRetrieveResult_13 = value;
		Il2CppCodeGenWriteBarrier((&___OnPlayerSignatureRetrieveResult_13), value);
	}

	inline static int32_t get_offset_of__IsInitialized_14() { return static_cast<int32_t>(offsetof(GameCenterManager_t3595206143_StaticFields, ____IsInitialized_14)); }
	inline bool get__IsInitialized_14() const { return ____IsInitialized_14; }
	inline bool* get_address_of__IsInitialized_14() { return &____IsInitialized_14; }
	inline void set__IsInitialized_14(bool value)
	{
		____IsInitialized_14 = value;
	}

	inline static int32_t get_offset_of__IsAchievementsInfoLoaded_15() { return static_cast<int32_t>(offsetof(GameCenterManager_t3595206143_StaticFields, ____IsAchievementsInfoLoaded_15)); }
	inline bool get__IsAchievementsInfoLoaded_15() const { return ____IsAchievementsInfoLoaded_15; }
	inline bool* get_address_of__IsAchievementsInfoLoaded_15() { return &____IsAchievementsInfoLoaded_15; }
	inline void set__IsAchievementsInfoLoaded_15(bool value)
	{
		____IsAchievementsInfoLoaded_15 = value;
	}

	inline static int32_t get_offset_of__players_16() { return static_cast<int32_t>(offsetof(GameCenterManager_t3595206143_StaticFields, ____players_16)); }
	inline Dictionary_2_t3246609090 * get__players_16() const { return ____players_16; }
	inline Dictionary_2_t3246609090 ** get_address_of__players_16() { return &____players_16; }
	inline void set__players_16(Dictionary_2_t3246609090 * value)
	{
		____players_16 = value;
		Il2CppCodeGenWriteBarrier((&____players_16), value);
	}

	inline static int32_t get_offset_of__friendsList_17() { return static_cast<int32_t>(offsetof(GameCenterManager_t3595206143_StaticFields, ____friendsList_17)); }
	inline List_1_t3319525431 * get__friendsList_17() const { return ____friendsList_17; }
	inline List_1_t3319525431 ** get_address_of__friendsList_17() { return &____friendsList_17; }
	inline void set__friendsList_17(List_1_t3319525431 * value)
	{
		____friendsList_17 = value;
		Il2CppCodeGenWriteBarrier((&____friendsList_17), value);
	}

	inline static int32_t get_offset_of__LeaderboardSets_18() { return static_cast<int32_t>(offsetof(GameCenterManager_t3595206143_StaticFields, ____LeaderboardSets_18)); }
	inline List_1_t359552288 * get__LeaderboardSets_18() const { return ____LeaderboardSets_18; }
	inline List_1_t359552288 ** get_address_of__LeaderboardSets_18() { return &____LeaderboardSets_18; }
	inline void set__LeaderboardSets_18(List_1_t359552288 * value)
	{
		____LeaderboardSets_18 = value;
		Il2CppCodeGenWriteBarrier((&____LeaderboardSets_18), value);
	}

	inline static int32_t get_offset_of__FriendRequests_19() { return static_cast<int32_t>(offsetof(GameCenterManager_t3595206143_StaticFields, ____FriendRequests_19)); }
	inline Dictionary_2_t1386088385 * get__FriendRequests_19() const { return ____FriendRequests_19; }
	inline Dictionary_2_t1386088385 ** get_address_of__FriendRequests_19() { return &____FriendRequests_19; }
	inline void set__FriendRequests_19(Dictionary_2_t1386088385 * value)
	{
		____FriendRequests_19 = value;
		Il2CppCodeGenWriteBarrier((&____FriendRequests_19), value);
	}

	inline static int32_t get_offset_of__player_20() { return static_cast<int32_t>(offsetof(GameCenterManager_t3595206143_StaticFields, ____player_20)); }
	inline GK_Player_t3461352791 * get__player_20() const { return ____player_20; }
	inline GK_Player_t3461352791 ** get_address_of__player_20() { return &____player_20; }
	inline void set__player_20(GK_Player_t3461352791 * value)
	{
		____player_20 = value;
		Il2CppCodeGenWriteBarrier((&____player_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMECENTERMANAGER_T3595206143_H
#ifndef SINGLETON_1_T3224837665_H
#define SINGLETON_1_T3224837665_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Pattern.Singleton`1<ISN_GameSaves>
struct  Singleton_1_t3224837665  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Singleton_1_t3224837665_StaticFields
{
public:
	// T SA.Common.Pattern.Singleton`1::_instance
	ISN_GameSaves_t3595065519 * ____instance_2;
	// System.Boolean SA.Common.Pattern.Singleton`1::applicationIsQuitting
	bool ___applicationIsQuitting_3;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t3224837665_StaticFields, ____instance_2)); }
	inline ISN_GameSaves_t3595065519 * get__instance_2() const { return ____instance_2; }
	inline ISN_GameSaves_t3595065519 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(ISN_GameSaves_t3595065519 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____instance_2), value);
	}

	inline static int32_t get_offset_of_applicationIsQuitting_3() { return static_cast<int32_t>(offsetof(Singleton_1_t3224837665_StaticFields, ___applicationIsQuitting_3)); }
	inline bool get_applicationIsQuitting_3() const { return ___applicationIsQuitting_3; }
	inline bool* get_address_of_applicationIsQuitting_3() { return &___applicationIsQuitting_3; }
	inline void set_applicationIsQuitting_3(bool value)
	{
		___applicationIsQuitting_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T3224837665_H
#ifndef SINGLETON_1_T1440197313_H
#define SINGLETON_1_T1440197313_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Pattern.Singleton`1<SA.IOSNative.Core.AppController>
struct  Singleton_1_t1440197313  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Singleton_1_t1440197313_StaticFields
{
public:
	// T SA.Common.Pattern.Singleton`1::_instance
	AppController_t1810425167 * ____instance_2;
	// System.Boolean SA.Common.Pattern.Singleton`1::applicationIsQuitting
	bool ___applicationIsQuitting_3;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t1440197313_StaticFields, ____instance_2)); }
	inline AppController_t1810425167 * get__instance_2() const { return ____instance_2; }
	inline AppController_t1810425167 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(AppController_t1810425167 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____instance_2), value);
	}

	inline static int32_t get_offset_of_applicationIsQuitting_3() { return static_cast<int32_t>(offsetof(Singleton_1_t1440197313_StaticFields, ___applicationIsQuitting_3)); }
	inline bool get_applicationIsQuitting_3() const { return ___applicationIsQuitting_3; }
	inline bool* get_address_of_applicationIsQuitting_3() { return &___applicationIsQuitting_3; }
	inline void set_applicationIsQuitting_3(bool value)
	{
		___applicationIsQuitting_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T1440197313_H
#ifndef ISN_SOOMLAGROW_T35334059_H
#define ISN_SOOMLAGROW_T35334059_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ISN_SoomlaGrow
struct  ISN_SoomlaGrow_t35334059  : public Singleton_1_t3960073501
{
public:

public:
};

struct ISN_SoomlaGrow_t35334059_StaticFields
{
public:
	// System.Action ISN_SoomlaGrow::ActionInitialized
	Action_t1264377477 * ___ActionInitialized_4;
	// System.Action ISN_SoomlaGrow::ActionConnected
	Action_t1264377477 * ___ActionConnected_5;
	// System.Action ISN_SoomlaGrow::ActionDisconnected
	Action_t1264377477 * ___ActionDisconnected_6;
	// System.Boolean ISN_SoomlaGrow::_IsInitialized
	bool ____IsInitialized_7;

public:
	inline static int32_t get_offset_of_ActionInitialized_4() { return static_cast<int32_t>(offsetof(ISN_SoomlaGrow_t35334059_StaticFields, ___ActionInitialized_4)); }
	inline Action_t1264377477 * get_ActionInitialized_4() const { return ___ActionInitialized_4; }
	inline Action_t1264377477 ** get_address_of_ActionInitialized_4() { return &___ActionInitialized_4; }
	inline void set_ActionInitialized_4(Action_t1264377477 * value)
	{
		___ActionInitialized_4 = value;
		Il2CppCodeGenWriteBarrier((&___ActionInitialized_4), value);
	}

	inline static int32_t get_offset_of_ActionConnected_5() { return static_cast<int32_t>(offsetof(ISN_SoomlaGrow_t35334059_StaticFields, ___ActionConnected_5)); }
	inline Action_t1264377477 * get_ActionConnected_5() const { return ___ActionConnected_5; }
	inline Action_t1264377477 ** get_address_of_ActionConnected_5() { return &___ActionConnected_5; }
	inline void set_ActionConnected_5(Action_t1264377477 * value)
	{
		___ActionConnected_5 = value;
		Il2CppCodeGenWriteBarrier((&___ActionConnected_5), value);
	}

	inline static int32_t get_offset_of_ActionDisconnected_6() { return static_cast<int32_t>(offsetof(ISN_SoomlaGrow_t35334059_StaticFields, ___ActionDisconnected_6)); }
	inline Action_t1264377477 * get_ActionDisconnected_6() const { return ___ActionDisconnected_6; }
	inline Action_t1264377477 ** get_address_of_ActionDisconnected_6() { return &___ActionDisconnected_6; }
	inline void set_ActionDisconnected_6(Action_t1264377477 * value)
	{
		___ActionDisconnected_6 = value;
		Il2CppCodeGenWriteBarrier((&___ActionDisconnected_6), value);
	}

	inline static int32_t get_offset_of__IsInitialized_7() { return static_cast<int32_t>(offsetof(ISN_SoomlaGrow_t35334059_StaticFields, ____IsInitialized_7)); }
	inline bool get__IsInitialized_7() const { return ____IsInitialized_7; }
	inline bool* get_address_of__IsInitialized_7() { return &____IsInitialized_7; }
	inline void set__IsInitialized_7(bool value)
	{
		____IsInitialized_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISN_SOOMLAGROW_T35334059_H
#ifndef ISN_GAMESAVES_T3595065519_H
#define ISN_GAMESAVES_T3595065519_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ISN_GameSaves
struct  ISN_GameSaves_t3595065519  : public Singleton_1_t3224837665
{
public:

public:
};

struct ISN_GameSaves_t3595065519_StaticFields
{
public:
	// System.Action`1<GK_SaveRemoveResult> ISN_GameSaves::ActionSaveRemoved
	Action_1_t1435461071 * ___ActionSaveRemoved_4;
	// System.Action`1<GK_SaveResult> ISN_GameSaves::ActionGameSaved
	Action_1_t497746395 * ___ActionGameSaved_5;
	// System.Action`1<GK_FetchResult> ISN_GameSaves::ActionSavesFetched
	Action_1_t254498298 * ___ActionSavesFetched_6;
	// System.Action`1<GK_SavesResolveResult> ISN_GameSaves::ActionSavesResolved
	Action_1_t4133968337 * ___ActionSavesResolved_7;
	// System.Collections.Generic.Dictionary`2<System.String,GK_SavedGame> ISN_GameSaves::_CachedGameSaves
	Dictionary_2_t3230328059 * ____CachedGameSaves_8;

public:
	inline static int32_t get_offset_of_ActionSaveRemoved_4() { return static_cast<int32_t>(offsetof(ISN_GameSaves_t3595065519_StaticFields, ___ActionSaveRemoved_4)); }
	inline Action_1_t1435461071 * get_ActionSaveRemoved_4() const { return ___ActionSaveRemoved_4; }
	inline Action_1_t1435461071 ** get_address_of_ActionSaveRemoved_4() { return &___ActionSaveRemoved_4; }
	inline void set_ActionSaveRemoved_4(Action_1_t1435461071 * value)
	{
		___ActionSaveRemoved_4 = value;
		Il2CppCodeGenWriteBarrier((&___ActionSaveRemoved_4), value);
	}

	inline static int32_t get_offset_of_ActionGameSaved_5() { return static_cast<int32_t>(offsetof(ISN_GameSaves_t3595065519_StaticFields, ___ActionGameSaved_5)); }
	inline Action_1_t497746395 * get_ActionGameSaved_5() const { return ___ActionGameSaved_5; }
	inline Action_1_t497746395 ** get_address_of_ActionGameSaved_5() { return &___ActionGameSaved_5; }
	inline void set_ActionGameSaved_5(Action_1_t497746395 * value)
	{
		___ActionGameSaved_5 = value;
		Il2CppCodeGenWriteBarrier((&___ActionGameSaved_5), value);
	}

	inline static int32_t get_offset_of_ActionSavesFetched_6() { return static_cast<int32_t>(offsetof(ISN_GameSaves_t3595065519_StaticFields, ___ActionSavesFetched_6)); }
	inline Action_1_t254498298 * get_ActionSavesFetched_6() const { return ___ActionSavesFetched_6; }
	inline Action_1_t254498298 ** get_address_of_ActionSavesFetched_6() { return &___ActionSavesFetched_6; }
	inline void set_ActionSavesFetched_6(Action_1_t254498298 * value)
	{
		___ActionSavesFetched_6 = value;
		Il2CppCodeGenWriteBarrier((&___ActionSavesFetched_6), value);
	}

	inline static int32_t get_offset_of_ActionSavesResolved_7() { return static_cast<int32_t>(offsetof(ISN_GameSaves_t3595065519_StaticFields, ___ActionSavesResolved_7)); }
	inline Action_1_t4133968337 * get_ActionSavesResolved_7() const { return ___ActionSavesResolved_7; }
	inline Action_1_t4133968337 ** get_address_of_ActionSavesResolved_7() { return &___ActionSavesResolved_7; }
	inline void set_ActionSavesResolved_7(Action_1_t4133968337 * value)
	{
		___ActionSavesResolved_7 = value;
		Il2CppCodeGenWriteBarrier((&___ActionSavesResolved_7), value);
	}

	inline static int32_t get_offset_of__CachedGameSaves_8() { return static_cast<int32_t>(offsetof(ISN_GameSaves_t3595065519_StaticFields, ____CachedGameSaves_8)); }
	inline Dictionary_2_t3230328059 * get__CachedGameSaves_8() const { return ____CachedGameSaves_8; }
	inline Dictionary_2_t3230328059 ** get_address_of__CachedGameSaves_8() { return &____CachedGameSaves_8; }
	inline void set__CachedGameSaves_8(Dictionary_2_t3230328059 * value)
	{
		____CachedGameSaves_8 = value;
		Il2CppCodeGenWriteBarrier((&____CachedGameSaves_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISN_GAMESAVES_T3595065519_H
#ifndef APPCONTROLLER_T1810425167_H
#define APPCONTROLLER_T1810425167_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSNative.Core.AppController
struct  AppController_t1810425167  : public Singleton_1_t1440197313
{
public:

public:
};

struct AppController_t1810425167_StaticFields
{
public:
	// System.Action SA.IOSNative.Core.AppController::OnApplicationDidEnterBackground
	Action_t1264377477 * ___OnApplicationDidEnterBackground_4;
	// System.Action SA.IOSNative.Core.AppController::OnApplicationDidBecomeActive
	Action_t1264377477 * ___OnApplicationDidBecomeActive_5;
	// System.Action SA.IOSNative.Core.AppController::OnApplicationDidReceiveMemoryWarning
	Action_t1264377477 * ___OnApplicationDidReceiveMemoryWarning_6;
	// System.Action SA.IOSNative.Core.AppController::OnApplicationWillResignActive
	Action_t1264377477 * ___OnApplicationWillResignActive_7;
	// System.Action SA.IOSNative.Core.AppController::OnApplicationWillTerminate
	Action_t1264377477 * ___OnApplicationWillTerminate_8;
	// System.Action`1<SA.IOSNative.Models.LaunchUrl> SA.IOSNative.Core.AppController::OnOpenURL
	Action_1_t1427027165 * ___OnOpenURL_9;
	// System.Action`1<SA.IOSNative.Models.UniversalLink> SA.IOSNative.Core.AppController::OnContinueUserActivity
	Action_1_t4277050132 * ___OnContinueUserActivity_10;

public:
	inline static int32_t get_offset_of_OnApplicationDidEnterBackground_4() { return static_cast<int32_t>(offsetof(AppController_t1810425167_StaticFields, ___OnApplicationDidEnterBackground_4)); }
	inline Action_t1264377477 * get_OnApplicationDidEnterBackground_4() const { return ___OnApplicationDidEnterBackground_4; }
	inline Action_t1264377477 ** get_address_of_OnApplicationDidEnterBackground_4() { return &___OnApplicationDidEnterBackground_4; }
	inline void set_OnApplicationDidEnterBackground_4(Action_t1264377477 * value)
	{
		___OnApplicationDidEnterBackground_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnApplicationDidEnterBackground_4), value);
	}

	inline static int32_t get_offset_of_OnApplicationDidBecomeActive_5() { return static_cast<int32_t>(offsetof(AppController_t1810425167_StaticFields, ___OnApplicationDidBecomeActive_5)); }
	inline Action_t1264377477 * get_OnApplicationDidBecomeActive_5() const { return ___OnApplicationDidBecomeActive_5; }
	inline Action_t1264377477 ** get_address_of_OnApplicationDidBecomeActive_5() { return &___OnApplicationDidBecomeActive_5; }
	inline void set_OnApplicationDidBecomeActive_5(Action_t1264377477 * value)
	{
		___OnApplicationDidBecomeActive_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnApplicationDidBecomeActive_5), value);
	}

	inline static int32_t get_offset_of_OnApplicationDidReceiveMemoryWarning_6() { return static_cast<int32_t>(offsetof(AppController_t1810425167_StaticFields, ___OnApplicationDidReceiveMemoryWarning_6)); }
	inline Action_t1264377477 * get_OnApplicationDidReceiveMemoryWarning_6() const { return ___OnApplicationDidReceiveMemoryWarning_6; }
	inline Action_t1264377477 ** get_address_of_OnApplicationDidReceiveMemoryWarning_6() { return &___OnApplicationDidReceiveMemoryWarning_6; }
	inline void set_OnApplicationDidReceiveMemoryWarning_6(Action_t1264377477 * value)
	{
		___OnApplicationDidReceiveMemoryWarning_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnApplicationDidReceiveMemoryWarning_6), value);
	}

	inline static int32_t get_offset_of_OnApplicationWillResignActive_7() { return static_cast<int32_t>(offsetof(AppController_t1810425167_StaticFields, ___OnApplicationWillResignActive_7)); }
	inline Action_t1264377477 * get_OnApplicationWillResignActive_7() const { return ___OnApplicationWillResignActive_7; }
	inline Action_t1264377477 ** get_address_of_OnApplicationWillResignActive_7() { return &___OnApplicationWillResignActive_7; }
	inline void set_OnApplicationWillResignActive_7(Action_t1264377477 * value)
	{
		___OnApplicationWillResignActive_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnApplicationWillResignActive_7), value);
	}

	inline static int32_t get_offset_of_OnApplicationWillTerminate_8() { return static_cast<int32_t>(offsetof(AppController_t1810425167_StaticFields, ___OnApplicationWillTerminate_8)); }
	inline Action_t1264377477 * get_OnApplicationWillTerminate_8() const { return ___OnApplicationWillTerminate_8; }
	inline Action_t1264377477 ** get_address_of_OnApplicationWillTerminate_8() { return &___OnApplicationWillTerminate_8; }
	inline void set_OnApplicationWillTerminate_8(Action_t1264377477 * value)
	{
		___OnApplicationWillTerminate_8 = value;
		Il2CppCodeGenWriteBarrier((&___OnApplicationWillTerminate_8), value);
	}

	inline static int32_t get_offset_of_OnOpenURL_9() { return static_cast<int32_t>(offsetof(AppController_t1810425167_StaticFields, ___OnOpenURL_9)); }
	inline Action_1_t1427027165 * get_OnOpenURL_9() const { return ___OnOpenURL_9; }
	inline Action_1_t1427027165 ** get_address_of_OnOpenURL_9() { return &___OnOpenURL_9; }
	inline void set_OnOpenURL_9(Action_1_t1427027165 * value)
	{
		___OnOpenURL_9 = value;
		Il2CppCodeGenWriteBarrier((&___OnOpenURL_9), value);
	}

	inline static int32_t get_offset_of_OnContinueUserActivity_10() { return static_cast<int32_t>(offsetof(AppController_t1810425167_StaticFields, ___OnContinueUserActivity_10)); }
	inline Action_1_t4277050132 * get_OnContinueUserActivity_10() const { return ___OnContinueUserActivity_10; }
	inline Action_1_t4277050132 ** get_address_of_OnContinueUserActivity_10() { return &___OnContinueUserActivity_10; }
	inline void set_OnContinueUserActivity_10(Action_1_t4277050132 * value)
	{
		___OnContinueUserActivity_10 = value;
		Il2CppCodeGenWriteBarrier((&___OnContinueUserActivity_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPCONTROLLER_T1810425167_H
#ifndef GAMECENTER_RTM_T1714402562_H
#define GAMECENTER_RTM_T1714402562_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameCenter_RTM
struct  GameCenter_RTM_t1714402562  : public Singleton_1_t1344174708
{
public:
	// GK_RTM_Match GameCenter_RTM::_CurrentMatch
	GK_RTM_Match_t949836071 * ____CurrentMatch_4;
	// System.Collections.Generic.Dictionary`2<System.String,GK_Player> GameCenter_RTM::_NearbyPlayers
	Dictionary_2_t3246609090 * ____NearbyPlayers_5;

public:
	inline static int32_t get_offset_of__CurrentMatch_4() { return static_cast<int32_t>(offsetof(GameCenter_RTM_t1714402562, ____CurrentMatch_4)); }
	inline GK_RTM_Match_t949836071 * get__CurrentMatch_4() const { return ____CurrentMatch_4; }
	inline GK_RTM_Match_t949836071 ** get_address_of__CurrentMatch_4() { return &____CurrentMatch_4; }
	inline void set__CurrentMatch_4(GK_RTM_Match_t949836071 * value)
	{
		____CurrentMatch_4 = value;
		Il2CppCodeGenWriteBarrier((&____CurrentMatch_4), value);
	}

	inline static int32_t get_offset_of__NearbyPlayers_5() { return static_cast<int32_t>(offsetof(GameCenter_RTM_t1714402562, ____NearbyPlayers_5)); }
	inline Dictionary_2_t3246609090 * get__NearbyPlayers_5() const { return ____NearbyPlayers_5; }
	inline Dictionary_2_t3246609090 ** get_address_of__NearbyPlayers_5() { return &____NearbyPlayers_5; }
	inline void set__NearbyPlayers_5(Dictionary_2_t3246609090 * value)
	{
		____NearbyPlayers_5 = value;
		Il2CppCodeGenWriteBarrier((&____NearbyPlayers_5), value);
	}
};

struct GameCenter_RTM_t1714402562_StaticFields
{
public:
	// System.Action`1<GK_RTM_MatchStartedResult> GameCenter_RTM::ActionMatchStarted
	Action_1_t3750106405 * ___ActionMatchStarted_6;
	// System.Action`1<SA.Common.Models.Error> GameCenter_RTM::ActionMatchFailed
	Action_1_t513010639 * ___ActionMatchFailed_7;
	// System.Action`2<GK_Player,System.Boolean> GameCenter_RTM::ActionNearbyPlayerStateUpdated
	Action_2_t3922627552 * ___ActionNearbyPlayerStateUpdated_8;
	// System.Action`1<GK_RTM_QueryActivityResult> GameCenter_RTM::ActionActivityResultReceived
	Action_1_t3818092299 * ___ActionActivityResultReceived_9;
	// System.Action`1<SA.Common.Models.Error> GameCenter_RTM::ActionDataSendError
	Action_1_t513010639 * ___ActionDataSendError_10;
	// System.Action`2<GK_Player,System.Byte[]> GameCenter_RTM::ActionDataReceived
	Action_2_t3647019948 * ___ActionDataReceived_11;
	// System.Action`3<GK_Player,GK_PlayerConnectionState,GK_RTM_Match> GameCenter_RTM::ActionPlayerStateChanged
	Action_3_t2719518085 * ___ActionPlayerStateChanged_12;
	// System.Action`1<GK_Player> GameCenter_RTM::ActionDiconnectedPlayerReinvited
	Action_1_t3633820386 * ___ActionDiconnectedPlayerReinvited_13;

public:
	inline static int32_t get_offset_of_ActionMatchStarted_6() { return static_cast<int32_t>(offsetof(GameCenter_RTM_t1714402562_StaticFields, ___ActionMatchStarted_6)); }
	inline Action_1_t3750106405 * get_ActionMatchStarted_6() const { return ___ActionMatchStarted_6; }
	inline Action_1_t3750106405 ** get_address_of_ActionMatchStarted_6() { return &___ActionMatchStarted_6; }
	inline void set_ActionMatchStarted_6(Action_1_t3750106405 * value)
	{
		___ActionMatchStarted_6 = value;
		Il2CppCodeGenWriteBarrier((&___ActionMatchStarted_6), value);
	}

	inline static int32_t get_offset_of_ActionMatchFailed_7() { return static_cast<int32_t>(offsetof(GameCenter_RTM_t1714402562_StaticFields, ___ActionMatchFailed_7)); }
	inline Action_1_t513010639 * get_ActionMatchFailed_7() const { return ___ActionMatchFailed_7; }
	inline Action_1_t513010639 ** get_address_of_ActionMatchFailed_7() { return &___ActionMatchFailed_7; }
	inline void set_ActionMatchFailed_7(Action_1_t513010639 * value)
	{
		___ActionMatchFailed_7 = value;
		Il2CppCodeGenWriteBarrier((&___ActionMatchFailed_7), value);
	}

	inline static int32_t get_offset_of_ActionNearbyPlayerStateUpdated_8() { return static_cast<int32_t>(offsetof(GameCenter_RTM_t1714402562_StaticFields, ___ActionNearbyPlayerStateUpdated_8)); }
	inline Action_2_t3922627552 * get_ActionNearbyPlayerStateUpdated_8() const { return ___ActionNearbyPlayerStateUpdated_8; }
	inline Action_2_t3922627552 ** get_address_of_ActionNearbyPlayerStateUpdated_8() { return &___ActionNearbyPlayerStateUpdated_8; }
	inline void set_ActionNearbyPlayerStateUpdated_8(Action_2_t3922627552 * value)
	{
		___ActionNearbyPlayerStateUpdated_8 = value;
		Il2CppCodeGenWriteBarrier((&___ActionNearbyPlayerStateUpdated_8), value);
	}

	inline static int32_t get_offset_of_ActionActivityResultReceived_9() { return static_cast<int32_t>(offsetof(GameCenter_RTM_t1714402562_StaticFields, ___ActionActivityResultReceived_9)); }
	inline Action_1_t3818092299 * get_ActionActivityResultReceived_9() const { return ___ActionActivityResultReceived_9; }
	inline Action_1_t3818092299 ** get_address_of_ActionActivityResultReceived_9() { return &___ActionActivityResultReceived_9; }
	inline void set_ActionActivityResultReceived_9(Action_1_t3818092299 * value)
	{
		___ActionActivityResultReceived_9 = value;
		Il2CppCodeGenWriteBarrier((&___ActionActivityResultReceived_9), value);
	}

	inline static int32_t get_offset_of_ActionDataSendError_10() { return static_cast<int32_t>(offsetof(GameCenter_RTM_t1714402562_StaticFields, ___ActionDataSendError_10)); }
	inline Action_1_t513010639 * get_ActionDataSendError_10() const { return ___ActionDataSendError_10; }
	inline Action_1_t513010639 ** get_address_of_ActionDataSendError_10() { return &___ActionDataSendError_10; }
	inline void set_ActionDataSendError_10(Action_1_t513010639 * value)
	{
		___ActionDataSendError_10 = value;
		Il2CppCodeGenWriteBarrier((&___ActionDataSendError_10), value);
	}

	inline static int32_t get_offset_of_ActionDataReceived_11() { return static_cast<int32_t>(offsetof(GameCenter_RTM_t1714402562_StaticFields, ___ActionDataReceived_11)); }
	inline Action_2_t3647019948 * get_ActionDataReceived_11() const { return ___ActionDataReceived_11; }
	inline Action_2_t3647019948 ** get_address_of_ActionDataReceived_11() { return &___ActionDataReceived_11; }
	inline void set_ActionDataReceived_11(Action_2_t3647019948 * value)
	{
		___ActionDataReceived_11 = value;
		Il2CppCodeGenWriteBarrier((&___ActionDataReceived_11), value);
	}

	inline static int32_t get_offset_of_ActionPlayerStateChanged_12() { return static_cast<int32_t>(offsetof(GameCenter_RTM_t1714402562_StaticFields, ___ActionPlayerStateChanged_12)); }
	inline Action_3_t2719518085 * get_ActionPlayerStateChanged_12() const { return ___ActionPlayerStateChanged_12; }
	inline Action_3_t2719518085 ** get_address_of_ActionPlayerStateChanged_12() { return &___ActionPlayerStateChanged_12; }
	inline void set_ActionPlayerStateChanged_12(Action_3_t2719518085 * value)
	{
		___ActionPlayerStateChanged_12 = value;
		Il2CppCodeGenWriteBarrier((&___ActionPlayerStateChanged_12), value);
	}

	inline static int32_t get_offset_of_ActionDiconnectedPlayerReinvited_13() { return static_cast<int32_t>(offsetof(GameCenter_RTM_t1714402562_StaticFields, ___ActionDiconnectedPlayerReinvited_13)); }
	inline Action_1_t3633820386 * get_ActionDiconnectedPlayerReinvited_13() const { return ___ActionDiconnectedPlayerReinvited_13; }
	inline Action_1_t3633820386 ** get_address_of_ActionDiconnectedPlayerReinvited_13() { return &___ActionDiconnectedPlayerReinvited_13; }
	inline void set_ActionDiconnectedPlayerReinvited_13(Action_1_t3633820386 * value)
	{
		___ActionDiconnectedPlayerReinvited_13 = value;
		Il2CppCodeGenWriteBarrier((&___ActionDiconnectedPlayerReinvited_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMECENTER_RTM_T1714402562_H
#ifndef GAMECENTER_TBM_T2861413648_H
#define GAMECENTER_TBM_T2861413648_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameCenter_TBM
struct  GameCenter_TBM_t2861413648  : public Singleton_1_t2491185794
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,GK_TBM_Match> GameCenter_TBM::_Matches
	Dictionary_2_t4163805476 * ____Matches_17;

public:
	inline static int32_t get_offset_of__Matches_17() { return static_cast<int32_t>(offsetof(GameCenter_TBM_t2861413648, ____Matches_17)); }
	inline Dictionary_2_t4163805476 * get__Matches_17() const { return ____Matches_17; }
	inline Dictionary_2_t4163805476 ** get_address_of__Matches_17() { return &____Matches_17; }
	inline void set__Matches_17(Dictionary_2_t4163805476 * value)
	{
		____Matches_17 = value;
		Il2CppCodeGenWriteBarrier((&____Matches_17), value);
	}
};

struct GameCenter_TBM_t2861413648_StaticFields
{
public:
	// System.Action`1<GK_TBM_LoadMatchResult> GameCenter_TBM::ActionMatchInfoLoaded
	Action_1_t1401582932 * ___ActionMatchInfoLoaded_4;
	// System.Action`1<GK_TBM_LoadMatchesResult> GameCenter_TBM::ActionMatchesInfoLoaded
	Action_1_t101443120 * ___ActionMatchesInfoLoaded_5;
	// System.Action`1<GK_TBM_MatchDataUpdateResult> GameCenter_TBM::ActionMatchDataUpdated
	Action_1_t224071095 * ___ActionMatchDataUpdated_6;
	// System.Action`1<GK_TBM_MatchInitResult> GameCenter_TBM::ActionMatchFound
	Action_1_t1935478288 * ___ActionMatchFound_7;
	// System.Action`1<GK_TBM_MatchQuitResult> GameCenter_TBM::ActionMatchQuit
	Action_1_t1555055752 * ___ActionMatchQuit_8;
	// System.Action`1<GK_TBM_EndTrunResult> GameCenter_TBM::ActionTrunEnded
	Action_1_t3655562227 * ___ActionTrunEnded_9;
	// System.Action`1<GK_TBM_MatchEndResult> GameCenter_TBM::ActionMacthEnded
	Action_1_t3909832116 * ___ActionMacthEnded_10;
	// System.Action`1<GK_TBM_RematchResult> GameCenter_TBM::ActionRematched
	Action_1_t1218639671 * ___ActionRematched_11;
	// System.Action`1<GK_TBM_MatchRemovedResult> GameCenter_TBM::ActionMatchRemoved
	Action_1_t565105849 * ___ActionMatchRemoved_12;
	// System.Action`1<GK_TBM_MatchInitResult> GameCenter_TBM::ActionMatchInvitationAccepted
	Action_1_t1935478288 * ___ActionMatchInvitationAccepted_13;
	// System.Action`1<GK_TBM_MatchRemovedResult> GameCenter_TBM::ActionMatchInvitationDeclined
	Action_1_t565105849 * ___ActionMatchInvitationDeclined_14;
	// System.Action`1<GK_TBM_Match> GameCenter_TBM::ActionPlayerQuitForMatch
	Action_1_t256049476 * ___ActionPlayerQuitForMatch_15;
	// System.Action`1<GK_TBM_MatchTurnResult> GameCenter_TBM::ActionTrunReceived
	Action_1_t2448326972 * ___ActionTrunReceived_16;

public:
	inline static int32_t get_offset_of_ActionMatchInfoLoaded_4() { return static_cast<int32_t>(offsetof(GameCenter_TBM_t2861413648_StaticFields, ___ActionMatchInfoLoaded_4)); }
	inline Action_1_t1401582932 * get_ActionMatchInfoLoaded_4() const { return ___ActionMatchInfoLoaded_4; }
	inline Action_1_t1401582932 ** get_address_of_ActionMatchInfoLoaded_4() { return &___ActionMatchInfoLoaded_4; }
	inline void set_ActionMatchInfoLoaded_4(Action_1_t1401582932 * value)
	{
		___ActionMatchInfoLoaded_4 = value;
		Il2CppCodeGenWriteBarrier((&___ActionMatchInfoLoaded_4), value);
	}

	inline static int32_t get_offset_of_ActionMatchesInfoLoaded_5() { return static_cast<int32_t>(offsetof(GameCenter_TBM_t2861413648_StaticFields, ___ActionMatchesInfoLoaded_5)); }
	inline Action_1_t101443120 * get_ActionMatchesInfoLoaded_5() const { return ___ActionMatchesInfoLoaded_5; }
	inline Action_1_t101443120 ** get_address_of_ActionMatchesInfoLoaded_5() { return &___ActionMatchesInfoLoaded_5; }
	inline void set_ActionMatchesInfoLoaded_5(Action_1_t101443120 * value)
	{
		___ActionMatchesInfoLoaded_5 = value;
		Il2CppCodeGenWriteBarrier((&___ActionMatchesInfoLoaded_5), value);
	}

	inline static int32_t get_offset_of_ActionMatchDataUpdated_6() { return static_cast<int32_t>(offsetof(GameCenter_TBM_t2861413648_StaticFields, ___ActionMatchDataUpdated_6)); }
	inline Action_1_t224071095 * get_ActionMatchDataUpdated_6() const { return ___ActionMatchDataUpdated_6; }
	inline Action_1_t224071095 ** get_address_of_ActionMatchDataUpdated_6() { return &___ActionMatchDataUpdated_6; }
	inline void set_ActionMatchDataUpdated_6(Action_1_t224071095 * value)
	{
		___ActionMatchDataUpdated_6 = value;
		Il2CppCodeGenWriteBarrier((&___ActionMatchDataUpdated_6), value);
	}

	inline static int32_t get_offset_of_ActionMatchFound_7() { return static_cast<int32_t>(offsetof(GameCenter_TBM_t2861413648_StaticFields, ___ActionMatchFound_7)); }
	inline Action_1_t1935478288 * get_ActionMatchFound_7() const { return ___ActionMatchFound_7; }
	inline Action_1_t1935478288 ** get_address_of_ActionMatchFound_7() { return &___ActionMatchFound_7; }
	inline void set_ActionMatchFound_7(Action_1_t1935478288 * value)
	{
		___ActionMatchFound_7 = value;
		Il2CppCodeGenWriteBarrier((&___ActionMatchFound_7), value);
	}

	inline static int32_t get_offset_of_ActionMatchQuit_8() { return static_cast<int32_t>(offsetof(GameCenter_TBM_t2861413648_StaticFields, ___ActionMatchQuit_8)); }
	inline Action_1_t1555055752 * get_ActionMatchQuit_8() const { return ___ActionMatchQuit_8; }
	inline Action_1_t1555055752 ** get_address_of_ActionMatchQuit_8() { return &___ActionMatchQuit_8; }
	inline void set_ActionMatchQuit_8(Action_1_t1555055752 * value)
	{
		___ActionMatchQuit_8 = value;
		Il2CppCodeGenWriteBarrier((&___ActionMatchQuit_8), value);
	}

	inline static int32_t get_offset_of_ActionTrunEnded_9() { return static_cast<int32_t>(offsetof(GameCenter_TBM_t2861413648_StaticFields, ___ActionTrunEnded_9)); }
	inline Action_1_t3655562227 * get_ActionTrunEnded_9() const { return ___ActionTrunEnded_9; }
	inline Action_1_t3655562227 ** get_address_of_ActionTrunEnded_9() { return &___ActionTrunEnded_9; }
	inline void set_ActionTrunEnded_9(Action_1_t3655562227 * value)
	{
		___ActionTrunEnded_9 = value;
		Il2CppCodeGenWriteBarrier((&___ActionTrunEnded_9), value);
	}

	inline static int32_t get_offset_of_ActionMacthEnded_10() { return static_cast<int32_t>(offsetof(GameCenter_TBM_t2861413648_StaticFields, ___ActionMacthEnded_10)); }
	inline Action_1_t3909832116 * get_ActionMacthEnded_10() const { return ___ActionMacthEnded_10; }
	inline Action_1_t3909832116 ** get_address_of_ActionMacthEnded_10() { return &___ActionMacthEnded_10; }
	inline void set_ActionMacthEnded_10(Action_1_t3909832116 * value)
	{
		___ActionMacthEnded_10 = value;
		Il2CppCodeGenWriteBarrier((&___ActionMacthEnded_10), value);
	}

	inline static int32_t get_offset_of_ActionRematched_11() { return static_cast<int32_t>(offsetof(GameCenter_TBM_t2861413648_StaticFields, ___ActionRematched_11)); }
	inline Action_1_t1218639671 * get_ActionRematched_11() const { return ___ActionRematched_11; }
	inline Action_1_t1218639671 ** get_address_of_ActionRematched_11() { return &___ActionRematched_11; }
	inline void set_ActionRematched_11(Action_1_t1218639671 * value)
	{
		___ActionRematched_11 = value;
		Il2CppCodeGenWriteBarrier((&___ActionRematched_11), value);
	}

	inline static int32_t get_offset_of_ActionMatchRemoved_12() { return static_cast<int32_t>(offsetof(GameCenter_TBM_t2861413648_StaticFields, ___ActionMatchRemoved_12)); }
	inline Action_1_t565105849 * get_ActionMatchRemoved_12() const { return ___ActionMatchRemoved_12; }
	inline Action_1_t565105849 ** get_address_of_ActionMatchRemoved_12() { return &___ActionMatchRemoved_12; }
	inline void set_ActionMatchRemoved_12(Action_1_t565105849 * value)
	{
		___ActionMatchRemoved_12 = value;
		Il2CppCodeGenWriteBarrier((&___ActionMatchRemoved_12), value);
	}

	inline static int32_t get_offset_of_ActionMatchInvitationAccepted_13() { return static_cast<int32_t>(offsetof(GameCenter_TBM_t2861413648_StaticFields, ___ActionMatchInvitationAccepted_13)); }
	inline Action_1_t1935478288 * get_ActionMatchInvitationAccepted_13() const { return ___ActionMatchInvitationAccepted_13; }
	inline Action_1_t1935478288 ** get_address_of_ActionMatchInvitationAccepted_13() { return &___ActionMatchInvitationAccepted_13; }
	inline void set_ActionMatchInvitationAccepted_13(Action_1_t1935478288 * value)
	{
		___ActionMatchInvitationAccepted_13 = value;
		Il2CppCodeGenWriteBarrier((&___ActionMatchInvitationAccepted_13), value);
	}

	inline static int32_t get_offset_of_ActionMatchInvitationDeclined_14() { return static_cast<int32_t>(offsetof(GameCenter_TBM_t2861413648_StaticFields, ___ActionMatchInvitationDeclined_14)); }
	inline Action_1_t565105849 * get_ActionMatchInvitationDeclined_14() const { return ___ActionMatchInvitationDeclined_14; }
	inline Action_1_t565105849 ** get_address_of_ActionMatchInvitationDeclined_14() { return &___ActionMatchInvitationDeclined_14; }
	inline void set_ActionMatchInvitationDeclined_14(Action_1_t565105849 * value)
	{
		___ActionMatchInvitationDeclined_14 = value;
		Il2CppCodeGenWriteBarrier((&___ActionMatchInvitationDeclined_14), value);
	}

	inline static int32_t get_offset_of_ActionPlayerQuitForMatch_15() { return static_cast<int32_t>(offsetof(GameCenter_TBM_t2861413648_StaticFields, ___ActionPlayerQuitForMatch_15)); }
	inline Action_1_t256049476 * get_ActionPlayerQuitForMatch_15() const { return ___ActionPlayerQuitForMatch_15; }
	inline Action_1_t256049476 ** get_address_of_ActionPlayerQuitForMatch_15() { return &___ActionPlayerQuitForMatch_15; }
	inline void set_ActionPlayerQuitForMatch_15(Action_1_t256049476 * value)
	{
		___ActionPlayerQuitForMatch_15 = value;
		Il2CppCodeGenWriteBarrier((&___ActionPlayerQuitForMatch_15), value);
	}

	inline static int32_t get_offset_of_ActionTrunReceived_16() { return static_cast<int32_t>(offsetof(GameCenter_TBM_t2861413648_StaticFields, ___ActionTrunReceived_16)); }
	inline Action_1_t2448326972 * get_ActionTrunReceived_16() const { return ___ActionTrunReceived_16; }
	inline Action_1_t2448326972 ** get_address_of_ActionTrunReceived_16() { return &___ActionTrunReceived_16; }
	inline void set_ActionTrunReceived_16(Action_1_t2448326972 * value)
	{
		___ActionTrunReceived_16 = value;
		Il2CppCodeGenWriteBarrier((&___ActionTrunReceived_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMECENTER_TBM_T2861413648_H
#ifndef GAMECENTERINVITATIONS_T630122588_H
#define GAMECENTERINVITATIONS_T630122588_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameCenterInvitations
struct  GameCenterInvitations_t630122588  : public Singleton_1_t259894734
{
public:

public:
};

struct GameCenterInvitations_t630122588_StaticFields
{
public:
	// System.Action`2<GK_Player,GK_InviteRecipientResponse> GameCenterInvitations::ActionInviteeResponse
	Action_2_t923696397 * ___ActionInviteeResponse_4;
	// System.Action`2<GK_MatchType,GK_Invite> GameCenterInvitations::ActionPlayerAcceptedInvitation
	Action_2_t493924140 * ___ActionPlayerAcceptedInvitation_5;
	// System.Action`3<GK_MatchType,System.String[],GK_Player[]> GameCenterInvitations::ActionPlayerRequestedMatchWithRecipients
	Action_3_t4118644065 * ___ActionPlayerRequestedMatchWithRecipients_6;

public:
	inline static int32_t get_offset_of_ActionInviteeResponse_4() { return static_cast<int32_t>(offsetof(GameCenterInvitations_t630122588_StaticFields, ___ActionInviteeResponse_4)); }
	inline Action_2_t923696397 * get_ActionInviteeResponse_4() const { return ___ActionInviteeResponse_4; }
	inline Action_2_t923696397 ** get_address_of_ActionInviteeResponse_4() { return &___ActionInviteeResponse_4; }
	inline void set_ActionInviteeResponse_4(Action_2_t923696397 * value)
	{
		___ActionInviteeResponse_4 = value;
		Il2CppCodeGenWriteBarrier((&___ActionInviteeResponse_4), value);
	}

	inline static int32_t get_offset_of_ActionPlayerAcceptedInvitation_5() { return static_cast<int32_t>(offsetof(GameCenterInvitations_t630122588_StaticFields, ___ActionPlayerAcceptedInvitation_5)); }
	inline Action_2_t493924140 * get_ActionPlayerAcceptedInvitation_5() const { return ___ActionPlayerAcceptedInvitation_5; }
	inline Action_2_t493924140 ** get_address_of_ActionPlayerAcceptedInvitation_5() { return &___ActionPlayerAcceptedInvitation_5; }
	inline void set_ActionPlayerAcceptedInvitation_5(Action_2_t493924140 * value)
	{
		___ActionPlayerAcceptedInvitation_5 = value;
		Il2CppCodeGenWriteBarrier((&___ActionPlayerAcceptedInvitation_5), value);
	}

	inline static int32_t get_offset_of_ActionPlayerRequestedMatchWithRecipients_6() { return static_cast<int32_t>(offsetof(GameCenterInvitations_t630122588_StaticFields, ___ActionPlayerRequestedMatchWithRecipients_6)); }
	inline Action_3_t4118644065 * get_ActionPlayerRequestedMatchWithRecipients_6() const { return ___ActionPlayerRequestedMatchWithRecipients_6; }
	inline Action_3_t4118644065 ** get_address_of_ActionPlayerRequestedMatchWithRecipients_6() { return &___ActionPlayerRequestedMatchWithRecipients_6; }
	inline void set_ActionPlayerRequestedMatchWithRecipients_6(Action_3_t4118644065 * value)
	{
		___ActionPlayerRequestedMatchWithRecipients_6 = value;
		Il2CppCodeGenWriteBarrier((&___ActionPlayerRequestedMatchWithRecipients_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMECENTERINVITATIONS_T630122588_H
#ifndef BASEMESHEFFECT_T2440176439_H
#define BASEMESHEFFECT_T2440176439_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BaseMeshEffect
struct  BaseMeshEffect_t2440176439  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.UI.Graphic UnityEngine.UI.BaseMeshEffect::m_Graphic
	Graphic_t1660335611 * ___m_Graphic_2;

public:
	inline static int32_t get_offset_of_m_Graphic_2() { return static_cast<int32_t>(offsetof(BaseMeshEffect_t2440176439, ___m_Graphic_2)); }
	inline Graphic_t1660335611 * get_m_Graphic_2() const { return ___m_Graphic_2; }
	inline Graphic_t1660335611 ** get_address_of_m_Graphic_2() { return &___m_Graphic_2; }
	inline void set_m_Graphic_2(Graphic_t1660335611 * value)
	{
		___m_Graphic_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Graphic_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEMESHEFFECT_T2440176439_H
#ifndef POSITIONASUV1_T3991086357_H
#define POSITIONASUV1_T3991086357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.PositionAsUV1
struct  PositionAsUV1_t3991086357  : public BaseMeshEffect_t2440176439
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITIONASUV1_T3991086357_H
#ifndef SHADOW_T773074319_H
#define SHADOW_T773074319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Shadow
struct  Shadow_t773074319  : public BaseMeshEffect_t2440176439
{
public:
	// UnityEngine.Color UnityEngine.UI.Shadow::m_EffectColor
	Color_t2555686324  ___m_EffectColor_3;
	// UnityEngine.Vector2 UnityEngine.UI.Shadow::m_EffectDistance
	Vector2_t2156229523  ___m_EffectDistance_4;
	// System.Boolean UnityEngine.UI.Shadow::m_UseGraphicAlpha
	bool ___m_UseGraphicAlpha_5;

public:
	inline static int32_t get_offset_of_m_EffectColor_3() { return static_cast<int32_t>(offsetof(Shadow_t773074319, ___m_EffectColor_3)); }
	inline Color_t2555686324  get_m_EffectColor_3() const { return ___m_EffectColor_3; }
	inline Color_t2555686324 * get_address_of_m_EffectColor_3() { return &___m_EffectColor_3; }
	inline void set_m_EffectColor_3(Color_t2555686324  value)
	{
		___m_EffectColor_3 = value;
	}

	inline static int32_t get_offset_of_m_EffectDistance_4() { return static_cast<int32_t>(offsetof(Shadow_t773074319, ___m_EffectDistance_4)); }
	inline Vector2_t2156229523  get_m_EffectDistance_4() const { return ___m_EffectDistance_4; }
	inline Vector2_t2156229523 * get_address_of_m_EffectDistance_4() { return &___m_EffectDistance_4; }
	inline void set_m_EffectDistance_4(Vector2_t2156229523  value)
	{
		___m_EffectDistance_4 = value;
	}

	inline static int32_t get_offset_of_m_UseGraphicAlpha_5() { return static_cast<int32_t>(offsetof(Shadow_t773074319, ___m_UseGraphicAlpha_5)); }
	inline bool get_m_UseGraphicAlpha_5() const { return ___m_UseGraphicAlpha_5; }
	inline bool* get_address_of_m_UseGraphicAlpha_5() { return &___m_UseGraphicAlpha_5; }
	inline void set_m_UseGraphicAlpha_5(bool value)
	{
		___m_UseGraphicAlpha_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADOW_T773074319_H
#ifndef OUTLINE_T2536100125_H
#define OUTLINE_T2536100125_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Outline
struct  Outline_t2536100125  : public Shadow_t773074319
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OUTLINE_T2536100125_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2300 = { sizeof (Raycast3DCallback_t701940803), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2301 = { sizeof (Raycast2DCallback_t768590915), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2302 = { sizeof (RaycastAllCallback_t1884415901), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2303 = { sizeof (GetRayIntersectionAllCallback_t3913627115), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2304 = { sizeof (GetRayIntersectionAllNonAllocCallback_t2311174851), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2305 = { sizeof (GetRaycastNonAllocCallback_t3841783507), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2306 = { sizeof (VertexHelper_t2453304189), -1, sizeof(VertexHelper_t2453304189_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2306[11] = 
{
	VertexHelper_t2453304189::get_offset_of_m_Positions_0(),
	VertexHelper_t2453304189::get_offset_of_m_Colors_1(),
	VertexHelper_t2453304189::get_offset_of_m_Uv0S_2(),
	VertexHelper_t2453304189::get_offset_of_m_Uv1S_3(),
	VertexHelper_t2453304189::get_offset_of_m_Uv2S_4(),
	VertexHelper_t2453304189::get_offset_of_m_Uv3S_5(),
	VertexHelper_t2453304189::get_offset_of_m_Normals_6(),
	VertexHelper_t2453304189::get_offset_of_m_Tangents_7(),
	VertexHelper_t2453304189::get_offset_of_m_Indices_8(),
	VertexHelper_t2453304189_StaticFields::get_offset_of_s_DefaultTangent_9(),
	VertexHelper_t2453304189_StaticFields::get_offset_of_s_DefaultNormal_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2307 = { sizeof (BaseVertexEffect_t2675891272), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2308 = { sizeof (BaseMeshEffect_t2440176439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2308[1] = 
{
	BaseMeshEffect_t2440176439::get_offset_of_m_Graphic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2309 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2310 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2311 = { sizeof (Outline_t2536100125), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2312 = { sizeof (PositionAsUV1_t3991086357), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2313 = { sizeof (Shadow_t773074319), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2313[4] = 
{
	Shadow_t773074319::get_offset_of_m_EffectColor_3(),
	Shadow_t773074319::get_offset_of_m_EffectDistance_4(),
	Shadow_t773074319::get_offset_of_m_UseGraphicAlpha_5(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2314 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255366), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2314[1] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2315 = { sizeof (U24ArrayTypeU3D12_t2488454196)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D12_t2488454196 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2316 = { sizeof (U3CModuleU3E_t692745548), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2317 = { sizeof (ISD_Settings_t3533405735), -1, sizeof(ISD_Settings_t3533405735_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2317[24] = 
{
	0,
	ISD_Settings_t3533405735::get_offset_of_IsfwSettingOpen_3(),
	ISD_Settings_t3533405735::get_offset_of_IsLibSettingOpen_4(),
	ISD_Settings_t3533405735::get_offset_of_IslinkerSettingOpne_5(),
	ISD_Settings_t3533405735::get_offset_of_IscompilerSettingsOpen_6(),
	ISD_Settings_t3533405735::get_offset_of_IsPlistSettingsOpen_7(),
	ISD_Settings_t3533405735::get_offset_of_IsLanguageSettingOpen_8(),
	ISD_Settings_t3533405735::get_offset_of_IsDefFrameworksOpen_9(),
	ISD_Settings_t3533405735::get_offset_of_IsDefLibrariesOpen_10(),
	ISD_Settings_t3533405735::get_offset_of_IsBuildSettingsOpen_11(),
	ISD_Settings_t3533405735::get_offset_of_ToolbarIndex_12(),
	ISD_Settings_t3533405735::get_offset_of_enableBitCode_13(),
	ISD_Settings_t3533405735::get_offset_of_enableTestability_14(),
	ISD_Settings_t3533405735::get_offset_of_generateProfilingCode_15(),
	ISD_Settings_t3533405735::get_offset_of_Frameworks_16(),
	ISD_Settings_t3533405735::get_offset_of_Libraries_17(),
	ISD_Settings_t3533405735::get_offset_of_Flags_18(),
	ISD_Settings_t3533405735::get_offset_of_PlistVariables_19(),
	ISD_Settings_t3533405735::get_offset_of_VariableDictionary_20(),
	ISD_Settings_t3533405735::get_offset_of_langFolders_21(),
	ISD_Settings_t3533405735::get_offset_of_Files_22(),
	0,
	0,
	ISD_Settings_t3533405735_StaticFields::get_offset_of_instance_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2318 = { sizeof (FlagType_t2855246900)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2318[3] = 
{
	FlagType_t2855246900::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2319 = { sizeof (iOSFramework_t87174759)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2319[95] = 
{
	iOSFramework_t87174759::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2320 = { sizeof (iOSLibrary_t2820135979)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2320[160] = 
{
	iOSLibrary_t2820135979::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2321 = { sizeof (PlistValueTypes_t4090938833)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2321[7] = 
{
	PlistValueTypes_t4090938833::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2322 = { sizeof (AssetFile_t64015363), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2322[3] = 
{
	AssetFile_t64015363::get_offset_of_IsOpen_0(),
	AssetFile_t64015363::get_offset_of_XCodePath_1(),
	AssetFile_t64015363::get_offset_of_Asset_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2323 = { sizeof (Flag_t3377423384), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2323[3] = 
{
	Flag_t3377423384::get_offset_of_IsOpen_0(),
	Flag_t3377423384::get_offset_of_Name_1(),
	Flag_t3377423384::get_offset_of_Type_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2324 = { sizeof (Framework_t3395257401), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2324[4] = 
{
	Framework_t3395257401::get_offset_of_IsOpen_0(),
	Framework_t3395257401::get_offset_of_Type_1(),
	Framework_t3395257401::get_offset_of_IsOptional_2(),
	Framework_t3395257401::get_offset_of_IsEmbeded_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2325 = { sizeof (ISD_FrameworkHandler_t3201591945), -1, sizeof(ISD_FrameworkHandler_t3201591945_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2325[1] = 
{
	ISD_FrameworkHandler_t3201591945_StaticFields::get_offset_of__DefaultFrameworks_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2326 = { sizeof (U3CGetImportedFrameworksU3Ec__AnonStorey0_t3484212793), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2326[1] = 
{
	U3CGetImportedFrameworksU3Ec__AnonStorey0_t3484212793::get_offset_of_dirrExtensions_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2327 = { sizeof (U3CGetImportedLibrariesU3Ec__AnonStorey1_t3892267358), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2327[1] = 
{
	U3CGetImportedLibrariesU3Ec__AnonStorey1_t3892267358::get_offset_of_fileExtensions_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2328 = { sizeof (Lib_t3857655653), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2328[3] = 
{
	Lib_t3857655653::get_offset_of_IsOpen_0(),
	Lib_t3857655653::get_offset_of_Type_1(),
	Lib_t3857655653::get_offset_of_IsOptional_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2329 = { sizeof (ISD_LibHandler_t1721854397), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2330 = { sizeof (Variable_t1035990833), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2330[9] = 
{
	Variable_t1035990833::get_offset_of_IsOpen_0(),
	Variable_t1035990833::get_offset_of_IsListOpen_1(),
	Variable_t1035990833::get_offset_of_Name_2(),
	Variable_t1035990833::get_offset_of_Type_3(),
	Variable_t1035990833::get_offset_of_StringValue_4(),
	Variable_t1035990833::get_offset_of_IntegerValue_5(),
	Variable_t1035990833::get_offset_of_FloatValue_6(),
	Variable_t1035990833::get_offset_of_BooleanValue_7(),
	Variable_t1035990833::get_offset_of_ChildrensIds_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2331 = { sizeof (VariableId_t222428825), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2331[2] = 
{
	VariableId_t222428825::get_offset_of_VariableValue_0(),
	VariableId_t222428825::get_offset_of_uniqueIdKey_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2332 = { sizeof (MiniJSON_t211875846), -1, sizeof(MiniJSON_t211875846_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2332[15] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	MiniJSON_t211875846_StaticFields::get_offset_of_lastErrorIndex_13(),
	MiniJSON_t211875846_StaticFields::get_offset_of_lastDecode_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2333 = { sizeof (MiniJsonExtensions_t1989422878), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2334 = { sizeof (PBXBuildFile_t2610392694), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2334[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2335 = { sizeof (PBXBuildPhase_t3890765868), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2335[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2336 = { sizeof (PBXFrameworksBuildPhase_t1675361410), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2337 = { sizeof (PBXResourcesBuildPhase_t3333333421), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2338 = { sizeof (PBXShellScriptBuildPhase_t3527688836), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2339 = { sizeof (PBXSourcesBuildPhase_t187938102), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2340 = { sizeof (PBXCopyFilesBuildPhase_t4167310815), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2341 = { sizeof (PBXDictionary_t2078602241), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2342 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2343 = { sizeof (PBXFileReference_t491780957), -1, sizeof(PBXFileReference_t491780957_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2343[11] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	PBXFileReference_t491780957::get_offset_of_compilerFlags_9(),
	PBXFileReference_t491780957::get_offset_of_buildPhase_10(),
	PBXFileReference_t491780957::get_offset_of_trees_11(),
	PBXFileReference_t491780957_StaticFields::get_offset_of_typeNames_12(),
	PBXFileReference_t491780957_StaticFields::get_offset_of_typePhases_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2344 = { sizeof (TreeEnum_t2111024294)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2344[7] = 
{
	TreeEnum_t2111024294::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2345 = { sizeof (PBXGroup_t2128436724), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2345[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2346 = { sizeof (PBXList_t4289188522), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2347 = { sizeof (PBXObject_t3397571057), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2347[3] = 
{
	0,
	PBXObject_t3397571057::get_offset_of__guid_1(),
	PBXObject_t3397571057::get_offset_of__data_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2348 = { sizeof (PBXNativeTarget_t793394962), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2349 = { sizeof (PBXContainerItemProxy_t3112265051), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2350 = { sizeof (PBXReferenceProxy_t1939220145), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2351 = { sizeof (PBXResolver_t144121547), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2351[3] = 
{
	PBXResolver_t144121547::get_offset_of_objects_0(),
	PBXResolver_t144121547::get_offset_of_rootObject_1(),
	PBXResolver_t144121547::get_offset_of_index_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2352 = { sizeof (PBXResolverReverseIndex_t3234151459), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2353 = { sizeof (PBXParser_t1614426892), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2353[24] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	PBXParser_t1614426892::get_offset_of_data_20(),
	PBXParser_t1614426892::get_offset_of_index_21(),
	PBXParser_t1614426892::get_offset_of_resolver_22(),
	PBXParser_t1614426892::get_offset_of_marker_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2354 = { sizeof (PBXProject_t4035248171), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2354[3] = 
{
	PBXProject_t4035248171::get_offset_of_MAINGROUP_KEY_3(),
	PBXProject_t4035248171::get_offset_of_KNOWN_REGIONS_KEY_4(),
	PBXProject_t4035248171::get_offset_of__clearedLoc_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2355 = { sizeof (PBXSortedDictionary_t718889789), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2356 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2357 = { sizeof (PBXVariantGroup_t2964284507), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2358 = { sizeof (Plist_t3259848363), -1, sizeof(Plist_t3259848363_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2358[8] = 
{
	Plist_t3259848363_StaticFields::get_offset_of_offsetTable_0(),
	Plist_t3259848363_StaticFields::get_offset_of_objectTable_1(),
	Plist_t3259848363_StaticFields::get_offset_of_refCount_2(),
	Plist_t3259848363_StaticFields::get_offset_of_objRefSize_3(),
	Plist_t3259848363_StaticFields::get_offset_of_offsetByteSize_4(),
	Plist_t3259848363_StaticFields::get_offset_of_offsetTableOffset_5(),
	Plist_t3259848363_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_6(),
	Plist_t3259848363_StaticFields::get_offset_of_U3CU3Ef__switchU24map1_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2359 = { sizeof (plistType_t2342801093)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2359[4] = 
{
	plistType_t2342801093::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2360 = { sizeof (PlistDateConverter_t1153959355), -1, sizeof(PlistDateConverter_t1153959355_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2360[1] = 
{
	PlistDateConverter_t1153959355_StaticFields::get_offset_of_timeDifference_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2361 = { sizeof (XCBuildConfiguration_t1451066300), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2361[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2362 = { sizeof (XCConfigurationList_t1100530024), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2363 = { sizeof (XCMod_t1014962890), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2363[4] = 
{
	XCMod_t1014962890::get_offset_of__datastore_0(),
	XCMod_t1014962890::get_offset_of__libs_1(),
	XCMod_t1014962890::get_offset_of_U3CnameU3Ek__BackingField_2(),
	XCMod_t1014962890::get_offset_of_U3CpathU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2364 = { sizeof (XCModFile_t1016819465), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2364[2] = 
{
	XCModFile_t1016819465::get_offset_of_U3CfilePathU3Ek__BackingField_0(),
	XCModFile_t1016819465::get_offset_of_U3CisWeakU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2365 = { sizeof (XCodePostProcess_t1429692402), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2366 = { sizeof (XCPlist_t2540119850), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2366[11] = 
{
	XCPlist_t2540119850::get_offset_of_plistPath_0(),
	XCPlist_t2540119850::get_offset_of_plistModified_1(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2367 = { sizeof (XCProject_t3157646134), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2367[21] = 
{
	XCProject_t3157646134::get_offset_of__datastore_0(),
	XCProject_t3157646134::get_offset_of__objects_1(),
	XCProject_t3157646134::get_offset_of__rootGroup_2(),
	XCProject_t3157646134::get_offset_of__rootObjectKey_3(),
	XCProject_t3157646134::get_offset_of_U3CprojectRootPathU3Ek__BackingField_4(),
	XCProject_t3157646134::get_offset_of_projectFileInfo_5(),
	XCProject_t3157646134::get_offset_of_U3CfilePathU3Ek__BackingField_6(),
	XCProject_t3157646134::get_offset_of_modified_7(),
	XCProject_t3157646134::get_offset_of__buildFiles_8(),
	XCProject_t3157646134::get_offset_of__groups_9(),
	XCProject_t3157646134::get_offset_of__fileReferences_10(),
	XCProject_t3157646134::get_offset_of__nativeTargets_11(),
	XCProject_t3157646134::get_offset_of__frameworkBuildPhases_12(),
	XCProject_t3157646134::get_offset_of__resourcesBuildPhases_13(),
	XCProject_t3157646134::get_offset_of__shellScriptBuildPhases_14(),
	XCProject_t3157646134::get_offset_of__sourcesBuildPhases_15(),
	XCProject_t3157646134::get_offset_of__copyBuildPhases_16(),
	XCProject_t3157646134::get_offset_of__variantGroups_17(),
	XCProject_t3157646134::get_offset_of__buildConfigurations_18(),
	XCProject_t3157646134::get_offset_of__configurationLists_19(),
	XCProject_t3157646134::get_offset_of__project_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2368 = { sizeof (ISN_SoomlaGrow_t35334059), -1, sizeof(ISN_SoomlaGrow_t35334059_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2368[4] = 
{
	ISN_SoomlaGrow_t35334059_StaticFields::get_offset_of_ActionInitialized_4(),
	ISN_SoomlaGrow_t35334059_StaticFields::get_offset_of_ActionConnected_5(),
	ISN_SoomlaGrow_t35334059_StaticFields::get_offset_of_ActionDisconnected_6(),
	ISN_SoomlaGrow_t35334059_StaticFields::get_offset_of__IsInitialized_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2369 = { sizeof (ISN_SoomlaAction_t722285317)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2369[6] = 
{
	ISN_SoomlaAction_t722285317::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2370 = { sizeof (ISN_SoomlaEvent_t315777455)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2370[5] = 
{
	ISN_SoomlaEvent_t315777455::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2371 = { sizeof (ISN_SoomlaProvider_t2652759424)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2371[13] = 
{
	ISN_SoomlaProvider_t2652759424::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2372 = { sizeof (IOSNativeSettings_t531545985), -1, sizeof(IOSNativeSettings_t531545985_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2372[60] = 
{
	0,
	IOSNativeSettings_t531545985::get_offset_of_EnableGameCenterAPI_3(),
	IOSNativeSettings_t531545985::get_offset_of_EnableInAppsAPI_4(),
	IOSNativeSettings_t531545985::get_offset_of_EnableCameraAPI_5(),
	IOSNativeSettings_t531545985::get_offset_of_EnableSocialSharingAPI_6(),
	IOSNativeSettings_t531545985::get_offset_of_EnablePickerAPI_7(),
	IOSNativeSettings_t531545985::get_offset_of_EnableMediaPlayerAPI_8(),
	IOSNativeSettings_t531545985::get_offset_of_EnableReplayKit_9(),
	IOSNativeSettings_t531545985::get_offset_of_EnableCloudKit_10(),
	IOSNativeSettings_t531545985::get_offset_of_EnableSoomla_11(),
	IOSNativeSettings_t531545985::get_offset_of_EnableGestureAPI_12(),
	IOSNativeSettings_t531545985::get_offset_of_EnableForceTouchAPI_13(),
	IOSNativeSettings_t531545985::get_offset_of_EnablePushNotificationsAPI_14(),
	IOSNativeSettings_t531545985::get_offset_of_EnableContactsAPI_15(),
	IOSNativeSettings_t531545985::get_offset_of_EnableAppEventsAPI_16(),
	IOSNativeSettings_t531545985::get_offset_of_EnableUserNotificationsAPI_17(),
	IOSNativeSettings_t531545985::get_offset_of_EnablePermissionAPI_18(),
	IOSNativeSettings_t531545985::get_offset_of_AppleId_19(),
	IOSNativeSettings_t531545985::get_offset_of_ToolbarIndex_20(),
	IOSNativeSettings_t531545985::get_offset_of_ExpandMoreActionsMenu_21(),
	IOSNativeSettings_t531545985::get_offset_of_ExpandModulesSettings_22(),
	IOSNativeSettings_t531545985::get_offset_of_InAppsEditorTesting_23(),
	IOSNativeSettings_t531545985::get_offset_of_CheckInternetBeforeLoadRequest_24(),
	IOSNativeSettings_t531545985::get_offset_of_PromotedPurchaseSupport_25(),
	IOSNativeSettings_t531545985::get_offset_of_TransactionsHandlingMode_26(),
	IOSNativeSettings_t531545985::get_offset_of_DefaultStoreProductsView_27(),
	IOSNativeSettings_t531545985::get_offset_of_InAppProducts_28(),
	IOSNativeSettings_t531545985::get_offset_of_ShowStoreKitProducts_29(),
	IOSNativeSettings_t531545985::get_offset_of_Leaderboards_30(),
	IOSNativeSettings_t531545985::get_offset_of_Achievements_31(),
	IOSNativeSettings_t531545985::get_offset_of_UseGCRequestCaching_32(),
	IOSNativeSettings_t531545985::get_offset_of_UsePPForAchievements_33(),
	IOSNativeSettings_t531545985::get_offset_of_AutoLoadUsersSmallImages_34(),
	IOSNativeSettings_t531545985::get_offset_of_AutoLoadUsersBigImages_35(),
	IOSNativeSettings_t531545985::get_offset_of_ShowLeaderboards_36(),
	IOSNativeSettings_t531545985::get_offset_of_ShowAchievementsParams_37(),
	IOSNativeSettings_t531545985::get_offset_of_AdEditorTesting_38(),
	IOSNativeSettings_t531545985::get_offset_of_EditorFillRateIndex_39(),
	IOSNativeSettings_t531545985::get_offset_of_EditorFillRate_40(),
	IOSNativeSettings_t531545985::get_offset_of_MaxImageLoadSize_41(),
	IOSNativeSettings_t531545985::get_offset_of_JPegCompressionRate_42(),
	IOSNativeSettings_t531545985::get_offset_of_GalleryImageFormat_43(),
	IOSNativeSettings_t531545985::get_offset_of_RPK_iPadViewType_44(),
	IOSNativeSettings_t531545985::get_offset_of_CameraUsageDescription_45(),
	IOSNativeSettings_t531545985::get_offset_of_PhotoLibraryUsageDescription_46(),
	IOSNativeSettings_t531545985::get_offset_of_AppleMusicUsageDescription_47(),
	IOSNativeSettings_t531545985::get_offset_of_ContactsUsageDescription_48(),
	IOSNativeSettings_t531545985::get_offset_of_UrlTypes_49(),
	IOSNativeSettings_t531545985::get_offset_of_ApplicationQueriesSchemes_50(),
	IOSNativeSettings_t531545985::get_offset_of_ForceTouchMenu_51(),
	IOSNativeSettings_t531545985::get_offset_of_DisablePluginLogs_52(),
	IOSNativeSettings_t531545985::get_offset_of_SoomlaDownloadLink_53(),
	IOSNativeSettings_t531545985::get_offset_of_SoomlaDocsLink_54(),
	IOSNativeSettings_t531545985::get_offset_of_SoomlaGameKey_55(),
	IOSNativeSettings_t531545985::get_offset_of_SoomlaEnvKey_56(),
	IOSNativeSettings_t531545985::get_offset_of_OneSignalEnabled_57(),
	IOSNativeSettings_t531545985::get_offset_of_OneSignalDocsLink_58(),
	0,
	0,
	IOSNativeSettings_t531545985_StaticFields::get_offset_of_instance_61(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2373 = { sizeof (AppController_t1810425167), -1, sizeof(AppController_t1810425167_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2373[7] = 
{
	AppController_t1810425167_StaticFields::get_offset_of_OnApplicationDidEnterBackground_4(),
	AppController_t1810425167_StaticFields::get_offset_of_OnApplicationDidBecomeActive_5(),
	AppController_t1810425167_StaticFields::get_offset_of_OnApplicationDidReceiveMemoryWarning_6(),
	AppController_t1810425167_StaticFields::get_offset_of_OnApplicationWillResignActive_7(),
	AppController_t1810425167_StaticFields::get_offset_of_OnApplicationWillTerminate_8(),
	AppController_t1810425167_StaticFields::get_offset_of_OnOpenURL_9(),
	AppController_t1810425167_StaticFields::get_offset_of_OnContinueUserActivity_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2374 = { sizeof (GameCenter_RTM_t1714402562), -1, sizeof(GameCenter_RTM_t1714402562_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2374[10] = 
{
	GameCenter_RTM_t1714402562::get_offset_of__CurrentMatch_4(),
	GameCenter_RTM_t1714402562::get_offset_of__NearbyPlayers_5(),
	GameCenter_RTM_t1714402562_StaticFields::get_offset_of_ActionMatchStarted_6(),
	GameCenter_RTM_t1714402562_StaticFields::get_offset_of_ActionMatchFailed_7(),
	GameCenter_RTM_t1714402562_StaticFields::get_offset_of_ActionNearbyPlayerStateUpdated_8(),
	GameCenter_RTM_t1714402562_StaticFields::get_offset_of_ActionActivityResultReceived_9(),
	GameCenter_RTM_t1714402562_StaticFields::get_offset_of_ActionDataSendError_10(),
	GameCenter_RTM_t1714402562_StaticFields::get_offset_of_ActionDataReceived_11(),
	GameCenter_RTM_t1714402562_StaticFields::get_offset_of_ActionPlayerStateChanged_12(),
	GameCenter_RTM_t1714402562_StaticFields::get_offset_of_ActionDiconnectedPlayerReinvited_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2375 = { sizeof (GameCenter_TBM_t2861413648), -1, sizeof(GameCenter_TBM_t2861413648_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2375[14] = 
{
	GameCenter_TBM_t2861413648_StaticFields::get_offset_of_ActionMatchInfoLoaded_4(),
	GameCenter_TBM_t2861413648_StaticFields::get_offset_of_ActionMatchesInfoLoaded_5(),
	GameCenter_TBM_t2861413648_StaticFields::get_offset_of_ActionMatchDataUpdated_6(),
	GameCenter_TBM_t2861413648_StaticFields::get_offset_of_ActionMatchFound_7(),
	GameCenter_TBM_t2861413648_StaticFields::get_offset_of_ActionMatchQuit_8(),
	GameCenter_TBM_t2861413648_StaticFields::get_offset_of_ActionTrunEnded_9(),
	GameCenter_TBM_t2861413648_StaticFields::get_offset_of_ActionMacthEnded_10(),
	GameCenter_TBM_t2861413648_StaticFields::get_offset_of_ActionRematched_11(),
	GameCenter_TBM_t2861413648_StaticFields::get_offset_of_ActionMatchRemoved_12(),
	GameCenter_TBM_t2861413648_StaticFields::get_offset_of_ActionMatchInvitationAccepted_13(),
	GameCenter_TBM_t2861413648_StaticFields::get_offset_of_ActionMatchInvitationDeclined_14(),
	GameCenter_TBM_t2861413648_StaticFields::get_offset_of_ActionPlayerQuitForMatch_15(),
	GameCenter_TBM_t2861413648_StaticFields::get_offset_of_ActionTrunReceived_16(),
	GameCenter_TBM_t2861413648::get_offset_of__Matches_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2376 = { sizeof (GameCenterInvitations_t630122588), -1, sizeof(GameCenterInvitations_t630122588_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2376[3] = 
{
	GameCenterInvitations_t630122588_StaticFields::get_offset_of_ActionInviteeResponse_4(),
	GameCenterInvitations_t630122588_StaticFields::get_offset_of_ActionPlayerAcceptedInvitation_5(),
	GameCenterInvitations_t630122588_StaticFields::get_offset_of_ActionPlayerRequestedMatchWithRecipients_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2377 = { sizeof (GameCenterManager_t3595206143), -1, sizeof(GameCenterManager_t3595206143_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2377[20] = 
{
	GameCenterManager_t3595206143_StaticFields::get_offset_of_OnAuthFinished_2(),
	GameCenterManager_t3595206143_StaticFields::get_offset_of_OnScoreSubmitted_3(),
	GameCenterManager_t3595206143_StaticFields::get_offset_of_OnScoresListLoaded_4(),
	GameCenterManager_t3595206143_StaticFields::get_offset_of_OnLeadrboardInfoLoaded_5(),
	GameCenterManager_t3595206143_StaticFields::get_offset_of_OnLeaderboardSetsInfoLoaded_6(),
	GameCenterManager_t3595206143_StaticFields::get_offset_of_OnAchievementsReset_7(),
	GameCenterManager_t3595206143_StaticFields::get_offset_of_OnAchievementsLoaded_8(),
	GameCenterManager_t3595206143_StaticFields::get_offset_of_OnAchievementsProgress_9(),
	GameCenterManager_t3595206143_StaticFields::get_offset_of_OnGameCenterViewDismissed_10(),
	GameCenterManager_t3595206143_StaticFields::get_offset_of_OnFriendsListLoaded_11(),
	GameCenterManager_t3595206143_StaticFields::get_offset_of_OnUserInfoLoaded_12(),
	GameCenterManager_t3595206143_StaticFields::get_offset_of_OnPlayerSignatureRetrieveResult_13(),
	GameCenterManager_t3595206143_StaticFields::get_offset_of__IsInitialized_14(),
	GameCenterManager_t3595206143_StaticFields::get_offset_of__IsAchievementsInfoLoaded_15(),
	GameCenterManager_t3595206143_StaticFields::get_offset_of__players_16(),
	GameCenterManager_t3595206143_StaticFields::get_offset_of__friendsList_17(),
	GameCenterManager_t3595206143_StaticFields::get_offset_of__LeaderboardSets_18(),
	GameCenterManager_t3595206143_StaticFields::get_offset_of__FriendRequests_19(),
	GameCenterManager_t3595206143_StaticFields::get_offset_of__player_20(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2378 = { sizeof (U3CLoadLeaderboardInfoLocalU3Ec__Iterator0_t812827298), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2378[6] = 
{
	U3CLoadLeaderboardInfoLocalU3Ec__Iterator0_t812827298::get_offset_of_U3CrequestIdU3E__0_0(),
	U3CLoadLeaderboardInfoLocalU3Ec__Iterator0_t812827298::get_offset_of_leaderboardId_1(),
	U3CLoadLeaderboardInfoLocalU3Ec__Iterator0_t812827298::get_offset_of_U3CleaderboardU3E__0_2(),
	U3CLoadLeaderboardInfoLocalU3Ec__Iterator0_t812827298::get_offset_of_U24current_3(),
	U3CLoadLeaderboardInfoLocalU3Ec__Iterator0_t812827298::get_offset_of_U24disposing_4(),
	U3CLoadLeaderboardInfoLocalU3Ec__Iterator0_t812827298::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2379 = { sizeof (ISN_CacheManager_t4161853135), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2379[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2380 = { sizeof (ISN_GameSaves_t3595065519), -1, sizeof(ISN_GameSaves_t3595065519_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2380[5] = 
{
	ISN_GameSaves_t3595065519_StaticFields::get_offset_of_ActionSaveRemoved_4(),
	ISN_GameSaves_t3595065519_StaticFields::get_offset_of_ActionGameSaved_5(),
	ISN_GameSaves_t3595065519_StaticFields::get_offset_of_ActionSavesFetched_6(),
	ISN_GameSaves_t3595065519_StaticFields::get_offset_of_ActionSavesResolved_7(),
	ISN_GameSaves_t3595065519_StaticFields::get_offset_of__CachedGameSaves_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2381 = { sizeof (GK_CollectionType_t4065955501)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2381[3] = 
{
	GK_CollectionType_t4065955501::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2382 = { sizeof (GK_InviteRecipientResponse_t1393324106)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2382[7] = 
{
	GK_InviteRecipientResponse_t1393324106::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2383 = { sizeof (GK_MatchType_t164054226)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2383[3] = 
{
	GK_MatchType_t164054226::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2384 = { sizeof (GK_PhotoSize_t1682985923)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2384[3] = 
{
	GK_PhotoSize_t1682985923::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2385 = { sizeof (GK_TimeSpan_t2679980730)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2385[4] = 
{
	GK_TimeSpan_t2679980730::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2386 = { sizeof (GK_FriendRequest_t2497375054), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2386[3] = 
{
	GK_FriendRequest_t2497375054::get_offset_of__Id_0(),
	GK_FriendRequest_t2497375054::get_offset_of__PlayersIds_1(),
	GK_FriendRequest_t2497375054::get_offset_of__Emails_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2387 = { sizeof (GK_AchievementProgressResult_t451478438), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2387[1] = 
{
	GK_AchievementProgressResult_t451478438::get_offset_of__tpl_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2388 = { sizeof (GK_LeaderboardResult_t1457562782), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2388[1] = 
{
	GK_LeaderboardResult_t1457562782::get_offset_of__Leaderboard_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2389 = { sizeof (GK_PlayerSignatureResult_t1029544157), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2389[4] = 
{
	GK_PlayerSignatureResult_t1029544157::get_offset_of__PublicKeyUrl_1(),
	GK_PlayerSignatureResult_t1029544157::get_offset_of__Signature_2(),
	GK_PlayerSignatureResult_t1029544157::get_offset_of__Salt_3(),
	GK_PlayerSignatureResult_t1029544157::get_offset_of__Timestamp_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2390 = { sizeof (GK_UserInfoLoadResult_t3587554467), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2390[2] = 
{
	GK_UserInfoLoadResult_t3587554467::get_offset_of__playerId_1(),
	GK_UserInfoLoadResult_t3587554467::get_offset_of__tpl_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2391 = { sizeof (GK_UserPhotoLoadResult_t3918760827), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2391[2] = 
{
	GK_UserPhotoLoadResult_t3918760827::get_offset_of__Photo_1(),
	GK_UserPhotoLoadResult_t3918760827::get_offset_of__Size_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2392 = { sizeof (ISN_LoadSetLeaderboardsInfoResult_t1754621461), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2392[1] = 
{
	ISN_LoadSetLeaderboardsInfoResult_t1754621461::get_offset_of__LeaderBoardsSet_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2393 = { sizeof (GK_AchievementTemplate_t3624697896), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2393[6] = 
{
	GK_AchievementTemplate_t3624697896::get_offset_of_IsOpen_0(),
	GK_AchievementTemplate_t3624697896::get_offset_of_Id_1(),
	GK_AchievementTemplate_t3624697896::get_offset_of_Title_2(),
	GK_AchievementTemplate_t3624697896::get_offset_of_Description_3(),
	GK_AchievementTemplate_t3624697896::get_offset_of__progress_4(),
	GK_AchievementTemplate_t3624697896::get_offset_of_Texture_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2394 = { sizeof (GK_Invite_t1194874416), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2394[4] = 
{
	GK_Invite_t1194874416::get_offset_of__Id_0(),
	GK_Invite_t1194874416::get_offset_of__Sender_1(),
	GK_Invite_t1194874416::get_offset_of__PlayerGroup_2(),
	GK_Invite_t1194874416::get_offset_of__PlayerAttributes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2395 = { sizeof (GK_Leaderboard_t591378496), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2395[7] = 
{
	GK_Leaderboard_t591378496::get_offset_of_IsOpen_0(),
	GK_Leaderboard_t591378496::get_offset_of__CurrentPlayerScoreLoaded_1(),
	GK_Leaderboard_t591378496::get_offset_of_SocsialCollection_2(),
	GK_Leaderboard_t591378496::get_offset_of_GlobalCollection_3(),
	GK_Leaderboard_t591378496::get_offset_of_CurrentPlayerScore_4(),
	GK_Leaderboard_t591378496::get_offset_of_ScoreUpdateListners_5(),
	GK_Leaderboard_t591378496::get_offset_of__info_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2396 = { sizeof (GK_LeaderBoardInfo_t323632070), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2396[5] = 
{
	GK_LeaderBoardInfo_t323632070::get_offset_of_Title_0(),
	GK_LeaderBoardInfo_t323632070::get_offset_of_Description_1(),
	GK_LeaderBoardInfo_t323632070::get_offset_of_Identifier_2(),
	GK_LeaderBoardInfo_t323632070::get_offset_of_Texture_3(),
	GK_LeaderBoardInfo_t323632070::get_offset_of_MaxRange_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2397 = { sizeof (GK_LeaderboardSet_t3182444842), -1, sizeof(GK_LeaderboardSet_t3182444842_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2397[6] = 
{
	GK_LeaderboardSet_t3182444842::get_offset_of_Title_0(),
	GK_LeaderboardSet_t3182444842::get_offset_of_Identifier_1(),
	GK_LeaderboardSet_t3182444842::get_offset_of_GroupIdentifier_2(),
	GK_LeaderboardSet_t3182444842::get_offset_of__BoardsInfo_3(),
	GK_LeaderboardSet_t3182444842::get_offset_of_OnLoaderboardsInfoLoaded_4(),
	GK_LeaderboardSet_t3182444842_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2398 = { sizeof (GK_LocalPlayerScoreUpdateListener_t617724180), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2398[5] = 
{
	GK_LocalPlayerScoreUpdateListener_t617724180::get_offset_of__RequestId_0(),
	GK_LocalPlayerScoreUpdateListener_t617724180::get_offset_of__IsInternal_1(),
	GK_LocalPlayerScoreUpdateListener_t617724180::get_offset_of__leaderboardId_2(),
	GK_LocalPlayerScoreUpdateListener_t617724180::get_offset_of__ErrorData_3(),
	GK_LocalPlayerScoreUpdateListener_t617724180::get_offset_of_Scores_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2399 = { sizeof (GK_Player_t3461352791), -1, sizeof(GK_Player_t3461352791_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2399[8] = 
{
	GK_Player_t3461352791::get_offset_of__PlayerId_0(),
	GK_Player_t3461352791::get_offset_of__DisplayName_1(),
	GK_Player_t3461352791::get_offset_of__Alias_2(),
	GK_Player_t3461352791::get_offset_of__SmallPhoto_3(),
	GK_Player_t3461352791::get_offset_of__BigPhoto_4(),
	GK_Player_t3461352791::get_offset_of_OnPlayerPhotoLoaded_5(),
	GK_Player_t3461352791_StaticFields::get_offset_of_LocalPhotosCache_6(),
	GK_Player_t3461352791_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_7(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
