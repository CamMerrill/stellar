﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.String
struct String_t;
// Mono.Xml.Schema.XsdKeyEntry
struct XsdKeyEntry_t693496666;
// Mono.Xml.Schema.XsdIdentityField
struct XsdIdentityField_t1964115728;
// Mono.Xml.Schema.XsdAnySimpleType
struct XsdAnySimpleType_t1257864485;
// Mono.Xml.Schema.XsdIdentityPath
struct XsdIdentityPath_t991900844;
// Mono.Xml.Schema.XsdKeyEntryFieldCollection
struct XsdKeyEntryFieldCollection_t3698183622;
// Mono.Xml.Schema.XsdKeyTable
struct XsdKeyTable_t2156891743;
// Mono.Xml.Schema.XsdValidationState
struct XsdValidationState_t376578997;
// System.Collections.Stack
struct Stack_t2329662280;
// System.Collections.ArrayList
struct ArrayList_t2718874744;
// Mono.Xml.Schema.XsdIdentitySelector
struct XsdIdentitySelector_t574258590;
// System.Xml.Schema.XmlSchemaIdentityConstraint
struct XmlSchemaIdentityConstraint_t297318432;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t2760654312;
// Mono.Xml.Schema.XsdKeyEntryCollection
struct XsdKeyEntryCollection_t3090959213;
// System.Xml.XmlReaderBinarySupport
struct XmlReaderBinarySupport_t1809665003;
// System.Xml.XmlReaderSettings
struct XmlReaderSettings_t2186285234;
// Mono.Xml.Schema.XsdInvalidValidationState
struct XsdInvalidValidationState_t3749995458;
// Mono.Xml.Schema.XsdParticleStateManager
struct XsdParticleStateManager_t726654767;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Uri/UriScheme[]
struct UriSchemeU5BU5D_t2082808316;
// System.UriParser
struct UriParser_t3890150400;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// Mono.Xml.Schema.XsdIdentityStep[]
struct XsdIdentityStepU5BU5D_t2964233348;
// Mono.Xml.Schema.XsdIdentityPath[]
struct XsdIdentityPathU5BU5D_t2466178853;
// UnityEngine.Analytics.AnalyticsSessionInfo/SessionStateChanged
struct SessionStateChanged_t3163629820;
// UnityEngine.RemoteSettings/UpdatedEventHandler
struct UpdatedEventHandler_t1027848393;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// Mono.Xml.Schema.XsdIdentityField[]
struct XsdIdentityFieldU5BU5D_t881076913;
// System.Char[]
struct CharU5BU5D_t3528271667;
// Mono.Xml.Schema.XmlSchemaUri
struct XmlSchemaUri_t3948303260;
// System.Void
struct Void_t1185182177;
// System.Xml.Schema.XmlSchemaAny
struct XmlSchemaAny_t1119175207;
// System.Xml.Schema.XmlSchemaAll
struct XmlSchemaAll_t1118454309;
// System.Xml.Schema.XmlSchemaChoice
struct XmlSchemaChoice_t959520675;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t463507806;
// System.Xml.Schema.XmlSchemaSequence
struct XmlSchemaSequence_t2018345177;
// System.Xml.Schema.XmlSchemaElement
struct XmlSchemaElement_t427880856;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Text.StringBuilder
struct StringBuilder_t;
// Mono.Xml.Schema.XsdString
struct XsdString_t3049094358;
// Mono.Xml.Schema.XsdNormalizedString
struct XsdNormalizedString_t3260789355;
// Mono.Xml.Schema.XsdToken
struct XsdToken_t1239036978;
// Mono.Xml.Schema.XsdLanguage
struct XsdLanguage_t1876291273;
// Mono.Xml.Schema.XsdNMToken
struct XsdNMToken_t834691671;
// Mono.Xml.Schema.XsdNMTokens
struct XsdNMTokens_t4246953255;
// Mono.Xml.Schema.XsdName
struct XsdName_t2755146808;
// Mono.Xml.Schema.XsdNCName
struct XsdNCName_t3943159043;
// Mono.Xml.Schema.XsdID
struct XsdID_t34704195;
// Mono.Xml.Schema.XsdIDRef
struct XsdIDRef_t2913612829;
// Mono.Xml.Schema.XsdIDRefs
struct XsdIDRefs_t16099206;
// Mono.Xml.Schema.XsdEntity
struct XsdEntity_t3956505874;
// Mono.Xml.Schema.XsdEntities
struct XsdEntities_t1477210398;
// Mono.Xml.Schema.XsdNotation
struct XsdNotation_t2827634056;
// Mono.Xml.Schema.XsdDecimal
struct XsdDecimal_t1288601093;
// Mono.Xml.Schema.XsdInteger
struct XsdInteger_t2044766898;
// Mono.Xml.Schema.XsdLong
struct XsdLong_t1324632828;
// Mono.Xml.Schema.XsdInt
struct XsdInt_t33917785;
// Mono.Xml.Schema.XsdShort
struct XsdShort_t3489811876;
// Mono.Xml.Schema.XsdByte
struct XsdByte_t2221093920;
// Mono.Xml.Schema.XsdNonNegativeInteger
struct XsdNonNegativeInteger_t308064234;
// Mono.Xml.Schema.XsdPositiveInteger
struct XsdPositiveInteger_t1704031413;
// Mono.Xml.Schema.XsdUnsignedLong
struct XsdUnsignedLong_t1409593434;
// Mono.Xml.Schema.XsdUnsignedInt
struct XsdUnsignedInt_t72105793;
// Mono.Xml.Schema.XsdUnsignedShort
struct XsdUnsignedShort_t3654069686;
// Mono.Xml.Schema.XsdUnsignedByte
struct XsdUnsignedByte_t2304219558;
// Mono.Xml.Schema.XsdNonPositiveInteger
struct XsdNonPositiveInteger_t1029055398;
// Mono.Xml.Schema.XsdNegativeInteger
struct XsdNegativeInteger_t2178753546;
// Mono.Xml.Schema.XsdFloat
struct XsdFloat_t3181928905;
// Mono.Xml.Schema.XsdDouble
struct XsdDouble_t3324344982;
// Mono.Xml.Schema.XsdBase64Binary
struct XsdBase64Binary_t3360383190;
// Mono.Xml.Schema.XsdBoolean
struct XsdBoolean_t380164876;
// Mono.Xml.Schema.XsdAnyURI
struct XsdAnyURI_t2755748070;
// Mono.Xml.Schema.XsdDuration
struct XsdDuration_t1555973170;
// Mono.Xml.Schema.XsdDateTime
struct XsdDateTime_t2563698975;
// Mono.Xml.Schema.XsdDate
struct XsdDate_t1417753656;
// Mono.Xml.Schema.XsdTime
struct XsdTime_t3558487088;
// Mono.Xml.Schema.XsdHexBinary
struct XsdHexBinary_t882812470;
// Mono.Xml.Schema.XsdQName
struct XsdQName_t2385631467;
// Mono.Xml.Schema.XsdGYearMonth
struct XsdGYearMonth_t3399073121;
// Mono.Xml.Schema.XsdGMonthDay
struct XsdGMonthDay_t2605134399;
// Mono.Xml.Schema.XsdGYear
struct XsdGYear_t3316212116;
// Mono.Xml.Schema.XsdGMonth
struct XsdGMonth_t3913018815;
// Mono.Xml.Schema.XsdGDay
struct XsdGDay_t293490745;
// Mono.Xml.Schema.XdtAnyAtomicType
struct XdtAnyAtomicType_t269366253;
// Mono.Xml.Schema.XdtUntypedAtomic
struct XdtUntypedAtomic_t1388131523;
// Mono.Xml.Schema.XdtDayTimeDuration
struct XdtDayTimeDuration_t268779858;
// Mono.Xml.Schema.XdtYearMonthDuration
struct XdtYearMonthDuration_t1503718519;
// System.Xml.Schema.XmlSchemaObject
struct XmlSchemaObject_t1315720168;
// System.Collections.Specialized.StringCollection
struct StringCollection_t167406615;
// System.Xml.Schema.XmlSchemaAttribute[]
struct XmlSchemaAttributeU5BU5D_t346244693;
// System.Xml.XmlReader
struct XmlReader_t3121518892;
// System.Xml.XmlResolver
struct XmlResolver_t626023767;
// Mono.Xml.IHasXmlSchemaInfo
struct IHasXmlSchemaInfo_t74872415;
// System.Xml.IXmlLineInfo
struct IXmlLineInfo_t2353988607;
// System.Xml.Schema.XmlSchemaSet
struct XmlSchemaSet_t266093086;
// Mono.Xml.Schema.XsdIDManager
struct XsdIDManager_t1008806102;
// System.Xml.Schema.ValidationEventHandler
struct ValidationEventHandler_t791314227;
// Mono.Xml.Schema.XsdValidationContext
struct XsdValidationContext_t1104170526;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// UnityEngine.Canvas/WillRenderCanvases
struct WillRenderCanvases_t3309123499;
// System.Byte[]
struct ByteU5BU5D_t4116647657;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T692745546_H
#define U3CMODULEU3E_T692745546_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745546 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745546_H
#ifndef U3CMODULEU3E_T692745545_H
#define U3CMODULEU3E_T692745545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745545 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745545_H
#ifndef U3CMODULEU3E_T692745544_H
#define U3CMODULEU3E_T692745544_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745544 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745544_H
#ifndef U3CMODULEU3E_T692745543_H
#define U3CMODULEU3E_T692745543_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745543 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745543_H
#ifndef XSDIDENTITYSTEP_T1480907129_H
#define XSDIDENTITYSTEP_T1480907129_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdIdentityStep
struct  XsdIdentityStep_t1480907129  : public RuntimeObject
{
public:
	// System.Boolean Mono.Xml.Schema.XsdIdentityStep::IsCurrent
	bool ___IsCurrent_0;
	// System.Boolean Mono.Xml.Schema.XsdIdentityStep::IsAttribute
	bool ___IsAttribute_1;
	// System.Boolean Mono.Xml.Schema.XsdIdentityStep::IsAnyName
	bool ___IsAnyName_2;
	// System.String Mono.Xml.Schema.XsdIdentityStep::NsName
	String_t* ___NsName_3;
	// System.String Mono.Xml.Schema.XsdIdentityStep::Name
	String_t* ___Name_4;
	// System.String Mono.Xml.Schema.XsdIdentityStep::Namespace
	String_t* ___Namespace_5;

public:
	inline static int32_t get_offset_of_IsCurrent_0() { return static_cast<int32_t>(offsetof(XsdIdentityStep_t1480907129, ___IsCurrent_0)); }
	inline bool get_IsCurrent_0() const { return ___IsCurrent_0; }
	inline bool* get_address_of_IsCurrent_0() { return &___IsCurrent_0; }
	inline void set_IsCurrent_0(bool value)
	{
		___IsCurrent_0 = value;
	}

	inline static int32_t get_offset_of_IsAttribute_1() { return static_cast<int32_t>(offsetof(XsdIdentityStep_t1480907129, ___IsAttribute_1)); }
	inline bool get_IsAttribute_1() const { return ___IsAttribute_1; }
	inline bool* get_address_of_IsAttribute_1() { return &___IsAttribute_1; }
	inline void set_IsAttribute_1(bool value)
	{
		___IsAttribute_1 = value;
	}

	inline static int32_t get_offset_of_IsAnyName_2() { return static_cast<int32_t>(offsetof(XsdIdentityStep_t1480907129, ___IsAnyName_2)); }
	inline bool get_IsAnyName_2() const { return ___IsAnyName_2; }
	inline bool* get_address_of_IsAnyName_2() { return &___IsAnyName_2; }
	inline void set_IsAnyName_2(bool value)
	{
		___IsAnyName_2 = value;
	}

	inline static int32_t get_offset_of_NsName_3() { return static_cast<int32_t>(offsetof(XsdIdentityStep_t1480907129, ___NsName_3)); }
	inline String_t* get_NsName_3() const { return ___NsName_3; }
	inline String_t** get_address_of_NsName_3() { return &___NsName_3; }
	inline void set_NsName_3(String_t* value)
	{
		___NsName_3 = value;
		Il2CppCodeGenWriteBarrier((&___NsName_3), value);
	}

	inline static int32_t get_offset_of_Name_4() { return static_cast<int32_t>(offsetof(XsdIdentityStep_t1480907129, ___Name_4)); }
	inline String_t* get_Name_4() const { return ___Name_4; }
	inline String_t** get_address_of_Name_4() { return &___Name_4; }
	inline void set_Name_4(String_t* value)
	{
		___Name_4 = value;
		Il2CppCodeGenWriteBarrier((&___Name_4), value);
	}

	inline static int32_t get_offset_of_Namespace_5() { return static_cast<int32_t>(offsetof(XsdIdentityStep_t1480907129, ___Namespace_5)); }
	inline String_t* get_Namespace_5() const { return ___Namespace_5; }
	inline String_t** get_address_of_Namespace_5() { return &___Namespace_5; }
	inline void set_Namespace_5(String_t* value)
	{
		___Namespace_5 = value;
		Il2CppCodeGenWriteBarrier((&___Namespace_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDIDENTITYSTEP_T1480907129_H
#ifndef XSDKEYENTRYFIELD_T3552275292_H
#define XSDKEYENTRYFIELD_T3552275292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdKeyEntryField
struct  XsdKeyEntryField_t3552275292  : public RuntimeObject
{
public:
	// Mono.Xml.Schema.XsdKeyEntry Mono.Xml.Schema.XsdKeyEntryField::entry
	XsdKeyEntry_t693496666 * ___entry_0;
	// Mono.Xml.Schema.XsdIdentityField Mono.Xml.Schema.XsdKeyEntryField::field
	XsdIdentityField_t1964115728 * ___field_1;
	// System.Boolean Mono.Xml.Schema.XsdKeyEntryField::FieldFound
	bool ___FieldFound_2;
	// System.Int32 Mono.Xml.Schema.XsdKeyEntryField::FieldLineNumber
	int32_t ___FieldLineNumber_3;
	// System.Int32 Mono.Xml.Schema.XsdKeyEntryField::FieldLinePosition
	int32_t ___FieldLinePosition_4;
	// System.Boolean Mono.Xml.Schema.XsdKeyEntryField::FieldHasLineInfo
	bool ___FieldHasLineInfo_5;
	// Mono.Xml.Schema.XsdAnySimpleType Mono.Xml.Schema.XsdKeyEntryField::FieldType
	XsdAnySimpleType_t1257864485 * ___FieldType_6;
	// System.Object Mono.Xml.Schema.XsdKeyEntryField::Identity
	RuntimeObject * ___Identity_7;
	// System.Boolean Mono.Xml.Schema.XsdKeyEntryField::IsXsiNil
	bool ___IsXsiNil_8;
	// System.Int32 Mono.Xml.Schema.XsdKeyEntryField::FieldFoundDepth
	int32_t ___FieldFoundDepth_9;
	// Mono.Xml.Schema.XsdIdentityPath Mono.Xml.Schema.XsdKeyEntryField::FieldFoundPath
	XsdIdentityPath_t991900844 * ___FieldFoundPath_10;
	// System.Boolean Mono.Xml.Schema.XsdKeyEntryField::Consuming
	bool ___Consuming_11;
	// System.Boolean Mono.Xml.Schema.XsdKeyEntryField::Consumed
	bool ___Consumed_12;

public:
	inline static int32_t get_offset_of_entry_0() { return static_cast<int32_t>(offsetof(XsdKeyEntryField_t3552275292, ___entry_0)); }
	inline XsdKeyEntry_t693496666 * get_entry_0() const { return ___entry_0; }
	inline XsdKeyEntry_t693496666 ** get_address_of_entry_0() { return &___entry_0; }
	inline void set_entry_0(XsdKeyEntry_t693496666 * value)
	{
		___entry_0 = value;
		Il2CppCodeGenWriteBarrier((&___entry_0), value);
	}

	inline static int32_t get_offset_of_field_1() { return static_cast<int32_t>(offsetof(XsdKeyEntryField_t3552275292, ___field_1)); }
	inline XsdIdentityField_t1964115728 * get_field_1() const { return ___field_1; }
	inline XsdIdentityField_t1964115728 ** get_address_of_field_1() { return &___field_1; }
	inline void set_field_1(XsdIdentityField_t1964115728 * value)
	{
		___field_1 = value;
		Il2CppCodeGenWriteBarrier((&___field_1), value);
	}

	inline static int32_t get_offset_of_FieldFound_2() { return static_cast<int32_t>(offsetof(XsdKeyEntryField_t3552275292, ___FieldFound_2)); }
	inline bool get_FieldFound_2() const { return ___FieldFound_2; }
	inline bool* get_address_of_FieldFound_2() { return &___FieldFound_2; }
	inline void set_FieldFound_2(bool value)
	{
		___FieldFound_2 = value;
	}

	inline static int32_t get_offset_of_FieldLineNumber_3() { return static_cast<int32_t>(offsetof(XsdKeyEntryField_t3552275292, ___FieldLineNumber_3)); }
	inline int32_t get_FieldLineNumber_3() const { return ___FieldLineNumber_3; }
	inline int32_t* get_address_of_FieldLineNumber_3() { return &___FieldLineNumber_3; }
	inline void set_FieldLineNumber_3(int32_t value)
	{
		___FieldLineNumber_3 = value;
	}

	inline static int32_t get_offset_of_FieldLinePosition_4() { return static_cast<int32_t>(offsetof(XsdKeyEntryField_t3552275292, ___FieldLinePosition_4)); }
	inline int32_t get_FieldLinePosition_4() const { return ___FieldLinePosition_4; }
	inline int32_t* get_address_of_FieldLinePosition_4() { return &___FieldLinePosition_4; }
	inline void set_FieldLinePosition_4(int32_t value)
	{
		___FieldLinePosition_4 = value;
	}

	inline static int32_t get_offset_of_FieldHasLineInfo_5() { return static_cast<int32_t>(offsetof(XsdKeyEntryField_t3552275292, ___FieldHasLineInfo_5)); }
	inline bool get_FieldHasLineInfo_5() const { return ___FieldHasLineInfo_5; }
	inline bool* get_address_of_FieldHasLineInfo_5() { return &___FieldHasLineInfo_5; }
	inline void set_FieldHasLineInfo_5(bool value)
	{
		___FieldHasLineInfo_5 = value;
	}

	inline static int32_t get_offset_of_FieldType_6() { return static_cast<int32_t>(offsetof(XsdKeyEntryField_t3552275292, ___FieldType_6)); }
	inline XsdAnySimpleType_t1257864485 * get_FieldType_6() const { return ___FieldType_6; }
	inline XsdAnySimpleType_t1257864485 ** get_address_of_FieldType_6() { return &___FieldType_6; }
	inline void set_FieldType_6(XsdAnySimpleType_t1257864485 * value)
	{
		___FieldType_6 = value;
		Il2CppCodeGenWriteBarrier((&___FieldType_6), value);
	}

	inline static int32_t get_offset_of_Identity_7() { return static_cast<int32_t>(offsetof(XsdKeyEntryField_t3552275292, ___Identity_7)); }
	inline RuntimeObject * get_Identity_7() const { return ___Identity_7; }
	inline RuntimeObject ** get_address_of_Identity_7() { return &___Identity_7; }
	inline void set_Identity_7(RuntimeObject * value)
	{
		___Identity_7 = value;
		Il2CppCodeGenWriteBarrier((&___Identity_7), value);
	}

	inline static int32_t get_offset_of_IsXsiNil_8() { return static_cast<int32_t>(offsetof(XsdKeyEntryField_t3552275292, ___IsXsiNil_8)); }
	inline bool get_IsXsiNil_8() const { return ___IsXsiNil_8; }
	inline bool* get_address_of_IsXsiNil_8() { return &___IsXsiNil_8; }
	inline void set_IsXsiNil_8(bool value)
	{
		___IsXsiNil_8 = value;
	}

	inline static int32_t get_offset_of_FieldFoundDepth_9() { return static_cast<int32_t>(offsetof(XsdKeyEntryField_t3552275292, ___FieldFoundDepth_9)); }
	inline int32_t get_FieldFoundDepth_9() const { return ___FieldFoundDepth_9; }
	inline int32_t* get_address_of_FieldFoundDepth_9() { return &___FieldFoundDepth_9; }
	inline void set_FieldFoundDepth_9(int32_t value)
	{
		___FieldFoundDepth_9 = value;
	}

	inline static int32_t get_offset_of_FieldFoundPath_10() { return static_cast<int32_t>(offsetof(XsdKeyEntryField_t3552275292, ___FieldFoundPath_10)); }
	inline XsdIdentityPath_t991900844 * get_FieldFoundPath_10() const { return ___FieldFoundPath_10; }
	inline XsdIdentityPath_t991900844 ** get_address_of_FieldFoundPath_10() { return &___FieldFoundPath_10; }
	inline void set_FieldFoundPath_10(XsdIdentityPath_t991900844 * value)
	{
		___FieldFoundPath_10 = value;
		Il2CppCodeGenWriteBarrier((&___FieldFoundPath_10), value);
	}

	inline static int32_t get_offset_of_Consuming_11() { return static_cast<int32_t>(offsetof(XsdKeyEntryField_t3552275292, ___Consuming_11)); }
	inline bool get_Consuming_11() const { return ___Consuming_11; }
	inline bool* get_address_of_Consuming_11() { return &___Consuming_11; }
	inline void set_Consuming_11(bool value)
	{
		___Consuming_11 = value;
	}

	inline static int32_t get_offset_of_Consumed_12() { return static_cast<int32_t>(offsetof(XsdKeyEntryField_t3552275292, ___Consumed_12)); }
	inline bool get_Consumed_12() const { return ___Consumed_12; }
	inline bool* get_address_of_Consumed_12() { return &___Consumed_12; }
	inline void set_Consumed_12(bool value)
	{
		___Consumed_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDKEYENTRYFIELD_T3552275292_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef XSDKEYENTRY_T693496666_H
#define XSDKEYENTRY_T693496666_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdKeyEntry
struct  XsdKeyEntry_t693496666  : public RuntimeObject
{
public:
	// System.Int32 Mono.Xml.Schema.XsdKeyEntry::StartDepth
	int32_t ___StartDepth_0;
	// System.Int32 Mono.Xml.Schema.XsdKeyEntry::SelectorLineNumber
	int32_t ___SelectorLineNumber_1;
	// System.Int32 Mono.Xml.Schema.XsdKeyEntry::SelectorLinePosition
	int32_t ___SelectorLinePosition_2;
	// System.Boolean Mono.Xml.Schema.XsdKeyEntry::SelectorHasLineInfo
	bool ___SelectorHasLineInfo_3;
	// Mono.Xml.Schema.XsdKeyEntryFieldCollection Mono.Xml.Schema.XsdKeyEntry::KeyFields
	XsdKeyEntryFieldCollection_t3698183622 * ___KeyFields_4;
	// System.Boolean Mono.Xml.Schema.XsdKeyEntry::KeyRefFound
	bool ___KeyRefFound_5;
	// Mono.Xml.Schema.XsdKeyTable Mono.Xml.Schema.XsdKeyEntry::OwnerSequence
	XsdKeyTable_t2156891743 * ___OwnerSequence_6;
	// System.Boolean Mono.Xml.Schema.XsdKeyEntry::keyFound
	bool ___keyFound_7;

public:
	inline static int32_t get_offset_of_StartDepth_0() { return static_cast<int32_t>(offsetof(XsdKeyEntry_t693496666, ___StartDepth_0)); }
	inline int32_t get_StartDepth_0() const { return ___StartDepth_0; }
	inline int32_t* get_address_of_StartDepth_0() { return &___StartDepth_0; }
	inline void set_StartDepth_0(int32_t value)
	{
		___StartDepth_0 = value;
	}

	inline static int32_t get_offset_of_SelectorLineNumber_1() { return static_cast<int32_t>(offsetof(XsdKeyEntry_t693496666, ___SelectorLineNumber_1)); }
	inline int32_t get_SelectorLineNumber_1() const { return ___SelectorLineNumber_1; }
	inline int32_t* get_address_of_SelectorLineNumber_1() { return &___SelectorLineNumber_1; }
	inline void set_SelectorLineNumber_1(int32_t value)
	{
		___SelectorLineNumber_1 = value;
	}

	inline static int32_t get_offset_of_SelectorLinePosition_2() { return static_cast<int32_t>(offsetof(XsdKeyEntry_t693496666, ___SelectorLinePosition_2)); }
	inline int32_t get_SelectorLinePosition_2() const { return ___SelectorLinePosition_2; }
	inline int32_t* get_address_of_SelectorLinePosition_2() { return &___SelectorLinePosition_2; }
	inline void set_SelectorLinePosition_2(int32_t value)
	{
		___SelectorLinePosition_2 = value;
	}

	inline static int32_t get_offset_of_SelectorHasLineInfo_3() { return static_cast<int32_t>(offsetof(XsdKeyEntry_t693496666, ___SelectorHasLineInfo_3)); }
	inline bool get_SelectorHasLineInfo_3() const { return ___SelectorHasLineInfo_3; }
	inline bool* get_address_of_SelectorHasLineInfo_3() { return &___SelectorHasLineInfo_3; }
	inline void set_SelectorHasLineInfo_3(bool value)
	{
		___SelectorHasLineInfo_3 = value;
	}

	inline static int32_t get_offset_of_KeyFields_4() { return static_cast<int32_t>(offsetof(XsdKeyEntry_t693496666, ___KeyFields_4)); }
	inline XsdKeyEntryFieldCollection_t3698183622 * get_KeyFields_4() const { return ___KeyFields_4; }
	inline XsdKeyEntryFieldCollection_t3698183622 ** get_address_of_KeyFields_4() { return &___KeyFields_4; }
	inline void set_KeyFields_4(XsdKeyEntryFieldCollection_t3698183622 * value)
	{
		___KeyFields_4 = value;
		Il2CppCodeGenWriteBarrier((&___KeyFields_4), value);
	}

	inline static int32_t get_offset_of_KeyRefFound_5() { return static_cast<int32_t>(offsetof(XsdKeyEntry_t693496666, ___KeyRefFound_5)); }
	inline bool get_KeyRefFound_5() const { return ___KeyRefFound_5; }
	inline bool* get_address_of_KeyRefFound_5() { return &___KeyRefFound_5; }
	inline void set_KeyRefFound_5(bool value)
	{
		___KeyRefFound_5 = value;
	}

	inline static int32_t get_offset_of_OwnerSequence_6() { return static_cast<int32_t>(offsetof(XsdKeyEntry_t693496666, ___OwnerSequence_6)); }
	inline XsdKeyTable_t2156891743 * get_OwnerSequence_6() const { return ___OwnerSequence_6; }
	inline XsdKeyTable_t2156891743 ** get_address_of_OwnerSequence_6() { return &___OwnerSequence_6; }
	inline void set_OwnerSequence_6(XsdKeyTable_t2156891743 * value)
	{
		___OwnerSequence_6 = value;
		Il2CppCodeGenWriteBarrier((&___OwnerSequence_6), value);
	}

	inline static int32_t get_offset_of_keyFound_7() { return static_cast<int32_t>(offsetof(XsdKeyEntry_t693496666, ___keyFound_7)); }
	inline bool get_keyFound_7() const { return ___keyFound_7; }
	inline bool* get_address_of_keyFound_7() { return &___keyFound_7; }
	inline void set_keyFound_7(bool value)
	{
		___keyFound_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDKEYENTRY_T693496666_H
#ifndef XSDVALIDATIONCONTEXT_T1104170526_H
#define XSDVALIDATIONCONTEXT_T1104170526_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdValidationContext
struct  XsdValidationContext_t1104170526  : public RuntimeObject
{
public:
	// System.Object Mono.Xml.Schema.XsdValidationContext::xsi_type
	RuntimeObject * ___xsi_type_0;
	// Mono.Xml.Schema.XsdValidationState Mono.Xml.Schema.XsdValidationContext::State
	XsdValidationState_t376578997 * ___State_1;
	// System.Collections.Stack Mono.Xml.Schema.XsdValidationContext::element_stack
	Stack_t2329662280 * ___element_stack_2;

public:
	inline static int32_t get_offset_of_xsi_type_0() { return static_cast<int32_t>(offsetof(XsdValidationContext_t1104170526, ___xsi_type_0)); }
	inline RuntimeObject * get_xsi_type_0() const { return ___xsi_type_0; }
	inline RuntimeObject ** get_address_of_xsi_type_0() { return &___xsi_type_0; }
	inline void set_xsi_type_0(RuntimeObject * value)
	{
		___xsi_type_0 = value;
		Il2CppCodeGenWriteBarrier((&___xsi_type_0), value);
	}

	inline static int32_t get_offset_of_State_1() { return static_cast<int32_t>(offsetof(XsdValidationContext_t1104170526, ___State_1)); }
	inline XsdValidationState_t376578997 * get_State_1() const { return ___State_1; }
	inline XsdValidationState_t376578997 ** get_address_of_State_1() { return &___State_1; }
	inline void set_State_1(XsdValidationState_t376578997 * value)
	{
		___State_1 = value;
		Il2CppCodeGenWriteBarrier((&___State_1), value);
	}

	inline static int32_t get_offset_of_element_stack_2() { return static_cast<int32_t>(offsetof(XsdValidationContext_t1104170526, ___element_stack_2)); }
	inline Stack_t2329662280 * get_element_stack_2() const { return ___element_stack_2; }
	inline Stack_t2329662280 ** get_address_of_element_stack_2() { return &___element_stack_2; }
	inline void set_element_stack_2(Stack_t2329662280 * value)
	{
		___element_stack_2 = value;
		Il2CppCodeGenWriteBarrier((&___element_stack_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDVALIDATIONCONTEXT_T1104170526_H
#ifndef COLLECTIONBASE_T2727926298_H
#define COLLECTIONBASE_T2727926298_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.CollectionBase
struct  CollectionBase_t2727926298  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.Collections.CollectionBase::list
	ArrayList_t2718874744 * ___list_0;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(CollectionBase_t2727926298, ___list_0)); }
	inline ArrayList_t2718874744 * get_list_0() const { return ___list_0; }
	inline ArrayList_t2718874744 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(ArrayList_t2718874744 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTIONBASE_T2727926298_H
#ifndef XSDKEYTABLE_T2156891743_H
#define XSDKEYTABLE_T2156891743_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdKeyTable
struct  XsdKeyTable_t2156891743  : public RuntimeObject
{
public:
	// System.Boolean Mono.Xml.Schema.XsdKeyTable::alwaysTrue
	bool ___alwaysTrue_0;
	// Mono.Xml.Schema.XsdIdentitySelector Mono.Xml.Schema.XsdKeyTable::selector
	XsdIdentitySelector_t574258590 * ___selector_1;
	// System.Xml.Schema.XmlSchemaIdentityConstraint Mono.Xml.Schema.XsdKeyTable::source
	XmlSchemaIdentityConstraint_t297318432 * ___source_2;
	// System.Xml.XmlQualifiedName Mono.Xml.Schema.XsdKeyTable::qname
	XmlQualifiedName_t2760654312 * ___qname_3;
	// System.Xml.XmlQualifiedName Mono.Xml.Schema.XsdKeyTable::refKeyName
	XmlQualifiedName_t2760654312 * ___refKeyName_4;
	// Mono.Xml.Schema.XsdKeyEntryCollection Mono.Xml.Schema.XsdKeyTable::Entries
	XsdKeyEntryCollection_t3090959213 * ___Entries_5;
	// Mono.Xml.Schema.XsdKeyEntryCollection Mono.Xml.Schema.XsdKeyTable::FinishedEntries
	XsdKeyEntryCollection_t3090959213 * ___FinishedEntries_6;
	// System.Int32 Mono.Xml.Schema.XsdKeyTable::StartDepth
	int32_t ___StartDepth_7;
	// Mono.Xml.Schema.XsdKeyTable Mono.Xml.Schema.XsdKeyTable::ReferencedKey
	XsdKeyTable_t2156891743 * ___ReferencedKey_8;

public:
	inline static int32_t get_offset_of_alwaysTrue_0() { return static_cast<int32_t>(offsetof(XsdKeyTable_t2156891743, ___alwaysTrue_0)); }
	inline bool get_alwaysTrue_0() const { return ___alwaysTrue_0; }
	inline bool* get_address_of_alwaysTrue_0() { return &___alwaysTrue_0; }
	inline void set_alwaysTrue_0(bool value)
	{
		___alwaysTrue_0 = value;
	}

	inline static int32_t get_offset_of_selector_1() { return static_cast<int32_t>(offsetof(XsdKeyTable_t2156891743, ___selector_1)); }
	inline XsdIdentitySelector_t574258590 * get_selector_1() const { return ___selector_1; }
	inline XsdIdentitySelector_t574258590 ** get_address_of_selector_1() { return &___selector_1; }
	inline void set_selector_1(XsdIdentitySelector_t574258590 * value)
	{
		___selector_1 = value;
		Il2CppCodeGenWriteBarrier((&___selector_1), value);
	}

	inline static int32_t get_offset_of_source_2() { return static_cast<int32_t>(offsetof(XsdKeyTable_t2156891743, ___source_2)); }
	inline XmlSchemaIdentityConstraint_t297318432 * get_source_2() const { return ___source_2; }
	inline XmlSchemaIdentityConstraint_t297318432 ** get_address_of_source_2() { return &___source_2; }
	inline void set_source_2(XmlSchemaIdentityConstraint_t297318432 * value)
	{
		___source_2 = value;
		Il2CppCodeGenWriteBarrier((&___source_2), value);
	}

	inline static int32_t get_offset_of_qname_3() { return static_cast<int32_t>(offsetof(XsdKeyTable_t2156891743, ___qname_3)); }
	inline XmlQualifiedName_t2760654312 * get_qname_3() const { return ___qname_3; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_qname_3() { return &___qname_3; }
	inline void set_qname_3(XmlQualifiedName_t2760654312 * value)
	{
		___qname_3 = value;
		Il2CppCodeGenWriteBarrier((&___qname_3), value);
	}

	inline static int32_t get_offset_of_refKeyName_4() { return static_cast<int32_t>(offsetof(XsdKeyTable_t2156891743, ___refKeyName_4)); }
	inline XmlQualifiedName_t2760654312 * get_refKeyName_4() const { return ___refKeyName_4; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_refKeyName_4() { return &___refKeyName_4; }
	inline void set_refKeyName_4(XmlQualifiedName_t2760654312 * value)
	{
		___refKeyName_4 = value;
		Il2CppCodeGenWriteBarrier((&___refKeyName_4), value);
	}

	inline static int32_t get_offset_of_Entries_5() { return static_cast<int32_t>(offsetof(XsdKeyTable_t2156891743, ___Entries_5)); }
	inline XsdKeyEntryCollection_t3090959213 * get_Entries_5() const { return ___Entries_5; }
	inline XsdKeyEntryCollection_t3090959213 ** get_address_of_Entries_5() { return &___Entries_5; }
	inline void set_Entries_5(XsdKeyEntryCollection_t3090959213 * value)
	{
		___Entries_5 = value;
		Il2CppCodeGenWriteBarrier((&___Entries_5), value);
	}

	inline static int32_t get_offset_of_FinishedEntries_6() { return static_cast<int32_t>(offsetof(XsdKeyTable_t2156891743, ___FinishedEntries_6)); }
	inline XsdKeyEntryCollection_t3090959213 * get_FinishedEntries_6() const { return ___FinishedEntries_6; }
	inline XsdKeyEntryCollection_t3090959213 ** get_address_of_FinishedEntries_6() { return &___FinishedEntries_6; }
	inline void set_FinishedEntries_6(XsdKeyEntryCollection_t3090959213 * value)
	{
		___FinishedEntries_6 = value;
		Il2CppCodeGenWriteBarrier((&___FinishedEntries_6), value);
	}

	inline static int32_t get_offset_of_StartDepth_7() { return static_cast<int32_t>(offsetof(XsdKeyTable_t2156891743, ___StartDepth_7)); }
	inline int32_t get_StartDepth_7() const { return ___StartDepth_7; }
	inline int32_t* get_address_of_StartDepth_7() { return &___StartDepth_7; }
	inline void set_StartDepth_7(int32_t value)
	{
		___StartDepth_7 = value;
	}

	inline static int32_t get_offset_of_ReferencedKey_8() { return static_cast<int32_t>(offsetof(XsdKeyTable_t2156891743, ___ReferencedKey_8)); }
	inline XsdKeyTable_t2156891743 * get_ReferencedKey_8() const { return ___ReferencedKey_8; }
	inline XsdKeyTable_t2156891743 ** get_address_of_ReferencedKey_8() { return &___ReferencedKey_8; }
	inline void set_ReferencedKey_8(XsdKeyTable_t2156891743 * value)
	{
		___ReferencedKey_8 = value;
		Il2CppCodeGenWriteBarrier((&___ReferencedKey_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDKEYTABLE_T2156891743_H
#ifndef XMLREADER_T3121518892_H
#define XMLREADER_T3121518892_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlReader
struct  XmlReader_t3121518892  : public RuntimeObject
{
public:
	// System.Xml.XmlReaderBinarySupport System.Xml.XmlReader::binary
	XmlReaderBinarySupport_t1809665003 * ___binary_0;
	// System.Xml.XmlReaderSettings System.Xml.XmlReader::settings
	XmlReaderSettings_t2186285234 * ___settings_1;

public:
	inline static int32_t get_offset_of_binary_0() { return static_cast<int32_t>(offsetof(XmlReader_t3121518892, ___binary_0)); }
	inline XmlReaderBinarySupport_t1809665003 * get_binary_0() const { return ___binary_0; }
	inline XmlReaderBinarySupport_t1809665003 ** get_address_of_binary_0() { return &___binary_0; }
	inline void set_binary_0(XmlReaderBinarySupport_t1809665003 * value)
	{
		___binary_0 = value;
		Il2CppCodeGenWriteBarrier((&___binary_0), value);
	}

	inline static int32_t get_offset_of_settings_1() { return static_cast<int32_t>(offsetof(XmlReader_t3121518892, ___settings_1)); }
	inline XmlReaderSettings_t2186285234 * get_settings_1() const { return ___settings_1; }
	inline XmlReaderSettings_t2186285234 ** get_address_of_settings_1() { return &___settings_1; }
	inline void set_settings_1(XmlReaderSettings_t2186285234 * value)
	{
		___settings_1 = value;
		Il2CppCodeGenWriteBarrier((&___settings_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLREADER_T3121518892_H
#ifndef XSDVALIDATIONSTATE_T376578997_H
#define XSDVALIDATIONSTATE_T376578997_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdValidationState
struct  XsdValidationState_t376578997  : public RuntimeObject
{
public:
	// System.Int32 Mono.Xml.Schema.XsdValidationState::occured
	int32_t ___occured_1;
	// Mono.Xml.Schema.XsdParticleStateManager Mono.Xml.Schema.XsdValidationState::manager
	XsdParticleStateManager_t726654767 * ___manager_2;

public:
	inline static int32_t get_offset_of_occured_1() { return static_cast<int32_t>(offsetof(XsdValidationState_t376578997, ___occured_1)); }
	inline int32_t get_occured_1() const { return ___occured_1; }
	inline int32_t* get_address_of_occured_1() { return &___occured_1; }
	inline void set_occured_1(int32_t value)
	{
		___occured_1 = value;
	}

	inline static int32_t get_offset_of_manager_2() { return static_cast<int32_t>(offsetof(XsdValidationState_t376578997, ___manager_2)); }
	inline XsdParticleStateManager_t726654767 * get_manager_2() const { return ___manager_2; }
	inline XsdParticleStateManager_t726654767 ** get_address_of_manager_2() { return &___manager_2; }
	inline void set_manager_2(XsdParticleStateManager_t726654767 * value)
	{
		___manager_2 = value;
		Il2CppCodeGenWriteBarrier((&___manager_2), value);
	}
};

struct XsdValidationState_t376578997_StaticFields
{
public:
	// Mono.Xml.Schema.XsdInvalidValidationState Mono.Xml.Schema.XsdValidationState::invalid
	XsdInvalidValidationState_t3749995458 * ___invalid_0;

public:
	inline static int32_t get_offset_of_invalid_0() { return static_cast<int32_t>(offsetof(XsdValidationState_t376578997_StaticFields, ___invalid_0)); }
	inline XsdInvalidValidationState_t3749995458 * get_invalid_0() const { return ___invalid_0; }
	inline XsdInvalidValidationState_t3749995458 ** get_address_of_invalid_0() { return &___invalid_0; }
	inline void set_invalid_0(XsdInvalidValidationState_t3749995458 * value)
	{
		___invalid_0 = value;
		Il2CppCodeGenWriteBarrier((&___invalid_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDVALIDATIONSTATE_T376578997_H
#ifndef URI_T100236324_H
#define URI_T100236324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Uri
struct  Uri_t100236324  : public RuntimeObject
{
public:
	// System.Boolean System.Uri::isUnixFilePath
	bool ___isUnixFilePath_1;
	// System.String System.Uri::source
	String_t* ___source_2;
	// System.String System.Uri::scheme
	String_t* ___scheme_3;
	// System.String System.Uri::host
	String_t* ___host_4;
	// System.Int32 System.Uri::port
	int32_t ___port_5;
	// System.String System.Uri::path
	String_t* ___path_6;
	// System.String System.Uri::query
	String_t* ___query_7;
	// System.String System.Uri::fragment
	String_t* ___fragment_8;
	// System.String System.Uri::userinfo
	String_t* ___userinfo_9;
	// System.Boolean System.Uri::isUnc
	bool ___isUnc_10;
	// System.Boolean System.Uri::isOpaquePart
	bool ___isOpaquePart_11;
	// System.Boolean System.Uri::isAbsoluteUri
	bool ___isAbsoluteUri_12;
	// System.String[] System.Uri::segments
	StringU5BU5D_t1281789340* ___segments_13;
	// System.Boolean System.Uri::userEscaped
	bool ___userEscaped_14;
	// System.String System.Uri::cachedAbsoluteUri
	String_t* ___cachedAbsoluteUri_15;
	// System.String System.Uri::cachedToString
	String_t* ___cachedToString_16;
	// System.String System.Uri::cachedLocalPath
	String_t* ___cachedLocalPath_17;
	// System.Int32 System.Uri::cachedHashCode
	int32_t ___cachedHashCode_18;
	// System.UriParser System.Uri::parser
	UriParser_t3890150400 * ___parser_32;

public:
	inline static int32_t get_offset_of_isUnixFilePath_1() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___isUnixFilePath_1)); }
	inline bool get_isUnixFilePath_1() const { return ___isUnixFilePath_1; }
	inline bool* get_address_of_isUnixFilePath_1() { return &___isUnixFilePath_1; }
	inline void set_isUnixFilePath_1(bool value)
	{
		___isUnixFilePath_1 = value;
	}

	inline static int32_t get_offset_of_source_2() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___source_2)); }
	inline String_t* get_source_2() const { return ___source_2; }
	inline String_t** get_address_of_source_2() { return &___source_2; }
	inline void set_source_2(String_t* value)
	{
		___source_2 = value;
		Il2CppCodeGenWriteBarrier((&___source_2), value);
	}

	inline static int32_t get_offset_of_scheme_3() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___scheme_3)); }
	inline String_t* get_scheme_3() const { return ___scheme_3; }
	inline String_t** get_address_of_scheme_3() { return &___scheme_3; }
	inline void set_scheme_3(String_t* value)
	{
		___scheme_3 = value;
		Il2CppCodeGenWriteBarrier((&___scheme_3), value);
	}

	inline static int32_t get_offset_of_host_4() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___host_4)); }
	inline String_t* get_host_4() const { return ___host_4; }
	inline String_t** get_address_of_host_4() { return &___host_4; }
	inline void set_host_4(String_t* value)
	{
		___host_4 = value;
		Il2CppCodeGenWriteBarrier((&___host_4), value);
	}

	inline static int32_t get_offset_of_port_5() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___port_5)); }
	inline int32_t get_port_5() const { return ___port_5; }
	inline int32_t* get_address_of_port_5() { return &___port_5; }
	inline void set_port_5(int32_t value)
	{
		___port_5 = value;
	}

	inline static int32_t get_offset_of_path_6() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___path_6)); }
	inline String_t* get_path_6() const { return ___path_6; }
	inline String_t** get_address_of_path_6() { return &___path_6; }
	inline void set_path_6(String_t* value)
	{
		___path_6 = value;
		Il2CppCodeGenWriteBarrier((&___path_6), value);
	}

	inline static int32_t get_offset_of_query_7() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___query_7)); }
	inline String_t* get_query_7() const { return ___query_7; }
	inline String_t** get_address_of_query_7() { return &___query_7; }
	inline void set_query_7(String_t* value)
	{
		___query_7 = value;
		Il2CppCodeGenWriteBarrier((&___query_7), value);
	}

	inline static int32_t get_offset_of_fragment_8() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___fragment_8)); }
	inline String_t* get_fragment_8() const { return ___fragment_8; }
	inline String_t** get_address_of_fragment_8() { return &___fragment_8; }
	inline void set_fragment_8(String_t* value)
	{
		___fragment_8 = value;
		Il2CppCodeGenWriteBarrier((&___fragment_8), value);
	}

	inline static int32_t get_offset_of_userinfo_9() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___userinfo_9)); }
	inline String_t* get_userinfo_9() const { return ___userinfo_9; }
	inline String_t** get_address_of_userinfo_9() { return &___userinfo_9; }
	inline void set_userinfo_9(String_t* value)
	{
		___userinfo_9 = value;
		Il2CppCodeGenWriteBarrier((&___userinfo_9), value);
	}

	inline static int32_t get_offset_of_isUnc_10() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___isUnc_10)); }
	inline bool get_isUnc_10() const { return ___isUnc_10; }
	inline bool* get_address_of_isUnc_10() { return &___isUnc_10; }
	inline void set_isUnc_10(bool value)
	{
		___isUnc_10 = value;
	}

	inline static int32_t get_offset_of_isOpaquePart_11() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___isOpaquePart_11)); }
	inline bool get_isOpaquePart_11() const { return ___isOpaquePart_11; }
	inline bool* get_address_of_isOpaquePart_11() { return &___isOpaquePart_11; }
	inline void set_isOpaquePart_11(bool value)
	{
		___isOpaquePart_11 = value;
	}

	inline static int32_t get_offset_of_isAbsoluteUri_12() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___isAbsoluteUri_12)); }
	inline bool get_isAbsoluteUri_12() const { return ___isAbsoluteUri_12; }
	inline bool* get_address_of_isAbsoluteUri_12() { return &___isAbsoluteUri_12; }
	inline void set_isAbsoluteUri_12(bool value)
	{
		___isAbsoluteUri_12 = value;
	}

	inline static int32_t get_offset_of_segments_13() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___segments_13)); }
	inline StringU5BU5D_t1281789340* get_segments_13() const { return ___segments_13; }
	inline StringU5BU5D_t1281789340** get_address_of_segments_13() { return &___segments_13; }
	inline void set_segments_13(StringU5BU5D_t1281789340* value)
	{
		___segments_13 = value;
		Il2CppCodeGenWriteBarrier((&___segments_13), value);
	}

	inline static int32_t get_offset_of_userEscaped_14() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___userEscaped_14)); }
	inline bool get_userEscaped_14() const { return ___userEscaped_14; }
	inline bool* get_address_of_userEscaped_14() { return &___userEscaped_14; }
	inline void set_userEscaped_14(bool value)
	{
		___userEscaped_14 = value;
	}

	inline static int32_t get_offset_of_cachedAbsoluteUri_15() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___cachedAbsoluteUri_15)); }
	inline String_t* get_cachedAbsoluteUri_15() const { return ___cachedAbsoluteUri_15; }
	inline String_t** get_address_of_cachedAbsoluteUri_15() { return &___cachedAbsoluteUri_15; }
	inline void set_cachedAbsoluteUri_15(String_t* value)
	{
		___cachedAbsoluteUri_15 = value;
		Il2CppCodeGenWriteBarrier((&___cachedAbsoluteUri_15), value);
	}

	inline static int32_t get_offset_of_cachedToString_16() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___cachedToString_16)); }
	inline String_t* get_cachedToString_16() const { return ___cachedToString_16; }
	inline String_t** get_address_of_cachedToString_16() { return &___cachedToString_16; }
	inline void set_cachedToString_16(String_t* value)
	{
		___cachedToString_16 = value;
		Il2CppCodeGenWriteBarrier((&___cachedToString_16), value);
	}

	inline static int32_t get_offset_of_cachedLocalPath_17() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___cachedLocalPath_17)); }
	inline String_t* get_cachedLocalPath_17() const { return ___cachedLocalPath_17; }
	inline String_t** get_address_of_cachedLocalPath_17() { return &___cachedLocalPath_17; }
	inline void set_cachedLocalPath_17(String_t* value)
	{
		___cachedLocalPath_17 = value;
		Il2CppCodeGenWriteBarrier((&___cachedLocalPath_17), value);
	}

	inline static int32_t get_offset_of_cachedHashCode_18() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___cachedHashCode_18)); }
	inline int32_t get_cachedHashCode_18() const { return ___cachedHashCode_18; }
	inline int32_t* get_address_of_cachedHashCode_18() { return &___cachedHashCode_18; }
	inline void set_cachedHashCode_18(int32_t value)
	{
		___cachedHashCode_18 = value;
	}

	inline static int32_t get_offset_of_parser_32() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___parser_32)); }
	inline UriParser_t3890150400 * get_parser_32() const { return ___parser_32; }
	inline UriParser_t3890150400 ** get_address_of_parser_32() { return &___parser_32; }
	inline void set_parser_32(UriParser_t3890150400 * value)
	{
		___parser_32 = value;
		Il2CppCodeGenWriteBarrier((&___parser_32), value);
	}
};

struct Uri_t100236324_StaticFields
{
public:
	// System.String System.Uri::hexUpperChars
	String_t* ___hexUpperChars_19;
	// System.String System.Uri::SchemeDelimiter
	String_t* ___SchemeDelimiter_20;
	// System.String System.Uri::UriSchemeFile
	String_t* ___UriSchemeFile_21;
	// System.String System.Uri::UriSchemeFtp
	String_t* ___UriSchemeFtp_22;
	// System.String System.Uri::UriSchemeGopher
	String_t* ___UriSchemeGopher_23;
	// System.String System.Uri::UriSchemeHttp
	String_t* ___UriSchemeHttp_24;
	// System.String System.Uri::UriSchemeHttps
	String_t* ___UriSchemeHttps_25;
	// System.String System.Uri::UriSchemeMailto
	String_t* ___UriSchemeMailto_26;
	// System.String System.Uri::UriSchemeNews
	String_t* ___UriSchemeNews_27;
	// System.String System.Uri::UriSchemeNntp
	String_t* ___UriSchemeNntp_28;
	// System.String System.Uri::UriSchemeNetPipe
	String_t* ___UriSchemeNetPipe_29;
	// System.String System.Uri::UriSchemeNetTcp
	String_t* ___UriSchemeNetTcp_30;
	// System.Uri/UriScheme[] System.Uri::schemes
	UriSchemeU5BU5D_t2082808316* ___schemes_31;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Uri::<>f__switch$map12
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map12_33;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Uri::<>f__switch$map13
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map13_34;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Uri::<>f__switch$map14
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map14_35;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Uri::<>f__switch$map15
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map15_36;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Uri::<>f__switch$map16
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map16_37;

public:
	inline static int32_t get_offset_of_hexUpperChars_19() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___hexUpperChars_19)); }
	inline String_t* get_hexUpperChars_19() const { return ___hexUpperChars_19; }
	inline String_t** get_address_of_hexUpperChars_19() { return &___hexUpperChars_19; }
	inline void set_hexUpperChars_19(String_t* value)
	{
		___hexUpperChars_19 = value;
		Il2CppCodeGenWriteBarrier((&___hexUpperChars_19), value);
	}

	inline static int32_t get_offset_of_SchemeDelimiter_20() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___SchemeDelimiter_20)); }
	inline String_t* get_SchemeDelimiter_20() const { return ___SchemeDelimiter_20; }
	inline String_t** get_address_of_SchemeDelimiter_20() { return &___SchemeDelimiter_20; }
	inline void set_SchemeDelimiter_20(String_t* value)
	{
		___SchemeDelimiter_20 = value;
		Il2CppCodeGenWriteBarrier((&___SchemeDelimiter_20), value);
	}

	inline static int32_t get_offset_of_UriSchemeFile_21() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeFile_21)); }
	inline String_t* get_UriSchemeFile_21() const { return ___UriSchemeFile_21; }
	inline String_t** get_address_of_UriSchemeFile_21() { return &___UriSchemeFile_21; }
	inline void set_UriSchemeFile_21(String_t* value)
	{
		___UriSchemeFile_21 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeFile_21), value);
	}

	inline static int32_t get_offset_of_UriSchemeFtp_22() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeFtp_22)); }
	inline String_t* get_UriSchemeFtp_22() const { return ___UriSchemeFtp_22; }
	inline String_t** get_address_of_UriSchemeFtp_22() { return &___UriSchemeFtp_22; }
	inline void set_UriSchemeFtp_22(String_t* value)
	{
		___UriSchemeFtp_22 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeFtp_22), value);
	}

	inline static int32_t get_offset_of_UriSchemeGopher_23() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeGopher_23)); }
	inline String_t* get_UriSchemeGopher_23() const { return ___UriSchemeGopher_23; }
	inline String_t** get_address_of_UriSchemeGopher_23() { return &___UriSchemeGopher_23; }
	inline void set_UriSchemeGopher_23(String_t* value)
	{
		___UriSchemeGopher_23 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeGopher_23), value);
	}

	inline static int32_t get_offset_of_UriSchemeHttp_24() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeHttp_24)); }
	inline String_t* get_UriSchemeHttp_24() const { return ___UriSchemeHttp_24; }
	inline String_t** get_address_of_UriSchemeHttp_24() { return &___UriSchemeHttp_24; }
	inline void set_UriSchemeHttp_24(String_t* value)
	{
		___UriSchemeHttp_24 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeHttp_24), value);
	}

	inline static int32_t get_offset_of_UriSchemeHttps_25() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeHttps_25)); }
	inline String_t* get_UriSchemeHttps_25() const { return ___UriSchemeHttps_25; }
	inline String_t** get_address_of_UriSchemeHttps_25() { return &___UriSchemeHttps_25; }
	inline void set_UriSchemeHttps_25(String_t* value)
	{
		___UriSchemeHttps_25 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeHttps_25), value);
	}

	inline static int32_t get_offset_of_UriSchemeMailto_26() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeMailto_26)); }
	inline String_t* get_UriSchemeMailto_26() const { return ___UriSchemeMailto_26; }
	inline String_t** get_address_of_UriSchemeMailto_26() { return &___UriSchemeMailto_26; }
	inline void set_UriSchemeMailto_26(String_t* value)
	{
		___UriSchemeMailto_26 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeMailto_26), value);
	}

	inline static int32_t get_offset_of_UriSchemeNews_27() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeNews_27)); }
	inline String_t* get_UriSchemeNews_27() const { return ___UriSchemeNews_27; }
	inline String_t** get_address_of_UriSchemeNews_27() { return &___UriSchemeNews_27; }
	inline void set_UriSchemeNews_27(String_t* value)
	{
		___UriSchemeNews_27 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeNews_27), value);
	}

	inline static int32_t get_offset_of_UriSchemeNntp_28() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeNntp_28)); }
	inline String_t* get_UriSchemeNntp_28() const { return ___UriSchemeNntp_28; }
	inline String_t** get_address_of_UriSchemeNntp_28() { return &___UriSchemeNntp_28; }
	inline void set_UriSchemeNntp_28(String_t* value)
	{
		___UriSchemeNntp_28 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeNntp_28), value);
	}

	inline static int32_t get_offset_of_UriSchemeNetPipe_29() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeNetPipe_29)); }
	inline String_t* get_UriSchemeNetPipe_29() const { return ___UriSchemeNetPipe_29; }
	inline String_t** get_address_of_UriSchemeNetPipe_29() { return &___UriSchemeNetPipe_29; }
	inline void set_UriSchemeNetPipe_29(String_t* value)
	{
		___UriSchemeNetPipe_29 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeNetPipe_29), value);
	}

	inline static int32_t get_offset_of_UriSchemeNetTcp_30() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeNetTcp_30)); }
	inline String_t* get_UriSchemeNetTcp_30() const { return ___UriSchemeNetTcp_30; }
	inline String_t** get_address_of_UriSchemeNetTcp_30() { return &___UriSchemeNetTcp_30; }
	inline void set_UriSchemeNetTcp_30(String_t* value)
	{
		___UriSchemeNetTcp_30 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeNetTcp_30), value);
	}

	inline static int32_t get_offset_of_schemes_31() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___schemes_31)); }
	inline UriSchemeU5BU5D_t2082808316* get_schemes_31() const { return ___schemes_31; }
	inline UriSchemeU5BU5D_t2082808316** get_address_of_schemes_31() { return &___schemes_31; }
	inline void set_schemes_31(UriSchemeU5BU5D_t2082808316* value)
	{
		___schemes_31 = value;
		Il2CppCodeGenWriteBarrier((&___schemes_31), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map12_33() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___U3CU3Ef__switchU24map12_33)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map12_33() const { return ___U3CU3Ef__switchU24map12_33; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map12_33() { return &___U3CU3Ef__switchU24map12_33; }
	inline void set_U3CU3Ef__switchU24map12_33(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map12_33 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map12_33), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map13_34() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___U3CU3Ef__switchU24map13_34)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map13_34() const { return ___U3CU3Ef__switchU24map13_34; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map13_34() { return &___U3CU3Ef__switchU24map13_34; }
	inline void set_U3CU3Ef__switchU24map13_34(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map13_34 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map13_34), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map14_35() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___U3CU3Ef__switchU24map14_35)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map14_35() const { return ___U3CU3Ef__switchU24map14_35; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map14_35() { return &___U3CU3Ef__switchU24map14_35; }
	inline void set_U3CU3Ef__switchU24map14_35(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map14_35 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map14_35), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map15_36() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___U3CU3Ef__switchU24map15_36)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map15_36() const { return ___U3CU3Ef__switchU24map15_36; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map15_36() { return &___U3CU3Ef__switchU24map15_36; }
	inline void set_U3CU3Ef__switchU24map15_36(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map15_36 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map15_36), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map16_37() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___U3CU3Ef__switchU24map16_37)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map16_37() const { return ___U3CU3Ef__switchU24map16_37; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map16_37() { return &___U3CU3Ef__switchU24map16_37; }
	inline void set_U3CU3Ef__switchU24map16_37(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map16_37 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map16_37), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URI_T100236324_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef XSDIDMANAGER_T1008806102_H
#define XSDIDMANAGER_T1008806102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdIDManager
struct  XsdIDManager_t1008806102  : public RuntimeObject
{
public:
	// System.Collections.Hashtable Mono.Xml.Schema.XsdIDManager::idList
	Hashtable_t1853889766 * ___idList_0;
	// System.Collections.ArrayList Mono.Xml.Schema.XsdIDManager::missingIDReferences
	ArrayList_t2718874744 * ___missingIDReferences_1;
	// System.String Mono.Xml.Schema.XsdIDManager::thisElementId
	String_t* ___thisElementId_2;

public:
	inline static int32_t get_offset_of_idList_0() { return static_cast<int32_t>(offsetof(XsdIDManager_t1008806102, ___idList_0)); }
	inline Hashtable_t1853889766 * get_idList_0() const { return ___idList_0; }
	inline Hashtable_t1853889766 ** get_address_of_idList_0() { return &___idList_0; }
	inline void set_idList_0(Hashtable_t1853889766 * value)
	{
		___idList_0 = value;
		Il2CppCodeGenWriteBarrier((&___idList_0), value);
	}

	inline static int32_t get_offset_of_missingIDReferences_1() { return static_cast<int32_t>(offsetof(XsdIDManager_t1008806102, ___missingIDReferences_1)); }
	inline ArrayList_t2718874744 * get_missingIDReferences_1() const { return ___missingIDReferences_1; }
	inline ArrayList_t2718874744 ** get_address_of_missingIDReferences_1() { return &___missingIDReferences_1; }
	inline void set_missingIDReferences_1(ArrayList_t2718874744 * value)
	{
		___missingIDReferences_1 = value;
		Il2CppCodeGenWriteBarrier((&___missingIDReferences_1), value);
	}

	inline static int32_t get_offset_of_thisElementId_2() { return static_cast<int32_t>(offsetof(XsdIDManager_t1008806102, ___thisElementId_2)); }
	inline String_t* get_thisElementId_2() const { return ___thisElementId_2; }
	inline String_t** get_address_of_thisElementId_2() { return &___thisElementId_2; }
	inline void set_thisElementId_2(String_t* value)
	{
		___thisElementId_2 = value;
		Il2CppCodeGenWriteBarrier((&___thisElementId_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDIDMANAGER_T1008806102_H
#ifndef XSDIDENTITYPATH_T991900844_H
#define XSDIDENTITYPATH_T991900844_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdIdentityPath
struct  XsdIdentityPath_t991900844  : public RuntimeObject
{
public:
	// Mono.Xml.Schema.XsdIdentityStep[] Mono.Xml.Schema.XsdIdentityPath::OrderedSteps
	XsdIdentityStepU5BU5D_t2964233348* ___OrderedSteps_0;
	// System.Boolean Mono.Xml.Schema.XsdIdentityPath::Descendants
	bool ___Descendants_1;

public:
	inline static int32_t get_offset_of_OrderedSteps_0() { return static_cast<int32_t>(offsetof(XsdIdentityPath_t991900844, ___OrderedSteps_0)); }
	inline XsdIdentityStepU5BU5D_t2964233348* get_OrderedSteps_0() const { return ___OrderedSteps_0; }
	inline XsdIdentityStepU5BU5D_t2964233348** get_address_of_OrderedSteps_0() { return &___OrderedSteps_0; }
	inline void set_OrderedSteps_0(XsdIdentityStepU5BU5D_t2964233348* value)
	{
		___OrderedSteps_0 = value;
		Il2CppCodeGenWriteBarrier((&___OrderedSteps_0), value);
	}

	inline static int32_t get_offset_of_Descendants_1() { return static_cast<int32_t>(offsetof(XsdIdentityPath_t991900844, ___Descendants_1)); }
	inline bool get_Descendants_1() const { return ___Descendants_1; }
	inline bool* get_address_of_Descendants_1() { return &___Descendants_1; }
	inline void set_Descendants_1(bool value)
	{
		___Descendants_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDIDENTITYPATH_T991900844_H
#ifndef XSDIDENTITYFIELD_T1964115728_H
#define XSDIDENTITYFIELD_T1964115728_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdIdentityField
struct  XsdIdentityField_t1964115728  : public RuntimeObject
{
public:
	// Mono.Xml.Schema.XsdIdentityPath[] Mono.Xml.Schema.XsdIdentityField::fieldPaths
	XsdIdentityPathU5BU5D_t2466178853* ___fieldPaths_0;
	// System.Int32 Mono.Xml.Schema.XsdIdentityField::index
	int32_t ___index_1;

public:
	inline static int32_t get_offset_of_fieldPaths_0() { return static_cast<int32_t>(offsetof(XsdIdentityField_t1964115728, ___fieldPaths_0)); }
	inline XsdIdentityPathU5BU5D_t2466178853* get_fieldPaths_0() const { return ___fieldPaths_0; }
	inline XsdIdentityPathU5BU5D_t2466178853** get_address_of_fieldPaths_0() { return &___fieldPaths_0; }
	inline void set_fieldPaths_0(XsdIdentityPathU5BU5D_t2466178853* value)
	{
		___fieldPaths_0 = value;
		Il2CppCodeGenWriteBarrier((&___fieldPaths_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(XsdIdentityField_t1964115728, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDIDENTITYFIELD_T1964115728_H
#ifndef CUSTOMYIELDINSTRUCTION_T1895667560_H
#define CUSTOMYIELDINSTRUCTION_T1895667560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CustomYieldInstruction
struct  CustomYieldInstruction_t1895667560  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMYIELDINSTRUCTION_T1895667560_H
#ifndef ANALYTICSSESSIONINFO_T2322308579_H
#define ANALYTICSSESSIONINFO_T2322308579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsSessionInfo
struct  AnalyticsSessionInfo_t2322308579  : public RuntimeObject
{
public:

public:
};

struct AnalyticsSessionInfo_t2322308579_StaticFields
{
public:
	// UnityEngine.Analytics.AnalyticsSessionInfo/SessionStateChanged UnityEngine.Analytics.AnalyticsSessionInfo::sessionStateChanged
	SessionStateChanged_t3163629820 * ___sessionStateChanged_0;

public:
	inline static int32_t get_offset_of_sessionStateChanged_0() { return static_cast<int32_t>(offsetof(AnalyticsSessionInfo_t2322308579_StaticFields, ___sessionStateChanged_0)); }
	inline SessionStateChanged_t3163629820 * get_sessionStateChanged_0() const { return ___sessionStateChanged_0; }
	inline SessionStateChanged_t3163629820 ** get_address_of_sessionStateChanged_0() { return &___sessionStateChanged_0; }
	inline void set_sessionStateChanged_0(SessionStateChanged_t3163629820 * value)
	{
		___sessionStateChanged_0 = value;
		Il2CppCodeGenWriteBarrier((&___sessionStateChanged_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSSESSIONINFO_T2322308579_H
#ifndef REMOTESETTINGS_T1718627291_H
#define REMOTESETTINGS_T1718627291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RemoteSettings
struct  RemoteSettings_t1718627291  : public RuntimeObject
{
public:

public:
};

struct RemoteSettings_t1718627291_StaticFields
{
public:
	// UnityEngine.RemoteSettings/UpdatedEventHandler UnityEngine.RemoteSettings::Updated
	UpdatedEventHandler_t1027848393 * ___Updated_0;

public:
	inline static int32_t get_offset_of_Updated_0() { return static_cast<int32_t>(offsetof(RemoteSettings_t1718627291_StaticFields, ___Updated_0)); }
	inline UpdatedEventHandler_t1027848393 * get_Updated_0() const { return ___Updated_0; }
	inline UpdatedEventHandler_t1027848393 ** get_address_of_Updated_0() { return &___Updated_0; }
	inline void set_Updated_0(UpdatedEventHandler_t1027848393 * value)
	{
		___Updated_0 = value;
		Il2CppCodeGenWriteBarrier((&___Updated_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTESETTINGS_T1718627291_H
#ifndef RECTTRANSFORMUTILITY_T1743242446_H
#define RECTTRANSFORMUTILITY_T1743242446_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectTransformUtility
struct  RectTransformUtility_t1743242446  : public RuntimeObject
{
public:

public:
};

struct RectTransformUtility_t1743242446_StaticFields
{
public:
	// UnityEngine.Vector3[] UnityEngine.RectTransformUtility::s_Corners
	Vector3U5BU5D_t1718750761* ___s_Corners_0;

public:
	inline static int32_t get_offset_of_s_Corners_0() { return static_cast<int32_t>(offsetof(RectTransformUtility_t1743242446_StaticFields, ___s_Corners_0)); }
	inline Vector3U5BU5D_t1718750761* get_s_Corners_0() const { return ___s_Corners_0; }
	inline Vector3U5BU5D_t1718750761** get_address_of_s_Corners_0() { return &___s_Corners_0; }
	inline void set_s_Corners_0(Vector3U5BU5D_t1718750761* value)
	{
		___s_Corners_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Corners_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORMUTILITY_T1743242446_H
#ifndef UISYSTEMPROFILERAPI_T2230074258_H
#define UISYSTEMPROFILERAPI_T2230074258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UISystemProfilerApi
struct  UISystemProfilerApi_t2230074258  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISYSTEMPROFILERAPI_T2230074258_H
#ifndef XSDIDENTITYSELECTOR_T574258590_H
#define XSDIDENTITYSELECTOR_T574258590_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdIdentitySelector
struct  XsdIdentitySelector_t574258590  : public RuntimeObject
{
public:
	// Mono.Xml.Schema.XsdIdentityPath[] Mono.Xml.Schema.XsdIdentitySelector::selectorPaths
	XsdIdentityPathU5BU5D_t2466178853* ___selectorPaths_0;
	// System.Collections.ArrayList Mono.Xml.Schema.XsdIdentitySelector::fields
	ArrayList_t2718874744 * ___fields_1;
	// Mono.Xml.Schema.XsdIdentityField[] Mono.Xml.Schema.XsdIdentitySelector::cachedFields
	XsdIdentityFieldU5BU5D_t881076913* ___cachedFields_2;

public:
	inline static int32_t get_offset_of_selectorPaths_0() { return static_cast<int32_t>(offsetof(XsdIdentitySelector_t574258590, ___selectorPaths_0)); }
	inline XsdIdentityPathU5BU5D_t2466178853* get_selectorPaths_0() const { return ___selectorPaths_0; }
	inline XsdIdentityPathU5BU5D_t2466178853** get_address_of_selectorPaths_0() { return &___selectorPaths_0; }
	inline void set_selectorPaths_0(XsdIdentityPathU5BU5D_t2466178853* value)
	{
		___selectorPaths_0 = value;
		Il2CppCodeGenWriteBarrier((&___selectorPaths_0), value);
	}

	inline static int32_t get_offset_of_fields_1() { return static_cast<int32_t>(offsetof(XsdIdentitySelector_t574258590, ___fields_1)); }
	inline ArrayList_t2718874744 * get_fields_1() const { return ___fields_1; }
	inline ArrayList_t2718874744 ** get_address_of_fields_1() { return &___fields_1; }
	inline void set_fields_1(ArrayList_t2718874744 * value)
	{
		___fields_1 = value;
		Il2CppCodeGenWriteBarrier((&___fields_1), value);
	}

	inline static int32_t get_offset_of_cachedFields_2() { return static_cast<int32_t>(offsetof(XsdIdentitySelector_t574258590, ___cachedFields_2)); }
	inline XsdIdentityFieldU5BU5D_t881076913* get_cachedFields_2() const { return ___cachedFields_2; }
	inline XsdIdentityFieldU5BU5D_t881076913** get_address_of_cachedFields_2() { return &___cachedFields_2; }
	inline void set_cachedFields_2(XsdIdentityFieldU5BU5D_t881076913* value)
	{
		___cachedFields_2 = value;
		Il2CppCodeGenWriteBarrier((&___cachedFields_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDIDENTITYSELECTOR_T574258590_H
#ifndef XSDINVALIDVALIDATIONSTATE_T3749995458_H
#define XSDINVALIDVALIDATIONSTATE_T3749995458_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdInvalidValidationState
struct  XsdInvalidValidationState_t3749995458  : public XsdValidationState_t376578997
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDINVALIDVALIDATIONSTATE_T3749995458_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef INT64_T3736567304_H
#define INT64_T3736567304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int64
struct  Int64_t3736567304 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int64_t3736567304, ___m_value_2)); }
	inline int64_t get_m_value_2() const { return ___m_value_2; }
	inline int64_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int64_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64_T3736567304_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef URIVALUETYPE_T2866347695_H
#define URIVALUETYPE_T2866347695_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.UriValueType
struct  UriValueType_t2866347695 
{
public:
	// Mono.Xml.Schema.XmlSchemaUri System.Xml.Schema.UriValueType::value
	XmlSchemaUri_t3948303260 * ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(UriValueType_t2866347695, ___value_0)); }
	inline XmlSchemaUri_t3948303260 * get_value_0() const { return ___value_0; }
	inline XmlSchemaUri_t3948303260 ** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(XmlSchemaUri_t3948303260 * value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.Schema.UriValueType
struct UriValueType_t2866347695_marshaled_pinvoke
{
	XmlSchemaUri_t3948303260 * ___value_0;
};
// Native definition for COM marshalling of System.Xml.Schema.UriValueType
struct UriValueType_t2866347695_marshaled_com
{
	XmlSchemaUri_t3948303260 * ___value_0;
};
#endif // URIVALUETYPE_T2866347695_H
#ifndef STRINGVALUETYPE_T261329552_H
#define STRINGVALUETYPE_T261329552_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.StringValueType
struct  StringValueType_t261329552 
{
public:
	// System.String System.Xml.Schema.StringValueType::value
	String_t* ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(StringValueType_t261329552, ___value_0)); }
	inline String_t* get_value_0() const { return ___value_0; }
	inline String_t** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(String_t* value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.Schema.StringValueType
struct StringValueType_t261329552_marshaled_pinvoke
{
	char* ___value_0;
};
// Native definition for COM marshalling of System.Xml.Schema.StringValueType
struct StringValueType_t261329552_marshaled_com
{
	Il2CppChar* ___value_0;
};
#endif // STRINGVALUETYPE_T261329552_H
#ifndef QNAMEVALUETYPE_T615604793_H
#define QNAMEVALUETYPE_T615604793_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.QNameValueType
struct  QNameValueType_t615604793 
{
public:
	// System.Xml.XmlQualifiedName System.Xml.Schema.QNameValueType::value
	XmlQualifiedName_t2760654312 * ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(QNameValueType_t615604793, ___value_0)); }
	inline XmlQualifiedName_t2760654312 * get_value_0() const { return ___value_0; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(XmlQualifiedName_t2760654312 * value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.Schema.QNameValueType
struct QNameValueType_t615604793_marshaled_pinvoke
{
	XmlQualifiedName_t2760654312 * ___value_0;
};
// Native definition for COM marshalling of System.Xml.Schema.QNameValueType
struct QNameValueType_t615604793_marshaled_com
{
	XmlQualifiedName_t2760654312 * ___value_0;
};
#endif // QNAMEVALUETYPE_T615604793_H
#ifndef XMLSCHEMAURI_T3948303260_H
#define XMLSCHEMAURI_T3948303260_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XmlSchemaUri
struct  XmlSchemaUri_t3948303260  : public Uri_t100236324
{
public:
	// System.String Mono.Xml.Schema.XmlSchemaUri::value
	String_t* ___value_38;

public:
	inline static int32_t get_offset_of_value_38() { return static_cast<int32_t>(offsetof(XmlSchemaUri_t3948303260, ___value_38)); }
	inline String_t* get_value_38() const { return ___value_38; }
	inline String_t** get_address_of_value_38() { return &___value_38; }
	inline void set_value_38(String_t* value)
	{
		___value_38 = value;
		Il2CppCodeGenWriteBarrier((&___value_38), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAURI_T3948303260_H
#ifndef XSDEMPTYVALIDATIONSTATE_T1344146143_H
#define XSDEMPTYVALIDATIONSTATE_T1344146143_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdEmptyValidationState
struct  XsdEmptyValidationState_t1344146143  : public XsdValidationState_t376578997
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDEMPTYVALIDATIONSTATE_T1344146143_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef XSDANYVALIDATIONSTATE_T3421545252_H
#define XSDANYVALIDATIONSTATE_T3421545252_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdAnyValidationState
struct  XsdAnyValidationState_t3421545252  : public XsdValidationState_t376578997
{
public:
	// System.Xml.Schema.XmlSchemaAny Mono.Xml.Schema.XsdAnyValidationState::any
	XmlSchemaAny_t1119175207 * ___any_3;

public:
	inline static int32_t get_offset_of_any_3() { return static_cast<int32_t>(offsetof(XsdAnyValidationState_t3421545252, ___any_3)); }
	inline XmlSchemaAny_t1119175207 * get_any_3() const { return ___any_3; }
	inline XmlSchemaAny_t1119175207 ** get_address_of_any_3() { return &___any_3; }
	inline void set_any_3(XmlSchemaAny_t1119175207 * value)
	{
		___any_3 = value;
		Il2CppCodeGenWriteBarrier((&___any_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDANYVALIDATIONSTATE_T3421545252_H
#ifndef XSDALLVALIDATIONSTATE_T2703884157_H
#define XSDALLVALIDATIONSTATE_T2703884157_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdAllValidationState
struct  XsdAllValidationState_t2703884157  : public XsdValidationState_t376578997
{
public:
	// System.Xml.Schema.XmlSchemaAll Mono.Xml.Schema.XsdAllValidationState::all
	XmlSchemaAll_t1118454309 * ___all_3;
	// System.Collections.ArrayList Mono.Xml.Schema.XsdAllValidationState::consumed
	ArrayList_t2718874744 * ___consumed_4;

public:
	inline static int32_t get_offset_of_all_3() { return static_cast<int32_t>(offsetof(XsdAllValidationState_t2703884157, ___all_3)); }
	inline XmlSchemaAll_t1118454309 * get_all_3() const { return ___all_3; }
	inline XmlSchemaAll_t1118454309 ** get_address_of_all_3() { return &___all_3; }
	inline void set_all_3(XmlSchemaAll_t1118454309 * value)
	{
		___all_3 = value;
		Il2CppCodeGenWriteBarrier((&___all_3), value);
	}

	inline static int32_t get_offset_of_consumed_4() { return static_cast<int32_t>(offsetof(XsdAllValidationState_t2703884157, ___consumed_4)); }
	inline ArrayList_t2718874744 * get_consumed_4() const { return ___consumed_4; }
	inline ArrayList_t2718874744 ** get_address_of_consumed_4() { return &___consumed_4; }
	inline void set_consumed_4(ArrayList_t2718874744 * value)
	{
		___consumed_4 = value;
		Il2CppCodeGenWriteBarrier((&___consumed_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDALLVALIDATIONSTATE_T2703884157_H
#ifndef XSDCHOICEVALIDATIONSTATE_T2566230191_H
#define XSDCHOICEVALIDATIONSTATE_T2566230191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdChoiceValidationState
struct  XsdChoiceValidationState_t2566230191  : public XsdValidationState_t376578997
{
public:
	// System.Xml.Schema.XmlSchemaChoice Mono.Xml.Schema.XsdChoiceValidationState::choice
	XmlSchemaChoice_t959520675 * ___choice_3;
	// System.Boolean Mono.Xml.Schema.XsdChoiceValidationState::emptiable
	bool ___emptiable_4;
	// System.Boolean Mono.Xml.Schema.XsdChoiceValidationState::emptiableComputed
	bool ___emptiableComputed_5;

public:
	inline static int32_t get_offset_of_choice_3() { return static_cast<int32_t>(offsetof(XsdChoiceValidationState_t2566230191, ___choice_3)); }
	inline XmlSchemaChoice_t959520675 * get_choice_3() const { return ___choice_3; }
	inline XmlSchemaChoice_t959520675 ** get_address_of_choice_3() { return &___choice_3; }
	inline void set_choice_3(XmlSchemaChoice_t959520675 * value)
	{
		___choice_3 = value;
		Il2CppCodeGenWriteBarrier((&___choice_3), value);
	}

	inline static int32_t get_offset_of_emptiable_4() { return static_cast<int32_t>(offsetof(XsdChoiceValidationState_t2566230191, ___emptiable_4)); }
	inline bool get_emptiable_4() const { return ___emptiable_4; }
	inline bool* get_address_of_emptiable_4() { return &___emptiable_4; }
	inline void set_emptiable_4(bool value)
	{
		___emptiable_4 = value;
	}

	inline static int32_t get_offset_of_emptiableComputed_5() { return static_cast<int32_t>(offsetof(XsdChoiceValidationState_t2566230191, ___emptiableComputed_5)); }
	inline bool get_emptiableComputed_5() const { return ___emptiableComputed_5; }
	inline bool* get_address_of_emptiableComputed_5() { return &___emptiableComputed_5; }
	inline void set_emptiableComputed_5(bool value)
	{
		___emptiableComputed_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDCHOICEVALIDATIONSTATE_T2566230191_H
#ifndef XSDAPPENDEDVALIDATIONSTATE_T3608891238_H
#define XSDAPPENDEDVALIDATIONSTATE_T3608891238_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdAppendedValidationState
struct  XsdAppendedValidationState_t3608891238  : public XsdValidationState_t376578997
{
public:
	// Mono.Xml.Schema.XsdValidationState Mono.Xml.Schema.XsdAppendedValidationState::head
	XsdValidationState_t376578997 * ___head_3;
	// Mono.Xml.Schema.XsdValidationState Mono.Xml.Schema.XsdAppendedValidationState::rest
	XsdValidationState_t376578997 * ___rest_4;

public:
	inline static int32_t get_offset_of_head_3() { return static_cast<int32_t>(offsetof(XsdAppendedValidationState_t3608891238, ___head_3)); }
	inline XsdValidationState_t376578997 * get_head_3() const { return ___head_3; }
	inline XsdValidationState_t376578997 ** get_address_of_head_3() { return &___head_3; }
	inline void set_head_3(XsdValidationState_t376578997 * value)
	{
		___head_3 = value;
		Il2CppCodeGenWriteBarrier((&___head_3), value);
	}

	inline static int32_t get_offset_of_rest_4() { return static_cast<int32_t>(offsetof(XsdAppendedValidationState_t3608891238, ___rest_4)); }
	inline XsdValidationState_t376578997 * get_rest_4() const { return ___rest_4; }
	inline XsdValidationState_t376578997 ** get_address_of_rest_4() { return &___rest_4; }
	inline void set_rest_4(XsdValidationState_t376578997 * value)
	{
		___rest_4 = value;
		Il2CppCodeGenWriteBarrier((&___rest_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDAPPENDEDVALIDATIONSTATE_T3608891238_H
#ifndef WWW_T3688466362_H
#define WWW_T3688466362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WWW
struct  WWW_t3688466362  : public CustomYieldInstruction_t1895667560
{
public:
	// UnityEngine.Networking.UnityWebRequest UnityEngine.WWW::_uwr
	UnityWebRequest_t463507806 * ____uwr_0;

public:
	inline static int32_t get_offset_of__uwr_0() { return static_cast<int32_t>(offsetof(WWW_t3688466362, ____uwr_0)); }
	inline UnityWebRequest_t463507806 * get__uwr_0() const { return ____uwr_0; }
	inline UnityWebRequest_t463507806 ** get_address_of__uwr_0() { return &____uwr_0; }
	inline void set__uwr_0(UnityWebRequest_t463507806 * value)
	{
		____uwr_0 = value;
		Il2CppCodeGenWriteBarrier((&____uwr_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WWW_T3688466362_H
#ifndef MONOTODOATTRIBUTE_T4131080584_H
#define MONOTODOATTRIBUTE_T4131080584_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MonoTODOAttribute
struct  MonoTODOAttribute_t4131080584  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOTODOATTRIBUTE_T4131080584_H
#ifndef XSDSEQUENCEVALIDATIONSTATE_T429792968_H
#define XSDSEQUENCEVALIDATIONSTATE_T429792968_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdSequenceValidationState
struct  XsdSequenceValidationState_t429792968  : public XsdValidationState_t376578997
{
public:
	// System.Xml.Schema.XmlSchemaSequence Mono.Xml.Schema.XsdSequenceValidationState::seq
	XmlSchemaSequence_t2018345177 * ___seq_3;
	// System.Int32 Mono.Xml.Schema.XsdSequenceValidationState::current
	int32_t ___current_4;
	// Mono.Xml.Schema.XsdValidationState Mono.Xml.Schema.XsdSequenceValidationState::currentAutomata
	XsdValidationState_t376578997 * ___currentAutomata_5;
	// System.Boolean Mono.Xml.Schema.XsdSequenceValidationState::emptiable
	bool ___emptiable_6;

public:
	inline static int32_t get_offset_of_seq_3() { return static_cast<int32_t>(offsetof(XsdSequenceValidationState_t429792968, ___seq_3)); }
	inline XmlSchemaSequence_t2018345177 * get_seq_3() const { return ___seq_3; }
	inline XmlSchemaSequence_t2018345177 ** get_address_of_seq_3() { return &___seq_3; }
	inline void set_seq_3(XmlSchemaSequence_t2018345177 * value)
	{
		___seq_3 = value;
		Il2CppCodeGenWriteBarrier((&___seq_3), value);
	}

	inline static int32_t get_offset_of_current_4() { return static_cast<int32_t>(offsetof(XsdSequenceValidationState_t429792968, ___current_4)); }
	inline int32_t get_current_4() const { return ___current_4; }
	inline int32_t* get_address_of_current_4() { return &___current_4; }
	inline void set_current_4(int32_t value)
	{
		___current_4 = value;
	}

	inline static int32_t get_offset_of_currentAutomata_5() { return static_cast<int32_t>(offsetof(XsdSequenceValidationState_t429792968, ___currentAutomata_5)); }
	inline XsdValidationState_t376578997 * get_currentAutomata_5() const { return ___currentAutomata_5; }
	inline XsdValidationState_t376578997 ** get_address_of_currentAutomata_5() { return &___currentAutomata_5; }
	inline void set_currentAutomata_5(XsdValidationState_t376578997 * value)
	{
		___currentAutomata_5 = value;
		Il2CppCodeGenWriteBarrier((&___currentAutomata_5), value);
	}

	inline static int32_t get_offset_of_emptiable_6() { return static_cast<int32_t>(offsetof(XsdSequenceValidationState_t429792968, ___emptiable_6)); }
	inline bool get_emptiable_6() const { return ___emptiable_6; }
	inline bool* get_address_of_emptiable_6() { return &___emptiable_6; }
	inline void set_emptiable_6(bool value)
	{
		___emptiable_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDSEQUENCEVALIDATIONSTATE_T429792968_H
#ifndef XSDELEMENTVALIDATIONSTATE_T2214590119_H
#define XSDELEMENTVALIDATIONSTATE_T2214590119_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdElementValidationState
struct  XsdElementValidationState_t2214590119  : public XsdValidationState_t376578997
{
public:
	// System.Xml.Schema.XmlSchemaElement Mono.Xml.Schema.XsdElementValidationState::element
	XmlSchemaElement_t427880856 * ___element_3;

public:
	inline static int32_t get_offset_of_element_3() { return static_cast<int32_t>(offsetof(XsdElementValidationState_t2214590119, ___element_3)); }
	inline XmlSchemaElement_t427880856 * get_element_3() const { return ___element_3; }
	inline XmlSchemaElement_t427880856 ** get_address_of_element_3() { return &___element_3; }
	inline void set_element_3(XmlSchemaElement_t427880856 * value)
	{
		___element_3 = value;
		Il2CppCodeGenWriteBarrier((&___element_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDELEMENTVALIDATIONSTATE_T2214590119_H
#ifndef XSDKEYENTRYFIELDCOLLECTION_T3698183622_H
#define XSDKEYENTRYFIELDCOLLECTION_T3698183622_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdKeyEntryFieldCollection
struct  XsdKeyEntryFieldCollection_t3698183622  : public CollectionBase_t2727926298
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDKEYENTRYFIELDCOLLECTION_T3698183622_H
#ifndef XSDKEYENTRYCOLLECTION_T3090959213_H
#define XSDKEYENTRYCOLLECTION_T3090959213_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdKeyEntryCollection
struct  XsdKeyEntryCollection_t3090959213  : public CollectionBase_t2727926298
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDKEYENTRYCOLLECTION_T3090959213_H
#ifndef SAMPLETYPE_T1208595618_H
#define SAMPLETYPE_T1208595618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UISystemProfilerApi/SampleType
struct  SampleType_t1208595618 
{
public:
	// System.Int32 UnityEngine.UISystemProfilerApi/SampleType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SampleType_t1208595618, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLETYPE_T1208595618_H
#ifndef ANALYTICSSESSIONSTATE_T681173134_H
#define ANALYTICSSESSIONSTATE_T681173134_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsSessionState
struct  AnalyticsSessionState_t681173134 
{
public:
	// System.Int32 UnityEngine.Analytics.AnalyticsSessionState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AnalyticsSessionState_t681173134, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSSESSIONSTATE_T681173134_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef XMLSCHEMACONTENTPROCESSING_T826201100_H
#define XMLSCHEMACONTENTPROCESSING_T826201100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaContentProcessing
struct  XmlSchemaContentProcessing_t826201100 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaContentProcessing::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlSchemaContentProcessing_t826201100, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMACONTENTPROCESSING_T826201100_H
#ifndef VALIDATIONTYPE_T4049928607_H
#define VALIDATIONTYPE_T4049928607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.ValidationType
struct  ValidationType_t4049928607 
{
public:
	// System.Int32 System.Xml.ValidationType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ValidationType_t4049928607, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALIDATIONTYPE_T4049928607_H
#ifndef FACET_T1501039206_H
#define FACET_T1501039206_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaFacet/Facet
struct  Facet_t1501039206 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaFacet/Facet::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Facet_t1501039206, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACET_T1501039206_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef XSDORDERING_T789960802_H
#define XSDORDERING_T789960802_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdOrdering
struct  XsdOrdering_t789960802 
{
public:
	// System.Int32 Mono.Xml.Schema.XsdOrdering::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XsdOrdering_t789960802, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDORDERING_T789960802_H
#ifndef XSDWHITESPACEFACET_T376308449_H
#define XSDWHITESPACEFACET_T376308449_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdWhitespaceFacet
struct  XsdWhitespaceFacet_t376308449 
{
public:
	// System.Int32 Mono.Xml.Schema.XsdWhitespaceFacet::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XsdWhitespaceFacet_t376308449, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDWHITESPACEFACET_T376308449_H
#ifndef XMLSCHEMADATATYPE_T322714710_H
#define XMLSCHEMADATATYPE_T322714710_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaDatatype
struct  XmlSchemaDatatype_t322714710  : public RuntimeObject
{
public:
	// Mono.Xml.Schema.XsdWhitespaceFacet System.Xml.Schema.XmlSchemaDatatype::WhitespaceValue
	int32_t ___WhitespaceValue_0;
	// System.Text.StringBuilder System.Xml.Schema.XmlSchemaDatatype::sb
	StringBuilder_t * ___sb_2;

public:
	inline static int32_t get_offset_of_WhitespaceValue_0() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t322714710, ___WhitespaceValue_0)); }
	inline int32_t get_WhitespaceValue_0() const { return ___WhitespaceValue_0; }
	inline int32_t* get_address_of_WhitespaceValue_0() { return &___WhitespaceValue_0; }
	inline void set_WhitespaceValue_0(int32_t value)
	{
		___WhitespaceValue_0 = value;
	}

	inline static int32_t get_offset_of_sb_2() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t322714710, ___sb_2)); }
	inline StringBuilder_t * get_sb_2() const { return ___sb_2; }
	inline StringBuilder_t ** get_address_of_sb_2() { return &___sb_2; }
	inline void set_sb_2(StringBuilder_t * value)
	{
		___sb_2 = value;
		Il2CppCodeGenWriteBarrier((&___sb_2), value);
	}
};

struct XmlSchemaDatatype_t322714710_StaticFields
{
public:
	// System.Char[] System.Xml.Schema.XmlSchemaDatatype::wsChars
	CharU5BU5D_t3528271667* ___wsChars_1;
	// Mono.Xml.Schema.XsdAnySimpleType System.Xml.Schema.XmlSchemaDatatype::datatypeAnySimpleType
	XsdAnySimpleType_t1257864485 * ___datatypeAnySimpleType_3;
	// Mono.Xml.Schema.XsdString System.Xml.Schema.XmlSchemaDatatype::datatypeString
	XsdString_t3049094358 * ___datatypeString_4;
	// Mono.Xml.Schema.XsdNormalizedString System.Xml.Schema.XmlSchemaDatatype::datatypeNormalizedString
	XsdNormalizedString_t3260789355 * ___datatypeNormalizedString_5;
	// Mono.Xml.Schema.XsdToken System.Xml.Schema.XmlSchemaDatatype::datatypeToken
	XsdToken_t1239036978 * ___datatypeToken_6;
	// Mono.Xml.Schema.XsdLanguage System.Xml.Schema.XmlSchemaDatatype::datatypeLanguage
	XsdLanguage_t1876291273 * ___datatypeLanguage_7;
	// Mono.Xml.Schema.XsdNMToken System.Xml.Schema.XmlSchemaDatatype::datatypeNMToken
	XsdNMToken_t834691671 * ___datatypeNMToken_8;
	// Mono.Xml.Schema.XsdNMTokens System.Xml.Schema.XmlSchemaDatatype::datatypeNMTokens
	XsdNMTokens_t4246953255 * ___datatypeNMTokens_9;
	// Mono.Xml.Schema.XsdName System.Xml.Schema.XmlSchemaDatatype::datatypeName
	XsdName_t2755146808 * ___datatypeName_10;
	// Mono.Xml.Schema.XsdNCName System.Xml.Schema.XmlSchemaDatatype::datatypeNCName
	XsdNCName_t3943159043 * ___datatypeNCName_11;
	// Mono.Xml.Schema.XsdID System.Xml.Schema.XmlSchemaDatatype::datatypeID
	XsdID_t34704195 * ___datatypeID_12;
	// Mono.Xml.Schema.XsdIDRef System.Xml.Schema.XmlSchemaDatatype::datatypeIDRef
	XsdIDRef_t2913612829 * ___datatypeIDRef_13;
	// Mono.Xml.Schema.XsdIDRefs System.Xml.Schema.XmlSchemaDatatype::datatypeIDRefs
	XsdIDRefs_t16099206 * ___datatypeIDRefs_14;
	// Mono.Xml.Schema.XsdEntity System.Xml.Schema.XmlSchemaDatatype::datatypeEntity
	XsdEntity_t3956505874 * ___datatypeEntity_15;
	// Mono.Xml.Schema.XsdEntities System.Xml.Schema.XmlSchemaDatatype::datatypeEntities
	XsdEntities_t1477210398 * ___datatypeEntities_16;
	// Mono.Xml.Schema.XsdNotation System.Xml.Schema.XmlSchemaDatatype::datatypeNotation
	XsdNotation_t2827634056 * ___datatypeNotation_17;
	// Mono.Xml.Schema.XsdDecimal System.Xml.Schema.XmlSchemaDatatype::datatypeDecimal
	XsdDecimal_t1288601093 * ___datatypeDecimal_18;
	// Mono.Xml.Schema.XsdInteger System.Xml.Schema.XmlSchemaDatatype::datatypeInteger
	XsdInteger_t2044766898 * ___datatypeInteger_19;
	// Mono.Xml.Schema.XsdLong System.Xml.Schema.XmlSchemaDatatype::datatypeLong
	XsdLong_t1324632828 * ___datatypeLong_20;
	// Mono.Xml.Schema.XsdInt System.Xml.Schema.XmlSchemaDatatype::datatypeInt
	XsdInt_t33917785 * ___datatypeInt_21;
	// Mono.Xml.Schema.XsdShort System.Xml.Schema.XmlSchemaDatatype::datatypeShort
	XsdShort_t3489811876 * ___datatypeShort_22;
	// Mono.Xml.Schema.XsdByte System.Xml.Schema.XmlSchemaDatatype::datatypeByte
	XsdByte_t2221093920 * ___datatypeByte_23;
	// Mono.Xml.Schema.XsdNonNegativeInteger System.Xml.Schema.XmlSchemaDatatype::datatypeNonNegativeInteger
	XsdNonNegativeInteger_t308064234 * ___datatypeNonNegativeInteger_24;
	// Mono.Xml.Schema.XsdPositiveInteger System.Xml.Schema.XmlSchemaDatatype::datatypePositiveInteger
	XsdPositiveInteger_t1704031413 * ___datatypePositiveInteger_25;
	// Mono.Xml.Schema.XsdUnsignedLong System.Xml.Schema.XmlSchemaDatatype::datatypeUnsignedLong
	XsdUnsignedLong_t1409593434 * ___datatypeUnsignedLong_26;
	// Mono.Xml.Schema.XsdUnsignedInt System.Xml.Schema.XmlSchemaDatatype::datatypeUnsignedInt
	XsdUnsignedInt_t72105793 * ___datatypeUnsignedInt_27;
	// Mono.Xml.Schema.XsdUnsignedShort System.Xml.Schema.XmlSchemaDatatype::datatypeUnsignedShort
	XsdUnsignedShort_t3654069686 * ___datatypeUnsignedShort_28;
	// Mono.Xml.Schema.XsdUnsignedByte System.Xml.Schema.XmlSchemaDatatype::datatypeUnsignedByte
	XsdUnsignedByte_t2304219558 * ___datatypeUnsignedByte_29;
	// Mono.Xml.Schema.XsdNonPositiveInteger System.Xml.Schema.XmlSchemaDatatype::datatypeNonPositiveInteger
	XsdNonPositiveInteger_t1029055398 * ___datatypeNonPositiveInteger_30;
	// Mono.Xml.Schema.XsdNegativeInteger System.Xml.Schema.XmlSchemaDatatype::datatypeNegativeInteger
	XsdNegativeInteger_t2178753546 * ___datatypeNegativeInteger_31;
	// Mono.Xml.Schema.XsdFloat System.Xml.Schema.XmlSchemaDatatype::datatypeFloat
	XsdFloat_t3181928905 * ___datatypeFloat_32;
	// Mono.Xml.Schema.XsdDouble System.Xml.Schema.XmlSchemaDatatype::datatypeDouble
	XsdDouble_t3324344982 * ___datatypeDouble_33;
	// Mono.Xml.Schema.XsdBase64Binary System.Xml.Schema.XmlSchemaDatatype::datatypeBase64Binary
	XsdBase64Binary_t3360383190 * ___datatypeBase64Binary_34;
	// Mono.Xml.Schema.XsdBoolean System.Xml.Schema.XmlSchemaDatatype::datatypeBoolean
	XsdBoolean_t380164876 * ___datatypeBoolean_35;
	// Mono.Xml.Schema.XsdAnyURI System.Xml.Schema.XmlSchemaDatatype::datatypeAnyURI
	XsdAnyURI_t2755748070 * ___datatypeAnyURI_36;
	// Mono.Xml.Schema.XsdDuration System.Xml.Schema.XmlSchemaDatatype::datatypeDuration
	XsdDuration_t1555973170 * ___datatypeDuration_37;
	// Mono.Xml.Schema.XsdDateTime System.Xml.Schema.XmlSchemaDatatype::datatypeDateTime
	XsdDateTime_t2563698975 * ___datatypeDateTime_38;
	// Mono.Xml.Schema.XsdDate System.Xml.Schema.XmlSchemaDatatype::datatypeDate
	XsdDate_t1417753656 * ___datatypeDate_39;
	// Mono.Xml.Schema.XsdTime System.Xml.Schema.XmlSchemaDatatype::datatypeTime
	XsdTime_t3558487088 * ___datatypeTime_40;
	// Mono.Xml.Schema.XsdHexBinary System.Xml.Schema.XmlSchemaDatatype::datatypeHexBinary
	XsdHexBinary_t882812470 * ___datatypeHexBinary_41;
	// Mono.Xml.Schema.XsdQName System.Xml.Schema.XmlSchemaDatatype::datatypeQName
	XsdQName_t2385631467 * ___datatypeQName_42;
	// Mono.Xml.Schema.XsdGYearMonth System.Xml.Schema.XmlSchemaDatatype::datatypeGYearMonth
	XsdGYearMonth_t3399073121 * ___datatypeGYearMonth_43;
	// Mono.Xml.Schema.XsdGMonthDay System.Xml.Schema.XmlSchemaDatatype::datatypeGMonthDay
	XsdGMonthDay_t2605134399 * ___datatypeGMonthDay_44;
	// Mono.Xml.Schema.XsdGYear System.Xml.Schema.XmlSchemaDatatype::datatypeGYear
	XsdGYear_t3316212116 * ___datatypeGYear_45;
	// Mono.Xml.Schema.XsdGMonth System.Xml.Schema.XmlSchemaDatatype::datatypeGMonth
	XsdGMonth_t3913018815 * ___datatypeGMonth_46;
	// Mono.Xml.Schema.XsdGDay System.Xml.Schema.XmlSchemaDatatype::datatypeGDay
	XsdGDay_t293490745 * ___datatypeGDay_47;
	// Mono.Xml.Schema.XdtAnyAtomicType System.Xml.Schema.XmlSchemaDatatype::datatypeAnyAtomicType
	XdtAnyAtomicType_t269366253 * ___datatypeAnyAtomicType_48;
	// Mono.Xml.Schema.XdtUntypedAtomic System.Xml.Schema.XmlSchemaDatatype::datatypeUntypedAtomic
	XdtUntypedAtomic_t1388131523 * ___datatypeUntypedAtomic_49;
	// Mono.Xml.Schema.XdtDayTimeDuration System.Xml.Schema.XmlSchemaDatatype::datatypeDayTimeDuration
	XdtDayTimeDuration_t268779858 * ___datatypeDayTimeDuration_50;
	// Mono.Xml.Schema.XdtYearMonthDuration System.Xml.Schema.XmlSchemaDatatype::datatypeYearMonthDuration
	XdtYearMonthDuration_t1503718519 * ___datatypeYearMonthDuration_51;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.Schema.XmlSchemaDatatype::<>f__switch$map2A
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map2A_52;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.Schema.XmlSchemaDatatype::<>f__switch$map2B
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map2B_53;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.Schema.XmlSchemaDatatype::<>f__switch$map2C
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map2C_54;

public:
	inline static int32_t get_offset_of_wsChars_1() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t322714710_StaticFields, ___wsChars_1)); }
	inline CharU5BU5D_t3528271667* get_wsChars_1() const { return ___wsChars_1; }
	inline CharU5BU5D_t3528271667** get_address_of_wsChars_1() { return &___wsChars_1; }
	inline void set_wsChars_1(CharU5BU5D_t3528271667* value)
	{
		___wsChars_1 = value;
		Il2CppCodeGenWriteBarrier((&___wsChars_1), value);
	}

	inline static int32_t get_offset_of_datatypeAnySimpleType_3() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t322714710_StaticFields, ___datatypeAnySimpleType_3)); }
	inline XsdAnySimpleType_t1257864485 * get_datatypeAnySimpleType_3() const { return ___datatypeAnySimpleType_3; }
	inline XsdAnySimpleType_t1257864485 ** get_address_of_datatypeAnySimpleType_3() { return &___datatypeAnySimpleType_3; }
	inline void set_datatypeAnySimpleType_3(XsdAnySimpleType_t1257864485 * value)
	{
		___datatypeAnySimpleType_3 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeAnySimpleType_3), value);
	}

	inline static int32_t get_offset_of_datatypeString_4() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t322714710_StaticFields, ___datatypeString_4)); }
	inline XsdString_t3049094358 * get_datatypeString_4() const { return ___datatypeString_4; }
	inline XsdString_t3049094358 ** get_address_of_datatypeString_4() { return &___datatypeString_4; }
	inline void set_datatypeString_4(XsdString_t3049094358 * value)
	{
		___datatypeString_4 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeString_4), value);
	}

	inline static int32_t get_offset_of_datatypeNormalizedString_5() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t322714710_StaticFields, ___datatypeNormalizedString_5)); }
	inline XsdNormalizedString_t3260789355 * get_datatypeNormalizedString_5() const { return ___datatypeNormalizedString_5; }
	inline XsdNormalizedString_t3260789355 ** get_address_of_datatypeNormalizedString_5() { return &___datatypeNormalizedString_5; }
	inline void set_datatypeNormalizedString_5(XsdNormalizedString_t3260789355 * value)
	{
		___datatypeNormalizedString_5 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeNormalizedString_5), value);
	}

	inline static int32_t get_offset_of_datatypeToken_6() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t322714710_StaticFields, ___datatypeToken_6)); }
	inline XsdToken_t1239036978 * get_datatypeToken_6() const { return ___datatypeToken_6; }
	inline XsdToken_t1239036978 ** get_address_of_datatypeToken_6() { return &___datatypeToken_6; }
	inline void set_datatypeToken_6(XsdToken_t1239036978 * value)
	{
		___datatypeToken_6 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeToken_6), value);
	}

	inline static int32_t get_offset_of_datatypeLanguage_7() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t322714710_StaticFields, ___datatypeLanguage_7)); }
	inline XsdLanguage_t1876291273 * get_datatypeLanguage_7() const { return ___datatypeLanguage_7; }
	inline XsdLanguage_t1876291273 ** get_address_of_datatypeLanguage_7() { return &___datatypeLanguage_7; }
	inline void set_datatypeLanguage_7(XsdLanguage_t1876291273 * value)
	{
		___datatypeLanguage_7 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeLanguage_7), value);
	}

	inline static int32_t get_offset_of_datatypeNMToken_8() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t322714710_StaticFields, ___datatypeNMToken_8)); }
	inline XsdNMToken_t834691671 * get_datatypeNMToken_8() const { return ___datatypeNMToken_8; }
	inline XsdNMToken_t834691671 ** get_address_of_datatypeNMToken_8() { return &___datatypeNMToken_8; }
	inline void set_datatypeNMToken_8(XsdNMToken_t834691671 * value)
	{
		___datatypeNMToken_8 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeNMToken_8), value);
	}

	inline static int32_t get_offset_of_datatypeNMTokens_9() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t322714710_StaticFields, ___datatypeNMTokens_9)); }
	inline XsdNMTokens_t4246953255 * get_datatypeNMTokens_9() const { return ___datatypeNMTokens_9; }
	inline XsdNMTokens_t4246953255 ** get_address_of_datatypeNMTokens_9() { return &___datatypeNMTokens_9; }
	inline void set_datatypeNMTokens_9(XsdNMTokens_t4246953255 * value)
	{
		___datatypeNMTokens_9 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeNMTokens_9), value);
	}

	inline static int32_t get_offset_of_datatypeName_10() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t322714710_StaticFields, ___datatypeName_10)); }
	inline XsdName_t2755146808 * get_datatypeName_10() const { return ___datatypeName_10; }
	inline XsdName_t2755146808 ** get_address_of_datatypeName_10() { return &___datatypeName_10; }
	inline void set_datatypeName_10(XsdName_t2755146808 * value)
	{
		___datatypeName_10 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeName_10), value);
	}

	inline static int32_t get_offset_of_datatypeNCName_11() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t322714710_StaticFields, ___datatypeNCName_11)); }
	inline XsdNCName_t3943159043 * get_datatypeNCName_11() const { return ___datatypeNCName_11; }
	inline XsdNCName_t3943159043 ** get_address_of_datatypeNCName_11() { return &___datatypeNCName_11; }
	inline void set_datatypeNCName_11(XsdNCName_t3943159043 * value)
	{
		___datatypeNCName_11 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeNCName_11), value);
	}

	inline static int32_t get_offset_of_datatypeID_12() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t322714710_StaticFields, ___datatypeID_12)); }
	inline XsdID_t34704195 * get_datatypeID_12() const { return ___datatypeID_12; }
	inline XsdID_t34704195 ** get_address_of_datatypeID_12() { return &___datatypeID_12; }
	inline void set_datatypeID_12(XsdID_t34704195 * value)
	{
		___datatypeID_12 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeID_12), value);
	}

	inline static int32_t get_offset_of_datatypeIDRef_13() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t322714710_StaticFields, ___datatypeIDRef_13)); }
	inline XsdIDRef_t2913612829 * get_datatypeIDRef_13() const { return ___datatypeIDRef_13; }
	inline XsdIDRef_t2913612829 ** get_address_of_datatypeIDRef_13() { return &___datatypeIDRef_13; }
	inline void set_datatypeIDRef_13(XsdIDRef_t2913612829 * value)
	{
		___datatypeIDRef_13 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeIDRef_13), value);
	}

	inline static int32_t get_offset_of_datatypeIDRefs_14() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t322714710_StaticFields, ___datatypeIDRefs_14)); }
	inline XsdIDRefs_t16099206 * get_datatypeIDRefs_14() const { return ___datatypeIDRefs_14; }
	inline XsdIDRefs_t16099206 ** get_address_of_datatypeIDRefs_14() { return &___datatypeIDRefs_14; }
	inline void set_datatypeIDRefs_14(XsdIDRefs_t16099206 * value)
	{
		___datatypeIDRefs_14 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeIDRefs_14), value);
	}

	inline static int32_t get_offset_of_datatypeEntity_15() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t322714710_StaticFields, ___datatypeEntity_15)); }
	inline XsdEntity_t3956505874 * get_datatypeEntity_15() const { return ___datatypeEntity_15; }
	inline XsdEntity_t3956505874 ** get_address_of_datatypeEntity_15() { return &___datatypeEntity_15; }
	inline void set_datatypeEntity_15(XsdEntity_t3956505874 * value)
	{
		___datatypeEntity_15 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeEntity_15), value);
	}

	inline static int32_t get_offset_of_datatypeEntities_16() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t322714710_StaticFields, ___datatypeEntities_16)); }
	inline XsdEntities_t1477210398 * get_datatypeEntities_16() const { return ___datatypeEntities_16; }
	inline XsdEntities_t1477210398 ** get_address_of_datatypeEntities_16() { return &___datatypeEntities_16; }
	inline void set_datatypeEntities_16(XsdEntities_t1477210398 * value)
	{
		___datatypeEntities_16 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeEntities_16), value);
	}

	inline static int32_t get_offset_of_datatypeNotation_17() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t322714710_StaticFields, ___datatypeNotation_17)); }
	inline XsdNotation_t2827634056 * get_datatypeNotation_17() const { return ___datatypeNotation_17; }
	inline XsdNotation_t2827634056 ** get_address_of_datatypeNotation_17() { return &___datatypeNotation_17; }
	inline void set_datatypeNotation_17(XsdNotation_t2827634056 * value)
	{
		___datatypeNotation_17 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeNotation_17), value);
	}

	inline static int32_t get_offset_of_datatypeDecimal_18() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t322714710_StaticFields, ___datatypeDecimal_18)); }
	inline XsdDecimal_t1288601093 * get_datatypeDecimal_18() const { return ___datatypeDecimal_18; }
	inline XsdDecimal_t1288601093 ** get_address_of_datatypeDecimal_18() { return &___datatypeDecimal_18; }
	inline void set_datatypeDecimal_18(XsdDecimal_t1288601093 * value)
	{
		___datatypeDecimal_18 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeDecimal_18), value);
	}

	inline static int32_t get_offset_of_datatypeInteger_19() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t322714710_StaticFields, ___datatypeInteger_19)); }
	inline XsdInteger_t2044766898 * get_datatypeInteger_19() const { return ___datatypeInteger_19; }
	inline XsdInteger_t2044766898 ** get_address_of_datatypeInteger_19() { return &___datatypeInteger_19; }
	inline void set_datatypeInteger_19(XsdInteger_t2044766898 * value)
	{
		___datatypeInteger_19 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeInteger_19), value);
	}

	inline static int32_t get_offset_of_datatypeLong_20() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t322714710_StaticFields, ___datatypeLong_20)); }
	inline XsdLong_t1324632828 * get_datatypeLong_20() const { return ___datatypeLong_20; }
	inline XsdLong_t1324632828 ** get_address_of_datatypeLong_20() { return &___datatypeLong_20; }
	inline void set_datatypeLong_20(XsdLong_t1324632828 * value)
	{
		___datatypeLong_20 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeLong_20), value);
	}

	inline static int32_t get_offset_of_datatypeInt_21() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t322714710_StaticFields, ___datatypeInt_21)); }
	inline XsdInt_t33917785 * get_datatypeInt_21() const { return ___datatypeInt_21; }
	inline XsdInt_t33917785 ** get_address_of_datatypeInt_21() { return &___datatypeInt_21; }
	inline void set_datatypeInt_21(XsdInt_t33917785 * value)
	{
		___datatypeInt_21 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeInt_21), value);
	}

	inline static int32_t get_offset_of_datatypeShort_22() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t322714710_StaticFields, ___datatypeShort_22)); }
	inline XsdShort_t3489811876 * get_datatypeShort_22() const { return ___datatypeShort_22; }
	inline XsdShort_t3489811876 ** get_address_of_datatypeShort_22() { return &___datatypeShort_22; }
	inline void set_datatypeShort_22(XsdShort_t3489811876 * value)
	{
		___datatypeShort_22 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeShort_22), value);
	}

	inline static int32_t get_offset_of_datatypeByte_23() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t322714710_StaticFields, ___datatypeByte_23)); }
	inline XsdByte_t2221093920 * get_datatypeByte_23() const { return ___datatypeByte_23; }
	inline XsdByte_t2221093920 ** get_address_of_datatypeByte_23() { return &___datatypeByte_23; }
	inline void set_datatypeByte_23(XsdByte_t2221093920 * value)
	{
		___datatypeByte_23 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeByte_23), value);
	}

	inline static int32_t get_offset_of_datatypeNonNegativeInteger_24() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t322714710_StaticFields, ___datatypeNonNegativeInteger_24)); }
	inline XsdNonNegativeInteger_t308064234 * get_datatypeNonNegativeInteger_24() const { return ___datatypeNonNegativeInteger_24; }
	inline XsdNonNegativeInteger_t308064234 ** get_address_of_datatypeNonNegativeInteger_24() { return &___datatypeNonNegativeInteger_24; }
	inline void set_datatypeNonNegativeInteger_24(XsdNonNegativeInteger_t308064234 * value)
	{
		___datatypeNonNegativeInteger_24 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeNonNegativeInteger_24), value);
	}

	inline static int32_t get_offset_of_datatypePositiveInteger_25() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t322714710_StaticFields, ___datatypePositiveInteger_25)); }
	inline XsdPositiveInteger_t1704031413 * get_datatypePositiveInteger_25() const { return ___datatypePositiveInteger_25; }
	inline XsdPositiveInteger_t1704031413 ** get_address_of_datatypePositiveInteger_25() { return &___datatypePositiveInteger_25; }
	inline void set_datatypePositiveInteger_25(XsdPositiveInteger_t1704031413 * value)
	{
		___datatypePositiveInteger_25 = value;
		Il2CppCodeGenWriteBarrier((&___datatypePositiveInteger_25), value);
	}

	inline static int32_t get_offset_of_datatypeUnsignedLong_26() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t322714710_StaticFields, ___datatypeUnsignedLong_26)); }
	inline XsdUnsignedLong_t1409593434 * get_datatypeUnsignedLong_26() const { return ___datatypeUnsignedLong_26; }
	inline XsdUnsignedLong_t1409593434 ** get_address_of_datatypeUnsignedLong_26() { return &___datatypeUnsignedLong_26; }
	inline void set_datatypeUnsignedLong_26(XsdUnsignedLong_t1409593434 * value)
	{
		___datatypeUnsignedLong_26 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeUnsignedLong_26), value);
	}

	inline static int32_t get_offset_of_datatypeUnsignedInt_27() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t322714710_StaticFields, ___datatypeUnsignedInt_27)); }
	inline XsdUnsignedInt_t72105793 * get_datatypeUnsignedInt_27() const { return ___datatypeUnsignedInt_27; }
	inline XsdUnsignedInt_t72105793 ** get_address_of_datatypeUnsignedInt_27() { return &___datatypeUnsignedInt_27; }
	inline void set_datatypeUnsignedInt_27(XsdUnsignedInt_t72105793 * value)
	{
		___datatypeUnsignedInt_27 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeUnsignedInt_27), value);
	}

	inline static int32_t get_offset_of_datatypeUnsignedShort_28() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t322714710_StaticFields, ___datatypeUnsignedShort_28)); }
	inline XsdUnsignedShort_t3654069686 * get_datatypeUnsignedShort_28() const { return ___datatypeUnsignedShort_28; }
	inline XsdUnsignedShort_t3654069686 ** get_address_of_datatypeUnsignedShort_28() { return &___datatypeUnsignedShort_28; }
	inline void set_datatypeUnsignedShort_28(XsdUnsignedShort_t3654069686 * value)
	{
		___datatypeUnsignedShort_28 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeUnsignedShort_28), value);
	}

	inline static int32_t get_offset_of_datatypeUnsignedByte_29() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t322714710_StaticFields, ___datatypeUnsignedByte_29)); }
	inline XsdUnsignedByte_t2304219558 * get_datatypeUnsignedByte_29() const { return ___datatypeUnsignedByte_29; }
	inline XsdUnsignedByte_t2304219558 ** get_address_of_datatypeUnsignedByte_29() { return &___datatypeUnsignedByte_29; }
	inline void set_datatypeUnsignedByte_29(XsdUnsignedByte_t2304219558 * value)
	{
		___datatypeUnsignedByte_29 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeUnsignedByte_29), value);
	}

	inline static int32_t get_offset_of_datatypeNonPositiveInteger_30() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t322714710_StaticFields, ___datatypeNonPositiveInteger_30)); }
	inline XsdNonPositiveInteger_t1029055398 * get_datatypeNonPositiveInteger_30() const { return ___datatypeNonPositiveInteger_30; }
	inline XsdNonPositiveInteger_t1029055398 ** get_address_of_datatypeNonPositiveInteger_30() { return &___datatypeNonPositiveInteger_30; }
	inline void set_datatypeNonPositiveInteger_30(XsdNonPositiveInteger_t1029055398 * value)
	{
		___datatypeNonPositiveInteger_30 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeNonPositiveInteger_30), value);
	}

	inline static int32_t get_offset_of_datatypeNegativeInteger_31() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t322714710_StaticFields, ___datatypeNegativeInteger_31)); }
	inline XsdNegativeInteger_t2178753546 * get_datatypeNegativeInteger_31() const { return ___datatypeNegativeInteger_31; }
	inline XsdNegativeInteger_t2178753546 ** get_address_of_datatypeNegativeInteger_31() { return &___datatypeNegativeInteger_31; }
	inline void set_datatypeNegativeInteger_31(XsdNegativeInteger_t2178753546 * value)
	{
		___datatypeNegativeInteger_31 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeNegativeInteger_31), value);
	}

	inline static int32_t get_offset_of_datatypeFloat_32() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t322714710_StaticFields, ___datatypeFloat_32)); }
	inline XsdFloat_t3181928905 * get_datatypeFloat_32() const { return ___datatypeFloat_32; }
	inline XsdFloat_t3181928905 ** get_address_of_datatypeFloat_32() { return &___datatypeFloat_32; }
	inline void set_datatypeFloat_32(XsdFloat_t3181928905 * value)
	{
		___datatypeFloat_32 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeFloat_32), value);
	}

	inline static int32_t get_offset_of_datatypeDouble_33() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t322714710_StaticFields, ___datatypeDouble_33)); }
	inline XsdDouble_t3324344982 * get_datatypeDouble_33() const { return ___datatypeDouble_33; }
	inline XsdDouble_t3324344982 ** get_address_of_datatypeDouble_33() { return &___datatypeDouble_33; }
	inline void set_datatypeDouble_33(XsdDouble_t3324344982 * value)
	{
		___datatypeDouble_33 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeDouble_33), value);
	}

	inline static int32_t get_offset_of_datatypeBase64Binary_34() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t322714710_StaticFields, ___datatypeBase64Binary_34)); }
	inline XsdBase64Binary_t3360383190 * get_datatypeBase64Binary_34() const { return ___datatypeBase64Binary_34; }
	inline XsdBase64Binary_t3360383190 ** get_address_of_datatypeBase64Binary_34() { return &___datatypeBase64Binary_34; }
	inline void set_datatypeBase64Binary_34(XsdBase64Binary_t3360383190 * value)
	{
		___datatypeBase64Binary_34 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeBase64Binary_34), value);
	}

	inline static int32_t get_offset_of_datatypeBoolean_35() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t322714710_StaticFields, ___datatypeBoolean_35)); }
	inline XsdBoolean_t380164876 * get_datatypeBoolean_35() const { return ___datatypeBoolean_35; }
	inline XsdBoolean_t380164876 ** get_address_of_datatypeBoolean_35() { return &___datatypeBoolean_35; }
	inline void set_datatypeBoolean_35(XsdBoolean_t380164876 * value)
	{
		___datatypeBoolean_35 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeBoolean_35), value);
	}

	inline static int32_t get_offset_of_datatypeAnyURI_36() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t322714710_StaticFields, ___datatypeAnyURI_36)); }
	inline XsdAnyURI_t2755748070 * get_datatypeAnyURI_36() const { return ___datatypeAnyURI_36; }
	inline XsdAnyURI_t2755748070 ** get_address_of_datatypeAnyURI_36() { return &___datatypeAnyURI_36; }
	inline void set_datatypeAnyURI_36(XsdAnyURI_t2755748070 * value)
	{
		___datatypeAnyURI_36 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeAnyURI_36), value);
	}

	inline static int32_t get_offset_of_datatypeDuration_37() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t322714710_StaticFields, ___datatypeDuration_37)); }
	inline XsdDuration_t1555973170 * get_datatypeDuration_37() const { return ___datatypeDuration_37; }
	inline XsdDuration_t1555973170 ** get_address_of_datatypeDuration_37() { return &___datatypeDuration_37; }
	inline void set_datatypeDuration_37(XsdDuration_t1555973170 * value)
	{
		___datatypeDuration_37 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeDuration_37), value);
	}

	inline static int32_t get_offset_of_datatypeDateTime_38() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t322714710_StaticFields, ___datatypeDateTime_38)); }
	inline XsdDateTime_t2563698975 * get_datatypeDateTime_38() const { return ___datatypeDateTime_38; }
	inline XsdDateTime_t2563698975 ** get_address_of_datatypeDateTime_38() { return &___datatypeDateTime_38; }
	inline void set_datatypeDateTime_38(XsdDateTime_t2563698975 * value)
	{
		___datatypeDateTime_38 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeDateTime_38), value);
	}

	inline static int32_t get_offset_of_datatypeDate_39() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t322714710_StaticFields, ___datatypeDate_39)); }
	inline XsdDate_t1417753656 * get_datatypeDate_39() const { return ___datatypeDate_39; }
	inline XsdDate_t1417753656 ** get_address_of_datatypeDate_39() { return &___datatypeDate_39; }
	inline void set_datatypeDate_39(XsdDate_t1417753656 * value)
	{
		___datatypeDate_39 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeDate_39), value);
	}

	inline static int32_t get_offset_of_datatypeTime_40() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t322714710_StaticFields, ___datatypeTime_40)); }
	inline XsdTime_t3558487088 * get_datatypeTime_40() const { return ___datatypeTime_40; }
	inline XsdTime_t3558487088 ** get_address_of_datatypeTime_40() { return &___datatypeTime_40; }
	inline void set_datatypeTime_40(XsdTime_t3558487088 * value)
	{
		___datatypeTime_40 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeTime_40), value);
	}

	inline static int32_t get_offset_of_datatypeHexBinary_41() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t322714710_StaticFields, ___datatypeHexBinary_41)); }
	inline XsdHexBinary_t882812470 * get_datatypeHexBinary_41() const { return ___datatypeHexBinary_41; }
	inline XsdHexBinary_t882812470 ** get_address_of_datatypeHexBinary_41() { return &___datatypeHexBinary_41; }
	inline void set_datatypeHexBinary_41(XsdHexBinary_t882812470 * value)
	{
		___datatypeHexBinary_41 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeHexBinary_41), value);
	}

	inline static int32_t get_offset_of_datatypeQName_42() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t322714710_StaticFields, ___datatypeQName_42)); }
	inline XsdQName_t2385631467 * get_datatypeQName_42() const { return ___datatypeQName_42; }
	inline XsdQName_t2385631467 ** get_address_of_datatypeQName_42() { return &___datatypeQName_42; }
	inline void set_datatypeQName_42(XsdQName_t2385631467 * value)
	{
		___datatypeQName_42 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeQName_42), value);
	}

	inline static int32_t get_offset_of_datatypeGYearMonth_43() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t322714710_StaticFields, ___datatypeGYearMonth_43)); }
	inline XsdGYearMonth_t3399073121 * get_datatypeGYearMonth_43() const { return ___datatypeGYearMonth_43; }
	inline XsdGYearMonth_t3399073121 ** get_address_of_datatypeGYearMonth_43() { return &___datatypeGYearMonth_43; }
	inline void set_datatypeGYearMonth_43(XsdGYearMonth_t3399073121 * value)
	{
		___datatypeGYearMonth_43 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeGYearMonth_43), value);
	}

	inline static int32_t get_offset_of_datatypeGMonthDay_44() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t322714710_StaticFields, ___datatypeGMonthDay_44)); }
	inline XsdGMonthDay_t2605134399 * get_datatypeGMonthDay_44() const { return ___datatypeGMonthDay_44; }
	inline XsdGMonthDay_t2605134399 ** get_address_of_datatypeGMonthDay_44() { return &___datatypeGMonthDay_44; }
	inline void set_datatypeGMonthDay_44(XsdGMonthDay_t2605134399 * value)
	{
		___datatypeGMonthDay_44 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeGMonthDay_44), value);
	}

	inline static int32_t get_offset_of_datatypeGYear_45() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t322714710_StaticFields, ___datatypeGYear_45)); }
	inline XsdGYear_t3316212116 * get_datatypeGYear_45() const { return ___datatypeGYear_45; }
	inline XsdGYear_t3316212116 ** get_address_of_datatypeGYear_45() { return &___datatypeGYear_45; }
	inline void set_datatypeGYear_45(XsdGYear_t3316212116 * value)
	{
		___datatypeGYear_45 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeGYear_45), value);
	}

	inline static int32_t get_offset_of_datatypeGMonth_46() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t322714710_StaticFields, ___datatypeGMonth_46)); }
	inline XsdGMonth_t3913018815 * get_datatypeGMonth_46() const { return ___datatypeGMonth_46; }
	inline XsdGMonth_t3913018815 ** get_address_of_datatypeGMonth_46() { return &___datatypeGMonth_46; }
	inline void set_datatypeGMonth_46(XsdGMonth_t3913018815 * value)
	{
		___datatypeGMonth_46 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeGMonth_46), value);
	}

	inline static int32_t get_offset_of_datatypeGDay_47() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t322714710_StaticFields, ___datatypeGDay_47)); }
	inline XsdGDay_t293490745 * get_datatypeGDay_47() const { return ___datatypeGDay_47; }
	inline XsdGDay_t293490745 ** get_address_of_datatypeGDay_47() { return &___datatypeGDay_47; }
	inline void set_datatypeGDay_47(XsdGDay_t293490745 * value)
	{
		___datatypeGDay_47 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeGDay_47), value);
	}

	inline static int32_t get_offset_of_datatypeAnyAtomicType_48() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t322714710_StaticFields, ___datatypeAnyAtomicType_48)); }
	inline XdtAnyAtomicType_t269366253 * get_datatypeAnyAtomicType_48() const { return ___datatypeAnyAtomicType_48; }
	inline XdtAnyAtomicType_t269366253 ** get_address_of_datatypeAnyAtomicType_48() { return &___datatypeAnyAtomicType_48; }
	inline void set_datatypeAnyAtomicType_48(XdtAnyAtomicType_t269366253 * value)
	{
		___datatypeAnyAtomicType_48 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeAnyAtomicType_48), value);
	}

	inline static int32_t get_offset_of_datatypeUntypedAtomic_49() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t322714710_StaticFields, ___datatypeUntypedAtomic_49)); }
	inline XdtUntypedAtomic_t1388131523 * get_datatypeUntypedAtomic_49() const { return ___datatypeUntypedAtomic_49; }
	inline XdtUntypedAtomic_t1388131523 ** get_address_of_datatypeUntypedAtomic_49() { return &___datatypeUntypedAtomic_49; }
	inline void set_datatypeUntypedAtomic_49(XdtUntypedAtomic_t1388131523 * value)
	{
		___datatypeUntypedAtomic_49 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeUntypedAtomic_49), value);
	}

	inline static int32_t get_offset_of_datatypeDayTimeDuration_50() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t322714710_StaticFields, ___datatypeDayTimeDuration_50)); }
	inline XdtDayTimeDuration_t268779858 * get_datatypeDayTimeDuration_50() const { return ___datatypeDayTimeDuration_50; }
	inline XdtDayTimeDuration_t268779858 ** get_address_of_datatypeDayTimeDuration_50() { return &___datatypeDayTimeDuration_50; }
	inline void set_datatypeDayTimeDuration_50(XdtDayTimeDuration_t268779858 * value)
	{
		___datatypeDayTimeDuration_50 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeDayTimeDuration_50), value);
	}

	inline static int32_t get_offset_of_datatypeYearMonthDuration_51() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t322714710_StaticFields, ___datatypeYearMonthDuration_51)); }
	inline XdtYearMonthDuration_t1503718519 * get_datatypeYearMonthDuration_51() const { return ___datatypeYearMonthDuration_51; }
	inline XdtYearMonthDuration_t1503718519 ** get_address_of_datatypeYearMonthDuration_51() { return &___datatypeYearMonthDuration_51; }
	inline void set_datatypeYearMonthDuration_51(XdtYearMonthDuration_t1503718519 * value)
	{
		___datatypeYearMonthDuration_51 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeYearMonthDuration_51), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map2A_52() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t322714710_StaticFields, ___U3CU3Ef__switchU24map2A_52)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map2A_52() const { return ___U3CU3Ef__switchU24map2A_52; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map2A_52() { return &___U3CU3Ef__switchU24map2A_52; }
	inline void set_U3CU3Ef__switchU24map2A_52(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map2A_52 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map2A_52), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map2B_53() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t322714710_StaticFields, ___U3CU3Ef__switchU24map2B_53)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map2B_53() const { return ___U3CU3Ef__switchU24map2B_53; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map2B_53() { return &___U3CU3Ef__switchU24map2B_53; }
	inline void set_U3CU3Ef__switchU24map2B_53(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map2B_53 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map2B_53), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map2C_54() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t322714710_StaticFields, ___U3CU3Ef__switchU24map2C_54)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map2C_54() const { return ___U3CU3Ef__switchU24map2C_54; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map2C_54() { return &___U3CU3Ef__switchU24map2C_54; }
	inline void set_U3CU3Ef__switchU24map2C_54(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map2C_54 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map2C_54), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMADATATYPE_T322714710_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef XSDWILDCARD_T2790389089_H
#define XSDWILDCARD_T2790389089_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdWildcard
struct  XsdWildcard_t2790389089  : public RuntimeObject
{
public:
	// System.Xml.Schema.XmlSchemaObject Mono.Xml.Schema.XsdWildcard::xsobj
	XmlSchemaObject_t1315720168 * ___xsobj_0;
	// System.Xml.Schema.XmlSchemaContentProcessing Mono.Xml.Schema.XsdWildcard::ResolvedProcessing
	int32_t ___ResolvedProcessing_1;
	// System.String Mono.Xml.Schema.XsdWildcard::TargetNamespace
	String_t* ___TargetNamespace_2;
	// System.Boolean Mono.Xml.Schema.XsdWildcard::SkipCompile
	bool ___SkipCompile_3;
	// System.Boolean Mono.Xml.Schema.XsdWildcard::HasValueAny
	bool ___HasValueAny_4;
	// System.Boolean Mono.Xml.Schema.XsdWildcard::HasValueLocal
	bool ___HasValueLocal_5;
	// System.Boolean Mono.Xml.Schema.XsdWildcard::HasValueOther
	bool ___HasValueOther_6;
	// System.Boolean Mono.Xml.Schema.XsdWildcard::HasValueTargetNamespace
	bool ___HasValueTargetNamespace_7;
	// System.Collections.Specialized.StringCollection Mono.Xml.Schema.XsdWildcard::ResolvedNamespaces
	StringCollection_t167406615 * ___ResolvedNamespaces_8;

public:
	inline static int32_t get_offset_of_xsobj_0() { return static_cast<int32_t>(offsetof(XsdWildcard_t2790389089, ___xsobj_0)); }
	inline XmlSchemaObject_t1315720168 * get_xsobj_0() const { return ___xsobj_0; }
	inline XmlSchemaObject_t1315720168 ** get_address_of_xsobj_0() { return &___xsobj_0; }
	inline void set_xsobj_0(XmlSchemaObject_t1315720168 * value)
	{
		___xsobj_0 = value;
		Il2CppCodeGenWriteBarrier((&___xsobj_0), value);
	}

	inline static int32_t get_offset_of_ResolvedProcessing_1() { return static_cast<int32_t>(offsetof(XsdWildcard_t2790389089, ___ResolvedProcessing_1)); }
	inline int32_t get_ResolvedProcessing_1() const { return ___ResolvedProcessing_1; }
	inline int32_t* get_address_of_ResolvedProcessing_1() { return &___ResolvedProcessing_1; }
	inline void set_ResolvedProcessing_1(int32_t value)
	{
		___ResolvedProcessing_1 = value;
	}

	inline static int32_t get_offset_of_TargetNamespace_2() { return static_cast<int32_t>(offsetof(XsdWildcard_t2790389089, ___TargetNamespace_2)); }
	inline String_t* get_TargetNamespace_2() const { return ___TargetNamespace_2; }
	inline String_t** get_address_of_TargetNamespace_2() { return &___TargetNamespace_2; }
	inline void set_TargetNamespace_2(String_t* value)
	{
		___TargetNamespace_2 = value;
		Il2CppCodeGenWriteBarrier((&___TargetNamespace_2), value);
	}

	inline static int32_t get_offset_of_SkipCompile_3() { return static_cast<int32_t>(offsetof(XsdWildcard_t2790389089, ___SkipCompile_3)); }
	inline bool get_SkipCompile_3() const { return ___SkipCompile_3; }
	inline bool* get_address_of_SkipCompile_3() { return &___SkipCompile_3; }
	inline void set_SkipCompile_3(bool value)
	{
		___SkipCompile_3 = value;
	}

	inline static int32_t get_offset_of_HasValueAny_4() { return static_cast<int32_t>(offsetof(XsdWildcard_t2790389089, ___HasValueAny_4)); }
	inline bool get_HasValueAny_4() const { return ___HasValueAny_4; }
	inline bool* get_address_of_HasValueAny_4() { return &___HasValueAny_4; }
	inline void set_HasValueAny_4(bool value)
	{
		___HasValueAny_4 = value;
	}

	inline static int32_t get_offset_of_HasValueLocal_5() { return static_cast<int32_t>(offsetof(XsdWildcard_t2790389089, ___HasValueLocal_5)); }
	inline bool get_HasValueLocal_5() const { return ___HasValueLocal_5; }
	inline bool* get_address_of_HasValueLocal_5() { return &___HasValueLocal_5; }
	inline void set_HasValueLocal_5(bool value)
	{
		___HasValueLocal_5 = value;
	}

	inline static int32_t get_offset_of_HasValueOther_6() { return static_cast<int32_t>(offsetof(XsdWildcard_t2790389089, ___HasValueOther_6)); }
	inline bool get_HasValueOther_6() const { return ___HasValueOther_6; }
	inline bool* get_address_of_HasValueOther_6() { return &___HasValueOther_6; }
	inline void set_HasValueOther_6(bool value)
	{
		___HasValueOther_6 = value;
	}

	inline static int32_t get_offset_of_HasValueTargetNamespace_7() { return static_cast<int32_t>(offsetof(XsdWildcard_t2790389089, ___HasValueTargetNamespace_7)); }
	inline bool get_HasValueTargetNamespace_7() const { return ___HasValueTargetNamespace_7; }
	inline bool* get_address_of_HasValueTargetNamespace_7() { return &___HasValueTargetNamespace_7; }
	inline void set_HasValueTargetNamespace_7(bool value)
	{
		___HasValueTargetNamespace_7 = value;
	}

	inline static int32_t get_offset_of_ResolvedNamespaces_8() { return static_cast<int32_t>(offsetof(XsdWildcard_t2790389089, ___ResolvedNamespaces_8)); }
	inline StringCollection_t167406615 * get_ResolvedNamespaces_8() const { return ___ResolvedNamespaces_8; }
	inline StringCollection_t167406615 ** get_address_of_ResolvedNamespaces_8() { return &___ResolvedNamespaces_8; }
	inline void set_ResolvedNamespaces_8(StringCollection_t167406615 * value)
	{
		___ResolvedNamespaces_8 = value;
		Il2CppCodeGenWriteBarrier((&___ResolvedNamespaces_8), value);
	}
};

struct XsdWildcard_t2790389089_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Schema.XsdWildcard::<>f__switch$map6
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map6_9;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map6_9() { return static_cast<int32_t>(offsetof(XsdWildcard_t2790389089_StaticFields, ___U3CU3Ef__switchU24map6_9)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map6_9() const { return ___U3CU3Ef__switchU24map6_9; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map6_9() { return &___U3CU3Ef__switchU24map6_9; }
	inline void set_U3CU3Ef__switchU24map6_9(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map6_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map6_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDWILDCARD_T2790389089_H
#ifndef XSDVALIDATINGREADER_T3961132625_H
#define XSDVALIDATINGREADER_T3961132625_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdValidatingReader
struct  XsdValidatingReader_t3961132625  : public XmlReader_t3121518892
{
public:
	// System.Xml.XmlReader Mono.Xml.Schema.XsdValidatingReader::reader
	XmlReader_t3121518892 * ___reader_3;
	// System.Xml.XmlResolver Mono.Xml.Schema.XsdValidatingReader::resolver
	XmlResolver_t626023767 * ___resolver_4;
	// Mono.Xml.IHasXmlSchemaInfo Mono.Xml.Schema.XsdValidatingReader::sourceReaderSchemaInfo
	RuntimeObject* ___sourceReaderSchemaInfo_5;
	// System.Xml.IXmlLineInfo Mono.Xml.Schema.XsdValidatingReader::readerLineInfo
	RuntimeObject* ___readerLineInfo_6;
	// System.Xml.ValidationType Mono.Xml.Schema.XsdValidatingReader::validationType
	int32_t ___validationType_7;
	// System.Xml.Schema.XmlSchemaSet Mono.Xml.Schema.XsdValidatingReader::schemas
	XmlSchemaSet_t266093086 * ___schemas_8;
	// System.Boolean Mono.Xml.Schema.XsdValidatingReader::namespaces
	bool ___namespaces_9;
	// System.Boolean Mono.Xml.Schema.XsdValidatingReader::validationStarted
	bool ___validationStarted_10;
	// System.Boolean Mono.Xml.Schema.XsdValidatingReader::checkIdentity
	bool ___checkIdentity_11;
	// Mono.Xml.Schema.XsdIDManager Mono.Xml.Schema.XsdValidatingReader::idManager
	XsdIDManager_t1008806102 * ___idManager_12;
	// System.Boolean Mono.Xml.Schema.XsdValidatingReader::checkKeyConstraints
	bool ___checkKeyConstraints_13;
	// System.Collections.ArrayList Mono.Xml.Schema.XsdValidatingReader::keyTables
	ArrayList_t2718874744 * ___keyTables_14;
	// System.Collections.ArrayList Mono.Xml.Schema.XsdValidatingReader::currentKeyFieldConsumers
	ArrayList_t2718874744 * ___currentKeyFieldConsumers_15;
	// System.Collections.ArrayList Mono.Xml.Schema.XsdValidatingReader::tmpKeyrefPool
	ArrayList_t2718874744 * ___tmpKeyrefPool_16;
	// System.Collections.ArrayList Mono.Xml.Schema.XsdValidatingReader::elementQNameStack
	ArrayList_t2718874744 * ___elementQNameStack_17;
	// Mono.Xml.Schema.XsdParticleStateManager Mono.Xml.Schema.XsdValidatingReader::state
	XsdParticleStateManager_t726654767 * ___state_18;
	// System.Int32 Mono.Xml.Schema.XsdValidatingReader::skipValidationDepth
	int32_t ___skipValidationDepth_19;
	// System.Int32 Mono.Xml.Schema.XsdValidatingReader::xsiNilDepth
	int32_t ___xsiNilDepth_20;
	// System.Text.StringBuilder Mono.Xml.Schema.XsdValidatingReader::storedCharacters
	StringBuilder_t * ___storedCharacters_21;
	// System.Boolean Mono.Xml.Schema.XsdValidatingReader::shouldValidateCharacters
	bool ___shouldValidateCharacters_22;
	// System.Xml.Schema.XmlSchemaAttribute[] Mono.Xml.Schema.XsdValidatingReader::defaultAttributes
	XmlSchemaAttributeU5BU5D_t346244693* ___defaultAttributes_23;
	// System.Int32 Mono.Xml.Schema.XsdValidatingReader::currentDefaultAttribute
	int32_t ___currentDefaultAttribute_24;
	// System.Collections.ArrayList Mono.Xml.Schema.XsdValidatingReader::defaultAttributesCache
	ArrayList_t2718874744 * ___defaultAttributesCache_25;
	// System.Boolean Mono.Xml.Schema.XsdValidatingReader::defaultAttributeConsumed
	bool ___defaultAttributeConsumed_26;
	// System.Object Mono.Xml.Schema.XsdValidatingReader::currentAttrType
	RuntimeObject * ___currentAttrType_27;
	// System.Xml.Schema.ValidationEventHandler Mono.Xml.Schema.XsdValidatingReader::ValidationEventHandler
	ValidationEventHandler_t791314227 * ___ValidationEventHandler_28;

public:
	inline static int32_t get_offset_of_reader_3() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t3961132625, ___reader_3)); }
	inline XmlReader_t3121518892 * get_reader_3() const { return ___reader_3; }
	inline XmlReader_t3121518892 ** get_address_of_reader_3() { return &___reader_3; }
	inline void set_reader_3(XmlReader_t3121518892 * value)
	{
		___reader_3 = value;
		Il2CppCodeGenWriteBarrier((&___reader_3), value);
	}

	inline static int32_t get_offset_of_resolver_4() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t3961132625, ___resolver_4)); }
	inline XmlResolver_t626023767 * get_resolver_4() const { return ___resolver_4; }
	inline XmlResolver_t626023767 ** get_address_of_resolver_4() { return &___resolver_4; }
	inline void set_resolver_4(XmlResolver_t626023767 * value)
	{
		___resolver_4 = value;
		Il2CppCodeGenWriteBarrier((&___resolver_4), value);
	}

	inline static int32_t get_offset_of_sourceReaderSchemaInfo_5() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t3961132625, ___sourceReaderSchemaInfo_5)); }
	inline RuntimeObject* get_sourceReaderSchemaInfo_5() const { return ___sourceReaderSchemaInfo_5; }
	inline RuntimeObject** get_address_of_sourceReaderSchemaInfo_5() { return &___sourceReaderSchemaInfo_5; }
	inline void set_sourceReaderSchemaInfo_5(RuntimeObject* value)
	{
		___sourceReaderSchemaInfo_5 = value;
		Il2CppCodeGenWriteBarrier((&___sourceReaderSchemaInfo_5), value);
	}

	inline static int32_t get_offset_of_readerLineInfo_6() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t3961132625, ___readerLineInfo_6)); }
	inline RuntimeObject* get_readerLineInfo_6() const { return ___readerLineInfo_6; }
	inline RuntimeObject** get_address_of_readerLineInfo_6() { return &___readerLineInfo_6; }
	inline void set_readerLineInfo_6(RuntimeObject* value)
	{
		___readerLineInfo_6 = value;
		Il2CppCodeGenWriteBarrier((&___readerLineInfo_6), value);
	}

	inline static int32_t get_offset_of_validationType_7() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t3961132625, ___validationType_7)); }
	inline int32_t get_validationType_7() const { return ___validationType_7; }
	inline int32_t* get_address_of_validationType_7() { return &___validationType_7; }
	inline void set_validationType_7(int32_t value)
	{
		___validationType_7 = value;
	}

	inline static int32_t get_offset_of_schemas_8() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t3961132625, ___schemas_8)); }
	inline XmlSchemaSet_t266093086 * get_schemas_8() const { return ___schemas_8; }
	inline XmlSchemaSet_t266093086 ** get_address_of_schemas_8() { return &___schemas_8; }
	inline void set_schemas_8(XmlSchemaSet_t266093086 * value)
	{
		___schemas_8 = value;
		Il2CppCodeGenWriteBarrier((&___schemas_8), value);
	}

	inline static int32_t get_offset_of_namespaces_9() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t3961132625, ___namespaces_9)); }
	inline bool get_namespaces_9() const { return ___namespaces_9; }
	inline bool* get_address_of_namespaces_9() { return &___namespaces_9; }
	inline void set_namespaces_9(bool value)
	{
		___namespaces_9 = value;
	}

	inline static int32_t get_offset_of_validationStarted_10() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t3961132625, ___validationStarted_10)); }
	inline bool get_validationStarted_10() const { return ___validationStarted_10; }
	inline bool* get_address_of_validationStarted_10() { return &___validationStarted_10; }
	inline void set_validationStarted_10(bool value)
	{
		___validationStarted_10 = value;
	}

	inline static int32_t get_offset_of_checkIdentity_11() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t3961132625, ___checkIdentity_11)); }
	inline bool get_checkIdentity_11() const { return ___checkIdentity_11; }
	inline bool* get_address_of_checkIdentity_11() { return &___checkIdentity_11; }
	inline void set_checkIdentity_11(bool value)
	{
		___checkIdentity_11 = value;
	}

	inline static int32_t get_offset_of_idManager_12() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t3961132625, ___idManager_12)); }
	inline XsdIDManager_t1008806102 * get_idManager_12() const { return ___idManager_12; }
	inline XsdIDManager_t1008806102 ** get_address_of_idManager_12() { return &___idManager_12; }
	inline void set_idManager_12(XsdIDManager_t1008806102 * value)
	{
		___idManager_12 = value;
		Il2CppCodeGenWriteBarrier((&___idManager_12), value);
	}

	inline static int32_t get_offset_of_checkKeyConstraints_13() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t3961132625, ___checkKeyConstraints_13)); }
	inline bool get_checkKeyConstraints_13() const { return ___checkKeyConstraints_13; }
	inline bool* get_address_of_checkKeyConstraints_13() { return &___checkKeyConstraints_13; }
	inline void set_checkKeyConstraints_13(bool value)
	{
		___checkKeyConstraints_13 = value;
	}

	inline static int32_t get_offset_of_keyTables_14() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t3961132625, ___keyTables_14)); }
	inline ArrayList_t2718874744 * get_keyTables_14() const { return ___keyTables_14; }
	inline ArrayList_t2718874744 ** get_address_of_keyTables_14() { return &___keyTables_14; }
	inline void set_keyTables_14(ArrayList_t2718874744 * value)
	{
		___keyTables_14 = value;
		Il2CppCodeGenWriteBarrier((&___keyTables_14), value);
	}

	inline static int32_t get_offset_of_currentKeyFieldConsumers_15() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t3961132625, ___currentKeyFieldConsumers_15)); }
	inline ArrayList_t2718874744 * get_currentKeyFieldConsumers_15() const { return ___currentKeyFieldConsumers_15; }
	inline ArrayList_t2718874744 ** get_address_of_currentKeyFieldConsumers_15() { return &___currentKeyFieldConsumers_15; }
	inline void set_currentKeyFieldConsumers_15(ArrayList_t2718874744 * value)
	{
		___currentKeyFieldConsumers_15 = value;
		Il2CppCodeGenWriteBarrier((&___currentKeyFieldConsumers_15), value);
	}

	inline static int32_t get_offset_of_tmpKeyrefPool_16() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t3961132625, ___tmpKeyrefPool_16)); }
	inline ArrayList_t2718874744 * get_tmpKeyrefPool_16() const { return ___tmpKeyrefPool_16; }
	inline ArrayList_t2718874744 ** get_address_of_tmpKeyrefPool_16() { return &___tmpKeyrefPool_16; }
	inline void set_tmpKeyrefPool_16(ArrayList_t2718874744 * value)
	{
		___tmpKeyrefPool_16 = value;
		Il2CppCodeGenWriteBarrier((&___tmpKeyrefPool_16), value);
	}

	inline static int32_t get_offset_of_elementQNameStack_17() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t3961132625, ___elementQNameStack_17)); }
	inline ArrayList_t2718874744 * get_elementQNameStack_17() const { return ___elementQNameStack_17; }
	inline ArrayList_t2718874744 ** get_address_of_elementQNameStack_17() { return &___elementQNameStack_17; }
	inline void set_elementQNameStack_17(ArrayList_t2718874744 * value)
	{
		___elementQNameStack_17 = value;
		Il2CppCodeGenWriteBarrier((&___elementQNameStack_17), value);
	}

	inline static int32_t get_offset_of_state_18() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t3961132625, ___state_18)); }
	inline XsdParticleStateManager_t726654767 * get_state_18() const { return ___state_18; }
	inline XsdParticleStateManager_t726654767 ** get_address_of_state_18() { return &___state_18; }
	inline void set_state_18(XsdParticleStateManager_t726654767 * value)
	{
		___state_18 = value;
		Il2CppCodeGenWriteBarrier((&___state_18), value);
	}

	inline static int32_t get_offset_of_skipValidationDepth_19() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t3961132625, ___skipValidationDepth_19)); }
	inline int32_t get_skipValidationDepth_19() const { return ___skipValidationDepth_19; }
	inline int32_t* get_address_of_skipValidationDepth_19() { return &___skipValidationDepth_19; }
	inline void set_skipValidationDepth_19(int32_t value)
	{
		___skipValidationDepth_19 = value;
	}

	inline static int32_t get_offset_of_xsiNilDepth_20() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t3961132625, ___xsiNilDepth_20)); }
	inline int32_t get_xsiNilDepth_20() const { return ___xsiNilDepth_20; }
	inline int32_t* get_address_of_xsiNilDepth_20() { return &___xsiNilDepth_20; }
	inline void set_xsiNilDepth_20(int32_t value)
	{
		___xsiNilDepth_20 = value;
	}

	inline static int32_t get_offset_of_storedCharacters_21() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t3961132625, ___storedCharacters_21)); }
	inline StringBuilder_t * get_storedCharacters_21() const { return ___storedCharacters_21; }
	inline StringBuilder_t ** get_address_of_storedCharacters_21() { return &___storedCharacters_21; }
	inline void set_storedCharacters_21(StringBuilder_t * value)
	{
		___storedCharacters_21 = value;
		Il2CppCodeGenWriteBarrier((&___storedCharacters_21), value);
	}

	inline static int32_t get_offset_of_shouldValidateCharacters_22() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t3961132625, ___shouldValidateCharacters_22)); }
	inline bool get_shouldValidateCharacters_22() const { return ___shouldValidateCharacters_22; }
	inline bool* get_address_of_shouldValidateCharacters_22() { return &___shouldValidateCharacters_22; }
	inline void set_shouldValidateCharacters_22(bool value)
	{
		___shouldValidateCharacters_22 = value;
	}

	inline static int32_t get_offset_of_defaultAttributes_23() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t3961132625, ___defaultAttributes_23)); }
	inline XmlSchemaAttributeU5BU5D_t346244693* get_defaultAttributes_23() const { return ___defaultAttributes_23; }
	inline XmlSchemaAttributeU5BU5D_t346244693** get_address_of_defaultAttributes_23() { return &___defaultAttributes_23; }
	inline void set_defaultAttributes_23(XmlSchemaAttributeU5BU5D_t346244693* value)
	{
		___defaultAttributes_23 = value;
		Il2CppCodeGenWriteBarrier((&___defaultAttributes_23), value);
	}

	inline static int32_t get_offset_of_currentDefaultAttribute_24() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t3961132625, ___currentDefaultAttribute_24)); }
	inline int32_t get_currentDefaultAttribute_24() const { return ___currentDefaultAttribute_24; }
	inline int32_t* get_address_of_currentDefaultAttribute_24() { return &___currentDefaultAttribute_24; }
	inline void set_currentDefaultAttribute_24(int32_t value)
	{
		___currentDefaultAttribute_24 = value;
	}

	inline static int32_t get_offset_of_defaultAttributesCache_25() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t3961132625, ___defaultAttributesCache_25)); }
	inline ArrayList_t2718874744 * get_defaultAttributesCache_25() const { return ___defaultAttributesCache_25; }
	inline ArrayList_t2718874744 ** get_address_of_defaultAttributesCache_25() { return &___defaultAttributesCache_25; }
	inline void set_defaultAttributesCache_25(ArrayList_t2718874744 * value)
	{
		___defaultAttributesCache_25 = value;
		Il2CppCodeGenWriteBarrier((&___defaultAttributesCache_25), value);
	}

	inline static int32_t get_offset_of_defaultAttributeConsumed_26() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t3961132625, ___defaultAttributeConsumed_26)); }
	inline bool get_defaultAttributeConsumed_26() const { return ___defaultAttributeConsumed_26; }
	inline bool* get_address_of_defaultAttributeConsumed_26() { return &___defaultAttributeConsumed_26; }
	inline void set_defaultAttributeConsumed_26(bool value)
	{
		___defaultAttributeConsumed_26 = value;
	}

	inline static int32_t get_offset_of_currentAttrType_27() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t3961132625, ___currentAttrType_27)); }
	inline RuntimeObject * get_currentAttrType_27() const { return ___currentAttrType_27; }
	inline RuntimeObject ** get_address_of_currentAttrType_27() { return &___currentAttrType_27; }
	inline void set_currentAttrType_27(RuntimeObject * value)
	{
		___currentAttrType_27 = value;
		Il2CppCodeGenWriteBarrier((&___currentAttrType_27), value);
	}

	inline static int32_t get_offset_of_ValidationEventHandler_28() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t3961132625, ___ValidationEventHandler_28)); }
	inline ValidationEventHandler_t791314227 * get_ValidationEventHandler_28() const { return ___ValidationEventHandler_28; }
	inline ValidationEventHandler_t791314227 ** get_address_of_ValidationEventHandler_28() { return &___ValidationEventHandler_28; }
	inline void set_ValidationEventHandler_28(ValidationEventHandler_t791314227 * value)
	{
		___ValidationEventHandler_28 = value;
		Il2CppCodeGenWriteBarrier((&___ValidationEventHandler_28), value);
	}
};

struct XsdValidatingReader_t3961132625_StaticFields
{
public:
	// System.Xml.Schema.XmlSchemaAttribute[] Mono.Xml.Schema.XsdValidatingReader::emptyAttributeArray
	XmlSchemaAttributeU5BU5D_t346244693* ___emptyAttributeArray_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Schema.XsdValidatingReader::<>f__switch$map3
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map3_29;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Schema.XsdValidatingReader::<>f__switch$map4
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map4_30;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Schema.XsdValidatingReader::<>f__switch$map5
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map5_31;

public:
	inline static int32_t get_offset_of_emptyAttributeArray_2() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t3961132625_StaticFields, ___emptyAttributeArray_2)); }
	inline XmlSchemaAttributeU5BU5D_t346244693* get_emptyAttributeArray_2() const { return ___emptyAttributeArray_2; }
	inline XmlSchemaAttributeU5BU5D_t346244693** get_address_of_emptyAttributeArray_2() { return &___emptyAttributeArray_2; }
	inline void set_emptyAttributeArray_2(XmlSchemaAttributeU5BU5D_t346244693* value)
	{
		___emptyAttributeArray_2 = value;
		Il2CppCodeGenWriteBarrier((&___emptyAttributeArray_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map3_29() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t3961132625_StaticFields, ___U3CU3Ef__switchU24map3_29)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map3_29() const { return ___U3CU3Ef__switchU24map3_29; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map3_29() { return &___U3CU3Ef__switchU24map3_29; }
	inline void set_U3CU3Ef__switchU24map3_29(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map3_29 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map3_29), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map4_30() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t3961132625_StaticFields, ___U3CU3Ef__switchU24map4_30)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map4_30() const { return ___U3CU3Ef__switchU24map4_30; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map4_30() { return &___U3CU3Ef__switchU24map4_30; }
	inline void set_U3CU3Ef__switchU24map4_30(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map4_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map4_30), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map5_31() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t3961132625_StaticFields, ___U3CU3Ef__switchU24map5_31)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map5_31() const { return ___U3CU3Ef__switchU24map5_31; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map5_31() { return &___U3CU3Ef__switchU24map5_31; }
	inline void set_U3CU3Ef__switchU24map5_31(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map5_31 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map5_31), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDVALIDATINGREADER_T3961132625_H
#ifndef XSDPARTICLESTATEMANAGER_T726654767_H
#define XSDPARTICLESTATEMANAGER_T726654767_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdParticleStateManager
struct  XsdParticleStateManager_t726654767  : public RuntimeObject
{
public:
	// System.Collections.Hashtable Mono.Xml.Schema.XsdParticleStateManager::table
	Hashtable_t1853889766 * ___table_0;
	// System.Xml.Schema.XmlSchemaContentProcessing Mono.Xml.Schema.XsdParticleStateManager::processContents
	int32_t ___processContents_1;
	// System.Xml.Schema.XmlSchemaElement Mono.Xml.Schema.XsdParticleStateManager::CurrentElement
	XmlSchemaElement_t427880856 * ___CurrentElement_2;
	// System.Collections.Stack Mono.Xml.Schema.XsdParticleStateManager::ContextStack
	Stack_t2329662280 * ___ContextStack_3;
	// Mono.Xml.Schema.XsdValidationContext Mono.Xml.Schema.XsdParticleStateManager::Context
	XsdValidationContext_t1104170526 * ___Context_4;

public:
	inline static int32_t get_offset_of_table_0() { return static_cast<int32_t>(offsetof(XsdParticleStateManager_t726654767, ___table_0)); }
	inline Hashtable_t1853889766 * get_table_0() const { return ___table_0; }
	inline Hashtable_t1853889766 ** get_address_of_table_0() { return &___table_0; }
	inline void set_table_0(Hashtable_t1853889766 * value)
	{
		___table_0 = value;
		Il2CppCodeGenWriteBarrier((&___table_0), value);
	}

	inline static int32_t get_offset_of_processContents_1() { return static_cast<int32_t>(offsetof(XsdParticleStateManager_t726654767, ___processContents_1)); }
	inline int32_t get_processContents_1() const { return ___processContents_1; }
	inline int32_t* get_address_of_processContents_1() { return &___processContents_1; }
	inline void set_processContents_1(int32_t value)
	{
		___processContents_1 = value;
	}

	inline static int32_t get_offset_of_CurrentElement_2() { return static_cast<int32_t>(offsetof(XsdParticleStateManager_t726654767, ___CurrentElement_2)); }
	inline XmlSchemaElement_t427880856 * get_CurrentElement_2() const { return ___CurrentElement_2; }
	inline XmlSchemaElement_t427880856 ** get_address_of_CurrentElement_2() { return &___CurrentElement_2; }
	inline void set_CurrentElement_2(XmlSchemaElement_t427880856 * value)
	{
		___CurrentElement_2 = value;
		Il2CppCodeGenWriteBarrier((&___CurrentElement_2), value);
	}

	inline static int32_t get_offset_of_ContextStack_3() { return static_cast<int32_t>(offsetof(XsdParticleStateManager_t726654767, ___ContextStack_3)); }
	inline Stack_t2329662280 * get_ContextStack_3() const { return ___ContextStack_3; }
	inline Stack_t2329662280 ** get_address_of_ContextStack_3() { return &___ContextStack_3; }
	inline void set_ContextStack_3(Stack_t2329662280 * value)
	{
		___ContextStack_3 = value;
		Il2CppCodeGenWriteBarrier((&___ContextStack_3), value);
	}

	inline static int32_t get_offset_of_Context_4() { return static_cast<int32_t>(offsetof(XsdParticleStateManager_t726654767, ___Context_4)); }
	inline XsdValidationContext_t1104170526 * get_Context_4() const { return ___Context_4; }
	inline XsdValidationContext_t1104170526 ** get_address_of_Context_4() { return &___Context_4; }
	inline void set_Context_4(XsdValidationContext_t1104170526 * value)
	{
		___Context_4 = value;
		Il2CppCodeGenWriteBarrier((&___Context_4), value);
	}
};

struct XsdParticleStateManager_t726654767_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Schema.XsdParticleStateManager::<>f__switch$map2
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map2_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map2_5() { return static_cast<int32_t>(offsetof(XsdParticleStateManager_t726654767_StaticFields, ___U3CU3Ef__switchU24map2_5)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map2_5() const { return ___U3CU3Ef__switchU24map2_5; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map2_5() { return &___U3CU3Ef__switchU24map2_5; }
	inline void set_U3CU3Ef__switchU24map2_5(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map2_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDPARTICLESTATEMANAGER_T726654767_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef CANVASGROUP_T4083511760_H
#define CANVASGROUP_T4083511760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CanvasGroup
struct  CanvasGroup_t4083511760  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVASGROUP_T4083511760_H
#ifndef WILLRENDERCANVASES_T3309123499_H
#define WILLRENDERCANVASES_T3309123499_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Canvas/WillRenderCanvases
struct  WillRenderCanvases_t3309123499  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WILLRENDERCANVASES_T3309123499_H
#ifndef SESSIONSTATECHANGED_T3163629820_H
#define SESSIONSTATECHANGED_T3163629820_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsSessionInfo/SessionStateChanged
struct  SessionStateChanged_t3163629820  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SESSIONSTATECHANGED_T3163629820_H
#ifndef CANVASRENDERER_T2598313366_H
#define CANVASRENDERER_T2598313366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CanvasRenderer
struct  CanvasRenderer_t2598313366  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVASRENDERER_T2598313366_H
#ifndef UPDATEDEVENTHANDLER_T1027848393_H
#define UPDATEDEVENTHANDLER_T1027848393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RemoteSettings/UpdatedEventHandler
struct  UpdatedEventHandler_t1027848393  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATEDEVENTHANDLER_T1027848393_H
#ifndef XSDANYSIMPLETYPE_T1257864485_H
#define XSDANYSIMPLETYPE_T1257864485_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdAnySimpleType
struct  XsdAnySimpleType_t1257864485  : public XmlSchemaDatatype_t322714710
{
public:

public:
};

struct XsdAnySimpleType_t1257864485_StaticFields
{
public:
	// Mono.Xml.Schema.XsdAnySimpleType Mono.Xml.Schema.XsdAnySimpleType::instance
	XsdAnySimpleType_t1257864485 * ___instance_55;
	// System.Char[] Mono.Xml.Schema.XsdAnySimpleType::whitespaceArray
	CharU5BU5D_t3528271667* ___whitespaceArray_56;
	// System.Xml.Schema.XmlSchemaFacet/Facet Mono.Xml.Schema.XsdAnySimpleType::booleanAllowedFacets
	int32_t ___booleanAllowedFacets_57;
	// System.Xml.Schema.XmlSchemaFacet/Facet Mono.Xml.Schema.XsdAnySimpleType::decimalAllowedFacets
	int32_t ___decimalAllowedFacets_58;
	// System.Xml.Schema.XmlSchemaFacet/Facet Mono.Xml.Schema.XsdAnySimpleType::durationAllowedFacets
	int32_t ___durationAllowedFacets_59;
	// System.Xml.Schema.XmlSchemaFacet/Facet Mono.Xml.Schema.XsdAnySimpleType::stringAllowedFacets
	int32_t ___stringAllowedFacets_60;

public:
	inline static int32_t get_offset_of_instance_55() { return static_cast<int32_t>(offsetof(XsdAnySimpleType_t1257864485_StaticFields, ___instance_55)); }
	inline XsdAnySimpleType_t1257864485 * get_instance_55() const { return ___instance_55; }
	inline XsdAnySimpleType_t1257864485 ** get_address_of_instance_55() { return &___instance_55; }
	inline void set_instance_55(XsdAnySimpleType_t1257864485 * value)
	{
		___instance_55 = value;
		Il2CppCodeGenWriteBarrier((&___instance_55), value);
	}

	inline static int32_t get_offset_of_whitespaceArray_56() { return static_cast<int32_t>(offsetof(XsdAnySimpleType_t1257864485_StaticFields, ___whitespaceArray_56)); }
	inline CharU5BU5D_t3528271667* get_whitespaceArray_56() const { return ___whitespaceArray_56; }
	inline CharU5BU5D_t3528271667** get_address_of_whitespaceArray_56() { return &___whitespaceArray_56; }
	inline void set_whitespaceArray_56(CharU5BU5D_t3528271667* value)
	{
		___whitespaceArray_56 = value;
		Il2CppCodeGenWriteBarrier((&___whitespaceArray_56), value);
	}

	inline static int32_t get_offset_of_booleanAllowedFacets_57() { return static_cast<int32_t>(offsetof(XsdAnySimpleType_t1257864485_StaticFields, ___booleanAllowedFacets_57)); }
	inline int32_t get_booleanAllowedFacets_57() const { return ___booleanAllowedFacets_57; }
	inline int32_t* get_address_of_booleanAllowedFacets_57() { return &___booleanAllowedFacets_57; }
	inline void set_booleanAllowedFacets_57(int32_t value)
	{
		___booleanAllowedFacets_57 = value;
	}

	inline static int32_t get_offset_of_decimalAllowedFacets_58() { return static_cast<int32_t>(offsetof(XsdAnySimpleType_t1257864485_StaticFields, ___decimalAllowedFacets_58)); }
	inline int32_t get_decimalAllowedFacets_58() const { return ___decimalAllowedFacets_58; }
	inline int32_t* get_address_of_decimalAllowedFacets_58() { return &___decimalAllowedFacets_58; }
	inline void set_decimalAllowedFacets_58(int32_t value)
	{
		___decimalAllowedFacets_58 = value;
	}

	inline static int32_t get_offset_of_durationAllowedFacets_59() { return static_cast<int32_t>(offsetof(XsdAnySimpleType_t1257864485_StaticFields, ___durationAllowedFacets_59)); }
	inline int32_t get_durationAllowedFacets_59() const { return ___durationAllowedFacets_59; }
	inline int32_t* get_address_of_durationAllowedFacets_59() { return &___durationAllowedFacets_59; }
	inline void set_durationAllowedFacets_59(int32_t value)
	{
		___durationAllowedFacets_59 = value;
	}

	inline static int32_t get_offset_of_stringAllowedFacets_60() { return static_cast<int32_t>(offsetof(XsdAnySimpleType_t1257864485_StaticFields, ___stringAllowedFacets_60)); }
	inline int32_t get_stringAllowedFacets_60() const { return ___stringAllowedFacets_60; }
	inline int32_t* get_address_of_stringAllowedFacets_60() { return &___stringAllowedFacets_60; }
	inline void set_stringAllowedFacets_60(int32_t value)
	{
		___stringAllowedFacets_60 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDANYSIMPLETYPE_T1257864485_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef XSDGDAY_T293490745_H
#define XSDGDAY_T293490745_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdGDay
struct  XsdGDay_t293490745  : public XsdAnySimpleType_t1257864485
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDGDAY_T293490745_H
#ifndef XSDGMONTH_T3913018815_H
#define XSDGMONTH_T3913018815_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdGMonth
struct  XsdGMonth_t3913018815  : public XsdAnySimpleType_t1257864485
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDGMONTH_T3913018815_H
#ifndef XSDGYEAR_T3316212116_H
#define XSDGYEAR_T3316212116_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdGYear
struct  XsdGYear_t3316212116  : public XsdAnySimpleType_t1257864485
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDGYEAR_T3316212116_H
#ifndef XSDGMONTHDAY_T2605134399_H
#define XSDGMONTHDAY_T2605134399_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdGMonthDay
struct  XsdGMonthDay_t2605134399  : public XsdAnySimpleType_t1257864485
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDGMONTHDAY_T2605134399_H
#ifndef XSDGYEARMONTH_T3399073121_H
#define XSDGYEARMONTH_T3399073121_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdGYearMonth
struct  XsdGYearMonth_t3399073121  : public XsdAnySimpleType_t1257864485
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDGYEARMONTH_T3399073121_H
#ifndef XSDTIME_T3558487088_H
#define XSDTIME_T3558487088_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdTime
struct  XsdTime_t3558487088  : public XsdAnySimpleType_t1257864485
{
public:

public:
};

struct XsdTime_t3558487088_StaticFields
{
public:
	// System.String[] Mono.Xml.Schema.XsdTime::timeFormats
	StringU5BU5D_t1281789340* ___timeFormats_61;

public:
	inline static int32_t get_offset_of_timeFormats_61() { return static_cast<int32_t>(offsetof(XsdTime_t3558487088_StaticFields, ___timeFormats_61)); }
	inline StringU5BU5D_t1281789340* get_timeFormats_61() const { return ___timeFormats_61; }
	inline StringU5BU5D_t1281789340** get_address_of_timeFormats_61() { return &___timeFormats_61; }
	inline void set_timeFormats_61(StringU5BU5D_t1281789340* value)
	{
		___timeFormats_61 = value;
		Il2CppCodeGenWriteBarrier((&___timeFormats_61), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDTIME_T3558487088_H
#ifndef XSDDATE_T1417753656_H
#define XSDDATE_T1417753656_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdDate
struct  XsdDate_t1417753656  : public XsdAnySimpleType_t1257864485
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDDATE_T1417753656_H
#ifndef XSDDATETIME_T2563698975_H
#define XSDDATETIME_T2563698975_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdDateTime
struct  XsdDateTime_t2563698975  : public XsdAnySimpleType_t1257864485
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDDATETIME_T2563698975_H
#ifndef XSDDURATION_T1555973170_H
#define XSDDURATION_T1555973170_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdDuration
struct  XsdDuration_t1555973170  : public XsdAnySimpleType_t1257864485
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDDURATION_T1555973170_H
#ifndef XSDBOOLEAN_T380164876_H
#define XSDBOOLEAN_T380164876_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdBoolean
struct  XsdBoolean_t380164876  : public XsdAnySimpleType_t1257864485
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDBOOLEAN_T380164876_H
#ifndef CANVAS_T3310196443_H
#define CANVAS_T3310196443_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Canvas
struct  Canvas_t3310196443  : public Behaviour_t1437897464
{
public:

public:
};

struct Canvas_t3310196443_StaticFields
{
public:
	// UnityEngine.Canvas/WillRenderCanvases UnityEngine.Canvas::willRenderCanvases
	WillRenderCanvases_t3309123499 * ___willRenderCanvases_2;

public:
	inline static int32_t get_offset_of_willRenderCanvases_2() { return static_cast<int32_t>(offsetof(Canvas_t3310196443_StaticFields, ___willRenderCanvases_2)); }
	inline WillRenderCanvases_t3309123499 * get_willRenderCanvases_2() const { return ___willRenderCanvases_2; }
	inline WillRenderCanvases_t3309123499 ** get_address_of_willRenderCanvases_2() { return &___willRenderCanvases_2; }
	inline void set_willRenderCanvases_2(WillRenderCanvases_t3309123499 * value)
	{
		___willRenderCanvases_2 = value;
		Il2CppCodeGenWriteBarrier((&___willRenderCanvases_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVAS_T3310196443_H
#ifndef XSDHEXBINARY_T882812470_H
#define XSDHEXBINARY_T882812470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdHexBinary
struct  XsdHexBinary_t882812470  : public XsdAnySimpleType_t1257864485
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDHEXBINARY_T882812470_H
#ifndef XSDDOUBLE_T3324344982_H
#define XSDDOUBLE_T3324344982_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdDouble
struct  XsdDouble_t3324344982  : public XsdAnySimpleType_t1257864485
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDDOUBLE_T3324344982_H
#ifndef XSDFLOAT_T3181928905_H
#define XSDFLOAT_T3181928905_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdFloat
struct  XsdFloat_t3181928905  : public XsdAnySimpleType_t1257864485
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDFLOAT_T3181928905_H
#ifndef XDTANYATOMICTYPE_T269366253_H
#define XDTANYATOMICTYPE_T269366253_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XdtAnyAtomicType
struct  XdtAnyAtomicType_t269366253  : public XsdAnySimpleType_t1257864485
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XDTANYATOMICTYPE_T269366253_H
#ifndef XSDSTRING_T3049094358_H
#define XSDSTRING_T3049094358_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdString
struct  XsdString_t3049094358  : public XsdAnySimpleType_t1257864485
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDSTRING_T3049094358_H
#ifndef XSDNOTATION_T2827634056_H
#define XSDNOTATION_T2827634056_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdNotation
struct  XsdNotation_t2827634056  : public XsdAnySimpleType_t1257864485
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDNOTATION_T2827634056_H
#ifndef XSDDECIMAL_T1288601093_H
#define XSDDECIMAL_T1288601093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdDecimal
struct  XsdDecimal_t1288601093  : public XsdAnySimpleType_t1257864485
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDDECIMAL_T1288601093_H
#ifndef XSDANYURI_T2755748070_H
#define XSDANYURI_T2755748070_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdAnyURI
struct  XsdAnyURI_t2755748070  : public XsdString_t3049094358
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDANYURI_T2755748070_H
#ifndef XSDBASE64BINARY_T3360383190_H
#define XSDBASE64BINARY_T3360383190_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdBase64Binary
struct  XsdBase64Binary_t3360383190  : public XsdString_t3049094358
{
public:

public:
};

struct XsdBase64Binary_t3360383190_StaticFields
{
public:
	// System.String Mono.Xml.Schema.XsdBase64Binary::ALPHABET
	String_t* ___ALPHABET_61;
	// System.Byte[] Mono.Xml.Schema.XsdBase64Binary::decodeTable
	ByteU5BU5D_t4116647657* ___decodeTable_62;

public:
	inline static int32_t get_offset_of_ALPHABET_61() { return static_cast<int32_t>(offsetof(XsdBase64Binary_t3360383190_StaticFields, ___ALPHABET_61)); }
	inline String_t* get_ALPHABET_61() const { return ___ALPHABET_61; }
	inline String_t** get_address_of_ALPHABET_61() { return &___ALPHABET_61; }
	inline void set_ALPHABET_61(String_t* value)
	{
		___ALPHABET_61 = value;
		Il2CppCodeGenWriteBarrier((&___ALPHABET_61), value);
	}

	inline static int32_t get_offset_of_decodeTable_62() { return static_cast<int32_t>(offsetof(XsdBase64Binary_t3360383190_StaticFields, ___decodeTable_62)); }
	inline ByteU5BU5D_t4116647657* get_decodeTable_62() const { return ___decodeTable_62; }
	inline ByteU5BU5D_t4116647657** get_address_of_decodeTable_62() { return &___decodeTable_62; }
	inline void set_decodeTable_62(ByteU5BU5D_t4116647657* value)
	{
		___decodeTable_62 = value;
		Il2CppCodeGenWriteBarrier((&___decodeTable_62), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDBASE64BINARY_T3360383190_H
#ifndef XDTDAYTIMEDURATION_T268779858_H
#define XDTDAYTIMEDURATION_T268779858_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XdtDayTimeDuration
struct  XdtDayTimeDuration_t268779858  : public XsdDuration_t1555973170
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XDTDAYTIMEDURATION_T268779858_H
#ifndef XDTYEARMONTHDURATION_T1503718519_H
#define XDTYEARMONTHDURATION_T1503718519_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XdtYearMonthDuration
struct  XdtYearMonthDuration_t1503718519  : public XsdDuration_t1555973170
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XDTYEARMONTHDURATION_T1503718519_H
#ifndef XDTUNTYPEDATOMIC_T1388131523_H
#define XDTUNTYPEDATOMIC_T1388131523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XdtUntypedAtomic
struct  XdtUntypedAtomic_t1388131523  : public XdtAnyAtomicType_t269366253
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XDTUNTYPEDATOMIC_T1388131523_H
#ifndef XSDNORMALIZEDSTRING_T3260789355_H
#define XSDNORMALIZEDSTRING_T3260789355_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdNormalizedString
struct  XsdNormalizedString_t3260789355  : public XsdString_t3049094358
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDNORMALIZEDSTRING_T3260789355_H
#ifndef XSDINTEGER_T2044766898_H
#define XSDINTEGER_T2044766898_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdInteger
struct  XsdInteger_t2044766898  : public XsdDecimal_t1288601093
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDINTEGER_T2044766898_H
#ifndef XSDTOKEN_T1239036978_H
#define XSDTOKEN_T1239036978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdToken
struct  XsdToken_t1239036978  : public XsdNormalizedString_t3260789355
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDTOKEN_T1239036978_H
#ifndef XSDLONG_T1324632828_H
#define XSDLONG_T1324632828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdLong
struct  XsdLong_t1324632828  : public XsdInteger_t2044766898
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDLONG_T1324632828_H
#ifndef XSDNONNEGATIVEINTEGER_T308064234_H
#define XSDNONNEGATIVEINTEGER_T308064234_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdNonNegativeInteger
struct  XsdNonNegativeInteger_t308064234  : public XsdInteger_t2044766898
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDNONNEGATIVEINTEGER_T308064234_H
#ifndef XSDNONPOSITIVEINTEGER_T1029055398_H
#define XSDNONPOSITIVEINTEGER_T1029055398_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdNonPositiveInteger
struct  XsdNonPositiveInteger_t1029055398  : public XsdInteger_t2044766898
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDNONPOSITIVEINTEGER_T1029055398_H
#ifndef XSDINT_T33917785_H
#define XSDINT_T33917785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdInt
struct  XsdInt_t33917785  : public XsdLong_t1324632828
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDINT_T33917785_H
#ifndef XSDNEGATIVEINTEGER_T2178753546_H
#define XSDNEGATIVEINTEGER_T2178753546_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdNegativeInteger
struct  XsdNegativeInteger_t2178753546  : public XsdNonPositiveInteger_t1029055398
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDNEGATIVEINTEGER_T2178753546_H
#ifndef XSDUNSIGNEDLONG_T1409593434_H
#define XSDUNSIGNEDLONG_T1409593434_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdUnsignedLong
struct  XsdUnsignedLong_t1409593434  : public XsdNonNegativeInteger_t308064234
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDUNSIGNEDLONG_T1409593434_H
#ifndef XSDPOSITIVEINTEGER_T1704031413_H
#define XSDPOSITIVEINTEGER_T1704031413_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdPositiveInteger
struct  XsdPositiveInteger_t1704031413  : public XsdNonNegativeInteger_t308064234
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDPOSITIVEINTEGER_T1704031413_H
#ifndef XSDLANGUAGE_T1876291273_H
#define XSDLANGUAGE_T1876291273_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdLanguage
struct  XsdLanguage_t1876291273  : public XsdToken_t1239036978
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDLANGUAGE_T1876291273_H
#ifndef XSDNMTOKEN_T834691671_H
#define XSDNMTOKEN_T834691671_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdNMToken
struct  XsdNMToken_t834691671  : public XsdToken_t1239036978
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDNMTOKEN_T834691671_H
#ifndef XSDNAME_T2755146808_H
#define XSDNAME_T2755146808_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdName
struct  XsdName_t2755146808  : public XsdToken_t1239036978
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDNAME_T2755146808_H
#ifndef XSDSHORT_T3489811876_H
#define XSDSHORT_T3489811876_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdShort
struct  XsdShort_t3489811876  : public XsdInt_t33917785
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDSHORT_T3489811876_H
#ifndef XSDNMTOKENS_T4246953255_H
#define XSDNMTOKENS_T4246953255_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdNMTokens
struct  XsdNMTokens_t4246953255  : public XsdNMToken_t834691671
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDNMTOKENS_T4246953255_H
#ifndef XSDNCNAME_T3943159043_H
#define XSDNCNAME_T3943159043_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdNCName
struct  XsdNCName_t3943159043  : public XsdName_t2755146808
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDNCNAME_T3943159043_H
#ifndef XSDID_T34704195_H
#define XSDID_T34704195_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdID
struct  XsdID_t34704195  : public XsdName_t2755146808
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDID_T34704195_H
#ifndef XSDIDREF_T2913612829_H
#define XSDIDREF_T2913612829_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdIDRef
struct  XsdIDRef_t2913612829  : public XsdName_t2755146808
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDIDREF_T2913612829_H
#ifndef XSDIDREFS_T16099206_H
#define XSDIDREFS_T16099206_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdIDRefs
struct  XsdIDRefs_t16099206  : public XsdName_t2755146808
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDIDREFS_T16099206_H
#ifndef XSDENTITY_T3956505874_H
#define XSDENTITY_T3956505874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdEntity
struct  XsdEntity_t3956505874  : public XsdName_t2755146808
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDENTITY_T3956505874_H
#ifndef XSDUNSIGNEDINT_T72105793_H
#define XSDUNSIGNEDINT_T72105793_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdUnsignedInt
struct  XsdUnsignedInt_t72105793  : public XsdUnsignedLong_t1409593434
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDUNSIGNEDINT_T72105793_H
#ifndef XSDQNAME_T2385631467_H
#define XSDQNAME_T2385631467_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdQName
struct  XsdQName_t2385631467  : public XsdName_t2755146808
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDQNAME_T2385631467_H
#ifndef XSDENTITIES_T1477210398_H
#define XSDENTITIES_T1477210398_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdEntities
struct  XsdEntities_t1477210398  : public XsdName_t2755146808
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDENTITIES_T1477210398_H
#ifndef XSDUNSIGNEDSHORT_T3654069686_H
#define XSDUNSIGNEDSHORT_T3654069686_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdUnsignedShort
struct  XsdUnsignedShort_t3654069686  : public XsdUnsignedInt_t72105793
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDUNSIGNEDSHORT_T3654069686_H
#ifndef XSDBYTE_T2221093920_H
#define XSDBYTE_T2221093920_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdByte
struct  XsdByte_t2221093920  : public XsdShort_t3489811876
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDBYTE_T2221093920_H
#ifndef XSDUNSIGNEDBYTE_T2304219558_H
#define XSDUNSIGNEDBYTE_T2304219558_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdUnsignedByte
struct  XsdUnsignedByte_t2304219558  : public XsdUnsignedShort_t3654069686
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDUNSIGNEDBYTE_T2304219558_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1800 = { sizeof (Canvas_t3310196443), -1, sizeof(Canvas_t3310196443_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1800[1] = 
{
	Canvas_t3310196443_StaticFields::get_offset_of_willRenderCanvases_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1801 = { sizeof (WillRenderCanvases_t3309123499), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1802 = { sizeof (UISystemProfilerApi_t2230074258), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1803 = { sizeof (SampleType_t1208595618)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1803[3] = 
{
	SampleType_t1208595618::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1804 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1805 = { sizeof (CanvasGroup_t4083511760), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1806 = { sizeof (CanvasRenderer_t2598313366), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1807 = { sizeof (RectTransformUtility_t1743242446), -1, sizeof(RectTransformUtility_t1743242446_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1807[1] = 
{
	RectTransformUtility_t1743242446_StaticFields::get_offset_of_s_Corners_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1808 = { sizeof (U3CModuleU3E_t692745543), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1809 = { sizeof (RemoteSettings_t1718627291), -1, sizeof(RemoteSettings_t1718627291_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1809[1] = 
{
	RemoteSettings_t1718627291_StaticFields::get_offset_of_Updated_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1810 = { sizeof (UpdatedEventHandler_t1027848393), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1811 = { sizeof (AnalyticsSessionState_t681173134)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1811[5] = 
{
	AnalyticsSessionState_t681173134::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1812 = { sizeof (AnalyticsSessionInfo_t2322308579), -1, sizeof(AnalyticsSessionInfo_t2322308579_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1812[1] = 
{
	AnalyticsSessionInfo_t2322308579_StaticFields::get_offset_of_sessionStateChanged_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1813 = { sizeof (SessionStateChanged_t3163629820), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1814 = { sizeof (U3CModuleU3E_t692745544), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1815 = { sizeof (WWW_t3688466362), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1815[1] = 
{
	WWW_t3688466362::get_offset_of__uwr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1816 = { sizeof (U3CModuleU3E_t692745545), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1817 = { sizeof (U3CModuleU3E_t692745546), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1818 = { sizeof (MonoTODOAttribute_t4131080584), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1819 = { sizeof (XsdIdentitySelector_t574258590), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1819[3] = 
{
	XsdIdentitySelector_t574258590::get_offset_of_selectorPaths_0(),
	XsdIdentitySelector_t574258590::get_offset_of_fields_1(),
	XsdIdentitySelector_t574258590::get_offset_of_cachedFields_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1820 = { sizeof (XsdIdentityField_t1964115728), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1820[2] = 
{
	XsdIdentityField_t1964115728::get_offset_of_fieldPaths_0(),
	XsdIdentityField_t1964115728::get_offset_of_index_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1821 = { sizeof (XsdIdentityPath_t991900844), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1821[2] = 
{
	XsdIdentityPath_t991900844::get_offset_of_OrderedSteps_0(),
	XsdIdentityPath_t991900844::get_offset_of_Descendants_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1822 = { sizeof (XsdIdentityStep_t1480907129), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1822[6] = 
{
	XsdIdentityStep_t1480907129::get_offset_of_IsCurrent_0(),
	XsdIdentityStep_t1480907129::get_offset_of_IsAttribute_1(),
	XsdIdentityStep_t1480907129::get_offset_of_IsAnyName_2(),
	XsdIdentityStep_t1480907129::get_offset_of_NsName_3(),
	XsdIdentityStep_t1480907129::get_offset_of_Name_4(),
	XsdIdentityStep_t1480907129::get_offset_of_Namespace_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1823 = { sizeof (XsdKeyEntryField_t3552275292), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1823[13] = 
{
	XsdKeyEntryField_t3552275292::get_offset_of_entry_0(),
	XsdKeyEntryField_t3552275292::get_offset_of_field_1(),
	XsdKeyEntryField_t3552275292::get_offset_of_FieldFound_2(),
	XsdKeyEntryField_t3552275292::get_offset_of_FieldLineNumber_3(),
	XsdKeyEntryField_t3552275292::get_offset_of_FieldLinePosition_4(),
	XsdKeyEntryField_t3552275292::get_offset_of_FieldHasLineInfo_5(),
	XsdKeyEntryField_t3552275292::get_offset_of_FieldType_6(),
	XsdKeyEntryField_t3552275292::get_offset_of_Identity_7(),
	XsdKeyEntryField_t3552275292::get_offset_of_IsXsiNil_8(),
	XsdKeyEntryField_t3552275292::get_offset_of_FieldFoundDepth_9(),
	XsdKeyEntryField_t3552275292::get_offset_of_FieldFoundPath_10(),
	XsdKeyEntryField_t3552275292::get_offset_of_Consuming_11(),
	XsdKeyEntryField_t3552275292::get_offset_of_Consumed_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1824 = { sizeof (XsdKeyEntryFieldCollection_t3698183622), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1825 = { sizeof (XsdKeyEntry_t693496666), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1825[8] = 
{
	XsdKeyEntry_t693496666::get_offset_of_StartDepth_0(),
	XsdKeyEntry_t693496666::get_offset_of_SelectorLineNumber_1(),
	XsdKeyEntry_t693496666::get_offset_of_SelectorLinePosition_2(),
	XsdKeyEntry_t693496666::get_offset_of_SelectorHasLineInfo_3(),
	XsdKeyEntry_t693496666::get_offset_of_KeyFields_4(),
	XsdKeyEntry_t693496666::get_offset_of_KeyRefFound_5(),
	XsdKeyEntry_t693496666::get_offset_of_OwnerSequence_6(),
	XsdKeyEntry_t693496666::get_offset_of_keyFound_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1826 = { sizeof (XsdKeyEntryCollection_t3090959213), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1827 = { sizeof (XsdKeyTable_t2156891743), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1827[9] = 
{
	XsdKeyTable_t2156891743::get_offset_of_alwaysTrue_0(),
	XsdKeyTable_t2156891743::get_offset_of_selector_1(),
	XsdKeyTable_t2156891743::get_offset_of_source_2(),
	XsdKeyTable_t2156891743::get_offset_of_qname_3(),
	XsdKeyTable_t2156891743::get_offset_of_refKeyName_4(),
	XsdKeyTable_t2156891743::get_offset_of_Entries_5(),
	XsdKeyTable_t2156891743::get_offset_of_FinishedEntries_6(),
	XsdKeyTable_t2156891743::get_offset_of_StartDepth_7(),
	XsdKeyTable_t2156891743::get_offset_of_ReferencedKey_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1828 = { sizeof (XsdParticleStateManager_t726654767), -1, sizeof(XsdParticleStateManager_t726654767_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1828[6] = 
{
	XsdParticleStateManager_t726654767::get_offset_of_table_0(),
	XsdParticleStateManager_t726654767::get_offset_of_processContents_1(),
	XsdParticleStateManager_t726654767::get_offset_of_CurrentElement_2(),
	XsdParticleStateManager_t726654767::get_offset_of_ContextStack_3(),
	XsdParticleStateManager_t726654767::get_offset_of_Context_4(),
	XsdParticleStateManager_t726654767_StaticFields::get_offset_of_U3CU3Ef__switchU24map2_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1829 = { sizeof (XsdValidationState_t376578997), -1, sizeof(XsdValidationState_t376578997_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1829[3] = 
{
	XsdValidationState_t376578997_StaticFields::get_offset_of_invalid_0(),
	XsdValidationState_t376578997::get_offset_of_occured_1(),
	XsdValidationState_t376578997::get_offset_of_manager_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1830 = { sizeof (XsdElementValidationState_t2214590119), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1830[1] = 
{
	XsdElementValidationState_t2214590119::get_offset_of_element_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1831 = { sizeof (XsdSequenceValidationState_t429792968), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1831[4] = 
{
	XsdSequenceValidationState_t429792968::get_offset_of_seq_3(),
	XsdSequenceValidationState_t429792968::get_offset_of_current_4(),
	XsdSequenceValidationState_t429792968::get_offset_of_currentAutomata_5(),
	XsdSequenceValidationState_t429792968::get_offset_of_emptiable_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1832 = { sizeof (XsdChoiceValidationState_t2566230191), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1832[3] = 
{
	XsdChoiceValidationState_t2566230191::get_offset_of_choice_3(),
	XsdChoiceValidationState_t2566230191::get_offset_of_emptiable_4(),
	XsdChoiceValidationState_t2566230191::get_offset_of_emptiableComputed_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1833 = { sizeof (XsdAllValidationState_t2703884157), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1833[2] = 
{
	XsdAllValidationState_t2703884157::get_offset_of_all_3(),
	XsdAllValidationState_t2703884157::get_offset_of_consumed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1834 = { sizeof (XsdAnyValidationState_t3421545252), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1834[1] = 
{
	XsdAnyValidationState_t3421545252::get_offset_of_any_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1835 = { sizeof (XsdAppendedValidationState_t3608891238), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1835[2] = 
{
	XsdAppendedValidationState_t3608891238::get_offset_of_head_3(),
	XsdAppendedValidationState_t3608891238::get_offset_of_rest_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1836 = { sizeof (XsdEmptyValidationState_t1344146143), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1837 = { sizeof (XsdInvalidValidationState_t3749995458), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1838 = { sizeof (XsdValidatingReader_t3961132625), -1, sizeof(XsdValidatingReader_t3961132625_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1838[30] = 
{
	XsdValidatingReader_t3961132625_StaticFields::get_offset_of_emptyAttributeArray_2(),
	XsdValidatingReader_t3961132625::get_offset_of_reader_3(),
	XsdValidatingReader_t3961132625::get_offset_of_resolver_4(),
	XsdValidatingReader_t3961132625::get_offset_of_sourceReaderSchemaInfo_5(),
	XsdValidatingReader_t3961132625::get_offset_of_readerLineInfo_6(),
	XsdValidatingReader_t3961132625::get_offset_of_validationType_7(),
	XsdValidatingReader_t3961132625::get_offset_of_schemas_8(),
	XsdValidatingReader_t3961132625::get_offset_of_namespaces_9(),
	XsdValidatingReader_t3961132625::get_offset_of_validationStarted_10(),
	XsdValidatingReader_t3961132625::get_offset_of_checkIdentity_11(),
	XsdValidatingReader_t3961132625::get_offset_of_idManager_12(),
	XsdValidatingReader_t3961132625::get_offset_of_checkKeyConstraints_13(),
	XsdValidatingReader_t3961132625::get_offset_of_keyTables_14(),
	XsdValidatingReader_t3961132625::get_offset_of_currentKeyFieldConsumers_15(),
	XsdValidatingReader_t3961132625::get_offset_of_tmpKeyrefPool_16(),
	XsdValidatingReader_t3961132625::get_offset_of_elementQNameStack_17(),
	XsdValidatingReader_t3961132625::get_offset_of_state_18(),
	XsdValidatingReader_t3961132625::get_offset_of_skipValidationDepth_19(),
	XsdValidatingReader_t3961132625::get_offset_of_xsiNilDepth_20(),
	XsdValidatingReader_t3961132625::get_offset_of_storedCharacters_21(),
	XsdValidatingReader_t3961132625::get_offset_of_shouldValidateCharacters_22(),
	XsdValidatingReader_t3961132625::get_offset_of_defaultAttributes_23(),
	XsdValidatingReader_t3961132625::get_offset_of_currentDefaultAttribute_24(),
	XsdValidatingReader_t3961132625::get_offset_of_defaultAttributesCache_25(),
	XsdValidatingReader_t3961132625::get_offset_of_defaultAttributeConsumed_26(),
	XsdValidatingReader_t3961132625::get_offset_of_currentAttrType_27(),
	XsdValidatingReader_t3961132625::get_offset_of_ValidationEventHandler_28(),
	XsdValidatingReader_t3961132625_StaticFields::get_offset_of_U3CU3Ef__switchU24map3_29(),
	XsdValidatingReader_t3961132625_StaticFields::get_offset_of_U3CU3Ef__switchU24map4_30(),
	XsdValidatingReader_t3961132625_StaticFields::get_offset_of_U3CU3Ef__switchU24map5_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1839 = { sizeof (XsdValidationContext_t1104170526), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1839[3] = 
{
	XsdValidationContext_t1104170526::get_offset_of_xsi_type_0(),
	XsdValidationContext_t1104170526::get_offset_of_State_1(),
	XsdValidationContext_t1104170526::get_offset_of_element_stack_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1840 = { sizeof (XsdIDManager_t1008806102), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1840[3] = 
{
	XsdIDManager_t1008806102::get_offset_of_idList_0(),
	XsdIDManager_t1008806102::get_offset_of_missingIDReferences_1(),
	XsdIDManager_t1008806102::get_offset_of_thisElementId_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1841 = { sizeof (XsdWildcard_t2790389089), -1, sizeof(XsdWildcard_t2790389089_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1841[10] = 
{
	XsdWildcard_t2790389089::get_offset_of_xsobj_0(),
	XsdWildcard_t2790389089::get_offset_of_ResolvedProcessing_1(),
	XsdWildcard_t2790389089::get_offset_of_TargetNamespace_2(),
	XsdWildcard_t2790389089::get_offset_of_SkipCompile_3(),
	XsdWildcard_t2790389089::get_offset_of_HasValueAny_4(),
	XsdWildcard_t2790389089::get_offset_of_HasValueLocal_5(),
	XsdWildcard_t2790389089::get_offset_of_HasValueOther_6(),
	XsdWildcard_t2790389089::get_offset_of_HasValueTargetNamespace_7(),
	XsdWildcard_t2790389089::get_offset_of_ResolvedNamespaces_8(),
	XsdWildcard_t2790389089_StaticFields::get_offset_of_U3CU3Ef__switchU24map6_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1842 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1843 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1844 = { sizeof (XsdWhitespaceFacet_t376308449)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1844[4] = 
{
	XsdWhitespaceFacet_t376308449::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1845 = { sizeof (XsdOrdering_t789960802)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1845[5] = 
{
	XsdOrdering_t789960802::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1846 = { sizeof (XsdAnySimpleType_t1257864485), -1, sizeof(XsdAnySimpleType_t1257864485_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1846[6] = 
{
	XsdAnySimpleType_t1257864485_StaticFields::get_offset_of_instance_55(),
	XsdAnySimpleType_t1257864485_StaticFields::get_offset_of_whitespaceArray_56(),
	XsdAnySimpleType_t1257864485_StaticFields::get_offset_of_booleanAllowedFacets_57(),
	XsdAnySimpleType_t1257864485_StaticFields::get_offset_of_decimalAllowedFacets_58(),
	XsdAnySimpleType_t1257864485_StaticFields::get_offset_of_durationAllowedFacets_59(),
	XsdAnySimpleType_t1257864485_StaticFields::get_offset_of_stringAllowedFacets_60(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1847 = { sizeof (XdtAnyAtomicType_t269366253), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1848 = { sizeof (XdtUntypedAtomic_t1388131523), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1849 = { sizeof (XsdString_t3049094358), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1850 = { sizeof (XsdNormalizedString_t3260789355), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1851 = { sizeof (XsdToken_t1239036978), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1852 = { sizeof (XsdLanguage_t1876291273), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1853 = { sizeof (XsdNMToken_t834691671), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1854 = { sizeof (XsdNMTokens_t4246953255), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1855 = { sizeof (XsdName_t2755146808), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1856 = { sizeof (XsdNCName_t3943159043), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1857 = { sizeof (XsdID_t34704195), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1858 = { sizeof (XsdIDRef_t2913612829), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1859 = { sizeof (XsdIDRefs_t16099206), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1860 = { sizeof (XsdEntity_t3956505874), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1861 = { sizeof (XsdEntities_t1477210398), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1862 = { sizeof (XsdNotation_t2827634056), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1863 = { sizeof (XsdDecimal_t1288601093), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1864 = { sizeof (XsdInteger_t2044766898), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1865 = { sizeof (XsdLong_t1324632828), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1866 = { sizeof (XsdInt_t33917785), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1867 = { sizeof (XsdShort_t3489811876), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1868 = { sizeof (XsdByte_t2221093920), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1869 = { sizeof (XsdNonNegativeInteger_t308064234), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1870 = { sizeof (XsdUnsignedLong_t1409593434), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1871 = { sizeof (XsdUnsignedInt_t72105793), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1872 = { sizeof (XsdUnsignedShort_t3654069686), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1873 = { sizeof (XsdUnsignedByte_t2304219558), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1874 = { sizeof (XsdPositiveInteger_t1704031413), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1875 = { sizeof (XsdNonPositiveInteger_t1029055398), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1876 = { sizeof (XsdNegativeInteger_t2178753546), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1877 = { sizeof (XsdFloat_t3181928905), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1878 = { sizeof (XsdDouble_t3324344982), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1879 = { sizeof (XsdBase64Binary_t3360383190), -1, sizeof(XsdBase64Binary_t3360383190_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1879[2] = 
{
	XsdBase64Binary_t3360383190_StaticFields::get_offset_of_ALPHABET_61(),
	XsdBase64Binary_t3360383190_StaticFields::get_offset_of_decodeTable_62(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1880 = { sizeof (XsdHexBinary_t882812470), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1881 = { sizeof (XsdQName_t2385631467), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1882 = { sizeof (XsdBoolean_t380164876), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1883 = { sizeof (XsdAnyURI_t2755748070), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1884 = { sizeof (XmlSchemaUri_t3948303260), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1884[1] = 
{
	XmlSchemaUri_t3948303260::get_offset_of_value_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1885 = { sizeof (XsdDuration_t1555973170), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1886 = { sizeof (XdtDayTimeDuration_t268779858), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1887 = { sizeof (XdtYearMonthDuration_t1503718519), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1888 = { sizeof (XsdDateTime_t2563698975), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1889 = { sizeof (XsdDate_t1417753656), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1890 = { sizeof (XsdTime_t3558487088), -1, sizeof(XsdTime_t3558487088_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1890[1] = 
{
	XsdTime_t3558487088_StaticFields::get_offset_of_timeFormats_61(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1891 = { sizeof (XsdGYearMonth_t3399073121), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1892 = { sizeof (XsdGMonthDay_t2605134399), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1893 = { sizeof (XsdGYear_t3316212116), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1894 = { sizeof (XsdGMonth_t3913018815), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1895 = { sizeof (XsdGDay_t293490745), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1896 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1897 = { sizeof (QNameValueType_t615604793)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1897[1] = 
{
	QNameValueType_t615604793::get_offset_of_value_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1898 = { sizeof (StringValueType_t261329552)+ sizeof (RuntimeObject), sizeof(StringValueType_t261329552_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1898[1] = 
{
	StringValueType_t261329552::get_offset_of_value_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1899 = { sizeof (UriValueType_t2866347695)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1899[1] = 
{
	UriValueType_t2866347695::get_offset_of_value_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
