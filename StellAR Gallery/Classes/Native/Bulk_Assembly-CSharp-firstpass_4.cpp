﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct VirtFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1, typename T2>
struct VirtActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// UnityEditor.XCodeEditor.XCConfigurationList
struct XCConfigurationList_t1100530024;
// System.String
struct String_t;
// UnityEditor.XCodeEditor.PBXDictionary
struct PBXDictionary_t2078602241;
// UnityEditor.XCodeEditor.PBXObject
struct PBXObject_t3397571057;
// UnityEditor.XCodeEditor.XCMod
struct XCMod_t1014962890;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.IO.FileInfo
struct FileInfo_t1169991790;
// System.IO.StreamReader
struct StreamReader_t4009935899;
// UnityEngine.UnityException
struct UnityException_t3598173660;
// System.Collections.ArrayList
struct ArrayList_t2718874744;
// UnityEditor.XCodeEditor.XCModFile
struct XCModFile_t1016819465;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Char[]
struct CharU5BU5D_t3528271667;
// UnityEditor.XCodeEditor.XCPlist
struct XCPlist_t2540119850;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t2865362463;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t132545152;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t257213610;
// UnityEditor.XCodeEditor.XCProject
struct XCProject_t3157646134;
// UnityEditor.XCodeEditor.PBXParser
struct PBXParser_t1614426892;
// System.Exception
struct Exception_t;
// UnityEditor.XCodeEditor.PBXProject
struct PBXProject_t4035248171;
// UnityEditor.XCodeEditor.PBXGroup
struct PBXGroup_t2128436724;
// UnityEditor.XCodeEditor.PBXSortedDictionary`1<UnityEditor.XCodeEditor.PBXBuildFile>
struct PBXSortedDictionary_1_t2756258120;
// UnityEditor.XCodeEditor.PBXSortedDictionary`1<System.Object>
struct PBXSortedDictionary_1_t3225971590;
// UnityEditor.XCodeEditor.PBXSortedDictionary`1<UnityEditor.XCodeEditor.PBXGroup>
struct PBXSortedDictionary_1_t2274302150;
// UnityEditor.XCodeEditor.PBXSortedDictionary`1<UnityEditor.XCodeEditor.PBXFileReference>
struct PBXSortedDictionary_1_t637646383;
// UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXVariantGroup>
struct PBXDictionary_1_t1414031086;
// UnityEditor.XCodeEditor.PBXDictionary`1<System.Object>
struct PBXDictionary_1_t1529852743;
// UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXNativeTarget>
struct PBXDictionary_1_t3538108837;
// UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.XCBuildConfiguration>
struct PBXDictionary_1_t4195780175;
// UnityEditor.XCodeEditor.PBXSortedDictionary`1<UnityEditor.XCodeEditor.XCConfigurationList>
struct PBXSortedDictionary_1_t1246395450;
// UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXFrameworksBuildPhase>
struct PBXDictionary_1_t125107989;
// UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXResourcesBuildPhase>
struct PBXDictionary_1_t1783080000;
// UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXShellScriptBuildPhase>
struct PBXDictionary_1_t1977435415;
// UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXSourcesBuildPhase>
struct PBXDictionary_1_t2932651977;
// UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXCopyFilesBuildPhase>
struct PBXDictionary_1_t2617057394;
// UnityEditor.XCodeEditor.PBXList
struct PBXList_t4289188522;
// System.Collections.Generic.Dictionary`2<System.String,UnityEditor.XCodeEditor.XCBuildConfiguration>
struct Dictionary_2_t1236322599;
// UnityEditor.XCodeEditor.XCBuildConfiguration
struct XCBuildConfiguration_t1451066300;
// System.Uri
struct Uri_t100236324;
// UnityEditor.XCodeEditor.PBXFileReference
struct PBXFileReference_t491780957;
// System.Type
struct Type_t;
// System.Collections.Generic.Dictionary`2<System.String,UnityEditor.XCodeEditor.PBXFrameworksBuildPhase>
struct Dictionary_2_t1460617709;
// System.Collections.Generic.Dictionary`2<System.String,UnityEditor.XCodeEditor.PBXResourcesBuildPhase>
struct Dictionary_2_t3118589720;
// System.Collections.Generic.Dictionary`2<System.String,UnityEditor.XCodeEditor.PBXShellScriptBuildPhase>
struct Dictionary_2_t3312945135;
// System.Collections.Generic.Dictionary`2<System.String,UnityEditor.XCodeEditor.PBXSourcesBuildPhase>
struct Dictionary_2_t4268161697;
// System.Collections.Generic.Dictionary`2<System.String,UnityEditor.XCodeEditor.PBXCopyFilesBuildPhase>
struct Dictionary_2_t3952567114;
// UnityEditor.XCodeEditor.PBXNativeTarget
struct PBXNativeTarget_t793394962;
// System.Collections.Generic.Dictionary`2<System.String,UnityEditor.XCodeEditor.PBXNativeTarget>
struct Dictionary_2_t578651261;
// UnityEditor.XCodeEditor.PBXCopyFilesBuildPhase
struct PBXCopyFilesBuildPhase_t4167310815;
// UnityEditor.XCodeEditor.PBXBuildFile
struct PBXBuildFile_t2610392694;
// UnityEditor.XCodeEditor.PBXBuildPhase
struct PBXBuildPhase_t3890765868;
// UnityEditor.XCodeEditor.PBXFrameworksBuildPhase
struct PBXFrameworksBuildPhase_t1675361410;
// UnityEditor.XCodeEditor.PBXResourcesBuildPhase
struct PBXResourcesBuildPhase_t3333333421;
// UnityEditor.XCodeEditor.PBXShellScriptBuildPhase
struct PBXShellScriptBuildPhase_t3527688836;
// UnityEditor.XCodeEditor.PBXSourcesBuildPhase
struct PBXSourcesBuildPhase_t187938102;
// System.IO.DirectoryInfo
struct DirectoryInfo_t35957480;
// UnityEditor.XCodeEditor.PBXVariantGroup
struct PBXVariantGroup_t2964284507;
// System.Collections.Generic.SortedDictionary`2<System.String,UnityEditor.XCodeEditor.PBXFileReference>
struct SortedDictionary_2_t1699557551;
// System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>
struct SortedDictionary_2_t1555065447;
// System.Collections.Generic.SortedDictionary`2<System.String,UnityEditor.XCodeEditor.PBXGroup>
struct SortedDictionary_2_t3336213318;
// System.IO.StreamWriter
struct StreamWriter_t1266378904;
// XUPorterJSON.MiniJSON
struct MiniJSON_t211875846;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t1632706988;
// System.Collections.ICollection
struct ICollection_t3904884886;
// UnityEditor.XCodeEditor.PBXResolver
struct PBXResolver_t144121547;
// System.IFormatProvider
struct IFormatProvider_t2518567562;
// System.Collections.Generic.RBTree
struct RBTree_t4095273678;
// System.Collections.Generic.SortedDictionary`2/NodeHelper<System.String,UnityEditor.XCodeEditor.PBXGroup>
struct NodeHelper_t1051634704;
// System.Collections.Generic.SortedDictionary`2/NodeHelper<System.String,UnityEditor.XCodeEditor.PBXFileReference>
struct NodeHelper_t3709946233;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t964245573;
// UnityEditor.XCodeEditor.PBXNativeTarget[]
struct PBXNativeTargetU5BU5D_t88167;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t3954782707;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t950877179;
// System.Collections.Generic.Dictionary`2/Transform`1<System.String,UnityEditor.XCodeEditor.PBXNativeTarget,System.Collections.DictionaryEntry>
struct Transform_1_t3145467179;
// UnityEditor.XCodeEditor.PBXCopyFilesBuildPhase[]
struct PBXCopyFilesBuildPhaseU5BU5D_t1750248390;
// System.Collections.Generic.Dictionary`2/Transform`1<System.String,UnityEditor.XCodeEditor.PBXCopyFilesBuildPhase,System.Collections.DictionaryEntry>
struct Transform_1_t600660106;
// UnityEditor.XCodeEditor.PBXSourcesBuildPhase[]
struct PBXSourcesBuildPhaseU5BU5D_t748958003;
// System.Collections.Generic.Dictionary`2/Transform`1<System.String,UnityEditor.XCodeEditor.PBXSourcesBuildPhase,System.Collections.DictionaryEntry>
struct Transform_1_t3894337015;
// UnityEditor.XCodeEditor.PBXShellScriptBuildPhase[]
struct PBXShellScriptBuildPhaseU5BU5D_t2003070701;
// System.Collections.Generic.Dictionary`2/Transform`1<System.String,UnityEditor.XCodeEditor.PBXShellScriptBuildPhase,System.Collections.DictionaryEntry>
struct Transform_1_t853482417;
// UnityEditor.XCodeEditor.PBXResourcesBuildPhase[]
struct PBXResourcesBuildPhaseU5BU5D_t1218824448;
// System.Collections.Generic.Dictionary`2/Transform`1<System.String,UnityEditor.XCodeEditor.PBXResourcesBuildPhase,System.Collections.DictionaryEntry>
struct Transform_1_t69236164;
// UnityEditor.XCodeEditor.PBXFrameworksBuildPhase[]
struct PBXFrameworksBuildPhaseU5BU5D_t1911506743;
// System.Collections.Generic.Dictionary`2/Transform`1<System.String,UnityEditor.XCodeEditor.PBXFrameworksBuildPhase,System.Collections.DictionaryEntry>
struct Transform_1_t761918459;
// UnityEditor.XCodeEditor.XCBuildConfiguration[]
struct XCBuildConfigurationU5BU5D_t2689903061;
// System.Collections.Generic.Dictionary`2/Transform`1<System.String,UnityEditor.XCodeEditor.XCBuildConfiguration,System.Collections.DictionaryEntry>
struct Transform_1_t1540314777;
// System.Uri/UriScheme[]
struct UriSchemeU5BU5D_t2082808316;
// System.UriParser
struct UriParser_t3890150400;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.Object,System.Collections.DictionaryEntry>
struct Transform_1_t1694351041;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.Collections.Hashtable/Slot[]
struct SlotU5BU5D_t2994659099;
// System.Collections.Hashtable/HashKeys
struct HashKeys_t1568156503;
// System.Collections.Hashtable/HashValues
struct HashValues_t618387445;
// System.Collections.IHashCodeProvider
struct IHashCodeProvider_t267601189;
// System.Collections.IComparer
struct IComparer_t1540313114;
// System.Collections.IEqualityComparer
struct IEqualityComparer_t1493878338;
// System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.String,System.Collections.DictionaryEntry>
struct Transform_1_t132201056;
// UnityEditor.XCodeEditor.PBXVariantGroup[]
struct PBXVariantGroupU5BU5D_t409628634;
// System.Collections.Generic.Dictionary`2/Transform`1<System.String,UnityEditor.XCodeEditor.PBXVariantGroup,System.Collections.DictionaryEntry>
struct Transform_1_t3555007646;
// System.Collections.Generic.SortedDictionary`2/NodeHelper<System.String,UnityEditor.XCodeEditor.PBXBuildFile>
struct NodeHelper_t1533590674;
// System.Collections.Generic.SortedDictionary`2/NodeHelper<System.String,UnityEditor.XCodeEditor.XCConfigurationList>
struct NodeHelper_t23728004;
// System.Runtime.Remoting.ServerIdentity
struct ServerIdentity_t2342208608;
// System.Void
struct Void_t1185182177;
// System.Text.Encoding
struct Encoding_t1523322056;
// System.IO.Stream
struct Stream_t1273022909;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Collections.Generic.Stack`1<System.Collections.Generic.RBTree/Node>
struct Stack_1_t2901053741;
// System.Collections.Generic.Dictionary`2<UnityEditor.XCodeEditor.TreeEnum,System.String>
struct Dictionary_2_t1851544083;
// System.Byte
struct Byte_t1134296376;
// System.Double
struct Double_t594665363;
// System.UInt16
struct UInt16_t2177724958;
// System.Text.Decoder
struct Decoder_t2204182725;
// System.Type[]
struct TypeU5BU5D_t3940880105;
// System.Reflection.MemberFilter
struct MemberFilter_t426314064;

extern RuntimeClass* Hashtable_t1853889766_il2cpp_TypeInfo_var;
extern RuntimeClass* FileInfo_t1169991790_il2cpp_TypeInfo_var;
extern RuntimeClass* Debug_t3317548046_il2cpp_TypeInfo_var;
extern RuntimeClass* Path_t1605229823_il2cpp_TypeInfo_var;
extern RuntimeClass* MiniJSON_t211875846_il2cpp_TypeInfo_var;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern RuntimeClass* UnityException_t3598173660_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral1490807184;
extern String_t* _stringLiteral4240777088;
extern String_t* _stringLiteral4073139529;
extern const uint32_t XCMod__ctor_m1865170813_MetadataUsageId;
extern String_t* _stringLiteral3122773422;
extern const uint32_t XCMod_get_group_m1801882381_MetadataUsageId;
extern RuntimeClass* ArrayList_t2718874744_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral4026311239;
extern const uint32_t XCMod_get_patches_m909294950_MetadataUsageId;
extern RuntimeClass* IEnumerator_t1853284238_il2cpp_TypeInfo_var;
extern RuntimeClass* XCModFile_t1016819465_il2cpp_TypeInfo_var;
extern RuntimeClass* IDisposable_t3640265483_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral4263797636;
extern String_t* _stringLiteral186182294;
extern const uint32_t XCMod_get_libs_m513448774_MetadataUsageId;
extern String_t* _stringLiteral2253447914;
extern const uint32_t XCMod_get_frameworks_m2568364667_MetadataUsageId;
extern String_t* _stringLiteral3213188363;
extern const uint32_t XCMod_get_headerpaths_m343671542_MetadataUsageId;
extern String_t* _stringLiteral452222907;
extern const uint32_t XCMod_get_files_m150690371_MetadataUsageId;
extern String_t* _stringLiteral3025985292;
extern const uint32_t XCMod_get_folders_m1874296509_MetadataUsageId;
extern String_t* _stringLiteral2596834824;
extern const uint32_t XCMod_get_excludes_m1351011736_MetadataUsageId;
extern String_t* _stringLiteral559529979;
extern const uint32_t XCMod_get_compiler_flags_m333845873_MetadataUsageId;
extern String_t* _stringLiteral3860215823;
extern const uint32_t XCMod_get_linker_flags_m3777781269_MetadataUsageId;
extern String_t* _stringLiteral377231163;
extern const uint32_t XCMod_get_embed_binaries_m3193459801_MetadataUsageId;
extern String_t* _stringLiteral4290053407;
extern const uint32_t XCMod_get_plist_m1661036855_MetadataUsageId;
extern RuntimeClass* CharU5BU5D_t3528271667_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral3452614550;
extern String_t* _stringLiteral4779758;
extern const uint32_t XCModFile__ctor_m3435055427_MetadataUsageId;
extern RuntimeClass* Plist_t3259848363_il2cpp_TypeInfo_var;
extern RuntimeClass* Dictionary_2_t2865362463_il2cpp_TypeInfo_var;
extern RuntimeClass* DictionaryEntry_t3123975638_il2cpp_TypeInfo_var;
extern const uint32_t XCPlist_Process_m1182457030_MetadataUsageId;
extern const RuntimeMethod* XCPlist_HashtableToDictionary_TisString_t_TisRuntimeObject_m3428346691_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_set_Item_m2329160973_RuntimeMethod_var;
extern String_t* _stringLiteral3386429785;
extern String_t* _stringLiteral472138309;
extern const uint32_t XCPlist_AddPlistItems_m4004335766_MetadataUsageId;
extern RuntimeClass* List_1_t257213610_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2_ContainsKey_m1520683221_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_get_Item_m3179620279_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m2321703786_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m3338814081_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2__ctor_m3962145734_RuntimeMethod_var;
extern String_t* _stringLiteral2500111239;
extern String_t* _stringLiteral1628940268;
extern String_t* _stringLiteral1935017661;
extern String_t* _stringLiteral62725243;
extern String_t* _stringLiteral3717251477;
extern String_t* _stringLiteral2703192964;
extern String_t* _stringLiteral1764797311;
extern String_t* _stringLiteral166333785;
extern const uint32_t XCPlist_processUrlTypes_m15864857_MetadataUsageId;
extern const RuntimeMethod* List_1_get_Count_m2934127733_RuntimeMethod_var;
extern const RuntimeMethod* List_1_GetEnumerator_m2930774921_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m470245444_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m2142368520_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m3007748546_RuntimeMethod_var;
extern const uint32_t XCPlist_findUrlTypeByName_m4027452897_MetadataUsageId;
extern RuntimeClass* PBXParser_t1614426892_il2cpp_TypeInfo_var;
extern RuntimeClass* Exception_t_il2cpp_TypeInfo_var;
extern RuntimeClass* Int32_t2950945753_il2cpp_TypeInfo_var;
extern RuntimeClass* PBXDictionary_t2078602241_il2cpp_TypeInfo_var;
extern RuntimeClass* PBXProject_t4035248171_il2cpp_TypeInfo_var;
extern RuntimeClass* PBXGroup_t2128436724_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2_get_Count_m2664023503_RuntimeMethod_var;
extern String_t* _stringLiteral3589804870;
extern String_t* _stringLiteral1225324344;
extern String_t* _stringLiteral2256741245;
extern String_t* _stringLiteral2300800221;
extern String_t* _stringLiteral3121128525;
extern String_t* _stringLiteral1669614476;
extern String_t* _stringLiteral2698730106;
extern String_t* _stringLiteral2770612856;
extern String_t* _stringLiteral2866737197;
extern String_t* _stringLiteral1506900225;
extern String_t* _stringLiteral1726171766;
extern String_t* _stringLiteral2693801196;
extern const uint32_t XCProject__ctor_m4217487726_MetadataUsageId;
extern RuntimeClass* PBXSortedDictionary_1_t2756258120_il2cpp_TypeInfo_var;
extern const RuntimeMethod* PBXSortedDictionary_1__ctor_m1490815053_RuntimeMethod_var;
extern const uint32_t XCProject_get_buildFiles_m2028255916_MetadataUsageId;
extern RuntimeClass* PBXSortedDictionary_1_t2274302150_il2cpp_TypeInfo_var;
extern const RuntimeMethod* PBXSortedDictionary_1__ctor_m2076080548_RuntimeMethod_var;
extern const uint32_t XCProject_get_groups_m1730659681_MetadataUsageId;
extern RuntimeClass* PBXSortedDictionary_1_t637646383_il2cpp_TypeInfo_var;
extern const RuntimeMethod* PBXSortedDictionary_1__ctor_m1533001087_RuntimeMethod_var;
extern const uint32_t XCProject_get_fileReferences_m858134601_MetadataUsageId;
extern RuntimeClass* PBXDictionary_1_t1414031086_il2cpp_TypeInfo_var;
extern const RuntimeMethod* PBXDictionary_1__ctor_m2094835722_RuntimeMethod_var;
extern const uint32_t XCProject_get_variantGroups_m3960773799_MetadataUsageId;
extern RuntimeClass* PBXDictionary_1_t3538108837_il2cpp_TypeInfo_var;
extern const RuntimeMethod* PBXDictionary_1__ctor_m4160535383_RuntimeMethod_var;
extern const uint32_t XCProject_get_nativeTargets_m2714694472_MetadataUsageId;
extern RuntimeClass* PBXDictionary_1_t4195780175_il2cpp_TypeInfo_var;
extern const RuntimeMethod* PBXDictionary_1__ctor_m1384061019_RuntimeMethod_var;
extern const uint32_t XCProject_get_buildConfigurations_m1437786018_MetadataUsageId;
extern RuntimeClass* PBXSortedDictionary_1_t1246395450_il2cpp_TypeInfo_var;
extern const RuntimeMethod* PBXSortedDictionary_1__ctor_m4001532630_RuntimeMethod_var;
extern const uint32_t XCProject_get_configurationLists_m3369988788_MetadataUsageId;
extern RuntimeClass* PBXDictionary_1_t125107989_il2cpp_TypeInfo_var;
extern const RuntimeMethod* PBXDictionary_1__ctor_m2416241266_RuntimeMethod_var;
extern const uint32_t XCProject_get_frameworkBuildPhases_m3514233196_MetadataUsageId;
extern RuntimeClass* PBXDictionary_1_t1783080000_il2cpp_TypeInfo_var;
extern const RuntimeMethod* PBXDictionary_1__ctor_m537106829_RuntimeMethod_var;
extern const uint32_t XCProject_get_resourcesBuildPhases_m2464200803_MetadataUsageId;
extern RuntimeClass* PBXDictionary_1_t1977435415_il2cpp_TypeInfo_var;
extern const RuntimeMethod* PBXDictionary_1__ctor_m3424980347_RuntimeMethod_var;
extern const uint32_t XCProject_get_shellScriptBuildPhases_m2311296572_MetadataUsageId;
extern RuntimeClass* PBXDictionary_1_t2932651977_il2cpp_TypeInfo_var;
extern const RuntimeMethod* PBXDictionary_1__ctor_m3351902458_RuntimeMethod_var;
extern const uint32_t XCProject_get_sourcesBuildPhases_m3700393290_MetadataUsageId;
extern RuntimeClass* PBXDictionary_1_t2617057394_il2cpp_TypeInfo_var;
extern const RuntimeMethod* PBXDictionary_1__ctor_m3032813453_RuntimeMethod_var;
extern const uint32_t XCProject_get_copyBuildPhases_m3593876452_MetadataUsageId;
extern RuntimeClass* PBXList_t4289188522_il2cpp_TypeInfo_var;
extern const uint32_t XCProject_AddOtherCFlags_m696039661_MetadataUsageId;
extern const RuntimeMethod* Dictionary_2_GetEnumerator_m1452846353_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m2313683576_RuntimeMethod_var;
extern const RuntimeMethod* KeyValuePair_2_get_Value_m953788127_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m3171309827_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m1544304532_RuntimeMethod_var;
extern const uint32_t XCProject_AddOtherCFlags_m432253178_MetadataUsageId;
extern const uint32_t XCProject_AddOtherLinkerFlags_m1772294907_MetadataUsageId;
extern const uint32_t XCProject_AddOtherLinkerFlags_m1003050127_MetadataUsageId;
extern RuntimeClass* StringU5BU5D_t1281789340_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral3405705519;
extern String_t* _stringLiteral3452614528;
extern String_t* _stringLiteral2910037981;
extern const uint32_t XCProject_overwriteBuildSetting_m3219624_MetadataUsageId;
extern const uint32_t XCProject_AddHeaderSearchPaths_m2329174020_MetadataUsageId;
extern String_t* _stringLiteral351569418;
extern const uint32_t XCProject_AddHeaderSearchPaths_m2713756577_MetadataUsageId;
extern const uint32_t XCProject_AddLibrarySearchPaths_m522864078_MetadataUsageId;
extern String_t* _stringLiteral1629623998;
extern const uint32_t XCProject_AddLibrarySearchPaths_m452363867_MetadataUsageId;
extern const uint32_t XCProject_AddFrameworkSearchPaths_m3452696326_MetadataUsageId;
extern const uint32_t XCProject_AddFrameworkSearchPaths_m2470425909_MetadataUsageId;
extern const uint32_t XCProject_GetObject_m1073236403_MetadataUsageId;
extern const RuntimeType* TreeEnum_t2111024294_0_0_0_var;
extern RuntimeClass* Uri_t100236324_il2cpp_TypeInfo_var;
extern RuntimeClass* Type_t_il2cpp_TypeInfo_var;
extern RuntimeClass* Enum_t4135868527_il2cpp_TypeInfo_var;
extern RuntimeClass* TreeEnum_t2111024294_il2cpp_TypeInfo_var;
extern RuntimeClass* PBXFileReference_t491780957_il2cpp_TypeInfo_var;
extern const RuntimeMethod* PBXSortedDictionary_1_Add_m3189495737_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Add_m825500632_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_GetEnumerator_m2923669497_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m2582763622_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m2971571199_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m636527872_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_GetEnumerator_m2740980526_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m2465070363_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m413976394_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m649549926_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_GetEnumerator_m1885921306_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m585116751_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m3699906100_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m2093597648_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_GetEnumerator_m951612022_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m2899533568_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m1034167961_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m754445729_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_GetEnumerator_m2675864505_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m1936049740_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m3513999528_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m1838677398_RuntimeMethod_var;
extern String_t* _stringLiteral2878191127;
extern String_t* _stringLiteral2867911068;
extern String_t* _stringLiteral1430076019;
extern String_t* _stringLiteral1293495386;
extern String_t* _stringLiteral901633645;
extern String_t* _stringLiteral3662213890;
extern String_t* _stringLiteral3450648449;
extern String_t* _stringLiteral3589484046;
extern String_t* _stringLiteral155981296;
extern String_t* _stringLiteral2046401046;
extern String_t* _stringLiteral2388984547;
extern String_t* _stringLiteral3050148334;
extern String_t* _stringLiteral506062875;
extern String_t* _stringLiteral2428378231;
extern String_t* _stringLiteral2369475652;
extern String_t* _stringLiteral1573716555;
extern String_t* _stringLiteral2060813112;
extern String_t* _stringLiteral4071089341;
extern String_t* _stringLiteral4093921082;
extern String_t* _stringLiteral4027760774;
extern String_t* _stringLiteral3168125321;
extern String_t* _stringLiteral3377412301;
extern const uint32_t XCProject_AddFile_m3957251854_MetadataUsageId;
extern const RuntimeMethod* Dictionary_2_GetEnumerator_m170078521_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m4204254741_RuntimeMethod_var;
extern const RuntimeMethod* KeyValuePair_2_get_Value_m543951955_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m726921650_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m3454157324_RuntimeMethod_var;
extern const uint32_t XCProject_GetNativeTarget_m2903336074_MetadataUsageId;
extern const RuntimeMethod* KeyValuePair_2_get_Value_m4174862611_RuntimeMethod_var;
extern String_t* _stringLiteral47317885;
extern const uint32_t XCProject_GetBuildActionMask_m3043263402_MetadataUsageId;
extern RuntimeClass* PBXCopyFilesBuildPhase_t4167310815_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2_TryGetValue_m2997259014_RuntimeMethod_var;
extern const RuntimeMethod* PBXDictionary_1_Add_m1199137400_RuntimeMethod_var;
extern String_t* _stringLiteral3505219263;
extern String_t* _stringLiteral2034847608;
extern String_t* _stringLiteral3175314671;
extern String_t* _stringLiteral2345052496;
extern const uint32_t XCProject_AddEmbedFrameworkBuildPhase_m2982154168_MetadataUsageId;
extern RuntimeClass* PBXBuildFile_t2610392694_il2cpp_TypeInfo_var;
extern const RuntimeMethod* PBXSortedDictionary_1_Add_m3739564697_RuntimeMethod_var;
extern String_t* _stringLiteral953705096;
extern String_t* _stringLiteral1780090642;
extern String_t* _stringLiteral2523233077;
extern const uint32_t XCProject_AddEmbedFramework_m1485538180_MetadataUsageId;
extern const RuntimeMethod* KeyValuePair_2_get_Value_m2936696925_RuntimeMethod_var;
extern const uint32_t XCProject_BuildAddFile_m2837910453_MetadataUsageId;
extern const RuntimeMethod* KeyValuePair_2_get_Value_m887331346_RuntimeMethod_var;
extern const uint32_t XCProject_BuildAddFile_m2859561336_MetadataUsageId;
extern const RuntimeMethod* KeyValuePair_2_get_Value_m4161882241_RuntimeMethod_var;
extern const uint32_t XCProject_BuildAddFile_m2061606299_MetadataUsageId;
extern const RuntimeMethod* KeyValuePair_2_get_Value_m990072085_RuntimeMethod_var;
extern const uint32_t XCProject_BuildAddFile_m1056009297_MetadataUsageId;
extern const uint32_t XCProject_BuildAddFile_m2193813989_MetadataUsageId;
extern RuntimeClass* DirectoryInfo_t35957480_il2cpp_TypeInfo_var;
extern RuntimeClass* Regex_t3657309853_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral1520028804;
extern String_t* _stringLiteral2481181957;
extern String_t* _stringLiteral3541885379;
extern String_t* _stringLiteral2037147000;
extern String_t* _stringLiteral3203609901;
extern String_t* _stringLiteral3134809294;
extern String_t* _stringLiteral103787962;
extern String_t* _stringLiteral1241662146;
extern String_t* _stringLiteral3032477948;
extern String_t* _stringLiteral392042043;
extern String_t* _stringLiteral1976437474;
extern String_t* _stringLiteral628085470;
extern String_t* _stringLiteral3452614612;
extern String_t* _stringLiteral1605085672;
extern const uint32_t XCProject_AddFolder_m3825015009_MetadataUsageId;
extern RuntimeClass* PBXVariantGroup_t2964284507_il2cpp_TypeInfo_var;
extern const RuntimeMethod* PBXDictionary_1_Add_m3289887501_RuntimeMethod_var;
extern const uint32_t XCProject_AddLocFolder_m1925302028_MetadataUsageId;
extern const RuntimeMethod* SortedDictionary_2_GetEnumerator_m2931611635_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m2415991694_RuntimeMethod_var;
extern const RuntimeMethod* KeyValuePair_2_get_Value_m3214895324_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m266277119_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m3852176452_RuntimeMethod_var;
extern const uint32_t XCProject_GetFile_m1538714244_MetadataUsageId;
extern const RuntimeMethod* SortedDictionary_2_GetEnumerator_m1969713601_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m1763852423_RuntimeMethod_var;
extern const RuntimeMethod* KeyValuePair_2_get_Value_m3727113552_RuntimeMethod_var;
extern const RuntimeMethod* KeyValuePair_2_get_Key_m2204949747_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m1528297490_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m4154903118_RuntimeMethod_var;
extern const RuntimeMethod* PBXSortedDictionary_1_Add_m259638508_RuntimeMethod_var;
extern const uint32_t XCProject_GetGroup_m2965518548_MetadataUsageId;
extern RuntimeClass* XCMod_t1014962890_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral641450103;
extern const uint32_t XCProject_ApplyMod_m3627033218_MetadataUsageId;
extern const RuntimeType* String_t_0_0_0_var;
extern RuntimeClass* XCPlist_t2540119850_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral2853481058;
extern String_t* _stringLiteral1188464369;
extern String_t* _stringLiteral2379681108;
extern String_t* _stringLiteral1217928750;
extern String_t* _stringLiteral2253480458;
extern String_t* _stringLiteral3642357633;
extern String_t* _stringLiteral3077977161;
extern String_t* _stringLiteral2110842851;
extern String_t* _stringLiteral649783399;
extern String_t* _stringLiteral4114321725;
extern String_t* _stringLiteral2705147582;
extern String_t* _stringLiteral2678398999;
extern String_t* _stringLiteral1245592032;
extern String_t* _stringLiteral1542650607;
extern String_t* _stringLiteral2539445417;
extern String_t* _stringLiteral366178444;
extern String_t* _stringLiteral488582302;
extern String_t* _stringLiteral853674241;
extern String_t* _stringLiteral4056293757;
extern String_t* _stringLiteral1428942434;
extern String_t* _stringLiteral836390878;
extern const uint32_t XCProject_ApplyMod_m3992715734_MetadataUsageId;
extern const RuntimeMethod* PBXDictionary_Append_TisPBXBuildFile_t2610392694_m3797558854_RuntimeMethod_var;
extern const RuntimeMethod* PBXDictionary_Append_TisPBXCopyFilesBuildPhase_t4167310815_m945363416_RuntimeMethod_var;
extern const RuntimeMethod* PBXDictionary_Append_TisPBXFileReference_t491780957_m334418948_RuntimeMethod_var;
extern const RuntimeMethod* PBXDictionary_Append_TisPBXFrameworksBuildPhase_t1675361410_m876528981_RuntimeMethod_var;
extern const RuntimeMethod* PBXDictionary_Append_TisPBXGroup_t2128436724_m2862935796_RuntimeMethod_var;
extern const RuntimeMethod* PBXDictionary_Append_TisPBXNativeTarget_t793394962_m1547927114_RuntimeMethod_var;
extern const RuntimeMethod* PBXDictionary_Append_TisPBXResourcesBuildPhase_t3333333421_m2729679594_RuntimeMethod_var;
extern const RuntimeMethod* PBXDictionary_Append_TisPBXShellScriptBuildPhase_t3527688836_m3716639730_RuntimeMethod_var;
extern const RuntimeMethod* PBXDictionary_Append_TisPBXSourcesBuildPhase_t187938102_m2147902702_RuntimeMethod_var;
extern const RuntimeMethod* PBXDictionary_Append_TisPBXVariantGroup_t2964284507_m211501280_RuntimeMethod_var;
extern const RuntimeMethod* PBXDictionary_Append_TisXCBuildConfiguration_t1451066300_m2304102353_RuntimeMethod_var;
extern const RuntimeMethod* PBXDictionary_Append_TisXCConfigurationList_t1100530024_m4062973500_RuntimeMethod_var;
extern const uint32_t XCProject_Consolidate_m609230681_MetadataUsageId;
extern String_t* _stringLiteral1755754488;
extern const uint32_t XCProject_Backup_m2128267110_MetadataUsageId;
extern const uint32_t XCProject_CreateNewProject_m1548580596_MetadataUsageId;
extern String_t* _stringLiteral1673557266;
extern String_t* _stringLiteral1718878004;
extern String_t* _stringLiteral2789466730;
extern const uint32_t XCProject_Save_m570185480_MetadataUsageId;
extern const uint32_t MiniJSON_jsonDecode_m2285616255_MetadataUsageId;
extern RuntimeClass* StringBuilder_t_il2cpp_TypeInfo_var;
extern const uint32_t MiniJSON_jsonEncode_m1032995782_MetadataUsageId;
extern const uint32_t MiniJSON_lastDecodeSuccessful_m3883034971_MetadataUsageId;
extern const uint32_t MiniJSON_getLastErrorIndex_m2235336515_MetadataUsageId;
extern const uint32_t MiniJSON_getLastErrorSnippet_m3000786183_MetadataUsageId;
extern const uint32_t MiniJSON_parseObject_m2380755468_MetadataUsageId;
extern const uint32_t MiniJSON_parseArray_m1439775387_MetadataUsageId;
extern RuntimeClass* Double_t594665363_il2cpp_TypeInfo_var;
extern RuntimeClass* Boolean_t97287965_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral107078765;
extern String_t* _stringLiteral47698025;
extern const uint32_t MiniJSON_parseValue_m3659675017_MetadataUsageId;
extern RuntimeClass* Char_t3634460470_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral4162292604;
extern String_t* _stringLiteral3452614549;
extern const uint32_t MiniJSON_parseString_m2073376077_MetadataUsageId;
extern const uint32_t MiniJSON_parseNumber_m3876524415_MetadataUsageId;
extern String_t* _stringLiteral2206812729;
extern const uint32_t MiniJSON_getLastIndexOfNumber_m1276157804_MetadataUsageId;
extern String_t* _stringLiteral1596281288;
extern const uint32_t MiniJSON_eatWhitespace_m1328618482_MetadataUsageId;
extern const uint32_t MiniJSON_lookAhead_m3599256246_MetadataUsageId;
extern const uint32_t MiniJSON_nextToken_m3582996375_MetadataUsageId;
extern const uint32_t MiniJSON_serializeObjectOrArray_m1482338836_MetadataUsageId;
extern RuntimeClass* IDictionaryEnumerator_t1693217257_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral3452614613;
extern String_t* _stringLiteral3450517380;
extern String_t* _stringLiteral3452614611;
extern const uint32_t MiniJSON_serializeObject_m716018166_MetadataUsageId;
extern const RuntimeMethod* Dictionary_2_GetEnumerator_m260861070_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m2519371089_RuntimeMethod_var;
extern const RuntimeMethod* KeyValuePair_2_get_Key_m3980750512_RuntimeMethod_var;
extern const RuntimeMethod* KeyValuePair_2_get_Value_m2243990694_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m1175750522_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m2198401511_RuntimeMethod_var;
extern const uint32_t MiniJSON_serializeDictionary_m2956593198_MetadataUsageId;
extern String_t* _stringLiteral3452614645;
extern String_t* _stringLiteral3452614643;
extern const uint32_t MiniJSON_serializeArray_m710485201_MetadataUsageId;
extern RuntimeClass* ICollection_t3904884886_il2cpp_TypeInfo_var;
extern RuntimeClass* Convert_t2465617642_il2cpp_TypeInfo_var;
extern RuntimeClass* Dictionary_2_t1632706988_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral1202628576;
extern String_t* _stringLiteral4002445229;
extern String_t* _stringLiteral3875954633;
extern const uint32_t MiniJSON_serializeValue_m2428040582_MetadataUsageId;
extern String_t* _stringLiteral3452614526;
extern String_t* _stringLiteral3450386420;
extern String_t* _stringLiteral3458119668;
extern String_t* _stringLiteral3454580724;
extern String_t* _stringLiteral3454318580;
extern String_t* _stringLiteral3454842868;
extern String_t* _stringLiteral3455629300;
extern String_t* _stringLiteral3455498228;
extern String_t* _stringLiteral3455432692;
extern const uint32_t MiniJSON_serializeString_m122238726_MetadataUsageId;
extern const uint32_t MiniJSON_serializeNumber_m3396057679_MetadataUsageId;
extern const uint32_t MiniJSON__cctor_m1969922968_MetadataUsageId;
extern const uint32_t MiniJsonExtensions_toJson_m1282538434_MetadataUsageId;
extern const uint32_t MiniJsonExtensions_toJson_m1260756840_MetadataUsageId;
extern const uint32_t MiniJsonExtensions_arrayListFromJson_m1736594186_MetadataUsageId;
extern const uint32_t MiniJsonExtensions_hashtableFromJson_m2915569823_MetadataUsageId;

struct StringU5BU5D_t1281789340;
struct CharU5BU5D_t3528271667;


#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef XCPROJECT_T3157646134_H
#define XCPROJECT_T3157646134_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.XCProject
struct  XCProject_t3157646134  : public RuntimeObject
{
public:
	// UnityEditor.XCodeEditor.PBXDictionary UnityEditor.XCodeEditor.XCProject::_datastore
	PBXDictionary_t2078602241 * ____datastore_0;
	// UnityEditor.XCodeEditor.PBXDictionary UnityEditor.XCodeEditor.XCProject::_objects
	PBXDictionary_t2078602241 * ____objects_1;
	// UnityEditor.XCodeEditor.PBXGroup UnityEditor.XCodeEditor.XCProject::_rootGroup
	PBXGroup_t2128436724 * ____rootGroup_2;
	// System.String UnityEditor.XCodeEditor.XCProject::_rootObjectKey
	String_t* ____rootObjectKey_3;
	// System.String UnityEditor.XCodeEditor.XCProject::<projectRootPath>k__BackingField
	String_t* ___U3CprojectRootPathU3Ek__BackingField_4;
	// System.IO.FileInfo UnityEditor.XCodeEditor.XCProject::projectFileInfo
	FileInfo_t1169991790 * ___projectFileInfo_5;
	// System.String UnityEditor.XCodeEditor.XCProject::<filePath>k__BackingField
	String_t* ___U3CfilePathU3Ek__BackingField_6;
	// System.Boolean UnityEditor.XCodeEditor.XCProject::modified
	bool ___modified_7;
	// UnityEditor.XCodeEditor.PBXSortedDictionary`1<UnityEditor.XCodeEditor.PBXBuildFile> UnityEditor.XCodeEditor.XCProject::_buildFiles
	PBXSortedDictionary_1_t2756258120 * ____buildFiles_8;
	// UnityEditor.XCodeEditor.PBXSortedDictionary`1<UnityEditor.XCodeEditor.PBXGroup> UnityEditor.XCodeEditor.XCProject::_groups
	PBXSortedDictionary_1_t2274302150 * ____groups_9;
	// UnityEditor.XCodeEditor.PBXSortedDictionary`1<UnityEditor.XCodeEditor.PBXFileReference> UnityEditor.XCodeEditor.XCProject::_fileReferences
	PBXSortedDictionary_1_t637646383 * ____fileReferences_10;
	// UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXNativeTarget> UnityEditor.XCodeEditor.XCProject::_nativeTargets
	PBXDictionary_1_t3538108837 * ____nativeTargets_11;
	// UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXFrameworksBuildPhase> UnityEditor.XCodeEditor.XCProject::_frameworkBuildPhases
	PBXDictionary_1_t125107989 * ____frameworkBuildPhases_12;
	// UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXResourcesBuildPhase> UnityEditor.XCodeEditor.XCProject::_resourcesBuildPhases
	PBXDictionary_1_t1783080000 * ____resourcesBuildPhases_13;
	// UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXShellScriptBuildPhase> UnityEditor.XCodeEditor.XCProject::_shellScriptBuildPhases
	PBXDictionary_1_t1977435415 * ____shellScriptBuildPhases_14;
	// UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXSourcesBuildPhase> UnityEditor.XCodeEditor.XCProject::_sourcesBuildPhases
	PBXDictionary_1_t2932651977 * ____sourcesBuildPhases_15;
	// UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXCopyFilesBuildPhase> UnityEditor.XCodeEditor.XCProject::_copyBuildPhases
	PBXDictionary_1_t2617057394 * ____copyBuildPhases_16;
	// UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXVariantGroup> UnityEditor.XCodeEditor.XCProject::_variantGroups
	PBXDictionary_1_t1414031086 * ____variantGroups_17;
	// UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.XCBuildConfiguration> UnityEditor.XCodeEditor.XCProject::_buildConfigurations
	PBXDictionary_1_t4195780175 * ____buildConfigurations_18;
	// UnityEditor.XCodeEditor.PBXSortedDictionary`1<UnityEditor.XCodeEditor.XCConfigurationList> UnityEditor.XCodeEditor.XCProject::_configurationLists
	PBXSortedDictionary_1_t1246395450 * ____configurationLists_19;
	// UnityEditor.XCodeEditor.PBXProject UnityEditor.XCodeEditor.XCProject::_project
	PBXProject_t4035248171 * ____project_20;

public:
	inline static int32_t get_offset_of__datastore_0() { return static_cast<int32_t>(offsetof(XCProject_t3157646134, ____datastore_0)); }
	inline PBXDictionary_t2078602241 * get__datastore_0() const { return ____datastore_0; }
	inline PBXDictionary_t2078602241 ** get_address_of__datastore_0() { return &____datastore_0; }
	inline void set__datastore_0(PBXDictionary_t2078602241 * value)
	{
		____datastore_0 = value;
		Il2CppCodeGenWriteBarrier((&____datastore_0), value);
	}

	inline static int32_t get_offset_of__objects_1() { return static_cast<int32_t>(offsetof(XCProject_t3157646134, ____objects_1)); }
	inline PBXDictionary_t2078602241 * get__objects_1() const { return ____objects_1; }
	inline PBXDictionary_t2078602241 ** get_address_of__objects_1() { return &____objects_1; }
	inline void set__objects_1(PBXDictionary_t2078602241 * value)
	{
		____objects_1 = value;
		Il2CppCodeGenWriteBarrier((&____objects_1), value);
	}

	inline static int32_t get_offset_of__rootGroup_2() { return static_cast<int32_t>(offsetof(XCProject_t3157646134, ____rootGroup_2)); }
	inline PBXGroup_t2128436724 * get__rootGroup_2() const { return ____rootGroup_2; }
	inline PBXGroup_t2128436724 ** get_address_of__rootGroup_2() { return &____rootGroup_2; }
	inline void set__rootGroup_2(PBXGroup_t2128436724 * value)
	{
		____rootGroup_2 = value;
		Il2CppCodeGenWriteBarrier((&____rootGroup_2), value);
	}

	inline static int32_t get_offset_of__rootObjectKey_3() { return static_cast<int32_t>(offsetof(XCProject_t3157646134, ____rootObjectKey_3)); }
	inline String_t* get__rootObjectKey_3() const { return ____rootObjectKey_3; }
	inline String_t** get_address_of__rootObjectKey_3() { return &____rootObjectKey_3; }
	inline void set__rootObjectKey_3(String_t* value)
	{
		____rootObjectKey_3 = value;
		Il2CppCodeGenWriteBarrier((&____rootObjectKey_3), value);
	}

	inline static int32_t get_offset_of_U3CprojectRootPathU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(XCProject_t3157646134, ___U3CprojectRootPathU3Ek__BackingField_4)); }
	inline String_t* get_U3CprojectRootPathU3Ek__BackingField_4() const { return ___U3CprojectRootPathU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CprojectRootPathU3Ek__BackingField_4() { return &___U3CprojectRootPathU3Ek__BackingField_4; }
	inline void set_U3CprojectRootPathU3Ek__BackingField_4(String_t* value)
	{
		___U3CprojectRootPathU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CprojectRootPathU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_projectFileInfo_5() { return static_cast<int32_t>(offsetof(XCProject_t3157646134, ___projectFileInfo_5)); }
	inline FileInfo_t1169991790 * get_projectFileInfo_5() const { return ___projectFileInfo_5; }
	inline FileInfo_t1169991790 ** get_address_of_projectFileInfo_5() { return &___projectFileInfo_5; }
	inline void set_projectFileInfo_5(FileInfo_t1169991790 * value)
	{
		___projectFileInfo_5 = value;
		Il2CppCodeGenWriteBarrier((&___projectFileInfo_5), value);
	}

	inline static int32_t get_offset_of_U3CfilePathU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(XCProject_t3157646134, ___U3CfilePathU3Ek__BackingField_6)); }
	inline String_t* get_U3CfilePathU3Ek__BackingField_6() const { return ___U3CfilePathU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CfilePathU3Ek__BackingField_6() { return &___U3CfilePathU3Ek__BackingField_6; }
	inline void set_U3CfilePathU3Ek__BackingField_6(String_t* value)
	{
		___U3CfilePathU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CfilePathU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_modified_7() { return static_cast<int32_t>(offsetof(XCProject_t3157646134, ___modified_7)); }
	inline bool get_modified_7() const { return ___modified_7; }
	inline bool* get_address_of_modified_7() { return &___modified_7; }
	inline void set_modified_7(bool value)
	{
		___modified_7 = value;
	}

	inline static int32_t get_offset_of__buildFiles_8() { return static_cast<int32_t>(offsetof(XCProject_t3157646134, ____buildFiles_8)); }
	inline PBXSortedDictionary_1_t2756258120 * get__buildFiles_8() const { return ____buildFiles_8; }
	inline PBXSortedDictionary_1_t2756258120 ** get_address_of__buildFiles_8() { return &____buildFiles_8; }
	inline void set__buildFiles_8(PBXSortedDictionary_1_t2756258120 * value)
	{
		____buildFiles_8 = value;
		Il2CppCodeGenWriteBarrier((&____buildFiles_8), value);
	}

	inline static int32_t get_offset_of__groups_9() { return static_cast<int32_t>(offsetof(XCProject_t3157646134, ____groups_9)); }
	inline PBXSortedDictionary_1_t2274302150 * get__groups_9() const { return ____groups_9; }
	inline PBXSortedDictionary_1_t2274302150 ** get_address_of__groups_9() { return &____groups_9; }
	inline void set__groups_9(PBXSortedDictionary_1_t2274302150 * value)
	{
		____groups_9 = value;
		Il2CppCodeGenWriteBarrier((&____groups_9), value);
	}

	inline static int32_t get_offset_of__fileReferences_10() { return static_cast<int32_t>(offsetof(XCProject_t3157646134, ____fileReferences_10)); }
	inline PBXSortedDictionary_1_t637646383 * get__fileReferences_10() const { return ____fileReferences_10; }
	inline PBXSortedDictionary_1_t637646383 ** get_address_of__fileReferences_10() { return &____fileReferences_10; }
	inline void set__fileReferences_10(PBXSortedDictionary_1_t637646383 * value)
	{
		____fileReferences_10 = value;
		Il2CppCodeGenWriteBarrier((&____fileReferences_10), value);
	}

	inline static int32_t get_offset_of__nativeTargets_11() { return static_cast<int32_t>(offsetof(XCProject_t3157646134, ____nativeTargets_11)); }
	inline PBXDictionary_1_t3538108837 * get__nativeTargets_11() const { return ____nativeTargets_11; }
	inline PBXDictionary_1_t3538108837 ** get_address_of__nativeTargets_11() { return &____nativeTargets_11; }
	inline void set__nativeTargets_11(PBXDictionary_1_t3538108837 * value)
	{
		____nativeTargets_11 = value;
		Il2CppCodeGenWriteBarrier((&____nativeTargets_11), value);
	}

	inline static int32_t get_offset_of__frameworkBuildPhases_12() { return static_cast<int32_t>(offsetof(XCProject_t3157646134, ____frameworkBuildPhases_12)); }
	inline PBXDictionary_1_t125107989 * get__frameworkBuildPhases_12() const { return ____frameworkBuildPhases_12; }
	inline PBXDictionary_1_t125107989 ** get_address_of__frameworkBuildPhases_12() { return &____frameworkBuildPhases_12; }
	inline void set__frameworkBuildPhases_12(PBXDictionary_1_t125107989 * value)
	{
		____frameworkBuildPhases_12 = value;
		Il2CppCodeGenWriteBarrier((&____frameworkBuildPhases_12), value);
	}

	inline static int32_t get_offset_of__resourcesBuildPhases_13() { return static_cast<int32_t>(offsetof(XCProject_t3157646134, ____resourcesBuildPhases_13)); }
	inline PBXDictionary_1_t1783080000 * get__resourcesBuildPhases_13() const { return ____resourcesBuildPhases_13; }
	inline PBXDictionary_1_t1783080000 ** get_address_of__resourcesBuildPhases_13() { return &____resourcesBuildPhases_13; }
	inline void set__resourcesBuildPhases_13(PBXDictionary_1_t1783080000 * value)
	{
		____resourcesBuildPhases_13 = value;
		Il2CppCodeGenWriteBarrier((&____resourcesBuildPhases_13), value);
	}

	inline static int32_t get_offset_of__shellScriptBuildPhases_14() { return static_cast<int32_t>(offsetof(XCProject_t3157646134, ____shellScriptBuildPhases_14)); }
	inline PBXDictionary_1_t1977435415 * get__shellScriptBuildPhases_14() const { return ____shellScriptBuildPhases_14; }
	inline PBXDictionary_1_t1977435415 ** get_address_of__shellScriptBuildPhases_14() { return &____shellScriptBuildPhases_14; }
	inline void set__shellScriptBuildPhases_14(PBXDictionary_1_t1977435415 * value)
	{
		____shellScriptBuildPhases_14 = value;
		Il2CppCodeGenWriteBarrier((&____shellScriptBuildPhases_14), value);
	}

	inline static int32_t get_offset_of__sourcesBuildPhases_15() { return static_cast<int32_t>(offsetof(XCProject_t3157646134, ____sourcesBuildPhases_15)); }
	inline PBXDictionary_1_t2932651977 * get__sourcesBuildPhases_15() const { return ____sourcesBuildPhases_15; }
	inline PBXDictionary_1_t2932651977 ** get_address_of__sourcesBuildPhases_15() { return &____sourcesBuildPhases_15; }
	inline void set__sourcesBuildPhases_15(PBXDictionary_1_t2932651977 * value)
	{
		____sourcesBuildPhases_15 = value;
		Il2CppCodeGenWriteBarrier((&____sourcesBuildPhases_15), value);
	}

	inline static int32_t get_offset_of__copyBuildPhases_16() { return static_cast<int32_t>(offsetof(XCProject_t3157646134, ____copyBuildPhases_16)); }
	inline PBXDictionary_1_t2617057394 * get__copyBuildPhases_16() const { return ____copyBuildPhases_16; }
	inline PBXDictionary_1_t2617057394 ** get_address_of__copyBuildPhases_16() { return &____copyBuildPhases_16; }
	inline void set__copyBuildPhases_16(PBXDictionary_1_t2617057394 * value)
	{
		____copyBuildPhases_16 = value;
		Il2CppCodeGenWriteBarrier((&____copyBuildPhases_16), value);
	}

	inline static int32_t get_offset_of__variantGroups_17() { return static_cast<int32_t>(offsetof(XCProject_t3157646134, ____variantGroups_17)); }
	inline PBXDictionary_1_t1414031086 * get__variantGroups_17() const { return ____variantGroups_17; }
	inline PBXDictionary_1_t1414031086 ** get_address_of__variantGroups_17() { return &____variantGroups_17; }
	inline void set__variantGroups_17(PBXDictionary_1_t1414031086 * value)
	{
		____variantGroups_17 = value;
		Il2CppCodeGenWriteBarrier((&____variantGroups_17), value);
	}

	inline static int32_t get_offset_of__buildConfigurations_18() { return static_cast<int32_t>(offsetof(XCProject_t3157646134, ____buildConfigurations_18)); }
	inline PBXDictionary_1_t4195780175 * get__buildConfigurations_18() const { return ____buildConfigurations_18; }
	inline PBXDictionary_1_t4195780175 ** get_address_of__buildConfigurations_18() { return &____buildConfigurations_18; }
	inline void set__buildConfigurations_18(PBXDictionary_1_t4195780175 * value)
	{
		____buildConfigurations_18 = value;
		Il2CppCodeGenWriteBarrier((&____buildConfigurations_18), value);
	}

	inline static int32_t get_offset_of__configurationLists_19() { return static_cast<int32_t>(offsetof(XCProject_t3157646134, ____configurationLists_19)); }
	inline PBXSortedDictionary_1_t1246395450 * get__configurationLists_19() const { return ____configurationLists_19; }
	inline PBXSortedDictionary_1_t1246395450 ** get_address_of__configurationLists_19() { return &____configurationLists_19; }
	inline void set__configurationLists_19(PBXSortedDictionary_1_t1246395450 * value)
	{
		____configurationLists_19 = value;
		Il2CppCodeGenWriteBarrier((&____configurationLists_19), value);
	}

	inline static int32_t get_offset_of__project_20() { return static_cast<int32_t>(offsetof(XCProject_t3157646134, ____project_20)); }
	inline PBXProject_t4035248171 * get__project_20() const { return ____project_20; }
	inline PBXProject_t4035248171 ** get_address_of__project_20() { return &____project_20; }
	inline void set__project_20(PBXProject_t4035248171 * value)
	{
		____project_20 = value;
		Il2CppCodeGenWriteBarrier((&____project_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XCPROJECT_T3157646134_H
#ifndef PBXPARSER_T1614426892_H
#define PBXPARSER_T1614426892_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.PBXParser
struct  PBXParser_t1614426892  : public RuntimeObject
{
public:
	// System.Char[] UnityEditor.XCodeEditor.PBXParser::data
	CharU5BU5D_t3528271667* ___data_20;
	// System.Int32 UnityEditor.XCodeEditor.PBXParser::index
	int32_t ___index_21;
	// UnityEditor.XCodeEditor.PBXResolver UnityEditor.XCodeEditor.PBXParser::resolver
	PBXResolver_t144121547 * ___resolver_22;
	// System.String UnityEditor.XCodeEditor.PBXParser::marker
	String_t* ___marker_23;

public:
	inline static int32_t get_offset_of_data_20() { return static_cast<int32_t>(offsetof(PBXParser_t1614426892, ___data_20)); }
	inline CharU5BU5D_t3528271667* get_data_20() const { return ___data_20; }
	inline CharU5BU5D_t3528271667** get_address_of_data_20() { return &___data_20; }
	inline void set_data_20(CharU5BU5D_t3528271667* value)
	{
		___data_20 = value;
		Il2CppCodeGenWriteBarrier((&___data_20), value);
	}

	inline static int32_t get_offset_of_index_21() { return static_cast<int32_t>(offsetof(PBXParser_t1614426892, ___index_21)); }
	inline int32_t get_index_21() const { return ___index_21; }
	inline int32_t* get_address_of_index_21() { return &___index_21; }
	inline void set_index_21(int32_t value)
	{
		___index_21 = value;
	}

	inline static int32_t get_offset_of_resolver_22() { return static_cast<int32_t>(offsetof(PBXParser_t1614426892, ___resolver_22)); }
	inline PBXResolver_t144121547 * get_resolver_22() const { return ___resolver_22; }
	inline PBXResolver_t144121547 ** get_address_of_resolver_22() { return &___resolver_22; }
	inline void set_resolver_22(PBXResolver_t144121547 * value)
	{
		___resolver_22 = value;
		Il2CppCodeGenWriteBarrier((&___resolver_22), value);
	}

	inline static int32_t get_offset_of_marker_23() { return static_cast<int32_t>(offsetof(PBXParser_t1614426892, ___marker_23)); }
	inline String_t* get_marker_23() const { return ___marker_23; }
	inline String_t** get_address_of_marker_23() { return &___marker_23; }
	inline void set_marker_23(String_t* value)
	{
		___marker_23 = value;
		Il2CppCodeGenWriteBarrier((&___marker_23), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBXPARSER_T1614426892_H
#ifndef STRINGBUILDER_T_H
#define STRINGBUILDER_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.StringBuilder
struct  StringBuilder_t  : public RuntimeObject
{
public:
	// System.Int32 System.Text.StringBuilder::_length
	int32_t ____length_1;
	// System.String System.Text.StringBuilder::_str
	String_t* ____str_2;
	// System.String System.Text.StringBuilder::_cached_str
	String_t* ____cached_str_3;
	// System.Int32 System.Text.StringBuilder::_maxCapacity
	int32_t ____maxCapacity_4;

public:
	inline static int32_t get_offset_of__length_1() { return static_cast<int32_t>(offsetof(StringBuilder_t, ____length_1)); }
	inline int32_t get__length_1() const { return ____length_1; }
	inline int32_t* get_address_of__length_1() { return &____length_1; }
	inline void set__length_1(int32_t value)
	{
		____length_1 = value;
	}

	inline static int32_t get_offset_of__str_2() { return static_cast<int32_t>(offsetof(StringBuilder_t, ____str_2)); }
	inline String_t* get__str_2() const { return ____str_2; }
	inline String_t** get_address_of__str_2() { return &____str_2; }
	inline void set__str_2(String_t* value)
	{
		____str_2 = value;
		Il2CppCodeGenWriteBarrier((&____str_2), value);
	}

	inline static int32_t get_offset_of__cached_str_3() { return static_cast<int32_t>(offsetof(StringBuilder_t, ____cached_str_3)); }
	inline String_t* get__cached_str_3() const { return ____cached_str_3; }
	inline String_t** get_address_of__cached_str_3() { return &____cached_str_3; }
	inline void set__cached_str_3(String_t* value)
	{
		____cached_str_3 = value;
		Il2CppCodeGenWriteBarrier((&____cached_str_3), value);
	}

	inline static int32_t get_offset_of__maxCapacity_4() { return static_cast<int32_t>(offsetof(StringBuilder_t, ____maxCapacity_4)); }
	inline int32_t get__maxCapacity_4() const { return ____maxCapacity_4; }
	inline int32_t* get_address_of__maxCapacity_4() { return &____maxCapacity_4; }
	inline void set__maxCapacity_4(int32_t value)
	{
		____maxCapacity_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGBUILDER_T_H
#ifndef MINIJSON_T211875846_H
#define MINIJSON_T211875846_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// XUPorterJSON.MiniJSON
struct  MiniJSON_t211875846  : public RuntimeObject
{
public:

public:
};

struct MiniJSON_t211875846_StaticFields
{
public:
	// System.Int32 XUPorterJSON.MiniJSON::lastErrorIndex
	int32_t ___lastErrorIndex_13;
	// System.String XUPorterJSON.MiniJSON::lastDecode
	String_t* ___lastDecode_14;

public:
	inline static int32_t get_offset_of_lastErrorIndex_13() { return static_cast<int32_t>(offsetof(MiniJSON_t211875846_StaticFields, ___lastErrorIndex_13)); }
	inline int32_t get_lastErrorIndex_13() const { return ___lastErrorIndex_13; }
	inline int32_t* get_address_of_lastErrorIndex_13() { return &___lastErrorIndex_13; }
	inline void set_lastErrorIndex_13(int32_t value)
	{
		___lastErrorIndex_13 = value;
	}

	inline static int32_t get_offset_of_lastDecode_14() { return static_cast<int32_t>(offsetof(MiniJSON_t211875846_StaticFields, ___lastDecode_14)); }
	inline String_t* get_lastDecode_14() const { return ___lastDecode_14; }
	inline String_t** get_address_of_lastDecode_14() { return &___lastDecode_14; }
	inline void set_lastDecode_14(String_t* value)
	{
		___lastDecode_14 = value;
		Il2CppCodeGenWriteBarrier((&___lastDecode_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINIJSON_T211875846_H
#ifndef XCODEPOSTPROCESS_T1429692402_H
#define XCODEPOSTPROCESS_T1429692402_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// XCodePostProcess
struct  XCodePostProcess_t1429692402  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XCODEPOSTPROCESS_T1429692402_H
#ifndef TEXTWRITER_T3478189236_H
#define TEXTWRITER_T3478189236_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.TextWriter
struct  TextWriter_t3478189236  : public RuntimeObject
{
public:
	// System.Char[] System.IO.TextWriter::CoreNewLine
	CharU5BU5D_t3528271667* ___CoreNewLine_0;
	// System.IFormatProvider System.IO.TextWriter::internalFormatProvider
	RuntimeObject* ___internalFormatProvider_1;

public:
	inline static int32_t get_offset_of_CoreNewLine_0() { return static_cast<int32_t>(offsetof(TextWriter_t3478189236, ___CoreNewLine_0)); }
	inline CharU5BU5D_t3528271667* get_CoreNewLine_0() const { return ___CoreNewLine_0; }
	inline CharU5BU5D_t3528271667** get_address_of_CoreNewLine_0() { return &___CoreNewLine_0; }
	inline void set_CoreNewLine_0(CharU5BU5D_t3528271667* value)
	{
		___CoreNewLine_0 = value;
		Il2CppCodeGenWriteBarrier((&___CoreNewLine_0), value);
	}

	inline static int32_t get_offset_of_internalFormatProvider_1() { return static_cast<int32_t>(offsetof(TextWriter_t3478189236, ___internalFormatProvider_1)); }
	inline RuntimeObject* get_internalFormatProvider_1() const { return ___internalFormatProvider_1; }
	inline RuntimeObject** get_address_of_internalFormatProvider_1() { return &___internalFormatProvider_1; }
	inline void set_internalFormatProvider_1(RuntimeObject* value)
	{
		___internalFormatProvider_1 = value;
		Il2CppCodeGenWriteBarrier((&___internalFormatProvider_1), value);
	}
};

struct TextWriter_t3478189236_StaticFields
{
public:
	// System.IO.TextWriter System.IO.TextWriter::Null
	TextWriter_t3478189236 * ___Null_2;

public:
	inline static int32_t get_offset_of_Null_2() { return static_cast<int32_t>(offsetof(TextWriter_t3478189236_StaticFields, ___Null_2)); }
	inline TextWriter_t3478189236 * get_Null_2() const { return ___Null_2; }
	inline TextWriter_t3478189236 ** get_address_of_Null_2() { return &___Null_2; }
	inline void set_Null_2(TextWriter_t3478189236 * value)
	{
		___Null_2 = value;
		Il2CppCodeGenWriteBarrier((&___Null_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTWRITER_T3478189236_H
#ifndef SORTEDDICTIONARY_2_T3336213318_H
#define SORTEDDICTIONARY_2_T3336213318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.SortedDictionary`2<System.String,UnityEditor.XCodeEditor.PBXGroup>
struct  SortedDictionary_2_t3336213318  : public RuntimeObject
{
public:
	// System.Collections.Generic.RBTree System.Collections.Generic.SortedDictionary`2::tree
	RBTree_t4095273678 * ___tree_0;
	// System.Collections.Generic.SortedDictionary`2/NodeHelper<TKey,TValue> System.Collections.Generic.SortedDictionary`2::hlp
	NodeHelper_t1051634704 * ___hlp_1;

public:
	inline static int32_t get_offset_of_tree_0() { return static_cast<int32_t>(offsetof(SortedDictionary_2_t3336213318, ___tree_0)); }
	inline RBTree_t4095273678 * get_tree_0() const { return ___tree_0; }
	inline RBTree_t4095273678 ** get_address_of_tree_0() { return &___tree_0; }
	inline void set_tree_0(RBTree_t4095273678 * value)
	{
		___tree_0 = value;
		Il2CppCodeGenWriteBarrier((&___tree_0), value);
	}

	inline static int32_t get_offset_of_hlp_1() { return static_cast<int32_t>(offsetof(SortedDictionary_2_t3336213318, ___hlp_1)); }
	inline NodeHelper_t1051634704 * get_hlp_1() const { return ___hlp_1; }
	inline NodeHelper_t1051634704 ** get_address_of_hlp_1() { return &___hlp_1; }
	inline void set_hlp_1(NodeHelper_t1051634704 * value)
	{
		___hlp_1 = value;
		Il2CppCodeGenWriteBarrier((&___hlp_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SORTEDDICTIONARY_2_T3336213318_H
#ifndef SORTEDDICTIONARY_2_T1699557551_H
#define SORTEDDICTIONARY_2_T1699557551_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.SortedDictionary`2<System.String,UnityEditor.XCodeEditor.PBXFileReference>
struct  SortedDictionary_2_t1699557551  : public RuntimeObject
{
public:
	// System.Collections.Generic.RBTree System.Collections.Generic.SortedDictionary`2::tree
	RBTree_t4095273678 * ___tree_0;
	// System.Collections.Generic.SortedDictionary`2/NodeHelper<TKey,TValue> System.Collections.Generic.SortedDictionary`2::hlp
	NodeHelper_t3709946233 * ___hlp_1;

public:
	inline static int32_t get_offset_of_tree_0() { return static_cast<int32_t>(offsetof(SortedDictionary_2_t1699557551, ___tree_0)); }
	inline RBTree_t4095273678 * get_tree_0() const { return ___tree_0; }
	inline RBTree_t4095273678 ** get_address_of_tree_0() { return &___tree_0; }
	inline void set_tree_0(RBTree_t4095273678 * value)
	{
		___tree_0 = value;
		Il2CppCodeGenWriteBarrier((&___tree_0), value);
	}

	inline static int32_t get_offset_of_hlp_1() { return static_cast<int32_t>(offsetof(SortedDictionary_2_t1699557551, ___hlp_1)); }
	inline NodeHelper_t3709946233 * get_hlp_1() const { return ___hlp_1; }
	inline NodeHelper_t3709946233 ** get_address_of_hlp_1() { return &___hlp_1; }
	inline void set_hlp_1(NodeHelper_t3709946233 * value)
	{
		___hlp_1 = value;
		Il2CppCodeGenWriteBarrier((&___hlp_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SORTEDDICTIONARY_2_T1699557551_H
#ifndef DICTIONARY_2_T578651261_H
#define DICTIONARY_2_T578651261_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.String,UnityEditor.XCodeEditor.PBXNativeTarget>
struct  Dictionary_2_t578651261  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t385246372* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t964245573* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	StringU5BU5D_t1281789340* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	PBXNativeTargetU5BU5D_t88167* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t950877179 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t578651261, ___table_4)); }
	inline Int32U5BU5D_t385246372* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t385246372** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t385246372* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t578651261, ___linkSlots_5)); }
	inline LinkU5BU5D_t964245573* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t964245573** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t964245573* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t578651261, ___keySlots_6)); }
	inline StringU5BU5D_t1281789340* get_keySlots_6() const { return ___keySlots_6; }
	inline StringU5BU5D_t1281789340** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(StringU5BU5D_t1281789340* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t578651261, ___valueSlots_7)); }
	inline PBXNativeTargetU5BU5D_t88167* get_valueSlots_7() const { return ___valueSlots_7; }
	inline PBXNativeTargetU5BU5D_t88167** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(PBXNativeTargetU5BU5D_t88167* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t578651261, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t578651261, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t578651261, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t578651261, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t578651261, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t578651261, ___serialization_info_13)); }
	inline SerializationInfo_t950877179 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t950877179 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t950877179 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t578651261, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t578651261_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t3145467179 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t578651261_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t3145467179 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t3145467179 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t3145467179 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T578651261_H
#ifndef DICTIONARY_2_T3952567114_H
#define DICTIONARY_2_T3952567114_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.String,UnityEditor.XCodeEditor.PBXCopyFilesBuildPhase>
struct  Dictionary_2_t3952567114  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t385246372* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t964245573* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	StringU5BU5D_t1281789340* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	PBXCopyFilesBuildPhaseU5BU5D_t1750248390* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t950877179 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t3952567114, ___table_4)); }
	inline Int32U5BU5D_t385246372* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t385246372** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t385246372* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t3952567114, ___linkSlots_5)); }
	inline LinkU5BU5D_t964245573* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t964245573** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t964245573* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t3952567114, ___keySlots_6)); }
	inline StringU5BU5D_t1281789340* get_keySlots_6() const { return ___keySlots_6; }
	inline StringU5BU5D_t1281789340** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(StringU5BU5D_t1281789340* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t3952567114, ___valueSlots_7)); }
	inline PBXCopyFilesBuildPhaseU5BU5D_t1750248390* get_valueSlots_7() const { return ___valueSlots_7; }
	inline PBXCopyFilesBuildPhaseU5BU5D_t1750248390** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(PBXCopyFilesBuildPhaseU5BU5D_t1750248390* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t3952567114, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t3952567114, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t3952567114, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t3952567114, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t3952567114, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t3952567114, ___serialization_info_13)); }
	inline SerializationInfo_t950877179 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t950877179 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t950877179 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t3952567114, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t3952567114_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t600660106 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t3952567114_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t600660106 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t600660106 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t600660106 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T3952567114_H
#ifndef DICTIONARY_2_T4268161697_H
#define DICTIONARY_2_T4268161697_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.String,UnityEditor.XCodeEditor.PBXSourcesBuildPhase>
struct  Dictionary_2_t4268161697  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t385246372* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t964245573* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	StringU5BU5D_t1281789340* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	PBXSourcesBuildPhaseU5BU5D_t748958003* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t950877179 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t4268161697, ___table_4)); }
	inline Int32U5BU5D_t385246372* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t385246372** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t385246372* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t4268161697, ___linkSlots_5)); }
	inline LinkU5BU5D_t964245573* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t964245573** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t964245573* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t4268161697, ___keySlots_6)); }
	inline StringU5BU5D_t1281789340* get_keySlots_6() const { return ___keySlots_6; }
	inline StringU5BU5D_t1281789340** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(StringU5BU5D_t1281789340* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t4268161697, ___valueSlots_7)); }
	inline PBXSourcesBuildPhaseU5BU5D_t748958003* get_valueSlots_7() const { return ___valueSlots_7; }
	inline PBXSourcesBuildPhaseU5BU5D_t748958003** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(PBXSourcesBuildPhaseU5BU5D_t748958003* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t4268161697, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t4268161697, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t4268161697, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t4268161697, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t4268161697, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t4268161697, ___serialization_info_13)); }
	inline SerializationInfo_t950877179 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t950877179 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t950877179 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t4268161697, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t4268161697_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t3894337015 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t4268161697_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t3894337015 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t3894337015 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t3894337015 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T4268161697_H
#ifndef DICTIONARY_2_T3312945135_H
#define DICTIONARY_2_T3312945135_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.String,UnityEditor.XCodeEditor.PBXShellScriptBuildPhase>
struct  Dictionary_2_t3312945135  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t385246372* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t964245573* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	StringU5BU5D_t1281789340* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	PBXShellScriptBuildPhaseU5BU5D_t2003070701* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t950877179 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t3312945135, ___table_4)); }
	inline Int32U5BU5D_t385246372* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t385246372** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t385246372* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t3312945135, ___linkSlots_5)); }
	inline LinkU5BU5D_t964245573* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t964245573** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t964245573* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t3312945135, ___keySlots_6)); }
	inline StringU5BU5D_t1281789340* get_keySlots_6() const { return ___keySlots_6; }
	inline StringU5BU5D_t1281789340** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(StringU5BU5D_t1281789340* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t3312945135, ___valueSlots_7)); }
	inline PBXShellScriptBuildPhaseU5BU5D_t2003070701* get_valueSlots_7() const { return ___valueSlots_7; }
	inline PBXShellScriptBuildPhaseU5BU5D_t2003070701** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(PBXShellScriptBuildPhaseU5BU5D_t2003070701* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t3312945135, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t3312945135, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t3312945135, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t3312945135, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t3312945135, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t3312945135, ___serialization_info_13)); }
	inline SerializationInfo_t950877179 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t950877179 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t950877179 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t3312945135, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t3312945135_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t853482417 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t3312945135_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t853482417 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t853482417 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t853482417 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T3312945135_H
#ifndef DICTIONARY_2_T3118589720_H
#define DICTIONARY_2_T3118589720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.String,UnityEditor.XCodeEditor.PBXResourcesBuildPhase>
struct  Dictionary_2_t3118589720  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t385246372* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t964245573* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	StringU5BU5D_t1281789340* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	PBXResourcesBuildPhaseU5BU5D_t1218824448* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t950877179 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t3118589720, ___table_4)); }
	inline Int32U5BU5D_t385246372* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t385246372** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t385246372* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t3118589720, ___linkSlots_5)); }
	inline LinkU5BU5D_t964245573* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t964245573** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t964245573* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t3118589720, ___keySlots_6)); }
	inline StringU5BU5D_t1281789340* get_keySlots_6() const { return ___keySlots_6; }
	inline StringU5BU5D_t1281789340** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(StringU5BU5D_t1281789340* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t3118589720, ___valueSlots_7)); }
	inline PBXResourcesBuildPhaseU5BU5D_t1218824448* get_valueSlots_7() const { return ___valueSlots_7; }
	inline PBXResourcesBuildPhaseU5BU5D_t1218824448** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(PBXResourcesBuildPhaseU5BU5D_t1218824448* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t3118589720, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t3118589720, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t3118589720, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t3118589720, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t3118589720, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t3118589720, ___serialization_info_13)); }
	inline SerializationInfo_t950877179 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t950877179 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t950877179 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t3118589720, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t3118589720_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t69236164 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t3118589720_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t69236164 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t69236164 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t69236164 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T3118589720_H
#ifndef DICTIONARY_2_T1460617709_H
#define DICTIONARY_2_T1460617709_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.String,UnityEditor.XCodeEditor.PBXFrameworksBuildPhase>
struct  Dictionary_2_t1460617709  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t385246372* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t964245573* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	StringU5BU5D_t1281789340* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	PBXFrameworksBuildPhaseU5BU5D_t1911506743* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t950877179 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t1460617709, ___table_4)); }
	inline Int32U5BU5D_t385246372* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t385246372** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t385246372* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t1460617709, ___linkSlots_5)); }
	inline LinkU5BU5D_t964245573* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t964245573** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t964245573* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t1460617709, ___keySlots_6)); }
	inline StringU5BU5D_t1281789340* get_keySlots_6() const { return ___keySlots_6; }
	inline StringU5BU5D_t1281789340** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(StringU5BU5D_t1281789340* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t1460617709, ___valueSlots_7)); }
	inline PBXFrameworksBuildPhaseU5BU5D_t1911506743* get_valueSlots_7() const { return ___valueSlots_7; }
	inline PBXFrameworksBuildPhaseU5BU5D_t1911506743** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(PBXFrameworksBuildPhaseU5BU5D_t1911506743* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t1460617709, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t1460617709, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t1460617709, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t1460617709, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t1460617709, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t1460617709, ___serialization_info_13)); }
	inline SerializationInfo_t950877179 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t950877179 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t950877179 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t1460617709, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t1460617709_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t761918459 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t1460617709_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t761918459 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t761918459 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t761918459 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T1460617709_H
#ifndef DICTIONARY_2_T1236322599_H
#define DICTIONARY_2_T1236322599_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.String,UnityEditor.XCodeEditor.XCBuildConfiguration>
struct  Dictionary_2_t1236322599  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t385246372* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t964245573* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	StringU5BU5D_t1281789340* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	XCBuildConfigurationU5BU5D_t2689903061* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t950877179 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t1236322599, ___table_4)); }
	inline Int32U5BU5D_t385246372* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t385246372** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t385246372* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t1236322599, ___linkSlots_5)); }
	inline LinkU5BU5D_t964245573* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t964245573** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t964245573* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t1236322599, ___keySlots_6)); }
	inline StringU5BU5D_t1281789340* get_keySlots_6() const { return ___keySlots_6; }
	inline StringU5BU5D_t1281789340** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(StringU5BU5D_t1281789340* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t1236322599, ___valueSlots_7)); }
	inline XCBuildConfigurationU5BU5D_t2689903061* get_valueSlots_7() const { return ___valueSlots_7; }
	inline XCBuildConfigurationU5BU5D_t2689903061** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(XCBuildConfigurationU5BU5D_t2689903061* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t1236322599, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t1236322599, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t1236322599, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t1236322599, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t1236322599, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t1236322599, ___serialization_info_13)); }
	inline SerializationInfo_t950877179 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t950877179 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t950877179 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t1236322599, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t1236322599_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t1540314777 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t1236322599_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t1540314777 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t1540314777 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t1540314777 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T1236322599_H
#ifndef URI_T100236324_H
#define URI_T100236324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Uri
struct  Uri_t100236324  : public RuntimeObject
{
public:
	// System.Boolean System.Uri::isUnixFilePath
	bool ___isUnixFilePath_1;
	// System.String System.Uri::source
	String_t* ___source_2;
	// System.String System.Uri::scheme
	String_t* ___scheme_3;
	// System.String System.Uri::host
	String_t* ___host_4;
	// System.Int32 System.Uri::port
	int32_t ___port_5;
	// System.String System.Uri::path
	String_t* ___path_6;
	// System.String System.Uri::query
	String_t* ___query_7;
	// System.String System.Uri::fragment
	String_t* ___fragment_8;
	// System.String System.Uri::userinfo
	String_t* ___userinfo_9;
	// System.Boolean System.Uri::isUnc
	bool ___isUnc_10;
	// System.Boolean System.Uri::isOpaquePart
	bool ___isOpaquePart_11;
	// System.Boolean System.Uri::isAbsoluteUri
	bool ___isAbsoluteUri_12;
	// System.String[] System.Uri::segments
	StringU5BU5D_t1281789340* ___segments_13;
	// System.Boolean System.Uri::userEscaped
	bool ___userEscaped_14;
	// System.String System.Uri::cachedAbsoluteUri
	String_t* ___cachedAbsoluteUri_15;
	// System.String System.Uri::cachedToString
	String_t* ___cachedToString_16;
	// System.String System.Uri::cachedLocalPath
	String_t* ___cachedLocalPath_17;
	// System.Int32 System.Uri::cachedHashCode
	int32_t ___cachedHashCode_18;
	// System.UriParser System.Uri::parser
	UriParser_t3890150400 * ___parser_32;

public:
	inline static int32_t get_offset_of_isUnixFilePath_1() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___isUnixFilePath_1)); }
	inline bool get_isUnixFilePath_1() const { return ___isUnixFilePath_1; }
	inline bool* get_address_of_isUnixFilePath_1() { return &___isUnixFilePath_1; }
	inline void set_isUnixFilePath_1(bool value)
	{
		___isUnixFilePath_1 = value;
	}

	inline static int32_t get_offset_of_source_2() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___source_2)); }
	inline String_t* get_source_2() const { return ___source_2; }
	inline String_t** get_address_of_source_2() { return &___source_2; }
	inline void set_source_2(String_t* value)
	{
		___source_2 = value;
		Il2CppCodeGenWriteBarrier((&___source_2), value);
	}

	inline static int32_t get_offset_of_scheme_3() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___scheme_3)); }
	inline String_t* get_scheme_3() const { return ___scheme_3; }
	inline String_t** get_address_of_scheme_3() { return &___scheme_3; }
	inline void set_scheme_3(String_t* value)
	{
		___scheme_3 = value;
		Il2CppCodeGenWriteBarrier((&___scheme_3), value);
	}

	inline static int32_t get_offset_of_host_4() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___host_4)); }
	inline String_t* get_host_4() const { return ___host_4; }
	inline String_t** get_address_of_host_4() { return &___host_4; }
	inline void set_host_4(String_t* value)
	{
		___host_4 = value;
		Il2CppCodeGenWriteBarrier((&___host_4), value);
	}

	inline static int32_t get_offset_of_port_5() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___port_5)); }
	inline int32_t get_port_5() const { return ___port_5; }
	inline int32_t* get_address_of_port_5() { return &___port_5; }
	inline void set_port_5(int32_t value)
	{
		___port_5 = value;
	}

	inline static int32_t get_offset_of_path_6() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___path_6)); }
	inline String_t* get_path_6() const { return ___path_6; }
	inline String_t** get_address_of_path_6() { return &___path_6; }
	inline void set_path_6(String_t* value)
	{
		___path_6 = value;
		Il2CppCodeGenWriteBarrier((&___path_6), value);
	}

	inline static int32_t get_offset_of_query_7() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___query_7)); }
	inline String_t* get_query_7() const { return ___query_7; }
	inline String_t** get_address_of_query_7() { return &___query_7; }
	inline void set_query_7(String_t* value)
	{
		___query_7 = value;
		Il2CppCodeGenWriteBarrier((&___query_7), value);
	}

	inline static int32_t get_offset_of_fragment_8() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___fragment_8)); }
	inline String_t* get_fragment_8() const { return ___fragment_8; }
	inline String_t** get_address_of_fragment_8() { return &___fragment_8; }
	inline void set_fragment_8(String_t* value)
	{
		___fragment_8 = value;
		Il2CppCodeGenWriteBarrier((&___fragment_8), value);
	}

	inline static int32_t get_offset_of_userinfo_9() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___userinfo_9)); }
	inline String_t* get_userinfo_9() const { return ___userinfo_9; }
	inline String_t** get_address_of_userinfo_9() { return &___userinfo_9; }
	inline void set_userinfo_9(String_t* value)
	{
		___userinfo_9 = value;
		Il2CppCodeGenWriteBarrier((&___userinfo_9), value);
	}

	inline static int32_t get_offset_of_isUnc_10() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___isUnc_10)); }
	inline bool get_isUnc_10() const { return ___isUnc_10; }
	inline bool* get_address_of_isUnc_10() { return &___isUnc_10; }
	inline void set_isUnc_10(bool value)
	{
		___isUnc_10 = value;
	}

	inline static int32_t get_offset_of_isOpaquePart_11() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___isOpaquePart_11)); }
	inline bool get_isOpaquePart_11() const { return ___isOpaquePart_11; }
	inline bool* get_address_of_isOpaquePart_11() { return &___isOpaquePart_11; }
	inline void set_isOpaquePart_11(bool value)
	{
		___isOpaquePart_11 = value;
	}

	inline static int32_t get_offset_of_isAbsoluteUri_12() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___isAbsoluteUri_12)); }
	inline bool get_isAbsoluteUri_12() const { return ___isAbsoluteUri_12; }
	inline bool* get_address_of_isAbsoluteUri_12() { return &___isAbsoluteUri_12; }
	inline void set_isAbsoluteUri_12(bool value)
	{
		___isAbsoluteUri_12 = value;
	}

	inline static int32_t get_offset_of_segments_13() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___segments_13)); }
	inline StringU5BU5D_t1281789340* get_segments_13() const { return ___segments_13; }
	inline StringU5BU5D_t1281789340** get_address_of_segments_13() { return &___segments_13; }
	inline void set_segments_13(StringU5BU5D_t1281789340* value)
	{
		___segments_13 = value;
		Il2CppCodeGenWriteBarrier((&___segments_13), value);
	}

	inline static int32_t get_offset_of_userEscaped_14() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___userEscaped_14)); }
	inline bool get_userEscaped_14() const { return ___userEscaped_14; }
	inline bool* get_address_of_userEscaped_14() { return &___userEscaped_14; }
	inline void set_userEscaped_14(bool value)
	{
		___userEscaped_14 = value;
	}

	inline static int32_t get_offset_of_cachedAbsoluteUri_15() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___cachedAbsoluteUri_15)); }
	inline String_t* get_cachedAbsoluteUri_15() const { return ___cachedAbsoluteUri_15; }
	inline String_t** get_address_of_cachedAbsoluteUri_15() { return &___cachedAbsoluteUri_15; }
	inline void set_cachedAbsoluteUri_15(String_t* value)
	{
		___cachedAbsoluteUri_15 = value;
		Il2CppCodeGenWriteBarrier((&___cachedAbsoluteUri_15), value);
	}

	inline static int32_t get_offset_of_cachedToString_16() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___cachedToString_16)); }
	inline String_t* get_cachedToString_16() const { return ___cachedToString_16; }
	inline String_t** get_address_of_cachedToString_16() { return &___cachedToString_16; }
	inline void set_cachedToString_16(String_t* value)
	{
		___cachedToString_16 = value;
		Il2CppCodeGenWriteBarrier((&___cachedToString_16), value);
	}

	inline static int32_t get_offset_of_cachedLocalPath_17() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___cachedLocalPath_17)); }
	inline String_t* get_cachedLocalPath_17() const { return ___cachedLocalPath_17; }
	inline String_t** get_address_of_cachedLocalPath_17() { return &___cachedLocalPath_17; }
	inline void set_cachedLocalPath_17(String_t* value)
	{
		___cachedLocalPath_17 = value;
		Il2CppCodeGenWriteBarrier((&___cachedLocalPath_17), value);
	}

	inline static int32_t get_offset_of_cachedHashCode_18() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___cachedHashCode_18)); }
	inline int32_t get_cachedHashCode_18() const { return ___cachedHashCode_18; }
	inline int32_t* get_address_of_cachedHashCode_18() { return &___cachedHashCode_18; }
	inline void set_cachedHashCode_18(int32_t value)
	{
		___cachedHashCode_18 = value;
	}

	inline static int32_t get_offset_of_parser_32() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___parser_32)); }
	inline UriParser_t3890150400 * get_parser_32() const { return ___parser_32; }
	inline UriParser_t3890150400 ** get_address_of_parser_32() { return &___parser_32; }
	inline void set_parser_32(UriParser_t3890150400 * value)
	{
		___parser_32 = value;
		Il2CppCodeGenWriteBarrier((&___parser_32), value);
	}
};

struct Uri_t100236324_StaticFields
{
public:
	// System.String System.Uri::hexUpperChars
	String_t* ___hexUpperChars_19;
	// System.String System.Uri::SchemeDelimiter
	String_t* ___SchemeDelimiter_20;
	// System.String System.Uri::UriSchemeFile
	String_t* ___UriSchemeFile_21;
	// System.String System.Uri::UriSchemeFtp
	String_t* ___UriSchemeFtp_22;
	// System.String System.Uri::UriSchemeGopher
	String_t* ___UriSchemeGopher_23;
	// System.String System.Uri::UriSchemeHttp
	String_t* ___UriSchemeHttp_24;
	// System.String System.Uri::UriSchemeHttps
	String_t* ___UriSchemeHttps_25;
	// System.String System.Uri::UriSchemeMailto
	String_t* ___UriSchemeMailto_26;
	// System.String System.Uri::UriSchemeNews
	String_t* ___UriSchemeNews_27;
	// System.String System.Uri::UriSchemeNntp
	String_t* ___UriSchemeNntp_28;
	// System.String System.Uri::UriSchemeNetPipe
	String_t* ___UriSchemeNetPipe_29;
	// System.String System.Uri::UriSchemeNetTcp
	String_t* ___UriSchemeNetTcp_30;
	// System.Uri/UriScheme[] System.Uri::schemes
	UriSchemeU5BU5D_t2082808316* ___schemes_31;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Uri::<>f__switch$map12
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map12_33;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Uri::<>f__switch$map13
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map13_34;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Uri::<>f__switch$map14
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map14_35;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Uri::<>f__switch$map15
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map15_36;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Uri::<>f__switch$map16
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map16_37;

public:
	inline static int32_t get_offset_of_hexUpperChars_19() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___hexUpperChars_19)); }
	inline String_t* get_hexUpperChars_19() const { return ___hexUpperChars_19; }
	inline String_t** get_address_of_hexUpperChars_19() { return &___hexUpperChars_19; }
	inline void set_hexUpperChars_19(String_t* value)
	{
		___hexUpperChars_19 = value;
		Il2CppCodeGenWriteBarrier((&___hexUpperChars_19), value);
	}

	inline static int32_t get_offset_of_SchemeDelimiter_20() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___SchemeDelimiter_20)); }
	inline String_t* get_SchemeDelimiter_20() const { return ___SchemeDelimiter_20; }
	inline String_t** get_address_of_SchemeDelimiter_20() { return &___SchemeDelimiter_20; }
	inline void set_SchemeDelimiter_20(String_t* value)
	{
		___SchemeDelimiter_20 = value;
		Il2CppCodeGenWriteBarrier((&___SchemeDelimiter_20), value);
	}

	inline static int32_t get_offset_of_UriSchemeFile_21() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeFile_21)); }
	inline String_t* get_UriSchemeFile_21() const { return ___UriSchemeFile_21; }
	inline String_t** get_address_of_UriSchemeFile_21() { return &___UriSchemeFile_21; }
	inline void set_UriSchemeFile_21(String_t* value)
	{
		___UriSchemeFile_21 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeFile_21), value);
	}

	inline static int32_t get_offset_of_UriSchemeFtp_22() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeFtp_22)); }
	inline String_t* get_UriSchemeFtp_22() const { return ___UriSchemeFtp_22; }
	inline String_t** get_address_of_UriSchemeFtp_22() { return &___UriSchemeFtp_22; }
	inline void set_UriSchemeFtp_22(String_t* value)
	{
		___UriSchemeFtp_22 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeFtp_22), value);
	}

	inline static int32_t get_offset_of_UriSchemeGopher_23() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeGopher_23)); }
	inline String_t* get_UriSchemeGopher_23() const { return ___UriSchemeGopher_23; }
	inline String_t** get_address_of_UriSchemeGopher_23() { return &___UriSchemeGopher_23; }
	inline void set_UriSchemeGopher_23(String_t* value)
	{
		___UriSchemeGopher_23 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeGopher_23), value);
	}

	inline static int32_t get_offset_of_UriSchemeHttp_24() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeHttp_24)); }
	inline String_t* get_UriSchemeHttp_24() const { return ___UriSchemeHttp_24; }
	inline String_t** get_address_of_UriSchemeHttp_24() { return &___UriSchemeHttp_24; }
	inline void set_UriSchemeHttp_24(String_t* value)
	{
		___UriSchemeHttp_24 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeHttp_24), value);
	}

	inline static int32_t get_offset_of_UriSchemeHttps_25() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeHttps_25)); }
	inline String_t* get_UriSchemeHttps_25() const { return ___UriSchemeHttps_25; }
	inline String_t** get_address_of_UriSchemeHttps_25() { return &___UriSchemeHttps_25; }
	inline void set_UriSchemeHttps_25(String_t* value)
	{
		___UriSchemeHttps_25 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeHttps_25), value);
	}

	inline static int32_t get_offset_of_UriSchemeMailto_26() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeMailto_26)); }
	inline String_t* get_UriSchemeMailto_26() const { return ___UriSchemeMailto_26; }
	inline String_t** get_address_of_UriSchemeMailto_26() { return &___UriSchemeMailto_26; }
	inline void set_UriSchemeMailto_26(String_t* value)
	{
		___UriSchemeMailto_26 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeMailto_26), value);
	}

	inline static int32_t get_offset_of_UriSchemeNews_27() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeNews_27)); }
	inline String_t* get_UriSchemeNews_27() const { return ___UriSchemeNews_27; }
	inline String_t** get_address_of_UriSchemeNews_27() { return &___UriSchemeNews_27; }
	inline void set_UriSchemeNews_27(String_t* value)
	{
		___UriSchemeNews_27 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeNews_27), value);
	}

	inline static int32_t get_offset_of_UriSchemeNntp_28() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeNntp_28)); }
	inline String_t* get_UriSchemeNntp_28() const { return ___UriSchemeNntp_28; }
	inline String_t** get_address_of_UriSchemeNntp_28() { return &___UriSchemeNntp_28; }
	inline void set_UriSchemeNntp_28(String_t* value)
	{
		___UriSchemeNntp_28 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeNntp_28), value);
	}

	inline static int32_t get_offset_of_UriSchemeNetPipe_29() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeNetPipe_29)); }
	inline String_t* get_UriSchemeNetPipe_29() const { return ___UriSchemeNetPipe_29; }
	inline String_t** get_address_of_UriSchemeNetPipe_29() { return &___UriSchemeNetPipe_29; }
	inline void set_UriSchemeNetPipe_29(String_t* value)
	{
		___UriSchemeNetPipe_29 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeNetPipe_29), value);
	}

	inline static int32_t get_offset_of_UriSchemeNetTcp_30() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeNetTcp_30)); }
	inline String_t* get_UriSchemeNetTcp_30() const { return ___UriSchemeNetTcp_30; }
	inline String_t** get_address_of_UriSchemeNetTcp_30() { return &___UriSchemeNetTcp_30; }
	inline void set_UriSchemeNetTcp_30(String_t* value)
	{
		___UriSchemeNetTcp_30 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeNetTcp_30), value);
	}

	inline static int32_t get_offset_of_schemes_31() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___schemes_31)); }
	inline UriSchemeU5BU5D_t2082808316* get_schemes_31() const { return ___schemes_31; }
	inline UriSchemeU5BU5D_t2082808316** get_address_of_schemes_31() { return &___schemes_31; }
	inline void set_schemes_31(UriSchemeU5BU5D_t2082808316* value)
	{
		___schemes_31 = value;
		Il2CppCodeGenWriteBarrier((&___schemes_31), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map12_33() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___U3CU3Ef__switchU24map12_33)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map12_33() const { return ___U3CU3Ef__switchU24map12_33; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map12_33() { return &___U3CU3Ef__switchU24map12_33; }
	inline void set_U3CU3Ef__switchU24map12_33(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map12_33 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map12_33), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map13_34() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___U3CU3Ef__switchU24map13_34)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map13_34() const { return ___U3CU3Ef__switchU24map13_34; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map13_34() { return &___U3CU3Ef__switchU24map13_34; }
	inline void set_U3CU3Ef__switchU24map13_34(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map13_34 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map13_34), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map14_35() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___U3CU3Ef__switchU24map14_35)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map14_35() const { return ___U3CU3Ef__switchU24map14_35; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map14_35() { return &___U3CU3Ef__switchU24map14_35; }
	inline void set_U3CU3Ef__switchU24map14_35(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map14_35 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map14_35), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map15_36() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___U3CU3Ef__switchU24map15_36)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map15_36() const { return ___U3CU3Ef__switchU24map15_36; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map15_36() { return &___U3CU3Ef__switchU24map15_36; }
	inline void set_U3CU3Ef__switchU24map15_36(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map15_36 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map15_36), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map16_37() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___U3CU3Ef__switchU24map16_37)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map16_37() const { return ___U3CU3Ef__switchU24map16_37; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map16_37() { return &___U3CU3Ef__switchU24map16_37; }
	inline void set_U3CU3Ef__switchU24map16_37(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map16_37 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map16_37), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URI_T100236324_H
#ifndef LIST_1_T257213610_H
#define LIST_1_T257213610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Object>
struct  List_1_t257213610  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_t2843939325* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t257213610, ____items_1)); }
	inline ObjectU5BU5D_t2843939325* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_t2843939325** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_t2843939325* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t257213610, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t257213610, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t257213610_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	ObjectU5BU5D_t2843939325* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t257213610_StaticFields, ___EmptyArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T257213610_H
#ifndef DICTIONARY_2_T2865362463_H
#define DICTIONARY_2_T2865362463_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct  Dictionary_2_t2865362463  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t385246372* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t964245573* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	StringU5BU5D_t1281789340* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	ObjectU5BU5D_t2843939325* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t950877179 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___table_4)); }
	inline Int32U5BU5D_t385246372* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t385246372** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t385246372* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___linkSlots_5)); }
	inline LinkU5BU5D_t964245573* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t964245573** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t964245573* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___keySlots_6)); }
	inline StringU5BU5D_t1281789340* get_keySlots_6() const { return ___keySlots_6; }
	inline StringU5BU5D_t1281789340** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(StringU5BU5D_t1281789340* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___valueSlots_7)); }
	inline ObjectU5BU5D_t2843939325* get_valueSlots_7() const { return ___valueSlots_7; }
	inline ObjectU5BU5D_t2843939325** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(ObjectU5BU5D_t2843939325* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___serialization_info_13)); }
	inline SerializationInfo_t950877179 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t950877179 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t950877179 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t2865362463_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t1694351041 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t1694351041 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t1694351041 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t1694351041 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T2865362463_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t4013366056* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t4013366056* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t4013366056* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t, ___inner_exception_1)); }
	inline Exception_t * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T_H
#ifndef XCPLIST_T2540119850_H
#define XCPLIST_T2540119850_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.XCPlist
struct  XCPlist_t2540119850  : public RuntimeObject
{
public:
	// System.String UnityEditor.XCodeEditor.XCPlist::plistPath
	String_t* ___plistPath_0;
	// System.Boolean UnityEditor.XCodeEditor.XCPlist::plistModified
	bool ___plistModified_1;

public:
	inline static int32_t get_offset_of_plistPath_0() { return static_cast<int32_t>(offsetof(XCPlist_t2540119850, ___plistPath_0)); }
	inline String_t* get_plistPath_0() const { return ___plistPath_0; }
	inline String_t** get_address_of_plistPath_0() { return &___plistPath_0; }
	inline void set_plistPath_0(String_t* value)
	{
		___plistPath_0 = value;
		Il2CppCodeGenWriteBarrier((&___plistPath_0), value);
	}

	inline static int32_t get_offset_of_plistModified_1() { return static_cast<int32_t>(offsetof(XCPlist_t2540119850, ___plistModified_1)); }
	inline bool get_plistModified_1() const { return ___plistModified_1; }
	inline bool* get_address_of_plistModified_1() { return &___plistModified_1; }
	inline void set_plistModified_1(bool value)
	{
		___plistModified_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XCPLIST_T2540119850_H
#ifndef PBXOBJECT_T3397571057_H
#define PBXOBJECT_T3397571057_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.PBXObject
struct  PBXObject_t3397571057  : public RuntimeObject
{
public:
	// System.String UnityEditor.XCodeEditor.PBXObject::_guid
	String_t* ____guid_1;
	// UnityEditor.XCodeEditor.PBXDictionary UnityEditor.XCodeEditor.PBXObject::_data
	PBXDictionary_t2078602241 * ____data_2;

public:
	inline static int32_t get_offset_of__guid_1() { return static_cast<int32_t>(offsetof(PBXObject_t3397571057, ____guid_1)); }
	inline String_t* get__guid_1() const { return ____guid_1; }
	inline String_t** get_address_of__guid_1() { return &____guid_1; }
	inline void set__guid_1(String_t* value)
	{
		____guid_1 = value;
		Il2CppCodeGenWriteBarrier((&____guid_1), value);
	}

	inline static int32_t get_offset_of__data_2() { return static_cast<int32_t>(offsetof(PBXObject_t3397571057, ____data_2)); }
	inline PBXDictionary_t2078602241 * get__data_2() const { return ____data_2; }
	inline PBXDictionary_t2078602241 ** get_address_of__data_2() { return &____data_2; }
	inline void set__data_2(PBXDictionary_t2078602241 * value)
	{
		____data_2 = value;
		Il2CppCodeGenWriteBarrier((&____data_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBXOBJECT_T3397571057_H
#ifndef HASHTABLE_T1853889766_H
#define HASHTABLE_T1853889766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Hashtable
struct  Hashtable_t1853889766  : public RuntimeObject
{
public:
	// System.Int32 System.Collections.Hashtable::inUse
	int32_t ___inUse_1;
	// System.Int32 System.Collections.Hashtable::modificationCount
	int32_t ___modificationCount_2;
	// System.Single System.Collections.Hashtable::loadFactor
	float ___loadFactor_3;
	// System.Collections.Hashtable/Slot[] System.Collections.Hashtable::table
	SlotU5BU5D_t2994659099* ___table_4;
	// System.Int32[] System.Collections.Hashtable::hashes
	Int32U5BU5D_t385246372* ___hashes_5;
	// System.Int32 System.Collections.Hashtable::threshold
	int32_t ___threshold_6;
	// System.Collections.Hashtable/HashKeys System.Collections.Hashtable::hashKeys
	HashKeys_t1568156503 * ___hashKeys_7;
	// System.Collections.Hashtable/HashValues System.Collections.Hashtable::hashValues
	HashValues_t618387445 * ___hashValues_8;
	// System.Collections.IHashCodeProvider System.Collections.Hashtable::hcpRef
	RuntimeObject* ___hcpRef_9;
	// System.Collections.IComparer System.Collections.Hashtable::comparerRef
	RuntimeObject* ___comparerRef_10;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Hashtable::serializationInfo
	SerializationInfo_t950877179 * ___serializationInfo_11;
	// System.Collections.IEqualityComparer System.Collections.Hashtable::equalityComparer
	RuntimeObject* ___equalityComparer_12;

public:
	inline static int32_t get_offset_of_inUse_1() { return static_cast<int32_t>(offsetof(Hashtable_t1853889766, ___inUse_1)); }
	inline int32_t get_inUse_1() const { return ___inUse_1; }
	inline int32_t* get_address_of_inUse_1() { return &___inUse_1; }
	inline void set_inUse_1(int32_t value)
	{
		___inUse_1 = value;
	}

	inline static int32_t get_offset_of_modificationCount_2() { return static_cast<int32_t>(offsetof(Hashtable_t1853889766, ___modificationCount_2)); }
	inline int32_t get_modificationCount_2() const { return ___modificationCount_2; }
	inline int32_t* get_address_of_modificationCount_2() { return &___modificationCount_2; }
	inline void set_modificationCount_2(int32_t value)
	{
		___modificationCount_2 = value;
	}

	inline static int32_t get_offset_of_loadFactor_3() { return static_cast<int32_t>(offsetof(Hashtable_t1853889766, ___loadFactor_3)); }
	inline float get_loadFactor_3() const { return ___loadFactor_3; }
	inline float* get_address_of_loadFactor_3() { return &___loadFactor_3; }
	inline void set_loadFactor_3(float value)
	{
		___loadFactor_3 = value;
	}

	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Hashtable_t1853889766, ___table_4)); }
	inline SlotU5BU5D_t2994659099* get_table_4() const { return ___table_4; }
	inline SlotU5BU5D_t2994659099** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(SlotU5BU5D_t2994659099* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_hashes_5() { return static_cast<int32_t>(offsetof(Hashtable_t1853889766, ___hashes_5)); }
	inline Int32U5BU5D_t385246372* get_hashes_5() const { return ___hashes_5; }
	inline Int32U5BU5D_t385246372** get_address_of_hashes_5() { return &___hashes_5; }
	inline void set_hashes_5(Int32U5BU5D_t385246372* value)
	{
		___hashes_5 = value;
		Il2CppCodeGenWriteBarrier((&___hashes_5), value);
	}

	inline static int32_t get_offset_of_threshold_6() { return static_cast<int32_t>(offsetof(Hashtable_t1853889766, ___threshold_6)); }
	inline int32_t get_threshold_6() const { return ___threshold_6; }
	inline int32_t* get_address_of_threshold_6() { return &___threshold_6; }
	inline void set_threshold_6(int32_t value)
	{
		___threshold_6 = value;
	}

	inline static int32_t get_offset_of_hashKeys_7() { return static_cast<int32_t>(offsetof(Hashtable_t1853889766, ___hashKeys_7)); }
	inline HashKeys_t1568156503 * get_hashKeys_7() const { return ___hashKeys_7; }
	inline HashKeys_t1568156503 ** get_address_of_hashKeys_7() { return &___hashKeys_7; }
	inline void set_hashKeys_7(HashKeys_t1568156503 * value)
	{
		___hashKeys_7 = value;
		Il2CppCodeGenWriteBarrier((&___hashKeys_7), value);
	}

	inline static int32_t get_offset_of_hashValues_8() { return static_cast<int32_t>(offsetof(Hashtable_t1853889766, ___hashValues_8)); }
	inline HashValues_t618387445 * get_hashValues_8() const { return ___hashValues_8; }
	inline HashValues_t618387445 ** get_address_of_hashValues_8() { return &___hashValues_8; }
	inline void set_hashValues_8(HashValues_t618387445 * value)
	{
		___hashValues_8 = value;
		Il2CppCodeGenWriteBarrier((&___hashValues_8), value);
	}

	inline static int32_t get_offset_of_hcpRef_9() { return static_cast<int32_t>(offsetof(Hashtable_t1853889766, ___hcpRef_9)); }
	inline RuntimeObject* get_hcpRef_9() const { return ___hcpRef_9; }
	inline RuntimeObject** get_address_of_hcpRef_9() { return &___hcpRef_9; }
	inline void set_hcpRef_9(RuntimeObject* value)
	{
		___hcpRef_9 = value;
		Il2CppCodeGenWriteBarrier((&___hcpRef_9), value);
	}

	inline static int32_t get_offset_of_comparerRef_10() { return static_cast<int32_t>(offsetof(Hashtable_t1853889766, ___comparerRef_10)); }
	inline RuntimeObject* get_comparerRef_10() const { return ___comparerRef_10; }
	inline RuntimeObject** get_address_of_comparerRef_10() { return &___comparerRef_10; }
	inline void set_comparerRef_10(RuntimeObject* value)
	{
		___comparerRef_10 = value;
		Il2CppCodeGenWriteBarrier((&___comparerRef_10), value);
	}

	inline static int32_t get_offset_of_serializationInfo_11() { return static_cast<int32_t>(offsetof(Hashtable_t1853889766, ___serializationInfo_11)); }
	inline SerializationInfo_t950877179 * get_serializationInfo_11() const { return ___serializationInfo_11; }
	inline SerializationInfo_t950877179 ** get_address_of_serializationInfo_11() { return &___serializationInfo_11; }
	inline void set_serializationInfo_11(SerializationInfo_t950877179 * value)
	{
		___serializationInfo_11 = value;
		Il2CppCodeGenWriteBarrier((&___serializationInfo_11), value);
	}

	inline static int32_t get_offset_of_equalityComparer_12() { return static_cast<int32_t>(offsetof(Hashtable_t1853889766, ___equalityComparer_12)); }
	inline RuntimeObject* get_equalityComparer_12() const { return ___equalityComparer_12; }
	inline RuntimeObject** get_address_of_equalityComparer_12() { return &___equalityComparer_12; }
	inline void set_equalityComparer_12(RuntimeObject* value)
	{
		___equalityComparer_12 = value;
		Il2CppCodeGenWriteBarrier((&___equalityComparer_12), value);
	}
};

struct Hashtable_t1853889766_StaticFields
{
public:
	// System.Int32[] System.Collections.Hashtable::primeTbl
	Int32U5BU5D_t385246372* ___primeTbl_13;

public:
	inline static int32_t get_offset_of_primeTbl_13() { return static_cast<int32_t>(offsetof(Hashtable_t1853889766_StaticFields, ___primeTbl_13)); }
	inline Int32U5BU5D_t385246372* get_primeTbl_13() const { return ___primeTbl_13; }
	inline Int32U5BU5D_t385246372** get_address_of_primeTbl_13() { return &___primeTbl_13; }
	inline void set_primeTbl_13(Int32U5BU5D_t385246372* value)
	{
		___primeTbl_13 = value;
		Il2CppCodeGenWriteBarrier((&___primeTbl_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHTABLE_T1853889766_H
#ifndef DICTIONARY_2_T1632706988_H
#define DICTIONARY_2_T1632706988_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct  Dictionary_2_t1632706988  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t385246372* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t964245573* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	StringU5BU5D_t1281789340* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	StringU5BU5D_t1281789340* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t950877179 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___table_4)); }
	inline Int32U5BU5D_t385246372* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t385246372** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t385246372* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___linkSlots_5)); }
	inline LinkU5BU5D_t964245573* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t964245573** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t964245573* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___keySlots_6)); }
	inline StringU5BU5D_t1281789340* get_keySlots_6() const { return ___keySlots_6; }
	inline StringU5BU5D_t1281789340** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(StringU5BU5D_t1281789340* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___valueSlots_7)); }
	inline StringU5BU5D_t1281789340* get_valueSlots_7() const { return ___valueSlots_7; }
	inline StringU5BU5D_t1281789340** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(StringU5BU5D_t1281789340* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___serialization_info_13)); }
	inline SerializationInfo_t950877179 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t950877179 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t950877179 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t1632706988_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t132201056 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t132201056 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t132201056 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t132201056 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T1632706988_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef DICTIONARY_2_T2749540806_H
#define DICTIONARY_2_T2749540806_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.String,UnityEditor.XCodeEditor.PBXVariantGroup>
struct  Dictionary_2_t2749540806  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t385246372* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t964245573* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	StringU5BU5D_t1281789340* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	PBXVariantGroupU5BU5D_t409628634* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t950877179 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t2749540806, ___table_4)); }
	inline Int32U5BU5D_t385246372* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t385246372** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t385246372* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t2749540806, ___linkSlots_5)); }
	inline LinkU5BU5D_t964245573* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t964245573** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t964245573* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t2749540806, ___keySlots_6)); }
	inline StringU5BU5D_t1281789340* get_keySlots_6() const { return ___keySlots_6; }
	inline StringU5BU5D_t1281789340** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(StringU5BU5D_t1281789340* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t2749540806, ___valueSlots_7)); }
	inline PBXVariantGroupU5BU5D_t409628634* get_valueSlots_7() const { return ___valueSlots_7; }
	inline PBXVariantGroupU5BU5D_t409628634** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(PBXVariantGroupU5BU5D_t409628634* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t2749540806, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t2749540806, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t2749540806, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t2749540806, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t2749540806, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t2749540806, ___serialization_info_13)); }
	inline SerializationInfo_t950877179 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t950877179 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t950877179 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t2749540806, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t2749540806_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t3555007646 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t2749540806_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t3555007646 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t3555007646 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t3555007646 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T2749540806_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t3528271667* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t3528271667* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t3528271667** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t3528271667* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef SORTEDDICTIONARY_2_T3818169288_H
#define SORTEDDICTIONARY_2_T3818169288_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.SortedDictionary`2<System.String,UnityEditor.XCodeEditor.PBXBuildFile>
struct  SortedDictionary_2_t3818169288  : public RuntimeObject
{
public:
	// System.Collections.Generic.RBTree System.Collections.Generic.SortedDictionary`2::tree
	RBTree_t4095273678 * ___tree_0;
	// System.Collections.Generic.SortedDictionary`2/NodeHelper<TKey,TValue> System.Collections.Generic.SortedDictionary`2::hlp
	NodeHelper_t1533590674 * ___hlp_1;

public:
	inline static int32_t get_offset_of_tree_0() { return static_cast<int32_t>(offsetof(SortedDictionary_2_t3818169288, ___tree_0)); }
	inline RBTree_t4095273678 * get_tree_0() const { return ___tree_0; }
	inline RBTree_t4095273678 ** get_address_of_tree_0() { return &___tree_0; }
	inline void set_tree_0(RBTree_t4095273678 * value)
	{
		___tree_0 = value;
		Il2CppCodeGenWriteBarrier((&___tree_0), value);
	}

	inline static int32_t get_offset_of_hlp_1() { return static_cast<int32_t>(offsetof(SortedDictionary_2_t3818169288, ___hlp_1)); }
	inline NodeHelper_t1533590674 * get_hlp_1() const { return ___hlp_1; }
	inline NodeHelper_t1533590674 ** get_address_of_hlp_1() { return &___hlp_1; }
	inline void set_hlp_1(NodeHelper_t1533590674 * value)
	{
		___hlp_1 = value;
		Il2CppCodeGenWriteBarrier((&___hlp_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SORTEDDICTIONARY_2_T3818169288_H
#ifndef XCMOD_T1014962890_H
#define XCMOD_T1014962890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.XCMod
struct  XCMod_t1014962890  : public RuntimeObject
{
public:
	// System.Collections.Hashtable UnityEditor.XCodeEditor.XCMod::_datastore
	Hashtable_t1853889766 * ____datastore_0;
	// System.Collections.ArrayList UnityEditor.XCodeEditor.XCMod::_libs
	ArrayList_t2718874744 * ____libs_1;
	// System.String UnityEditor.XCodeEditor.XCMod::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_2;
	// System.String UnityEditor.XCodeEditor.XCMod::<path>k__BackingField
	String_t* ___U3CpathU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of__datastore_0() { return static_cast<int32_t>(offsetof(XCMod_t1014962890, ____datastore_0)); }
	inline Hashtable_t1853889766 * get__datastore_0() const { return ____datastore_0; }
	inline Hashtable_t1853889766 ** get_address_of__datastore_0() { return &____datastore_0; }
	inline void set__datastore_0(Hashtable_t1853889766 * value)
	{
		____datastore_0 = value;
		Il2CppCodeGenWriteBarrier((&____datastore_0), value);
	}

	inline static int32_t get_offset_of__libs_1() { return static_cast<int32_t>(offsetof(XCMod_t1014962890, ____libs_1)); }
	inline ArrayList_t2718874744 * get__libs_1() const { return ____libs_1; }
	inline ArrayList_t2718874744 ** get_address_of__libs_1() { return &____libs_1; }
	inline void set__libs_1(ArrayList_t2718874744 * value)
	{
		____libs_1 = value;
		Il2CppCodeGenWriteBarrier((&____libs_1), value);
	}

	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(XCMod_t1014962890, ___U3CnameU3Ek__BackingField_2)); }
	inline String_t* get_U3CnameU3Ek__BackingField_2() const { return ___U3CnameU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_2() { return &___U3CnameU3Ek__BackingField_2; }
	inline void set_U3CnameU3Ek__BackingField_2(String_t* value)
	{
		___U3CnameU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CpathU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(XCMod_t1014962890, ___U3CpathU3Ek__BackingField_3)); }
	inline String_t* get_U3CpathU3Ek__BackingField_3() const { return ___U3CpathU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CpathU3Ek__BackingField_3() { return &___U3CpathU3Ek__BackingField_3; }
	inline void set_U3CpathU3Ek__BackingField_3(String_t* value)
	{
		___U3CpathU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpathU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XCMOD_T1014962890_H
#ifndef SORTEDDICTIONARY_2_T2308306618_H
#define SORTEDDICTIONARY_2_T2308306618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.SortedDictionary`2<System.String,UnityEditor.XCodeEditor.XCConfigurationList>
struct  SortedDictionary_2_t2308306618  : public RuntimeObject
{
public:
	// System.Collections.Generic.RBTree System.Collections.Generic.SortedDictionary`2::tree
	RBTree_t4095273678 * ___tree_0;
	// System.Collections.Generic.SortedDictionary`2/NodeHelper<TKey,TValue> System.Collections.Generic.SortedDictionary`2::hlp
	NodeHelper_t23728004 * ___hlp_1;

public:
	inline static int32_t get_offset_of_tree_0() { return static_cast<int32_t>(offsetof(SortedDictionary_2_t2308306618, ___tree_0)); }
	inline RBTree_t4095273678 * get_tree_0() const { return ___tree_0; }
	inline RBTree_t4095273678 ** get_address_of_tree_0() { return &___tree_0; }
	inline void set_tree_0(RBTree_t4095273678 * value)
	{
		___tree_0 = value;
		Il2CppCodeGenWriteBarrier((&___tree_0), value);
	}

	inline static int32_t get_offset_of_hlp_1() { return static_cast<int32_t>(offsetof(SortedDictionary_2_t2308306618, ___hlp_1)); }
	inline NodeHelper_t23728004 * get_hlp_1() const { return ___hlp_1; }
	inline NodeHelper_t23728004 ** get_address_of_hlp_1() { return &___hlp_1; }
	inline void set_hlp_1(NodeHelper_t23728004 * value)
	{
		___hlp_1 = value;
		Il2CppCodeGenWriteBarrier((&___hlp_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SORTEDDICTIONARY_2_T2308306618_H
#ifndef TEXTREADER_T283511965_H
#define TEXTREADER_T283511965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.TextReader
struct  TextReader_t283511965  : public RuntimeObject
{
public:

public:
};

struct TextReader_t283511965_StaticFields
{
public:
	// System.IO.TextReader System.IO.TextReader::Null
	TextReader_t283511965 * ___Null_0;

public:
	inline static int32_t get_offset_of_Null_0() { return static_cast<int32_t>(offsetof(TextReader_t283511965_StaticFields, ___Null_0)); }
	inline TextReader_t283511965 * get_Null_0() const { return ___Null_0; }
	inline TextReader_t283511965 ** get_address_of_Null_0() { return &___Null_0; }
	inline void set_Null_0(TextReader_t283511965 * value)
	{
		___Null_0 = value;
		Il2CppCodeGenWriteBarrier((&___Null_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTREADER_T283511965_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef MINIJSONEXTENSIONS_T1989422878_H
#define MINIJSONEXTENSIONS_T1989422878_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// XUPorterJSON.MiniJsonExtensions
struct  MiniJsonExtensions_t1989422878  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINIJSONEXTENSIONS_T1989422878_H
#ifndef ARRAYLIST_T2718874744_H
#define ARRAYLIST_T2718874744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.ArrayList
struct  ArrayList_t2718874744  : public RuntimeObject
{
public:
	// System.Int32 System.Collections.ArrayList::_size
	int32_t ____size_1;
	// System.Object[] System.Collections.ArrayList::_items
	ObjectU5BU5D_t2843939325* ____items_2;
	// System.Int32 System.Collections.ArrayList::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__size_1() { return static_cast<int32_t>(offsetof(ArrayList_t2718874744, ____size_1)); }
	inline int32_t get__size_1() const { return ____size_1; }
	inline int32_t* get_address_of__size_1() { return &____size_1; }
	inline void set__size_1(int32_t value)
	{
		____size_1 = value;
	}

	inline static int32_t get_offset_of__items_2() { return static_cast<int32_t>(offsetof(ArrayList_t2718874744, ____items_2)); }
	inline ObjectU5BU5D_t2843939325* get__items_2() const { return ____items_2; }
	inline ObjectU5BU5D_t2843939325** get_address_of__items_2() { return &____items_2; }
	inline void set__items_2(ObjectU5BU5D_t2843939325* value)
	{
		____items_2 = value;
		Il2CppCodeGenWriteBarrier((&____items_2), value);
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(ArrayList_t2718874744, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct ArrayList_t2718874744_StaticFields
{
public:
	// System.Object[] System.Collections.ArrayList::EmptyArray
	ObjectU5BU5D_t2843939325* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(ArrayList_t2718874744_StaticFields, ___EmptyArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYLIST_T2718874744_H
#ifndef XCMODFILE_T1016819465_H
#define XCMODFILE_T1016819465_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.XCModFile
struct  XCModFile_t1016819465  : public RuntimeObject
{
public:
	// System.String UnityEditor.XCodeEditor.XCModFile::<filePath>k__BackingField
	String_t* ___U3CfilePathU3Ek__BackingField_0;
	// System.Boolean UnityEditor.XCodeEditor.XCModFile::<isWeak>k__BackingField
	bool ___U3CisWeakU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CfilePathU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(XCModFile_t1016819465, ___U3CfilePathU3Ek__BackingField_0)); }
	inline String_t* get_U3CfilePathU3Ek__BackingField_0() const { return ___U3CfilePathU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CfilePathU3Ek__BackingField_0() { return &___U3CfilePathU3Ek__BackingField_0; }
	inline void set_U3CfilePathU3Ek__BackingField_0(String_t* value)
	{
		___U3CfilePathU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CfilePathU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CisWeakU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(XCModFile_t1016819465, ___U3CisWeakU3Ek__BackingField_1)); }
	inline bool get_U3CisWeakU3Ek__BackingField_1() const { return ___U3CisWeakU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CisWeakU3Ek__BackingField_1() { return &___U3CisWeakU3Ek__BackingField_1; }
	inline void set_U3CisWeakU3Ek__BackingField_1(bool value)
	{
		___U3CisWeakU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XCMODFILE_T1016819465_H
#ifndef MARSHALBYREFOBJECT_T2760389100_H
#define MARSHALBYREFOBJECT_T2760389100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_t2760389100  : public RuntimeObject
{
public:
	// System.Runtime.Remoting.ServerIdentity System.MarshalByRefObject::_identity
	ServerIdentity_t2342208608 * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_t2760389100, ____identity_0)); }
	inline ServerIdentity_t2342208608 * get__identity_0() const { return ____identity_0; }
	inline ServerIdentity_t2342208608 ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(ServerIdentity_t2342208608 * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARSHALBYREFOBJECT_T2760389100_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef KEYVALUEPAIR_2_T2370866568_H
#define KEYVALUEPAIR_2_T2370866568_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.String,UnityEditor.XCodeEditor.PBXSourcesBuildPhase>
struct  KeyValuePair_2_t2370866568 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	PBXSourcesBuildPhase_t187938102 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2370866568, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2370866568, ___value_1)); }
	inline PBXSourcesBuildPhase_t187938102 * get_value_1() const { return ___value_1; }
	inline PBXSourcesBuildPhase_t187938102 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(PBXSourcesBuildPhase_t187938102 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2370866568_H
#ifndef KEYVALUEPAIR_2_T1415650006_H
#define KEYVALUEPAIR_2_T1415650006_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.String,UnityEditor.XCodeEditor.PBXShellScriptBuildPhase>
struct  KeyValuePair_2_t1415650006 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	PBXShellScriptBuildPhase_t3527688836 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1415650006, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1415650006, ___value_1)); }
	inline PBXShellScriptBuildPhase_t3527688836 * get_value_1() const { return ___value_1; }
	inline PBXShellScriptBuildPhase_t3527688836 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(PBXShellScriptBuildPhase_t3527688836 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T1415650006_H
#ifndef KEYVALUEPAIR_2_T2055271985_H
#define KEYVALUEPAIR_2_T2055271985_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.String,UnityEditor.XCodeEditor.PBXCopyFilesBuildPhase>
struct  KeyValuePair_2_t2055271985 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	PBXCopyFilesBuildPhase_t4167310815 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2055271985, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2055271985, ___value_1)); }
	inline PBXCopyFilesBuildPhase_t4167310815 * get_value_1() const { return ___value_1; }
	inline PBXCopyFilesBuildPhase_t4167310815 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(PBXCopyFilesBuildPhase_t4167310815 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2055271985_H
#ifndef PBXBUILDPHASE_T3890765868_H
#define PBXBUILDPHASE_T3890765868_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.PBXBuildPhase
struct  PBXBuildPhase_t3890765868  : public PBXObject_t3397571057
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBXBUILDPHASE_T3890765868_H
#ifndef KEYVALUEPAIR_2_T2976323428_H
#define KEYVALUEPAIR_2_T2976323428_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.String,UnityEditor.XCodeEditor.PBXNativeTarget>
struct  KeyValuePair_2_t2976323428 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	PBXNativeTarget_t793394962 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2976323428, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2976323428, ___value_1)); }
	inline PBXNativeTarget_t793394962 * get_value_1() const { return ___value_1; }
	inline PBXNativeTarget_t793394962 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(PBXNativeTarget_t793394962 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2976323428_H
#ifndef PBXBUILDFILE_T2610392694_H
#define PBXBUILDFILE_T2610392694_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.PBXBuildFile
struct  PBXBuildFile_t2610392694  : public PBXObject_t3397571057
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBXBUILDFILE_T2610392694_H
#ifndef KEYVALUEPAIR_2_T2674709423_H
#define KEYVALUEPAIR_2_T2674709423_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.String,UnityEditor.XCodeEditor.PBXFileReference>
struct  KeyValuePair_2_t2674709423 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	PBXFileReference_t491780957 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2674709423, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2674709423, ___value_1)); }
	inline PBXFileReference_t491780957 * get_value_1() const { return ___value_1; }
	inline PBXFileReference_t491780957 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(PBXFileReference_t491780957 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2674709423_H
#ifndef KEYVALUEPAIR_2_T16397894_H
#define KEYVALUEPAIR_2_T16397894_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.String,UnityEditor.XCodeEditor.PBXGroup>
struct  KeyValuePair_2_t16397894 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	PBXGroup_t2128436724 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t16397894, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t16397894, ___value_1)); }
	inline PBXGroup_t2128436724 * get_value_1() const { return ___value_1; }
	inline PBXGroup_t2128436724 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(PBXGroup_t2128436724 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T16397894_H
#ifndef STREAMWRITER_T1266378904_H
#define STREAMWRITER_T1266378904_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.StreamWriter
struct  StreamWriter_t1266378904  : public TextWriter_t3478189236
{
public:
	// System.Text.Encoding System.IO.StreamWriter::internalEncoding
	Encoding_t1523322056 * ___internalEncoding_3;
	// System.IO.Stream System.IO.StreamWriter::internalStream
	Stream_t1273022909 * ___internalStream_4;
	// System.Boolean System.IO.StreamWriter::iflush
	bool ___iflush_5;
	// System.Byte[] System.IO.StreamWriter::byte_buf
	ByteU5BU5D_t4116647657* ___byte_buf_6;
	// System.Int32 System.IO.StreamWriter::byte_pos
	int32_t ___byte_pos_7;
	// System.Char[] System.IO.StreamWriter::decode_buf
	CharU5BU5D_t3528271667* ___decode_buf_8;
	// System.Int32 System.IO.StreamWriter::decode_pos
	int32_t ___decode_pos_9;
	// System.Boolean System.IO.StreamWriter::DisposedAlready
	bool ___DisposedAlready_10;
	// System.Boolean System.IO.StreamWriter::preamble_done
	bool ___preamble_done_11;

public:
	inline static int32_t get_offset_of_internalEncoding_3() { return static_cast<int32_t>(offsetof(StreamWriter_t1266378904, ___internalEncoding_3)); }
	inline Encoding_t1523322056 * get_internalEncoding_3() const { return ___internalEncoding_3; }
	inline Encoding_t1523322056 ** get_address_of_internalEncoding_3() { return &___internalEncoding_3; }
	inline void set_internalEncoding_3(Encoding_t1523322056 * value)
	{
		___internalEncoding_3 = value;
		Il2CppCodeGenWriteBarrier((&___internalEncoding_3), value);
	}

	inline static int32_t get_offset_of_internalStream_4() { return static_cast<int32_t>(offsetof(StreamWriter_t1266378904, ___internalStream_4)); }
	inline Stream_t1273022909 * get_internalStream_4() const { return ___internalStream_4; }
	inline Stream_t1273022909 ** get_address_of_internalStream_4() { return &___internalStream_4; }
	inline void set_internalStream_4(Stream_t1273022909 * value)
	{
		___internalStream_4 = value;
		Il2CppCodeGenWriteBarrier((&___internalStream_4), value);
	}

	inline static int32_t get_offset_of_iflush_5() { return static_cast<int32_t>(offsetof(StreamWriter_t1266378904, ___iflush_5)); }
	inline bool get_iflush_5() const { return ___iflush_5; }
	inline bool* get_address_of_iflush_5() { return &___iflush_5; }
	inline void set_iflush_5(bool value)
	{
		___iflush_5 = value;
	}

	inline static int32_t get_offset_of_byte_buf_6() { return static_cast<int32_t>(offsetof(StreamWriter_t1266378904, ___byte_buf_6)); }
	inline ByteU5BU5D_t4116647657* get_byte_buf_6() const { return ___byte_buf_6; }
	inline ByteU5BU5D_t4116647657** get_address_of_byte_buf_6() { return &___byte_buf_6; }
	inline void set_byte_buf_6(ByteU5BU5D_t4116647657* value)
	{
		___byte_buf_6 = value;
		Il2CppCodeGenWriteBarrier((&___byte_buf_6), value);
	}

	inline static int32_t get_offset_of_byte_pos_7() { return static_cast<int32_t>(offsetof(StreamWriter_t1266378904, ___byte_pos_7)); }
	inline int32_t get_byte_pos_7() const { return ___byte_pos_7; }
	inline int32_t* get_address_of_byte_pos_7() { return &___byte_pos_7; }
	inline void set_byte_pos_7(int32_t value)
	{
		___byte_pos_7 = value;
	}

	inline static int32_t get_offset_of_decode_buf_8() { return static_cast<int32_t>(offsetof(StreamWriter_t1266378904, ___decode_buf_8)); }
	inline CharU5BU5D_t3528271667* get_decode_buf_8() const { return ___decode_buf_8; }
	inline CharU5BU5D_t3528271667** get_address_of_decode_buf_8() { return &___decode_buf_8; }
	inline void set_decode_buf_8(CharU5BU5D_t3528271667* value)
	{
		___decode_buf_8 = value;
		Il2CppCodeGenWriteBarrier((&___decode_buf_8), value);
	}

	inline static int32_t get_offset_of_decode_pos_9() { return static_cast<int32_t>(offsetof(StreamWriter_t1266378904, ___decode_pos_9)); }
	inline int32_t get_decode_pos_9() const { return ___decode_pos_9; }
	inline int32_t* get_address_of_decode_pos_9() { return &___decode_pos_9; }
	inline void set_decode_pos_9(int32_t value)
	{
		___decode_pos_9 = value;
	}

	inline static int32_t get_offset_of_DisposedAlready_10() { return static_cast<int32_t>(offsetof(StreamWriter_t1266378904, ___DisposedAlready_10)); }
	inline bool get_DisposedAlready_10() const { return ___DisposedAlready_10; }
	inline bool* get_address_of_DisposedAlready_10() { return &___DisposedAlready_10; }
	inline void set_DisposedAlready_10(bool value)
	{
		___DisposedAlready_10 = value;
	}

	inline static int32_t get_offset_of_preamble_done_11() { return static_cast<int32_t>(offsetof(StreamWriter_t1266378904, ___preamble_done_11)); }
	inline bool get_preamble_done_11() const { return ___preamble_done_11; }
	inline bool* get_address_of_preamble_done_11() { return &___preamble_done_11; }
	inline void set_preamble_done_11(bool value)
	{
		___preamble_done_11 = value;
	}
};

struct StreamWriter_t1266378904_StaticFields
{
public:
	// System.IO.StreamWriter System.IO.StreamWriter::Null
	StreamWriter_t1266378904 * ___Null_12;

public:
	inline static int32_t get_offset_of_Null_12() { return static_cast<int32_t>(offsetof(StreamWriter_t1266378904_StaticFields, ___Null_12)); }
	inline StreamWriter_t1266378904 * get_Null_12() const { return ___Null_12; }
	inline StreamWriter_t1266378904 ** get_address_of_Null_12() { return &___Null_12; }
	inline void set_Null_12(StreamWriter_t1266378904 * value)
	{
		___Null_12 = value;
		Il2CppCodeGenWriteBarrier((&___Null_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMWRITER_T1266378904_H
#ifndef NODEENUMERATOR_T2183475207_H
#define NODEENUMERATOR_T2183475207_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.RBTree/NodeEnumerator
struct  NodeEnumerator_t2183475207 
{
public:
	// System.Collections.Generic.RBTree System.Collections.Generic.RBTree/NodeEnumerator::tree
	RBTree_t4095273678 * ___tree_0;
	// System.UInt32 System.Collections.Generic.RBTree/NodeEnumerator::version
	uint32_t ___version_1;
	// System.Collections.Generic.Stack`1<System.Collections.Generic.RBTree/Node> System.Collections.Generic.RBTree/NodeEnumerator::pennants
	Stack_1_t2901053741 * ___pennants_2;

public:
	inline static int32_t get_offset_of_tree_0() { return static_cast<int32_t>(offsetof(NodeEnumerator_t2183475207, ___tree_0)); }
	inline RBTree_t4095273678 * get_tree_0() const { return ___tree_0; }
	inline RBTree_t4095273678 ** get_address_of_tree_0() { return &___tree_0; }
	inline void set_tree_0(RBTree_t4095273678 * value)
	{
		___tree_0 = value;
		Il2CppCodeGenWriteBarrier((&___tree_0), value);
	}

	inline static int32_t get_offset_of_version_1() { return static_cast<int32_t>(offsetof(NodeEnumerator_t2183475207, ___version_1)); }
	inline uint32_t get_version_1() const { return ___version_1; }
	inline uint32_t* get_address_of_version_1() { return &___version_1; }
	inline void set_version_1(uint32_t value)
	{
		___version_1 = value;
	}

	inline static int32_t get_offset_of_pennants_2() { return static_cast<int32_t>(offsetof(NodeEnumerator_t2183475207, ___pennants_2)); }
	inline Stack_1_t2901053741 * get_pennants_2() const { return ___pennants_2; }
	inline Stack_1_t2901053741 ** get_address_of_pennants_2() { return &___pennants_2; }
	inline void set_pennants_2(Stack_1_t2901053741 * value)
	{
		___pennants_2 = value;
		Il2CppCodeGenWriteBarrier((&___pennants_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Collections.Generic.RBTree/NodeEnumerator
struct NodeEnumerator_t2183475207_marshaled_pinvoke
{
	RBTree_t4095273678 * ___tree_0;
	uint32_t ___version_1;
	Stack_1_t2901053741 * ___pennants_2;
};
// Native definition for COM marshalling of System.Collections.Generic.RBTree/NodeEnumerator
struct NodeEnumerator_t2183475207_marshaled_com
{
	RBTree_t4095273678 * ___tree_0;
	uint32_t ___version_1;
	Stack_1_t2901053741 * ___pennants_2;
};
#endif // NODEENUMERATOR_T2183475207_H
#ifndef DOUBLE_T594665363_H
#define DOUBLE_T594665363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Double
struct  Double_t594665363 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_13;

public:
	inline static int32_t get_offset_of_m_value_13() { return static_cast<int32_t>(offsetof(Double_t594665363, ___m_value_13)); }
	inline double get_m_value_13() const { return ___m_value_13; }
	inline double* get_address_of_m_value_13() { return &___m_value_13; }
	inline void set_m_value_13(double value)
	{
		___m_value_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLE_T594665363_H
#ifndef KEYVALUEPAIR_2_T4030379155_H
#define KEYVALUEPAIR_2_T4030379155_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.String,System.String>
struct  KeyValuePair_2_t4030379155 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	String_t* ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t4030379155, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t4030379155, ___value_1)); }
	inline String_t* get_value_1() const { return ___value_1; }
	inline String_t** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(String_t* value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T4030379155_H
#ifndef PBXNATIVETARGET_T793394962_H
#define PBXNATIVETARGET_T793394962_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.PBXNativeTarget
struct  PBXNativeTarget_t793394962  : public PBXObject_t3397571057
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBXNATIVETARGET_T793394962_H
#ifndef KEYVALUEPAIR_2_T3858289876_H
#define KEYVALUEPAIR_2_T3858289876_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.String,UnityEditor.XCodeEditor.PBXFrameworksBuildPhase>
struct  KeyValuePair_2_t3858289876 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	PBXFrameworksBuildPhase_t1675361410 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3858289876, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3858289876, ___value_1)); }
	inline PBXFrameworksBuildPhase_t1675361410 * get_value_1() const { return ___value_1; }
	inline PBXFrameworksBuildPhase_t1675361410 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(PBXFrameworksBuildPhase_t1675361410 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T3858289876_H
#ifndef KEYVALUEPAIR_2_T1221294591_H
#define KEYVALUEPAIR_2_T1221294591_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.String,UnityEditor.XCodeEditor.PBXResourcesBuildPhase>
struct  KeyValuePair_2_t1221294591 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	PBXResourcesBuildPhase_t3333333421 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1221294591, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1221294591, ___value_1)); }
	inline PBXResourcesBuildPhase_t3333333421 * get_value_1() const { return ___value_1; }
	inline PBXResourcesBuildPhase_t3333333421 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(PBXResourcesBuildPhase_t3333333421 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T1221294591_H
#ifndef PBXFILEREFERENCE_T491780957_H
#define PBXFILEREFERENCE_T491780957_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.PBXFileReference
struct  PBXFileReference_t491780957  : public PBXObject_t3397571057
{
public:
	// System.String UnityEditor.XCodeEditor.PBXFileReference::compilerFlags
	String_t* ___compilerFlags_9;
	// System.String UnityEditor.XCodeEditor.PBXFileReference::buildPhase
	String_t* ___buildPhase_10;
	// System.Collections.Generic.Dictionary`2<UnityEditor.XCodeEditor.TreeEnum,System.String> UnityEditor.XCodeEditor.PBXFileReference::trees
	Dictionary_2_t1851544083 * ___trees_11;

public:
	inline static int32_t get_offset_of_compilerFlags_9() { return static_cast<int32_t>(offsetof(PBXFileReference_t491780957, ___compilerFlags_9)); }
	inline String_t* get_compilerFlags_9() const { return ___compilerFlags_9; }
	inline String_t** get_address_of_compilerFlags_9() { return &___compilerFlags_9; }
	inline void set_compilerFlags_9(String_t* value)
	{
		___compilerFlags_9 = value;
		Il2CppCodeGenWriteBarrier((&___compilerFlags_9), value);
	}

	inline static int32_t get_offset_of_buildPhase_10() { return static_cast<int32_t>(offsetof(PBXFileReference_t491780957, ___buildPhase_10)); }
	inline String_t* get_buildPhase_10() const { return ___buildPhase_10; }
	inline String_t** get_address_of_buildPhase_10() { return &___buildPhase_10; }
	inline void set_buildPhase_10(String_t* value)
	{
		___buildPhase_10 = value;
		Il2CppCodeGenWriteBarrier((&___buildPhase_10), value);
	}

	inline static int32_t get_offset_of_trees_11() { return static_cast<int32_t>(offsetof(PBXFileReference_t491780957, ___trees_11)); }
	inline Dictionary_2_t1851544083 * get_trees_11() const { return ___trees_11; }
	inline Dictionary_2_t1851544083 ** get_address_of_trees_11() { return &___trees_11; }
	inline void set_trees_11(Dictionary_2_t1851544083 * value)
	{
		___trees_11 = value;
		Il2CppCodeGenWriteBarrier((&___trees_11), value);
	}
};

struct PBXFileReference_t491780957_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.String> UnityEditor.XCodeEditor.PBXFileReference::typeNames
	Dictionary_2_t1632706988 * ___typeNames_12;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> UnityEditor.XCodeEditor.PBXFileReference::typePhases
	Dictionary_2_t1632706988 * ___typePhases_13;

public:
	inline static int32_t get_offset_of_typeNames_12() { return static_cast<int32_t>(offsetof(PBXFileReference_t491780957_StaticFields, ___typeNames_12)); }
	inline Dictionary_2_t1632706988 * get_typeNames_12() const { return ___typeNames_12; }
	inline Dictionary_2_t1632706988 ** get_address_of_typeNames_12() { return &___typeNames_12; }
	inline void set_typeNames_12(Dictionary_2_t1632706988 * value)
	{
		___typeNames_12 = value;
		Il2CppCodeGenWriteBarrier((&___typeNames_12), value);
	}

	inline static int32_t get_offset_of_typePhases_13() { return static_cast<int32_t>(offsetof(PBXFileReference_t491780957_StaticFields, ___typePhases_13)); }
	inline Dictionary_2_t1632706988 * get_typePhases_13() const { return ___typePhases_13; }
	inline Dictionary_2_t1632706988 ** get_address_of_typePhases_13() { return &___typePhases_13; }
	inline void set_typePhases_13(Dictionary_2_t1632706988 * value)
	{
		___typePhases_13 = value;
		Il2CppCodeGenWriteBarrier((&___typePhases_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBXFILEREFERENCE_T491780957_H
#ifndef PBXDICTIONARY_1_T3538108837_H
#define PBXDICTIONARY_1_T3538108837_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXNativeTarget>
struct  PBXDictionary_1_t3538108837  : public Dictionary_2_t578651261
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBXDICTIONARY_1_T3538108837_H
#ifndef PBXDICTIONARY_1_T1414031086_H
#define PBXDICTIONARY_1_T1414031086_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXVariantGroup>
struct  PBXDictionary_1_t1414031086  : public Dictionary_2_t2749540806
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBXDICTIONARY_1_T1414031086_H
#ifndef PBXSORTEDDICTIONARY_1_T637646383_H
#define PBXSORTEDDICTIONARY_1_T637646383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.PBXSortedDictionary`1<UnityEditor.XCodeEditor.PBXFileReference>
struct  PBXSortedDictionary_1_t637646383  : public SortedDictionary_2_t1699557551
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBXSORTEDDICTIONARY_1_T637646383_H
#ifndef PBXSORTEDDICTIONARY_1_T2274302150_H
#define PBXSORTEDDICTIONARY_1_T2274302150_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.PBXSortedDictionary`1<UnityEditor.XCodeEditor.PBXGroup>
struct  PBXSortedDictionary_1_t2274302150  : public SortedDictionary_2_t3336213318
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBXSORTEDDICTIONARY_1_T2274302150_H
#ifndef PBXSORTEDDICTIONARY_1_T2756258120_H
#define PBXSORTEDDICTIONARY_1_T2756258120_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.PBXSortedDictionary`1<UnityEditor.XCodeEditor.PBXBuildFile>
struct  PBXSortedDictionary_1_t2756258120  : public SortedDictionary_2_t3818169288
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBXSORTEDDICTIONARY_1_T2756258120_H
#ifndef PBXGROUP_T2128436724_H
#define PBXGROUP_T2128436724_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.PBXGroup
struct  PBXGroup_t2128436724  : public PBXObject_t3397571057
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBXGROUP_T2128436724_H
#ifndef PBXPROJECT_T4035248171_H
#define PBXPROJECT_T4035248171_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.PBXProject
struct  PBXProject_t4035248171  : public PBXObject_t3397571057
{
public:
	// System.String UnityEditor.XCodeEditor.PBXProject::MAINGROUP_KEY
	String_t* ___MAINGROUP_KEY_3;
	// System.String UnityEditor.XCodeEditor.PBXProject::KNOWN_REGIONS_KEY
	String_t* ___KNOWN_REGIONS_KEY_4;
	// System.Boolean UnityEditor.XCodeEditor.PBXProject::_clearedLoc
	bool ____clearedLoc_5;

public:
	inline static int32_t get_offset_of_MAINGROUP_KEY_3() { return static_cast<int32_t>(offsetof(PBXProject_t4035248171, ___MAINGROUP_KEY_3)); }
	inline String_t* get_MAINGROUP_KEY_3() const { return ___MAINGROUP_KEY_3; }
	inline String_t** get_address_of_MAINGROUP_KEY_3() { return &___MAINGROUP_KEY_3; }
	inline void set_MAINGROUP_KEY_3(String_t* value)
	{
		___MAINGROUP_KEY_3 = value;
		Il2CppCodeGenWriteBarrier((&___MAINGROUP_KEY_3), value);
	}

	inline static int32_t get_offset_of_KNOWN_REGIONS_KEY_4() { return static_cast<int32_t>(offsetof(PBXProject_t4035248171, ___KNOWN_REGIONS_KEY_4)); }
	inline String_t* get_KNOWN_REGIONS_KEY_4() const { return ___KNOWN_REGIONS_KEY_4; }
	inline String_t** get_address_of_KNOWN_REGIONS_KEY_4() { return &___KNOWN_REGIONS_KEY_4; }
	inline void set_KNOWN_REGIONS_KEY_4(String_t* value)
	{
		___KNOWN_REGIONS_KEY_4 = value;
		Il2CppCodeGenWriteBarrier((&___KNOWN_REGIONS_KEY_4), value);
	}

	inline static int32_t get_offset_of__clearedLoc_5() { return static_cast<int32_t>(offsetof(PBXProject_t4035248171, ____clearedLoc_5)); }
	inline bool get__clearedLoc_5() const { return ____clearedLoc_5; }
	inline bool* get_address_of__clearedLoc_5() { return &____clearedLoc_5; }
	inline void set__clearedLoc_5(bool value)
	{
		____clearedLoc_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBXPROJECT_T4035248171_H
#ifndef ENUMERATOR_T2146457487_H
#define ENUMERATOR_T2146457487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.Object>
struct  Enumerator_t2146457487 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t257213610 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___l_0)); }
	inline List_1_t257213610 * get_l_0() const { return ___l_0; }
	inline List_1_t257213610 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t257213610 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2146457487_H
#ifndef DICTIONARYENTRY_T3123975638_H
#define DICTIONARYENTRY_T3123975638_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.DictionaryEntry
struct  DictionaryEntry_t3123975638 
{
public:
	// System.Object System.Collections.DictionaryEntry::_key
	RuntimeObject * ____key_0;
	// System.Object System.Collections.DictionaryEntry::_value
	RuntimeObject * ____value_1;

public:
	inline static int32_t get_offset_of__key_0() { return static_cast<int32_t>(offsetof(DictionaryEntry_t3123975638, ____key_0)); }
	inline RuntimeObject * get__key_0() const { return ____key_0; }
	inline RuntimeObject ** get_address_of__key_0() { return &____key_0; }
	inline void set__key_0(RuntimeObject * value)
	{
		____key_0 = value;
		Il2CppCodeGenWriteBarrier((&____key_0), value);
	}

	inline static int32_t get_offset_of__value_1() { return static_cast<int32_t>(offsetof(DictionaryEntry_t3123975638, ____value_1)); }
	inline RuntimeObject * get__value_1() const { return ____value_1; }
	inline RuntimeObject ** get_address_of__value_1() { return &____value_1; }
	inline void set__value_1(RuntimeObject * value)
	{
		____value_1 = value;
		Il2CppCodeGenWriteBarrier((&____value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Collections.DictionaryEntry
struct DictionaryEntry_t3123975638_marshaled_pinvoke
{
	Il2CppIUnknown* ____key_0;
	Il2CppIUnknown* ____value_1;
};
// Native definition for COM marshalling of System.Collections.DictionaryEntry
struct DictionaryEntry_t3123975638_marshaled_com
{
	Il2CppIUnknown* ____key_0;
	Il2CppIUnknown* ____value_1;
};
#endif // DICTIONARYENTRY_T3123975638_H
#ifndef CHAR_T3634460470_H
#define CHAR_T3634460470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Char
struct  Char_t3634460470 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Char_t3634460470, ___m_value_2)); }
	inline Il2CppChar get_m_value_2() const { return ___m_value_2; }
	inline Il2CppChar* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(Il2CppChar value)
	{
		___m_value_2 = value;
	}
};

struct Char_t3634460470_StaticFields
{
public:
	// System.Byte* System.Char::category_data
	uint8_t* ___category_data_3;
	// System.Byte* System.Char::numeric_data
	uint8_t* ___numeric_data_4;
	// System.Double* System.Char::numeric_data_values
	double* ___numeric_data_values_5;
	// System.UInt16* System.Char::to_lower_data_low
	uint16_t* ___to_lower_data_low_6;
	// System.UInt16* System.Char::to_lower_data_high
	uint16_t* ___to_lower_data_high_7;
	// System.UInt16* System.Char::to_upper_data_low
	uint16_t* ___to_upper_data_low_8;
	// System.UInt16* System.Char::to_upper_data_high
	uint16_t* ___to_upper_data_high_9;

public:
	inline static int32_t get_offset_of_category_data_3() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___category_data_3)); }
	inline uint8_t* get_category_data_3() const { return ___category_data_3; }
	inline uint8_t** get_address_of_category_data_3() { return &___category_data_3; }
	inline void set_category_data_3(uint8_t* value)
	{
		___category_data_3 = value;
	}

	inline static int32_t get_offset_of_numeric_data_4() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___numeric_data_4)); }
	inline uint8_t* get_numeric_data_4() const { return ___numeric_data_4; }
	inline uint8_t** get_address_of_numeric_data_4() { return &___numeric_data_4; }
	inline void set_numeric_data_4(uint8_t* value)
	{
		___numeric_data_4 = value;
	}

	inline static int32_t get_offset_of_numeric_data_values_5() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___numeric_data_values_5)); }
	inline double* get_numeric_data_values_5() const { return ___numeric_data_values_5; }
	inline double** get_address_of_numeric_data_values_5() { return &___numeric_data_values_5; }
	inline void set_numeric_data_values_5(double* value)
	{
		___numeric_data_values_5 = value;
	}

	inline static int32_t get_offset_of_to_lower_data_low_6() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___to_lower_data_low_6)); }
	inline uint16_t* get_to_lower_data_low_6() const { return ___to_lower_data_low_6; }
	inline uint16_t** get_address_of_to_lower_data_low_6() { return &___to_lower_data_low_6; }
	inline void set_to_lower_data_low_6(uint16_t* value)
	{
		___to_lower_data_low_6 = value;
	}

	inline static int32_t get_offset_of_to_lower_data_high_7() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___to_lower_data_high_7)); }
	inline uint16_t* get_to_lower_data_high_7() const { return ___to_lower_data_high_7; }
	inline uint16_t** get_address_of_to_lower_data_high_7() { return &___to_lower_data_high_7; }
	inline void set_to_lower_data_high_7(uint16_t* value)
	{
		___to_lower_data_high_7 = value;
	}

	inline static int32_t get_offset_of_to_upper_data_low_8() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___to_upper_data_low_8)); }
	inline uint16_t* get_to_upper_data_low_8() const { return ___to_upper_data_low_8; }
	inline uint16_t** get_address_of_to_upper_data_low_8() { return &___to_upper_data_low_8; }
	inline void set_to_upper_data_low_8(uint16_t* value)
	{
		___to_upper_data_low_8 = value;
	}

	inline static int32_t get_offset_of_to_upper_data_high_9() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___to_upper_data_high_9)); }
	inline uint16_t* get_to_upper_data_high_9() const { return ___to_upper_data_high_9; }
	inline uint16_t** get_address_of_to_upper_data_high_9() { return &___to_upper_data_high_9; }
	inline void set_to_upper_data_high_9(uint16_t* value)
	{
		___to_upper_data_high_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHAR_T3634460470_H
#ifndef UNITYEXCEPTION_T3598173660_H
#define UNITYEXCEPTION_T3598173660_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UnityException
struct  UnityException_t3598173660  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEXCEPTION_T3598173660_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef STREAMREADER_T4009935899_H
#define STREAMREADER_T4009935899_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.StreamReader
struct  StreamReader_t4009935899  : public TextReader_t283511965
{
public:
	// System.Byte[] System.IO.StreamReader::input_buffer
	ByteU5BU5D_t4116647657* ___input_buffer_1;
	// System.Char[] System.IO.StreamReader::decoded_buffer
	CharU5BU5D_t3528271667* ___decoded_buffer_2;
	// System.Int32 System.IO.StreamReader::decoded_count
	int32_t ___decoded_count_3;
	// System.Int32 System.IO.StreamReader::pos
	int32_t ___pos_4;
	// System.Int32 System.IO.StreamReader::buffer_size
	int32_t ___buffer_size_5;
	// System.Int32 System.IO.StreamReader::do_checks
	int32_t ___do_checks_6;
	// System.Text.Encoding System.IO.StreamReader::encoding
	Encoding_t1523322056 * ___encoding_7;
	// System.Text.Decoder System.IO.StreamReader::decoder
	Decoder_t2204182725 * ___decoder_8;
	// System.IO.Stream System.IO.StreamReader::base_stream
	Stream_t1273022909 * ___base_stream_9;
	// System.Boolean System.IO.StreamReader::mayBlock
	bool ___mayBlock_10;
	// System.Text.StringBuilder System.IO.StreamReader::line_builder
	StringBuilder_t * ___line_builder_11;
	// System.Boolean System.IO.StreamReader::foundCR
	bool ___foundCR_13;

public:
	inline static int32_t get_offset_of_input_buffer_1() { return static_cast<int32_t>(offsetof(StreamReader_t4009935899, ___input_buffer_1)); }
	inline ByteU5BU5D_t4116647657* get_input_buffer_1() const { return ___input_buffer_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_input_buffer_1() { return &___input_buffer_1; }
	inline void set_input_buffer_1(ByteU5BU5D_t4116647657* value)
	{
		___input_buffer_1 = value;
		Il2CppCodeGenWriteBarrier((&___input_buffer_1), value);
	}

	inline static int32_t get_offset_of_decoded_buffer_2() { return static_cast<int32_t>(offsetof(StreamReader_t4009935899, ___decoded_buffer_2)); }
	inline CharU5BU5D_t3528271667* get_decoded_buffer_2() const { return ___decoded_buffer_2; }
	inline CharU5BU5D_t3528271667** get_address_of_decoded_buffer_2() { return &___decoded_buffer_2; }
	inline void set_decoded_buffer_2(CharU5BU5D_t3528271667* value)
	{
		___decoded_buffer_2 = value;
		Il2CppCodeGenWriteBarrier((&___decoded_buffer_2), value);
	}

	inline static int32_t get_offset_of_decoded_count_3() { return static_cast<int32_t>(offsetof(StreamReader_t4009935899, ___decoded_count_3)); }
	inline int32_t get_decoded_count_3() const { return ___decoded_count_3; }
	inline int32_t* get_address_of_decoded_count_3() { return &___decoded_count_3; }
	inline void set_decoded_count_3(int32_t value)
	{
		___decoded_count_3 = value;
	}

	inline static int32_t get_offset_of_pos_4() { return static_cast<int32_t>(offsetof(StreamReader_t4009935899, ___pos_4)); }
	inline int32_t get_pos_4() const { return ___pos_4; }
	inline int32_t* get_address_of_pos_4() { return &___pos_4; }
	inline void set_pos_4(int32_t value)
	{
		___pos_4 = value;
	}

	inline static int32_t get_offset_of_buffer_size_5() { return static_cast<int32_t>(offsetof(StreamReader_t4009935899, ___buffer_size_5)); }
	inline int32_t get_buffer_size_5() const { return ___buffer_size_5; }
	inline int32_t* get_address_of_buffer_size_5() { return &___buffer_size_5; }
	inline void set_buffer_size_5(int32_t value)
	{
		___buffer_size_5 = value;
	}

	inline static int32_t get_offset_of_do_checks_6() { return static_cast<int32_t>(offsetof(StreamReader_t4009935899, ___do_checks_6)); }
	inline int32_t get_do_checks_6() const { return ___do_checks_6; }
	inline int32_t* get_address_of_do_checks_6() { return &___do_checks_6; }
	inline void set_do_checks_6(int32_t value)
	{
		___do_checks_6 = value;
	}

	inline static int32_t get_offset_of_encoding_7() { return static_cast<int32_t>(offsetof(StreamReader_t4009935899, ___encoding_7)); }
	inline Encoding_t1523322056 * get_encoding_7() const { return ___encoding_7; }
	inline Encoding_t1523322056 ** get_address_of_encoding_7() { return &___encoding_7; }
	inline void set_encoding_7(Encoding_t1523322056 * value)
	{
		___encoding_7 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_7), value);
	}

	inline static int32_t get_offset_of_decoder_8() { return static_cast<int32_t>(offsetof(StreamReader_t4009935899, ___decoder_8)); }
	inline Decoder_t2204182725 * get_decoder_8() const { return ___decoder_8; }
	inline Decoder_t2204182725 ** get_address_of_decoder_8() { return &___decoder_8; }
	inline void set_decoder_8(Decoder_t2204182725 * value)
	{
		___decoder_8 = value;
		Il2CppCodeGenWriteBarrier((&___decoder_8), value);
	}

	inline static int32_t get_offset_of_base_stream_9() { return static_cast<int32_t>(offsetof(StreamReader_t4009935899, ___base_stream_9)); }
	inline Stream_t1273022909 * get_base_stream_9() const { return ___base_stream_9; }
	inline Stream_t1273022909 ** get_address_of_base_stream_9() { return &___base_stream_9; }
	inline void set_base_stream_9(Stream_t1273022909 * value)
	{
		___base_stream_9 = value;
		Il2CppCodeGenWriteBarrier((&___base_stream_9), value);
	}

	inline static int32_t get_offset_of_mayBlock_10() { return static_cast<int32_t>(offsetof(StreamReader_t4009935899, ___mayBlock_10)); }
	inline bool get_mayBlock_10() const { return ___mayBlock_10; }
	inline bool* get_address_of_mayBlock_10() { return &___mayBlock_10; }
	inline void set_mayBlock_10(bool value)
	{
		___mayBlock_10 = value;
	}

	inline static int32_t get_offset_of_line_builder_11() { return static_cast<int32_t>(offsetof(StreamReader_t4009935899, ___line_builder_11)); }
	inline StringBuilder_t * get_line_builder_11() const { return ___line_builder_11; }
	inline StringBuilder_t ** get_address_of_line_builder_11() { return &___line_builder_11; }
	inline void set_line_builder_11(StringBuilder_t * value)
	{
		___line_builder_11 = value;
		Il2CppCodeGenWriteBarrier((&___line_builder_11), value);
	}

	inline static int32_t get_offset_of_foundCR_13() { return static_cast<int32_t>(offsetof(StreamReader_t4009935899, ___foundCR_13)); }
	inline bool get_foundCR_13() const { return ___foundCR_13; }
	inline bool* get_address_of_foundCR_13() { return &___foundCR_13; }
	inline void set_foundCR_13(bool value)
	{
		___foundCR_13 = value;
	}
};

struct StreamReader_t4009935899_StaticFields
{
public:
	// System.IO.StreamReader System.IO.StreamReader::Null
	StreamReader_t4009935899 * ___Null_12;

public:
	inline static int32_t get_offset_of_Null_12() { return static_cast<int32_t>(offsetof(StreamReader_t4009935899_StaticFields, ___Null_12)); }
	inline StreamReader_t4009935899 * get_Null_12() const { return ___Null_12; }
	inline StreamReader_t4009935899 ** get_address_of_Null_12() { return &___Null_12; }
	inline void set_Null_12(StreamReader_t4009935899 * value)
	{
		___Null_12 = value;
		Il2CppCodeGenWriteBarrier((&___Null_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMREADER_T4009935899_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef PBXDICTIONARY_T2078602241_H
#define PBXDICTIONARY_T2078602241_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.PBXDictionary
struct  PBXDictionary_t2078602241  : public Dictionary_2_t2865362463
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBXDICTIONARY_T2078602241_H
#ifndef XCCONFIGURATIONLIST_T1100530024_H
#define XCCONFIGURATIONLIST_T1100530024_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.XCConfigurationList
struct  XCConfigurationList_t1100530024  : public PBXObject_t3397571057
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XCCONFIGURATIONLIST_T1100530024_H
#ifndef PBXDICTIONARY_1_T4195780175_H
#define PBXDICTIONARY_1_T4195780175_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.XCBuildConfiguration>
struct  PBXDictionary_1_t4195780175  : public Dictionary_2_t1236322599
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBXDICTIONARY_1_T4195780175_H
#ifndef PBXDICTIONARY_1_T125107989_H
#define PBXDICTIONARY_1_T125107989_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXFrameworksBuildPhase>
struct  PBXDictionary_1_t125107989  : public Dictionary_2_t1460617709
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBXDICTIONARY_1_T125107989_H
#ifndef PBXSORTEDDICTIONARY_1_T1246395450_H
#define PBXSORTEDDICTIONARY_1_T1246395450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.PBXSortedDictionary`1<UnityEditor.XCodeEditor.XCConfigurationList>
struct  PBXSortedDictionary_1_t1246395450  : public SortedDictionary_2_t2308306618
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBXSORTEDDICTIONARY_1_T1246395450_H
#ifndef PBXLIST_T4289188522_H
#define PBXLIST_T4289188522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.PBXList
struct  PBXList_t4289188522  : public ArrayList_t2718874744
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBXLIST_T4289188522_H
#ifndef PBXDICTIONARY_1_T2617057394_H
#define PBXDICTIONARY_1_T2617057394_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXCopyFilesBuildPhase>
struct  PBXDictionary_1_t2617057394  : public Dictionary_2_t3952567114
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBXDICTIONARY_1_T2617057394_H
#ifndef KEYVALUEPAIR_2_T3633994766_H
#define KEYVALUEPAIR_2_T3633994766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.String,UnityEditor.XCodeEditor.XCBuildConfiguration>
struct  KeyValuePair_2_t3633994766 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	XCBuildConfiguration_t1451066300 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3633994766, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3633994766, ___value_1)); }
	inline XCBuildConfiguration_t1451066300 * get_value_1() const { return ___value_1; }
	inline XCBuildConfiguration_t1451066300 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(XCBuildConfiguration_t1451066300 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T3633994766_H
#ifndef PBXDICTIONARY_1_T2932651977_H
#define PBXDICTIONARY_1_T2932651977_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXSourcesBuildPhase>
struct  PBXDictionary_1_t2932651977  : public Dictionary_2_t4268161697
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBXDICTIONARY_1_T2932651977_H
#ifndef PBXDICTIONARY_1_T1977435415_H
#define PBXDICTIONARY_1_T1977435415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXShellScriptBuildPhase>
struct  PBXDictionary_1_t1977435415  : public Dictionary_2_t3312945135
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBXDICTIONARY_1_T1977435415_H
#ifndef KEYVALUEPAIR_2_T2530217319_H
#define KEYVALUEPAIR_2_T2530217319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
struct  KeyValuePair_2_t2530217319 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2530217319, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2530217319, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2530217319_H
#ifndef XCBUILDCONFIGURATION_T1451066300_H
#define XCBUILDCONFIGURATION_T1451066300_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.XCBuildConfiguration
struct  XCBuildConfiguration_t1451066300  : public PBXObject_t3397571057
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XCBUILDCONFIGURATION_T1451066300_H
#ifndef PBXDICTIONARY_1_T1783080000_H
#define PBXDICTIONARY_1_T1783080000_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXResourcesBuildPhase>
struct  PBXDictionary_1_t1783080000  : public Dictionary_2_t3118589720
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBXDICTIONARY_1_T1783080000_H
#ifndef ENUMERATOR_T1611782593_H
#define ENUMERATOR_T1611782593_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEditor.XCodeEditor.PBXCopyFilesBuildPhase>
struct  Enumerator_t1611782593 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_t3952567114 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_t2055271985  ___current_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t1611782593, ___dictionary_0)); }
	inline Dictionary_2_t3952567114 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t3952567114 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t3952567114 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t1611782593, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t1611782593, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t1611782593, ___current_3)); }
	inline KeyValuePair_2_t2055271985  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t2055271985 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t2055271985  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T1611782593_H
#ifndef ENUMERATOR_T3190505374_H
#define ENUMERATOR_T3190505374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEditor.XCodeEditor.XCBuildConfiguration>
struct  Enumerator_t3190505374 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_t1236322599 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_t3633994766  ___current_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t3190505374, ___dictionary_0)); }
	inline Dictionary_2_t1236322599 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t1236322599 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t1236322599 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t3190505374, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t3190505374, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t3190505374, ___current_3)); }
	inline KeyValuePair_2_t3633994766  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t3633994766 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t3633994766  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T3190505374_H
#ifndef ENUMERATOR_T3586889763_H
#define ENUMERATOR_T3586889763_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>
struct  Enumerator_t3586889763 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_t1632706988 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_t4030379155  ___current_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t3586889763, ___dictionary_0)); }
	inline Dictionary_2_t1632706988 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t1632706988 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t1632706988 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t3586889763, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t3586889763, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t3586889763, ___current_3)); }
	inline KeyValuePair_2_t4030379155  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t4030379155 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t4030379155  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T3586889763_H
#ifndef ENUMERATOR_T1927377176_H
#define ENUMERATOR_T1927377176_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEditor.XCodeEditor.PBXSourcesBuildPhase>
struct  Enumerator_t1927377176 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_t4268161697 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_t2370866568  ___current_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t1927377176, ___dictionary_0)); }
	inline Dictionary_2_t4268161697 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t4268161697 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t4268161697 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t1927377176, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t1927377176, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t1927377176, ___current_3)); }
	inline KeyValuePair_2_t2370866568  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t2370866568 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t2370866568  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T1927377176_H
#ifndef ENUMERATOR_T972160614_H
#define ENUMERATOR_T972160614_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEditor.XCodeEditor.PBXShellScriptBuildPhase>
struct  Enumerator_t972160614 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_t3312945135 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_t1415650006  ___current_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t972160614, ___dictionary_0)); }
	inline Dictionary_2_t3312945135 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t3312945135 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t3312945135 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t972160614, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t972160614, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t972160614, ___current_3)); }
	inline KeyValuePair_2_t1415650006  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t1415650006 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t1415650006  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T972160614_H
#ifndef TREEENUM_T2111024294_H
#define TREEENUM_T2111024294_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.TreeEnum
struct  TreeEnum_t2111024294 
{
public:
	// System.Int32 UnityEditor.XCodeEditor.TreeEnum::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TreeEnum_t2111024294, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TREEENUM_T2111024294_H
#ifndef BINDINGFLAGS_T2721792723_H
#define BINDINGFLAGS_T2721792723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t2721792723 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BindingFlags_t2721792723, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T2721792723_H
#ifndef ENUMERATOR_T777805199_H
#define ENUMERATOR_T777805199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEditor.XCodeEditor.PBXResourcesBuildPhase>
struct  Enumerator_t777805199 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_t3118589720 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_t1221294591  ___current_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t777805199, ___dictionary_0)); }
	inline Dictionary_2_t3118589720 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t3118589720 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t3118589720 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t777805199, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t777805199, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t777805199, ___current_3)); }
	inline KeyValuePair_2_t1221294591  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t1221294591 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t1221294591  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T777805199_H
#ifndef ENUMERATOR_T2086727927_H
#define ENUMERATOR_T2086727927_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
struct  Enumerator_t2086727927 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_t132545152 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_t2530217319  ___current_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t2086727927, ___dictionary_0)); }
	inline Dictionary_2_t132545152 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t132545152 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t132545152 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2086727927, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t2086727927, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2086727927, ___current_3)); }
	inline KeyValuePair_2_t2530217319  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t2530217319 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t2530217319  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2086727927_H
#ifndef RUNTIMETYPEHANDLE_T3027515415_H
#define RUNTIMETYPEHANDLE_T3027515415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t3027515415 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t3027515415, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T3027515415_H
#ifndef PBXFRAMEWORKSBUILDPHASE_T1675361410_H
#define PBXFRAMEWORKSBUILDPHASE_T1675361410_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.PBXFrameworksBuildPhase
struct  PBXFrameworksBuildPhase_t1675361410  : public PBXBuildPhase_t3890765868
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBXFRAMEWORKSBUILDPHASE_T1675361410_H
#ifndef VERTEXY_T499074406_H
#define VERTEXY_T499074406_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexY
struct  VertexY_t499074406 
{
public:
	// System.Int32 VertexY::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(VertexY_t499074406, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXY_T499074406_H
#ifndef PBXRESOURCESBUILDPHASE_T3333333421_H
#define PBXRESOURCESBUILDPHASE_T3333333421_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.PBXResourcesBuildPhase
struct  PBXResourcesBuildPhase_t3333333421  : public PBXBuildPhase_t3890765868
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBXRESOURCESBUILDPHASE_T3333333421_H
#ifndef PBXSHELLSCRIPTBUILDPHASE_T3527688836_H
#define PBXSHELLSCRIPTBUILDPHASE_T3527688836_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.PBXShellScriptBuildPhase
struct  PBXShellScriptBuildPhase_t3527688836  : public PBXBuildPhase_t3890765868
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBXSHELLSCRIPTBUILDPHASE_T3527688836_H
#ifndef PBXSOURCESBUILDPHASE_T187938102_H
#define PBXSOURCESBUILDPHASE_T187938102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.PBXSourcesBuildPhase
struct  PBXSourcesBuildPhase_t187938102  : public PBXBuildPhase_t3890765868
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBXSOURCESBUILDPHASE_T187938102_H
#ifndef VERTEXZ_T3227957761_H
#define VERTEXZ_T3227957761_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexZ
struct  VertexZ_t3227957761 
{
public:
	// System.Int32 VertexZ::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(VertexZ_t3227957761, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXZ_T3227957761_H
#ifndef PBXVARIANTGROUP_T2964284507_H
#define PBXVARIANTGROUP_T2964284507_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.PBXVariantGroup
struct  PBXVariantGroup_t2964284507  : public PBXGroup_t2128436724
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBXVARIANTGROUP_T2964284507_H
#ifndef ENUMERATOR_T3200245381_H
#define ENUMERATOR_T3200245381_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.SortedDictionary`2/Enumerator<System.String,UnityEditor.XCodeEditor.PBXFileReference>
struct  Enumerator_t3200245381 
{
public:
	// System.Collections.Generic.RBTree/NodeEnumerator System.Collections.Generic.SortedDictionary`2/Enumerator::host
	NodeEnumerator_t2183475207  ___host_0;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.SortedDictionary`2/Enumerator::current
	KeyValuePair_2_t2674709423  ___current_1;

public:
	inline static int32_t get_offset_of_host_0() { return static_cast<int32_t>(offsetof(Enumerator_t3200245381, ___host_0)); }
	inline NodeEnumerator_t2183475207  get_host_0() const { return ___host_0; }
	inline NodeEnumerator_t2183475207 * get_address_of_host_0() { return &___host_0; }
	inline void set_host_0(NodeEnumerator_t2183475207  value)
	{
		___host_0 = value;
	}

	inline static int32_t get_offset_of_current_1() { return static_cast<int32_t>(offsetof(Enumerator_t3200245381, ___current_1)); }
	inline KeyValuePair_2_t2674709423  get_current_1() const { return ___current_1; }
	inline KeyValuePair_2_t2674709423 * get_address_of_current_1() { return &___current_1; }
	inline void set_current_1(KeyValuePair_2_t2674709423  value)
	{
		___current_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T3200245381_H
#ifndef FILEATTRIBUTES_T3417205536_H
#define FILEATTRIBUTES_T3417205536_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FileAttributes
struct  FileAttributes_t3417205536 
{
public:
	// System.Int32 System.IO.FileAttributes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FileAttributes_t3417205536, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEATTRIBUTES_T3417205536_H
#ifndef ENUMERATOR_T3414800484_H
#define ENUMERATOR_T3414800484_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEditor.XCodeEditor.PBXFrameworksBuildPhase>
struct  Enumerator_t3414800484 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_t1460617709 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_t3858289876  ___current_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t3414800484, ___dictionary_0)); }
	inline Dictionary_2_t1460617709 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t1460617709 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t1460617709 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t3414800484, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t3414800484, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t3414800484, ___current_3)); }
	inline KeyValuePair_2_t3858289876  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t3858289876 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t3858289876  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T3414800484_H
#ifndef PBXCOPYFILESBUILDPHASE_T4167310815_H
#define PBXCOPYFILESBUILDPHASE_T4167310815_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEditor.XCodeEditor.PBXCopyFilesBuildPhase
struct  PBXCopyFilesBuildPhase_t4167310815  : public PBXBuildPhase_t3890765868
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBXCOPYFILESBUILDPHASE_T4167310815_H
#ifndef ENUMERATOR_T541933852_H
#define ENUMERATOR_T541933852_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.SortedDictionary`2/Enumerator<System.String,UnityEditor.XCodeEditor.PBXGroup>
struct  Enumerator_t541933852 
{
public:
	// System.Collections.Generic.RBTree/NodeEnumerator System.Collections.Generic.SortedDictionary`2/Enumerator::host
	NodeEnumerator_t2183475207  ___host_0;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.SortedDictionary`2/Enumerator::current
	KeyValuePair_2_t16397894  ___current_1;

public:
	inline static int32_t get_offset_of_host_0() { return static_cast<int32_t>(offsetof(Enumerator_t541933852, ___host_0)); }
	inline NodeEnumerator_t2183475207  get_host_0() const { return ___host_0; }
	inline NodeEnumerator_t2183475207 * get_address_of_host_0() { return &___host_0; }
	inline void set_host_0(NodeEnumerator_t2183475207  value)
	{
		___host_0 = value;
	}

	inline static int32_t get_offset_of_current_1() { return static_cast<int32_t>(offsetof(Enumerator_t541933852, ___current_1)); }
	inline KeyValuePair_2_t16397894  get_current_1() const { return ___current_1; }
	inline KeyValuePair_2_t16397894 * get_address_of_current_1() { return &___current_1; }
	inline void set_current_1(KeyValuePair_2_t16397894  value)
	{
		___current_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T541933852_H
#ifndef ENUMERATOR_T2532834036_H
#define ENUMERATOR_T2532834036_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEditor.XCodeEditor.PBXNativeTarget>
struct  Enumerator_t2532834036 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_t578651261 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_t2976323428  ___current_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t2532834036, ___dictionary_0)); }
	inline Dictionary_2_t578651261 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t578651261 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t578651261 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2532834036, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t2532834036, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2532834036, ___current_3)); }
	inline KeyValuePair_2_t2976323428  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t2976323428 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t2976323428  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2532834036_H
#ifndef VERTEXX_T2065158347_H
#define VERTEXX_T2065158347_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexX
struct  VertexX_t2065158347 
{
public:
	// System.Int32 VertexX::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(VertexX_t2065158347, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXX_T2065158347_H
#ifndef ENUMERATOR_T3055753277_H
#define ENUMERATOR_T3055753277_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.SortedDictionary`2/Enumerator<System.Object,System.Object>
struct  Enumerator_t3055753277 
{
public:
	// System.Collections.Generic.RBTree/NodeEnumerator System.Collections.Generic.SortedDictionary`2/Enumerator::host
	NodeEnumerator_t2183475207  ___host_0;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.SortedDictionary`2/Enumerator::current
	KeyValuePair_2_t2530217319  ___current_1;

public:
	inline static int32_t get_offset_of_host_0() { return static_cast<int32_t>(offsetof(Enumerator_t3055753277, ___host_0)); }
	inline NodeEnumerator_t2183475207  get_host_0() const { return ___host_0; }
	inline NodeEnumerator_t2183475207 * get_address_of_host_0() { return &___host_0; }
	inline void set_host_0(NodeEnumerator_t2183475207  value)
	{
		___host_0 = value;
	}

	inline static int32_t get_offset_of_current_1() { return static_cast<int32_t>(offsetof(Enumerator_t3055753277, ___current_1)); }
	inline KeyValuePair_2_t2530217319  get_current_1() const { return ___current_1; }
	inline KeyValuePair_2_t2530217319 * get_address_of_current_1() { return &___current_1; }
	inline void set_current_1(KeyValuePair_2_t2530217319  value)
	{
		___current_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T3055753277_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t3027515415  ____impl_1;

public:
	inline static int32_t get_offset_of__impl_1() { return static_cast<int32_t>(offsetof(Type_t, ____impl_1)); }
	inline RuntimeTypeHandle_t3027515415  get__impl_1() const { return ____impl_1; }
	inline RuntimeTypeHandle_t3027515415 * get_address_of__impl_1() { return &____impl_1; }
	inline void set__impl_1(RuntimeTypeHandle_t3027515415  value)
	{
		____impl_1 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_2;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t3940880105* ___EmptyTypes_3;
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t426314064 * ___FilterAttribute_4;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t426314064 * ___FilterName_5;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t426314064 * ___FilterNameIgnoreCase_6;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_7;

public:
	inline static int32_t get_offset_of_Delimiter_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_2)); }
	inline Il2CppChar get_Delimiter_2() const { return ___Delimiter_2; }
	inline Il2CppChar* get_address_of_Delimiter_2() { return &___Delimiter_2; }
	inline void set_Delimiter_2(Il2CppChar value)
	{
		___Delimiter_2 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_3)); }
	inline TypeU5BU5D_t3940880105* get_EmptyTypes_3() const { return ___EmptyTypes_3; }
	inline TypeU5BU5D_t3940880105** get_address_of_EmptyTypes_3() { return &___EmptyTypes_3; }
	inline void set_EmptyTypes_3(TypeU5BU5D_t3940880105* value)
	{
		___EmptyTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_3), value);
	}

	inline static int32_t get_offset_of_FilterAttribute_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_4)); }
	inline MemberFilter_t426314064 * get_FilterAttribute_4() const { return ___FilterAttribute_4; }
	inline MemberFilter_t426314064 ** get_address_of_FilterAttribute_4() { return &___FilterAttribute_4; }
	inline void set_FilterAttribute_4(MemberFilter_t426314064 * value)
	{
		___FilterAttribute_4 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_4), value);
	}

	inline static int32_t get_offset_of_FilterName_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_5)); }
	inline MemberFilter_t426314064 * get_FilterName_5() const { return ___FilterName_5; }
	inline MemberFilter_t426314064 ** get_address_of_FilterName_5() { return &___FilterName_5; }
	inline void set_FilterName_5(MemberFilter_t426314064 * value)
	{
		___FilterName_5 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_5), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_6)); }
	inline MemberFilter_t426314064 * get_FilterNameIgnoreCase_6() const { return ___FilterNameIgnoreCase_6; }
	inline MemberFilter_t426314064 ** get_address_of_FilterNameIgnoreCase_6() { return &___FilterNameIgnoreCase_6; }
	inline void set_FilterNameIgnoreCase_6(MemberFilter_t426314064 * value)
	{
		___FilterNameIgnoreCase_6 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_6), value);
	}

	inline static int32_t get_offset_of_Missing_7() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_7)); }
	inline RuntimeObject * get_Missing_7() const { return ___Missing_7; }
	inline RuntimeObject ** get_address_of_Missing_7() { return &___Missing_7; }
	inline void set_Missing_7(RuntimeObject * value)
	{
		___Missing_7 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef MONOIOSTAT_T592533987_H
#define MONOIOSTAT_T592533987_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.MonoIOStat
struct  MonoIOStat_t592533987 
{
public:
	// System.String System.IO.MonoIOStat::Name
	String_t* ___Name_0;
	// System.IO.FileAttributes System.IO.MonoIOStat::Attributes
	int32_t ___Attributes_1;
	// System.Int64 System.IO.MonoIOStat::Length
	int64_t ___Length_2;
	// System.Int64 System.IO.MonoIOStat::CreationTime
	int64_t ___CreationTime_3;
	// System.Int64 System.IO.MonoIOStat::LastAccessTime
	int64_t ___LastAccessTime_4;
	// System.Int64 System.IO.MonoIOStat::LastWriteTime
	int64_t ___LastWriteTime_5;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(MonoIOStat_t592533987, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_Attributes_1() { return static_cast<int32_t>(offsetof(MonoIOStat_t592533987, ___Attributes_1)); }
	inline int32_t get_Attributes_1() const { return ___Attributes_1; }
	inline int32_t* get_address_of_Attributes_1() { return &___Attributes_1; }
	inline void set_Attributes_1(int32_t value)
	{
		___Attributes_1 = value;
	}

	inline static int32_t get_offset_of_Length_2() { return static_cast<int32_t>(offsetof(MonoIOStat_t592533987, ___Length_2)); }
	inline int64_t get_Length_2() const { return ___Length_2; }
	inline int64_t* get_address_of_Length_2() { return &___Length_2; }
	inline void set_Length_2(int64_t value)
	{
		___Length_2 = value;
	}

	inline static int32_t get_offset_of_CreationTime_3() { return static_cast<int32_t>(offsetof(MonoIOStat_t592533987, ___CreationTime_3)); }
	inline int64_t get_CreationTime_3() const { return ___CreationTime_3; }
	inline int64_t* get_address_of_CreationTime_3() { return &___CreationTime_3; }
	inline void set_CreationTime_3(int64_t value)
	{
		___CreationTime_3 = value;
	}

	inline static int32_t get_offset_of_LastAccessTime_4() { return static_cast<int32_t>(offsetof(MonoIOStat_t592533987, ___LastAccessTime_4)); }
	inline int64_t get_LastAccessTime_4() const { return ___LastAccessTime_4; }
	inline int64_t* get_address_of_LastAccessTime_4() { return &___LastAccessTime_4; }
	inline void set_LastAccessTime_4(int64_t value)
	{
		___LastAccessTime_4 = value;
	}

	inline static int32_t get_offset_of_LastWriteTime_5() { return static_cast<int32_t>(offsetof(MonoIOStat_t592533987, ___LastWriteTime_5)); }
	inline int64_t get_LastWriteTime_5() const { return ___LastWriteTime_5; }
	inline int64_t* get_address_of_LastWriteTime_5() { return &___LastWriteTime_5; }
	inline void set_LastWriteTime_5(int64_t value)
	{
		___LastWriteTime_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.IO.MonoIOStat
struct MonoIOStat_t592533987_marshaled_pinvoke
{
	char* ___Name_0;
	int32_t ___Attributes_1;
	int64_t ___Length_2;
	int64_t ___CreationTime_3;
	int64_t ___LastAccessTime_4;
	int64_t ___LastWriteTime_5;
};
// Native definition for COM marshalling of System.IO.MonoIOStat
struct MonoIOStat_t592533987_marshaled_com
{
	Il2CppChar* ___Name_0;
	int32_t ___Attributes_1;
	int64_t ___Length_2;
	int64_t ___CreationTime_3;
	int64_t ___LastAccessTime_4;
	int64_t ___LastWriteTime_5;
};
#endif // MONOIOSTAT_T592533987_H
#ifndef FILESYSTEMINFO_T3745885336_H
#define FILESYSTEMINFO_T3745885336_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FileSystemInfo
struct  FileSystemInfo_t3745885336  : public MarshalByRefObject_t2760389100
{
public:
	// System.String System.IO.FileSystemInfo::FullPath
	String_t* ___FullPath_1;
	// System.String System.IO.FileSystemInfo::OriginalPath
	String_t* ___OriginalPath_2;
	// System.IO.MonoIOStat System.IO.FileSystemInfo::stat
	MonoIOStat_t592533987  ___stat_3;
	// System.Boolean System.IO.FileSystemInfo::valid
	bool ___valid_4;

public:
	inline static int32_t get_offset_of_FullPath_1() { return static_cast<int32_t>(offsetof(FileSystemInfo_t3745885336, ___FullPath_1)); }
	inline String_t* get_FullPath_1() const { return ___FullPath_1; }
	inline String_t** get_address_of_FullPath_1() { return &___FullPath_1; }
	inline void set_FullPath_1(String_t* value)
	{
		___FullPath_1 = value;
		Il2CppCodeGenWriteBarrier((&___FullPath_1), value);
	}

	inline static int32_t get_offset_of_OriginalPath_2() { return static_cast<int32_t>(offsetof(FileSystemInfo_t3745885336, ___OriginalPath_2)); }
	inline String_t* get_OriginalPath_2() const { return ___OriginalPath_2; }
	inline String_t** get_address_of_OriginalPath_2() { return &___OriginalPath_2; }
	inline void set_OriginalPath_2(String_t* value)
	{
		___OriginalPath_2 = value;
		Il2CppCodeGenWriteBarrier((&___OriginalPath_2), value);
	}

	inline static int32_t get_offset_of_stat_3() { return static_cast<int32_t>(offsetof(FileSystemInfo_t3745885336, ___stat_3)); }
	inline MonoIOStat_t592533987  get_stat_3() const { return ___stat_3; }
	inline MonoIOStat_t592533987 * get_address_of_stat_3() { return &___stat_3; }
	inline void set_stat_3(MonoIOStat_t592533987  value)
	{
		___stat_3 = value;
	}

	inline static int32_t get_offset_of_valid_4() { return static_cast<int32_t>(offsetof(FileSystemInfo_t3745885336, ___valid_4)); }
	inline bool get_valid_4() const { return ___valid_4; }
	inline bool* get_address_of_valid_4() { return &___valid_4; }
	inline void set_valid_4(bool value)
	{
		___valid_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILESYSTEMINFO_T3745885336_H
#ifndef FILEINFO_T1169991790_H
#define FILEINFO_T1169991790_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FileInfo
struct  FileInfo_t1169991790  : public FileSystemInfo_t3745885336
{
public:
	// System.Boolean System.IO.FileInfo::exists
	bool ___exists_5;

public:
	inline static int32_t get_offset_of_exists_5() { return static_cast<int32_t>(offsetof(FileInfo_t1169991790, ___exists_5)); }
	inline bool get_exists_5() const { return ___exists_5; }
	inline bool* get_address_of_exists_5() { return &___exists_5; }
	inline void set_exists_5(bool value)
	{
		___exists_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEINFO_T1169991790_H
#ifndef DIRECTORYINFO_T35957480_H
#define DIRECTORYINFO_T35957480_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.DirectoryInfo
struct  DirectoryInfo_t35957480  : public FileSystemInfo_t3745885336
{
public:
	// System.String System.IO.DirectoryInfo::current
	String_t* ___current_5;
	// System.String System.IO.DirectoryInfo::parent
	String_t* ___parent_6;

public:
	inline static int32_t get_offset_of_current_5() { return static_cast<int32_t>(offsetof(DirectoryInfo_t35957480, ___current_5)); }
	inline String_t* get_current_5() const { return ___current_5; }
	inline String_t** get_address_of_current_5() { return &___current_5; }
	inline void set_current_5(String_t* value)
	{
		___current_5 = value;
		Il2CppCodeGenWriteBarrier((&___current_5), value);
	}

	inline static int32_t get_offset_of_parent_6() { return static_cast<int32_t>(offsetof(DirectoryInfo_t35957480, ___parent_6)); }
	inline String_t* get_parent_6() const { return ___parent_6; }
	inline String_t** get_address_of_parent_6() { return &___parent_6; }
	inline void set_parent_6(String_t* value)
	{
		___parent_6 = value;
		Il2CppCodeGenWriteBarrier((&___parent_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTORYINFO_T35957480_H
// System.String[]
struct StringU5BU5D_t1281789340  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Char[]
struct CharU5BU5D_t3528271667  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Il2CppChar m_Items[1];

public:
	inline Il2CppChar GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppChar value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Il2CppChar GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppChar value)
	{
		m_Items[index] = value;
	}
};


// System.Collections.Generic.Dictionary`2<K,V> UnityEditor.XCodeEditor.XCPlist::HashtableToDictionary<System.Object,System.Object>(System.Collections.Hashtable)
extern "C"  Dictionary_2_t132545152 * XCPlist_HashtableToDictionary_TisRuntimeObject_TisRuntimeObject_m2110740326_gshared (RuntimeObject * __this /* static, unused */, Hashtable_t1853889766 * ___table0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::set_Item(!0,!1)
extern "C"  void Dictionary_2_set_Item_m3474379962_gshared (Dictionary_2_t132545152 * __this, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ContainsKey(!0)
extern "C"  bool Dictionary_2_ContainsKey_m2278349286_gshared (Dictionary_2_t132545152 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// !1 System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Item(!0)
extern "C"  RuntimeObject * Dictionary_2_get_Item_m2714930061_gshared (Dictionary_2_t132545152 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m2321703786_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C"  void List_1_Add_m3338814081_gshared (List_1_t257213610 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m518943619_gshared (Dictionary_2_t132545152 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C"  int32_t List_1_get_Count_m2934127733_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C"  Enumerator_t2146457487  List_1_GetEnumerator_m2930774921_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C"  RuntimeObject * Enumerator_get_Current_m470245444_gshared (Enumerator_t2146457487 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2142368520_gshared (Enumerator_t2146457487 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3007748546_gshared (Enumerator_t2146457487 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m3919933788_gshared (Dictionary_2_t132545152 * __this, const RuntimeMethod* method);
// System.Void UnityEditor.XCodeEditor.PBXSortedDictionary`1<System.Object>::.ctor(UnityEditor.XCodeEditor.PBXDictionary)
extern "C"  void PBXSortedDictionary_1__ctor_m1139504554_gshared (PBXSortedDictionary_1_t3225971590 * __this, PBXDictionary_t2078602241 * p0, const RuntimeMethod* method);
// System.Void UnityEditor.XCodeEditor.PBXDictionary`1<System.Object>::.ctor(UnityEditor.XCodeEditor.PBXDictionary)
extern "C"  void PBXDictionary_1__ctor_m3440743536_gshared (PBXDictionary_1_t1529852743 * __this, PBXDictionary_t2078602241 * p0, const RuntimeMethod* method);
// System.Collections.Generic.Dictionary`2/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2<System.Object,System.Object>::GetEnumerator()
extern "C"  Enumerator_t2086727927  Dictionary_2_GetEnumerator_m3278257048_gshared (Dictionary_2_t132545152 * __this, const RuntimeMethod* method);
// System.Collections.Generic.KeyValuePair`2<!0,!1> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t2530217319  Enumerator_get_Current_m2655181939_gshared (Enumerator_t2086727927 * __this, const RuntimeMethod* method);
// !1 System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Value()
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m3464904234_gshared (KeyValuePair_2_t2530217319 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1107569389_gshared (Enumerator_t2086727927 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3885012575_gshared (Enumerator_t2086727927 * __this, const RuntimeMethod* method);
// System.Void UnityEditor.XCodeEditor.PBXSortedDictionary`1<System.Object>::Add(T)
extern "C"  void PBXSortedDictionary_1_Add_m2839724894_gshared (PBXSortedDictionary_1_t3225971590 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Add(!0,!1)
extern "C"  void Dictionary_2_Add_m2387223709_gshared (Dictionary_2_t132545152 * __this, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::TryGetValue(!0,!1&)
extern "C"  bool Dictionary_2_TryGetValue_m3280774074_gshared (Dictionary_2_t132545152 * __this, RuntimeObject * p0, RuntimeObject ** p1, const RuntimeMethod* method);
// System.Void UnityEditor.XCodeEditor.PBXDictionary`1<System.Object>::Add(T)
extern "C"  void PBXDictionary_1_Add_m796765728_gshared (PBXDictionary_1_t1529852743 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Collections.Generic.SortedDictionary`2/Enumerator<!0,!1> System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::GetEnumerator()
extern "C"  Enumerator_t3055753277  SortedDictionary_2_GetEnumerator_m1049615467_gshared (SortedDictionary_2_t1555065447 * __this, const RuntimeMethod* method);
// System.Collections.Generic.KeyValuePair`2<!0,!1> System.Collections.Generic.SortedDictionary`2/Enumerator<System.Object,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t2530217319  Enumerator_get_Current_m1646926595_gshared (Enumerator_t3055753277 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.SortedDictionary`2/Enumerator<System.Object,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2228859771_gshared (Enumerator_t3055753277 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.SortedDictionary`2/Enumerator<System.Object,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m2851724990_gshared (Enumerator_t3055753277 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Key()
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m1328507389_gshared (KeyValuePair_2_t2530217319 * __this, const RuntimeMethod* method);
// System.Void UnityEditor.XCodeEditor.PBXDictionary::Append<System.Object>(UnityEditor.XCodeEditor.PBXSortedDictionary`1<T>)
extern "C"  void PBXDictionary_Append_TisRuntimeObject_m853028870_gshared (PBXDictionary_t2078602241 * __this, PBXSortedDictionary_1_t3225971590 * ___dictionary0, const RuntimeMethod* method);
// System.Void UnityEditor.XCodeEditor.PBXDictionary::Append<System.Object>(UnityEditor.XCodeEditor.PBXDictionary`1<T>)
extern "C"  void PBXDictionary_Append_TisRuntimeObject_m4248456785_gshared (PBXDictionary_t2078602241 * __this, PBXDictionary_1_t1529852743 * ___dictionary0, const RuntimeMethod* method);

// System.Void UnityEditor.XCodeEditor.PBXObject::.ctor(System.String,UnityEditor.XCodeEditor.PBXDictionary)
extern "C"  void PBXObject__ctor_m664518219 (PBXObject_t3397571057 * __this, String_t* ___guid0, PBXDictionary_t2078602241 * ___dictionary1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Hashtable::.ctor()
extern "C"  void Hashtable__ctor_m1815022027 (Hashtable_t1853889766 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m297566312 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.FileInfo::.ctor(System.String)
extern "C"  void FileInfo__ctor_m3289795077 (FileInfo_t1169991790 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogWarning(System.Object)
extern "C"  void Debug_LogWarning_m3752629331 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.IO.Path::GetFileNameWithoutExtension(System.String)
extern "C"  String_t* Path_GetFileNameWithoutExtension_m895535029 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEditor.XCodeEditor.XCMod::set_name(System.String)
extern "C"  void XCMod_set_name_m2740663738 (XCMod_t1014962890 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.IO.Path::GetDirectoryName(System.String)
extern "C"  String_t* Path_GetDirectoryName_m3496866581 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEditor.XCodeEditor.XCMod::set_path(System.String)
extern "C"  void XCMod_set_path_m633881003 (XCMod_t1014962890 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.IO.StreamReader System.IO.FileInfo::OpenText()
extern "C"  StreamReader_t4009935899 * FileInfo_OpenText_m40196162 (FileInfo_t1169991790 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C"  void Debug_Log_m4051431634 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Object XUPorterJSON.MiniJSON::jsonDecode(System.String)
extern "C"  RuntimeObject * MiniJSON_jsonDecode_m2285616255 (RuntimeObject * __this /* static, unused */, String_t* ___json0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.IO.Path::GetFileName(System.String)
extern "C"  String_t* Path_GetFileName_m1354558116 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String,System.String)
extern "C"  String_t* String_Concat_m3755062657 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UnityException::.ctor(System.String)
extern "C"  void UnityException__ctor_m872329880 (UnityException_t3598173660 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList::.ctor(System.Int32)
extern "C"  void ArrayList__ctor_m3828927650 (ArrayList_t2718874744 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String)
extern "C"  String_t* String_Concat_m3937257545 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEditor.XCodeEditor.XCModFile::.ctor(System.String)
extern "C"  void XCModFile__ctor_m3435055427 (XCModFile_t1016819465 * __this, String_t* ___inputString0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEditor.XCodeEditor.XCModFile::set_isWeak(System.Boolean)
extern "C"  void XCModFile_set_isWeak_m2297174501 (XCModFile_t1016819465 * __this, bool ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::Contains(System.String)
extern "C"  bool String_Contains_m1147431944 (String_t* __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String[] System.String::Split(System.Char[])
extern "C"  StringU5BU5D_t1281789340* String_Split_m3646115398 (String_t* __this, CharU5BU5D_t3528271667* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEditor.XCodeEditor.XCModFile::set_filePath(System.String)
extern "C"  void XCModFile_set_filePath_m1367883274 (XCModFile_t1016819465 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::CompareTo(System.String)
extern "C"  int32_t String_CompareTo_m3414379165 (String_t* __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Object PlistCS.Plist::readPlist(System.String)
extern "C"  RuntimeObject * Plist_readPlist_m3299688712 (RuntimeObject * __this /* static, unused */, String_t* ___path0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.DictionaryEntry::get_Key()
extern "C"  RuntimeObject * DictionaryEntry_get_Key_m3117378551 (DictionaryEntry_t3123975638 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.DictionaryEntry::get_Value()
extern "C"  RuntimeObject * DictionaryEntry_get_Value_m618120527 (DictionaryEntry_t3123975638 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEditor.XCodeEditor.XCPlist::AddPlistItems(System.String,System.Object,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void XCPlist_AddPlistItems_m4004335766 (XCPlist_t2540119850 * __this, String_t* ___key0, RuntimeObject * ___value1, Dictionary_2_t2865362463 * ___dict2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void PlistCS.Plist::writeXml(System.Object,System.String)
extern "C"  void Plist_writeXml_m1164383522 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___value0, String_t* ___path1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEditor.XCodeEditor.XCPlist::processUrlTypes(System.Collections.ArrayList,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void XCPlist_processUrlTypes_m15864857 (XCPlist_t2540119850 * __this, ArrayList_t2718874744 * ___urltypes0, Dictionary_2_t2865362463 * ___dict1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<K,V> UnityEditor.XCodeEditor.XCPlist::HashtableToDictionary<System.String,System.Object>(System.Collections.Hashtable)
#define XCPlist_HashtableToDictionary_TisString_t_TisRuntimeObject_m3428346691(__this /* static, unused */, ___table0, method) ((  Dictionary_2_t2865362463 * (*) (RuntimeObject * /* static, unused */, Hashtable_t1853889766 *, const RuntimeMethod*))XCPlist_HashtableToDictionary_TisRuntimeObject_TisRuntimeObject_m2110740326_gshared)(__this /* static, unused */, ___table0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::set_Item(!0,!1)
#define Dictionary_2_set_Item_m2329160973(__this, p0, p1, method) ((  void (*) (Dictionary_2_t2865362463 *, String_t*, RuntimeObject *, const RuntimeMethod*))Dictionary_2_set_Item_m3474379962_gshared)(__this, p0, p1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Object>::ContainsKey(!0)
#define Dictionary_2_ContainsKey_m1520683221(__this, p0, method) ((  bool (*) (Dictionary_2_t2865362463 *, String_t*, const RuntimeMethod*))Dictionary_2_ContainsKey_m2278349286_gshared)(__this, p0, method)
// !1 System.Collections.Generic.Dictionary`2<System.String,System.Object>::get_Item(!0)
#define Dictionary_2_get_Item_m3179620279(__this, p0, method) ((  RuntimeObject * (*) (Dictionary_2_t2865362463 *, String_t*, const RuntimeMethod*))Dictionary_2_get_Item_m2714930061_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
#define List_1__ctor_m2321703786(__this, method) ((  void (*) (List_1_t257213610 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method)
// System.Boolean System.String::IsNullOrEmpty(System.String)
extern "C"  bool String_IsNullOrEmpty_m2969720369 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
#define List_1_Add_m3338814081(__this, p0, method) ((  void (*) (List_1_t257213610 *, RuntimeObject *, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method)
// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEditor.XCodeEditor.XCPlist::findUrlTypeByName(System.Collections.Generic.List`1<System.Object>,System.String)
extern "C"  Dictionary_2_t2865362463 * XCPlist_findUrlTypeByName_m4027452897 (XCPlist_t2540119850 * __this, List_1_t257213610 * ___bundleUrlTypes0, String_t* ___name1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::.ctor()
#define Dictionary_2__ctor_m3962145734(__this, method) ((  void (*) (Dictionary_2_t2865362463 *, const RuntimeMethod*))Dictionary_2__ctor_m518943619_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
#define List_1_get_Count_m2934127733(__this, method) ((  int32_t (*) (List_1_t257213610 *, const RuntimeMethod*))List_1_get_Count_m2934127733_gshared)(__this, method)
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
#define List_1_GetEnumerator_m2930774921(__this, method) ((  Enumerator_t2146457487  (*) (List_1_t257213610 *, const RuntimeMethod*))List_1_GetEnumerator_m2930774921_gshared)(__this, method)
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
#define Enumerator_get_Current_m470245444(__this, method) ((  RuntimeObject * (*) (Enumerator_t2146457487 *, const RuntimeMethod*))Enumerator_get_Current_m470245444_gshared)(__this, method)
// System.Int32 System.String::Compare(System.String,System.String)
extern "C"  int32_t String_Compare_m3735043349 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
#define Enumerator_MoveNext_m2142368520(__this, method) ((  bool (*) (Enumerator_t2146457487 *, const RuntimeMethod*))Enumerator_MoveNext_m2142368520_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
#define Enumerator_Dispose_m3007748546(__this, method) ((  void (*) (Enumerator_t2146457487 *, const RuntimeMethod*))Enumerator_Dispose_m3007748546_gshared)(__this, method)
// System.Void UnityEditor.XCodeEditor.XCProject::.ctor()
extern "C"  void XCProject__ctor_m2980377193 (XCProject_t3157646134 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IO.Directory::Exists(System.String)
extern "C"  bool Directory_Exists_m1484791558 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::EndsWith(System.String)
extern "C"  bool String_EndsWith_m1901926500 (String_t* __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEditor.XCodeEditor.XCProject::set_projectRootPath(System.String)
extern "C"  void XCProject_set_projectRootPath_m4200784150 (XCProject_t3157646134 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEditor.XCodeEditor.XCProject::set_filePath(System.String)
extern "C"  void XCProject_set_filePath_m1792948959 (XCProject_t3157646134 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String[] System.IO.Directory::GetDirectories(System.String,System.String)
extern "C"  StringU5BU5D_t1281789340* Directory_GetDirectories_m4129725300 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEditor.XCodeEditor.XCProject::get_projectRootPath()
extern "C"  String_t* XCProject_get_projectRootPath_m3623540506 (XCProject_t3157646134 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IO.Path::IsPathRooted(System.String)
extern "C"  bool Path_IsPathRooted_m3515805419 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Application::get_dataPath()
extern "C"  String_t* Application_get_dataPath_m4232621142 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Replace(System.String,System.String)
extern "C"  String_t* String_Replace_m1273907647 (String_t* __this, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEditor.XCodeEditor.XCProject::get_filePath()
extern "C"  String_t* XCProject_get_filePath_m4061641646 (XCProject_t3157646134 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.IO.Path::Combine(System.String,System.String)
extern "C"  String_t* Path_Combine_m3389272516 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEditor.XCodeEditor.PBXParser::.ctor()
extern "C"  void PBXParser__ctor_m3197616242 (PBXParser_t1614426892 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEditor.XCodeEditor.PBXDictionary UnityEditor.XCodeEditor.PBXParser::Decode(System.String)
extern "C"  PBXDictionary_t2078602241 * PBXParser_Decode_m2851013094 (PBXParser_t1614426892 * __this, String_t* ___data0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Exception::.ctor(System.String)
extern "C"  void Exception__ctor_m1152696503 (Exception_t * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.Dictionary`2<System.String,System.Object>::get_Count()
#define Dictionary_2_get_Count_m2664023503(__this, method) ((  int32_t (*) (Dictionary_2_t2865362463 *, const RuntimeMethod*))Dictionary_2_get_Count_m3919933788_gshared)(__this, method)
// System.String System.String::Concat(System.Object,System.Object)
extern "C"  String_t* String_Concat_m904156431 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEditor.XCodeEditor.PBXProject::.ctor(System.String,UnityEditor.XCodeEditor.PBXDictionary)
extern "C"  void PBXProject__ctor_m505489420 (PBXProject_t4035248171 * __this, String_t* ___guid0, PBXDictionary_t2078602241 * ___dictionary1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEditor.XCodeEditor.PBXProject::get_mainGroupID()
extern "C"  String_t* PBXProject_get_mainGroupID_m4232880761 (PBXProject_t4035248171 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEditor.XCodeEditor.PBXGroup::.ctor(System.String,UnityEditor.XCodeEditor.PBXDictionary)
extern "C"  void PBXGroup__ctor_m1354276094 (PBXGroup_t2128436724 * __this, String_t* ___guid0, PBXDictionary_t2078602241 * ___dictionary1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEditor.XCodeEditor.PBXSortedDictionary`1<UnityEditor.XCodeEditor.PBXBuildFile>::.ctor(UnityEditor.XCodeEditor.PBXDictionary)
#define PBXSortedDictionary_1__ctor_m1490815053(__this, p0, method) ((  void (*) (PBXSortedDictionary_1_t2756258120 *, PBXDictionary_t2078602241 *, const RuntimeMethod*))PBXSortedDictionary_1__ctor_m1139504554_gshared)(__this, p0, method)
// System.Void UnityEditor.XCodeEditor.PBXSortedDictionary`1<UnityEditor.XCodeEditor.PBXGroup>::.ctor(UnityEditor.XCodeEditor.PBXDictionary)
#define PBXSortedDictionary_1__ctor_m2076080548(__this, p0, method) ((  void (*) (PBXSortedDictionary_1_t2274302150 *, PBXDictionary_t2078602241 *, const RuntimeMethod*))PBXSortedDictionary_1__ctor_m1139504554_gshared)(__this, p0, method)
// System.Void UnityEditor.XCodeEditor.PBXSortedDictionary`1<UnityEditor.XCodeEditor.PBXFileReference>::.ctor(UnityEditor.XCodeEditor.PBXDictionary)
#define PBXSortedDictionary_1__ctor_m1533001087(__this, p0, method) ((  void (*) (PBXSortedDictionary_1_t637646383 *, PBXDictionary_t2078602241 *, const RuntimeMethod*))PBXSortedDictionary_1__ctor_m1139504554_gshared)(__this, p0, method)
// System.Void UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXVariantGroup>::.ctor(UnityEditor.XCodeEditor.PBXDictionary)
#define PBXDictionary_1__ctor_m2094835722(__this, p0, method) ((  void (*) (PBXDictionary_1_t1414031086 *, PBXDictionary_t2078602241 *, const RuntimeMethod*))PBXDictionary_1__ctor_m3440743536_gshared)(__this, p0, method)
// System.Void UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXNativeTarget>::.ctor(UnityEditor.XCodeEditor.PBXDictionary)
#define PBXDictionary_1__ctor_m4160535383(__this, p0, method) ((  void (*) (PBXDictionary_1_t3538108837 *, PBXDictionary_t2078602241 *, const RuntimeMethod*))PBXDictionary_1__ctor_m3440743536_gshared)(__this, p0, method)
// System.Void UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.XCBuildConfiguration>::.ctor(UnityEditor.XCodeEditor.PBXDictionary)
#define PBXDictionary_1__ctor_m1384061019(__this, p0, method) ((  void (*) (PBXDictionary_1_t4195780175 *, PBXDictionary_t2078602241 *, const RuntimeMethod*))PBXDictionary_1__ctor_m3440743536_gshared)(__this, p0, method)
// System.Void UnityEditor.XCodeEditor.PBXSortedDictionary`1<UnityEditor.XCodeEditor.XCConfigurationList>::.ctor(UnityEditor.XCodeEditor.PBXDictionary)
#define PBXSortedDictionary_1__ctor_m4001532630(__this, p0, method) ((  void (*) (PBXSortedDictionary_1_t1246395450 *, PBXDictionary_t2078602241 *, const RuntimeMethod*))PBXSortedDictionary_1__ctor_m1139504554_gshared)(__this, p0, method)
// System.Void UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXFrameworksBuildPhase>::.ctor(UnityEditor.XCodeEditor.PBXDictionary)
#define PBXDictionary_1__ctor_m2416241266(__this, p0, method) ((  void (*) (PBXDictionary_1_t125107989 *, PBXDictionary_t2078602241 *, const RuntimeMethod*))PBXDictionary_1__ctor_m3440743536_gshared)(__this, p0, method)
// System.Void UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXResourcesBuildPhase>::.ctor(UnityEditor.XCodeEditor.PBXDictionary)
#define PBXDictionary_1__ctor_m537106829(__this, p0, method) ((  void (*) (PBXDictionary_1_t1783080000 *, PBXDictionary_t2078602241 *, const RuntimeMethod*))PBXDictionary_1__ctor_m3440743536_gshared)(__this, p0, method)
// System.Void UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXShellScriptBuildPhase>::.ctor(UnityEditor.XCodeEditor.PBXDictionary)
#define PBXDictionary_1__ctor_m3424980347(__this, p0, method) ((  void (*) (PBXDictionary_1_t1977435415 *, PBXDictionary_t2078602241 *, const RuntimeMethod*))PBXDictionary_1__ctor_m3440743536_gshared)(__this, p0, method)
// System.Void UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXSourcesBuildPhase>::.ctor(UnityEditor.XCodeEditor.PBXDictionary)
#define PBXDictionary_1__ctor_m3351902458(__this, p0, method) ((  void (*) (PBXDictionary_1_t2932651977 *, PBXDictionary_t2078602241 *, const RuntimeMethod*))PBXDictionary_1__ctor_m3440743536_gshared)(__this, p0, method)
// System.Void UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXCopyFilesBuildPhase>::.ctor(UnityEditor.XCodeEditor.PBXDictionary)
#define PBXDictionary_1__ctor_m3032813453(__this, p0, method) ((  void (*) (PBXDictionary_1_t2617057394 *, PBXDictionary_t2078602241 *, const RuntimeMethod*))PBXDictionary_1__ctor_m3440743536_gshared)(__this, p0, method)
// System.Void UnityEditor.XCodeEditor.PBXList::.ctor(System.Object)
extern "C"  void PBXList__ctor_m3538174448 (PBXList_t4289188522 * __this, RuntimeObject * ___firstValue0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEditor.XCodeEditor.XCProject::AddOtherCFlags(UnityEditor.XCodeEditor.PBXList)
extern "C"  bool XCProject_AddOtherCFlags_m432253178 (XCProject_t3157646134 * __this, PBXList_t4289188522 * ___flags0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.XCBuildConfiguration> UnityEditor.XCodeEditor.XCProject::get_buildConfigurations()
extern "C"  PBXDictionary_1_t4195780175 * XCProject_get_buildConfigurations_m1437786018 (XCProject_t3157646134 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2<System.String,UnityEditor.XCodeEditor.XCBuildConfiguration>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m1452846353(__this, method) ((  Enumerator_t3190505374  (*) (Dictionary_2_t1236322599 *, const RuntimeMethod*))Dictionary_2_GetEnumerator_m3278257048_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<!0,!1> System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEditor.XCodeEditor.XCBuildConfiguration>::get_Current()
#define Enumerator_get_Current_m2313683576(__this, method) ((  KeyValuePair_2_t3633994766  (*) (Enumerator_t3190505374 *, const RuntimeMethod*))Enumerator_get_Current_m2655181939_gshared)(__this, method)
// !1 System.Collections.Generic.KeyValuePair`2<System.String,UnityEditor.XCodeEditor.XCBuildConfiguration>::get_Value()
#define KeyValuePair_2_get_Value_m953788127(__this, method) ((  XCBuildConfiguration_t1451066300 * (*) (KeyValuePair_2_t3633994766 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m3464904234_gshared)(__this, method)
// System.Boolean UnityEditor.XCodeEditor.XCBuildConfiguration::AddOtherCFlags(UnityEditor.XCodeEditor.PBXList)
extern "C"  bool XCBuildConfiguration_AddOtherCFlags_m3896651138 (XCBuildConfiguration_t1451066300 * __this, PBXList_t4289188522 * ___flags0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEditor.XCodeEditor.XCBuildConfiguration>::MoveNext()
#define Enumerator_MoveNext_m3171309827(__this, method) ((  bool (*) (Enumerator_t3190505374 *, const RuntimeMethod*))Enumerator_MoveNext_m1107569389_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEditor.XCodeEditor.XCBuildConfiguration>::Dispose()
#define Enumerator_Dispose_m1544304532(__this, method) ((  void (*) (Enumerator_t3190505374 *, const RuntimeMethod*))Enumerator_Dispose_m3885012575_gshared)(__this, method)
// System.Boolean UnityEditor.XCodeEditor.XCProject::AddOtherLinkerFlags(UnityEditor.XCodeEditor.PBXList)
extern "C"  bool XCProject_AddOtherLinkerFlags_m1003050127 (XCProject_t3157646134 * __this, PBXList_t4289188522 * ___flags0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEditor.XCodeEditor.XCBuildConfiguration::AddOtherLinkerFlags(UnityEditor.XCodeEditor.PBXList)
extern "C"  bool XCBuildConfiguration_AddOtherLinkerFlags_m3172986619 (XCBuildConfiguration_t1451066300 * __this, PBXList_t4289188522 * ___flags0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String[])
extern "C"  String_t* String_Concat_m1809518182 (RuntimeObject * __this /* static, unused */, StringU5BU5D_t1281789340* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEditor.XCodeEditor.PBXDictionary UnityEditor.XCodeEditor.PBXObject::get_data()
extern "C"  PBXDictionary_t2078602241 * PBXObject_get_data_m910114329 (PBXObject_t3397571057 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C"  bool String_op_Equality_m920492651 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEditor.XCodeEditor.XCBuildConfiguration::overwriteBuildSetting(System.String,System.String)
extern "C"  bool XCBuildConfiguration_overwriteBuildSetting_m77475801 (XCBuildConfiguration_t1451066300 * __this, String_t* ___settingName0, String_t* ___settingValue1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEditor.XCodeEditor.XCProject::AddHeaderSearchPaths(UnityEditor.XCodeEditor.PBXList)
extern "C"  bool XCProject_AddHeaderSearchPaths_m2713756577 (XCProject_t3157646134 * __this, PBXList_t4289188522 * ___paths0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEditor.XCodeEditor.XCBuildConfiguration::AddHeaderSearchPaths(UnityEditor.XCodeEditor.PBXList,System.Boolean)
extern "C"  bool XCBuildConfiguration_AddHeaderSearchPaths_m4278200854 (XCBuildConfiguration_t1451066300 * __this, PBXList_t4289188522 * ___paths0, bool ___recursive1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEditor.XCodeEditor.XCProject::AddLibrarySearchPaths(UnityEditor.XCodeEditor.PBXList)
extern "C"  bool XCProject_AddLibrarySearchPaths_m452363867 (XCProject_t3157646134 * __this, PBXList_t4289188522 * ___paths0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEditor.XCodeEditor.XCBuildConfiguration::AddLibrarySearchPaths(UnityEditor.XCodeEditor.PBXList,System.Boolean)
extern "C"  bool XCBuildConfiguration_AddLibrarySearchPaths_m1306486965 (XCBuildConfiguration_t1451066300 * __this, PBXList_t4289188522 * ___paths0, bool ___recursive1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEditor.XCodeEditor.XCProject::AddFrameworkSearchPaths(UnityEditor.XCodeEditor.PBXList)
extern "C"  bool XCProject_AddFrameworkSearchPaths_m2470425909 (XCProject_t3157646134 * __this, PBXList_t4289188522 * ___paths0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEditor.XCodeEditor.XCBuildConfiguration::AddFrameworkSearchPaths(UnityEditor.XCodeEditor.PBXList,System.Boolean)
extern "C"  bool XCBuildConfiguration_AddFrameworkSearchPaths_m458952264 (XCBuildConfiguration_t1451066300 * __this, PBXList_t4289188522 * ___paths0, bool ___recursive1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEditor.XCodeEditor.PBXDictionary::.ctor()
extern "C"  void PBXDictionary__ctor_m3541187054 (PBXDictionary_t2078602241 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogError(System.Object)
extern "C"  void Debug_LogError_m2850623458 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IO.File::Exists(System.String)
extern "C"  bool File_Exists_m3943585060 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Uri::.ctor(System.String)
extern "C"  void Uri__ctor_m800430703 (Uri_t100236324 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Uri System.Uri::MakeRelativeUri(System.Uri)
extern "C"  Uri_t100236324 * Uri_MakeRelativeUri_m3468717094 (Uri_t100236324 * __this, Uri_t100236324 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEditor.XCodeEditor.PBXFileReference UnityEditor.XCodeEditor.XCProject::GetFile(System.String)
extern "C"  PBXFileReference_t491780957 * XCProject_GetFile_m1538714244 (XCProject_t3157646134 * __this, String_t* ___name0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C"  Type_t * Type_GetTypeFromHandle_m1620074514 (RuntimeObject * __this /* static, unused */, RuntimeTypeHandle_t3027515415  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Object System.Enum::Parse(System.Type,System.String)
extern "C"  RuntimeObject * Enum_Parse_m1871331077 (RuntimeObject * __this /* static, unused */, Type_t * p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEditor.XCodeEditor.PBXFileReference::.ctor(System.String,UnityEditor.XCodeEditor.TreeEnum)
extern "C"  void PBXFileReference__ctor_m417092888 (PBXFileReference_t491780957 * __this, String_t* ___filePath0, int32_t ___tree1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEditor.XCodeEditor.PBXGroup::AddChild(UnityEditor.XCodeEditor.PBXObject)
extern "C"  String_t* PBXGroup_AddChild_m3690815735 (PBXGroup_t2128436724 * __this, PBXObject_t3397571057 * ___child0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEditor.XCodeEditor.PBXSortedDictionary`1<UnityEditor.XCodeEditor.PBXFileReference> UnityEditor.XCodeEditor.XCProject::get_fileReferences()
extern "C"  PBXSortedDictionary_1_t637646383 * XCProject_get_fileReferences_m858134601 (XCProject_t3157646134 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEditor.XCodeEditor.PBXSortedDictionary`1<UnityEditor.XCodeEditor.PBXFileReference>::Add(T)
#define PBXSortedDictionary_1_Add_m3189495737(__this, p0, method) ((  void (*) (PBXSortedDictionary_1_t637646383 *, PBXFileReference_t491780957 *, const RuntimeMethod*))PBXSortedDictionary_1_Add_m2839724894_gshared)(__this, p0, method)
// System.String UnityEditor.XCodeEditor.PBXObject::get_guid()
extern "C"  String_t* PBXObject_get_guid_m2491609601 (PBXObject_t3397571057 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::Add(!0,!1)
#define Dictionary_2_Add_m825500632(__this, p0, p1, method) ((  void (*) (Dictionary_2_t2865362463 *, String_t*, RuntimeObject *, const RuntimeMethod*))Dictionary_2_Add_m2387223709_gshared)(__this, p0, p1, method)
// UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXFrameworksBuildPhase> UnityEditor.XCodeEditor.XCProject::get_frameworkBuildPhases()
extern "C"  PBXDictionary_1_t125107989 * XCProject_get_frameworkBuildPhases_m3514233196 (XCProject_t3157646134 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2<System.String,UnityEditor.XCodeEditor.PBXFrameworksBuildPhase>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m2923669497(__this, method) ((  Enumerator_t3414800484  (*) (Dictionary_2_t1460617709 *, const RuntimeMethod*))Dictionary_2_GetEnumerator_m3278257048_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<!0,!1> System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEditor.XCodeEditor.PBXFrameworksBuildPhase>::get_Current()
#define Enumerator_get_Current_m2582763622(__this, method) ((  KeyValuePair_2_t3858289876  (*) (Enumerator_t3414800484 *, const RuntimeMethod*))Enumerator_get_Current_m2655181939_gshared)(__this, method)
// System.Void UnityEditor.XCodeEditor.XCProject::BuildAddFile(UnityEditor.XCodeEditor.PBXFileReference,System.Collections.Generic.KeyValuePair`2<System.String,UnityEditor.XCodeEditor.PBXFrameworksBuildPhase>,System.Boolean)
extern "C"  void XCProject_BuildAddFile_m2837910453 (XCProject_t3157646134 * __this, PBXFileReference_t491780957 * ___fileReference0, KeyValuePair_2_t3858289876  ___currentObject1, bool ___weak2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEditor.XCodeEditor.PBXFrameworksBuildPhase>::MoveNext()
#define Enumerator_MoveNext_m2971571199(__this, method) ((  bool (*) (Enumerator_t3414800484 *, const RuntimeMethod*))Enumerator_MoveNext_m1107569389_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEditor.XCodeEditor.PBXFrameworksBuildPhase>::Dispose()
#define Enumerator_Dispose_m636527872(__this, method) ((  void (*) (Enumerator_t3414800484 *, const RuntimeMethod*))Enumerator_Dispose_m3885012575_gshared)(__this, method)
// UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXResourcesBuildPhase> UnityEditor.XCodeEditor.XCProject::get_resourcesBuildPhases()
extern "C"  PBXDictionary_1_t1783080000 * XCProject_get_resourcesBuildPhases_m2464200803 (XCProject_t3157646134 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2<System.String,UnityEditor.XCodeEditor.PBXResourcesBuildPhase>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m2740980526(__this, method) ((  Enumerator_t777805199  (*) (Dictionary_2_t3118589720 *, const RuntimeMethod*))Dictionary_2_GetEnumerator_m3278257048_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<!0,!1> System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEditor.XCodeEditor.PBXResourcesBuildPhase>::get_Current()
#define Enumerator_get_Current_m2465070363(__this, method) ((  KeyValuePair_2_t1221294591  (*) (Enumerator_t777805199 *, const RuntimeMethod*))Enumerator_get_Current_m2655181939_gshared)(__this, method)
// System.Void UnityEditor.XCodeEditor.XCProject::BuildAddFile(UnityEditor.XCodeEditor.PBXFileReference,System.Collections.Generic.KeyValuePair`2<System.String,UnityEditor.XCodeEditor.PBXResourcesBuildPhase>,System.Boolean)
extern "C"  void XCProject_BuildAddFile_m2859561336 (XCProject_t3157646134 * __this, PBXFileReference_t491780957 * ___fileReference0, KeyValuePair_2_t1221294591  ___currentObject1, bool ___weak2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEditor.XCodeEditor.PBXResourcesBuildPhase>::MoveNext()
#define Enumerator_MoveNext_m413976394(__this, method) ((  bool (*) (Enumerator_t777805199 *, const RuntimeMethod*))Enumerator_MoveNext_m1107569389_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEditor.XCodeEditor.PBXResourcesBuildPhase>::Dispose()
#define Enumerator_Dispose_m649549926(__this, method) ((  void (*) (Enumerator_t777805199 *, const RuntimeMethod*))Enumerator_Dispose_m3885012575_gshared)(__this, method)
// UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXShellScriptBuildPhase> UnityEditor.XCodeEditor.XCProject::get_shellScriptBuildPhases()
extern "C"  PBXDictionary_1_t1977435415 * XCProject_get_shellScriptBuildPhases_m2311296572 (XCProject_t3157646134 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2<System.String,UnityEditor.XCodeEditor.PBXShellScriptBuildPhase>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m1885921306(__this, method) ((  Enumerator_t972160614  (*) (Dictionary_2_t3312945135 *, const RuntimeMethod*))Dictionary_2_GetEnumerator_m3278257048_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<!0,!1> System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEditor.XCodeEditor.PBXShellScriptBuildPhase>::get_Current()
#define Enumerator_get_Current_m585116751(__this, method) ((  KeyValuePair_2_t1415650006  (*) (Enumerator_t972160614 *, const RuntimeMethod*))Enumerator_get_Current_m2655181939_gshared)(__this, method)
// System.Void UnityEditor.XCodeEditor.XCProject::BuildAddFile(UnityEditor.XCodeEditor.PBXFileReference,System.Collections.Generic.KeyValuePair`2<System.String,UnityEditor.XCodeEditor.PBXShellScriptBuildPhase>,System.Boolean)
extern "C"  void XCProject_BuildAddFile_m2061606299 (XCProject_t3157646134 * __this, PBXFileReference_t491780957 * ___fileReference0, KeyValuePair_2_t1415650006  ___currentObject1, bool ___weak2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEditor.XCodeEditor.PBXShellScriptBuildPhase>::MoveNext()
#define Enumerator_MoveNext_m3699906100(__this, method) ((  bool (*) (Enumerator_t972160614 *, const RuntimeMethod*))Enumerator_MoveNext_m1107569389_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEditor.XCodeEditor.PBXShellScriptBuildPhase>::Dispose()
#define Enumerator_Dispose_m2093597648(__this, method) ((  void (*) (Enumerator_t972160614 *, const RuntimeMethod*))Enumerator_Dispose_m3885012575_gshared)(__this, method)
// UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXSourcesBuildPhase> UnityEditor.XCodeEditor.XCProject::get_sourcesBuildPhases()
extern "C"  PBXDictionary_1_t2932651977 * XCProject_get_sourcesBuildPhases_m3700393290 (XCProject_t3157646134 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2<System.String,UnityEditor.XCodeEditor.PBXSourcesBuildPhase>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m951612022(__this, method) ((  Enumerator_t1927377176  (*) (Dictionary_2_t4268161697 *, const RuntimeMethod*))Dictionary_2_GetEnumerator_m3278257048_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<!0,!1> System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEditor.XCodeEditor.PBXSourcesBuildPhase>::get_Current()
#define Enumerator_get_Current_m2899533568(__this, method) ((  KeyValuePair_2_t2370866568  (*) (Enumerator_t1927377176 *, const RuntimeMethod*))Enumerator_get_Current_m2655181939_gshared)(__this, method)
// System.Void UnityEditor.XCodeEditor.XCProject::BuildAddFile(UnityEditor.XCodeEditor.PBXFileReference,System.Collections.Generic.KeyValuePair`2<System.String,UnityEditor.XCodeEditor.PBXSourcesBuildPhase>,System.Boolean)
extern "C"  void XCProject_BuildAddFile_m1056009297 (XCProject_t3157646134 * __this, PBXFileReference_t491780957 * ___fileReference0, KeyValuePair_2_t2370866568  ___currentObject1, bool ___weak2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEditor.XCodeEditor.PBXSourcesBuildPhase>::MoveNext()
#define Enumerator_MoveNext_m1034167961(__this, method) ((  bool (*) (Enumerator_t1927377176 *, const RuntimeMethod*))Enumerator_MoveNext_m1107569389_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEditor.XCodeEditor.PBXSourcesBuildPhase>::Dispose()
#define Enumerator_Dispose_m754445729(__this, method) ((  void (*) (Enumerator_t1927377176 *, const RuntimeMethod*))Enumerator_Dispose_m3885012575_gshared)(__this, method)
// UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXCopyFilesBuildPhase> UnityEditor.XCodeEditor.XCProject::get_copyBuildPhases()
extern "C"  PBXDictionary_1_t2617057394 * XCProject_get_copyBuildPhases_m3593876452 (XCProject_t3157646134 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2<System.String,UnityEditor.XCodeEditor.PBXCopyFilesBuildPhase>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m2675864505(__this, method) ((  Enumerator_t1611782593  (*) (Dictionary_2_t3952567114 *, const RuntimeMethod*))Dictionary_2_GetEnumerator_m3278257048_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<!0,!1> System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEditor.XCodeEditor.PBXCopyFilesBuildPhase>::get_Current()
#define Enumerator_get_Current_m1936049740(__this, method) ((  KeyValuePair_2_t2055271985  (*) (Enumerator_t1611782593 *, const RuntimeMethod*))Enumerator_get_Current_m2655181939_gshared)(__this, method)
// System.Void UnityEditor.XCodeEditor.XCProject::BuildAddFile(UnityEditor.XCodeEditor.PBXFileReference,System.Collections.Generic.KeyValuePair`2<System.String,UnityEditor.XCodeEditor.PBXCopyFilesBuildPhase>,System.Boolean)
extern "C"  void XCProject_BuildAddFile_m2193813989 (XCProject_t3157646134 * __this, PBXFileReference_t491780957 * ___fileReference0, KeyValuePair_2_t2055271985  ___currentObject1, bool ___weak2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEditor.XCodeEditor.PBXCopyFilesBuildPhase>::MoveNext()
#define Enumerator_MoveNext_m3513999528(__this, method) ((  bool (*) (Enumerator_t1611782593 *, const RuntimeMethod*))Enumerator_MoveNext_m1107569389_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEditor.XCodeEditor.PBXCopyFilesBuildPhase>::Dispose()
#define Enumerator_Dispose_m1838677398(__this, method) ((  void (*) (Enumerator_t1611782593 *, const RuntimeMethod*))Enumerator_Dispose_m3885012575_gshared)(__this, method)
// UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXNativeTarget> UnityEditor.XCodeEditor.XCProject::get_nativeTargets()
extern "C"  PBXDictionary_1_t3538108837 * XCProject_get_nativeTargets_m2714694472 (XCProject_t3157646134 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2<System.String,UnityEditor.XCodeEditor.PBXNativeTarget>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m170078521(__this, method) ((  Enumerator_t2532834036  (*) (Dictionary_2_t578651261 *, const RuntimeMethod*))Dictionary_2_GetEnumerator_m3278257048_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<!0,!1> System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEditor.XCodeEditor.PBXNativeTarget>::get_Current()
#define Enumerator_get_Current_m4204254741(__this, method) ((  KeyValuePair_2_t2976323428  (*) (Enumerator_t2532834036 *, const RuntimeMethod*))Enumerator_get_Current_m2655181939_gshared)(__this, method)
// !1 System.Collections.Generic.KeyValuePair`2<System.String,UnityEditor.XCodeEditor.PBXNativeTarget>::get_Value()
#define KeyValuePair_2_get_Value_m543951955(__this, method) ((  PBXNativeTarget_t793394962 * (*) (KeyValuePair_2_t2976323428 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m3464904234_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEditor.XCodeEditor.PBXNativeTarget>::MoveNext()
#define Enumerator_MoveNext_m726921650(__this, method) ((  bool (*) (Enumerator_t2532834036 *, const RuntimeMethod*))Enumerator_MoveNext_m1107569389_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEditor.XCodeEditor.PBXNativeTarget>::Dispose()
#define Enumerator_Dispose_m3454157324(__this, method) ((  void (*) (Enumerator_t2532834036 *, const RuntimeMethod*))Enumerator_Dispose_m3885012575_gshared)(__this, method)
// !1 System.Collections.Generic.KeyValuePair`2<System.String,UnityEditor.XCodeEditor.PBXCopyFilesBuildPhase>::get_Value()
#define KeyValuePair_2_get_Value_m4174862611(__this, method) ((  PBXCopyFilesBuildPhase_t4167310815 * (*) (KeyValuePair_2_t2055271985 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m3464904234_gshared)(__this, method)
// UnityEditor.XCodeEditor.PBXNativeTarget UnityEditor.XCodeEditor.XCProject::GetNativeTarget(System.String)
extern "C"  PBXNativeTarget_t793394962 * XCProject_GetNativeTarget_m2903336074 (XCProject_t3157646134 * __this, String_t* ___name0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Object>::TryGetValue(!0,!1&)
#define Dictionary_2_TryGetValue_m2997259014(__this, p0, p1, method) ((  bool (*) (Dictionary_2_t2865362463 *, String_t*, RuntimeObject **, const RuntimeMethod*))Dictionary_2_TryGetValue_m3280774074_gshared)(__this, p0, p1, method)
// System.Int32 UnityEditor.XCodeEditor.XCProject::GetBuildActionMask()
extern "C"  int32_t XCProject_GetBuildActionMask_m3043263402 (XCProject_t3157646134 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEditor.XCodeEditor.PBXCopyFilesBuildPhase::.ctor(System.Int32)
extern "C"  void PBXCopyFilesBuildPhase__ctor_m2022625091 (PBXCopyFilesBuildPhase_t4167310815 * __this, int32_t ___buildActionMask0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXCopyFilesBuildPhase>::Add(T)
#define PBXDictionary_1_Add_m1199137400(__this, p0, method) ((  void (*) (PBXDictionary_1_t2617057394 *, PBXCopyFilesBuildPhase_t4167310815 *, const RuntimeMethod*))PBXDictionary_1_Add_m796765728_gshared)(__this, p0, method)
// UnityEditor.XCodeEditor.PBXCopyFilesBuildPhase UnityEditor.XCodeEditor.XCProject::AddEmbedFrameworkBuildPhase()
extern "C"  PBXCopyFilesBuildPhase_t4167310815 * XCProject_AddEmbedFrameworkBuildPhase_m2982154168 (XCProject_t3157646134 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEditor.XCodeEditor.PBXBuildFile::.ctor(UnityEditor.XCodeEditor.PBXFileReference,System.Boolean)
extern "C"  void PBXBuildFile__ctor_m879448857 (PBXBuildFile_t2610392694 * __this, PBXFileReference_t491780957 * ___fileRef0, bool ___weak1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEditor.XCodeEditor.PBXBuildFile::AddCodeSignOnCopy()
extern "C"  bool PBXBuildFile_AddCodeSignOnCopy_m269553985 (PBXBuildFile_t2610392694 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEditor.XCodeEditor.PBXSortedDictionary`1<UnityEditor.XCodeEditor.PBXBuildFile> UnityEditor.XCodeEditor.XCProject::get_buildFiles()
extern "C"  PBXSortedDictionary_1_t2756258120 * XCProject_get_buildFiles_m2028255916 (XCProject_t3157646134 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEditor.XCodeEditor.PBXSortedDictionary`1<UnityEditor.XCodeEditor.PBXBuildFile>::Add(T)
#define PBXSortedDictionary_1_Add_m3739564697(__this, p0, method) ((  void (*) (PBXSortedDictionary_1_t2756258120 *, PBXBuildFile_t2610392694 *, const RuntimeMethod*))PBXSortedDictionary_1_Add_m2839724894_gshared)(__this, p0, method)
// System.Boolean UnityEditor.XCodeEditor.PBXBuildPhase::AddBuildFile(UnityEditor.XCodeEditor.PBXBuildFile)
extern "C"  bool PBXBuildPhase_AddBuildFile_m1828418701 (PBXBuildPhase_t3890765868 * __this, PBXBuildFile_t2610392694 * ___file0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !1 System.Collections.Generic.KeyValuePair`2<System.String,UnityEditor.XCodeEditor.PBXFrameworksBuildPhase>::get_Value()
#define KeyValuePair_2_get_Value_m2936696925(__this, method) ((  PBXFrameworksBuildPhase_t1675361410 * (*) (KeyValuePair_2_t3858289876 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m3464904234_gshared)(__this, method)
// !1 System.Collections.Generic.KeyValuePair`2<System.String,UnityEditor.XCodeEditor.PBXResourcesBuildPhase>::get_Value()
#define KeyValuePair_2_get_Value_m887331346(__this, method) ((  PBXResourcesBuildPhase_t3333333421 * (*) (KeyValuePair_2_t1221294591 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m3464904234_gshared)(__this, method)
// !1 System.Collections.Generic.KeyValuePair`2<System.String,UnityEditor.XCodeEditor.PBXShellScriptBuildPhase>::get_Value()
#define KeyValuePair_2_get_Value_m4161882241(__this, method) ((  PBXShellScriptBuildPhase_t3527688836 * (*) (KeyValuePair_2_t1415650006 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m3464904234_gshared)(__this, method)
// !1 System.Collections.Generic.KeyValuePair`2<System.String,UnityEditor.XCodeEditor.PBXSourcesBuildPhase>::get_Value()
#define KeyValuePair_2_get_Value_m990072085(__this, method) ((  PBXSourcesBuildPhase_t187938102 * (*) (KeyValuePair_2_t2370866568 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m3464904234_gshared)(__this, method)
// System.Boolean UnityEditor.XCodeEditor.XCProject::AddLocFolder(System.String,UnityEditor.XCodeEditor.PBXGroup,System.String[],System.Boolean)
extern "C"  bool XCProject_AddLocFolder_m1925302028 (XCProject_t3157646134 * __this, String_t* ___folderPath0, PBXGroup_t2128436724 * ___parent1, StringU5BU5D_t1281789340* ___exclude2, bool ___createBuildFile3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.DirectoryInfo::.ctor(System.String)
extern "C"  void DirectoryInfo__ctor_m1000259829 (DirectoryInfo_t35957480 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEditor.XCodeEditor.PBXGroup UnityEditor.XCodeEditor.XCProject::get_rootGroup()
extern "C"  PBXGroup_t2128436724 * XCProject_get_rootGroup_m3903765605 (XCProject_t3157646134 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEditor.XCodeEditor.PBXGroup UnityEditor.XCodeEditor.XCProject::GetGroup(System.String,System.String,UnityEditor.XCodeEditor.PBXGroup)
extern "C"  PBXGroup_t2128436724 * XCProject_GetGroup_m2965518548 (XCProject_t3157646134 * __this, String_t* ___name0, String_t* ___path1, PBXGroup_t2128436724 * ___parent2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String[] System.IO.Directory::GetDirectories(System.String)
extern "C"  StringU5BU5D_t1281789340* Directory_GetDirectories_m1966820948 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEditor.XCodeEditor.PBXDictionary UnityEditor.XCodeEditor.XCProject::AddFile(System.String,UnityEditor.XCodeEditor.PBXGroup,System.String,System.Boolean,System.Boolean)
extern "C"  PBXDictionary_t2078602241 * XCProject_AddFile_m3957251854 (XCProject_t3157646134 * __this, String_t* ___filePath0, PBXGroup_t2128436724 * ___parent1, String_t* ___tree2, bool ___createBuildFiles3, bool ___weak4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEditor.XCodeEditor.XCProject::AddFolder(System.String,UnityEditor.XCodeEditor.PBXGroup,System.String[],System.Boolean,System.Boolean)
extern "C"  bool XCProject_AddFolder_m3825015009 (XCProject_t3157646134 * __this, String_t* ___folderPath0, PBXGroup_t2128436724 * ___parent1, StringU5BU5D_t1281789340* ___exclude2, bool ___recursive3, bool ___createBuildFile4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Join(System.String,System.String[])
extern "C"  String_t* String_Join_m2050845953 (RuntimeObject * __this /* static, unused */, String_t* p0, StringU5BU5D_t1281789340* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Format(System.String,System.Object)
extern "C"  String_t* String_Format_m2844511972 (RuntimeObject * __this /* static, unused */, String_t* p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String[] System.IO.Directory::GetFiles(System.String)
extern "C"  StringU5BU5D_t1281789340* Directory_GetFiles_m932257245 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Text.RegularExpressions.Regex::IsMatch(System.String,System.String)
extern "C"  bool Regex_IsMatch_m3266004395 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.IO.FileInfo::get_DirectoryName()
extern "C"  String_t* FileInfo_get_DirectoryName_m576702270 (FileInfo_t1169991790 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::get_Length()
extern "C"  int32_t String_get_Length_m3847582255 (String_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Substring(System.Int32,System.Int32)
extern "C"  String_t* String_Substring_m1610150815 (String_t* __this, int32_t p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEditor.XCodeEditor.PBXProject UnityEditor.XCodeEditor.XCProject::get_project()
extern "C"  PBXProject_t4035248171 * XCProject_get_project_m2578328735 (XCProject_t3157646134 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEditor.XCodeEditor.PBXProject::AddRegion(System.String)
extern "C"  void PBXProject_AddRegion_m58745414 (PBXProject_t4035248171 * __this, String_t* ___region0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEditor.XCodeEditor.PBXVariantGroup::.ctor(System.String,System.String,System.String)
extern "C"  void PBXVariantGroup__ctor_m2891305890 (PBXVariantGroup_t2964284507 * __this, String_t* ___name0, String_t* ___path1, String_t* ___tree2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXVariantGroup> UnityEditor.XCodeEditor.XCProject::get_variantGroups()
extern "C"  PBXDictionary_1_t1414031086 * XCProject_get_variantGroups_m3960773799 (XCProject_t3157646134 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXVariantGroup>::Add(T)
#define PBXDictionary_1_Add_m3289887501(__this, p0, method) ((  void (*) (PBXDictionary_1_t1414031086 *, PBXVariantGroup_t2964284507 *, const RuntimeMethod*))PBXDictionary_1_Add_m796765728_gshared)(__this, p0, method)
// System.Collections.Generic.SortedDictionary`2/Enumerator<!0,!1> System.Collections.Generic.SortedDictionary`2<System.String,UnityEditor.XCodeEditor.PBXFileReference>::GetEnumerator()
#define SortedDictionary_2_GetEnumerator_m2931611635(__this, method) ((  Enumerator_t3200245381  (*) (SortedDictionary_2_t1699557551 *, const RuntimeMethod*))SortedDictionary_2_GetEnumerator_m1049615467_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<!0,!1> System.Collections.Generic.SortedDictionary`2/Enumerator<System.String,UnityEditor.XCodeEditor.PBXFileReference>::get_Current()
#define Enumerator_get_Current_m2415991694(__this, method) ((  KeyValuePair_2_t2674709423  (*) (Enumerator_t3200245381 *, const RuntimeMethod*))Enumerator_get_Current_m1646926595_gshared)(__this, method)
// !1 System.Collections.Generic.KeyValuePair`2<System.String,UnityEditor.XCodeEditor.PBXFileReference>::get_Value()
#define KeyValuePair_2_get_Value_m3214895324(__this, method) ((  PBXFileReference_t491780957 * (*) (KeyValuePair_2_t2674709423 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m3464904234_gshared)(__this, method)
// System.String UnityEditor.XCodeEditor.PBXFileReference::get_name()
extern "C"  String_t* PBXFileReference_get_name_m3005884813 (PBXFileReference_t491780957 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.SortedDictionary`2/Enumerator<System.String,UnityEditor.XCodeEditor.PBXFileReference>::MoveNext()
#define Enumerator_MoveNext_m266277119(__this, method) ((  bool (*) (Enumerator_t3200245381 *, const RuntimeMethod*))Enumerator_MoveNext_m2228859771_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2/Enumerator<System.String,UnityEditor.XCodeEditor.PBXFileReference>::Dispose()
#define Enumerator_Dispose_m3852176452(__this, method) ((  void (*) (Enumerator_t3200245381 *, const RuntimeMethod*))Enumerator_Dispose_m2851724990_gshared)(__this, method)
// UnityEditor.XCodeEditor.PBXSortedDictionary`1<UnityEditor.XCodeEditor.PBXGroup> UnityEditor.XCodeEditor.XCProject::get_groups()
extern "C"  PBXSortedDictionary_1_t2274302150 * XCProject_get_groups_m1730659681 (XCProject_t3157646134 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.SortedDictionary`2/Enumerator<!0,!1> System.Collections.Generic.SortedDictionary`2<System.String,UnityEditor.XCodeEditor.PBXGroup>::GetEnumerator()
#define SortedDictionary_2_GetEnumerator_m1969713601(__this, method) ((  Enumerator_t541933852  (*) (SortedDictionary_2_t3336213318 *, const RuntimeMethod*))SortedDictionary_2_GetEnumerator_m1049615467_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<!0,!1> System.Collections.Generic.SortedDictionary`2/Enumerator<System.String,UnityEditor.XCodeEditor.PBXGroup>::get_Current()
#define Enumerator_get_Current_m1763852423(__this, method) ((  KeyValuePair_2_t16397894  (*) (Enumerator_t541933852 *, const RuntimeMethod*))Enumerator_get_Current_m1646926595_gshared)(__this, method)
// !1 System.Collections.Generic.KeyValuePair`2<System.String,UnityEditor.XCodeEditor.PBXGroup>::get_Value()
#define KeyValuePair_2_get_Value_m3727113552(__this, method) ((  PBXGroup_t2128436724 * (*) (KeyValuePair_2_t16397894 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m3464904234_gshared)(__this, method)
// System.String UnityEditor.XCodeEditor.PBXGroup::get_name()
extern "C"  String_t* PBXGroup_get_name_m1192634509 (PBXGroup_t2128436724 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEditor.XCodeEditor.PBXGroup::get_path()
extern "C"  String_t* PBXGroup_get_path_m1501355754 (PBXGroup_t2128436724 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !0 System.Collections.Generic.KeyValuePair`2<System.String,UnityEditor.XCodeEditor.PBXGroup>::get_Key()
#define KeyValuePair_2_get_Key_m2204949747(__this, method) ((  String_t* (*) (KeyValuePair_2_t16397894 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m1328507389_gshared)(__this, method)
// System.Boolean UnityEditor.XCodeEditor.PBXGroup::HasChild(System.String)
extern "C"  bool PBXGroup_HasChild_m177685167 (PBXGroup_t2128436724 * __this, String_t* ___id0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.SortedDictionary`2/Enumerator<System.String,UnityEditor.XCodeEditor.PBXGroup>::MoveNext()
#define Enumerator_MoveNext_m1528297490(__this, method) ((  bool (*) (Enumerator_t541933852 *, const RuntimeMethod*))Enumerator_MoveNext_m2228859771_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2/Enumerator<System.String,UnityEditor.XCodeEditor.PBXGroup>::Dispose()
#define Enumerator_Dispose_m4154903118(__this, method) ((  void (*) (Enumerator_t541933852 *, const RuntimeMethod*))Enumerator_Dispose_m2851724990_gshared)(__this, method)
// System.Void UnityEditor.XCodeEditor.PBXGroup::.ctor(System.String,System.String,System.String)
extern "C"  void PBXGroup__ctor_m605567610 (PBXGroup_t2128436724 * __this, String_t* ___name0, String_t* ___path1, String_t* ___tree2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEditor.XCodeEditor.PBXSortedDictionary`1<UnityEditor.XCodeEditor.PBXGroup>::Add(T)
#define PBXSortedDictionary_1_Add_m259638508(__this, p0, method) ((  void (*) (PBXSortedDictionary_1_t2274302150 *, PBXGroup_t2128436724 *, const RuntimeMethod*))PBXSortedDictionary_1_Add_m2839724894_gshared)(__this, p0, method)
// System.Void UnityEditor.XCodeEditor.XCMod::.ctor(System.String)
extern "C"  void XCMod__ctor_m1865170813 (XCMod_t1014962890 * __this, String_t* ___filename0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList UnityEditor.XCodeEditor.XCMod::get_libs()
extern "C"  ArrayList_t2718874744 * XCMod_get_libs_m513448774 (XCMod_t1014962890 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEditor.XCodeEditor.XCProject::ApplyMod(UnityEditor.XCodeEditor.XCMod)
extern "C"  void XCProject_ApplyMod_m3992715734 (XCProject_t3157646134 * __this, XCMod_t1014962890 * ___mod0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEditor.XCodeEditor.XCMod::get_group()
extern "C"  String_t* XCMod_get_group_m1801882381 (XCMod_t1014962890 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEditor.XCodeEditor.XCModFile::get_filePath()
extern "C"  String_t* XCModFile_get_filePath_m2182538701 (XCModFile_t1016819465 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEditor.XCodeEditor.XCModFile::get_isWeak()
extern "C"  bool XCModFile_get_isWeak_m3536481063 (XCModFile_t1016819465 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList UnityEditor.XCodeEditor.XCMod::get_frameworks()
extern "C"  ArrayList_t2718874744 * XCMod_get_frameworks_m2568364667 (XCMod_t1014962890 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList UnityEditor.XCodeEditor.XCMod::get_files()
extern "C"  ArrayList_t2718874744 * XCMod_get_files_m150690371 (XCMod_t1014962890 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEditor.XCodeEditor.XCMod::get_path()
extern "C"  String_t* XCMod_get_path_m530774122 (XCMod_t1014962890 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList UnityEditor.XCodeEditor.XCMod::get_embed_binaries()
extern "C"  ArrayList_t2718874744 * XCMod_get_embed_binaries_m3193459801 (XCMod_t1014962890 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEditor.XCodeEditor.XCProject::overwriteBuildSetting(System.String,System.String,System.String)
extern "C"  bool XCProject_overwriteBuildSetting_m3219624 (XCProject_t3157646134 * __this, String_t* ___settingName0, String_t* ___newValue1, String_t* ___buildConfigName2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEditor.XCodeEditor.XCProject::AddEmbedFramework(System.String)
extern "C"  void XCProject_AddEmbedFramework_m1485538180 (XCProject_t3157646134 * __this, String_t* ___fileName0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList UnityEditor.XCodeEditor.XCMod::get_folders()
extern "C"  ArrayList_t2718874744 * XCMod_get_folders_m1874296509 (XCMod_t1014962890 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList UnityEditor.XCodeEditor.XCMod::get_excludes()
extern "C"  ArrayList_t2718874744 * XCMod_get_excludes_m1351011736 (XCMod_t1014962890 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList UnityEditor.XCodeEditor.XCMod::get_headerpaths()
extern "C"  ArrayList_t2718874744 * XCMod_get_headerpaths_m343671542 (XCMod_t1014962890 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEditor.XCodeEditor.XCProject::AddHeaderSearchPaths(System.String)
extern "C"  bool XCProject_AddHeaderSearchPaths_m2329174020 (XCProject_t3157646134 * __this, String_t* ___path0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList UnityEditor.XCodeEditor.XCMod::get_compiler_flags()
extern "C"  ArrayList_t2718874744 * XCMod_get_compiler_flags_m333845873 (XCMod_t1014962890 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEditor.XCodeEditor.XCProject::AddOtherCFlags(System.String)
extern "C"  bool XCProject_AddOtherCFlags_m696039661 (XCProject_t3157646134 * __this, String_t* ___flag0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList UnityEditor.XCodeEditor.XCMod::get_linker_flags()
extern "C"  ArrayList_t2718874744 * XCMod_get_linker_flags_m3777781269 (XCMod_t1014962890 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEditor.XCodeEditor.XCProject::AddOtherLinkerFlags(System.String)
extern "C"  bool XCProject_AddOtherLinkerFlags_m1772294907 (XCProject_t3157646134 * __this, String_t* ___flag0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEditor.XCodeEditor.XCPlist::.ctor(System.String)
extern "C"  void XCPlist__ctor_m1809109823 (XCPlist_t2540119850 * __this, String_t* ___plistPath0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Hashtable UnityEditor.XCodeEditor.XCMod::get_plist()
extern "C"  Hashtable_t1853889766 * XCMod_get_plist_m1661036855 (XCMod_t1014962890 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEditor.XCodeEditor.XCPlist::Process(System.Collections.Hashtable)
extern "C"  void XCPlist_Process_m1182457030 (XCPlist_t2540119850 * __this, Hashtable_t1853889766 * ___plist0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEditor.XCodeEditor.XCProject::Consolidate()
extern "C"  void XCProject_Consolidate_m609230681 (XCProject_t3157646134 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEditor.XCodeEditor.PBXDictionary::Append<UnityEditor.XCodeEditor.PBXBuildFile>(UnityEditor.XCodeEditor.PBXSortedDictionary`1<T>)
#define PBXDictionary_Append_TisPBXBuildFile_t2610392694_m3797558854(__this, ___dictionary0, method) ((  void (*) (PBXDictionary_t2078602241 *, PBXSortedDictionary_1_t2756258120 *, const RuntimeMethod*))PBXDictionary_Append_TisRuntimeObject_m853028870_gshared)(__this, ___dictionary0, method)
// System.Void UnityEditor.XCodeEditor.PBXDictionary::Append<UnityEditor.XCodeEditor.PBXCopyFilesBuildPhase>(UnityEditor.XCodeEditor.PBXDictionary`1<T>)
#define PBXDictionary_Append_TisPBXCopyFilesBuildPhase_t4167310815_m945363416(__this, ___dictionary0, method) ((  void (*) (PBXDictionary_t2078602241 *, PBXDictionary_1_t2617057394 *, const RuntimeMethod*))PBXDictionary_Append_TisRuntimeObject_m4248456785_gshared)(__this, ___dictionary0, method)
// System.Void UnityEditor.XCodeEditor.PBXDictionary::Append<UnityEditor.XCodeEditor.PBXFileReference>(UnityEditor.XCodeEditor.PBXSortedDictionary`1<T>)
#define PBXDictionary_Append_TisPBXFileReference_t491780957_m334418948(__this, ___dictionary0, method) ((  void (*) (PBXDictionary_t2078602241 *, PBXSortedDictionary_1_t637646383 *, const RuntimeMethod*))PBXDictionary_Append_TisRuntimeObject_m853028870_gshared)(__this, ___dictionary0, method)
// System.Void UnityEditor.XCodeEditor.PBXDictionary::Append<UnityEditor.XCodeEditor.PBXFrameworksBuildPhase>(UnityEditor.XCodeEditor.PBXDictionary`1<T>)
#define PBXDictionary_Append_TisPBXFrameworksBuildPhase_t1675361410_m876528981(__this, ___dictionary0, method) ((  void (*) (PBXDictionary_t2078602241 *, PBXDictionary_1_t125107989 *, const RuntimeMethod*))PBXDictionary_Append_TisRuntimeObject_m4248456785_gshared)(__this, ___dictionary0, method)
// System.Void UnityEditor.XCodeEditor.PBXDictionary::Append<UnityEditor.XCodeEditor.PBXGroup>(UnityEditor.XCodeEditor.PBXSortedDictionary`1<T>)
#define PBXDictionary_Append_TisPBXGroup_t2128436724_m2862935796(__this, ___dictionary0, method) ((  void (*) (PBXDictionary_t2078602241 *, PBXSortedDictionary_1_t2274302150 *, const RuntimeMethod*))PBXDictionary_Append_TisRuntimeObject_m853028870_gshared)(__this, ___dictionary0, method)
// System.Void UnityEditor.XCodeEditor.PBXDictionary::Append<UnityEditor.XCodeEditor.PBXNativeTarget>(UnityEditor.XCodeEditor.PBXDictionary`1<T>)
#define PBXDictionary_Append_TisPBXNativeTarget_t793394962_m1547927114(__this, ___dictionary0, method) ((  void (*) (PBXDictionary_t2078602241 *, PBXDictionary_1_t3538108837 *, const RuntimeMethod*))PBXDictionary_Append_TisRuntimeObject_m4248456785_gshared)(__this, ___dictionary0, method)
// System.Void UnityEditor.XCodeEditor.PBXDictionary::Append<UnityEditor.XCodeEditor.PBXResourcesBuildPhase>(UnityEditor.XCodeEditor.PBXDictionary`1<T>)
#define PBXDictionary_Append_TisPBXResourcesBuildPhase_t3333333421_m2729679594(__this, ___dictionary0, method) ((  void (*) (PBXDictionary_t2078602241 *, PBXDictionary_1_t1783080000 *, const RuntimeMethod*))PBXDictionary_Append_TisRuntimeObject_m4248456785_gshared)(__this, ___dictionary0, method)
// System.Void UnityEditor.XCodeEditor.PBXDictionary::Append<UnityEditor.XCodeEditor.PBXShellScriptBuildPhase>(UnityEditor.XCodeEditor.PBXDictionary`1<T>)
#define PBXDictionary_Append_TisPBXShellScriptBuildPhase_t3527688836_m3716639730(__this, ___dictionary0, method) ((  void (*) (PBXDictionary_t2078602241 *, PBXDictionary_1_t1977435415 *, const RuntimeMethod*))PBXDictionary_Append_TisRuntimeObject_m4248456785_gshared)(__this, ___dictionary0, method)
// System.Void UnityEditor.XCodeEditor.PBXDictionary::Append<UnityEditor.XCodeEditor.PBXSourcesBuildPhase>(UnityEditor.XCodeEditor.PBXDictionary`1<T>)
#define PBXDictionary_Append_TisPBXSourcesBuildPhase_t187938102_m2147902702(__this, ___dictionary0, method) ((  void (*) (PBXDictionary_t2078602241 *, PBXDictionary_1_t2932651977 *, const RuntimeMethod*))PBXDictionary_Append_TisRuntimeObject_m4248456785_gshared)(__this, ___dictionary0, method)
// System.Void UnityEditor.XCodeEditor.PBXDictionary::Append<UnityEditor.XCodeEditor.PBXVariantGroup>(UnityEditor.XCodeEditor.PBXDictionary`1<T>)
#define PBXDictionary_Append_TisPBXVariantGroup_t2964284507_m211501280(__this, ___dictionary0, method) ((  void (*) (PBXDictionary_t2078602241 *, PBXDictionary_1_t1414031086 *, const RuntimeMethod*))PBXDictionary_Append_TisRuntimeObject_m4248456785_gshared)(__this, ___dictionary0, method)
// System.Void UnityEditor.XCodeEditor.PBXDictionary::Append<UnityEditor.XCodeEditor.XCBuildConfiguration>(UnityEditor.XCodeEditor.PBXDictionary`1<T>)
#define PBXDictionary_Append_TisXCBuildConfiguration_t1451066300_m2304102353(__this, ___dictionary0, method) ((  void (*) (PBXDictionary_t2078602241 *, PBXDictionary_1_t4195780175 *, const RuntimeMethod*))PBXDictionary_Append_TisRuntimeObject_m4248456785_gshared)(__this, ___dictionary0, method)
// UnityEditor.XCodeEditor.PBXSortedDictionary`1<UnityEditor.XCodeEditor.XCConfigurationList> UnityEditor.XCodeEditor.XCProject::get_configurationLists()
extern "C"  PBXSortedDictionary_1_t1246395450 * XCProject_get_configurationLists_m3369988788 (XCProject_t3157646134 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEditor.XCodeEditor.PBXDictionary::Append<UnityEditor.XCodeEditor.XCConfigurationList>(UnityEditor.XCodeEditor.PBXSortedDictionary`1<T>)
#define PBXDictionary_Append_TisXCConfigurationList_t1100530024_m4062973500(__this, ___dictionary0, method) ((  void (*) (PBXDictionary_t2078602241 *, PBXSortedDictionary_1_t1246395450 *, const RuntimeMethod*))PBXDictionary_Append_TisRuntimeObject_m853028870_gshared)(__this, ___dictionary0, method)
// System.Void System.IO.File::Delete(System.String)
extern "C"  void File_Delete_m321251800 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.File::Copy(System.String,System.String)
extern "C"  void File_Copy_m497281780 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.IO.StreamWriter System.IO.File::CreateText(System.String)
extern "C"  StreamWriter_t1266378904 * File_CreateText_m1585655010 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEditor.XCodeEditor.PBXParser::Encode(UnityEditor.XCodeEditor.PBXDictionary,System.Boolean)
extern "C"  String_t* PBXParser_Encode_m3998271658 (PBXParser_t1614426892 * __this, PBXDictionary_t2078602241 * ___pbxData0, bool ___readable1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEditor.XCodeEditor.XCProject::DeleteExisting(System.String)
extern "C"  void XCProject_DeleteExisting_m829740160 (XCProject_t3157646134 * __this, String_t* ___path0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEditor.XCodeEditor.XCProject::CreateNewProject(UnityEditor.XCodeEditor.PBXDictionary,System.String)
extern "C"  void XCProject_CreateNewProject_m1548580596 (XCProject_t3157646134 * __this, PBXDictionary_t2078602241 * ___result0, String_t* ___path1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Char[] System.String::ToCharArray()
extern "C"  CharU5BU5D_t3528271667* String_ToCharArray_m1492846834 (String_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Object XUPorterJSON.MiniJSON::parseValue(System.Char[],System.Int32&,System.Boolean&)
extern "C"  RuntimeObject * MiniJSON_parseValue_m3659675017 (RuntimeObject * __this /* static, unused */, CharU5BU5D_t3528271667* ___json0, int32_t* ___index1, bool* ___success2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.StringBuilder::.ctor(System.Int32)
extern "C"  void StringBuilder__ctor_m2367297767 (StringBuilder_t * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean XUPorterJSON.MiniJSON::serializeValue(System.Object,System.Text.StringBuilder)
extern "C"  bool MiniJSON_serializeValue_m2428040582 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___value0, StringBuilder_t * ___builder1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 XUPorterJSON.MiniJSON::nextToken(System.Char[],System.Int32&)
extern "C"  int32_t MiniJSON_nextToken_m3582996375 (RuntimeObject * __this /* static, unused */, CharU5BU5D_t3528271667* ___json0, int32_t* ___index1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 XUPorterJSON.MiniJSON::lookAhead(System.Char[],System.Int32)
extern "C"  int32_t MiniJSON_lookAhead_m3599256246 (RuntimeObject * __this /* static, unused */, CharU5BU5D_t3528271667* ___json0, int32_t ___index1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String XUPorterJSON.MiniJSON::parseString(System.Char[],System.Int32&)
extern "C"  String_t* MiniJSON_parseString_m2073376077 (RuntimeObject * __this /* static, unused */, CharU5BU5D_t3528271667* ___json0, int32_t* ___index1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList::.ctor()
extern "C"  void ArrayList__ctor_m4254721275 (ArrayList_t2718874744 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Double XUPorterJSON.MiniJSON::parseNumber(System.Char[],System.Int32&)
extern "C"  double MiniJSON_parseNumber_m3876524415 (RuntimeObject * __this /* static, unused */, CharU5BU5D_t3528271667* ___json0, int32_t* ___index1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Hashtable XUPorterJSON.MiniJSON::parseObject(System.Char[],System.Int32&)
extern "C"  Hashtable_t1853889766 * MiniJSON_parseObject_m2380755468 (RuntimeObject * __this /* static, unused */, CharU5BU5D_t3528271667* ___json0, int32_t* ___index1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList XUPorterJSON.MiniJSON::parseArray(System.Char[],System.Int32&)
extern "C"  ArrayList_t2718874744 * MiniJSON_parseArray_m1439775387 (RuntimeObject * __this /* static, unused */, CharU5BU5D_t3528271667* ___json0, int32_t* ___index1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Boolean::Parse(System.String)
extern "C"  bool Boolean_Parse_m2370352694 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void XUPorterJSON.MiniJSON::eatWhitespace(System.Char[],System.Int32&)
extern "C"  void MiniJSON_eatWhitespace_m1328618482 (RuntimeObject * __this /* static, unused */, CharU5BU5D_t3528271667* ___json0, int32_t* ___index1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::Copy(System.Array,System.Int32,System.Array,System.Int32,System.Int32)
extern "C"  void Array_Copy_m344457298 (RuntimeObject * __this /* static, unused */, RuntimeArray * p0, int32_t p1, RuntimeArray * p2, int32_t p3, int32_t p4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::CreateString(System.Char[])
extern "C"  String_t* String_CreateString_m2818852475 (String_t* __this, CharU5BU5D_t3528271667* ___val0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String,System.String,System.String)
extern "C"  String_t* String_Concat_m2163913788 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, String_t* p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 XUPorterJSON.MiniJSON::getLastIndexOfNumber(System.Char[],System.Int32)
extern "C"  int32_t MiniJSON_getLastIndexOfNumber_m1276157804 (RuntimeObject * __this /* static, unused */, CharU5BU5D_t3528271667* ___json0, int32_t ___index1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Double System.Double::Parse(System.String)
extern "C"  double Double_Parse_m4153729520 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::IndexOf(System.Char)
extern "C"  int32_t String_IndexOf_m363431711 (String_t* __this, Il2CppChar p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean XUPorterJSON.MiniJSON::serializeObject(System.Collections.Hashtable,System.Text.StringBuilder)
extern "C"  bool MiniJSON_serializeObject_m716018166 (RuntimeObject * __this /* static, unused */, Hashtable_t1853889766 * ___anObject0, StringBuilder_t * ___builder1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean XUPorterJSON.MiniJSON::serializeArray(System.Collections.ArrayList,System.Text.StringBuilder)
extern "C"  bool MiniJSON_serializeArray_m710485201 (RuntimeObject * __this /* static, unused */, ArrayList_t2718874744 * ___anArray0, StringBuilder_t * ___builder1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.String)
extern "C"  StringBuilder_t * StringBuilder_Append_m1965104174 (StringBuilder_t * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void XUPorterJSON.MiniJSON::serializeString(System.String,System.Text.StringBuilder)
extern "C"  void MiniJSON_serializeString_m122238726 (RuntimeObject * __this /* static, unused */, String_t* ___aString0, StringBuilder_t * ___builder1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2<System.String,System.String>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m260861070(__this, method) ((  Enumerator_t3586889763  (*) (Dictionary_2_t1632706988 *, const RuntimeMethod*))Dictionary_2_GetEnumerator_m3278257048_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<!0,!1> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::get_Current()
#define Enumerator_get_Current_m2519371089(__this, method) ((  KeyValuePair_2_t4030379155  (*) (Enumerator_t3586889763 *, const RuntimeMethod*))Enumerator_get_Current_m2655181939_gshared)(__this, method)
// !0 System.Collections.Generic.KeyValuePair`2<System.String,System.String>::get_Key()
#define KeyValuePair_2_get_Key_m3980750512(__this, method) ((  String_t* (*) (KeyValuePair_2_t4030379155 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m1328507389_gshared)(__this, method)
// !1 System.Collections.Generic.KeyValuePair`2<System.String,System.String>::get_Value()
#define KeyValuePair_2_get_Value_m2243990694(__this, method) ((  String_t* (*) (KeyValuePair_2_t4030379155 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m3464904234_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::MoveNext()
#define Enumerator_MoveNext_m1175750522(__this, method) ((  bool (*) (Enumerator_t3586889763 *, const RuntimeMethod*))Enumerator_MoveNext_m1107569389_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::Dispose()
#define Enumerator_Dispose_m2198401511(__this, method) ((  void (*) (Enumerator_t3586889763 *, const RuntimeMethod*))Enumerator_Dispose_m3885012575_gshared)(__this, method)
// System.Type System.Object::GetType()
extern "C"  Type_t * Object_GetType_m88164663 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Type::get_IsArray()
extern "C"  bool Type_get_IsArray_m2591212821 (Type_t * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList::.ctor(System.Collections.ICollection)
extern "C"  void ArrayList__ctor_m2130986447 (ArrayList_t2718874744 * __this, RuntimeObject* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Convert::ToString(System.Char)
extern "C"  String_t* Convert_ToString_m1553911740 (RuntimeObject * __this /* static, unused */, Il2CppChar p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean XUPorterJSON.MiniJSON::serializeDictionary(System.Collections.Generic.Dictionary`2<System.String,System.String>,System.Text.StringBuilder)
extern "C"  bool MiniJSON_serializeDictionary_m2956593198 (RuntimeObject * __this /* static, unused */, Dictionary_2_t1632706988 * ___dict0, StringBuilder_t * ___builder1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Type::get_IsPrimitive()
extern "C"  bool Type_get_IsPrimitive_m1114712797 (Type_t * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Double System.Convert::ToDouble(System.Object)
extern "C"  double Convert_ToDouble_m4025515304 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void XUPorterJSON.MiniJSON::serializeNumber(System.Double,System.Text.StringBuilder)
extern "C"  void MiniJSON_serializeNumber_m3396057679 (RuntimeObject * __this /* static, unused */, double ___number0, StringBuilder_t * ___builder1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Convert::ToInt32(System.Char)
extern "C"  int32_t Convert_ToInt32_m1876369743 (RuntimeObject * __this /* static, unused */, Il2CppChar p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.Char)
extern "C"  StringBuilder_t * StringBuilder_Append_m2383614642 (StringBuilder_t * __this, Il2CppChar p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Convert::ToString(System.Int32,System.Int32)
extern "C"  String_t* Convert_ToString_m2142825503 (RuntimeObject * __this /* static, unused */, int32_t p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::PadLeft(System.Int32,System.Char)
extern "C"  String_t* String_PadLeft_m1263950263 (String_t* __this, int32_t p0, Il2CppChar p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Convert::ToString(System.Double)
extern "C"  String_t* Convert_ToString_m545017250 (RuntimeObject * __this /* static, unused */, double p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String XUPorterJSON.MiniJSON::jsonEncode(System.Object)
extern "C"  String_t* MiniJSON_jsonEncode_m1032995782 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___json0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEditor.XCodeEditor.XCConfigurationList::.ctor(System.String,UnityEditor.XCodeEditor.PBXDictionary)
extern "C"  void XCConfigurationList__ctor_m753058562 (XCConfigurationList_t1100530024 * __this, String_t* ___guid0, PBXDictionary_t2078602241 * ___dictionary1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___guid0;
		PBXDictionary_t2078602241 * L_1 = ___dictionary1;
		PBXObject__ctor_m664518219(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEditor.XCodeEditor.XCMod::.ctor(System.String)
extern "C"  void XCMod__ctor_m1865170813 (XCMod_t1014962890 * __this, String_t* ___filename0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCMod__ctor_m1865170813_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	FileInfo_t1169991790 * V_0 = NULL;
	String_t* V_1 = NULL;
	{
		Hashtable_t1853889766 * L_0 = (Hashtable_t1853889766 *)il2cpp_codegen_object_new(Hashtable_t1853889766_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1815022027(L_0, /*hidden argument*/NULL);
		__this->set__datastore_0(L_0);
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		String_t* L_1 = ___filename0;
		FileInfo_t1169991790 * L_2 = (FileInfo_t1169991790 *)il2cpp_codegen_object_new(FileInfo_t1169991790_il2cpp_TypeInfo_var);
		FileInfo__ctor_m3289795077(L_2, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		FileInfo_t1169991790 * L_3 = V_0;
		NullCheck(L_3);
		bool L_4 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean System.IO.FileSystemInfo::get_Exists() */, L_3);
		if (L_4)
		{
			goto IL_002d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3752629331(NULL /*static, unused*/, _stringLiteral1490807184, /*hidden argument*/NULL);
	}

IL_002d:
	{
		String_t* L_5 = ___filename0;
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1605229823_il2cpp_TypeInfo_var);
		String_t* L_6 = Path_GetFileNameWithoutExtension_m895535029(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		XCMod_set_name_m2740663738(__this, L_6, /*hidden argument*/NULL);
		String_t* L_7 = ___filename0;
		String_t* L_8 = Path_GetDirectoryName_m3496866581(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		XCMod_set_path_m633881003(__this, L_8, /*hidden argument*/NULL);
		FileInfo_t1169991790 * L_9 = V_0;
		NullCheck(L_9);
		StreamReader_t4009935899 * L_10 = FileInfo_OpenText_m40196162(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(11 /* System.String System.IO.TextReader::ReadToEnd() */, L_10);
		V_1 = L_11;
		String_t* L_12 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		String_t* L_13 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t211875846_il2cpp_TypeInfo_var);
		RuntimeObject * L_14 = MiniJSON_jsonDecode_m2285616255(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		__this->set__datastore_0(((Hashtable_t1853889766 *)CastclassClass((RuntimeObject*)L_14, Hashtable_t1853889766_il2cpp_TypeInfo_var)));
		Hashtable_t1853889766 * L_15 = __this->get__datastore_0();
		if (!L_15)
		{
			goto IL_0083;
		}
	}
	{
		Hashtable_t1853889766 * L_16 = __this->get__datastore_0();
		NullCheck(L_16);
		int32_t L_17 = VirtFuncInvoker0< int32_t >::Invoke(21 /* System.Int32 System.Collections.Hashtable::get_Count() */, L_16);
		if (L_17)
		{
			goto IL_00a4;
		}
	}

IL_0083:
	{
		String_t* L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		String_t* L_19 = ___filename0;
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1605229823_il2cpp_TypeInfo_var);
		String_t* L_20 = Path_GetFileName_m1354558116(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = String_Concat_m3755062657(NULL /*static, unused*/, _stringLiteral4240777088, L_20, _stringLiteral4073139529, /*hidden argument*/NULL);
		UnityException_t3598173660 * L_22 = (UnityException_t3598173660 *)il2cpp_codegen_object_new(UnityException_t3598173660_il2cpp_TypeInfo_var);
		UnityException__ctor_m872329880(L_22, L_21, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_22);
	}

IL_00a4:
	{
		return;
	}
}
// System.String UnityEditor.XCodeEditor.XCMod::get_name()
extern "C"  String_t* XCMod_get_name_m1342914417 (XCMod_t1014962890 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CnameU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void UnityEditor.XCodeEditor.XCMod::set_name(System.String)
extern "C"  void XCMod_set_name_m2740663738 (XCMod_t1014962890 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CnameU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String UnityEditor.XCodeEditor.XCMod::get_path()
extern "C"  String_t* XCMod_get_path_m530774122 (XCMod_t1014962890 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CpathU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void UnityEditor.XCodeEditor.XCMod::set_path(System.String)
extern "C"  void XCMod_set_path_m633881003 (XCMod_t1014962890 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CpathU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.String UnityEditor.XCodeEditor.XCMod::get_group()
extern "C"  String_t* XCMod_get_group_m1801882381 (XCMod_t1014962890 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCMod_get_group_m1801882381_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Hashtable_t1853889766 * L_0 = __this->get__datastore_0();
		if (!L_0)
		{
			goto IL_0036;
		}
	}
	{
		Hashtable_t1853889766 * L_1 = __this->get__datastore_0();
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(31 /* System.Boolean System.Collections.Hashtable::Contains(System.Object) */, L_1, _stringLiteral3122773422);
		if (!L_2)
		{
			goto IL_0036;
		}
	}
	{
		Hashtable_t1853889766 * L_3 = __this->get__datastore_0();
		NullCheck(L_3);
		RuntimeObject * L_4 = VirtFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(26 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_3, _stringLiteral3122773422);
		return ((String_t*)CastclassSealed((RuntimeObject*)L_4, String_t_il2cpp_TypeInfo_var));
	}

IL_0036:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		return L_5;
	}
}
// System.Collections.ArrayList UnityEditor.XCodeEditor.XCMod::get_patches()
extern "C"  ArrayList_t2718874744 * XCMod_get_patches_m909294950 (XCMod_t1014962890 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCMod_get_patches_m909294950_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Hashtable_t1853889766 * L_0 = __this->get__datastore_0();
		NullCheck(L_0);
		RuntimeObject * L_1 = VirtFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(26 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_0, _stringLiteral4026311239);
		return ((ArrayList_t2718874744 *)CastclassClass((RuntimeObject*)L_1, ArrayList_t2718874744_il2cpp_TypeInfo_var));
	}
}
// System.Collections.ArrayList UnityEditor.XCodeEditor.XCMod::get_libs()
extern "C"  ArrayList_t2718874744 * XCMod_get_libs_m513448774 (XCMod_t1014962890 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCMod_get_libs_m513448774_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	RuntimeObject* V_2 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		ArrayList_t2718874744 * L_0 = __this->get__libs_1();
		if (L_0)
		{
			goto IL_00a2;
		}
	}
	{
		Hashtable_t1853889766 * L_1 = __this->get__datastore_0();
		NullCheck(L_1);
		RuntimeObject * L_2 = VirtFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(26 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_1, _stringLiteral4263797636);
		NullCheck(((ArrayList_t2718874744 *)CastclassClass((RuntimeObject*)L_2, ArrayList_t2718874744_il2cpp_TypeInfo_var)));
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, ((ArrayList_t2718874744 *)CastclassClass((RuntimeObject*)L_2, ArrayList_t2718874744_il2cpp_TypeInfo_var)));
		ArrayList_t2718874744 * L_4 = (ArrayList_t2718874744 *)il2cpp_codegen_object_new(ArrayList_t2718874744_il2cpp_TypeInfo_var);
		ArrayList__ctor_m3828927650(L_4, L_3, /*hidden argument*/NULL);
		__this->set__libs_1(L_4);
		Hashtable_t1853889766 * L_5 = __this->get__datastore_0();
		NullCheck(L_5);
		RuntimeObject * L_6 = VirtFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(26 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_5, _stringLiteral4263797636);
		NullCheck(((ArrayList_t2718874744 *)CastclassClass((RuntimeObject*)L_6, ArrayList_t2718874744_il2cpp_TypeInfo_var)));
		RuntimeObject* L_7 = VirtFuncInvoker0< RuntimeObject* >::Invoke(43 /* System.Collections.IEnumerator System.Collections.ArrayList::GetEnumerator() */, ((ArrayList_t2718874744 *)CastclassClass((RuntimeObject*)L_6, ArrayList_t2718874744_il2cpp_TypeInfo_var)));
		V_1 = L_7;
	}

IL_004b:
	try
	{ // begin try (depth: 1)
		{
			goto IL_007e;
		}

IL_0050:
		{
			RuntimeObject* L_8 = V_1;
			NullCheck(L_8);
			RuntimeObject * L_9 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_8);
			V_0 = ((String_t*)CastclassSealed((RuntimeObject*)L_9, String_t_il2cpp_TypeInfo_var));
			String_t* L_10 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_11 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral186182294, L_10, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
			Debug_Log_m4051431634(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
			ArrayList_t2718874744 * L_12 = __this->get__libs_1();
			String_t* L_13 = V_0;
			XCModFile_t1016819465 * L_14 = (XCModFile_t1016819465 *)il2cpp_codegen_object_new(XCModFile_t1016819465_il2cpp_TypeInfo_var);
			XCModFile__ctor_m3435055427(L_14, L_13, /*hidden argument*/NULL);
			NullCheck(L_12);
			VirtFuncInvoker1< int32_t, RuntimeObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_12, L_14);
		}

IL_007e:
		{
			RuntimeObject* L_15 = V_1;
			NullCheck(L_15);
			bool L_16 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_15);
			if (L_16)
			{
				goto IL_0050;
			}
		}

IL_0089:
		{
			IL2CPP_LEAVE(0xA2, FINALLY_008e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_008e;
	}

FINALLY_008e:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_17 = V_1;
			RuntimeObject* L_18 = ((RuntimeObject*)IsInst((RuntimeObject*)L_17, IDisposable_t3640265483_il2cpp_TypeInfo_var));
			V_2 = L_18;
			if (!L_18)
			{
				goto IL_00a1;
			}
		}

IL_009b:
		{
			RuntimeObject* L_19 = V_2;
			NullCheck(L_19);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, L_19);
		}

IL_00a1:
		{
			IL2CPP_END_FINALLY(142)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(142)
	{
		IL2CPP_JUMP_TBL(0xA2, IL_00a2)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_00a2:
	{
		ArrayList_t2718874744 * L_20 = __this->get__libs_1();
		return L_20;
	}
}
// System.Collections.ArrayList UnityEditor.XCodeEditor.XCMod::get_frameworks()
extern "C"  ArrayList_t2718874744 * XCMod_get_frameworks_m2568364667 (XCMod_t1014962890 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCMod_get_frameworks_m2568364667_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Hashtable_t1853889766 * L_0 = __this->get__datastore_0();
		NullCheck(L_0);
		RuntimeObject * L_1 = VirtFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(26 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_0, _stringLiteral2253447914);
		return ((ArrayList_t2718874744 *)CastclassClass((RuntimeObject*)L_1, ArrayList_t2718874744_il2cpp_TypeInfo_var));
	}
}
// System.Collections.ArrayList UnityEditor.XCodeEditor.XCMod::get_headerpaths()
extern "C"  ArrayList_t2718874744 * XCMod_get_headerpaths_m343671542 (XCMod_t1014962890 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCMod_get_headerpaths_m343671542_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Hashtable_t1853889766 * L_0 = __this->get__datastore_0();
		NullCheck(L_0);
		RuntimeObject * L_1 = VirtFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(26 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_0, _stringLiteral3213188363);
		return ((ArrayList_t2718874744 *)CastclassClass((RuntimeObject*)L_1, ArrayList_t2718874744_il2cpp_TypeInfo_var));
	}
}
// System.Collections.ArrayList UnityEditor.XCodeEditor.XCMod::get_files()
extern "C"  ArrayList_t2718874744 * XCMod_get_files_m150690371 (XCMod_t1014962890 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCMod_get_files_m150690371_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Hashtable_t1853889766 * L_0 = __this->get__datastore_0();
		NullCheck(L_0);
		RuntimeObject * L_1 = VirtFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(26 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_0, _stringLiteral452222907);
		return ((ArrayList_t2718874744 *)CastclassClass((RuntimeObject*)L_1, ArrayList_t2718874744_il2cpp_TypeInfo_var));
	}
}
// System.Collections.ArrayList UnityEditor.XCodeEditor.XCMod::get_folders()
extern "C"  ArrayList_t2718874744 * XCMod_get_folders_m1874296509 (XCMod_t1014962890 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCMod_get_folders_m1874296509_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Hashtable_t1853889766 * L_0 = __this->get__datastore_0();
		NullCheck(L_0);
		RuntimeObject * L_1 = VirtFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(26 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_0, _stringLiteral3025985292);
		return ((ArrayList_t2718874744 *)CastclassClass((RuntimeObject*)L_1, ArrayList_t2718874744_il2cpp_TypeInfo_var));
	}
}
// System.Collections.ArrayList UnityEditor.XCodeEditor.XCMod::get_excludes()
extern "C"  ArrayList_t2718874744 * XCMod_get_excludes_m1351011736 (XCMod_t1014962890 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCMod_get_excludes_m1351011736_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Hashtable_t1853889766 * L_0 = __this->get__datastore_0();
		NullCheck(L_0);
		RuntimeObject * L_1 = VirtFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(26 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_0, _stringLiteral2596834824);
		return ((ArrayList_t2718874744 *)CastclassClass((RuntimeObject*)L_1, ArrayList_t2718874744_il2cpp_TypeInfo_var));
	}
}
// System.Collections.ArrayList UnityEditor.XCodeEditor.XCMod::get_compiler_flags()
extern "C"  ArrayList_t2718874744 * XCMod_get_compiler_flags_m333845873 (XCMod_t1014962890 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCMod_get_compiler_flags_m333845873_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Hashtable_t1853889766 * L_0 = __this->get__datastore_0();
		NullCheck(L_0);
		RuntimeObject * L_1 = VirtFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(26 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_0, _stringLiteral559529979);
		return ((ArrayList_t2718874744 *)CastclassClass((RuntimeObject*)L_1, ArrayList_t2718874744_il2cpp_TypeInfo_var));
	}
}
// System.Collections.ArrayList UnityEditor.XCodeEditor.XCMod::get_linker_flags()
extern "C"  ArrayList_t2718874744 * XCMod_get_linker_flags_m3777781269 (XCMod_t1014962890 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCMod_get_linker_flags_m3777781269_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Hashtable_t1853889766 * L_0 = __this->get__datastore_0();
		NullCheck(L_0);
		RuntimeObject * L_1 = VirtFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(26 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_0, _stringLiteral3860215823);
		return ((ArrayList_t2718874744 *)CastclassClass((RuntimeObject*)L_1, ArrayList_t2718874744_il2cpp_TypeInfo_var));
	}
}
// System.Collections.ArrayList UnityEditor.XCodeEditor.XCMod::get_embed_binaries()
extern "C"  ArrayList_t2718874744 * XCMod_get_embed_binaries_m3193459801 (XCMod_t1014962890 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCMod_get_embed_binaries_m3193459801_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Hashtable_t1853889766 * L_0 = __this->get__datastore_0();
		NullCheck(L_0);
		RuntimeObject * L_1 = VirtFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(26 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_0, _stringLiteral377231163);
		return ((ArrayList_t2718874744 *)CastclassClass((RuntimeObject*)L_1, ArrayList_t2718874744_il2cpp_TypeInfo_var));
	}
}
// System.Collections.Hashtable UnityEditor.XCodeEditor.XCMod::get_plist()
extern "C"  Hashtable_t1853889766 * XCMod_get_plist_m1661036855 (XCMod_t1014962890 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCMod_get_plist_m1661036855_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Hashtable_t1853889766 * L_0 = __this->get__datastore_0();
		NullCheck(L_0);
		RuntimeObject * L_1 = VirtFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(26 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_0, _stringLiteral4290053407);
		return ((Hashtable_t1853889766 *)CastclassClass((RuntimeObject*)L_1, Hashtable_t1853889766_il2cpp_TypeInfo_var));
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEditor.XCodeEditor.XCModFile::.ctor(System.String)
extern "C"  void XCModFile__ctor_m3435055427 (XCModFile_t1016819465 * __this, String_t* ___inputString0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCModFile__ctor_m3435055427_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringU5BU5D_t1281789340* V_0 = NULL;
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		XCModFile_set_isWeak_m2297174501(__this, (bool)0, /*hidden argument*/NULL);
		String_t* L_0 = ___inputString0;
		NullCheck(L_0);
		bool L_1 = String_Contains_m1147431944(L_0, _stringLiteral3452614550, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0053;
		}
	}
	{
		String_t* L_2 = ___inputString0;
		CharU5BU5D_t3528271667* L_3 = ((CharU5BU5D_t3528271667*)SZArrayNew(CharU5BU5D_t3528271667_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)58));
		NullCheck(L_2);
		StringU5BU5D_t1281789340* L_4 = String_Split_m3646115398(L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		StringU5BU5D_t1281789340* L_5 = V_0;
		NullCheck(L_5);
		int32_t L_6 = 0;
		String_t* L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		XCModFile_set_filePath_m1367883274(__this, L_7, /*hidden argument*/NULL);
		StringU5BU5D_t1281789340* L_8 = V_0;
		NullCheck(L_8);
		int32_t L_9 = 1;
		String_t* L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck(L_10);
		int32_t L_11 = String_CompareTo_m3414379165(L_10, _stringLiteral4779758, /*hidden argument*/NULL);
		XCModFile_set_isWeak_m2297174501(__this, (bool)((((int32_t)L_11) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		goto IL_005a;
	}

IL_0053:
	{
		String_t* L_12 = ___inputString0;
		XCModFile_set_filePath_m1367883274(__this, L_12, /*hidden argument*/NULL);
	}

IL_005a:
	{
		return;
	}
}
// System.String UnityEditor.XCodeEditor.XCModFile::get_filePath()
extern "C"  String_t* XCModFile_get_filePath_m2182538701 (XCModFile_t1016819465 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CfilePathU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void UnityEditor.XCodeEditor.XCModFile::set_filePath(System.String)
extern "C"  void XCModFile_set_filePath_m1367883274 (XCModFile_t1016819465 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CfilePathU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Boolean UnityEditor.XCodeEditor.XCModFile::get_isWeak()
extern "C"  bool XCModFile_get_isWeak_m3536481063 (XCModFile_t1016819465 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_U3CisWeakU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void UnityEditor.XCodeEditor.XCModFile::set_isWeak(System.Boolean)
extern "C"  void XCModFile_set_isWeak_m2297174501 (XCModFile_t1016819465 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CisWeakU3Ek__BackingField_1(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEditor.XCodeEditor.XCPlist::.ctor(System.String)
extern "C"  void XCPlist__ctor_m1809109823 (XCPlist_t2540119850 * __this, String_t* ___plistPath0, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___plistPath0;
		__this->set_plistPath_0(L_0);
		return;
	}
}
// System.Void UnityEditor.XCodeEditor.XCPlist::Process(System.Collections.Hashtable)
extern "C"  void XCPlist_Process_m1182457030 (XCPlist_t2540119850 * __this, Hashtable_t1853889766 * ___plist0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCPlist_Process_m1182457030_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t2865362463 * V_0 = NULL;
	DictionaryEntry_t3123975638  V_1;
	memset(&V_1, 0, sizeof(V_1));
	RuntimeObject* V_2 = NULL;
	RuntimeObject* V_3 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = __this->get_plistPath_0();
		IL2CPP_RUNTIME_CLASS_INIT(Plist_t3259848363_il2cpp_TypeInfo_var);
		RuntimeObject * L_1 = Plist_readPlist_m3299688712(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = ((Dictionary_2_t2865362463 *)CastclassClass((RuntimeObject*)L_1, Dictionary_2_t2865362463_il2cpp_TypeInfo_var));
		Hashtable_t1853889766 * L_2 = ___plist0;
		NullCheck(L_2);
		RuntimeObject* L_3 = VirtFuncInvoker0< RuntimeObject* >::Invoke(32 /* System.Collections.IDictionaryEnumerator System.Collections.Hashtable::GetEnumerator() */, L_2);
		V_2 = L_3;
	}

IL_0018:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0043;
		}

IL_001d:
		{
			RuntimeObject* L_4 = V_2;
			NullCheck(L_4);
			RuntimeObject * L_5 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_4);
			V_1 = ((*(DictionaryEntry_t3123975638 *)((DictionaryEntry_t3123975638 *)UnBox(L_5, DictionaryEntry_t3123975638_il2cpp_TypeInfo_var))));
			RuntimeObject * L_6 = DictionaryEntry_get_Key_m3117378551((&V_1), /*hidden argument*/NULL);
			RuntimeObject * L_7 = DictionaryEntry_get_Value_m618120527((&V_1), /*hidden argument*/NULL);
			Dictionary_2_t2865362463 * L_8 = V_0;
			XCPlist_AddPlistItems_m4004335766(__this, ((String_t*)CastclassSealed((RuntimeObject*)L_6, String_t_il2cpp_TypeInfo_var)), L_7, L_8, /*hidden argument*/NULL);
		}

IL_0043:
		{
			RuntimeObject* L_9 = V_2;
			NullCheck(L_9);
			bool L_10 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_9);
			if (L_10)
			{
				goto IL_001d;
			}
		}

IL_004e:
		{
			IL2CPP_LEAVE(0x67, FINALLY_0053);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0053;
	}

FINALLY_0053:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_11 = V_2;
			RuntimeObject* L_12 = ((RuntimeObject*)IsInst((RuntimeObject*)L_11, IDisposable_t3640265483_il2cpp_TypeInfo_var));
			V_3 = L_12;
			if (!L_12)
			{
				goto IL_0066;
			}
		}

IL_0060:
		{
			RuntimeObject* L_13 = V_3;
			NullCheck(L_13);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, L_13);
		}

IL_0066:
		{
			IL2CPP_END_FINALLY(83)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(83)
	{
		IL2CPP_JUMP_TBL(0x67, IL_0067)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0067:
	{
		bool L_14 = __this->get_plistModified_1();
		if (!L_14)
		{
			goto IL_007e;
		}
	}
	{
		Dictionary_2_t2865362463 * L_15 = V_0;
		String_t* L_16 = __this->get_plistPath_0();
		IL2CPP_RUNTIME_CLASS_INIT(Plist_t3259848363_il2cpp_TypeInfo_var);
		Plist_writeXml_m1164383522(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
	}

IL_007e:
	{
		return;
	}
}
// System.Void UnityEditor.XCodeEditor.XCPlist::AddPlistItems(System.String,System.Object,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void XCPlist_AddPlistItems_m4004335766 (XCPlist_t2540119850 * __this, String_t* ___key0, RuntimeObject * ___value1, Dictionary_2_t2865362463 * ___dict2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCPlist_AddPlistItems_m4004335766_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___key0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral3386429785, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___key0;
		NullCheck(L_2);
		int32_t L_3 = String_CompareTo_m3414379165(L_2, _stringLiteral472138309, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0032;
		}
	}
	{
		RuntimeObject * L_4 = ___value1;
		Dictionary_2_t2865362463 * L_5 = ___dict2;
		XCPlist_processUrlTypes_m15864857(__this, ((ArrayList_t2718874744 *)CastclassClass((RuntimeObject*)L_4, ArrayList_t2718874744_il2cpp_TypeInfo_var)), L_5, /*hidden argument*/NULL);
		goto IL_004b;
	}

IL_0032:
	{
		Dictionary_2_t2865362463 * L_6 = ___dict2;
		String_t* L_7 = ___key0;
		RuntimeObject * L_8 = ___value1;
		Dictionary_2_t2865362463 * L_9 = XCPlist_HashtableToDictionary_TisString_t_TisRuntimeObject_m3428346691(NULL /*static, unused*/, ((Hashtable_t1853889766 *)CastclassClass((RuntimeObject*)L_8, Hashtable_t1853889766_il2cpp_TypeInfo_var)), /*hidden argument*/XCPlist_HashtableToDictionary_TisString_t_TisRuntimeObject_m3428346691_RuntimeMethod_var);
		NullCheck(L_6);
		Dictionary_2_set_Item_m2329160973(L_6, L_7, L_9, /*hidden argument*/Dictionary_2_set_Item_m2329160973_RuntimeMethod_var);
		__this->set_plistModified_1((bool)1);
	}

IL_004b:
	{
		return;
	}
}
// System.Void UnityEditor.XCodeEditor.XCPlist::processUrlTypes(System.Collections.ArrayList,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void XCPlist_processUrlTypes_m15864857 (XCPlist_t2540119850 * __this, ArrayList_t2718874744 * ___urltypes0, Dictionary_2_t2865362463 * ___dict1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCPlist_processUrlTypes_m15864857_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t257213610 * V_0 = NULL;
	Hashtable_t1853889766 * V_1 = NULL;
	RuntimeObject* V_2 = NULL;
	String_t* V_3 = NULL;
	String_t* V_4 = NULL;
	ArrayList_t2718874744 * V_5 = NULL;
	List_1_t257213610 * V_6 = NULL;
	String_t* V_7 = NULL;
	RuntimeObject* V_8 = NULL;
	RuntimeObject* V_9 = NULL;
	Dictionary_2_t2865362463 * V_10 = NULL;
	RuntimeObject* V_11 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t2865362463 * L_0 = ___dict1;
		NullCheck(L_0);
		bool L_1 = Dictionary_2_ContainsKey_m1520683221(L_0, _stringLiteral2500111239, /*hidden argument*/Dictionary_2_ContainsKey_m1520683221_RuntimeMethod_var);
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		Dictionary_2_t2865362463 * L_2 = ___dict1;
		NullCheck(L_2);
		RuntimeObject * L_3 = Dictionary_2_get_Item_m3179620279(L_2, _stringLiteral2500111239, /*hidden argument*/Dictionary_2_get_Item_m3179620279_RuntimeMethod_var);
		V_0 = ((List_1_t257213610 *)CastclassClass((RuntimeObject*)L_3, List_1_t257213610_il2cpp_TypeInfo_var));
		goto IL_002c;
	}

IL_0026:
	{
		List_1_t257213610 * L_4 = (List_1_t257213610 *)il2cpp_codegen_object_new(List_1_t257213610_il2cpp_TypeInfo_var);
		List_1__ctor_m2321703786(L_4, /*hidden argument*/List_1__ctor_m2321703786_RuntimeMethod_var);
		V_0 = L_4;
	}

IL_002c:
	{
		ArrayList_t2718874744 * L_5 = ___urltypes0;
		NullCheck(L_5);
		RuntimeObject* L_6 = VirtFuncInvoker0< RuntimeObject* >::Invoke(43 /* System.Collections.IEnumerator System.Collections.ArrayList::GetEnumerator() */, L_5);
		V_2 = L_6;
	}

IL_0033:
	try
	{ // begin try (depth: 1)
		{
			goto IL_014f;
		}

IL_0038:
		{
			RuntimeObject* L_7 = V_2;
			NullCheck(L_7);
			RuntimeObject * L_8 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_7);
			V_1 = ((Hashtable_t1853889766 *)CastclassClass((RuntimeObject*)L_8, Hashtable_t1853889766_il2cpp_TypeInfo_var));
			Hashtable_t1853889766 * L_9 = V_1;
			NullCheck(L_9);
			RuntimeObject * L_10 = VirtFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(26 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_9, _stringLiteral1628940268);
			V_3 = ((String_t*)CastclassSealed((RuntimeObject*)L_10, String_t_il2cpp_TypeInfo_var));
			String_t* L_11 = V_3;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_12 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
			if (!L_12)
			{
				goto IL_0066;
			}
		}

IL_0060:
		{
			V_3 = _stringLiteral1935017661;
		}

IL_0066:
		{
			Hashtable_t1853889766 * L_13 = V_1;
			NullCheck(L_13);
			RuntimeObject * L_14 = VirtFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(26 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_13, _stringLiteral62725243);
			V_4 = ((String_t*)CastclassSealed((RuntimeObject*)L_14, String_t_il2cpp_TypeInfo_var));
			Hashtable_t1853889766 * L_15 = V_1;
			NullCheck(L_15);
			RuntimeObject * L_16 = VirtFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(26 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_15, _stringLiteral3717251477);
			V_5 = ((ArrayList_t2718874744 *)CastclassClass((RuntimeObject*)L_16, ArrayList_t2718874744_il2cpp_TypeInfo_var));
			List_1_t257213610 * L_17 = (List_1_t257213610 *)il2cpp_codegen_object_new(List_1_t257213610_il2cpp_TypeInfo_var);
			List_1__ctor_m2321703786(L_17, /*hidden argument*/List_1__ctor_m2321703786_RuntimeMethod_var);
			V_6 = L_17;
			ArrayList_t2718874744 * L_18 = V_5;
			NullCheck(L_18);
			RuntimeObject* L_19 = VirtFuncInvoker0< RuntimeObject* >::Invoke(43 /* System.Collections.IEnumerator System.Collections.ArrayList::GetEnumerator() */, L_18);
			V_8 = L_19;
		}

IL_009a:
		try
		{ // begin try (depth: 2)
			{
				goto IL_00b6;
			}

IL_009f:
			{
				RuntimeObject* L_20 = V_8;
				NullCheck(L_20);
				RuntimeObject * L_21 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_20);
				V_7 = ((String_t*)CastclassSealed((RuntimeObject*)L_21, String_t_il2cpp_TypeInfo_var));
				List_1_t257213610 * L_22 = V_6;
				String_t* L_23 = V_7;
				NullCheck(L_22);
				List_1_Add_m3338814081(L_22, L_23, /*hidden argument*/List_1_Add_m3338814081_RuntimeMethod_var);
			}

IL_00b6:
			{
				RuntimeObject* L_24 = V_8;
				NullCheck(L_24);
				bool L_25 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_24);
				if (L_25)
				{
					goto IL_009f;
				}
			}

IL_00c2:
			{
				IL2CPP_LEAVE(0xDE, FINALLY_00c7);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t *)e.ex;
			goto FINALLY_00c7;
		}

FINALLY_00c7:
		{ // begin finally (depth: 2)
			{
				RuntimeObject* L_26 = V_8;
				RuntimeObject* L_27 = ((RuntimeObject*)IsInst((RuntimeObject*)L_26, IDisposable_t3640265483_il2cpp_TypeInfo_var));
				V_9 = L_27;
				if (!L_27)
				{
					goto IL_00dd;
				}
			}

IL_00d6:
			{
				RuntimeObject* L_28 = V_9;
				NullCheck(L_28);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, L_28);
			}

IL_00dd:
			{
				IL2CPP_END_FINALLY(199)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(199)
		{
			IL2CPP_JUMP_TBL(0xDE, IL_00de)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		}

IL_00de:
		{
			List_1_t257213610 * L_29 = V_0;
			String_t* L_30 = V_4;
			Dictionary_2_t2865362463 * L_31 = XCPlist_findUrlTypeByName_m4027452897(__this, L_29, L_30, /*hidden argument*/NULL);
			V_10 = L_31;
			Dictionary_2_t2865362463 * L_32 = V_10;
			if (L_32)
			{
				goto IL_012d;
			}
		}

IL_00f0:
		{
			Dictionary_2_t2865362463 * L_33 = (Dictionary_2_t2865362463 *)il2cpp_codegen_object_new(Dictionary_2_t2865362463_il2cpp_TypeInfo_var);
			Dictionary_2__ctor_m3962145734(L_33, /*hidden argument*/Dictionary_2__ctor_m3962145734_RuntimeMethod_var);
			V_10 = L_33;
			Dictionary_2_t2865362463 * L_34 = V_10;
			String_t* L_35 = V_3;
			NullCheck(L_34);
			Dictionary_2_set_Item_m2329160973(L_34, _stringLiteral2703192964, L_35, /*hidden argument*/Dictionary_2_set_Item_m2329160973_RuntimeMethod_var);
			Dictionary_2_t2865362463 * L_36 = V_10;
			String_t* L_37 = V_4;
			NullCheck(L_36);
			Dictionary_2_set_Item_m2329160973(L_36, _stringLiteral1764797311, L_37, /*hidden argument*/Dictionary_2_set_Item_m2329160973_RuntimeMethod_var);
			Dictionary_2_t2865362463 * L_38 = V_10;
			List_1_t257213610 * L_39 = V_6;
			NullCheck(L_38);
			Dictionary_2_set_Item_m2329160973(L_38, _stringLiteral166333785, L_39, /*hidden argument*/Dictionary_2_set_Item_m2329160973_RuntimeMethod_var);
			List_1_t257213610 * L_40 = V_0;
			Dictionary_2_t2865362463 * L_41 = V_10;
			NullCheck(L_40);
			List_1_Add_m3338814081(L_40, L_41, /*hidden argument*/List_1_Add_m3338814081_RuntimeMethod_var);
			goto IL_0148;
		}

IL_012d:
		{
			Dictionary_2_t2865362463 * L_42 = V_10;
			String_t* L_43 = V_3;
			NullCheck(L_42);
			Dictionary_2_set_Item_m2329160973(L_42, _stringLiteral2703192964, L_43, /*hidden argument*/Dictionary_2_set_Item_m2329160973_RuntimeMethod_var);
			Dictionary_2_t2865362463 * L_44 = V_10;
			List_1_t257213610 * L_45 = V_6;
			NullCheck(L_44);
			Dictionary_2_set_Item_m2329160973(L_44, _stringLiteral166333785, L_45, /*hidden argument*/Dictionary_2_set_Item_m2329160973_RuntimeMethod_var);
		}

IL_0148:
		{
			__this->set_plistModified_1((bool)1);
		}

IL_014f:
		{
			RuntimeObject* L_46 = V_2;
			NullCheck(L_46);
			bool L_47 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_46);
			if (L_47)
			{
				goto IL_0038;
			}
		}

IL_015a:
		{
			IL2CPP_LEAVE(0x175, FINALLY_015f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_015f;
	}

FINALLY_015f:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_48 = V_2;
			RuntimeObject* L_49 = ((RuntimeObject*)IsInst((RuntimeObject*)L_48, IDisposable_t3640265483_il2cpp_TypeInfo_var));
			V_11 = L_49;
			if (!L_49)
			{
				goto IL_0174;
			}
		}

IL_016d:
		{
			RuntimeObject* L_50 = V_11;
			NullCheck(L_50);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, L_50);
		}

IL_0174:
		{
			IL2CPP_END_FINALLY(351)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(351)
	{
		IL2CPP_JUMP_TBL(0x175, IL_0175)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0175:
	{
		Dictionary_2_t2865362463 * L_51 = ___dict1;
		List_1_t257213610 * L_52 = V_0;
		NullCheck(L_51);
		Dictionary_2_set_Item_m2329160973(L_51, _stringLiteral2500111239, L_52, /*hidden argument*/Dictionary_2_set_Item_m2329160973_RuntimeMethod_var);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEditor.XCodeEditor.XCPlist::findUrlTypeByName(System.Collections.Generic.List`1<System.Object>,System.String)
extern "C"  Dictionary_2_t2865362463 * XCPlist_findUrlTypeByName_m4027452897 (XCPlist_t2540119850 * __this, List_1_t257213610 * ___bundleUrlTypes0, String_t* ___name1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCPlist_findUrlTypeByName_m4027452897_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t2865362463 * V_0 = NULL;
	Enumerator_t2146457487  V_1;
	memset(&V_1, 0, sizeof(V_1));
	String_t* V_2 = NULL;
	Dictionary_2_t2865362463 * V_3 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t257213610 * L_0 = ___bundleUrlTypes0;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		List_1_t257213610 * L_1 = ___bundleUrlTypes0;
		NullCheck(L_1);
		int32_t L_2 = List_1_get_Count_m2934127733(L_1, /*hidden argument*/List_1_get_Count_m2934127733_RuntimeMethod_var);
		if (L_2)
		{
			goto IL_0013;
		}
	}

IL_0011:
	{
		return (Dictionary_2_t2865362463 *)NULL;
	}

IL_0013:
	{
		List_1_t257213610 * L_3 = ___bundleUrlTypes0;
		NullCheck(L_3);
		Enumerator_t2146457487  L_4 = List_1_GetEnumerator_m2930774921(L_3, /*hidden argument*/List_1_GetEnumerator_m2930774921_RuntimeMethod_var);
		V_1 = L_4;
	}

IL_001a:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0050;
		}

IL_001f:
		{
			RuntimeObject * L_5 = Enumerator_get_Current_m470245444((&V_1), /*hidden argument*/Enumerator_get_Current_m470245444_RuntimeMethod_var);
			V_0 = ((Dictionary_2_t2865362463 *)CastclassClass((RuntimeObject*)L_5, Dictionary_2_t2865362463_il2cpp_TypeInfo_var));
			Dictionary_2_t2865362463 * L_6 = V_0;
			NullCheck(L_6);
			RuntimeObject * L_7 = Dictionary_2_get_Item_m3179620279(L_6, _stringLiteral1764797311, /*hidden argument*/Dictionary_2_get_Item_m3179620279_RuntimeMethod_var);
			V_2 = ((String_t*)CastclassSealed((RuntimeObject*)L_7, String_t_il2cpp_TypeInfo_var));
			String_t* L_8 = V_2;
			String_t* L_9 = ___name1;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			int32_t L_10 = String_Compare_m3735043349(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
			if (L_10)
			{
				goto IL_0050;
			}
		}

IL_0049:
		{
			Dictionary_2_t2865362463 * L_11 = V_0;
			V_3 = L_11;
			IL2CPP_LEAVE(0x71, FINALLY_0061);
		}

IL_0050:
		{
			bool L_12 = Enumerator_MoveNext_m2142368520((&V_1), /*hidden argument*/Enumerator_MoveNext_m2142368520_RuntimeMethod_var);
			if (L_12)
			{
				goto IL_001f;
			}
		}

IL_005c:
		{
			IL2CPP_LEAVE(0x6F, FINALLY_0061);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0061;
	}

FINALLY_0061:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3007748546((&V_1), /*hidden argument*/Enumerator_Dispose_m3007748546_RuntimeMethod_var);
		IL2CPP_END_FINALLY(97)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(97)
	{
		IL2CPP_JUMP_TBL(0x71, IL_0071)
		IL2CPP_JUMP_TBL(0x6F, IL_006f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_006f:
	{
		return (Dictionary_2_t2865362463 *)NULL;
	}

IL_0071:
	{
		Dictionary_2_t2865362463 * L_13 = V_3;
		return L_13;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEditor.XCodeEditor.XCProject::.ctor()
extern "C"  void XCProject__ctor_m2980377193 (XCProject_t3157646134 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEditor.XCodeEditor.XCProject::.ctor(System.String)
extern "C"  void XCProject__ctor_m4217487726 (XCProject_t3157646134 * __this, String_t* ___filePath0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCProject__ctor_m4217487726_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringU5BU5D_t1281789340* V_0 = NULL;
	String_t* V_1 = NULL;
	PBXParser_t1614426892 * V_2 = NULL;
	{
		XCProject__ctor_m2980377193(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___filePath0;
		bool L_1 = Directory_Exists_m1484791558(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0022;
		}
	}
	{
		String_t* L_2 = ___filePath0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral3589804870, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3752629331(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}

IL_0022:
	{
		String_t* L_4 = ___filePath0;
		NullCheck(L_4);
		bool L_5 = String_EndsWith_m1901926500(L_4, _stringLiteral1225324344, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_005a;
		}
	}
	{
		String_t* L_6 = ___filePath0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral2256741245, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		String_t* L_8 = ___filePath0;
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1605229823_il2cpp_TypeInfo_var);
		String_t* L_9 = Path_GetDirectoryName_m3496866581(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		XCProject_set_projectRootPath_m4200784150(__this, L_9, /*hidden argument*/NULL);
		String_t* L_10 = ___filePath0;
		XCProject_set_filePath_m1792948959(__this, L_10, /*hidden argument*/NULL);
		goto IL_00be;
	}

IL_005a:
	{
		String_t* L_11 = ___filePath0;
		StringU5BU5D_t1281789340* L_12 = Directory_GetDirectories_m4129725300(NULL /*static, unused*/, L_11, _stringLiteral2300800221, /*hidden argument*/NULL);
		V_0 = L_12;
		StringU5BU5D_t1281789340* L_13 = V_0;
		NullCheck(L_13);
		if ((((int32_t)((int32_t)(((RuntimeArray *)L_13)->max_length)))))
		{
			goto IL_0079;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3752629331(NULL /*static, unused*/, _stringLiteral3121128525, /*hidden argument*/NULL);
		return;
	}

IL_0079:
	{
		String_t* L_14 = ___filePath0;
		XCProject_set_projectRootPath_m4200784150(__this, L_14, /*hidden argument*/NULL);
		String_t* L_15 = XCProject_get_projectRootPath_m3623540506(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1605229823_il2cpp_TypeInfo_var);
		bool L_16 = Path_IsPathRooted_m3515805419(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		if (L_16)
		{
			goto IL_00b5;
		}
	}
	{
		String_t* L_17 = Application_get_dataPath_m4232621142(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		NullCheck(L_17);
		String_t* L_19 = String_Replace_m1273907647(L_17, _stringLiteral1669614476, L_18, /*hidden argument*/NULL);
		String_t* L_20 = XCProject_get_projectRootPath_m3623540506(__this, /*hidden argument*/NULL);
		String_t* L_21 = String_Concat_m3937257545(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
		XCProject_set_projectRootPath_m4200784150(__this, L_21, /*hidden argument*/NULL);
	}

IL_00b5:
	{
		StringU5BU5D_t1281789340* L_22 = V_0;
		NullCheck(L_22);
		int32_t L_23 = 0;
		String_t* L_24 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		XCProject_set_filePath_m1792948959(__this, L_24, /*hidden argument*/NULL);
	}

IL_00be:
	{
		String_t* L_25 = XCProject_get_filePath_m4061641646(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1605229823_il2cpp_TypeInfo_var);
		String_t* L_26 = Path_Combine_m3389272516(NULL /*static, unused*/, L_25, _stringLiteral2698730106, /*hidden argument*/NULL);
		FileInfo_t1169991790 * L_27 = (FileInfo_t1169991790 *)il2cpp_codegen_object_new(FileInfo_t1169991790_il2cpp_TypeInfo_var);
		FileInfo__ctor_m3289795077(L_27, L_26, /*hidden argument*/NULL);
		__this->set_projectFileInfo_5(L_27);
		FileInfo_t1169991790 * L_28 = __this->get_projectFileInfo_5();
		NullCheck(L_28);
		StreamReader_t4009935899 * L_29 = FileInfo_OpenText_m40196162(L_28, /*hidden argument*/NULL);
		NullCheck(L_29);
		String_t* L_30 = VirtFuncInvoker0< String_t* >::Invoke(11 /* System.String System.IO.TextReader::ReadToEnd() */, L_29);
		V_1 = L_30;
		PBXParser_t1614426892 * L_31 = (PBXParser_t1614426892 *)il2cpp_codegen_object_new(PBXParser_t1614426892_il2cpp_TypeInfo_var);
		PBXParser__ctor_m3197616242(L_31, /*hidden argument*/NULL);
		V_2 = L_31;
		PBXParser_t1614426892 * L_32 = V_2;
		String_t* L_33 = V_1;
		NullCheck(L_32);
		PBXDictionary_t2078602241 * L_34 = PBXParser_Decode_m2851013094(L_32, L_33, /*hidden argument*/NULL);
		__this->set__datastore_0(L_34);
		PBXDictionary_t2078602241 * L_35 = __this->get__datastore_0();
		if (L_35)
		{
			goto IL_0119;
		}
	}
	{
		String_t* L_36 = ___filePath0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_37 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral2770612856, L_36, /*hidden argument*/NULL);
		Exception_t * L_38 = (Exception_t *)il2cpp_codegen_object_new(Exception_t_il2cpp_TypeInfo_var);
		Exception__ctor_m1152696503(L_38, L_37, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_38);
	}

IL_0119:
	{
		PBXDictionary_t2078602241 * L_39 = __this->get__datastore_0();
		NullCheck(L_39);
		bool L_40 = Dictionary_2_ContainsKey_m1520683221(L_39, _stringLiteral2866737197, /*hidden argument*/Dictionary_2_ContainsKey_m1520683221_RuntimeMethod_var);
		if (L_40)
		{
			goto IL_014e;
		}
	}
	{
		PBXDictionary_t2078602241 * L_41 = __this->get__datastore_0();
		NullCheck(L_41);
		int32_t L_42 = Dictionary_2_get_Count_m2664023503(L_41, /*hidden argument*/Dictionary_2_get_Count_m2664023503_RuntimeMethod_var);
		int32_t L_43 = L_42;
		RuntimeObject * L_44 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_43);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_45 = String_Concat_m904156431(NULL /*static, unused*/, _stringLiteral1506900225, L_44, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_45, /*hidden argument*/NULL);
		return;
	}

IL_014e:
	{
		PBXDictionary_t2078602241 * L_46 = __this->get__datastore_0();
		NullCheck(L_46);
		RuntimeObject * L_47 = Dictionary_2_get_Item_m3179620279(L_46, _stringLiteral2866737197, /*hidden argument*/Dictionary_2_get_Item_m3179620279_RuntimeMethod_var);
		__this->set__objects_1(((PBXDictionary_t2078602241 *)CastclassClass((RuntimeObject*)L_47, PBXDictionary_t2078602241_il2cpp_TypeInfo_var)));
		__this->set_modified_7((bool)0);
		PBXDictionary_t2078602241 * L_48 = __this->get__datastore_0();
		NullCheck(L_48);
		RuntimeObject * L_49 = Dictionary_2_get_Item_m3179620279(L_48, _stringLiteral1726171766, /*hidden argument*/Dictionary_2_get_Item_m3179620279_RuntimeMethod_var);
		__this->set__rootObjectKey_3(((String_t*)CastclassSealed((RuntimeObject*)L_49, String_t_il2cpp_TypeInfo_var)));
		String_t* L_50 = __this->get__rootObjectKey_3();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_51 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		if (L_51)
		{
			goto IL_01f3;
		}
	}
	{
		String_t* L_52 = __this->get__rootObjectKey_3();
		PBXDictionary_t2078602241 * L_53 = __this->get__objects_1();
		String_t* L_54 = __this->get__rootObjectKey_3();
		NullCheck(L_53);
		RuntimeObject * L_55 = Dictionary_2_get_Item_m3179620279(L_53, L_54, /*hidden argument*/Dictionary_2_get_Item_m3179620279_RuntimeMethod_var);
		PBXProject_t4035248171 * L_56 = (PBXProject_t4035248171 *)il2cpp_codegen_object_new(PBXProject_t4035248171_il2cpp_TypeInfo_var);
		PBXProject__ctor_m505489420(L_56, L_52, ((PBXDictionary_t2078602241 *)CastclassClass((RuntimeObject*)L_55, PBXDictionary_t2078602241_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		__this->set__project_20(L_56);
		String_t* L_57 = __this->get__rootObjectKey_3();
		PBXDictionary_t2078602241 * L_58 = __this->get__objects_1();
		PBXProject_t4035248171 * L_59 = __this->get__project_20();
		NullCheck(L_59);
		String_t* L_60 = PBXProject_get_mainGroupID_m4232880761(L_59, /*hidden argument*/NULL);
		NullCheck(L_58);
		RuntimeObject * L_61 = Dictionary_2_get_Item_m3179620279(L_58, L_60, /*hidden argument*/Dictionary_2_get_Item_m3179620279_RuntimeMethod_var);
		PBXGroup_t2128436724 * L_62 = (PBXGroup_t2128436724 *)il2cpp_codegen_object_new(PBXGroup_t2128436724_il2cpp_TypeInfo_var);
		PBXGroup__ctor_m1354276094(L_62, L_57, ((PBXDictionary_t2078602241 *)CastclassClass((RuntimeObject*)L_61, PBXDictionary_t2078602241_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		__this->set__rootGroup_2(L_62);
		goto IL_020b;
	}

IL_01f3:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3752629331(NULL /*static, unused*/, _stringLiteral2693801196, /*hidden argument*/NULL);
		__this->set__project_20((PBXProject_t4035248171 *)NULL);
		__this->set__rootGroup_2((PBXGroup_t2128436724 *)NULL);
	}

IL_020b:
	{
		return;
	}
}
// System.String UnityEditor.XCodeEditor.XCProject::get_projectRootPath()
extern "C"  String_t* XCProject_get_projectRootPath_m3623540506 (XCProject_t3157646134 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CprojectRootPathU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void UnityEditor.XCodeEditor.XCProject::set_projectRootPath(System.String)
extern "C"  void XCProject_set_projectRootPath_m4200784150 (XCProject_t3157646134 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CprojectRootPathU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.String UnityEditor.XCodeEditor.XCProject::get_filePath()
extern "C"  String_t* XCProject_get_filePath_m4061641646 (XCProject_t3157646134 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CfilePathU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void UnityEditor.XCodeEditor.XCProject::set_filePath(System.String)
extern "C"  void XCProject_set_filePath_m1792948959 (XCProject_t3157646134 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CfilePathU3Ek__BackingField_6(L_0);
		return;
	}
}
// UnityEditor.XCodeEditor.PBXProject UnityEditor.XCodeEditor.XCProject::get_project()
extern "C"  PBXProject_t4035248171 * XCProject_get_project_m2578328735 (XCProject_t3157646134 * __this, const RuntimeMethod* method)
{
	{
		PBXProject_t4035248171 * L_0 = __this->get__project_20();
		return L_0;
	}
}
// UnityEditor.XCodeEditor.PBXGroup UnityEditor.XCodeEditor.XCProject::get_rootGroup()
extern "C"  PBXGroup_t2128436724 * XCProject_get_rootGroup_m3903765605 (XCProject_t3157646134 * __this, const RuntimeMethod* method)
{
	{
		PBXGroup_t2128436724 * L_0 = __this->get__rootGroup_2();
		return L_0;
	}
}
// UnityEditor.XCodeEditor.PBXSortedDictionary`1<UnityEditor.XCodeEditor.PBXBuildFile> UnityEditor.XCodeEditor.XCProject::get_buildFiles()
extern "C"  PBXSortedDictionary_1_t2756258120 * XCProject_get_buildFiles_m2028255916 (XCProject_t3157646134 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCProject_get_buildFiles_m2028255916_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PBXSortedDictionary_1_t2756258120 * L_0 = __this->get__buildFiles_8();
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		PBXDictionary_t2078602241 * L_1 = __this->get__objects_1();
		PBXSortedDictionary_1_t2756258120 * L_2 = (PBXSortedDictionary_1_t2756258120 *)il2cpp_codegen_object_new(PBXSortedDictionary_1_t2756258120_il2cpp_TypeInfo_var);
		PBXSortedDictionary_1__ctor_m1490815053(L_2, L_1, /*hidden argument*/PBXSortedDictionary_1__ctor_m1490815053_RuntimeMethod_var);
		__this->set__buildFiles_8(L_2);
	}

IL_001c:
	{
		PBXSortedDictionary_1_t2756258120 * L_3 = __this->get__buildFiles_8();
		return L_3;
	}
}
// UnityEditor.XCodeEditor.PBXSortedDictionary`1<UnityEditor.XCodeEditor.PBXGroup> UnityEditor.XCodeEditor.XCProject::get_groups()
extern "C"  PBXSortedDictionary_1_t2274302150 * XCProject_get_groups_m1730659681 (XCProject_t3157646134 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCProject_get_groups_m1730659681_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PBXSortedDictionary_1_t2274302150 * L_0 = __this->get__groups_9();
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		PBXDictionary_t2078602241 * L_1 = __this->get__objects_1();
		PBXSortedDictionary_1_t2274302150 * L_2 = (PBXSortedDictionary_1_t2274302150 *)il2cpp_codegen_object_new(PBXSortedDictionary_1_t2274302150_il2cpp_TypeInfo_var);
		PBXSortedDictionary_1__ctor_m2076080548(L_2, L_1, /*hidden argument*/PBXSortedDictionary_1__ctor_m2076080548_RuntimeMethod_var);
		__this->set__groups_9(L_2);
	}

IL_001c:
	{
		PBXSortedDictionary_1_t2274302150 * L_3 = __this->get__groups_9();
		return L_3;
	}
}
// UnityEditor.XCodeEditor.PBXSortedDictionary`1<UnityEditor.XCodeEditor.PBXFileReference> UnityEditor.XCodeEditor.XCProject::get_fileReferences()
extern "C"  PBXSortedDictionary_1_t637646383 * XCProject_get_fileReferences_m858134601 (XCProject_t3157646134 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCProject_get_fileReferences_m858134601_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PBXSortedDictionary_1_t637646383 * L_0 = __this->get__fileReferences_10();
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		PBXDictionary_t2078602241 * L_1 = __this->get__objects_1();
		PBXSortedDictionary_1_t637646383 * L_2 = (PBXSortedDictionary_1_t637646383 *)il2cpp_codegen_object_new(PBXSortedDictionary_1_t637646383_il2cpp_TypeInfo_var);
		PBXSortedDictionary_1__ctor_m1533001087(L_2, L_1, /*hidden argument*/PBXSortedDictionary_1__ctor_m1533001087_RuntimeMethod_var);
		__this->set__fileReferences_10(L_2);
	}

IL_001c:
	{
		PBXSortedDictionary_1_t637646383 * L_3 = __this->get__fileReferences_10();
		return L_3;
	}
}
// UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXVariantGroup> UnityEditor.XCodeEditor.XCProject::get_variantGroups()
extern "C"  PBXDictionary_1_t1414031086 * XCProject_get_variantGroups_m3960773799 (XCProject_t3157646134 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCProject_get_variantGroups_m3960773799_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PBXDictionary_1_t1414031086 * L_0 = __this->get__variantGroups_17();
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		PBXDictionary_t2078602241 * L_1 = __this->get__objects_1();
		PBXDictionary_1_t1414031086 * L_2 = (PBXDictionary_1_t1414031086 *)il2cpp_codegen_object_new(PBXDictionary_1_t1414031086_il2cpp_TypeInfo_var);
		PBXDictionary_1__ctor_m2094835722(L_2, L_1, /*hidden argument*/PBXDictionary_1__ctor_m2094835722_RuntimeMethod_var);
		__this->set__variantGroups_17(L_2);
	}

IL_001c:
	{
		PBXDictionary_1_t1414031086 * L_3 = __this->get__variantGroups_17();
		return L_3;
	}
}
// UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXNativeTarget> UnityEditor.XCodeEditor.XCProject::get_nativeTargets()
extern "C"  PBXDictionary_1_t3538108837 * XCProject_get_nativeTargets_m2714694472 (XCProject_t3157646134 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCProject_get_nativeTargets_m2714694472_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PBXDictionary_1_t3538108837 * L_0 = __this->get__nativeTargets_11();
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		PBXDictionary_t2078602241 * L_1 = __this->get__objects_1();
		PBXDictionary_1_t3538108837 * L_2 = (PBXDictionary_1_t3538108837 *)il2cpp_codegen_object_new(PBXDictionary_1_t3538108837_il2cpp_TypeInfo_var);
		PBXDictionary_1__ctor_m4160535383(L_2, L_1, /*hidden argument*/PBXDictionary_1__ctor_m4160535383_RuntimeMethod_var);
		__this->set__nativeTargets_11(L_2);
	}

IL_001c:
	{
		PBXDictionary_1_t3538108837 * L_3 = __this->get__nativeTargets_11();
		return L_3;
	}
}
// UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.XCBuildConfiguration> UnityEditor.XCodeEditor.XCProject::get_buildConfigurations()
extern "C"  PBXDictionary_1_t4195780175 * XCProject_get_buildConfigurations_m1437786018 (XCProject_t3157646134 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCProject_get_buildConfigurations_m1437786018_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PBXDictionary_1_t4195780175 * L_0 = __this->get__buildConfigurations_18();
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		PBXDictionary_t2078602241 * L_1 = __this->get__objects_1();
		PBXDictionary_1_t4195780175 * L_2 = (PBXDictionary_1_t4195780175 *)il2cpp_codegen_object_new(PBXDictionary_1_t4195780175_il2cpp_TypeInfo_var);
		PBXDictionary_1__ctor_m1384061019(L_2, L_1, /*hidden argument*/PBXDictionary_1__ctor_m1384061019_RuntimeMethod_var);
		__this->set__buildConfigurations_18(L_2);
	}

IL_001c:
	{
		PBXDictionary_1_t4195780175 * L_3 = __this->get__buildConfigurations_18();
		return L_3;
	}
}
// UnityEditor.XCodeEditor.PBXSortedDictionary`1<UnityEditor.XCodeEditor.XCConfigurationList> UnityEditor.XCodeEditor.XCProject::get_configurationLists()
extern "C"  PBXSortedDictionary_1_t1246395450 * XCProject_get_configurationLists_m3369988788 (XCProject_t3157646134 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCProject_get_configurationLists_m3369988788_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PBXSortedDictionary_1_t1246395450 * L_0 = __this->get__configurationLists_19();
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		PBXDictionary_t2078602241 * L_1 = __this->get__objects_1();
		PBXSortedDictionary_1_t1246395450 * L_2 = (PBXSortedDictionary_1_t1246395450 *)il2cpp_codegen_object_new(PBXSortedDictionary_1_t1246395450_il2cpp_TypeInfo_var);
		PBXSortedDictionary_1__ctor_m4001532630(L_2, L_1, /*hidden argument*/PBXSortedDictionary_1__ctor_m4001532630_RuntimeMethod_var);
		__this->set__configurationLists_19(L_2);
	}

IL_001c:
	{
		PBXSortedDictionary_1_t1246395450 * L_3 = __this->get__configurationLists_19();
		return L_3;
	}
}
// UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXFrameworksBuildPhase> UnityEditor.XCodeEditor.XCProject::get_frameworkBuildPhases()
extern "C"  PBXDictionary_1_t125107989 * XCProject_get_frameworkBuildPhases_m3514233196 (XCProject_t3157646134 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCProject_get_frameworkBuildPhases_m3514233196_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PBXDictionary_1_t125107989 * L_0 = __this->get__frameworkBuildPhases_12();
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		PBXDictionary_t2078602241 * L_1 = __this->get__objects_1();
		PBXDictionary_1_t125107989 * L_2 = (PBXDictionary_1_t125107989 *)il2cpp_codegen_object_new(PBXDictionary_1_t125107989_il2cpp_TypeInfo_var);
		PBXDictionary_1__ctor_m2416241266(L_2, L_1, /*hidden argument*/PBXDictionary_1__ctor_m2416241266_RuntimeMethod_var);
		__this->set__frameworkBuildPhases_12(L_2);
	}

IL_001c:
	{
		PBXDictionary_1_t125107989 * L_3 = __this->get__frameworkBuildPhases_12();
		return L_3;
	}
}
// UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXResourcesBuildPhase> UnityEditor.XCodeEditor.XCProject::get_resourcesBuildPhases()
extern "C"  PBXDictionary_1_t1783080000 * XCProject_get_resourcesBuildPhases_m2464200803 (XCProject_t3157646134 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCProject_get_resourcesBuildPhases_m2464200803_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PBXDictionary_1_t1783080000 * L_0 = __this->get__resourcesBuildPhases_13();
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		PBXDictionary_t2078602241 * L_1 = __this->get__objects_1();
		PBXDictionary_1_t1783080000 * L_2 = (PBXDictionary_1_t1783080000 *)il2cpp_codegen_object_new(PBXDictionary_1_t1783080000_il2cpp_TypeInfo_var);
		PBXDictionary_1__ctor_m537106829(L_2, L_1, /*hidden argument*/PBXDictionary_1__ctor_m537106829_RuntimeMethod_var);
		__this->set__resourcesBuildPhases_13(L_2);
	}

IL_001c:
	{
		PBXDictionary_1_t1783080000 * L_3 = __this->get__resourcesBuildPhases_13();
		return L_3;
	}
}
// UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXShellScriptBuildPhase> UnityEditor.XCodeEditor.XCProject::get_shellScriptBuildPhases()
extern "C"  PBXDictionary_1_t1977435415 * XCProject_get_shellScriptBuildPhases_m2311296572 (XCProject_t3157646134 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCProject_get_shellScriptBuildPhases_m2311296572_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PBXDictionary_1_t1977435415 * L_0 = __this->get__shellScriptBuildPhases_14();
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		PBXDictionary_t2078602241 * L_1 = __this->get__objects_1();
		PBXDictionary_1_t1977435415 * L_2 = (PBXDictionary_1_t1977435415 *)il2cpp_codegen_object_new(PBXDictionary_1_t1977435415_il2cpp_TypeInfo_var);
		PBXDictionary_1__ctor_m3424980347(L_2, L_1, /*hidden argument*/PBXDictionary_1__ctor_m3424980347_RuntimeMethod_var);
		__this->set__shellScriptBuildPhases_14(L_2);
	}

IL_001c:
	{
		PBXDictionary_1_t1977435415 * L_3 = __this->get__shellScriptBuildPhases_14();
		return L_3;
	}
}
// UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXSourcesBuildPhase> UnityEditor.XCodeEditor.XCProject::get_sourcesBuildPhases()
extern "C"  PBXDictionary_1_t2932651977 * XCProject_get_sourcesBuildPhases_m3700393290 (XCProject_t3157646134 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCProject_get_sourcesBuildPhases_m3700393290_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PBXDictionary_1_t2932651977 * L_0 = __this->get__sourcesBuildPhases_15();
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		PBXDictionary_t2078602241 * L_1 = __this->get__objects_1();
		PBXDictionary_1_t2932651977 * L_2 = (PBXDictionary_1_t2932651977 *)il2cpp_codegen_object_new(PBXDictionary_1_t2932651977_il2cpp_TypeInfo_var);
		PBXDictionary_1__ctor_m3351902458(L_2, L_1, /*hidden argument*/PBXDictionary_1__ctor_m3351902458_RuntimeMethod_var);
		__this->set__sourcesBuildPhases_15(L_2);
	}

IL_001c:
	{
		PBXDictionary_1_t2932651977 * L_3 = __this->get__sourcesBuildPhases_15();
		return L_3;
	}
}
// UnityEditor.XCodeEditor.PBXDictionary`1<UnityEditor.XCodeEditor.PBXCopyFilesBuildPhase> UnityEditor.XCodeEditor.XCProject::get_copyBuildPhases()
extern "C"  PBXDictionary_1_t2617057394 * XCProject_get_copyBuildPhases_m3593876452 (XCProject_t3157646134 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCProject_get_copyBuildPhases_m3593876452_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PBXDictionary_1_t2617057394 * L_0 = __this->get__copyBuildPhases_16();
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		PBXDictionary_t2078602241 * L_1 = __this->get__objects_1();
		PBXDictionary_1_t2617057394 * L_2 = (PBXDictionary_1_t2617057394 *)il2cpp_codegen_object_new(PBXDictionary_1_t2617057394_il2cpp_TypeInfo_var);
		PBXDictionary_1__ctor_m3032813453(L_2, L_1, /*hidden argument*/PBXDictionary_1__ctor_m3032813453_RuntimeMethod_var);
		__this->set__copyBuildPhases_16(L_2);
	}

IL_001c:
	{
		PBXDictionary_1_t2617057394 * L_3 = __this->get__copyBuildPhases_16();
		return L_3;
	}
}
// System.Boolean UnityEditor.XCodeEditor.XCProject::AddOtherCFlags(System.String)
extern "C"  bool XCProject_AddOtherCFlags_m696039661 (XCProject_t3157646134 * __this, String_t* ___flag0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCProject_AddOtherCFlags_m696039661_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___flag0;
		PBXList_t4289188522 * L_1 = (PBXList_t4289188522 *)il2cpp_codegen_object_new(PBXList_t4289188522_il2cpp_TypeInfo_var);
		PBXList__ctor_m3538174448(L_1, L_0, /*hidden argument*/NULL);
		bool L_2 = XCProject_AddOtherCFlags_m432253178(__this, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEditor.XCodeEditor.XCProject::AddOtherCFlags(UnityEditor.XCodeEditor.PBXList)
extern "C"  bool XCProject_AddOtherCFlags_m432253178 (XCProject_t3157646134 * __this, PBXList_t4289188522 * ___flags0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCProject_AddOtherCFlags_m432253178_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t3633994766  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Enumerator_t3190505374  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		PBXDictionary_1_t4195780175 * L_0 = XCProject_get_buildConfigurations_m1437786018(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Enumerator_t3190505374  L_1 = Dictionary_2_GetEnumerator_m1452846353(L_0, /*hidden argument*/Dictionary_2_GetEnumerator_m1452846353_RuntimeMethod_var);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0027;
		}

IL_0011:
		{
			KeyValuePair_2_t3633994766  L_2 = Enumerator_get_Current_m2313683576((&V_1), /*hidden argument*/Enumerator_get_Current_m2313683576_RuntimeMethod_var);
			V_0 = L_2;
			XCBuildConfiguration_t1451066300 * L_3 = KeyValuePair_2_get_Value_m953788127((&V_0), /*hidden argument*/KeyValuePair_2_get_Value_m953788127_RuntimeMethod_var);
			PBXList_t4289188522 * L_4 = ___flags0;
			NullCheck(L_3);
			XCBuildConfiguration_AddOtherCFlags_m3896651138(L_3, L_4, /*hidden argument*/NULL);
		}

IL_0027:
		{
			bool L_5 = Enumerator_MoveNext_m3171309827((&V_1), /*hidden argument*/Enumerator_MoveNext_m3171309827_RuntimeMethod_var);
			if (L_5)
			{
				goto IL_0011;
			}
		}

IL_0033:
		{
			IL2CPP_LEAVE(0x46, FINALLY_0038);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0038;
	}

FINALLY_0038:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1544304532((&V_1), /*hidden argument*/Enumerator_Dispose_m1544304532_RuntimeMethod_var);
		IL2CPP_END_FINALLY(56)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(56)
	{
		IL2CPP_JUMP_TBL(0x46, IL_0046)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0046:
	{
		__this->set_modified_7((bool)1);
		bool L_6 = __this->get_modified_7();
		return L_6;
	}
}
// System.Boolean UnityEditor.XCodeEditor.XCProject::AddOtherLinkerFlags(System.String)
extern "C"  bool XCProject_AddOtherLinkerFlags_m1772294907 (XCProject_t3157646134 * __this, String_t* ___flag0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCProject_AddOtherLinkerFlags_m1772294907_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___flag0;
		PBXList_t4289188522 * L_1 = (PBXList_t4289188522 *)il2cpp_codegen_object_new(PBXList_t4289188522_il2cpp_TypeInfo_var);
		PBXList__ctor_m3538174448(L_1, L_0, /*hidden argument*/NULL);
		bool L_2 = XCProject_AddOtherLinkerFlags_m1003050127(__this, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEditor.XCodeEditor.XCProject::AddOtherLinkerFlags(UnityEditor.XCodeEditor.PBXList)
extern "C"  bool XCProject_AddOtherLinkerFlags_m1003050127 (XCProject_t3157646134 * __this, PBXList_t4289188522 * ___flags0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCProject_AddOtherLinkerFlags_m1003050127_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t3633994766  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Enumerator_t3190505374  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		PBXDictionary_1_t4195780175 * L_0 = XCProject_get_buildConfigurations_m1437786018(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Enumerator_t3190505374  L_1 = Dictionary_2_GetEnumerator_m1452846353(L_0, /*hidden argument*/Dictionary_2_GetEnumerator_m1452846353_RuntimeMethod_var);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0027;
		}

IL_0011:
		{
			KeyValuePair_2_t3633994766  L_2 = Enumerator_get_Current_m2313683576((&V_1), /*hidden argument*/Enumerator_get_Current_m2313683576_RuntimeMethod_var);
			V_0 = L_2;
			XCBuildConfiguration_t1451066300 * L_3 = KeyValuePair_2_get_Value_m953788127((&V_0), /*hidden argument*/KeyValuePair_2_get_Value_m953788127_RuntimeMethod_var);
			PBXList_t4289188522 * L_4 = ___flags0;
			NullCheck(L_3);
			XCBuildConfiguration_AddOtherLinkerFlags_m3172986619(L_3, L_4, /*hidden argument*/NULL);
		}

IL_0027:
		{
			bool L_5 = Enumerator_MoveNext_m3171309827((&V_1), /*hidden argument*/Enumerator_MoveNext_m3171309827_RuntimeMethod_var);
			if (L_5)
			{
				goto IL_0011;
			}
		}

IL_0033:
		{
			IL2CPP_LEAVE(0x46, FINALLY_0038);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0038;
	}

FINALLY_0038:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1544304532((&V_1), /*hidden argument*/Enumerator_Dispose_m1544304532_RuntimeMethod_var);
		IL2CPP_END_FINALLY(56)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(56)
	{
		IL2CPP_JUMP_TBL(0x46, IL_0046)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0046:
	{
		__this->set_modified_7((bool)1);
		bool L_6 = __this->get_modified_7();
		return L_6;
	}
}
// System.Boolean UnityEditor.XCodeEditor.XCProject::overwriteBuildSetting(System.String,System.String,System.String)
extern "C"  bool XCProject_overwriteBuildSetting_m3219624 (XCProject_t3157646134 * __this, String_t* ___settingName0, String_t* ___newValue1, String_t* ___buildConfigName2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCProject_overwriteBuildSetting_m3219624_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t3633994766  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Enumerator_t3190505374  V_1;
	memset(&V_1, 0, sizeof(V_1));
	XCBuildConfiguration_t1451066300 * V_2 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		StringU5BU5D_t1281789340* L_0 = ((StringU5BU5D_t1281789340*)SZArrayNew(StringU5BU5D_t1281789340_il2cpp_TypeInfo_var, (uint32_t)6));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral3405705519);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3405705519);
		StringU5BU5D_t1281789340* L_1 = L_0;
		String_t* L_2 = ___settingName0;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_2);
		StringU5BU5D_t1281789340* L_3 = L_1;
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, _stringLiteral3452614528);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral3452614528);
		StringU5BU5D_t1281789340* L_4 = L_3;
		String_t* L_5 = ___newValue1;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_5);
		StringU5BU5D_t1281789340* L_6 = L_4;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral3452614528);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral3452614528);
		StringU5BU5D_t1281789340* L_7 = L_6;
		String_t* L_8 = ___buildConfigName2;
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)L_8);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m1809518182(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		PBXDictionary_1_t4195780175 * L_10 = XCProject_get_buildConfigurations_m1437786018(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		Enumerator_t3190505374  L_11 = Dictionary_2_GetEnumerator_m1452846353(L_10, /*hidden argument*/Dictionary_2_GetEnumerator_m1452846353_RuntimeMethod_var);
		V_1 = L_11;
	}

IL_0040:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00a0;
		}

IL_0045:
		{
			KeyValuePair_2_t3633994766  L_12 = Enumerator_get_Current_m2313683576((&V_1), /*hidden argument*/Enumerator_get_Current_m2313683576_RuntimeMethod_var);
			V_0 = L_12;
			XCBuildConfiguration_t1451066300 * L_13 = KeyValuePair_2_get_Value_m953788127((&V_0), /*hidden argument*/KeyValuePair_2_get_Value_m953788127_RuntimeMethod_var);
			V_2 = L_13;
			XCBuildConfiguration_t1451066300 * L_14 = V_2;
			NullCheck(L_14);
			PBXDictionary_t2078602241 * L_15 = PBXObject_get_data_m910114329(L_14, /*hidden argument*/NULL);
			NullCheck(L_15);
			RuntimeObject * L_16 = Dictionary_2_get_Item_m3179620279(L_15, _stringLiteral62725243, /*hidden argument*/Dictionary_2_get_Item_m3179620279_RuntimeMethod_var);
			String_t* L_17 = ___buildConfigName2;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_18 = String_op_Equality_m920492651(NULL /*static, unused*/, ((String_t*)CastclassSealed((RuntimeObject*)L_16, String_t_il2cpp_TypeInfo_var)), L_17, /*hidden argument*/NULL);
			if (L_18)
			{
				goto IL_0085;
			}
		}

IL_0075:
		{
			String_t* L_19 = ___buildConfigName2;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_20 = String_op_Equality_m920492651(NULL /*static, unused*/, L_19, _stringLiteral2910037981, /*hidden argument*/NULL);
			if (!L_20)
			{
				goto IL_00a0;
			}
		}

IL_0085:
		{
			XCBuildConfiguration_t1451066300 * L_21 = KeyValuePair_2_get_Value_m953788127((&V_0), /*hidden argument*/KeyValuePair_2_get_Value_m953788127_RuntimeMethod_var);
			String_t* L_22 = ___settingName0;
			String_t* L_23 = ___newValue1;
			NullCheck(L_21);
			XCBuildConfiguration_overwriteBuildSetting_m77475801(L_21, L_22, L_23, /*hidden argument*/NULL);
			__this->set_modified_7((bool)1);
			goto IL_00a0;
		}

IL_00a0:
		{
			bool L_24 = Enumerator_MoveNext_m3171309827((&V_1), /*hidden argument*/Enumerator_MoveNext_m3171309827_RuntimeMethod_var);
			if (L_24)
			{
				goto IL_0045;
			}
		}

IL_00ac:
		{
			IL2CPP_LEAVE(0xBF, FINALLY_00b1);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_00b1;
	}

FINALLY_00b1:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1544304532((&V_1), /*hidden argument*/Enumerator_Dispose_m1544304532_RuntimeMethod_var);
		IL2CPP_END_FINALLY(177)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(177)
	{
		IL2CPP_JUMP_TBL(0xBF, IL_00bf)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_00bf:
	{
		bool L_25 = __this->get_modified_7();
		return L_25;
	}
}
// System.Boolean UnityEditor.XCodeEditor.XCProject::AddHeaderSearchPaths(System.String)
extern "C"  bool XCProject_AddHeaderSearchPaths_m2329174020 (XCProject_t3157646134 * __this, String_t* ___path0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCProject_AddHeaderSearchPaths_m2329174020_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___path0;
		PBXList_t4289188522 * L_1 = (PBXList_t4289188522 *)il2cpp_codegen_object_new(PBXList_t4289188522_il2cpp_TypeInfo_var);
		PBXList__ctor_m3538174448(L_1, L_0, /*hidden argument*/NULL);
		bool L_2 = XCProject_AddHeaderSearchPaths_m2713756577(__this, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEditor.XCodeEditor.XCProject::AddHeaderSearchPaths(UnityEditor.XCodeEditor.PBXList)
extern "C"  bool XCProject_AddHeaderSearchPaths_m2713756577 (XCProject_t3157646134 * __this, PBXList_t4289188522 * ___paths0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCProject_AddHeaderSearchPaths_m2713756577_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t3633994766  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Enumerator_t3190505374  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		PBXList_t4289188522 * L_0 = ___paths0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m904156431(NULL /*static, unused*/, _stringLiteral351569418, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		PBXDictionary_1_t4195780175 * L_2 = XCProject_get_buildConfigurations_m1437786018(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Enumerator_t3190505374  L_3 = Dictionary_2_GetEnumerator_m1452846353(L_2, /*hidden argument*/Dictionary_2_GetEnumerator_m1452846353_RuntimeMethod_var);
		V_1 = L_3;
	}

IL_001c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0038;
		}

IL_0021:
		{
			KeyValuePair_2_t3633994766  L_4 = Enumerator_get_Current_m2313683576((&V_1), /*hidden argument*/Enumerator_get_Current_m2313683576_RuntimeMethod_var);
			V_0 = L_4;
			XCBuildConfiguration_t1451066300 * L_5 = KeyValuePair_2_get_Value_m953788127((&V_0), /*hidden argument*/KeyValuePair_2_get_Value_m953788127_RuntimeMethod_var);
			PBXList_t4289188522 * L_6 = ___paths0;
			NullCheck(L_5);
			XCBuildConfiguration_AddHeaderSearchPaths_m4278200854(L_5, L_6, (bool)1, /*hidden argument*/NULL);
		}

IL_0038:
		{
			bool L_7 = Enumerator_MoveNext_m3171309827((&V_1), /*hidden argument*/Enumerator_MoveNext_m3171309827_RuntimeMethod_var);
			if (L_7)
			{
				goto IL_0021;
			}
		}

IL_0044:
		{
			IL2CPP_LEAVE(0x57, FINALLY_0049);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0049;
	}

FINALLY_0049:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1544304532((&V_1), /*hidden argument*/Enumerator_Dispose_m1544304532_RuntimeMethod_var);
		IL2CPP_END_FINALLY(73)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(73)
	{
		IL2CPP_JUMP_TBL(0x57, IL_0057)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0057:
	{
		__this->set_modified_7((bool)1);
		bool L_8 = __this->get_modified_7();
		return L_8;
	}
}
// System.Boolean UnityEditor.XCodeEditor.XCProject::AddLibrarySearchPaths(System.String)
extern "C"  bool XCProject_AddLibrarySearchPaths_m522864078 (XCProject_t3157646134 * __this, String_t* ___path0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCProject_AddLibrarySearchPaths_m522864078_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___path0;
		PBXList_t4289188522 * L_1 = (PBXList_t4289188522 *)il2cpp_codegen_object_new(PBXList_t4289188522_il2cpp_TypeInfo_var);
		PBXList__ctor_m3538174448(L_1, L_0, /*hidden argument*/NULL);
		bool L_2 = XCProject_AddLibrarySearchPaths_m452363867(__this, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEditor.XCodeEditor.XCProject::AddLibrarySearchPaths(UnityEditor.XCodeEditor.PBXList)
extern "C"  bool XCProject_AddLibrarySearchPaths_m452363867 (XCProject_t3157646134 * __this, PBXList_t4289188522 * ___paths0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCProject_AddLibrarySearchPaths_m452363867_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t3633994766  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Enumerator_t3190505374  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		PBXList_t4289188522 * L_0 = ___paths0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m904156431(NULL /*static, unused*/, _stringLiteral1629623998, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		PBXDictionary_1_t4195780175 * L_2 = XCProject_get_buildConfigurations_m1437786018(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Enumerator_t3190505374  L_3 = Dictionary_2_GetEnumerator_m1452846353(L_2, /*hidden argument*/Dictionary_2_GetEnumerator_m1452846353_RuntimeMethod_var);
		V_1 = L_3;
	}

IL_001c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0038;
		}

IL_0021:
		{
			KeyValuePair_2_t3633994766  L_4 = Enumerator_get_Current_m2313683576((&V_1), /*hidden argument*/Enumerator_get_Current_m2313683576_RuntimeMethod_var);
			V_0 = L_4;
			XCBuildConfiguration_t1451066300 * L_5 = KeyValuePair_2_get_Value_m953788127((&V_0), /*hidden argument*/KeyValuePair_2_get_Value_m953788127_RuntimeMethod_var);
			PBXList_t4289188522 * L_6 = ___paths0;
			NullCheck(L_5);
			XCBuildConfiguration_AddLibrarySearchPaths_m1306486965(L_5, L_6, (bool)1, /*hidden argument*/NULL);
		}

IL_0038:
		{
			bool L_7 = Enumerator_MoveNext_m3171309827((&V_1), /*hidden argument*/Enumerator_MoveNext_m3171309827_RuntimeMethod_var);
			if (L_7)
			{
				goto IL_0021;
			}
		}

IL_0044:
		{
			IL2CPP_LEAVE(0x57, FINALLY_0049);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0049;
	}

FINALLY_0049:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1544304532((&V_1), /*hidden argument*/Enumerator_Dispose_m1544304532_RuntimeMethod_var);
		IL2CPP_END_FINALLY(73)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(73)
	{
		IL2CPP_JUMP_TBL(0x57, IL_0057)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0057:
	{
		__this->set_modified_7((bool)1);
		bool L_8 = __this->get_modified_7();
		return L_8;
	}
}
// System.Boolean UnityEditor.XCodeEditor.XCProject::AddFrameworkSearchPaths(System.String)
extern "C"  bool XCProject_AddFrameworkSearchPaths_m3452696326 (XCProject_t3157646134 * __this, String_t* ___path0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCProject_AddFrameworkSearchPaths_m3452696326_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___path0;
		PBXList_t4289188522 * L_1 = (PBXList_t4289188522 *)il2cpp_codegen_object_new(PBXList_t4289188522_il2cpp_TypeInfo_var);
		PBXList__ctor_m3538174448(L_1, L_0, /*hidden argument*/NULL);
		bool L_2 = XCProject_AddFrameworkSearchPaths_m2470425909(__this, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEditor.XCodeEditor.XCProject::AddFrameworkSearchPaths(UnityEditor.XCodeEditor.PBXList)
extern "C"  bool XCProject_AddFrameworkSearchPaths_m2470425909 (XCProject_t3157646134 * __this, PBXList_t4289188522 * ___paths0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCProject_AddFrameworkSearchPaths_m2470425909_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t3633994766  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Enumerator_t3190505374  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		PBXDictionary_1_t4195780175 * L_0 = XCProject_get_buildConfigurations_m1437786018(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Enumerator_t3190505374  L_1 = Dictionary_2_GetEnumerator_m1452846353(L_0, /*hidden argument*/Dictionary_2_GetEnumerator_m1452846353_RuntimeMethod_var);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0028;
		}

IL_0011:
		{
			KeyValuePair_2_t3633994766  L_2 = Enumerator_get_Current_m2313683576((&V_1), /*hidden argument*/Enumerator_get_Current_m2313683576_RuntimeMethod_var);
			V_0 = L_2;
			XCBuildConfiguration_t1451066300 * L_3 = KeyValuePair_2_get_Value_m953788127((&V_0), /*hidden argument*/KeyValuePair_2_get_Value_m953788127_RuntimeMethod_var);
			PBXList_t4289188522 * L_4 = ___paths0;
			NullCheck(L_3);
			XCBuildConfiguration_AddFrameworkSearchPaths_m458952264(L_3, L_4, (bool)1, /*hidden argument*/NULL);
		}

IL_0028:
		{
			bool L_5 = Enumerator_MoveNext_m3171309827((&V_1), /*hidden argument*/Enumerator_MoveNext_m3171309827_RuntimeMethod_var);
			if (L_5)
			{
				goto IL_0011;
			}
		}

IL_0034:
		{
			IL2CPP_LEAVE(0x47, FINALLY_0039);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0039;
	}

FINALLY_0039:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1544304532((&V_1), /*hidden argument*/Enumerator_Dispose_m1544304532_RuntimeMethod_var);
		IL2CPP_END_FINALLY(57)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(57)
	{
		IL2CPP_JUMP_TBL(0x47, IL_0047)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0047:
	{
		__this->set_modified_7((bool)1);
		bool L_6 = __this->get_modified_7();
		return L_6;
	}
}
// System.Object UnityEditor.XCodeEditor.XCProject::GetObject(System.String)
extern "C"  RuntimeObject * XCProject_GetObject_m1073236403 (XCProject_t3157646134 * __this, String_t* ___guid0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCProject_GetObject_m1073236403_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PBXDictionary_t2078602241 * L_0 = __this->get__objects_1();
		String_t* L_1 = ___guid0;
		NullCheck(L_0);
		RuntimeObject * L_2 = Dictionary_2_get_Item_m3179620279(L_0, L_1, /*hidden argument*/Dictionary_2_get_Item_m3179620279_RuntimeMethod_var);
		return L_2;
	}
}
// UnityEditor.XCodeEditor.PBXDictionary UnityEditor.XCodeEditor.XCProject::AddFile(System.String,UnityEditor.XCodeEditor.PBXGroup,System.String,System.Boolean,System.Boolean)
extern "C"  PBXDictionary_t2078602241 * XCProject_AddFile_m3957251854 (XCProject_t3157646134 * __this, String_t* ___filePath0, PBXGroup_t2128436724 * ___parent1, String_t* ___tree2, bool ___createBuildFiles3, bool ___weak4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCProject_AddFile_m3957251854_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PBXDictionary_t2078602241 * V_0 = NULL;
	String_t* V_1 = NULL;
	Uri_t100236324 * V_2 = NULL;
	Uri_t100236324 * V_3 = NULL;
	PBXFileReference_t491780957 * V_4 = NULL;
	String_t* V_5 = NULL;
	KeyValuePair_2_t3858289876  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Enumerator_t3414800484  V_7;
	memset(&V_7, 0, sizeof(V_7));
	String_t* V_8 = NULL;
	KeyValuePair_2_t1221294591  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Enumerator_t777805199  V_10;
	memset(&V_10, 0, sizeof(V_10));
	KeyValuePair_2_t1415650006  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Enumerator_t972160614  V_12;
	memset(&V_12, 0, sizeof(V_12));
	KeyValuePair_2_t2370866568  V_13;
	memset(&V_13, 0, sizeof(V_13));
	Enumerator_t1927377176  V_14;
	memset(&V_14, 0, sizeof(V_14));
	KeyValuePair_2_t2055271985  V_15;
	memset(&V_15, 0, sizeof(V_15));
	Enumerator_t1611782593  V_16;
	memset(&V_16, 0, sizeof(V_16));
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		PBXDictionary_t2078602241 * L_0 = (PBXDictionary_t2078602241 *)il2cpp_codegen_object_new(PBXDictionary_t2078602241_il2cpp_TypeInfo_var);
		PBXDictionary__ctor_m3541187054(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = ___filePath0;
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2850623458(NULL /*static, unused*/, _stringLiteral2878191127, /*hidden argument*/NULL);
		PBXDictionary_t2078602241 * L_2 = V_0;
		return L_2;
	}

IL_0018:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		V_1 = L_3;
		String_t* L_4 = ___filePath0;
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1605229823_il2cpp_TypeInfo_var);
		bool L_5 = Path_IsPathRooted_m3515805419(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral2867911068, /*hidden argument*/NULL);
		String_t* L_6 = ___filePath0;
		V_1 = L_6;
		goto IL_0056;
	}

IL_003a:
	{
		String_t* L_7 = ___tree2;
		NullCheck(L_7);
		int32_t L_8 = String_CompareTo_m3414379165(L_7, _stringLiteral1430076019, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0056;
		}
	}
	{
		String_t* L_9 = Application_get_dataPath_m4232621142(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_10 = ___filePath0;
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1605229823_il2cpp_TypeInfo_var);
		String_t* L_11 = Path_Combine_m3389272516(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		V_1 = L_11;
	}

IL_0056:
	{
		String_t* L_12 = V_1;
		bool L_13 = File_Exists_m3943585060(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_008e;
		}
	}
	{
		String_t* L_14 = V_1;
		bool L_15 = Directory_Exists_m1484791558(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_008e;
		}
	}
	{
		String_t* L_16 = ___tree2;
		NullCheck(L_16);
		int32_t L_17 = String_CompareTo_m3414379165(L_16, _stringLiteral1430076019, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_008e;
		}
	}
	{
		String_t* L_18 = ___filePath0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral1293495386, L_18, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		PBXDictionary_t2078602241 * L_20 = V_0;
		return L_20;
	}

IL_008e:
	{
		String_t* L_21 = ___tree2;
		NullCheck(L_21);
		int32_t L_22 = String_CompareTo_m3414379165(L_21, _stringLiteral901633645, /*hidden argument*/NULL);
		if (L_22)
		{
			goto IL_00d8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral3662213890, /*hidden argument*/NULL);
		String_t* L_23 = V_1;
		Uri_t100236324 * L_24 = (Uri_t100236324 *)il2cpp_codegen_object_new(Uri_t100236324_il2cpp_TypeInfo_var);
		Uri__ctor_m800430703(L_24, L_23, /*hidden argument*/NULL);
		V_2 = L_24;
		String_t* L_25 = XCProject_get_projectRootPath_m3623540506(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_26 = String_Concat_m3937257545(NULL /*static, unused*/, L_25, _stringLiteral3450648449, /*hidden argument*/NULL);
		Uri_t100236324 * L_27 = (Uri_t100236324 *)il2cpp_codegen_object_new(Uri_t100236324_il2cpp_TypeInfo_var);
		Uri__ctor_m800430703(L_27, L_26, /*hidden argument*/NULL);
		V_3 = L_27;
		Uri_t100236324 * L_28 = V_3;
		Uri_t100236324 * L_29 = V_2;
		NullCheck(L_28);
		Uri_t100236324 * L_30 = Uri_MakeRelativeUri_m3468717094(L_28, L_29, /*hidden argument*/NULL);
		NullCheck(L_30);
		String_t* L_31 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_30);
		___filePath0 = L_31;
		goto IL_00fa;
	}

IL_00d8:
	{
		String_t* L_32 = ___tree2;
		NullCheck(L_32);
		int32_t L_33 = String_CompareTo_m3414379165(L_32, _stringLiteral3589484046, /*hidden argument*/NULL);
		if (L_33)
		{
			goto IL_00fa;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral155981296, /*hidden argument*/NULL);
		String_t* L_34 = ___filePath0;
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1605229823_il2cpp_TypeInfo_var);
		String_t* L_35 = Path_GetFileName_m1354558116(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		___filePath0 = L_35;
	}

IL_00fa:
	{
		PBXGroup_t2128436724 * L_36 = ___parent1;
		if (L_36)
		{
			goto IL_0108;
		}
	}
	{
		PBXGroup_t2128436724 * L_37 = __this->get__rootGroup_2();
		___parent1 = L_37;
	}

IL_0108:
	{
		String_t* L_38 = ___filePath0;
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1605229823_il2cpp_TypeInfo_var);
		String_t* L_39 = Path_GetFileName_m1354558116(NULL /*static, unused*/, L_38, /*hidden argument*/NULL);
		PBXFileReference_t491780957 * L_40 = XCProject_GetFile_m1538714244(__this, L_39, /*hidden argument*/NULL);
		V_4 = L_40;
		PBXFileReference_t491780957 * L_41 = V_4;
		if (!L_41)
		{
			goto IL_012f;
		}
	}
	{
		String_t* L_42 = ___filePath0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_43 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral2046401046, L_42, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_43, /*hidden argument*/NULL);
		return (PBXDictionary_t2078602241 *)NULL;
	}

IL_012f:
	{
		String_t* L_44 = ___filePath0;
		RuntimeTypeHandle_t3027515415  L_45 = { reinterpret_cast<intptr_t> (TreeEnum_t2111024294_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_46 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_45, /*hidden argument*/NULL);
		String_t* L_47 = ___tree2;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t4135868527_il2cpp_TypeInfo_var);
		RuntimeObject * L_48 = Enum_Parse_m1871331077(NULL /*static, unused*/, L_46, L_47, /*hidden argument*/NULL);
		PBXFileReference_t491780957 * L_49 = (PBXFileReference_t491780957 *)il2cpp_codegen_object_new(PBXFileReference_t491780957_il2cpp_TypeInfo_var);
		PBXFileReference__ctor_m417092888(L_49, L_44, ((*(int32_t*)((int32_t*)UnBox(L_48, TreeEnum_t2111024294_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		V_4 = L_49;
		PBXGroup_t2128436724 * L_50 = ___parent1;
		PBXFileReference_t491780957 * L_51 = V_4;
		NullCheck(L_50);
		PBXGroup_AddChild_m3690815735(L_50, L_51, /*hidden argument*/NULL);
		PBXSortedDictionary_1_t637646383 * L_52 = XCProject_get_fileReferences_m858134601(__this, /*hidden argument*/NULL);
		PBXFileReference_t491780957 * L_53 = V_4;
		NullCheck(L_52);
		PBXSortedDictionary_1_Add_m3189495737(L_52, L_53, /*hidden argument*/PBXSortedDictionary_1_Add_m3189495737_RuntimeMethod_var);
		PBXDictionary_t2078602241 * L_54 = V_0;
		PBXFileReference_t491780957 * L_55 = V_4;
		NullCheck(L_55);
		String_t* L_56 = PBXObject_get_guid_m2491609601(L_55, /*hidden argument*/NULL);
		PBXFileReference_t491780957 * L_57 = V_4;
		NullCheck(L_54);
		Dictionary_2_Add_m825500632(L_54, L_56, L_57, /*hidden argument*/Dictionary_2_Add_m825500632_RuntimeMethod_var);
		PBXFileReference_t491780957 * L_58 = V_4;
		NullCheck(L_58);
		String_t* L_59 = L_58->get_buildPhase_10();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_60 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_59, /*hidden argument*/NULL);
		if (L_60)
		{
			goto IL_040c;
		}
	}
	{
		bool L_61 = ___createBuildFiles3;
		if (!L_61)
		{
			goto IL_040c;
		}
	}
	{
		PBXFileReference_t491780957 * L_62 = V_4;
		NullCheck(L_62);
		String_t* L_63 = L_62->get_buildPhase_10();
		V_5 = L_63;
		String_t* L_64 = V_5;
		if (!L_64)
		{
			goto IL_03eb;
		}
	}
	{
		String_t* L_65 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_66 = String_op_Equality_m920492651(NULL /*static, unused*/, L_65, _stringLiteral2388984547, /*hidden argument*/NULL);
		if (L_66)
		{
			goto IL_01f3;
		}
	}
	{
		String_t* L_67 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_68 = String_op_Equality_m920492651(NULL /*static, unused*/, L_67, _stringLiteral3050148334, /*hidden argument*/NULL);
		if (L_68)
		{
			goto IL_0297;
		}
	}
	{
		String_t* L_69 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_70 = String_op_Equality_m920492651(NULL /*static, unused*/, L_69, _stringLiteral506062875, /*hidden argument*/NULL);
		if (L_70)
		{
			goto IL_02ec;
		}
	}
	{
		String_t* L_71 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_72 = String_op_Equality_m920492651(NULL /*static, unused*/, L_71, _stringLiteral2428378231, /*hidden argument*/NULL);
		if (L_72)
		{
			goto IL_0341;
		}
	}
	{
		String_t* L_73 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_74 = String_op_Equality_m920492651(NULL /*static, unused*/, L_73, _stringLiteral2369475652, /*hidden argument*/NULL);
		if (L_74)
		{
			goto IL_0396;
		}
	}
	{
		goto IL_0400;
	}

IL_01f3:
	{
		PBXDictionary_1_t125107989 * L_75 = XCProject_get_frameworkBuildPhases_m3514233196(__this, /*hidden argument*/NULL);
		NullCheck(L_75);
		Enumerator_t3414800484  L_76 = Dictionary_2_GetEnumerator_m2923669497(L_75, /*hidden argument*/Dictionary_2_GetEnumerator_m2923669497_RuntimeMethod_var);
		V_7 = L_76;
	}

IL_0200:
	try
	{ // begin try (depth: 1)
		{
			goto IL_021a;
		}

IL_0205:
		{
			KeyValuePair_2_t3858289876  L_77 = Enumerator_get_Current_m2582763622((&V_7), /*hidden argument*/Enumerator_get_Current_m2582763622_RuntimeMethod_var);
			V_6 = L_77;
			PBXFileReference_t491780957 * L_78 = V_4;
			KeyValuePair_2_t3858289876  L_79 = V_6;
			bool L_80 = ___weak4;
			XCProject_BuildAddFile_m2837910453(__this, L_78, L_79, L_80, /*hidden argument*/NULL);
		}

IL_021a:
		{
			bool L_81 = Enumerator_MoveNext_m2971571199((&V_7), /*hidden argument*/Enumerator_MoveNext_m2971571199_RuntimeMethod_var);
			if (L_81)
			{
				goto IL_0205;
			}
		}

IL_0226:
		{
			IL2CPP_LEAVE(0x239, FINALLY_022b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_022b;
	}

FINALLY_022b:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m636527872((&V_7), /*hidden argument*/Enumerator_Dispose_m636527872_RuntimeMethod_var);
		IL2CPP_END_FINALLY(555)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(555)
	{
		IL2CPP_JUMP_TBL(0x239, IL_0239)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0239:
	{
		String_t* L_82 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_83 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_82, /*hidden argument*/NULL);
		if (L_83)
		{
			goto IL_0292;
		}
	}
	{
		String_t* L_84 = ___tree2;
		NullCheck(L_84);
		int32_t L_85 = String_CompareTo_m3414379165(L_84, _stringLiteral901633645, /*hidden argument*/NULL);
		if (L_85)
		{
			goto IL_0292;
		}
	}
	{
		String_t* L_86 = ___filePath0;
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1605229823_il2cpp_TypeInfo_var);
		String_t* L_87 = Path_GetDirectoryName_m3496866581(NULL /*static, unused*/, L_86, /*hidden argument*/NULL);
		String_t* L_88 = Path_Combine_m3389272516(NULL /*static, unused*/, _stringLiteral1573716555, L_87, /*hidden argument*/NULL);
		V_8 = L_88;
		String_t* L_89 = V_1;
		bool L_90 = File_Exists_m3943585060(NULL /*static, unused*/, L_89, /*hidden argument*/NULL);
		if (!L_90)
		{
			goto IL_0284;
		}
	}
	{
		String_t* L_91 = V_8;
		PBXList_t4289188522 * L_92 = (PBXList_t4289188522 *)il2cpp_codegen_object_new(PBXList_t4289188522_il2cpp_TypeInfo_var);
		PBXList__ctor_m3538174448(L_92, L_91, /*hidden argument*/NULL);
		XCProject_AddLibrarySearchPaths_m452363867(__this, L_92, /*hidden argument*/NULL);
		goto IL_0292;
	}

IL_0284:
	{
		String_t* L_93 = V_8;
		PBXList_t4289188522 * L_94 = (PBXList_t4289188522 *)il2cpp_codegen_object_new(PBXList_t4289188522_il2cpp_TypeInfo_var);
		PBXList__ctor_m3538174448(L_94, L_93, /*hidden argument*/NULL);
		XCProject_AddFrameworkSearchPaths_m2470425909(__this, L_94, /*hidden argument*/NULL);
	}

IL_0292:
	{
		goto IL_040c;
	}

IL_0297:
	{
		PBXDictionary_1_t1783080000 * L_95 = XCProject_get_resourcesBuildPhases_m2464200803(__this, /*hidden argument*/NULL);
		NullCheck(L_95);
		Enumerator_t777805199  L_96 = Dictionary_2_GetEnumerator_m2740980526(L_95, /*hidden argument*/Dictionary_2_GetEnumerator_m2740980526_RuntimeMethod_var);
		V_10 = L_96;
	}

IL_02a4:
	try
	{ // begin try (depth: 1)
		{
			goto IL_02c8;
		}

IL_02a9:
		{
			KeyValuePair_2_t1221294591  L_97 = Enumerator_get_Current_m2465070363((&V_10), /*hidden argument*/Enumerator_get_Current_m2465070363_RuntimeMethod_var);
			V_9 = L_97;
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
			Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral2060813112, /*hidden argument*/NULL);
			PBXFileReference_t491780957 * L_98 = V_4;
			KeyValuePair_2_t1221294591  L_99 = V_9;
			bool L_100 = ___weak4;
			XCProject_BuildAddFile_m2859561336(__this, L_98, L_99, L_100, /*hidden argument*/NULL);
		}

IL_02c8:
		{
			bool L_101 = Enumerator_MoveNext_m413976394((&V_10), /*hidden argument*/Enumerator_MoveNext_m413976394_RuntimeMethod_var);
			if (L_101)
			{
				goto IL_02a9;
			}
		}

IL_02d4:
		{
			IL2CPP_LEAVE(0x2E7, FINALLY_02d9);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_02d9;
	}

FINALLY_02d9:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m649549926((&V_10), /*hidden argument*/Enumerator_Dispose_m649549926_RuntimeMethod_var);
		IL2CPP_END_FINALLY(729)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(729)
	{
		IL2CPP_JUMP_TBL(0x2E7, IL_02e7)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_02e7:
	{
		goto IL_040c;
	}

IL_02ec:
	{
		PBXDictionary_1_t1977435415 * L_102 = XCProject_get_shellScriptBuildPhases_m2311296572(__this, /*hidden argument*/NULL);
		NullCheck(L_102);
		Enumerator_t972160614  L_103 = Dictionary_2_GetEnumerator_m1885921306(L_102, /*hidden argument*/Dictionary_2_GetEnumerator_m1885921306_RuntimeMethod_var);
		V_12 = L_103;
	}

IL_02f9:
	try
	{ // begin try (depth: 1)
		{
			goto IL_031d;
		}

IL_02fe:
		{
			KeyValuePair_2_t1415650006  L_104 = Enumerator_get_Current_m585116751((&V_12), /*hidden argument*/Enumerator_get_Current_m585116751_RuntimeMethod_var);
			V_11 = L_104;
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
			Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral4071089341, /*hidden argument*/NULL);
			PBXFileReference_t491780957 * L_105 = V_4;
			KeyValuePair_2_t1415650006  L_106 = V_11;
			bool L_107 = ___weak4;
			XCProject_BuildAddFile_m2061606299(__this, L_105, L_106, L_107, /*hidden argument*/NULL);
		}

IL_031d:
		{
			bool L_108 = Enumerator_MoveNext_m3699906100((&V_12), /*hidden argument*/Enumerator_MoveNext_m3699906100_RuntimeMethod_var);
			if (L_108)
			{
				goto IL_02fe;
			}
		}

IL_0329:
		{
			IL2CPP_LEAVE(0x33C, FINALLY_032e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_032e;
	}

FINALLY_032e:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m2093597648((&V_12), /*hidden argument*/Enumerator_Dispose_m2093597648_RuntimeMethod_var);
		IL2CPP_END_FINALLY(814)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(814)
	{
		IL2CPP_JUMP_TBL(0x33C, IL_033c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_033c:
	{
		goto IL_040c;
	}

IL_0341:
	{
		PBXDictionary_1_t2932651977 * L_109 = XCProject_get_sourcesBuildPhases_m3700393290(__this, /*hidden argument*/NULL);
		NullCheck(L_109);
		Enumerator_t1927377176  L_110 = Dictionary_2_GetEnumerator_m951612022(L_109, /*hidden argument*/Dictionary_2_GetEnumerator_m951612022_RuntimeMethod_var);
		V_14 = L_110;
	}

IL_034e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0372;
		}

IL_0353:
		{
			KeyValuePair_2_t2370866568  L_111 = Enumerator_get_Current_m2899533568((&V_14), /*hidden argument*/Enumerator_get_Current_m2899533568_RuntimeMethod_var);
			V_13 = L_111;
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
			Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral4093921082, /*hidden argument*/NULL);
			PBXFileReference_t491780957 * L_112 = V_4;
			KeyValuePair_2_t2370866568  L_113 = V_13;
			bool L_114 = ___weak4;
			XCProject_BuildAddFile_m1056009297(__this, L_112, L_113, L_114, /*hidden argument*/NULL);
		}

IL_0372:
		{
			bool L_115 = Enumerator_MoveNext_m1034167961((&V_14), /*hidden argument*/Enumerator_MoveNext_m1034167961_RuntimeMethod_var);
			if (L_115)
			{
				goto IL_0353;
			}
		}

IL_037e:
		{
			IL2CPP_LEAVE(0x391, FINALLY_0383);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0383;
	}

FINALLY_0383:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m754445729((&V_14), /*hidden argument*/Enumerator_Dispose_m754445729_RuntimeMethod_var);
		IL2CPP_END_FINALLY(899)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(899)
	{
		IL2CPP_JUMP_TBL(0x391, IL_0391)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0391:
	{
		goto IL_040c;
	}

IL_0396:
	{
		PBXDictionary_1_t2617057394 * L_116 = XCProject_get_copyBuildPhases_m3593876452(__this, /*hidden argument*/NULL);
		NullCheck(L_116);
		Enumerator_t1611782593  L_117 = Dictionary_2_GetEnumerator_m2675864505(L_116, /*hidden argument*/Dictionary_2_GetEnumerator_m2675864505_RuntimeMethod_var);
		V_16 = L_117;
	}

IL_03a3:
	try
	{ // begin try (depth: 1)
		{
			goto IL_03c7;
		}

IL_03a8:
		{
			KeyValuePair_2_t2055271985  L_118 = Enumerator_get_Current_m1936049740((&V_16), /*hidden argument*/Enumerator_get_Current_m1936049740_RuntimeMethod_var);
			V_15 = L_118;
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
			Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral4027760774, /*hidden argument*/NULL);
			PBXFileReference_t491780957 * L_119 = V_4;
			KeyValuePair_2_t2055271985  L_120 = V_15;
			bool L_121 = ___weak4;
			XCProject_BuildAddFile_m2193813989(__this, L_119, L_120, L_121, /*hidden argument*/NULL);
		}

IL_03c7:
		{
			bool L_122 = Enumerator_MoveNext_m3513999528((&V_16), /*hidden argument*/Enumerator_MoveNext_m3513999528_RuntimeMethod_var);
			if (L_122)
			{
				goto IL_03a8;
			}
		}

IL_03d3:
		{
			IL2CPP_LEAVE(0x3E6, FINALLY_03d8);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_03d8;
	}

FINALLY_03d8:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1838677398((&V_16), /*hidden argument*/Enumerator_Dispose_m1838677398_RuntimeMethod_var);
		IL2CPP_END_FINALLY(984)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(984)
	{
		IL2CPP_JUMP_TBL(0x3E6, IL_03e6)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_03e6:
	{
		goto IL_040c;
	}

IL_03eb:
	{
		String_t* L_123 = ___filePath0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_124 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral3168125321, L_123, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3752629331(NULL /*static, unused*/, L_124, /*hidden argument*/NULL);
		goto IL_040c;
	}

IL_0400:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3752629331(NULL /*static, unused*/, _stringLiteral3377412301, /*hidden argument*/NULL);
		return (PBXDictionary_t2078602241 *)NULL;
	}

IL_040c:
	{
		PBXDictionary_t2078602241 * L_125 = V_0;
		return L_125;
	}
}
// UnityEditor.XCodeEditor.PBXNativeTarget UnityEditor.XCodeEditor.XCProject::GetNativeTarget(System.String)
extern "C"  PBXNativeTarget_t793394962 * XCProject_GetNativeTarget_m2903336074 (XCProject_t3157646134 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCProject_GetNativeTarget_m2903336074_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PBXNativeTarget_t793394962 * V_0 = NULL;
	KeyValuePair_2_t2976323428  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Enumerator_t2532834036  V_2;
	memset(&V_2, 0, sizeof(V_2));
	String_t* V_3 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (PBXNativeTarget_t793394962 *)NULL;
		PBXDictionary_1_t3538108837 * L_0 = XCProject_get_nativeTargets_m2714694472(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Enumerator_t2532834036  L_1 = Dictionary_2_GetEnumerator_m170078521(L_0, /*hidden argument*/Dictionary_2_GetEnumerator_m170078521_RuntimeMethod_var);
		V_2 = L_1;
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0050;
		}

IL_0013:
		{
			KeyValuePair_2_t2976323428  L_2 = Enumerator_get_Current_m4204254741((&V_2), /*hidden argument*/Enumerator_get_Current_m4204254741_RuntimeMethod_var);
			V_1 = L_2;
			PBXNativeTarget_t793394962 * L_3 = KeyValuePair_2_get_Value_m543951955((&V_1), /*hidden argument*/KeyValuePair_2_get_Value_m543951955_RuntimeMethod_var);
			NullCheck(L_3);
			PBXDictionary_t2078602241 * L_4 = PBXObject_get_data_m910114329(L_3, /*hidden argument*/NULL);
			NullCheck(L_4);
			RuntimeObject * L_5 = Dictionary_2_get_Item_m3179620279(L_4, _stringLiteral62725243, /*hidden argument*/Dictionary_2_get_Item_m3179620279_RuntimeMethod_var);
			V_3 = ((String_t*)CastclassSealed((RuntimeObject*)L_5, String_t_il2cpp_TypeInfo_var));
			String_t* L_6 = V_3;
			String_t* L_7 = ___name0;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_8 = String_op_Equality_m920492651(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
			if (!L_8)
			{
				goto IL_0050;
			}
		}

IL_0043:
		{
			PBXNativeTarget_t793394962 * L_9 = KeyValuePair_2_get_Value_m543951955((&V_1), /*hidden argument*/KeyValuePair_2_get_Value_m543951955_RuntimeMethod_var);
			V_0 = L_9;
			goto IL_005c;
		}

IL_0050:
		{
			bool L_10 = Enumerator_MoveNext_m726921650((&V_2), /*hidden argument*/Enumerator_MoveNext_m726921650_RuntimeMethod_var);
			if (L_10)
			{
				goto IL_0013;
			}
		}

IL_005c:
		{
			IL2CPP_LEAVE(0x6F, FINALLY_0061);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0061;
	}

FINALLY_0061:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3454157324((&V_2), /*hidden argument*/Enumerator_Dispose_m3454157324_RuntimeMethod_var);
		IL2CPP_END_FINALLY(97)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(97)
	{
		IL2CPP_JUMP_TBL(0x6F, IL_006f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_006f:
	{
		PBXNativeTarget_t793394962 * L_11 = V_0;
		return L_11;
	}
}
// System.Int32 UnityEditor.XCodeEditor.XCProject::GetBuildActionMask()
extern "C"  int32_t XCProject_GetBuildActionMask_m3043263402 (XCProject_t3157646134 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCProject_GetBuildActionMask_m3043263402_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	KeyValuePair_2_t2055271985  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Enumerator_t1611782593  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = 0;
		PBXDictionary_1_t2617057394 * L_0 = XCProject_get_copyBuildPhases_m3593876452(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Enumerator_t1611782593  L_1 = Dictionary_2_GetEnumerator_m2675864505(L_0, /*hidden argument*/Dictionary_2_GetEnumerator_m2675864505_RuntimeMethod_var);
		V_2 = L_1;
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_003c;
		}

IL_0013:
		{
			KeyValuePair_2_t2055271985  L_2 = Enumerator_get_Current_m1936049740((&V_2), /*hidden argument*/Enumerator_get_Current_m1936049740_RuntimeMethod_var);
			V_1 = L_2;
			PBXCopyFilesBuildPhase_t4167310815 * L_3 = KeyValuePair_2_get_Value_m4174862611((&V_1), /*hidden argument*/KeyValuePair_2_get_Value_m4174862611_RuntimeMethod_var);
			NullCheck(L_3);
			PBXDictionary_t2078602241 * L_4 = PBXObject_get_data_m910114329(L_3, /*hidden argument*/NULL);
			NullCheck(L_4);
			RuntimeObject * L_5 = Dictionary_2_get_Item_m3179620279(L_4, _stringLiteral47317885, /*hidden argument*/Dictionary_2_get_Item_m3179620279_RuntimeMethod_var);
			V_0 = ((*(int32_t*)((int32_t*)UnBox(L_5, Int32_t2950945753_il2cpp_TypeInfo_var))));
			goto IL_0048;
		}

IL_003c:
		{
			bool L_6 = Enumerator_MoveNext_m3513999528((&V_2), /*hidden argument*/Enumerator_MoveNext_m3513999528_RuntimeMethod_var);
			if (L_6)
			{
				goto IL_0013;
			}
		}

IL_0048:
		{
			IL2CPP_LEAVE(0x5B, FINALLY_004d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_004d;
	}

FINALLY_004d:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1838677398((&V_2), /*hidden argument*/Enumerator_Dispose_m1838677398_RuntimeMethod_var);
		IL2CPP_END_FINALLY(77)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(77)
	{
		IL2CPP_JUMP_TBL(0x5B, IL_005b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_005b:
	{
		int32_t L_7 = V_0;
		return L_7;
	}
}
// UnityEditor.XCodeEditor.PBXCopyFilesBuildPhase UnityEditor.XCodeEditor.XCProject::AddEmbedFrameworkBuildPhase()
extern "C"  PBXCopyFilesBuildPhase_t4167310815 * XCProject_AddEmbedFrameworkBuildPhase_m2982154168 (XCProject_t3157646134 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCProject_AddEmbedFrameworkBuildPhase_m2982154168_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PBXCopyFilesBuildPhase_t4167310815 * V_0 = NULL;
	PBXNativeTarget_t793394962 * V_1 = NULL;
	KeyValuePair_2_t2055271985  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Enumerator_t1611782593  V_3;
	memset(&V_3, 0, sizeof(V_3));
	RuntimeObject * V_4 = NULL;
	String_t* V_5 = NULL;
	PBXCopyFilesBuildPhase_t4167310815 * V_6 = NULL;
	int32_t V_7 = 0;
	ArrayList_t2718874744 * V_8 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (PBXCopyFilesBuildPhase_t4167310815 *)NULL;
		PBXNativeTarget_t793394962 * L_0 = XCProject_GetNativeTarget_m2903336074(__this, _stringLiteral3505219263, /*hidden argument*/NULL);
		V_1 = L_0;
		PBXNativeTarget_t793394962 * L_1 = V_1;
		if (L_1)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral2034847608, /*hidden argument*/NULL);
		PBXCopyFilesBuildPhase_t4167310815 * L_2 = V_0;
		return L_2;
	}

IL_0020:
	{
		PBXDictionary_1_t2617057394 * L_3 = XCProject_get_copyBuildPhases_m3593876452(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Enumerator_t1611782593  L_4 = Dictionary_2_GetEnumerator_m2675864505(L_3, /*hidden argument*/Dictionary_2_GetEnumerator_m2675864505_RuntimeMethod_var);
		V_3 = L_4;
	}

IL_002c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0081;
		}

IL_0031:
		{
			KeyValuePair_2_t2055271985  L_5 = Enumerator_get_Current_m1936049740((&V_3), /*hidden argument*/Enumerator_get_Current_m1936049740_RuntimeMethod_var);
			V_2 = L_5;
			V_4 = NULL;
			PBXCopyFilesBuildPhase_t4167310815 * L_6 = KeyValuePair_2_get_Value_m4174862611((&V_2), /*hidden argument*/KeyValuePair_2_get_Value_m4174862611_RuntimeMethod_var);
			NullCheck(L_6);
			PBXDictionary_t2078602241 * L_7 = PBXObject_get_data_m910114329(L_6, /*hidden argument*/NULL);
			NullCheck(L_7);
			bool L_8 = Dictionary_2_TryGetValue_m2997259014(L_7, _stringLiteral62725243, (&V_4), /*hidden argument*/Dictionary_2_TryGetValue_m2997259014_RuntimeMethod_var);
			if (!L_8)
			{
				goto IL_0081;
			}
		}

IL_0059:
		{
			RuntimeObject * L_9 = V_4;
			V_5 = ((String_t*)CastclassSealed((RuntimeObject*)L_9, String_t_il2cpp_TypeInfo_var));
			String_t* L_10 = V_5;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_11 = String_op_Equality_m920492651(NULL /*static, unused*/, L_10, _stringLiteral3175314671, /*hidden argument*/NULL);
			if (!L_11)
			{
				goto IL_0081;
			}
		}

IL_0073:
		{
			PBXCopyFilesBuildPhase_t4167310815 * L_12 = KeyValuePair_2_get_Value_m4174862611((&V_2), /*hidden argument*/KeyValuePair_2_get_Value_m4174862611_RuntimeMethod_var);
			V_6 = L_12;
			IL2CPP_LEAVE(0xE3, FINALLY_0092);
		}

IL_0081:
		{
			bool L_13 = Enumerator_MoveNext_m3513999528((&V_3), /*hidden argument*/Enumerator_MoveNext_m3513999528_RuntimeMethod_var);
			if (L_13)
			{
				goto IL_0031;
			}
		}

IL_008d:
		{
			IL2CPP_LEAVE(0xA0, FINALLY_0092);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0092;
	}

FINALLY_0092:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1838677398((&V_3), /*hidden argument*/Enumerator_Dispose_m1838677398_RuntimeMethod_var);
		IL2CPP_END_FINALLY(146)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(146)
	{
		IL2CPP_JUMP_TBL(0xE3, IL_00e3)
		IL2CPP_JUMP_TBL(0xA0, IL_00a0)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_00a0:
	{
		int32_t L_14 = XCProject_GetBuildActionMask_m3043263402(__this, /*hidden argument*/NULL);
		V_7 = L_14;
		int32_t L_15 = V_7;
		PBXCopyFilesBuildPhase_t4167310815 * L_16 = (PBXCopyFilesBuildPhase_t4167310815 *)il2cpp_codegen_object_new(PBXCopyFilesBuildPhase_t4167310815_il2cpp_TypeInfo_var);
		PBXCopyFilesBuildPhase__ctor_m2022625091(L_16, L_15, /*hidden argument*/NULL);
		V_0 = L_16;
		PBXNativeTarget_t793394962 * L_17 = V_1;
		NullCheck(L_17);
		PBXDictionary_t2078602241 * L_18 = PBXObject_get_data_m910114329(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		RuntimeObject * L_19 = Dictionary_2_get_Item_m3179620279(L_18, _stringLiteral2345052496, /*hidden argument*/Dictionary_2_get_Item_m3179620279_RuntimeMethod_var);
		V_8 = ((ArrayList_t2718874744 *)CastclassClass((RuntimeObject*)L_19, ArrayList_t2718874744_il2cpp_TypeInfo_var));
		ArrayList_t2718874744 * L_20 = V_8;
		PBXCopyFilesBuildPhase_t4167310815 * L_21 = V_0;
		NullCheck(L_21);
		String_t* L_22 = PBXObject_get_guid_m2491609601(L_21, /*hidden argument*/NULL);
		NullCheck(L_20);
		VirtFuncInvoker1< int32_t, RuntimeObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_20, L_22);
		PBXDictionary_1_t2617057394 * L_23 = XCProject_get_copyBuildPhases_m3593876452(__this, /*hidden argument*/NULL);
		PBXCopyFilesBuildPhase_t4167310815 * L_24 = V_0;
		NullCheck(L_23);
		PBXDictionary_1_Add_m1199137400(L_23, L_24, /*hidden argument*/PBXDictionary_1_Add_m1199137400_RuntimeMethod_var);
		PBXCopyFilesBuildPhase_t4167310815 * L_25 = V_0;
		return L_25;
	}

IL_00e3:
	{
		PBXCopyFilesBuildPhase_t4167310815 * L_26 = V_6;
		return L_26;
	}
}
// System.Void UnityEditor.XCodeEditor.XCProject::AddEmbedFramework(System.String)
extern "C"  void XCProject_AddEmbedFramework_m1485538180 (XCProject_t3157646134 * __this, String_t* ___fileName0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCProject_AddEmbedFramework_m1485538180_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PBXFileReference_t491780957 * V_0 = NULL;
	PBXCopyFilesBuildPhase_t4167310815 * V_1 = NULL;
	PBXBuildFile_t2610392694 * V_2 = NULL;
	{
		String_t* L_0 = ___fileName0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral953705096, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___fileName0;
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1605229823_il2cpp_TypeInfo_var);
		String_t* L_3 = Path_GetFileName_m1354558116(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		PBXFileReference_t491780957 * L_4 = XCProject_GetFile_m1538714244(__this, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		PBXFileReference_t491780957 * L_5 = V_0;
		if (L_5)
		{
			goto IL_0034;
		}
	}
	{
		String_t* L_6 = ___fileName0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral1780090642, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		return;
	}

IL_0034:
	{
		PBXCopyFilesBuildPhase_t4167310815 * L_8 = XCProject_AddEmbedFrameworkBuildPhase_m2982154168(__this, /*hidden argument*/NULL);
		V_1 = L_8;
		PBXCopyFilesBuildPhase_t4167310815 * L_9 = V_1;
		if (L_9)
		{
			goto IL_004c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral2523233077, /*hidden argument*/NULL);
		return;
	}

IL_004c:
	{
		PBXFileReference_t491780957 * L_10 = V_0;
		PBXBuildFile_t2610392694 * L_11 = (PBXBuildFile_t2610392694 *)il2cpp_codegen_object_new(PBXBuildFile_t2610392694_il2cpp_TypeInfo_var);
		PBXBuildFile__ctor_m879448857(L_11, L_10, (bool)0, /*hidden argument*/NULL);
		V_2 = L_11;
		PBXBuildFile_t2610392694 * L_12 = V_2;
		NullCheck(L_12);
		PBXBuildFile_AddCodeSignOnCopy_m269553985(L_12, /*hidden argument*/NULL);
		PBXSortedDictionary_1_t2756258120 * L_13 = XCProject_get_buildFiles_m2028255916(__this, /*hidden argument*/NULL);
		PBXBuildFile_t2610392694 * L_14 = V_2;
		NullCheck(L_13);
		PBXSortedDictionary_1_Add_m3739564697(L_13, L_14, /*hidden argument*/PBXSortedDictionary_1_Add_m3739564697_RuntimeMethod_var);
		PBXCopyFilesBuildPhase_t4167310815 * L_15 = V_1;
		PBXBuildFile_t2610392694 * L_16 = V_2;
		NullCheck(L_15);
		PBXBuildPhase_AddBuildFile_m1828418701(L_15, L_16, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEditor.XCodeEditor.XCProject::BuildAddFile(UnityEditor.XCodeEditor.PBXFileReference,System.Collections.Generic.KeyValuePair`2<System.String,UnityEditor.XCodeEditor.PBXFrameworksBuildPhase>,System.Boolean)
extern "C"  void XCProject_BuildAddFile_m2837910453 (XCProject_t3157646134 * __this, PBXFileReference_t491780957 * ___fileReference0, KeyValuePair_2_t3858289876  ___currentObject1, bool ___weak2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCProject_BuildAddFile_m2837910453_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PBXBuildFile_t2610392694 * V_0 = NULL;
	{
		PBXFileReference_t491780957 * L_0 = ___fileReference0;
		bool L_1 = ___weak2;
		PBXBuildFile_t2610392694 * L_2 = (PBXBuildFile_t2610392694 *)il2cpp_codegen_object_new(PBXBuildFile_t2610392694_il2cpp_TypeInfo_var);
		PBXBuildFile__ctor_m879448857(L_2, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		PBXSortedDictionary_1_t2756258120 * L_3 = XCProject_get_buildFiles_m2028255916(__this, /*hidden argument*/NULL);
		PBXBuildFile_t2610392694 * L_4 = V_0;
		NullCheck(L_3);
		PBXSortedDictionary_1_Add_m3739564697(L_3, L_4, /*hidden argument*/PBXSortedDictionary_1_Add_m3739564697_RuntimeMethod_var);
		PBXFrameworksBuildPhase_t1675361410 * L_5 = KeyValuePair_2_get_Value_m2936696925((&___currentObject1), /*hidden argument*/KeyValuePair_2_get_Value_m2936696925_RuntimeMethod_var);
		PBXBuildFile_t2610392694 * L_6 = V_0;
		NullCheck(L_5);
		PBXBuildPhase_AddBuildFile_m1828418701(L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEditor.XCodeEditor.XCProject::BuildAddFile(UnityEditor.XCodeEditor.PBXFileReference,System.Collections.Generic.KeyValuePair`2<System.String,UnityEditor.XCodeEditor.PBXResourcesBuildPhase>,System.Boolean)
extern "C"  void XCProject_BuildAddFile_m2859561336 (XCProject_t3157646134 * __this, PBXFileReference_t491780957 * ___fileReference0, KeyValuePair_2_t1221294591  ___currentObject1, bool ___weak2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCProject_BuildAddFile_m2859561336_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PBXBuildFile_t2610392694 * V_0 = NULL;
	{
		PBXFileReference_t491780957 * L_0 = ___fileReference0;
		bool L_1 = ___weak2;
		PBXBuildFile_t2610392694 * L_2 = (PBXBuildFile_t2610392694 *)il2cpp_codegen_object_new(PBXBuildFile_t2610392694_il2cpp_TypeInfo_var);
		PBXBuildFile__ctor_m879448857(L_2, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		PBXSortedDictionary_1_t2756258120 * L_3 = XCProject_get_buildFiles_m2028255916(__this, /*hidden argument*/NULL);
		PBXBuildFile_t2610392694 * L_4 = V_0;
		NullCheck(L_3);
		PBXSortedDictionary_1_Add_m3739564697(L_3, L_4, /*hidden argument*/PBXSortedDictionary_1_Add_m3739564697_RuntimeMethod_var);
		PBXResourcesBuildPhase_t3333333421 * L_5 = KeyValuePair_2_get_Value_m887331346((&___currentObject1), /*hidden argument*/KeyValuePair_2_get_Value_m887331346_RuntimeMethod_var);
		PBXBuildFile_t2610392694 * L_6 = V_0;
		NullCheck(L_5);
		PBXBuildPhase_AddBuildFile_m1828418701(L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEditor.XCodeEditor.XCProject::BuildAddFile(UnityEditor.XCodeEditor.PBXFileReference,System.Collections.Generic.KeyValuePair`2<System.String,UnityEditor.XCodeEditor.PBXShellScriptBuildPhase>,System.Boolean)
extern "C"  void XCProject_BuildAddFile_m2061606299 (XCProject_t3157646134 * __this, PBXFileReference_t491780957 * ___fileReference0, KeyValuePair_2_t1415650006  ___currentObject1, bool ___weak2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCProject_BuildAddFile_m2061606299_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PBXBuildFile_t2610392694 * V_0 = NULL;
	{
		PBXFileReference_t491780957 * L_0 = ___fileReference0;
		bool L_1 = ___weak2;
		PBXBuildFile_t2610392694 * L_2 = (PBXBuildFile_t2610392694 *)il2cpp_codegen_object_new(PBXBuildFile_t2610392694_il2cpp_TypeInfo_var);
		PBXBuildFile__ctor_m879448857(L_2, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		PBXSortedDictionary_1_t2756258120 * L_3 = XCProject_get_buildFiles_m2028255916(__this, /*hidden argument*/NULL);
		PBXBuildFile_t2610392694 * L_4 = V_0;
		NullCheck(L_3);
		PBXSortedDictionary_1_Add_m3739564697(L_3, L_4, /*hidden argument*/PBXSortedDictionary_1_Add_m3739564697_RuntimeMethod_var);
		PBXShellScriptBuildPhase_t3527688836 * L_5 = KeyValuePair_2_get_Value_m4161882241((&___currentObject1), /*hidden argument*/KeyValuePair_2_get_Value_m4161882241_RuntimeMethod_var);
		PBXBuildFile_t2610392694 * L_6 = V_0;
		NullCheck(L_5);
		PBXBuildPhase_AddBuildFile_m1828418701(L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEditor.XCodeEditor.XCProject::BuildAddFile(UnityEditor.XCodeEditor.PBXFileReference,System.Collections.Generic.KeyValuePair`2<System.String,UnityEditor.XCodeEditor.PBXSourcesBuildPhase>,System.Boolean)
extern "C"  void XCProject_BuildAddFile_m1056009297 (XCProject_t3157646134 * __this, PBXFileReference_t491780957 * ___fileReference0, KeyValuePair_2_t2370866568  ___currentObject1, bool ___weak2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCProject_BuildAddFile_m1056009297_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PBXBuildFile_t2610392694 * V_0 = NULL;
	{
		PBXFileReference_t491780957 * L_0 = ___fileReference0;
		bool L_1 = ___weak2;
		PBXBuildFile_t2610392694 * L_2 = (PBXBuildFile_t2610392694 *)il2cpp_codegen_object_new(PBXBuildFile_t2610392694_il2cpp_TypeInfo_var);
		PBXBuildFile__ctor_m879448857(L_2, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		PBXSortedDictionary_1_t2756258120 * L_3 = XCProject_get_buildFiles_m2028255916(__this, /*hidden argument*/NULL);
		PBXBuildFile_t2610392694 * L_4 = V_0;
		NullCheck(L_3);
		PBXSortedDictionary_1_Add_m3739564697(L_3, L_4, /*hidden argument*/PBXSortedDictionary_1_Add_m3739564697_RuntimeMethod_var);
		PBXSourcesBuildPhase_t187938102 * L_5 = KeyValuePair_2_get_Value_m990072085((&___currentObject1), /*hidden argument*/KeyValuePair_2_get_Value_m990072085_RuntimeMethod_var);
		PBXBuildFile_t2610392694 * L_6 = V_0;
		NullCheck(L_5);
		PBXBuildPhase_AddBuildFile_m1828418701(L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEditor.XCodeEditor.XCProject::BuildAddFile(UnityEditor.XCodeEditor.PBXFileReference,System.Collections.Generic.KeyValuePair`2<System.String,UnityEditor.XCodeEditor.PBXCopyFilesBuildPhase>,System.Boolean)
extern "C"  void XCProject_BuildAddFile_m2193813989 (XCProject_t3157646134 * __this, PBXFileReference_t491780957 * ___fileReference0, KeyValuePair_2_t2055271985  ___currentObject1, bool ___weak2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCProject_BuildAddFile_m2193813989_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PBXBuildFile_t2610392694 * V_0 = NULL;
	{
		PBXFileReference_t491780957 * L_0 = ___fileReference0;
		bool L_1 = ___weak2;
		PBXBuildFile_t2610392694 * L_2 = (PBXBuildFile_t2610392694 *)il2cpp_codegen_object_new(PBXBuildFile_t2610392694_il2cpp_TypeInfo_var);
		PBXBuildFile__ctor_m879448857(L_2, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		PBXSortedDictionary_1_t2756258120 * L_3 = XCProject_get_buildFiles_m2028255916(__this, /*hidden argument*/NULL);
		PBXBuildFile_t2610392694 * L_4 = V_0;
		NullCheck(L_3);
		PBXSortedDictionary_1_Add_m3739564697(L_3, L_4, /*hidden argument*/PBXSortedDictionary_1_Add_m3739564697_RuntimeMethod_var);
		PBXCopyFilesBuildPhase_t4167310815 * L_5 = KeyValuePair_2_get_Value_m4174862611((&___currentObject1), /*hidden argument*/KeyValuePair_2_get_Value_m4174862611_RuntimeMethod_var);
		PBXBuildFile_t2610392694 * L_6 = V_0;
		NullCheck(L_5);
		PBXBuildPhase_AddBuildFile_m1828418701(L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEditor.XCodeEditor.XCProject::AddFolder(System.String,UnityEditor.XCodeEditor.PBXGroup,System.String[],System.Boolean,System.Boolean)
extern "C"  bool XCProject_AddFolder_m3825015009 (XCProject_t3157646134 * __this, String_t* ___folderPath0, PBXGroup_t2128436724 * ___parent1, StringU5BU5D_t1281789340* ___exclude2, bool ___recursive3, bool ___createBuildFile4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCProject_AddFolder_m3825015009_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DirectoryInfo_t35957480 * V_0 = NULL;
	PBXGroup_t2128436724 * V_1 = NULL;
	String_t* V_2 = NULL;
	StringU5BU5D_t1281789340* V_3 = NULL;
	int32_t V_4 = 0;
	String_t* V_5 = NULL;
	String_t* V_6 = NULL;
	StringU5BU5D_t1281789340* V_7 = NULL;
	int32_t V_8 = 0;
	{
		String_t* L_0 = ___folderPath0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral1520028804, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___folderPath0;
		bool L_3 = Directory_Exists_m1484791558(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0027;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral2481181957, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_0027:
	{
		String_t* L_4 = ___folderPath0;
		NullCheck(L_4);
		bool L_5 = String_EndsWith_m1901926500(L_4, _stringLiteral3541885379, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_004d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral2037147000, /*hidden argument*/NULL);
		String_t* L_6 = ___folderPath0;
		PBXGroup_t2128436724 * L_7 = ___parent1;
		StringU5BU5D_t1281789340* L_8 = ___exclude2;
		bool L_9 = ___createBuildFile4;
		bool L_10 = XCProject_AddLocFolder_m1925302028(__this, L_6, L_7, L_8, L_9, /*hidden argument*/NULL);
		return L_10;
	}

IL_004d:
	{
		String_t* L_11 = ___folderPath0;
		DirectoryInfo_t35957480 * L_12 = (DirectoryInfo_t35957480 *)il2cpp_codegen_object_new(DirectoryInfo_t35957480_il2cpp_TypeInfo_var);
		DirectoryInfo__ctor_m1000259829(L_12, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		StringU5BU5D_t1281789340* L_13 = ___exclude2;
		if (L_13)
		{
			goto IL_006c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral3203609901, /*hidden argument*/NULL);
		___exclude2 = ((StringU5BU5D_t1281789340*)SZArrayNew(StringU5BU5D_t1281789340_il2cpp_TypeInfo_var, (uint32_t)0));
	}

IL_006c:
	{
		PBXGroup_t2128436724 * L_14 = ___parent1;
		if (L_14)
		{
			goto IL_0084;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral3134809294, /*hidden argument*/NULL);
		PBXGroup_t2128436724 * L_15 = XCProject_get_rootGroup_m3903765605(__this, /*hidden argument*/NULL);
		___parent1 = L_15;
	}

IL_0084:
	{
		DirectoryInfo_t35957480 * L_16 = V_0;
		NullCheck(L_16);
		String_t* L_17 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.IO.FileSystemInfo::get_Name() */, L_16);
		PBXGroup_t2128436724 * L_18 = ___parent1;
		PBXGroup_t2128436724 * L_19 = XCProject_GetGroup_m2965518548(__this, L_17, (String_t*)NULL, L_18, /*hidden argument*/NULL);
		V_1 = L_19;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral103787962, /*hidden argument*/NULL);
		String_t* L_20 = ___folderPath0;
		StringU5BU5D_t1281789340* L_21 = Directory_GetDirectories_m1966820948(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		V_3 = L_21;
		V_4 = 0;
		goto IL_011c;
	}

IL_00ac:
	{
		StringU5BU5D_t1281789340* L_22 = V_3;
		int32_t L_23 = V_4;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		String_t* L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		V_2 = L_25;
		String_t* L_26 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral1241662146, L_26, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
		String_t* L_28 = V_2;
		NullCheck(L_28);
		bool L_29 = String_EndsWith_m1901926500(L_28, _stringLiteral3032477948, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_00f7;
		}
	}
	{
		String_t* L_30 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_31 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral392042043, L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3752629331(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		String_t* L_32 = V_2;
		PBXGroup_t2128436724 * L_33 = V_1;
		bool L_34 = ___createBuildFile4;
		XCProject_AddFile_m3957251854(__this, L_32, L_33, _stringLiteral901633645, L_34, (bool)0, /*hidden argument*/NULL);
		goto IL_0116;
	}

IL_00f7:
	{
		bool L_35 = ___recursive3;
		if (!L_35)
		{
			goto IL_0116;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral1976437474, /*hidden argument*/NULL);
		String_t* L_36 = V_2;
		PBXGroup_t2128436724 * L_37 = V_1;
		StringU5BU5D_t1281789340* L_38 = ___exclude2;
		bool L_39 = ___recursive3;
		bool L_40 = ___createBuildFile4;
		XCProject_AddFolder_m3825015009(__this, L_36, L_37, L_38, L_39, L_40, /*hidden argument*/NULL);
	}

IL_0116:
	{
		int32_t L_41 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_41, (int32_t)1));
	}

IL_011c:
	{
		int32_t L_42 = V_4;
		StringU5BU5D_t1281789340* L_43 = V_3;
		NullCheck(L_43);
		if ((((int32_t)L_42) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_43)->max_length)))))))
		{
			goto IL_00ac;
		}
	}
	{
		StringU5BU5D_t1281789340* L_44 = ___exclude2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_45 = String_Join_m2050845953(NULL /*static, unused*/, _stringLiteral3452614612, L_44, /*hidden argument*/NULL);
		String_t* L_46 = String_Format_m2844511972(NULL /*static, unused*/, _stringLiteral628085470, L_45, /*hidden argument*/NULL);
		V_5 = L_46;
		String_t* L_47 = ___folderPath0;
		StringU5BU5D_t1281789340* L_48 = Directory_GetFiles_m932257245(NULL /*static, unused*/, L_47, /*hidden argument*/NULL);
		V_7 = L_48;
		V_8 = 0;
		goto IL_0189;
	}

IL_014d:
	{
		StringU5BU5D_t1281789340* L_49 = V_7;
		int32_t L_50 = V_8;
		NullCheck(L_49);
		int32_t L_51 = L_50;
		String_t* L_52 = (L_49)->GetAt(static_cast<il2cpp_array_size_t>(L_51));
		V_6 = L_52;
		String_t* L_53 = V_6;
		String_t* L_54 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Regex_t3657309853_il2cpp_TypeInfo_var);
		bool L_55 = Regex_IsMatch_m3266004395(NULL /*static, unused*/, L_53, L_54, /*hidden argument*/NULL);
		if (!L_55)
		{
			goto IL_0167;
		}
	}
	{
		goto IL_0183;
	}

IL_0167:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral1605085672, /*hidden argument*/NULL);
		String_t* L_56 = V_6;
		PBXGroup_t2128436724 * L_57 = V_1;
		bool L_58 = ___createBuildFile4;
		XCProject_AddFile_m3957251854(__this, L_56, L_57, _stringLiteral901633645, L_58, (bool)0, /*hidden argument*/NULL);
	}

IL_0183:
	{
		int32_t L_59 = V_8;
		V_8 = ((int32_t)il2cpp_codegen_add((int32_t)L_59, (int32_t)1));
	}

IL_0189:
	{
		int32_t L_60 = V_8;
		StringU5BU5D_t1281789340* L_61 = V_7;
		NullCheck(L_61);
		if ((((int32_t)L_60) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_61)->max_length)))))))
		{
			goto IL_014d;
		}
	}
	{
		__this->set_modified_7((bool)1);
		bool L_62 = __this->get_modified_7();
		return L_62;
	}
}
// System.Boolean UnityEditor.XCodeEditor.XCProject::AddLocFolder(System.String,UnityEditor.XCodeEditor.PBXGroup,System.String[],System.Boolean)
extern "C"  bool XCProject_AddLocFolder_m1925302028 (XCProject_t3157646134 * __this, String_t* ___folderPath0, PBXGroup_t2128436724 * ___parent1, StringU5BU5D_t1281789340* ___exclude2, bool ___createBuildFile3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCProject_AddLocFolder_m1925302028_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DirectoryInfo_t35957480 * V_0 = NULL;
	Uri_t100236324 * V_1 = NULL;
	Uri_t100236324 * V_2 = NULL;
	String_t* V_3 = NULL;
	PBXGroup_t2128436724 * V_4 = NULL;
	String_t* V_5 = NULL;
	String_t* V_6 = NULL;
	String_t* V_7 = NULL;
	String_t* V_8 = NULL;
	StringU5BU5D_t1281789340* V_9 = NULL;
	int32_t V_10 = 0;
	PBXVariantGroup_t2964284507 * V_11 = NULL;
	{
		String_t* L_0 = ___folderPath0;
		DirectoryInfo_t35957480 * L_1 = (DirectoryInfo_t35957480 *)il2cpp_codegen_object_new(DirectoryInfo_t35957480_il2cpp_TypeInfo_var);
		DirectoryInfo__ctor_m1000259829(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		StringU5BU5D_t1281789340* L_2 = ___exclude2;
		if (L_2)
		{
			goto IL_0015;
		}
	}
	{
		___exclude2 = ((StringU5BU5D_t1281789340*)SZArrayNew(StringU5BU5D_t1281789340_il2cpp_TypeInfo_var, (uint32_t)0));
	}

IL_0015:
	{
		PBXGroup_t2128436724 * L_3 = ___parent1;
		if (L_3)
		{
			goto IL_0023;
		}
	}
	{
		PBXGroup_t2128436724 * L_4 = XCProject_get_rootGroup_m3903765605(__this, /*hidden argument*/NULL);
		___parent1 = L_4;
	}

IL_0023:
	{
		FileInfo_t1169991790 * L_5 = __this->get_projectFileInfo_5();
		NullCheck(L_5);
		String_t* L_6 = FileInfo_get_DirectoryName_m576702270(L_5, /*hidden argument*/NULL);
		Uri_t100236324 * L_7 = (Uri_t100236324 *)il2cpp_codegen_object_new(Uri_t100236324_il2cpp_TypeInfo_var);
		Uri__ctor_m800430703(L_7, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		String_t* L_8 = ___folderPath0;
		Uri_t100236324 * L_9 = (Uri_t100236324 *)il2cpp_codegen_object_new(Uri_t100236324_il2cpp_TypeInfo_var);
		Uri__ctor_m800430703(L_9, L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		Uri_t100236324 * L_10 = V_1;
		Uri_t100236324 * L_11 = V_2;
		NullCheck(L_10);
		Uri_t100236324 * L_12 = Uri_MakeRelativeUri_m3468717094(L_10, L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		String_t* L_13 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_12);
		V_3 = L_13;
		DirectoryInfo_t35957480 * L_14 = V_0;
		NullCheck(L_14);
		String_t* L_15 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.IO.FileSystemInfo::get_Name() */, L_14);
		String_t* L_16 = V_3;
		PBXGroup_t2128436724 * L_17 = ___parent1;
		PBXGroup_t2128436724 * L_18 = XCProject_GetGroup_m2965518548(__this, L_15, L_16, L_17, /*hidden argument*/NULL);
		V_4 = L_18;
		DirectoryInfo_t35957480 * L_19 = V_0;
		NullCheck(L_19);
		String_t* L_20 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.IO.FileSystemInfo::get_Name() */, L_19);
		V_5 = L_20;
		String_t* L_21 = V_5;
		String_t* L_22 = V_5;
		NullCheck(L_22);
		int32_t L_23 = String_get_Length_m3847582255(L_22, /*hidden argument*/NULL);
		NullCheck(_stringLiteral3541885379);
		int32_t L_24 = String_get_Length_m3847582255(_stringLiteral3541885379, /*hidden argument*/NULL);
		NullCheck(L_21);
		String_t* L_25 = String_Substring_m1610150815(L_21, 0, ((int32_t)il2cpp_codegen_subtract((int32_t)L_23, (int32_t)L_24)), /*hidden argument*/NULL);
		V_6 = L_25;
		PBXProject_t4035248171 * L_26 = XCProject_get_project_m2578328735(__this, /*hidden argument*/NULL);
		String_t* L_27 = V_6;
		NullCheck(L_26);
		PBXProject_AddRegion_m58745414(L_26, L_27, /*hidden argument*/NULL);
		StringU5BU5D_t1281789340* L_28 = ___exclude2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_29 = String_Join_m2050845953(NULL /*static, unused*/, _stringLiteral3452614612, L_28, /*hidden argument*/NULL);
		String_t* L_30 = String_Format_m2844511972(NULL /*static, unused*/, _stringLiteral628085470, L_29, /*hidden argument*/NULL);
		V_7 = L_30;
		String_t* L_31 = ___folderPath0;
		StringU5BU5D_t1281789340* L_32 = Directory_GetFiles_m932257245(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		V_9 = L_32;
		V_10 = 0;
		goto IL_010e;
	}

IL_00b0:
	{
		StringU5BU5D_t1281789340* L_33 = V_9;
		int32_t L_34 = V_10;
		NullCheck(L_33);
		int32_t L_35 = L_34;
		String_t* L_36 = (L_33)->GetAt(static_cast<il2cpp_array_size_t>(L_35));
		V_8 = L_36;
		String_t* L_37 = V_8;
		String_t* L_38 = V_7;
		IL2CPP_RUNTIME_CLASS_INIT(Regex_t3657309853_il2cpp_TypeInfo_var);
		bool L_39 = Regex_IsMatch_m3266004395(NULL /*static, unused*/, L_37, L_38, /*hidden argument*/NULL);
		if (!L_39)
		{
			goto IL_00ca;
		}
	}
	{
		goto IL_0108;
	}

IL_00ca:
	{
		String_t* L_40 = V_8;
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1605229823_il2cpp_TypeInfo_var);
		String_t* L_41 = Path_GetFileName_m1354558116(NULL /*static, unused*/, L_40, /*hidden argument*/NULL);
		PBXVariantGroup_t2964284507 * L_42 = (PBXVariantGroup_t2964284507 *)il2cpp_codegen_object_new(PBXVariantGroup_t2964284507_il2cpp_TypeInfo_var);
		PBXVariantGroup__ctor_m2891305890(L_42, L_41, (String_t*)NULL, _stringLiteral3589484046, /*hidden argument*/NULL);
		V_11 = L_42;
		PBXDictionary_1_t1414031086 * L_43 = XCProject_get_variantGroups_m3960773799(__this, /*hidden argument*/NULL);
		PBXVariantGroup_t2964284507 * L_44 = V_11;
		NullCheck(L_43);
		PBXDictionary_1_Add_m3289887501(L_43, L_44, /*hidden argument*/PBXDictionary_1_Add_m3289887501_RuntimeMethod_var);
		PBXGroup_t2128436724 * L_45 = V_4;
		PBXVariantGroup_t2964284507 * L_46 = V_11;
		NullCheck(L_45);
		PBXGroup_AddChild_m3690815735(L_45, L_46, /*hidden argument*/NULL);
		String_t* L_47 = V_8;
		PBXVariantGroup_t2964284507 * L_48 = V_11;
		bool L_49 = ___createBuildFile3;
		XCProject_AddFile_m3957251854(__this, L_47, L_48, _stringLiteral3589484046, L_49, (bool)0, /*hidden argument*/NULL);
	}

IL_0108:
	{
		int32_t L_50 = V_10;
		V_10 = ((int32_t)il2cpp_codegen_add((int32_t)L_50, (int32_t)1));
	}

IL_010e:
	{
		int32_t L_51 = V_10;
		StringU5BU5D_t1281789340* L_52 = V_9;
		NullCheck(L_52);
		if ((((int32_t)L_51) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_52)->max_length)))))))
		{
			goto IL_00b0;
		}
	}
	{
		__this->set_modified_7((bool)1);
		bool L_53 = __this->get_modified_7();
		return L_53;
	}
}
// UnityEditor.XCodeEditor.PBXFileReference UnityEditor.XCodeEditor.XCProject::GetFile(System.String)
extern "C"  PBXFileReference_t491780957 * XCProject_GetFile_m1538714244 (XCProject_t3157646134 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCProject_GetFile_m1538714244_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t2674709423  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Enumerator_t3200245381  V_1;
	memset(&V_1, 0, sizeof(V_1));
	PBXFileReference_t491780957 * V_2 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return (PBXFileReference_t491780957 *)NULL;
	}

IL_000d:
	{
		PBXSortedDictionary_1_t637646383 * L_2 = XCProject_get_fileReferences_m858134601(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Enumerator_t3200245381  L_3 = SortedDictionary_2_GetEnumerator_m2931611635(L_2, /*hidden argument*/SortedDictionary_2_GetEnumerator_m2931611635_RuntimeMethod_var);
		V_1 = L_3;
	}

IL_0019:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0060;
		}

IL_001e:
		{
			KeyValuePair_2_t2674709423  L_4 = Enumerator_get_Current_m2415991694((&V_1), /*hidden argument*/Enumerator_get_Current_m2415991694_RuntimeMethod_var);
			V_0 = L_4;
			PBXFileReference_t491780957 * L_5 = KeyValuePair_2_get_Value_m3214895324((&V_0), /*hidden argument*/KeyValuePair_2_get_Value_m3214895324_RuntimeMethod_var);
			NullCheck(L_5);
			String_t* L_6 = PBXFileReference_get_name_m3005884813(L_5, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_7 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
			if (L_7)
			{
				goto IL_0060;
			}
		}

IL_003c:
		{
			PBXFileReference_t491780957 * L_8 = KeyValuePair_2_get_Value_m3214895324((&V_0), /*hidden argument*/KeyValuePair_2_get_Value_m3214895324_RuntimeMethod_var);
			NullCheck(L_8);
			String_t* L_9 = PBXFileReference_get_name_m3005884813(L_8, /*hidden argument*/NULL);
			String_t* L_10 = ___name0;
			NullCheck(L_9);
			int32_t L_11 = String_CompareTo_m3414379165(L_9, L_10, /*hidden argument*/NULL);
			if (L_11)
			{
				goto IL_0060;
			}
		}

IL_0053:
		{
			PBXFileReference_t491780957 * L_12 = KeyValuePair_2_get_Value_m3214895324((&V_0), /*hidden argument*/KeyValuePair_2_get_Value_m3214895324_RuntimeMethod_var);
			V_2 = L_12;
			IL2CPP_LEAVE(0x81, FINALLY_0071);
		}

IL_0060:
		{
			bool L_13 = Enumerator_MoveNext_m266277119((&V_1), /*hidden argument*/Enumerator_MoveNext_m266277119_RuntimeMethod_var);
			if (L_13)
			{
				goto IL_001e;
			}
		}

IL_006c:
		{
			IL2CPP_LEAVE(0x7F, FINALLY_0071);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0071;
	}

FINALLY_0071:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3852176452((&V_1), /*hidden argument*/Enumerator_Dispose_m3852176452_RuntimeMethod_var);
		IL2CPP_END_FINALLY(113)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(113)
	{
		IL2CPP_JUMP_TBL(0x81, IL_0081)
		IL2CPP_JUMP_TBL(0x7F, IL_007f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_007f:
	{
		return (PBXFileReference_t491780957 *)NULL;
	}

IL_0081:
	{
		PBXFileReference_t491780957 * L_14 = V_2;
		return L_14;
	}
}
// UnityEditor.XCodeEditor.PBXGroup UnityEditor.XCodeEditor.XCProject::GetGroup(System.String,System.String,UnityEditor.XCodeEditor.PBXGroup)
extern "C"  PBXGroup_t2128436724 * XCProject_GetGroup_m2965518548 (XCProject_t3157646134 * __this, String_t* ___name0, String_t* ___path1, PBXGroup_t2128436724 * ___parent2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCProject_GetGroup_m2965518548_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t16397894  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Enumerator_t541933852  V_1;
	memset(&V_1, 0, sizeof(V_1));
	PBXGroup_t2128436724 * V_2 = NULL;
	PBXGroup_t2128436724 * V_3 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return (PBXGroup_t2128436724 *)NULL;
	}

IL_000d:
	{
		PBXGroup_t2128436724 * L_2 = ___parent2;
		if (L_2)
		{
			goto IL_001b;
		}
	}
	{
		PBXGroup_t2128436724 * L_3 = XCProject_get_rootGroup_m3903765605(__this, /*hidden argument*/NULL);
		___parent2 = L_3;
	}

IL_001b:
	{
		PBXSortedDictionary_1_t2274302150 * L_4 = XCProject_get_groups_m1730659681(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Enumerator_t541933852  L_5 = SortedDictionary_2_GetEnumerator_m1969713601(L_4, /*hidden argument*/SortedDictionary_2_GetEnumerator_m1969713601_RuntimeMethod_var);
		V_1 = L_5;
	}

IL_0027:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00bb;
		}

IL_002c:
		{
			KeyValuePair_2_t16397894  L_6 = Enumerator_get_Current_m1763852423((&V_1), /*hidden argument*/Enumerator_get_Current_m1763852423_RuntimeMethod_var);
			V_0 = L_6;
			PBXGroup_t2128436724 * L_7 = KeyValuePair_2_get_Value_m3727113552((&V_0), /*hidden argument*/KeyValuePair_2_get_Value_m3727113552_RuntimeMethod_var);
			NullCheck(L_7);
			String_t* L_8 = PBXGroup_get_name_m1192634509(L_7, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_9 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
			if (!L_9)
			{
				goto IL_0085;
			}
		}

IL_004a:
		{
			PBXGroup_t2128436724 * L_10 = KeyValuePair_2_get_Value_m3727113552((&V_0), /*hidden argument*/KeyValuePair_2_get_Value_m3727113552_RuntimeMethod_var);
			NullCheck(L_10);
			String_t* L_11 = PBXGroup_get_path_m1501355754(L_10, /*hidden argument*/NULL);
			String_t* L_12 = ___name0;
			NullCheck(L_11);
			int32_t L_13 = String_CompareTo_m3414379165(L_11, L_12, /*hidden argument*/NULL);
			if (L_13)
			{
				goto IL_0080;
			}
		}

IL_0061:
		{
			PBXGroup_t2128436724 * L_14 = ___parent2;
			String_t* L_15 = KeyValuePair_2_get_Key_m2204949747((&V_0), /*hidden argument*/KeyValuePair_2_get_Key_m2204949747_RuntimeMethod_var);
			NullCheck(L_14);
			bool L_16 = PBXGroup_HasChild_m177685167(L_14, L_15, /*hidden argument*/NULL);
			if (!L_16)
			{
				goto IL_0080;
			}
		}

IL_0073:
		{
			PBXGroup_t2128436724 * L_17 = KeyValuePair_2_get_Value_m3727113552((&V_0), /*hidden argument*/KeyValuePair_2_get_Value_m3727113552_RuntimeMethod_var);
			V_2 = L_17;
			IL2CPP_LEAVE(0x104, FINALLY_00cc);
		}

IL_0080:
		{
			goto IL_00bb;
		}

IL_0085:
		{
			PBXGroup_t2128436724 * L_18 = KeyValuePair_2_get_Value_m3727113552((&V_0), /*hidden argument*/KeyValuePair_2_get_Value_m3727113552_RuntimeMethod_var);
			NullCheck(L_18);
			String_t* L_19 = PBXGroup_get_name_m1192634509(L_18, /*hidden argument*/NULL);
			String_t* L_20 = ___name0;
			NullCheck(L_19);
			int32_t L_21 = String_CompareTo_m3414379165(L_19, L_20, /*hidden argument*/NULL);
			if (L_21)
			{
				goto IL_00bb;
			}
		}

IL_009c:
		{
			PBXGroup_t2128436724 * L_22 = ___parent2;
			String_t* L_23 = KeyValuePair_2_get_Key_m2204949747((&V_0), /*hidden argument*/KeyValuePair_2_get_Key_m2204949747_RuntimeMethod_var);
			NullCheck(L_22);
			bool L_24 = PBXGroup_HasChild_m177685167(L_22, L_23, /*hidden argument*/NULL);
			if (!L_24)
			{
				goto IL_00bb;
			}
		}

IL_00ae:
		{
			PBXGroup_t2128436724 * L_25 = KeyValuePair_2_get_Value_m3727113552((&V_0), /*hidden argument*/KeyValuePair_2_get_Value_m3727113552_RuntimeMethod_var);
			V_2 = L_25;
			IL2CPP_LEAVE(0x104, FINALLY_00cc);
		}

IL_00bb:
		{
			bool L_26 = Enumerator_MoveNext_m1528297490((&V_1), /*hidden argument*/Enumerator_MoveNext_m1528297490_RuntimeMethod_var);
			if (L_26)
			{
				goto IL_002c;
			}
		}

IL_00c7:
		{
			IL2CPP_LEAVE(0xDA, FINALLY_00cc);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_00cc;
	}

FINALLY_00cc:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m4154903118((&V_1), /*hidden argument*/Enumerator_Dispose_m4154903118_RuntimeMethod_var);
		IL2CPP_END_FINALLY(204)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(204)
	{
		IL2CPP_JUMP_TBL(0x104, IL_0104)
		IL2CPP_JUMP_TBL(0xDA, IL_00da)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_00da:
	{
		String_t* L_27 = ___name0;
		String_t* L_28 = ___path1;
		PBXGroup_t2128436724 * L_29 = (PBXGroup_t2128436724 *)il2cpp_codegen_object_new(PBXGroup_t2128436724_il2cpp_TypeInfo_var);
		PBXGroup__ctor_m605567610(L_29, L_27, L_28, _stringLiteral901633645, /*hidden argument*/NULL);
		V_3 = L_29;
		PBXSortedDictionary_1_t2274302150 * L_30 = XCProject_get_groups_m1730659681(__this, /*hidden argument*/NULL);
		PBXGroup_t2128436724 * L_31 = V_3;
		NullCheck(L_30);
		PBXSortedDictionary_1_Add_m259638508(L_30, L_31, /*hidden argument*/PBXSortedDictionary_1_Add_m259638508_RuntimeMethod_var);
		PBXGroup_t2128436724 * L_32 = ___parent2;
		PBXGroup_t2128436724 * L_33 = V_3;
		NullCheck(L_32);
		PBXGroup_AddChild_m3690815735(L_32, L_33, /*hidden argument*/NULL);
		__this->set_modified_7((bool)1);
		PBXGroup_t2128436724 * L_34 = V_3;
		return L_34;
	}

IL_0104:
	{
		PBXGroup_t2128436724 * L_35 = V_2;
		return L_35;
	}
}
// System.Void UnityEditor.XCodeEditor.XCProject::ApplyMod(System.String)
extern "C"  void XCProject_ApplyMod_m3627033218 (XCProject_t3157646134 * __this, String_t* ___pbxmod0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCProject_ApplyMod_m3627033218_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	XCMod_t1014962890 * V_0 = NULL;
	RuntimeObject * V_1 = NULL;
	RuntimeObject* V_2 = NULL;
	RuntimeObject* V_3 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___pbxmod0;
		XCMod_t1014962890 * L_1 = (XCMod_t1014962890 *)il2cpp_codegen_object_new(XCMod_t1014962890_il2cpp_TypeInfo_var);
		XCMod__ctor_m1865170813(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		XCMod_t1014962890 * L_2 = V_0;
		NullCheck(L_2);
		ArrayList_t2718874744 * L_3 = XCMod_get_libs_m513448774(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		RuntimeObject* L_4 = VirtFuncInvoker0< RuntimeObject* >::Invoke(43 /* System.Collections.IEnumerator System.Collections.ArrayList::GetEnumerator() */, L_3);
		V_2 = L_4;
	}

IL_0013:
	try
	{ // begin try (depth: 1)
		{
			goto IL_002f;
		}

IL_0018:
		{
			RuntimeObject* L_5 = V_2;
			NullCheck(L_5);
			RuntimeObject * L_6 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_5);
			V_1 = L_6;
			RuntimeObject * L_7 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_8 = String_Concat_m904156431(NULL /*static, unused*/, _stringLiteral641450103, L_7, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
			Debug_Log_m4051431634(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		}

IL_002f:
		{
			RuntimeObject* L_9 = V_2;
			NullCheck(L_9);
			bool L_10 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_9);
			if (L_10)
			{
				goto IL_0018;
			}
		}

IL_003a:
		{
			IL2CPP_LEAVE(0x53, FINALLY_003f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_003f;
	}

FINALLY_003f:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_11 = V_2;
			RuntimeObject* L_12 = ((RuntimeObject*)IsInst((RuntimeObject*)L_11, IDisposable_t3640265483_il2cpp_TypeInfo_var));
			V_3 = L_12;
			if (!L_12)
			{
				goto IL_0052;
			}
		}

IL_004c:
		{
			RuntimeObject* L_13 = V_3;
			NullCheck(L_13);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, L_13);
		}

IL_0052:
		{
			IL2CPP_END_FINALLY(63)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(63)
	{
		IL2CPP_JUMP_TBL(0x53, IL_0053)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0053:
	{
		XCMod_t1014962890 * L_14 = V_0;
		XCProject_ApplyMod_m3992715734(__this, L_14, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEditor.XCodeEditor.XCProject::ApplyMod(UnityEditor.XCodeEditor.XCMod)
extern "C"  void XCProject_ApplyMod_m3992715734 (XCProject_t3157646134 * __this, XCMod_t1014962890 * ___mod0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCProject_ApplyMod_m3992715734_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PBXGroup_t2128436724 * V_0 = NULL;
	XCModFile_t1016819465 * V_1 = NULL;
	RuntimeObject* V_2 = NULL;
	String_t* V_3 = NULL;
	RuntimeObject* V_4 = NULL;
	PBXGroup_t2128436724 * V_5 = NULL;
	String_t* V_6 = NULL;
	RuntimeObject* V_7 = NULL;
	StringU5BU5D_t1281789340* V_8 = NULL;
	bool V_9 = false;
	String_t* V_10 = NULL;
	RuntimeObject* V_11 = NULL;
	String_t* V_12 = NULL;
	RuntimeObject* V_13 = NULL;
	String_t* V_14 = NULL;
	RuntimeObject* V_15 = NULL;
	String_t* V_16 = NULL;
	RuntimeObject* V_17 = NULL;
	String_t* V_18 = NULL;
	RuntimeObject* V_19 = NULL;
	String_t* V_20 = NULL;
	RuntimeObject* V_21 = NULL;
	String_t* V_22 = NULL;
	RuntimeObject* V_23 = NULL;
	String_t* V_24 = NULL;
	RuntimeObject* V_25 = NULL;
	String_t* V_26 = NULL;
	RuntimeObject* V_27 = NULL;
	String_t* V_28 = NULL;
	RuntimeObject* V_29 = NULL;
	RuntimeObject* V_30 = NULL;
	String_t* V_31 = NULL;
	RuntimeObject* V_32 = NULL;
	RuntimeObject* V_33 = NULL;
	String_t* V_34 = NULL;
	XCPlist_t2540119850 * V_35 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B13_0 = 0;
	{
		XCMod_t1014962890 * L_0 = ___mod0;
		NullCheck(L_0);
		String_t* L_1 = XCMod_get_group_m1801882381(L_0, /*hidden argument*/NULL);
		PBXGroup_t2128436724 * L_2 = XCProject_GetGroup_m2965518548(__this, L_1, (String_t*)NULL, (PBXGroup_t2128436724 *)NULL, /*hidden argument*/NULL);
		V_0 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral2853481058, /*hidden argument*/NULL);
		XCMod_t1014962890 * L_3 = ___mod0;
		NullCheck(L_3);
		ArrayList_t2718874744 * L_4 = XCMod_get_libs_m513448774(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		RuntimeObject* L_5 = VirtFuncInvoker0< RuntimeObject* >::Invoke(43 /* System.Collections.IEnumerator System.Collections.ArrayList::GetEnumerator() */, L_4);
		V_2 = L_5;
	}

IL_0025:
	try
	{ // begin try (depth: 1)
		{
			goto IL_006c;
		}

IL_002a:
		{
			RuntimeObject* L_6 = V_2;
			NullCheck(L_6);
			RuntimeObject * L_7 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_6);
			V_1 = ((XCModFile_t1016819465 *)CastclassClass((RuntimeObject*)L_7, XCModFile_t1016819465_il2cpp_TypeInfo_var));
			XCModFile_t1016819465 * L_8 = V_1;
			NullCheck(L_8);
			String_t* L_9 = XCModFile_get_filePath_m2182538701(L_8, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Path_t1605229823_il2cpp_TypeInfo_var);
			String_t* L_10 = Path_Combine_m3389272516(NULL /*static, unused*/, _stringLiteral1188464369, L_9, /*hidden argument*/NULL);
			V_3 = L_10;
			String_t* L_11 = V_3;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_12 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral2379681108, L_11, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
			Debug_Log_m4051431634(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
			String_t* L_13 = V_3;
			PBXGroup_t2128436724 * L_14 = V_0;
			XCModFile_t1016819465 * L_15 = V_1;
			NullCheck(L_15);
			bool L_16 = XCModFile_get_isWeak_m3536481063(L_15, /*hidden argument*/NULL);
			XCProject_AddFile_m3957251854(__this, L_13, L_14, _stringLiteral1430076019, (bool)1, L_16, /*hidden argument*/NULL);
		}

IL_006c:
		{
			RuntimeObject* L_17 = V_2;
			NullCheck(L_17);
			bool L_18 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_17);
			if (L_18)
			{
				goto IL_002a;
			}
		}

IL_0077:
		{
			IL2CPP_LEAVE(0x92, FINALLY_007c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_007c;
	}

FINALLY_007c:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_19 = V_2;
			RuntimeObject* L_20 = ((RuntimeObject*)IsInst((RuntimeObject*)L_19, IDisposable_t3640265483_il2cpp_TypeInfo_var));
			V_4 = L_20;
			if (!L_20)
			{
				goto IL_0091;
			}
		}

IL_008a:
		{
			RuntimeObject* L_21 = V_4;
			NullCheck(L_21);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, L_21);
		}

IL_0091:
		{
			IL2CPP_END_FINALLY(124)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(124)
	{
		IL2CPP_JUMP_TBL(0x92, IL_0092)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0092:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral1217928750, /*hidden argument*/NULL);
		PBXGroup_t2128436724 * L_22 = XCProject_GetGroup_m2965518548(__this, _stringLiteral2253480458, (String_t*)NULL, (PBXGroup_t2128436724 *)NULL, /*hidden argument*/NULL);
		V_5 = L_22;
		XCMod_t1014962890 * L_23 = ___mod0;
		NullCheck(L_23);
		ArrayList_t2718874744 * L_24 = XCMod_get_frameworks_m2568364667(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		RuntimeObject* L_25 = VirtFuncInvoker0< RuntimeObject* >::Invoke(43 /* System.Collections.IEnumerator System.Collections.ArrayList::GetEnumerator() */, L_24);
		V_7 = L_25;
	}

IL_00b8:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0115;
		}

IL_00bd:
		{
			RuntimeObject* L_26 = V_7;
			NullCheck(L_26);
			RuntimeObject * L_27 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_26);
			V_6 = ((String_t*)CastclassSealed((RuntimeObject*)L_27, String_t_il2cpp_TypeInfo_var));
			String_t* L_28 = V_6;
			CharU5BU5D_t3528271667* L_29 = ((CharU5BU5D_t3528271667*)SZArrayNew(CharU5BU5D_t3528271667_il2cpp_TypeInfo_var, (uint32_t)1));
			NullCheck(L_29);
			(L_29)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)58));
			NullCheck(L_28);
			StringU5BU5D_t1281789340* L_30 = String_Split_m3646115398(L_28, L_29, /*hidden argument*/NULL);
			V_8 = L_30;
			StringU5BU5D_t1281789340* L_31 = V_8;
			NullCheck(L_31);
			if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_31)->max_length))))) <= ((int32_t)1)))
			{
				goto IL_00ef;
			}
		}

IL_00e9:
		{
			G_B13_0 = 1;
			goto IL_00f0;
		}

IL_00ef:
		{
			G_B13_0 = 0;
		}

IL_00f0:
		{
			V_9 = (bool)G_B13_0;
			StringU5BU5D_t1281789340* L_32 = V_8;
			NullCheck(L_32);
			int32_t L_33 = 0;
			String_t* L_34 = (L_32)->GetAt(static_cast<il2cpp_array_size_t>(L_33));
			IL2CPP_RUNTIME_CLASS_INIT(Path_t1605229823_il2cpp_TypeInfo_var);
			String_t* L_35 = Path_Combine_m3389272516(NULL /*static, unused*/, _stringLiteral3642357633, L_34, /*hidden argument*/NULL);
			V_10 = L_35;
			String_t* L_36 = V_10;
			PBXGroup_t2128436724 * L_37 = V_5;
			bool L_38 = V_9;
			XCProject_AddFile_m3957251854(__this, L_36, L_37, _stringLiteral1430076019, (bool)1, L_38, /*hidden argument*/NULL);
		}

IL_0115:
		{
			RuntimeObject* L_39 = V_7;
			NullCheck(L_39);
			bool L_40 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_39);
			if (L_40)
			{
				goto IL_00bd;
			}
		}

IL_0121:
		{
			IL2CPP_LEAVE(0x13D, FINALLY_0126);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0126;
	}

FINALLY_0126:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_41 = V_7;
			RuntimeObject* L_42 = ((RuntimeObject*)IsInst((RuntimeObject*)L_41, IDisposable_t3640265483_il2cpp_TypeInfo_var));
			V_11 = L_42;
			if (!L_42)
			{
				goto IL_013c;
			}
		}

IL_0135:
		{
			RuntimeObject* L_43 = V_11;
			NullCheck(L_43);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, L_43);
		}

IL_013c:
		{
			IL2CPP_END_FINALLY(294)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(294)
	{
		IL2CPP_JUMP_TBL(0x13D, IL_013d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_013d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral3077977161, /*hidden argument*/NULL);
		XCMod_t1014962890 * L_44 = ___mod0;
		NullCheck(L_44);
		ArrayList_t2718874744 * L_45 = XCMod_get_files_m150690371(L_44, /*hidden argument*/NULL);
		NullCheck(L_45);
		RuntimeObject* L_46 = VirtFuncInvoker0< RuntimeObject* >::Invoke(43 /* System.Collections.IEnumerator System.Collections.ArrayList::GetEnumerator() */, L_45);
		V_13 = L_46;
	}

IL_0154:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0187;
		}

IL_0159:
		{
			RuntimeObject* L_47 = V_13;
			NullCheck(L_47);
			RuntimeObject * L_48 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_47);
			V_12 = ((String_t*)CastclassSealed((RuntimeObject*)L_48, String_t_il2cpp_TypeInfo_var));
			XCMod_t1014962890 * L_49 = ___mod0;
			NullCheck(L_49);
			String_t* L_50 = XCMod_get_path_m530774122(L_49, /*hidden argument*/NULL);
			String_t* L_51 = V_12;
			IL2CPP_RUNTIME_CLASS_INIT(Path_t1605229823_il2cpp_TypeInfo_var);
			String_t* L_52 = Path_Combine_m3389272516(NULL /*static, unused*/, L_50, L_51, /*hidden argument*/NULL);
			V_14 = L_52;
			String_t* L_53 = V_14;
			PBXGroup_t2128436724 * L_54 = V_0;
			XCProject_AddFile_m3957251854(__this, L_53, L_54, _stringLiteral901633645, (bool)1, (bool)0, /*hidden argument*/NULL);
		}

IL_0187:
		{
			RuntimeObject* L_55 = V_13;
			NullCheck(L_55);
			bool L_56 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_55);
			if (L_56)
			{
				goto IL_0159;
			}
		}

IL_0193:
		{
			IL2CPP_LEAVE(0x1AF, FINALLY_0198);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0198;
	}

FINALLY_0198:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_57 = V_13;
			RuntimeObject* L_58 = ((RuntimeObject*)IsInst((RuntimeObject*)L_57, IDisposable_t3640265483_il2cpp_TypeInfo_var));
			V_15 = L_58;
			if (!L_58)
			{
				goto IL_01ae;
			}
		}

IL_01a7:
		{
			RuntimeObject* L_59 = V_15;
			NullCheck(L_59);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, L_59);
		}

IL_01ae:
		{
			IL2CPP_END_FINALLY(408)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(408)
	{
		IL2CPP_JUMP_TBL(0x1AF, IL_01af)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_01af:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral2110842851, /*hidden argument*/NULL);
		XCMod_t1014962890 * L_60 = ___mod0;
		NullCheck(L_60);
		ArrayList_t2718874744 * L_61 = XCMod_get_embed_binaries_m3193459801(L_60, /*hidden argument*/NULL);
		if (!L_61)
		{
			goto IL_024f;
		}
	}
	{
		XCProject_overwriteBuildSetting_m3219624(__this, _stringLiteral649783399, _stringLiteral4114321725, _stringLiteral2705147582, /*hidden argument*/NULL);
		XCProject_overwriteBuildSetting_m3219624(__this, _stringLiteral649783399, _stringLiteral4114321725, _stringLiteral2678398999, /*hidden argument*/NULL);
		XCMod_t1014962890 * L_62 = ___mod0;
		NullCheck(L_62);
		ArrayList_t2718874744 * L_63 = XCMod_get_embed_binaries_m3193459801(L_62, /*hidden argument*/NULL);
		NullCheck(L_63);
		RuntimeObject* L_64 = VirtFuncInvoker0< RuntimeObject* >::Invoke(43 /* System.Collections.IEnumerator System.Collections.ArrayList::GetEnumerator() */, L_63);
		V_17 = L_64;
	}

IL_01fd:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0227;
		}

IL_0202:
		{
			RuntimeObject* L_65 = V_17;
			NullCheck(L_65);
			RuntimeObject * L_66 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_65);
			V_16 = ((String_t*)CastclassSealed((RuntimeObject*)L_66, String_t_il2cpp_TypeInfo_var));
			XCMod_t1014962890 * L_67 = ___mod0;
			NullCheck(L_67);
			String_t* L_68 = XCMod_get_path_m530774122(L_67, /*hidden argument*/NULL);
			String_t* L_69 = V_16;
			IL2CPP_RUNTIME_CLASS_INIT(Path_t1605229823_il2cpp_TypeInfo_var);
			String_t* L_70 = Path_Combine_m3389272516(NULL /*static, unused*/, L_68, L_69, /*hidden argument*/NULL);
			V_18 = L_70;
			String_t* L_71 = V_18;
			XCProject_AddEmbedFramework_m1485538180(__this, L_71, /*hidden argument*/NULL);
		}

IL_0227:
		{
			RuntimeObject* L_72 = V_17;
			NullCheck(L_72);
			bool L_73 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_72);
			if (L_73)
			{
				goto IL_0202;
			}
		}

IL_0233:
		{
			IL2CPP_LEAVE(0x24F, FINALLY_0238);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0238;
	}

FINALLY_0238:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_74 = V_17;
			RuntimeObject* L_75 = ((RuntimeObject*)IsInst((RuntimeObject*)L_74, IDisposable_t3640265483_il2cpp_TypeInfo_var));
			V_19 = L_75;
			if (!L_75)
			{
				goto IL_024e;
			}
		}

IL_0247:
		{
			RuntimeObject* L_76 = V_19;
			NullCheck(L_76);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, L_76);
		}

IL_024e:
		{
			IL2CPP_END_FINALLY(568)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(568)
	{
		IL2CPP_JUMP_TBL(0x24F, IL_024f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_024f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral1245592032, /*hidden argument*/NULL);
		XCMod_t1014962890 * L_77 = ___mod0;
		NullCheck(L_77);
		ArrayList_t2718874744 * L_78 = XCMod_get_folders_m1874296509(L_77, /*hidden argument*/NULL);
		NullCheck(L_78);
		RuntimeObject* L_79 = VirtFuncInvoker0< RuntimeObject* >::Invoke(43 /* System.Collections.IEnumerator System.Collections.ArrayList::GetEnumerator() */, L_78);
		V_21 = L_79;
	}

IL_0266:
	try
	{ // begin try (depth: 1)
		{
			goto IL_02be;
		}

IL_026b:
		{
			RuntimeObject* L_80 = V_21;
			NullCheck(L_80);
			RuntimeObject * L_81 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_80);
			V_20 = ((String_t*)CastclassSealed((RuntimeObject*)L_81, String_t_il2cpp_TypeInfo_var));
			String_t* L_82 = Application_get_dataPath_m4232621142(NULL /*static, unused*/, /*hidden argument*/NULL);
			String_t* L_83 = V_20;
			IL2CPP_RUNTIME_CLASS_INIT(Path_t1605229823_il2cpp_TypeInfo_var);
			String_t* L_84 = Path_Combine_m3389272516(NULL /*static, unused*/, L_82, L_83, /*hidden argument*/NULL);
			V_22 = L_84;
			String_t* L_85 = V_22;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_86 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral1542650607, L_85, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
			Debug_Log_m4051431634(NULL /*static, unused*/, L_86, /*hidden argument*/NULL);
			String_t* L_87 = V_22;
			PBXGroup_t2128436724 * L_88 = V_0;
			XCMod_t1014962890 * L_89 = ___mod0;
			NullCheck(L_89);
			ArrayList_t2718874744 * L_90 = XCMod_get_excludes_m1351011736(L_89, /*hidden argument*/NULL);
			RuntimeTypeHandle_t3027515415  L_91 = { reinterpret_cast<intptr_t> (String_t_0_0_0_var) };
			IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
			Type_t * L_92 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_91, /*hidden argument*/NULL);
			NullCheck(L_90);
			RuntimeArray * L_93 = VirtFuncInvoker1< RuntimeArray *, Type_t * >::Invoke(48 /* System.Array System.Collections.ArrayList::ToArray(System.Type) */, L_90, L_92);
			XCProject_AddFolder_m3825015009(__this, L_87, L_88, ((StringU5BU5D_t1281789340*)Castclass((RuntimeObject*)L_93, StringU5BU5D_t1281789340_il2cpp_TypeInfo_var)), (bool)1, (bool)1, /*hidden argument*/NULL);
		}

IL_02be:
		{
			RuntimeObject* L_94 = V_21;
			NullCheck(L_94);
			bool L_95 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_94);
			if (L_95)
			{
				goto IL_026b;
			}
		}

IL_02ca:
		{
			IL2CPP_LEAVE(0x2E6, FINALLY_02cf);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_02cf;
	}

FINALLY_02cf:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_96 = V_21;
			RuntimeObject* L_97 = ((RuntimeObject*)IsInst((RuntimeObject*)L_96, IDisposable_t3640265483_il2cpp_TypeInfo_var));
			V_23 = L_97;
			if (!L_97)
			{
				goto IL_02e5;
			}
		}

IL_02de:
		{
			RuntimeObject* L_98 = V_23;
			NullCheck(L_98);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, L_98);
		}

IL_02e5:
		{
			IL2CPP_END_FINALLY(719)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(719)
	{
		IL2CPP_JUMP_TBL(0x2E6, IL_02e6)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_02e6:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral2539445417, /*hidden argument*/NULL);
		XCMod_t1014962890 * L_99 = ___mod0;
		NullCheck(L_99);
		ArrayList_t2718874744 * L_100 = XCMod_get_headerpaths_m343671542(L_99, /*hidden argument*/NULL);
		NullCheck(L_100);
		RuntimeObject* L_101 = VirtFuncInvoker0< RuntimeObject* >::Invoke(43 /* System.Collections.IEnumerator System.Collections.ArrayList::GetEnumerator() */, L_100);
		V_25 = L_101;
	}

IL_02fd:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0358;
		}

IL_0302:
		{
			RuntimeObject* L_102 = V_25;
			NullCheck(L_102);
			RuntimeObject * L_103 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_102);
			V_24 = ((String_t*)CastclassSealed((RuntimeObject*)L_103, String_t_il2cpp_TypeInfo_var));
			String_t* L_104 = V_24;
			NullCheck(L_104);
			bool L_105 = String_Contains_m1147431944(L_104, _stringLiteral366178444, /*hidden argument*/NULL);
			if (!L_105)
			{
				goto IL_0340;
			}
		}

IL_0321:
		{
			String_t* L_106 = V_24;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_107 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral488582302, L_106, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
			Debug_Log_m4051431634(NULL /*static, unused*/, L_107, /*hidden argument*/NULL);
			String_t* L_108 = V_24;
			XCProject_AddHeaderSearchPaths_m2329174020(__this, L_108, /*hidden argument*/NULL);
			goto IL_0358;
		}

IL_0340:
		{
			XCMod_t1014962890 * L_109 = ___mod0;
			NullCheck(L_109);
			String_t* L_110 = XCMod_get_path_m530774122(L_109, /*hidden argument*/NULL);
			String_t* L_111 = V_24;
			IL2CPP_RUNTIME_CLASS_INIT(Path_t1605229823_il2cpp_TypeInfo_var);
			String_t* L_112 = Path_Combine_m3389272516(NULL /*static, unused*/, L_110, L_111, /*hidden argument*/NULL);
			V_26 = L_112;
			String_t* L_113 = V_26;
			XCProject_AddHeaderSearchPaths_m2329174020(__this, L_113, /*hidden argument*/NULL);
		}

IL_0358:
		{
			RuntimeObject* L_114 = V_25;
			NullCheck(L_114);
			bool L_115 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_114);
			if (L_115)
			{
				goto IL_0302;
			}
		}

IL_0364:
		{
			IL2CPP_LEAVE(0x380, FINALLY_0369);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0369;
	}

FINALLY_0369:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_116 = V_25;
			RuntimeObject* L_117 = ((RuntimeObject*)IsInst((RuntimeObject*)L_116, IDisposable_t3640265483_il2cpp_TypeInfo_var));
			V_27 = L_117;
			if (!L_117)
			{
				goto IL_037f;
			}
		}

IL_0378:
		{
			RuntimeObject* L_118 = V_27;
			NullCheck(L_118);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, L_118);
		}

IL_037f:
		{
			IL2CPP_END_FINALLY(873)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(873)
	{
		IL2CPP_JUMP_TBL(0x380, IL_0380)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0380:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral853674241, /*hidden argument*/NULL);
		XCMod_t1014962890 * L_119 = ___mod0;
		NullCheck(L_119);
		ArrayList_t2718874744 * L_120 = XCMod_get_compiler_flags_m333845873(L_119, /*hidden argument*/NULL);
		NullCheck(L_120);
		RuntimeObject* L_121 = VirtFuncInvoker0< RuntimeObject* >::Invoke(43 /* System.Collections.IEnumerator System.Collections.ArrayList::GetEnumerator() */, L_120);
		V_29 = L_121;
	}

IL_0397:
	try
	{ // begin try (depth: 1)
		{
			goto IL_03b3;
		}

IL_039c:
		{
			RuntimeObject* L_122 = V_29;
			NullCheck(L_122);
			RuntimeObject * L_123 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_122);
			V_28 = ((String_t*)CastclassSealed((RuntimeObject*)L_123, String_t_il2cpp_TypeInfo_var));
			String_t* L_124 = V_28;
			XCProject_AddOtherCFlags_m696039661(__this, L_124, /*hidden argument*/NULL);
		}

IL_03b3:
		{
			RuntimeObject* L_125 = V_29;
			NullCheck(L_125);
			bool L_126 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_125);
			if (L_126)
			{
				goto IL_039c;
			}
		}

IL_03bf:
		{
			IL2CPP_LEAVE(0x3DB, FINALLY_03c4);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_03c4;
	}

FINALLY_03c4:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_127 = V_29;
			RuntimeObject* L_128 = ((RuntimeObject*)IsInst((RuntimeObject*)L_127, IDisposable_t3640265483_il2cpp_TypeInfo_var));
			V_30 = L_128;
			if (!L_128)
			{
				goto IL_03da;
			}
		}

IL_03d3:
		{
			RuntimeObject* L_129 = V_30;
			NullCheck(L_129);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, L_129);
		}

IL_03da:
		{
			IL2CPP_END_FINALLY(964)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(964)
	{
		IL2CPP_JUMP_TBL(0x3DB, IL_03db)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_03db:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral4056293757, /*hidden argument*/NULL);
		XCMod_t1014962890 * L_130 = ___mod0;
		NullCheck(L_130);
		ArrayList_t2718874744 * L_131 = XCMod_get_linker_flags_m3777781269(L_130, /*hidden argument*/NULL);
		NullCheck(L_131);
		RuntimeObject* L_132 = VirtFuncInvoker0< RuntimeObject* >::Invoke(43 /* System.Collections.IEnumerator System.Collections.ArrayList::GetEnumerator() */, L_131);
		V_32 = L_132;
	}

IL_03f2:
	try
	{ // begin try (depth: 1)
		{
			goto IL_040e;
		}

IL_03f7:
		{
			RuntimeObject* L_133 = V_32;
			NullCheck(L_133);
			RuntimeObject * L_134 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_133);
			V_31 = ((String_t*)CastclassSealed((RuntimeObject*)L_134, String_t_il2cpp_TypeInfo_var));
			String_t* L_135 = V_31;
			XCProject_AddOtherLinkerFlags_m1772294907(__this, L_135, /*hidden argument*/NULL);
		}

IL_040e:
		{
			RuntimeObject* L_136 = V_32;
			NullCheck(L_136);
			bool L_137 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_136);
			if (L_137)
			{
				goto IL_03f7;
			}
		}

IL_041a:
		{
			IL2CPP_LEAVE(0x436, FINALLY_041f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_041f;
	}

FINALLY_041f:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_138 = V_32;
			RuntimeObject* L_139 = ((RuntimeObject*)IsInst((RuntimeObject*)L_138, IDisposable_t3640265483_il2cpp_TypeInfo_var));
			V_33 = L_139;
			if (!L_139)
			{
				goto IL_0435;
			}
		}

IL_042e:
		{
			RuntimeObject* L_140 = V_33;
			NullCheck(L_140);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, L_140);
		}

IL_0435:
		{
			IL2CPP_END_FINALLY(1055)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(1055)
	{
		IL2CPP_JUMP_TBL(0x436, IL_0436)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0436:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral1428942434, /*hidden argument*/NULL);
		String_t* L_141 = XCProject_get_projectRootPath_m3623540506(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_142 = String_Concat_m3937257545(NULL /*static, unused*/, L_141, _stringLiteral836390878, /*hidden argument*/NULL);
		V_34 = L_142;
		String_t* L_143 = V_34;
		XCPlist_t2540119850 * L_144 = (XCPlist_t2540119850 *)il2cpp_codegen_object_new(XCPlist_t2540119850_il2cpp_TypeInfo_var);
		XCPlist__ctor_m1809109823(L_144, L_143, /*hidden argument*/NULL);
		V_35 = L_144;
		XCPlist_t2540119850 * L_145 = V_35;
		XCMod_t1014962890 * L_146 = ___mod0;
		NullCheck(L_146);
		Hashtable_t1853889766 * L_147 = XCMod_get_plist_m1661036855(L_146, /*hidden argument*/NULL);
		NullCheck(L_145);
		XCPlist_Process_m1182457030(L_145, L_147, /*hidden argument*/NULL);
		XCProject_Consolidate_m609230681(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEditor.XCodeEditor.XCProject::Consolidate()
extern "C"  void XCProject_Consolidate_m609230681 (XCProject_t3157646134 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCProject_Consolidate_m609230681_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PBXDictionary_t2078602241 * V_0 = NULL;
	{
		PBXDictionary_t2078602241 * L_0 = (PBXDictionary_t2078602241 *)il2cpp_codegen_object_new(PBXDictionary_t2078602241_il2cpp_TypeInfo_var);
		PBXDictionary__ctor_m3541187054(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		PBXDictionary_t2078602241 * L_1 = V_0;
		PBXSortedDictionary_1_t2756258120 * L_2 = XCProject_get_buildFiles_m2028255916(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		PBXDictionary_Append_TisPBXBuildFile_t2610392694_m3797558854(L_1, L_2, /*hidden argument*/PBXDictionary_Append_TisPBXBuildFile_t2610392694_m3797558854_RuntimeMethod_var);
		PBXDictionary_t2078602241 * L_3 = V_0;
		PBXDictionary_1_t2617057394 * L_4 = XCProject_get_copyBuildPhases_m3593876452(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		PBXDictionary_Append_TisPBXCopyFilesBuildPhase_t4167310815_m945363416(L_3, L_4, /*hidden argument*/PBXDictionary_Append_TisPBXCopyFilesBuildPhase_t4167310815_m945363416_RuntimeMethod_var);
		PBXDictionary_t2078602241 * L_5 = V_0;
		PBXSortedDictionary_1_t637646383 * L_6 = XCProject_get_fileReferences_m858134601(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		PBXDictionary_Append_TisPBXFileReference_t491780957_m334418948(L_5, L_6, /*hidden argument*/PBXDictionary_Append_TisPBXFileReference_t491780957_m334418948_RuntimeMethod_var);
		PBXDictionary_t2078602241 * L_7 = V_0;
		PBXDictionary_1_t125107989 * L_8 = XCProject_get_frameworkBuildPhases_m3514233196(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		PBXDictionary_Append_TisPBXFrameworksBuildPhase_t1675361410_m876528981(L_7, L_8, /*hidden argument*/PBXDictionary_Append_TisPBXFrameworksBuildPhase_t1675361410_m876528981_RuntimeMethod_var);
		PBXDictionary_t2078602241 * L_9 = V_0;
		PBXSortedDictionary_1_t2274302150 * L_10 = XCProject_get_groups_m1730659681(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		PBXDictionary_Append_TisPBXGroup_t2128436724_m2862935796(L_9, L_10, /*hidden argument*/PBXDictionary_Append_TisPBXGroup_t2128436724_m2862935796_RuntimeMethod_var);
		PBXDictionary_t2078602241 * L_11 = V_0;
		PBXDictionary_1_t3538108837 * L_12 = XCProject_get_nativeTargets_m2714694472(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		PBXDictionary_Append_TisPBXNativeTarget_t793394962_m1547927114(L_11, L_12, /*hidden argument*/PBXDictionary_Append_TisPBXNativeTarget_t793394962_m1547927114_RuntimeMethod_var);
		PBXDictionary_t2078602241 * L_13 = V_0;
		PBXProject_t4035248171 * L_14 = XCProject_get_project_m2578328735(__this, /*hidden argument*/NULL);
		NullCheck(L_14);
		String_t* L_15 = PBXObject_get_guid_m2491609601(L_14, /*hidden argument*/NULL);
		PBXProject_t4035248171 * L_16 = XCProject_get_project_m2578328735(__this, /*hidden argument*/NULL);
		NullCheck(L_16);
		PBXDictionary_t2078602241 * L_17 = PBXObject_get_data_m910114329(L_16, /*hidden argument*/NULL);
		NullCheck(L_13);
		Dictionary_2_Add_m825500632(L_13, L_15, L_17, /*hidden argument*/Dictionary_2_Add_m825500632_RuntimeMethod_var);
		PBXDictionary_t2078602241 * L_18 = V_0;
		PBXDictionary_1_t1783080000 * L_19 = XCProject_get_resourcesBuildPhases_m2464200803(__this, /*hidden argument*/NULL);
		NullCheck(L_18);
		PBXDictionary_Append_TisPBXResourcesBuildPhase_t3333333421_m2729679594(L_18, L_19, /*hidden argument*/PBXDictionary_Append_TisPBXResourcesBuildPhase_t3333333421_m2729679594_RuntimeMethod_var);
		PBXDictionary_t2078602241 * L_20 = V_0;
		PBXDictionary_1_t1977435415 * L_21 = XCProject_get_shellScriptBuildPhases_m2311296572(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		PBXDictionary_Append_TisPBXShellScriptBuildPhase_t3527688836_m3716639730(L_20, L_21, /*hidden argument*/PBXDictionary_Append_TisPBXShellScriptBuildPhase_t3527688836_m3716639730_RuntimeMethod_var);
		PBXDictionary_t2078602241 * L_22 = V_0;
		PBXDictionary_1_t2932651977 * L_23 = XCProject_get_sourcesBuildPhases_m3700393290(__this, /*hidden argument*/NULL);
		NullCheck(L_22);
		PBXDictionary_Append_TisPBXSourcesBuildPhase_t187938102_m2147902702(L_22, L_23, /*hidden argument*/PBXDictionary_Append_TisPBXSourcesBuildPhase_t187938102_m2147902702_RuntimeMethod_var);
		PBXDictionary_t2078602241 * L_24 = V_0;
		PBXDictionary_1_t1414031086 * L_25 = XCProject_get_variantGroups_m3960773799(__this, /*hidden argument*/NULL);
		NullCheck(L_24);
		PBXDictionary_Append_TisPBXVariantGroup_t2964284507_m211501280(L_24, L_25, /*hidden argument*/PBXDictionary_Append_TisPBXVariantGroup_t2964284507_m211501280_RuntimeMethod_var);
		PBXDictionary_t2078602241 * L_26 = V_0;
		PBXDictionary_1_t4195780175 * L_27 = XCProject_get_buildConfigurations_m1437786018(__this, /*hidden argument*/NULL);
		NullCheck(L_26);
		PBXDictionary_Append_TisXCBuildConfiguration_t1451066300_m2304102353(L_26, L_27, /*hidden argument*/PBXDictionary_Append_TisXCBuildConfiguration_t1451066300_m2304102353_RuntimeMethod_var);
		PBXDictionary_t2078602241 * L_28 = V_0;
		PBXSortedDictionary_1_t1246395450 * L_29 = XCProject_get_configurationLists_m3369988788(__this, /*hidden argument*/NULL);
		NullCheck(L_28);
		PBXDictionary_Append_TisXCConfigurationList_t1100530024_m4062973500(L_28, L_29, /*hidden argument*/PBXDictionary_Append_TisXCConfigurationList_t1100530024_m4062973500_RuntimeMethod_var);
		PBXDictionary_t2078602241 * L_30 = V_0;
		__this->set__objects_1(L_30);
		V_0 = (PBXDictionary_t2078602241 *)NULL;
		return;
	}
}
// System.Void UnityEditor.XCodeEditor.XCProject::Backup()
extern "C"  void XCProject_Backup_m2128267110 (XCProject_t3157646134 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCProject_Backup_m2128267110_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		String_t* L_0 = XCProject_get_filePath_m4061641646(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1605229823_il2cpp_TypeInfo_var);
		String_t* L_1 = Path_Combine_m3389272516(NULL /*static, unused*/, L_0, _stringLiteral1755754488, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = V_0;
		bool L_3 = File_Exists_m3943585060(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0022;
		}
	}
	{
		String_t* L_4 = V_0;
		File_Delete_m321251800(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_0022:
	{
		String_t* L_5 = XCProject_get_filePath_m4061641646(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1605229823_il2cpp_TypeInfo_var);
		String_t* L_6 = Path_Combine_m3389272516(NULL /*static, unused*/, L_5, _stringLiteral2698730106, /*hidden argument*/NULL);
		String_t* L_7 = V_0;
		File_Copy_m497281780(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEditor.XCodeEditor.XCProject::DeleteExisting(System.String)
extern "C"  void XCProject_DeleteExisting_m829740160 (XCProject_t3157646134 * __this, String_t* ___path0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___path0;
		bool L_1 = File_Exists_m3943585060(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		String_t* L_2 = ___path0;
		File_Delete_m321251800(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_0011:
	{
		return;
	}
}
// System.Void UnityEditor.XCodeEditor.XCProject::CreateNewProject(UnityEditor.XCodeEditor.PBXDictionary,System.String)
extern "C"  void XCProject_CreateNewProject_m1548580596 (XCProject_t3157646134 * __this, PBXDictionary_t2078602241 * ___result0, String_t* ___path1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCProject_CreateNewProject_m1548580596_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PBXParser_t1614426892 * V_0 = NULL;
	StreamWriter_t1266378904 * V_1 = NULL;
	{
		PBXParser_t1614426892 * L_0 = (PBXParser_t1614426892 *)il2cpp_codegen_object_new(PBXParser_t1614426892_il2cpp_TypeInfo_var);
		PBXParser__ctor_m3197616242(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = ___path1;
		StreamWriter_t1266378904 * L_2 = File_CreateText_m1585655010(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		StreamWriter_t1266378904 * L_3 = V_1;
		PBXParser_t1614426892 * L_4 = V_0;
		PBXDictionary_t2078602241 * L_5 = ___result0;
		NullCheck(L_4);
		String_t* L_6 = PBXParser_Encode_m3998271658(L_4, L_5, (bool)1, /*hidden argument*/NULL);
		NullCheck(L_3);
		VirtActionInvoker1< String_t* >::Invoke(12 /* System.Void System.IO.TextWriter::Write(System.String) */, L_3, L_6);
		StreamWriter_t1266378904 * L_7 = V_1;
		NullCheck(L_7);
		VirtActionInvoker0::Invoke(7 /* System.Void System.IO.TextWriter::Close() */, L_7);
		return;
	}
}
// System.Void UnityEditor.XCodeEditor.XCProject::Save()
extern "C"  void XCProject_Save_m570185480 (XCProject_t3157646134 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XCProject_Save_m570185480_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PBXDictionary_t2078602241 * V_0 = NULL;
	String_t* V_1 = NULL;
	{
		PBXDictionary_t2078602241 * L_0 = (PBXDictionary_t2078602241 *)il2cpp_codegen_object_new(PBXDictionary_t2078602241_il2cpp_TypeInfo_var);
		PBXDictionary__ctor_m3541187054(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		PBXDictionary_t2078602241 * L_1 = V_0;
		int32_t L_2 = 1;
		RuntimeObject * L_3 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_1);
		Dictionary_2_Add_m825500632(L_1, _stringLiteral1673557266, L_3, /*hidden argument*/Dictionary_2_Add_m825500632_RuntimeMethod_var);
		PBXDictionary_t2078602241 * L_4 = V_0;
		PBXDictionary_t2078602241 * L_5 = (PBXDictionary_t2078602241 *)il2cpp_codegen_object_new(PBXDictionary_t2078602241_il2cpp_TypeInfo_var);
		PBXDictionary__ctor_m3541187054(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		Dictionary_2_Add_m825500632(L_4, _stringLiteral1718878004, L_5, /*hidden argument*/Dictionary_2_Add_m825500632_RuntimeMethod_var);
		PBXDictionary_t2078602241 * L_6 = V_0;
		int32_t L_7 = ((int32_t)46);
		RuntimeObject * L_8 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_6);
		Dictionary_2_Add_m825500632(L_6, _stringLiteral2789466730, L_8, /*hidden argument*/Dictionary_2_Add_m825500632_RuntimeMethod_var);
		XCProject_Consolidate_m609230681(__this, /*hidden argument*/NULL);
		PBXDictionary_t2078602241 * L_9 = V_0;
		PBXDictionary_t2078602241 * L_10 = __this->get__objects_1();
		NullCheck(L_9);
		Dictionary_2_Add_m825500632(L_9, _stringLiteral2866737197, L_10, /*hidden argument*/Dictionary_2_Add_m825500632_RuntimeMethod_var);
		PBXDictionary_t2078602241 * L_11 = V_0;
		String_t* L_12 = __this->get__rootObjectKey_3();
		NullCheck(L_11);
		Dictionary_2_Add_m825500632(L_11, _stringLiteral1726171766, L_12, /*hidden argument*/Dictionary_2_Add_m825500632_RuntimeMethod_var);
		String_t* L_13 = XCProject_get_filePath_m4061641646(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1605229823_il2cpp_TypeInfo_var);
		String_t* L_14 = Path_Combine_m3389272516(NULL /*static, unused*/, L_13, _stringLiteral2698730106, /*hidden argument*/NULL);
		V_1 = L_14;
		String_t* L_15 = V_1;
		XCProject_DeleteExisting_m829740160(__this, L_15, /*hidden argument*/NULL);
		PBXDictionary_t2078602241 * L_16 = V_0;
		String_t* L_17 = V_1;
		XCProject_CreateNewProject_m1548580596(__this, L_16, L_17, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEditor.XCodeEditor.XCProject::get_objects()
extern "C"  Dictionary_2_t2865362463 * XCProject_get_objects_m2048402756 (XCProject_t3157646134 * __this, const RuntimeMethod* method)
{
	{
		return (Dictionary_2_t2865362463 *)NULL;
	}
}
// System.Void UnityEditor.XCodeEditor.XCProject::Dispose()
extern "C"  void XCProject_Dispose_m1510269625 (XCProject_t3157646134 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void XUPorterJSON.MiniJSON::.ctor()
extern "C"  void MiniJSON__ctor_m2672551501 (MiniJSON_t211875846 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object XUPorterJSON.MiniJSON::jsonDecode(System.String)
extern "C"  RuntimeObject * MiniJSON_jsonDecode_m2285616255 (RuntimeObject * __this /* static, unused */, String_t* ___json0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_jsonDecode_m2285616255_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CharU5BU5D_t3528271667* V_0 = NULL;
	int32_t V_1 = 0;
	bool V_2 = false;
	RuntimeObject * V_3 = NULL;
	{
		String_t* L_0 = ___json0;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t211875846_il2cpp_TypeInfo_var);
		((MiniJSON_t211875846_StaticFields*)il2cpp_codegen_static_fields_for(MiniJSON_t211875846_il2cpp_TypeInfo_var))->set_lastDecode_14(L_0);
		String_t* L_1 = ___json0;
		if (!L_1)
		{
			goto IL_003b;
		}
	}
	{
		String_t* L_2 = ___json0;
		NullCheck(L_2);
		CharU5BU5D_t3528271667* L_3 = String_ToCharArray_m1492846834(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		V_1 = 0;
		V_2 = (bool)1;
		CharU5BU5D_t3528271667* L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t211875846_il2cpp_TypeInfo_var);
		RuntimeObject * L_5 = MiniJSON_parseValue_m3659675017(NULL /*static, unused*/, L_4, (&V_1), (&V_2), /*hidden argument*/NULL);
		V_3 = L_5;
		bool L_6 = V_2;
		if (!L_6)
		{
			goto IL_0033;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t211875846_il2cpp_TypeInfo_var);
		((MiniJSON_t211875846_StaticFields*)il2cpp_codegen_static_fields_for(MiniJSON_t211875846_il2cpp_TypeInfo_var))->set_lastErrorIndex_13((-1));
		goto IL_0039;
	}

IL_0033:
	{
		int32_t L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t211875846_il2cpp_TypeInfo_var);
		((MiniJSON_t211875846_StaticFields*)il2cpp_codegen_static_fields_for(MiniJSON_t211875846_il2cpp_TypeInfo_var))->set_lastErrorIndex_13(L_7);
	}

IL_0039:
	{
		RuntimeObject * L_8 = V_3;
		return L_8;
	}

IL_003b:
	{
		return NULL;
	}
}
// System.String XUPorterJSON.MiniJSON::jsonEncode(System.Object)
extern "C"  String_t* MiniJSON_jsonEncode_m1032995782 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___json0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_jsonEncode_m1032995782_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t * V_0 = NULL;
	bool V_1 = false;
	String_t* G_B3_0 = NULL;
	{
		StringBuilder_t * L_0 = (StringBuilder_t *)il2cpp_codegen_object_new(StringBuilder_t_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m2367297767(L_0, ((int32_t)2000), /*hidden argument*/NULL);
		V_0 = L_0;
		RuntimeObject * L_1 = ___json0;
		StringBuilder_t * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t211875846_il2cpp_TypeInfo_var);
		bool L_3 = MiniJSON_serializeValue_m2428040582(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_0024;
		}
	}
	{
		StringBuilder_t * L_5 = V_0;
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_5);
		G_B3_0 = L_6;
		goto IL_0025;
	}

IL_0024:
	{
		G_B3_0 = ((String_t*)(NULL));
	}

IL_0025:
	{
		return G_B3_0;
	}
}
// System.Boolean XUPorterJSON.MiniJSON::lastDecodeSuccessful()
extern "C"  bool MiniJSON_lastDecodeSuccessful_m3883034971 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_lastDecodeSuccessful_m3883034971_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t211875846_il2cpp_TypeInfo_var);
		int32_t L_0 = ((MiniJSON_t211875846_StaticFields*)il2cpp_codegen_static_fields_for(MiniJSON_t211875846_il2cpp_TypeInfo_var))->get_lastErrorIndex_13();
		return (bool)((((int32_t)L_0) == ((int32_t)(-1)))? 1 : 0);
	}
}
// System.Int32 XUPorterJSON.MiniJSON::getLastErrorIndex()
extern "C"  int32_t MiniJSON_getLastErrorIndex_m2235336515 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_getLastErrorIndex_m2235336515_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t211875846_il2cpp_TypeInfo_var);
		int32_t L_0 = ((MiniJSON_t211875846_StaticFields*)il2cpp_codegen_static_fields_for(MiniJSON_t211875846_il2cpp_TypeInfo_var))->get_lastErrorIndex_13();
		return L_0;
	}
}
// System.String XUPorterJSON.MiniJSON::getLastErrorSnippet()
extern "C"  String_t* MiniJSON_getLastErrorSnippet_m3000786183 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_getLastErrorSnippet_m3000786183_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t211875846_il2cpp_TypeInfo_var);
		int32_t L_0 = ((MiniJSON_t211875846_StaticFields*)il2cpp_codegen_static_fields_for(MiniJSON_t211875846_il2cpp_TypeInfo_var))->get_lastErrorIndex_13();
		if ((!(((uint32_t)L_0) == ((uint32_t)(-1)))))
		{
			goto IL_0011;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		return L_1;
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t211875846_il2cpp_TypeInfo_var);
		int32_t L_2 = ((MiniJSON_t211875846_StaticFields*)il2cpp_codegen_static_fields_for(MiniJSON_t211875846_il2cpp_TypeInfo_var))->get_lastErrorIndex_13();
		V_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_2, (int32_t)5));
		int32_t L_3 = ((MiniJSON_t211875846_StaticFields*)il2cpp_codegen_static_fields_for(MiniJSON_t211875846_il2cpp_TypeInfo_var))->get_lastErrorIndex_13();
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)((int32_t)15)));
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_002b;
		}
	}
	{
		V_0 = 0;
	}

IL_002b:
	{
		int32_t L_5 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t211875846_il2cpp_TypeInfo_var);
		String_t* L_6 = ((MiniJSON_t211875846_StaticFields*)il2cpp_codegen_static_fields_for(MiniJSON_t211875846_il2cpp_TypeInfo_var))->get_lastDecode_14();
		NullCheck(L_6);
		int32_t L_7 = String_get_Length_m3847582255(L_6, /*hidden argument*/NULL);
		if ((((int32_t)L_5) < ((int32_t)L_7)))
		{
			goto IL_0048;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t211875846_il2cpp_TypeInfo_var);
		String_t* L_8 = ((MiniJSON_t211875846_StaticFields*)il2cpp_codegen_static_fields_for(MiniJSON_t211875846_il2cpp_TypeInfo_var))->get_lastDecode_14();
		NullCheck(L_8);
		int32_t L_9 = String_get_Length_m3847582255(L_8, /*hidden argument*/NULL);
		V_1 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_9, (int32_t)1));
	}

IL_0048:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t211875846_il2cpp_TypeInfo_var);
		String_t* L_10 = ((MiniJSON_t211875846_StaticFields*)il2cpp_codegen_static_fields_for(MiniJSON_t211875846_il2cpp_TypeInfo_var))->get_lastDecode_14();
		int32_t L_11 = V_0;
		int32_t L_12 = V_1;
		int32_t L_13 = V_0;
		NullCheck(L_10);
		String_t* L_14 = String_Substring_m1610150815(L_10, L_11, ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_12, (int32_t)L_13)), (int32_t)1)), /*hidden argument*/NULL);
		return L_14;
	}
}
// System.Collections.Hashtable XUPorterJSON.MiniJSON::parseObject(System.Char[],System.Int32&)
extern "C"  Hashtable_t1853889766 * MiniJSON_parseObject_m2380755468 (RuntimeObject * __this /* static, unused */, CharU5BU5D_t3528271667* ___json0, int32_t* ___index1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_parseObject_m2380755468_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Hashtable_t1853889766 * V_0 = NULL;
	int32_t V_1 = 0;
	bool V_2 = false;
	String_t* V_3 = NULL;
	bool V_4 = false;
	RuntimeObject * V_5 = NULL;
	{
		Hashtable_t1853889766 * L_0 = (Hashtable_t1853889766 *)il2cpp_codegen_object_new(Hashtable_t1853889766_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1815022027(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		CharU5BU5D_t3528271667* L_1 = ___json0;
		int32_t* L_2 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t211875846_il2cpp_TypeInfo_var);
		MiniJSON_nextToken_m3582996375(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		V_2 = (bool)0;
		goto IL_008c;
	}

IL_0015:
	{
		CharU5BU5D_t3528271667* L_3 = ___json0;
		int32_t* L_4 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t211875846_il2cpp_TypeInfo_var);
		int32_t L_5 = MiniJSON_lookAhead_m3599256246(NULL /*static, unused*/, L_3, (*((int32_t*)L_4)), /*hidden argument*/NULL);
		V_1 = L_5;
		int32_t L_6 = V_1;
		if (L_6)
		{
			goto IL_0026;
		}
	}
	{
		return (Hashtable_t1853889766 *)NULL;
	}

IL_0026:
	{
		int32_t L_7 = V_1;
		if ((!(((uint32_t)L_7) == ((uint32_t)6))))
		{
			goto IL_003a;
		}
	}
	{
		CharU5BU5D_t3528271667* L_8 = ___json0;
		int32_t* L_9 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t211875846_il2cpp_TypeInfo_var);
		MiniJSON_nextToken_m3582996375(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		goto IL_008c;
	}

IL_003a:
	{
		int32_t L_10 = V_1;
		if ((!(((uint32_t)L_10) == ((uint32_t)2))))
		{
			goto IL_004b;
		}
	}
	{
		CharU5BU5D_t3528271667* L_11 = ___json0;
		int32_t* L_12 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t211875846_il2cpp_TypeInfo_var);
		MiniJSON_nextToken_m3582996375(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		Hashtable_t1853889766 * L_13 = V_0;
		return L_13;
	}

IL_004b:
	{
		CharU5BU5D_t3528271667* L_14 = ___json0;
		int32_t* L_15 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t211875846_il2cpp_TypeInfo_var);
		String_t* L_16 = MiniJSON_parseString_m2073376077(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		V_3 = L_16;
		String_t* L_17 = V_3;
		if (L_17)
		{
			goto IL_005b;
		}
	}
	{
		return (Hashtable_t1853889766 *)NULL;
	}

IL_005b:
	{
		CharU5BU5D_t3528271667* L_18 = ___json0;
		int32_t* L_19 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t211875846_il2cpp_TypeInfo_var);
		int32_t L_20 = MiniJSON_nextToken_m3582996375(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		V_1 = L_20;
		int32_t L_21 = V_1;
		if ((((int32_t)L_21) == ((int32_t)5)))
		{
			goto IL_006c;
		}
	}
	{
		return (Hashtable_t1853889766 *)NULL;
	}

IL_006c:
	{
		V_4 = (bool)1;
		CharU5BU5D_t3528271667* L_22 = ___json0;
		int32_t* L_23 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t211875846_il2cpp_TypeInfo_var);
		RuntimeObject * L_24 = MiniJSON_parseValue_m3659675017(NULL /*static, unused*/, L_22, L_23, (&V_4), /*hidden argument*/NULL);
		V_5 = L_24;
		bool L_25 = V_4;
		if (L_25)
		{
			goto IL_0083;
		}
	}
	{
		return (Hashtable_t1853889766 *)NULL;
	}

IL_0083:
	{
		Hashtable_t1853889766 * L_26 = V_0;
		String_t* L_27 = V_3;
		RuntimeObject * L_28 = V_5;
		NullCheck(L_26);
		VirtActionInvoker2< RuntimeObject *, RuntimeObject * >::Invoke(27 /* System.Void System.Collections.Hashtable::set_Item(System.Object,System.Object) */, L_26, L_27, L_28);
	}

IL_008c:
	{
		bool L_29 = V_2;
		if (!L_29)
		{
			goto IL_0015;
		}
	}
	{
		Hashtable_t1853889766 * L_30 = V_0;
		return L_30;
	}
}
// System.Collections.ArrayList XUPorterJSON.MiniJSON::parseArray(System.Char[],System.Int32&)
extern "C"  ArrayList_t2718874744 * MiniJSON_parseArray_m1439775387 (RuntimeObject * __this /* static, unused */, CharU5BU5D_t3528271667* ___json0, int32_t* ___index1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_parseArray_m1439775387_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ArrayList_t2718874744 * V_0 = NULL;
	bool V_1 = false;
	int32_t V_2 = 0;
	bool V_3 = false;
	RuntimeObject * V_4 = NULL;
	{
		ArrayList_t2718874744 * L_0 = (ArrayList_t2718874744 *)il2cpp_codegen_object_new(ArrayList_t2718874744_il2cpp_TypeInfo_var);
		ArrayList__ctor_m4254721275(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		CharU5BU5D_t3528271667* L_1 = ___json0;
		int32_t* L_2 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t211875846_il2cpp_TypeInfo_var);
		MiniJSON_nextToken_m3582996375(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		V_1 = (bool)0;
		goto IL_006c;
	}

IL_0015:
	{
		CharU5BU5D_t3528271667* L_3 = ___json0;
		int32_t* L_4 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t211875846_il2cpp_TypeInfo_var);
		int32_t L_5 = MiniJSON_lookAhead_m3599256246(NULL /*static, unused*/, L_3, (*((int32_t*)L_4)), /*hidden argument*/NULL);
		V_2 = L_5;
		int32_t L_6 = V_2;
		if (L_6)
		{
			goto IL_0026;
		}
	}
	{
		return (ArrayList_t2718874744 *)NULL;
	}

IL_0026:
	{
		int32_t L_7 = V_2;
		if ((!(((uint32_t)L_7) == ((uint32_t)6))))
		{
			goto IL_003a;
		}
	}
	{
		CharU5BU5D_t3528271667* L_8 = ___json0;
		int32_t* L_9 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t211875846_il2cpp_TypeInfo_var);
		MiniJSON_nextToken_m3582996375(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		goto IL_006c;
	}

IL_003a:
	{
		int32_t L_10 = V_2;
		if ((!(((uint32_t)L_10) == ((uint32_t)4))))
		{
			goto IL_004e;
		}
	}
	{
		CharU5BU5D_t3528271667* L_11 = ___json0;
		int32_t* L_12 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t211875846_il2cpp_TypeInfo_var);
		MiniJSON_nextToken_m3582996375(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		goto IL_0072;
	}

IL_004e:
	{
		V_3 = (bool)1;
		CharU5BU5D_t3528271667* L_13 = ___json0;
		int32_t* L_14 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t211875846_il2cpp_TypeInfo_var);
		RuntimeObject * L_15 = MiniJSON_parseValue_m3659675017(NULL /*static, unused*/, L_13, L_14, (&V_3), /*hidden argument*/NULL);
		V_4 = L_15;
		bool L_16 = V_3;
		if (L_16)
		{
			goto IL_0063;
		}
	}
	{
		return (ArrayList_t2718874744 *)NULL;
	}

IL_0063:
	{
		ArrayList_t2718874744 * L_17 = V_0;
		RuntimeObject * L_18 = V_4;
		NullCheck(L_17);
		VirtFuncInvoker1< int32_t, RuntimeObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_17, L_18);
	}

IL_006c:
	{
		bool L_19 = V_1;
		if (!L_19)
		{
			goto IL_0015;
		}
	}

IL_0072:
	{
		ArrayList_t2718874744 * L_20 = V_0;
		return L_20;
	}
}
// System.Object XUPorterJSON.MiniJSON::parseValue(System.Char[],System.Int32&,System.Boolean&)
extern "C"  RuntimeObject * MiniJSON_parseValue_m3659675017 (RuntimeObject * __this /* static, unused */, CharU5BU5D_t3528271667* ___json0, int32_t* ___index1, bool* ___success2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_parseValue_m3659675017_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		CharU5BU5D_t3528271667* L_0 = ___json0;
		int32_t* L_1 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t211875846_il2cpp_TypeInfo_var);
		int32_t L_2 = MiniJSON_lookAhead_m3599256246(NULL /*static, unused*/, L_0, (*((int32_t*)L_1)), /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = V_0;
		switch (L_3)
		{
			case 0:
			{
				goto IL_00a3;
			}
			case 1:
			{
				goto IL_0059;
			}
			case 2:
			{
				goto IL_00a8;
			}
			case 3:
			{
				goto IL_0061;
			}
			case 4:
			{
				goto IL_00a8;
			}
			case 5:
			{
				goto IL_00a8;
			}
			case 6:
			{
				goto IL_00a8;
			}
			case 7:
			{
				goto IL_0044;
			}
			case 8:
			{
				goto IL_004c;
			}
			case 9:
			{
				goto IL_0069;
			}
			case 10:
			{
				goto IL_0081;
			}
			case 11:
			{
				goto IL_0099;
			}
		}
	}
	{
		goto IL_00a8;
	}

IL_0044:
	{
		CharU5BU5D_t3528271667* L_4 = ___json0;
		int32_t* L_5 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t211875846_il2cpp_TypeInfo_var);
		String_t* L_6 = MiniJSON_parseString_m2073376077(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}

IL_004c:
	{
		CharU5BU5D_t3528271667* L_7 = ___json0;
		int32_t* L_8 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t211875846_il2cpp_TypeInfo_var);
		double L_9 = MiniJSON_parseNumber_m3876524415(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		double L_10 = L_9;
		RuntimeObject * L_11 = Box(Double_t594665363_il2cpp_TypeInfo_var, &L_10);
		return L_11;
	}

IL_0059:
	{
		CharU5BU5D_t3528271667* L_12 = ___json0;
		int32_t* L_13 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t211875846_il2cpp_TypeInfo_var);
		Hashtable_t1853889766 * L_14 = MiniJSON_parseObject_m2380755468(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		return L_14;
	}

IL_0061:
	{
		CharU5BU5D_t3528271667* L_15 = ___json0;
		int32_t* L_16 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t211875846_il2cpp_TypeInfo_var);
		ArrayList_t2718874744 * L_17 = MiniJSON_parseArray_m1439775387(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
		return L_17;
	}

IL_0069:
	{
		CharU5BU5D_t3528271667* L_18 = ___json0;
		int32_t* L_19 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t211875846_il2cpp_TypeInfo_var);
		MiniJSON_nextToken_m3582996375(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Boolean_t97287965_il2cpp_TypeInfo_var);
		bool L_20 = Boolean_Parse_m2370352694(NULL /*static, unused*/, _stringLiteral107078765, /*hidden argument*/NULL);
		bool L_21 = L_20;
		RuntimeObject * L_22 = Box(Boolean_t97287965_il2cpp_TypeInfo_var, &L_21);
		return L_22;
	}

IL_0081:
	{
		CharU5BU5D_t3528271667* L_23 = ___json0;
		int32_t* L_24 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t211875846_il2cpp_TypeInfo_var);
		MiniJSON_nextToken_m3582996375(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Boolean_t97287965_il2cpp_TypeInfo_var);
		bool L_25 = Boolean_Parse_m2370352694(NULL /*static, unused*/, _stringLiteral47698025, /*hidden argument*/NULL);
		bool L_26 = L_25;
		RuntimeObject * L_27 = Box(Boolean_t97287965_il2cpp_TypeInfo_var, &L_26);
		return L_27;
	}

IL_0099:
	{
		CharU5BU5D_t3528271667* L_28 = ___json0;
		int32_t* L_29 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t211875846_il2cpp_TypeInfo_var);
		MiniJSON_nextToken_m3582996375(NULL /*static, unused*/, L_28, L_29, /*hidden argument*/NULL);
		return NULL;
	}

IL_00a3:
	{
		goto IL_00a8;
	}

IL_00a8:
	{
		bool* L_30 = ___success2;
		*((int8_t*)(L_30)) = (int8_t)0;
		return NULL;
	}
}
// System.String XUPorterJSON.MiniJSON::parseString(System.Char[],System.Int32&)
extern "C"  String_t* MiniJSON_parseString_m2073376077 (RuntimeObject * __this /* static, unused */, CharU5BU5D_t3528271667* ___json0, int32_t* ___index1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_parseString_m2073376077_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	Il2CppChar V_1 = 0x0;
	int32_t V_2 = 0;
	bool V_3 = false;
	int32_t V_4 = 0;
	CharU5BU5D_t3528271667* V_5 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		V_0 = L_0;
		CharU5BU5D_t3528271667* L_1 = ___json0;
		int32_t* L_2 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t211875846_il2cpp_TypeInfo_var);
		MiniJSON_eatWhitespace_m1328618482(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		CharU5BU5D_t3528271667* L_3 = ___json0;
		int32_t* L_4 = ___index1;
		int32_t* L_5 = ___index1;
		int32_t L_6 = (*((int32_t*)L_5));
		V_2 = L_6;
		*((int32_t*)(L_4)) = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)1));
		int32_t L_7 = V_2;
		NullCheck(L_3);
		int32_t L_8 = L_7;
		uint16_t L_9 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_1 = L_9;
		V_3 = (bool)0;
		goto IL_01aa;
	}

IL_0020:
	{
		int32_t* L_10 = ___index1;
		CharU5BU5D_t3528271667* L_11 = ___json0;
		NullCheck(L_11);
		if ((!(((uint32_t)(*((int32_t*)L_10))) == ((uint32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_11)->max_length))))))))
		{
			goto IL_002f;
		}
	}
	{
		goto IL_01b0;
	}

IL_002f:
	{
		CharU5BU5D_t3528271667* L_12 = ___json0;
		int32_t* L_13 = ___index1;
		int32_t* L_14 = ___index1;
		int32_t L_15 = (*((int32_t*)L_14));
		V_2 = L_15;
		*((int32_t*)(L_13)) = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
		int32_t L_16 = V_2;
		NullCheck(L_12);
		int32_t L_17 = L_16;
		uint16_t L_18 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		V_1 = L_18;
		Il2CppChar L_19 = V_1;
		if ((!(((uint32_t)L_19) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_004a;
		}
	}
	{
		V_3 = (bool)1;
		goto IL_01b0;
	}

IL_004a:
	{
		Il2CppChar L_20 = V_1;
		if ((!(((uint32_t)L_20) == ((uint32_t)((int32_t)92)))))
		{
			goto IL_019d;
		}
	}
	{
		int32_t* L_21 = ___index1;
		CharU5BU5D_t3528271667* L_22 = ___json0;
		NullCheck(L_22);
		if ((!(((uint32_t)(*((int32_t*)L_21))) == ((uint32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_22)->max_length))))))))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_01b0;
	}

IL_0061:
	{
		CharU5BU5D_t3528271667* L_23 = ___json0;
		int32_t* L_24 = ___index1;
		int32_t* L_25 = ___index1;
		int32_t L_26 = (*((int32_t*)L_25));
		V_2 = L_26;
		*((int32_t*)(L_24)) = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_26, (int32_t)1));
		int32_t L_27 = V_2;
		NullCheck(L_23);
		int32_t L_28 = L_27;
		uint16_t L_29 = (L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		V_1 = L_29;
		Il2CppChar L_30 = V_1;
		if ((!(((uint32_t)L_30) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_0088;
		}
	}
	{
		String_t* L_31 = V_0;
		Il2CppChar L_32 = ((Il2CppChar)((int32_t)34));
		RuntimeObject * L_33 = Box(Char_t3634460470_il2cpp_TypeInfo_var, &L_32);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_34 = String_Concat_m904156431(NULL /*static, unused*/, L_31, L_33, /*hidden argument*/NULL);
		V_0 = L_34;
		goto IL_0198;
	}

IL_0088:
	{
		Il2CppChar L_35 = V_1;
		if ((!(((uint32_t)L_35) == ((uint32_t)((int32_t)92)))))
		{
			goto IL_00a3;
		}
	}
	{
		String_t* L_36 = V_0;
		Il2CppChar L_37 = ((Il2CppChar)((int32_t)92));
		RuntimeObject * L_38 = Box(Char_t3634460470_il2cpp_TypeInfo_var, &L_37);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_39 = String_Concat_m904156431(NULL /*static, unused*/, L_36, L_38, /*hidden argument*/NULL);
		V_0 = L_39;
		goto IL_0198;
	}

IL_00a3:
	{
		Il2CppChar L_40 = V_1;
		if ((!(((uint32_t)L_40) == ((uint32_t)((int32_t)47)))))
		{
			goto IL_00be;
		}
	}
	{
		String_t* L_41 = V_0;
		Il2CppChar L_42 = ((Il2CppChar)((int32_t)47));
		RuntimeObject * L_43 = Box(Char_t3634460470_il2cpp_TypeInfo_var, &L_42);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_44 = String_Concat_m904156431(NULL /*static, unused*/, L_41, L_43, /*hidden argument*/NULL);
		V_0 = L_44;
		goto IL_0198;
	}

IL_00be:
	{
		Il2CppChar L_45 = V_1;
		if ((!(((uint32_t)L_45) == ((uint32_t)((int32_t)98)))))
		{
			goto IL_00d8;
		}
	}
	{
		String_t* L_46 = V_0;
		Il2CppChar L_47 = ((Il2CppChar)8);
		RuntimeObject * L_48 = Box(Char_t3634460470_il2cpp_TypeInfo_var, &L_47);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_49 = String_Concat_m904156431(NULL /*static, unused*/, L_46, L_48, /*hidden argument*/NULL);
		V_0 = L_49;
		goto IL_0198;
	}

IL_00d8:
	{
		Il2CppChar L_50 = V_1;
		if ((!(((uint32_t)L_50) == ((uint32_t)((int32_t)102)))))
		{
			goto IL_00f3;
		}
	}
	{
		String_t* L_51 = V_0;
		Il2CppChar L_52 = ((Il2CppChar)((int32_t)12));
		RuntimeObject * L_53 = Box(Char_t3634460470_il2cpp_TypeInfo_var, &L_52);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_54 = String_Concat_m904156431(NULL /*static, unused*/, L_51, L_53, /*hidden argument*/NULL);
		V_0 = L_54;
		goto IL_0198;
	}

IL_00f3:
	{
		Il2CppChar L_55 = V_1;
		if ((!(((uint32_t)L_55) == ((uint32_t)((int32_t)110)))))
		{
			goto IL_010e;
		}
	}
	{
		String_t* L_56 = V_0;
		Il2CppChar L_57 = ((Il2CppChar)((int32_t)10));
		RuntimeObject * L_58 = Box(Char_t3634460470_il2cpp_TypeInfo_var, &L_57);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_59 = String_Concat_m904156431(NULL /*static, unused*/, L_56, L_58, /*hidden argument*/NULL);
		V_0 = L_59;
		goto IL_0198;
	}

IL_010e:
	{
		Il2CppChar L_60 = V_1;
		if ((!(((uint32_t)L_60) == ((uint32_t)((int32_t)114)))))
		{
			goto IL_0129;
		}
	}
	{
		String_t* L_61 = V_0;
		Il2CppChar L_62 = ((Il2CppChar)((int32_t)13));
		RuntimeObject * L_63 = Box(Char_t3634460470_il2cpp_TypeInfo_var, &L_62);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_64 = String_Concat_m904156431(NULL /*static, unused*/, L_61, L_63, /*hidden argument*/NULL);
		V_0 = L_64;
		goto IL_0198;
	}

IL_0129:
	{
		Il2CppChar L_65 = V_1;
		if ((!(((uint32_t)L_65) == ((uint32_t)((int32_t)116)))))
		{
			goto IL_0144;
		}
	}
	{
		String_t* L_66 = V_0;
		Il2CppChar L_67 = ((Il2CppChar)((int32_t)9));
		RuntimeObject * L_68 = Box(Char_t3634460470_il2cpp_TypeInfo_var, &L_67);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_69 = String_Concat_m904156431(NULL /*static, unused*/, L_66, L_68, /*hidden argument*/NULL);
		V_0 = L_69;
		goto IL_0198;
	}

IL_0144:
	{
		Il2CppChar L_70 = V_1;
		if ((!(((uint32_t)L_70) == ((uint32_t)((int32_t)117)))))
		{
			goto IL_0198;
		}
	}
	{
		CharU5BU5D_t3528271667* L_71 = ___json0;
		NullCheck(L_71);
		int32_t* L_72 = ___index1;
		V_4 = ((int32_t)il2cpp_codegen_subtract((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_71)->max_length)))), (int32_t)(*((int32_t*)L_72))));
		int32_t L_73 = V_4;
		if ((((int32_t)L_73) < ((int32_t)4)))
		{
			goto IL_0193;
		}
	}
	{
		V_5 = ((CharU5BU5D_t3528271667*)SZArrayNew(CharU5BU5D_t3528271667_il2cpp_TypeInfo_var, (uint32_t)4));
		CharU5BU5D_t3528271667* L_74 = ___json0;
		int32_t* L_75 = ___index1;
		CharU5BU5D_t3528271667* L_76 = V_5;
		Array_Copy_m344457298(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_74, (*((int32_t*)L_75)), (RuntimeArray *)(RuntimeArray *)L_76, 0, 4, /*hidden argument*/NULL);
		String_t* L_77 = V_0;
		CharU5BU5D_t3528271667* L_78 = V_5;
		String_t* L_79 = String_CreateString_m2818852475(NULL, L_78, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_80 = String_Concat_m2163913788(NULL /*static, unused*/, L_77, _stringLiteral4162292604, L_79, _stringLiteral3452614549, /*hidden argument*/NULL);
		V_0 = L_80;
		int32_t* L_81 = ___index1;
		int32_t* L_82 = ___index1;
		*((int32_t*)(L_81)) = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)(*((int32_t*)L_82)), (int32_t)4));
		goto IL_0198;
	}

IL_0193:
	{
		goto IL_01b0;
	}

IL_0198:
	{
		goto IL_01aa;
	}

IL_019d:
	{
		String_t* L_83 = V_0;
		Il2CppChar L_84 = V_1;
		Il2CppChar L_85 = L_84;
		RuntimeObject * L_86 = Box(Char_t3634460470_il2cpp_TypeInfo_var, &L_85);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_87 = String_Concat_m904156431(NULL /*static, unused*/, L_83, L_86, /*hidden argument*/NULL);
		V_0 = L_87;
	}

IL_01aa:
	{
		bool L_88 = V_3;
		if (!L_88)
		{
			goto IL_0020;
		}
	}

IL_01b0:
	{
		bool L_89 = V_3;
		if (L_89)
		{
			goto IL_01b8;
		}
	}
	{
		return (String_t*)NULL;
	}

IL_01b8:
	{
		String_t* L_90 = V_0;
		return L_90;
	}
}
// System.Double XUPorterJSON.MiniJSON::parseNumber(System.Char[],System.Int32&)
extern "C"  double MiniJSON_parseNumber_m3876524415 (RuntimeObject * __this /* static, unused */, CharU5BU5D_t3528271667* ___json0, int32_t* ___index1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_parseNumber_m3876524415_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	CharU5BU5D_t3528271667* V_2 = NULL;
	{
		CharU5BU5D_t3528271667* L_0 = ___json0;
		int32_t* L_1 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t211875846_il2cpp_TypeInfo_var);
		MiniJSON_eatWhitespace_m1328618482(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		CharU5BU5D_t3528271667* L_2 = ___json0;
		int32_t* L_3 = ___index1;
		int32_t L_4 = MiniJSON_getLastIndexOfNumber_m1276157804(NULL /*static, unused*/, L_2, (*((int32_t*)L_3)), /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = V_0;
		int32_t* L_6 = ___index1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_5, (int32_t)(*((int32_t*)L_6)))), (int32_t)1));
		int32_t L_7 = V_1;
		V_2 = ((CharU5BU5D_t3528271667*)SZArrayNew(CharU5BU5D_t3528271667_il2cpp_TypeInfo_var, (uint32_t)L_7));
		CharU5BU5D_t3528271667* L_8 = ___json0;
		int32_t* L_9 = ___index1;
		CharU5BU5D_t3528271667* L_10 = V_2;
		int32_t L_11 = V_1;
		Array_Copy_m344457298(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_8, (*((int32_t*)L_9)), (RuntimeArray *)(RuntimeArray *)L_10, 0, L_11, /*hidden argument*/NULL);
		int32_t* L_12 = ___index1;
		int32_t L_13 = V_0;
		*((int32_t*)(L_12)) = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)1));
		CharU5BU5D_t3528271667* L_14 = V_2;
		String_t* L_15 = String_CreateString_m2818852475(NULL, L_14, /*hidden argument*/NULL);
		double L_16 = Double_Parse_m4153729520(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		return L_16;
	}
}
// System.Int32 XUPorterJSON.MiniJSON::getLastIndexOfNumber(System.Char[],System.Int32)
extern "C"  int32_t MiniJSON_getLastIndexOfNumber_m1276157804 (RuntimeObject * __this /* static, unused */, CharU5BU5D_t3528271667* ___json0, int32_t ___index1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_getLastIndexOfNumber_m1276157804_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index1;
		V_0 = L_0;
		goto IL_0023;
	}

IL_0007:
	{
		CharU5BU5D_t3528271667* L_1 = ___json0;
		int32_t L_2 = V_0;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		uint16_t L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		NullCheck(_stringLiteral2206812729);
		int32_t L_5 = String_IndexOf_m363431711(_stringLiteral2206812729, L_4, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_5) == ((uint32_t)(-1)))))
		{
			goto IL_001f;
		}
	}
	{
		goto IL_002c;
	}

IL_001f:
	{
		int32_t L_6 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)1));
	}

IL_0023:
	{
		int32_t L_7 = V_0;
		CharU5BU5D_t3528271667* L_8 = ___json0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_8)->max_length)))))))
		{
			goto IL_0007;
		}
	}

IL_002c:
	{
		int32_t L_9 = V_0;
		return ((int32_t)il2cpp_codegen_subtract((int32_t)L_9, (int32_t)1));
	}
}
// System.Void XUPorterJSON.MiniJSON::eatWhitespace(System.Char[],System.Int32&)
extern "C"  void MiniJSON_eatWhitespace_m1328618482 (RuntimeObject * __this /* static, unused */, CharU5BU5D_t3528271667* ___json0, int32_t* ___index1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_eatWhitespace_m1328618482_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		goto IL_0024;
	}

IL_0005:
	{
		CharU5BU5D_t3528271667* L_0 = ___json0;
		int32_t* L_1 = ___index1;
		NullCheck(L_0);
		int32_t L_2 = (*((int32_t*)L_1));
		uint16_t L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(_stringLiteral1596281288);
		int32_t L_4 = String_IndexOf_m363431711(_stringLiteral1596281288, L_3, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)(-1)))))
		{
			goto IL_001e;
		}
	}
	{
		goto IL_002e;
	}

IL_001e:
	{
		int32_t* L_5 = ___index1;
		int32_t* L_6 = ___index1;
		*((int32_t*)(L_5)) = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)(*((int32_t*)L_6)), (int32_t)1));
	}

IL_0024:
	{
		int32_t* L_7 = ___index1;
		CharU5BU5D_t3528271667* L_8 = ___json0;
		NullCheck(L_8);
		if ((((int32_t)(*((int32_t*)L_7))) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_8)->max_length)))))))
		{
			goto IL_0005;
		}
	}

IL_002e:
	{
		return;
	}
}
// System.Int32 XUPorterJSON.MiniJSON::lookAhead(System.Char[],System.Int32)
extern "C"  int32_t MiniJSON_lookAhead_m3599256246 (RuntimeObject * __this /* static, unused */, CharU5BU5D_t3528271667* ___json0, int32_t ___index1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_lookAhead_m3599256246_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index1;
		V_0 = L_0;
		CharU5BU5D_t3528271667* L_1 = ___json0;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t211875846_il2cpp_TypeInfo_var);
		int32_t L_2 = MiniJSON_nextToken_m3582996375(NULL /*static, unused*/, L_1, (&V_0), /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 XUPorterJSON.MiniJSON::nextToken(System.Char[],System.Int32&)
extern "C"  int32_t MiniJSON_nextToken_m3582996375 (RuntimeObject * __this /* static, unused */, CharU5BU5D_t3528271667* ___json0, int32_t* ___index1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_nextToken_m3582996375_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppChar V_0 = 0x0;
	int32_t V_1 = 0;
	{
		CharU5BU5D_t3528271667* L_0 = ___json0;
		int32_t* L_1 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t211875846_il2cpp_TypeInfo_var);
		MiniJSON_eatWhitespace_m1328618482(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		int32_t* L_2 = ___index1;
		CharU5BU5D_t3528271667* L_3 = ___json0;
		NullCheck(L_3);
		if ((!(((uint32_t)(*((int32_t*)L_2))) == ((uint32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_3)->max_length))))))))
		{
			goto IL_0013;
		}
	}
	{
		return 0;
	}

IL_0013:
	{
		CharU5BU5D_t3528271667* L_4 = ___json0;
		int32_t* L_5 = ___index1;
		NullCheck(L_4);
		int32_t L_6 = (*((int32_t*)L_5));
		uint16_t L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_0 = L_7;
		int32_t* L_8 = ___index1;
		int32_t* L_9 = ___index1;
		*((int32_t*)(L_8)) = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)(*((int32_t*)L_9)), (int32_t)1));
		Il2CppChar L_10 = V_0;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_10, (int32_t)((int32_t)44))))
		{
			case 0:
			{
				goto IL_00a2;
			}
			case 1:
			{
				goto IL_00a6;
			}
			case 2:
			{
				goto IL_0063;
			}
			case 3:
			{
				goto IL_0063;
			}
			case 4:
			{
				goto IL_00a6;
			}
			case 5:
			{
				goto IL_00a6;
			}
			case 6:
			{
				goto IL_00a6;
			}
			case 7:
			{
				goto IL_00a6;
			}
			case 8:
			{
				goto IL_00a6;
			}
			case 9:
			{
				goto IL_00a6;
			}
			case 10:
			{
				goto IL_00a6;
			}
			case 11:
			{
				goto IL_00a6;
			}
			case 12:
			{
				goto IL_00a6;
			}
			case 13:
			{
				goto IL_00a6;
			}
			case 14:
			{
				goto IL_00a8;
			}
		}
	}

IL_0063:
	{
		Il2CppChar L_11 = V_0;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_11, (int32_t)((int32_t)91))))
		{
			case 0:
			{
				goto IL_009e;
			}
			case 1:
			{
				goto IL_0078;
			}
			case 2:
			{
				goto IL_00a0;
			}
		}
	}

IL_0078:
	{
		Il2CppChar L_12 = V_0;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_12, (int32_t)((int32_t)123))))
		{
			case 0:
			{
				goto IL_009a;
			}
			case 1:
			{
				goto IL_008d;
			}
			case 2:
			{
				goto IL_009c;
			}
		}
	}

IL_008d:
	{
		Il2CppChar L_13 = V_0;
		if ((((int32_t)L_13) == ((int32_t)((int32_t)34))))
		{
			goto IL_00a4;
		}
	}
	{
		goto IL_00aa;
	}

IL_009a:
	{
		return 1;
	}

IL_009c:
	{
		return 2;
	}

IL_009e:
	{
		return 3;
	}

IL_00a0:
	{
		return 4;
	}

IL_00a2:
	{
		return 6;
	}

IL_00a4:
	{
		return 7;
	}

IL_00a6:
	{
		return 8;
	}

IL_00a8:
	{
		return 5;
	}

IL_00aa:
	{
		int32_t* L_14 = ___index1;
		int32_t* L_15 = ___index1;
		*((int32_t*)(L_14)) = (int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)(*((int32_t*)L_15)), (int32_t)1));
		CharU5BU5D_t3528271667* L_16 = ___json0;
		NullCheck(L_16);
		int32_t* L_17 = ___index1;
		V_1 = ((int32_t)il2cpp_codegen_subtract((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_16)->max_length)))), (int32_t)(*((int32_t*)L_17))));
		int32_t L_18 = V_1;
		if ((((int32_t)L_18) < ((int32_t)5)))
		{
			goto IL_0106;
		}
	}
	{
		CharU5BU5D_t3528271667* L_19 = ___json0;
		int32_t* L_20 = ___index1;
		NullCheck(L_19);
		int32_t L_21 = (*((int32_t*)L_20));
		uint16_t L_22 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		if ((!(((uint32_t)L_22) == ((uint32_t)((int32_t)102)))))
		{
			goto IL_0106;
		}
	}
	{
		CharU5BU5D_t3528271667* L_23 = ___json0;
		int32_t* L_24 = ___index1;
		NullCheck(L_23);
		int32_t L_25 = ((int32_t)il2cpp_codegen_add((int32_t)(*((int32_t*)L_24)), (int32_t)1));
		uint16_t L_26 = (L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_25));
		if ((!(((uint32_t)L_26) == ((uint32_t)((int32_t)97)))))
		{
			goto IL_0106;
		}
	}
	{
		CharU5BU5D_t3528271667* L_27 = ___json0;
		int32_t* L_28 = ___index1;
		NullCheck(L_27);
		int32_t L_29 = ((int32_t)il2cpp_codegen_add((int32_t)(*((int32_t*)L_28)), (int32_t)2));
		uint16_t L_30 = (L_27)->GetAt(static_cast<il2cpp_array_size_t>(L_29));
		if ((!(((uint32_t)L_30) == ((uint32_t)((int32_t)108)))))
		{
			goto IL_0106;
		}
	}
	{
		CharU5BU5D_t3528271667* L_31 = ___json0;
		int32_t* L_32 = ___index1;
		NullCheck(L_31);
		int32_t L_33 = ((int32_t)il2cpp_codegen_add((int32_t)(*((int32_t*)L_32)), (int32_t)3));
		uint16_t L_34 = (L_31)->GetAt(static_cast<il2cpp_array_size_t>(L_33));
		if ((!(((uint32_t)L_34) == ((uint32_t)((int32_t)115)))))
		{
			goto IL_0106;
		}
	}
	{
		CharU5BU5D_t3528271667* L_35 = ___json0;
		int32_t* L_36 = ___index1;
		NullCheck(L_35);
		int32_t L_37 = ((int32_t)il2cpp_codegen_add((int32_t)(*((int32_t*)L_36)), (int32_t)4));
		uint16_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		if ((!(((uint32_t)L_38) == ((uint32_t)((int32_t)101)))))
		{
			goto IL_0106;
		}
	}
	{
		int32_t* L_39 = ___index1;
		int32_t* L_40 = ___index1;
		*((int32_t*)(L_39)) = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)(*((int32_t*)L_40)), (int32_t)5));
		return ((int32_t)10);
	}

IL_0106:
	{
		int32_t L_41 = V_1;
		if ((((int32_t)L_41) < ((int32_t)4)))
		{
			goto IL_0148;
		}
	}
	{
		CharU5BU5D_t3528271667* L_42 = ___json0;
		int32_t* L_43 = ___index1;
		NullCheck(L_42);
		int32_t L_44 = (*((int32_t*)L_43));
		uint16_t L_45 = (L_42)->GetAt(static_cast<il2cpp_array_size_t>(L_44));
		if ((!(((uint32_t)L_45) == ((uint32_t)((int32_t)116)))))
		{
			goto IL_0148;
		}
	}
	{
		CharU5BU5D_t3528271667* L_46 = ___json0;
		int32_t* L_47 = ___index1;
		NullCheck(L_46);
		int32_t L_48 = ((int32_t)il2cpp_codegen_add((int32_t)(*((int32_t*)L_47)), (int32_t)1));
		uint16_t L_49 = (L_46)->GetAt(static_cast<il2cpp_array_size_t>(L_48));
		if ((!(((uint32_t)L_49) == ((uint32_t)((int32_t)114)))))
		{
			goto IL_0148;
		}
	}
	{
		CharU5BU5D_t3528271667* L_50 = ___json0;
		int32_t* L_51 = ___index1;
		NullCheck(L_50);
		int32_t L_52 = ((int32_t)il2cpp_codegen_add((int32_t)(*((int32_t*)L_51)), (int32_t)2));
		uint16_t L_53 = (L_50)->GetAt(static_cast<il2cpp_array_size_t>(L_52));
		if ((!(((uint32_t)L_53) == ((uint32_t)((int32_t)117)))))
		{
			goto IL_0148;
		}
	}
	{
		CharU5BU5D_t3528271667* L_54 = ___json0;
		int32_t* L_55 = ___index1;
		NullCheck(L_54);
		int32_t L_56 = ((int32_t)il2cpp_codegen_add((int32_t)(*((int32_t*)L_55)), (int32_t)3));
		uint16_t L_57 = (L_54)->GetAt(static_cast<il2cpp_array_size_t>(L_56));
		if ((!(((uint32_t)L_57) == ((uint32_t)((int32_t)101)))))
		{
			goto IL_0148;
		}
	}
	{
		int32_t* L_58 = ___index1;
		int32_t* L_59 = ___index1;
		*((int32_t*)(L_58)) = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)(*((int32_t*)L_59)), (int32_t)4));
		return ((int32_t)9);
	}

IL_0148:
	{
		int32_t L_60 = V_1;
		if ((((int32_t)L_60) < ((int32_t)4)))
		{
			goto IL_018a;
		}
	}
	{
		CharU5BU5D_t3528271667* L_61 = ___json0;
		int32_t* L_62 = ___index1;
		NullCheck(L_61);
		int32_t L_63 = (*((int32_t*)L_62));
		uint16_t L_64 = (L_61)->GetAt(static_cast<il2cpp_array_size_t>(L_63));
		if ((!(((uint32_t)L_64) == ((uint32_t)((int32_t)110)))))
		{
			goto IL_018a;
		}
	}
	{
		CharU5BU5D_t3528271667* L_65 = ___json0;
		int32_t* L_66 = ___index1;
		NullCheck(L_65);
		int32_t L_67 = ((int32_t)il2cpp_codegen_add((int32_t)(*((int32_t*)L_66)), (int32_t)1));
		uint16_t L_68 = (L_65)->GetAt(static_cast<il2cpp_array_size_t>(L_67));
		if ((!(((uint32_t)L_68) == ((uint32_t)((int32_t)117)))))
		{
			goto IL_018a;
		}
	}
	{
		CharU5BU5D_t3528271667* L_69 = ___json0;
		int32_t* L_70 = ___index1;
		NullCheck(L_69);
		int32_t L_71 = ((int32_t)il2cpp_codegen_add((int32_t)(*((int32_t*)L_70)), (int32_t)2));
		uint16_t L_72 = (L_69)->GetAt(static_cast<il2cpp_array_size_t>(L_71));
		if ((!(((uint32_t)L_72) == ((uint32_t)((int32_t)108)))))
		{
			goto IL_018a;
		}
	}
	{
		CharU5BU5D_t3528271667* L_73 = ___json0;
		int32_t* L_74 = ___index1;
		NullCheck(L_73);
		int32_t L_75 = ((int32_t)il2cpp_codegen_add((int32_t)(*((int32_t*)L_74)), (int32_t)3));
		uint16_t L_76 = (L_73)->GetAt(static_cast<il2cpp_array_size_t>(L_75));
		if ((!(((uint32_t)L_76) == ((uint32_t)((int32_t)108)))))
		{
			goto IL_018a;
		}
	}
	{
		int32_t* L_77 = ___index1;
		int32_t* L_78 = ___index1;
		*((int32_t*)(L_77)) = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)(*((int32_t*)L_78)), (int32_t)4));
		return ((int32_t)11);
	}

IL_018a:
	{
		return 0;
	}
}
// System.Boolean XUPorterJSON.MiniJSON::serializeObjectOrArray(System.Object,System.Text.StringBuilder)
extern "C"  bool MiniJSON_serializeObjectOrArray_m1482338836 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___objectOrArray0, StringBuilder_t * ___builder1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_serializeObjectOrArray_m1482338836_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___objectOrArray0;
		if (!((Hashtable_t1853889766 *)IsInstClass((RuntimeObject*)L_0, Hashtable_t1853889766_il2cpp_TypeInfo_var)))
		{
			goto IL_0018;
		}
	}
	{
		RuntimeObject * L_1 = ___objectOrArray0;
		StringBuilder_t * L_2 = ___builder1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t211875846_il2cpp_TypeInfo_var);
		bool L_3 = MiniJSON_serializeObject_m716018166(NULL /*static, unused*/, ((Hashtable_t1853889766 *)CastclassClass((RuntimeObject*)L_1, Hashtable_t1853889766_il2cpp_TypeInfo_var)), L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0018:
	{
		RuntimeObject * L_4 = ___objectOrArray0;
		if (!((ArrayList_t2718874744 *)IsInstClass((RuntimeObject*)L_4, ArrayList_t2718874744_il2cpp_TypeInfo_var)))
		{
			goto IL_0030;
		}
	}
	{
		RuntimeObject * L_5 = ___objectOrArray0;
		StringBuilder_t * L_6 = ___builder1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t211875846_il2cpp_TypeInfo_var);
		bool L_7 = MiniJSON_serializeArray_m710485201(NULL /*static, unused*/, ((ArrayList_t2718874744 *)CastclassClass((RuntimeObject*)L_5, ArrayList_t2718874744_il2cpp_TypeInfo_var)), L_6, /*hidden argument*/NULL);
		return L_7;
	}

IL_0030:
	{
		return (bool)0;
	}
}
// System.Boolean XUPorterJSON.MiniJSON::serializeObject(System.Collections.Hashtable,System.Text.StringBuilder)
extern "C"  bool MiniJSON_serializeObject_m716018166 (RuntimeObject * __this /* static, unused */, Hashtable_t1853889766 * ___anObject0, StringBuilder_t * ___builder1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_serializeObject_m716018166_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	bool V_1 = false;
	String_t* V_2 = NULL;
	RuntimeObject * V_3 = NULL;
	{
		StringBuilder_t * L_0 = ___builder1;
		NullCheck(L_0);
		StringBuilder_Append_m1965104174(L_0, _stringLiteral3452614613, /*hidden argument*/NULL);
		Hashtable_t1853889766 * L_1 = ___anObject0;
		NullCheck(L_1);
		RuntimeObject* L_2 = VirtFuncInvoker0< RuntimeObject* >::Invoke(32 /* System.Collections.IDictionaryEnumerator System.Collections.Hashtable::GetEnumerator() */, L_1);
		V_0 = L_2;
		V_1 = (bool)1;
		goto IL_0062;
	}

IL_001a:
	{
		RuntimeObject* L_3 = V_0;
		NullCheck(L_3);
		RuntimeObject * L_4 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(1 /* System.Object System.Collections.IDictionaryEnumerator::get_Key() */, IDictionaryEnumerator_t1693217257_il2cpp_TypeInfo_var, L_3);
		NullCheck(L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_4);
		V_2 = L_5;
		RuntimeObject* L_6 = V_0;
		NullCheck(L_6);
		RuntimeObject * L_7 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(2 /* System.Object System.Collections.IDictionaryEnumerator::get_Value() */, IDictionaryEnumerator_t1693217257_il2cpp_TypeInfo_var, L_6);
		V_3 = L_7;
		bool L_8 = V_1;
		if (L_8)
		{
			goto IL_003f;
		}
	}
	{
		StringBuilder_t * L_9 = ___builder1;
		NullCheck(L_9);
		StringBuilder_Append_m1965104174(L_9, _stringLiteral3450517380, /*hidden argument*/NULL);
	}

IL_003f:
	{
		String_t* L_10 = V_2;
		StringBuilder_t * L_11 = ___builder1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t211875846_il2cpp_TypeInfo_var);
		MiniJSON_serializeString_m122238726(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		StringBuilder_t * L_12 = ___builder1;
		NullCheck(L_12);
		StringBuilder_Append_m1965104174(L_12, _stringLiteral3452614550, /*hidden argument*/NULL);
		RuntimeObject * L_13 = V_3;
		StringBuilder_t * L_14 = ___builder1;
		bool L_15 = MiniJSON_serializeValue_m2428040582(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_0060;
		}
	}
	{
		return (bool)0;
	}

IL_0060:
	{
		V_1 = (bool)0;
	}

IL_0062:
	{
		RuntimeObject* L_16 = V_0;
		NullCheck(L_16);
		bool L_17 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_16);
		if (L_17)
		{
			goto IL_001a;
		}
	}
	{
		StringBuilder_t * L_18 = ___builder1;
		NullCheck(L_18);
		StringBuilder_Append_m1965104174(L_18, _stringLiteral3452614611, /*hidden argument*/NULL);
		return (bool)1;
	}
}
// System.Boolean XUPorterJSON.MiniJSON::serializeDictionary(System.Collections.Generic.Dictionary`2<System.String,System.String>,System.Text.StringBuilder)
extern "C"  bool MiniJSON_serializeDictionary_m2956593198 (RuntimeObject * __this /* static, unused */, Dictionary_2_t1632706988 * ___dict0, StringBuilder_t * ___builder1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_serializeDictionary_m2956593198_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	KeyValuePair_2_t4030379155  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Enumerator_t3586889763  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		StringBuilder_t * L_0 = ___builder1;
		NullCheck(L_0);
		StringBuilder_Append_m1965104174(L_0, _stringLiteral3452614613, /*hidden argument*/NULL);
		V_0 = (bool)1;
		Dictionary_2_t1632706988 * L_1 = ___dict0;
		NullCheck(L_1);
		Enumerator_t3586889763  L_2 = Dictionary_2_GetEnumerator_m260861070(L_1, /*hidden argument*/Dictionary_2_GetEnumerator_m260861070_RuntimeMethod_var);
		V_2 = L_2;
	}

IL_0015:
	try
	{ // begin try (depth: 1)
		{
			goto IL_005c;
		}

IL_001a:
		{
			KeyValuePair_2_t4030379155  L_3 = Enumerator_get_Current_m2519371089((&V_2), /*hidden argument*/Enumerator_get_Current_m2519371089_RuntimeMethod_var);
			V_1 = L_3;
			bool L_4 = V_0;
			if (L_4)
			{
				goto IL_0034;
			}
		}

IL_0028:
		{
			StringBuilder_t * L_5 = ___builder1;
			NullCheck(L_5);
			StringBuilder_Append_m1965104174(L_5, _stringLiteral3450517380, /*hidden argument*/NULL);
		}

IL_0034:
		{
			String_t* L_6 = KeyValuePair_2_get_Key_m3980750512((&V_1), /*hidden argument*/KeyValuePair_2_get_Key_m3980750512_RuntimeMethod_var);
			StringBuilder_t * L_7 = ___builder1;
			IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t211875846_il2cpp_TypeInfo_var);
			MiniJSON_serializeString_m122238726(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
			StringBuilder_t * L_8 = ___builder1;
			NullCheck(L_8);
			StringBuilder_Append_m1965104174(L_8, _stringLiteral3452614550, /*hidden argument*/NULL);
			String_t* L_9 = KeyValuePair_2_get_Value_m2243990694((&V_1), /*hidden argument*/KeyValuePair_2_get_Value_m2243990694_RuntimeMethod_var);
			StringBuilder_t * L_10 = ___builder1;
			MiniJSON_serializeString_m122238726(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
			V_0 = (bool)0;
		}

IL_005c:
		{
			bool L_11 = Enumerator_MoveNext_m1175750522((&V_2), /*hidden argument*/Enumerator_MoveNext_m1175750522_RuntimeMethod_var);
			if (L_11)
			{
				goto IL_001a;
			}
		}

IL_0068:
		{
			IL2CPP_LEAVE(0x7B, FINALLY_006d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_006d;
	}

FINALLY_006d:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m2198401511((&V_2), /*hidden argument*/Enumerator_Dispose_m2198401511_RuntimeMethod_var);
		IL2CPP_END_FINALLY(109)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(109)
	{
		IL2CPP_JUMP_TBL(0x7B, IL_007b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_007b:
	{
		StringBuilder_t * L_12 = ___builder1;
		NullCheck(L_12);
		StringBuilder_Append_m1965104174(L_12, _stringLiteral3452614611, /*hidden argument*/NULL);
		return (bool)1;
	}
}
// System.Boolean XUPorterJSON.MiniJSON::serializeArray(System.Collections.ArrayList,System.Text.StringBuilder)
extern "C"  bool MiniJSON_serializeArray_m710485201 (RuntimeObject * __this /* static, unused */, ArrayList_t2718874744 * ___anArray0, StringBuilder_t * ___builder1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_serializeArray_m710485201_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	RuntimeObject * V_2 = NULL;
	{
		StringBuilder_t * L_0 = ___builder1;
		NullCheck(L_0);
		StringBuilder_Append_m1965104174(L_0, _stringLiteral3452614645, /*hidden argument*/NULL);
		V_0 = (bool)1;
		V_1 = 0;
		goto IL_0043;
	}

IL_0015:
	{
		ArrayList_t2718874744 * L_1 = ___anArray0;
		int32_t L_2 = V_1;
		NullCheck(L_1);
		RuntimeObject * L_3 = VirtFuncInvoker1< RuntimeObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_1, L_2);
		V_2 = L_3;
		bool L_4 = V_0;
		if (L_4)
		{
			goto IL_002f;
		}
	}
	{
		StringBuilder_t * L_5 = ___builder1;
		NullCheck(L_5);
		StringBuilder_Append_m1965104174(L_5, _stringLiteral3450517380, /*hidden argument*/NULL);
	}

IL_002f:
	{
		RuntimeObject * L_6 = V_2;
		StringBuilder_t * L_7 = ___builder1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t211875846_il2cpp_TypeInfo_var);
		bool L_8 = MiniJSON_serializeValue_m2428040582(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_003d;
		}
	}
	{
		return (bool)0;
	}

IL_003d:
	{
		V_0 = (bool)0;
		int32_t L_9 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1));
	}

IL_0043:
	{
		int32_t L_10 = V_1;
		ArrayList_t2718874744 * L_11 = ___anArray0;
		NullCheck(L_11);
		int32_t L_12 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_11);
		if ((((int32_t)L_10) < ((int32_t)L_12)))
		{
			goto IL_0015;
		}
	}
	{
		StringBuilder_t * L_13 = ___builder1;
		NullCheck(L_13);
		StringBuilder_Append_m1965104174(L_13, _stringLiteral3452614643, /*hidden argument*/NULL);
		return (bool)1;
	}
}
// System.Boolean XUPorterJSON.MiniJSON::serializeValue(System.Object,System.Text.StringBuilder)
extern "C"  bool MiniJSON_serializeValue_m2428040582 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___value0, StringBuilder_t * ___builder1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_serializeValue_m2428040582_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___value0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		StringBuilder_t * L_1 = ___builder1;
		NullCheck(L_1);
		StringBuilder_Append_m1965104174(L_1, _stringLiteral1202628576, /*hidden argument*/NULL);
		goto IL_0143;
	}

IL_0017:
	{
		RuntimeObject * L_2 = ___value0;
		NullCheck(L_2);
		Type_t * L_3 = Object_GetType_m88164663(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		bool L_4 = Type_get_IsArray_m2591212821(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_003e;
		}
	}
	{
		RuntimeObject * L_5 = ___value0;
		ArrayList_t2718874744 * L_6 = (ArrayList_t2718874744 *)il2cpp_codegen_object_new(ArrayList_t2718874744_il2cpp_TypeInfo_var);
		ArrayList__ctor_m2130986447(L_6, ((RuntimeObject*)Castclass((RuntimeObject*)L_5, ICollection_t3904884886_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		StringBuilder_t * L_7 = ___builder1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t211875846_il2cpp_TypeInfo_var);
		MiniJSON_serializeArray_m710485201(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		goto IL_0143;
	}

IL_003e:
	{
		RuntimeObject * L_8 = ___value0;
		if (!((String_t*)IsInstSealed((RuntimeObject*)L_8, String_t_il2cpp_TypeInfo_var)))
		{
			goto IL_005a;
		}
	}
	{
		RuntimeObject * L_9 = ___value0;
		StringBuilder_t * L_10 = ___builder1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t211875846_il2cpp_TypeInfo_var);
		MiniJSON_serializeString_m122238726(NULL /*static, unused*/, ((String_t*)CastclassSealed((RuntimeObject*)L_9, String_t_il2cpp_TypeInfo_var)), L_10, /*hidden argument*/NULL);
		goto IL_0143;
	}

IL_005a:
	{
		RuntimeObject * L_11 = ___value0;
		if (!((RuntimeObject *)IsInstSealed((RuntimeObject*)L_11, Char_t3634460470_il2cpp_TypeInfo_var)))
		{
			goto IL_007b;
		}
	}
	{
		RuntimeObject * L_12 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2465617642_il2cpp_TypeInfo_var);
		String_t* L_13 = Convert_ToString_m1553911740(NULL /*static, unused*/, ((*(Il2CppChar*)((Il2CppChar*)UnBox(L_12, Char_t3634460470_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		StringBuilder_t * L_14 = ___builder1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t211875846_il2cpp_TypeInfo_var);
		MiniJSON_serializeString_m122238726(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		goto IL_0143;
	}

IL_007b:
	{
		RuntimeObject * L_15 = ___value0;
		if (!((Hashtable_t1853889766 *)IsInstClass((RuntimeObject*)L_15, Hashtable_t1853889766_il2cpp_TypeInfo_var)))
		{
			goto IL_0098;
		}
	}
	{
		RuntimeObject * L_16 = ___value0;
		StringBuilder_t * L_17 = ___builder1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t211875846_il2cpp_TypeInfo_var);
		MiniJSON_serializeObject_m716018166(NULL /*static, unused*/, ((Hashtable_t1853889766 *)CastclassClass((RuntimeObject*)L_16, Hashtable_t1853889766_il2cpp_TypeInfo_var)), L_17, /*hidden argument*/NULL);
		goto IL_0143;
	}

IL_0098:
	{
		RuntimeObject * L_18 = ___value0;
		if (!((Dictionary_2_t1632706988 *)IsInstClass((RuntimeObject*)L_18, Dictionary_2_t1632706988_il2cpp_TypeInfo_var)))
		{
			goto IL_00b5;
		}
	}
	{
		RuntimeObject * L_19 = ___value0;
		StringBuilder_t * L_20 = ___builder1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t211875846_il2cpp_TypeInfo_var);
		MiniJSON_serializeDictionary_m2956593198(NULL /*static, unused*/, ((Dictionary_2_t1632706988 *)CastclassClass((RuntimeObject*)L_19, Dictionary_2_t1632706988_il2cpp_TypeInfo_var)), L_20, /*hidden argument*/NULL);
		goto IL_0143;
	}

IL_00b5:
	{
		RuntimeObject * L_21 = ___value0;
		if (!((ArrayList_t2718874744 *)IsInstClass((RuntimeObject*)L_21, ArrayList_t2718874744_il2cpp_TypeInfo_var)))
		{
			goto IL_00d2;
		}
	}
	{
		RuntimeObject * L_22 = ___value0;
		StringBuilder_t * L_23 = ___builder1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t211875846_il2cpp_TypeInfo_var);
		MiniJSON_serializeArray_m710485201(NULL /*static, unused*/, ((ArrayList_t2718874744 *)CastclassClass((RuntimeObject*)L_22, ArrayList_t2718874744_il2cpp_TypeInfo_var)), L_23, /*hidden argument*/NULL);
		goto IL_0143;
	}

IL_00d2:
	{
		RuntimeObject * L_24 = ___value0;
		if (!((RuntimeObject *)IsInstSealed((RuntimeObject*)L_24, Boolean_t97287965_il2cpp_TypeInfo_var)))
		{
			goto IL_00f9;
		}
	}
	{
		RuntimeObject * L_25 = ___value0;
		if (!((*(bool*)((bool*)UnBox(L_25, Boolean_t97287965_il2cpp_TypeInfo_var)))))
		{
			goto IL_00f9;
		}
	}
	{
		StringBuilder_t * L_26 = ___builder1;
		NullCheck(L_26);
		StringBuilder_Append_m1965104174(L_26, _stringLiteral4002445229, /*hidden argument*/NULL);
		goto IL_0143;
	}

IL_00f9:
	{
		RuntimeObject * L_27 = ___value0;
		if (!((RuntimeObject *)IsInstSealed((RuntimeObject*)L_27, Boolean_t97287965_il2cpp_TypeInfo_var)))
		{
			goto IL_0120;
		}
	}
	{
		RuntimeObject * L_28 = ___value0;
		if (((*(bool*)((bool*)UnBox(L_28, Boolean_t97287965_il2cpp_TypeInfo_var)))))
		{
			goto IL_0120;
		}
	}
	{
		StringBuilder_t * L_29 = ___builder1;
		NullCheck(L_29);
		StringBuilder_Append_m1965104174(L_29, _stringLiteral3875954633, /*hidden argument*/NULL);
		goto IL_0143;
	}

IL_0120:
	{
		RuntimeObject * L_30 = ___value0;
		NullCheck(L_30);
		Type_t * L_31 = Object_GetType_m88164663(L_30, /*hidden argument*/NULL);
		NullCheck(L_31);
		bool L_32 = Type_get_IsPrimitive_m1114712797(L_31, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_0141;
		}
	}
	{
		RuntimeObject * L_33 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2465617642_il2cpp_TypeInfo_var);
		double L_34 = Convert_ToDouble_m4025515304(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
		StringBuilder_t * L_35 = ___builder1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t211875846_il2cpp_TypeInfo_var);
		MiniJSON_serializeNumber_m3396057679(NULL /*static, unused*/, L_34, L_35, /*hidden argument*/NULL);
		goto IL_0143;
	}

IL_0141:
	{
		return (bool)0;
	}

IL_0143:
	{
		return (bool)1;
	}
}
// System.Void XUPorterJSON.MiniJSON::serializeString(System.String,System.Text.StringBuilder)
extern "C"  void MiniJSON_serializeString_m122238726 (RuntimeObject * __this /* static, unused */, String_t* ___aString0, StringBuilder_t * ___builder1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_serializeString_m122238726_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CharU5BU5D_t3528271667* V_0 = NULL;
	int32_t V_1 = 0;
	Il2CppChar V_2 = 0x0;
	int32_t V_3 = 0;
	{
		StringBuilder_t * L_0 = ___builder1;
		NullCheck(L_0);
		StringBuilder_Append_m1965104174(L_0, _stringLiteral3452614526, /*hidden argument*/NULL);
		String_t* L_1 = ___aString0;
		NullCheck(L_1);
		CharU5BU5D_t3528271667* L_2 = String_ToCharArray_m1492846834(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		V_1 = 0;
		goto IL_0115;
	}

IL_001a:
	{
		CharU5BU5D_t3528271667* L_3 = V_0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		uint16_t L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		V_2 = L_6;
		Il2CppChar L_7 = V_2;
		if ((!(((uint32_t)L_7) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_0037;
		}
	}
	{
		StringBuilder_t * L_8 = ___builder1;
		NullCheck(L_8);
		StringBuilder_Append_m1965104174(L_8, _stringLiteral3450386420, /*hidden argument*/NULL);
		goto IL_0111;
	}

IL_0037:
	{
		Il2CppChar L_9 = V_2;
		if ((!(((uint32_t)L_9) == ((uint32_t)((int32_t)92)))))
		{
			goto IL_0050;
		}
	}
	{
		StringBuilder_t * L_10 = ___builder1;
		NullCheck(L_10);
		StringBuilder_Append_m1965104174(L_10, _stringLiteral3458119668, /*hidden argument*/NULL);
		goto IL_0111;
	}

IL_0050:
	{
		Il2CppChar L_11 = V_2;
		if ((!(((uint32_t)L_11) == ((uint32_t)8))))
		{
			goto IL_0068;
		}
	}
	{
		StringBuilder_t * L_12 = ___builder1;
		NullCheck(L_12);
		StringBuilder_Append_m1965104174(L_12, _stringLiteral3454580724, /*hidden argument*/NULL);
		goto IL_0111;
	}

IL_0068:
	{
		Il2CppChar L_13 = V_2;
		if ((!(((uint32_t)L_13) == ((uint32_t)((int32_t)12)))))
		{
			goto IL_0081;
		}
	}
	{
		StringBuilder_t * L_14 = ___builder1;
		NullCheck(L_14);
		StringBuilder_Append_m1965104174(L_14, _stringLiteral3454318580, /*hidden argument*/NULL);
		goto IL_0111;
	}

IL_0081:
	{
		Il2CppChar L_15 = V_2;
		if ((!(((uint32_t)L_15) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_009a;
		}
	}
	{
		StringBuilder_t * L_16 = ___builder1;
		NullCheck(L_16);
		StringBuilder_Append_m1965104174(L_16, _stringLiteral3454842868, /*hidden argument*/NULL);
		goto IL_0111;
	}

IL_009a:
	{
		Il2CppChar L_17 = V_2;
		if ((!(((uint32_t)L_17) == ((uint32_t)((int32_t)13)))))
		{
			goto IL_00b3;
		}
	}
	{
		StringBuilder_t * L_18 = ___builder1;
		NullCheck(L_18);
		StringBuilder_Append_m1965104174(L_18, _stringLiteral3455629300, /*hidden argument*/NULL);
		goto IL_0111;
	}

IL_00b3:
	{
		Il2CppChar L_19 = V_2;
		if ((!(((uint32_t)L_19) == ((uint32_t)((int32_t)9)))))
		{
			goto IL_00cc;
		}
	}
	{
		StringBuilder_t * L_20 = ___builder1;
		NullCheck(L_20);
		StringBuilder_Append_m1965104174(L_20, _stringLiteral3455498228, /*hidden argument*/NULL);
		goto IL_0111;
	}

IL_00cc:
	{
		Il2CppChar L_21 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2465617642_il2cpp_TypeInfo_var);
		int32_t L_22 = Convert_ToInt32_m1876369743(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		V_3 = L_22;
		int32_t L_23 = V_3;
		if ((((int32_t)L_23) < ((int32_t)((int32_t)32))))
		{
			goto IL_00f0;
		}
	}
	{
		int32_t L_24 = V_3;
		if ((((int32_t)L_24) > ((int32_t)((int32_t)126))))
		{
			goto IL_00f0;
		}
	}
	{
		StringBuilder_t * L_25 = ___builder1;
		Il2CppChar L_26 = V_2;
		NullCheck(L_25);
		StringBuilder_Append_m2383614642(L_25, L_26, /*hidden argument*/NULL);
		goto IL_0111;
	}

IL_00f0:
	{
		StringBuilder_t * L_27 = ___builder1;
		int32_t L_28 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2465617642_il2cpp_TypeInfo_var);
		String_t* L_29 = Convert_ToString_m2142825503(NULL /*static, unused*/, L_28, ((int32_t)16), /*hidden argument*/NULL);
		NullCheck(L_29);
		String_t* L_30 = String_PadLeft_m1263950263(L_29, 4, ((int32_t)48), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_31 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral3455432692, L_30, /*hidden argument*/NULL);
		NullCheck(L_27);
		StringBuilder_Append_m1965104174(L_27, L_31, /*hidden argument*/NULL);
	}

IL_0111:
	{
		int32_t L_32 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_32, (int32_t)1));
	}

IL_0115:
	{
		int32_t L_33 = V_1;
		CharU5BU5D_t3528271667* L_34 = V_0;
		NullCheck(L_34);
		if ((((int32_t)L_33) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_34)->max_length)))))))
		{
			goto IL_001a;
		}
	}
	{
		StringBuilder_t * L_35 = ___builder1;
		NullCheck(L_35);
		StringBuilder_Append_m1965104174(L_35, _stringLiteral3452614526, /*hidden argument*/NULL);
		return;
	}
}
// System.Void XUPorterJSON.MiniJSON::serializeNumber(System.Double,System.Text.StringBuilder)
extern "C"  void MiniJSON_serializeNumber_m3396057679 (RuntimeObject * __this /* static, unused */, double ___number0, StringBuilder_t * ___builder1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_serializeNumber_m3396057679_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringBuilder_t * L_0 = ___builder1;
		double L_1 = ___number0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2465617642_il2cpp_TypeInfo_var);
		String_t* L_2 = Convert_ToString_m545017250(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		StringBuilder_Append_m1965104174(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void XUPorterJSON.MiniJSON::.cctor()
extern "C"  void MiniJSON__cctor_m1969922968 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON__cctor_m1969922968_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((MiniJSON_t211875846_StaticFields*)il2cpp_codegen_static_fields_for(MiniJSON_t211875846_il2cpp_TypeInfo_var))->set_lastErrorIndex_13((-1));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		((MiniJSON_t211875846_StaticFields*)il2cpp_codegen_static_fields_for(MiniJSON_t211875846_il2cpp_TypeInfo_var))->set_lastDecode_14(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String XUPorterJSON.MiniJsonExtensions::toJson(System.Collections.Hashtable)
extern "C"  String_t* MiniJsonExtensions_toJson_m1282538434 (RuntimeObject * __this /* static, unused */, Hashtable_t1853889766 * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiniJsonExtensions_toJson_m1282538434_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Hashtable_t1853889766 * L_0 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t211875846_il2cpp_TypeInfo_var);
		String_t* L_1 = MiniJSON_jsonEncode_m1032995782(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String XUPorterJSON.MiniJsonExtensions::toJson(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  String_t* MiniJsonExtensions_toJson_m1260756840 (RuntimeObject * __this /* static, unused */, Dictionary_2_t1632706988 * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiniJsonExtensions_toJson_m1260756840_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t1632706988 * L_0 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t211875846_il2cpp_TypeInfo_var);
		String_t* L_1 = MiniJSON_jsonEncode_m1032995782(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Collections.ArrayList XUPorterJSON.MiniJsonExtensions::arrayListFromJson(System.String)
extern "C"  ArrayList_t2718874744 * MiniJsonExtensions_arrayListFromJson_m1736594186 (RuntimeObject * __this /* static, unused */, String_t* ___json0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiniJsonExtensions_arrayListFromJson_m1736594186_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___json0;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t211875846_il2cpp_TypeInfo_var);
		RuntimeObject * L_1 = MiniJSON_jsonDecode_m2285616255(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return ((ArrayList_t2718874744 *)IsInstClass((RuntimeObject*)L_1, ArrayList_t2718874744_il2cpp_TypeInfo_var));
	}
}
// System.Collections.Hashtable XUPorterJSON.MiniJsonExtensions::hashtableFromJson(System.String)
extern "C"  Hashtable_t1853889766 * MiniJsonExtensions_hashtableFromJson_m2915569823 (RuntimeObject * __this /* static, unused */, String_t* ___json0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiniJsonExtensions_hashtableFromJson_m2915569823_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___json0;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t211875846_il2cpp_TypeInfo_var);
		RuntimeObject * L_1 = MiniJSON_jsonDecode_m2285616255(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return ((Hashtable_t1853889766 *)IsInstClass((RuntimeObject*)L_1, Hashtable_t1853889766_il2cpp_TypeInfo_var));
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
